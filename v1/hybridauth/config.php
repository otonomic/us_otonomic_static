<?php
/**
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2014, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$host = $_SERVER['HTTP_HOST'];
if(strpos('otonomic.com', $host) !== false) {
    $facebook_app_id = "373931652687761";
    $facebook_app_secret = "d154036467714f4ac4706e653a1211ad";
}
if(strpos('verisites.com', $host) !== false) {
    $facebook_app_id = "202562333264809";
    $facebook_app_secret = "62f41e56660ac8d5851ac8ac79be11b5";
}
if(strpos('otonomic.test', $host) !== false) {
    $facebook_app_id = "286934271328156";
    $facebook_app_secret = "55bf8f49cd5030ba6d6fecb50b896a77";
}


return 
	array(
		"base_url" => "http://".$host."/hybridauth/index.php",

		"providers" => array ( 
			// openid providers
			"OpenID" => array (
				"enabled" => true
			),

			"Yahoo" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" ),
			),

			"AOL"  => array ( 
				"enabled" => true 
			),

			"Google" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "1088210894326-hjjrmql58q54q2ht4avhl5i6duk5p1p5.apps.googleusercontent.com", "secret" => "Oxc3sEoGC2H-GEbtHITMtxf7" ), 
			),

			"Facebook" => array ( 
				"enabled" => true,
				"keys"    => array (
                    "id" => $facebook_app_id,
                    "secret" => $facebook_app_secret
                ),
				"trustForwarded" => false,
                    "display" => 'popup'
			),

			"Twitter" => array ( 
				"enabled" => true,
				"keys"    => array (
                    "key" => "qGLS0xMSrfggxSVVqg4Sa8dqd",
                    "secret" => "uLhmzkgdYR8bjt5D4lU3S2PaQeVdmhbormVThfGv24ahPp7weL"
                )
			),

			// windows live
			"Live" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),

			"LinkedIn" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "75sly2eejaunvj", "secret" => "PaoZ91PeY6aRURxr" ) 
			),

			"Foursquare" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),

            "Instagram" => array(
                "enabled" => true,
                "keys"    => array (
                    "id" => "ef4ef97d5bda4b51b575e22b2d179d1a",
                    "secret" => "3497f47d336a4704a5851eb11e688586"
                )
            )
		),

		// If you want to enable logging, set 'debug_mode' to true.
		// You can also set it to
		// - "error" To log only error messages. Useful in production
		// - "info" To log info and error messages (ignore debug messages) 
		"debug_mode" => false,

		// Path to file writable by the web server. Required if 'debug_mode' is not false
		"debug_file" => "",
	);
