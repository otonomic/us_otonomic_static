var builder_domain;
if (is_localhost()) {
    builder_domain = "http://wp.test";
} else {
	builder_domain = 'http://wp.'+window.location.hostname.replace('www.', '');
}


function checkConnectedWithFacebook() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            // the user is logged in and has authenticated your
            // app, and response.authResponse supplies
            // the user's ID, a valid access token, a signed
            // request, and the time the access token
            // and signed request each expire
            var uid = response.authResponse.userID;
            var accessToken = response.authResponse.accessToken;

            console.log(uid);
            console.log(accessToken);

            $('#authorize_Facebook').hide();

        } else if (response.status === 'not_authorized') {
            // the user is logged in to Facebook,
            // but has not authenticated your app
        } else {
            // the user isn't logged in to Facebook.
        }
    });
}

function is_valid_email(email)
{
     return email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/);
}


(function ($, window, undefined) {

    var settings = {
        user_edits_contact: false,
        user_edits_store: false,
        user_edits_booking: true
    };

	window.do_redirect = 0;

	var page_id = getParameterByName('page_id');
	var page_name = getParameterByName('page_name');
    var category = getParameterByName('category');
    var category_list = getParameterByName('category_list');

	window.authorized_channel = [];
	
	var page_load_timestamp;
	
	var contact_load_timestamp;
	var store_load_timestamp;
	var category_load_timestamp;
	
	page_load_timestamp = new Date();

    if (page_name) {
        $('.site-name').html(page_name);
        $('.ot-fb-name').html(page_name);
    }

    if(category) {
        $('#fb_category').val(category);
    }

    track_event('Loading Page', 'Start');
	track_virtual_pageview('installer_start');
	jQuery('input[type=text]').addClass('LoNotSensitive');

    function move_slide(pressed_button, event) {
        var current_slide = pressed_button.parents('.installer-stage');
        $('.notice').hide();

        // Check for required fields
        var required_fields = pressed_button.attr('data-required-fields');
        if(required_fields && required_fields.length) {
            required_fields = required_fields.split(',');
            var can_move_slide = true;
            $.each(required_fields, function (index, value) {
                if (!$(value).val()) {
                    $(value + '-notice').show();
                    can_move_slide = false;
                }
                if($(value).val() && value == '#email'){
                    if(!is_valid_email($(value).val())){
                        $(value + '-valid-notice').show();
                        can_move_slide = false;
                    }
                }
            });
            if (!can_move_slide) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }

        // Move to next slide
        track_event('Loading Page', 'Next', current_slide.attr('id'));
        current_slide.fadeOut('slow', function () {
            current_slide.next().removeClass('hidden').fadeIn();
            $('html, body').animate({
                scrollTop: 0
            }, 500);
        });
    }

    // Intro next btn - Let the magic begin
    $('.btn-back').click(function(event) {
        var $this = $(this);
        var current_slide = $this.parents('.installer-stage');
        track_event('Loading Page', 'Back', current_slide.attr('id'));
        event.preventDefault();

        current_slide.fadeOut('slow', function () {
            current_slide.addClass('hidden');
        });
        current_slide.prev().removeClass('hidden').fadeIn();
    });

    // Stage next btn
    $('.btn-next').click(function(event){
        event.preventDefault();
        move_slide( $(this), event);
    });

	// Stage-3 next btn - Store/Booking
	$('.js-stage3-next').click(function(event){
        event.preventDefault();
        move_slide( $(this), event);

        var values = {};
        values.show_store = $('#option-online-store').hasClass('checked') ? 'yes' : 'no';
        values.show_booking = $('#option-booking').hasClass('checked') ? 'yes' : 'no';
        enqueue_submit('show_store',   values.show_store,   'send_store');
        return enqueue_submit('show_booking', values.show_booking, 'send_booking');
	});

    $(".js-switch-to-congratz").click(function(event){
        event.preventDefault();
        move_slide( $(this), event);
        switchToCongratz();
        $('#User_site_creation').submit();
    });

    $('#User_site_creation').submit(function(){

        return false;
    });

    // Stage-5 next btn - Select Template
    $('.btn-choose-template').click(function(event){
        event.preventDefault();
        move_slide( $(this), event);
        
        var skin = $(this).attr('data-option-value');
        $('input[name="skin"]').val(skin);
        return enqueue_submit('skin', skin, 'send_template');
    });

    jQuery('#link-tos').click(function (e){
		track_event('Loading Page', 'ToS', '');
	});

    $('[data-toggle="tooltip"]').tooltip();

	// function that switched to stage-5 from stage-4
	/////////////////////////////////////////////////////
	function switchToCongratz() {
        var form_data = $('#User_site_creation').serialize();

        var request_url;
        request_url = builder_domain+"/migration/index.php";

        /*console.log(form_data);
        return false;*/

        $.ajax({
            url: request_url,
            type: "GET",
            dataType: "jsonp",
            data: form_data,
            jsonp: "callback",
            jsonpCallback: "callback"
        });

		// CountDown
		var sec = 10;
		var timer = setInterval(function() { 
			if (sec > 1) {
				$('#congratz #counter').text(sec+' שניות');
                sec--;

            } else {
				$('#congratz #counter').text('שניה אחת');
                sec--;

				if (sec < -2) {
					$('.congratz-title').text('מעביר אתכם לאתר');
                    $('.ot-fb-name').html('');
                    $('.site-name').html('');

                    clearInterval(timer);

					// now redirect
                    //window.location.replace(window.site_url);
				}
			}

            if(sec == 4) {
				ga('set', 'metric5', '1');
                track_event('Loading Page', 'Redirect to website', '');
                window.do_redirect = 1;
                redirect_to_website();
            }
		}, 1000);
    }


	if (page_id) {
        window.site_url = builder_domain+'/wp-admin/admin-ajax.php?action=check_page&page_id='+page_id;
        $('#oto-web-url').html('<a href="'+window.site_url+'">this link</a>');
		createWebsiteUsingAjax(page_id);
        if(settings.user_edits_address) {
		    getFacebookPageAddress(page_id);
        }
	}

    // typeahead
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                }
            });

            cb(matches);
        };
    };

    var categories = [
        "ספרים ומגזינים > ספר",
        "ספרים ומגזינים > מחבר",
        "ספרים ומגזינים > מוציא לאור",
        "ספרים ומגזינים > חנות ספרים",
        "ספרים ומגזינים > ספרייה",
        "ספרים ומגזינים > מגזין",
        "ספרים ומגזינים > סדרת ספרים",
        "מוצרים ומותגים > מוצר / שירות",
        "מוצרים ומותגים > אתר אינטרנט",
        "מוצרים ומותגים > מכוניות",
        "מוצרים ומותגים > תיקים / מזוודות",
        "מוצרים ומותגים > מצלמה / תמונה",
        "מוצרים ומותגים > ביגוד",
        "מוצרים ומותגים > מחשבים",
        "מוצרים ומותגים > תוכנה",
        "מוצרים ומותגים > ציוד משרדי",
        "מוצרים ומותגים > אלקטרוניקה",
        "מוצרים ומותגים > בריאות / יופי",
        "מוצרים ומותגים > מוצרי חשמל",
        "מוצרים ומותגים > חומרי בניין",
        "מוצרים ומותגים > ציוד מסחרי",
        "מוצרים ומותגים > עיצוב בית",
        "מוצרים ומותגים > ריהוט",
        "מוצרים ומותגים > ספקי משק בית",
        "מוצרים ומותגים > מטבח / בישול",
        "מוצרים ומותגים > פאטיו / גינה",
        "מוצרים ומותגים > כלים / ציוד",
        "מוצרים ומותגים > יין / אלכוהול",
        "מוצרים ומותגים > תכשיטים / שעונים",
        "מוצרים ומותגים > חיות מחמד",
        "מוצרים ומותגים > ציוד  ספורט",
        "מוצרים ומותגים > מוצרים לתינוקות / ילדים",
        "מוצרים ומותגים > מזון / משקאות",
        "מוצרים ומותגים > ויטמינים / תוספי מזון",
        "מוצרים ומותגים > תרופות",
        "מוצרים ומותגים > טלפון סלולארי / טאבלט",
        "מוצרים ומותגים > משחקים / צעצועים",
        "מוצרים ומותגים > App הדף",
        "מוצרים ומותגים > משחק וידאו",
        "מוצרים ומותגים > משחקי לוח ",
        "חברות וארגונים > חברה",
        "חברות וארגונים > בריאות / יופי",
        "חברות וארגונים > מדיה / חדשות / הוצאה לאור",
        "חברות וארגונים > בנק / מוסד פיננסי",
        "חברות וארגונים > ארגון לא ממשלתי (NGO)",
        "חברות וארגונים > חברה לביטוח ",
        "חברות וארגונים > עסק קטן",
        "חברות וארגונים > אנרגיה / שירות",
        "חברות וארגונים > קמעונאות וסחורה לצרכן",
        "חברות וארגונים > מכוניות וחלקי חילוף",
        "חברות וארגונים > Industrials",
        "חברות וארגונים > תחבורה / הובלה",
        "חברות וארגונים > בריאות / רפואה / פרמצבטיקה",
        "חברות וארגונים > חלל האוויר / ביטחון",
        "חברות וארגונים > כרייה / חומרים",
        "חברות וארגונים > Farming / חקלאות",
        "חברות וארגונים > כימיקלים",
        "חברות וארגונים > שירותי ייעוץ / עסקים",
        "חברות וארגונים < חוק משפטי",
        "חברות וארגונים > חינוך",
        "חברות וארגונים > הנדסה / בנייה",
        "חברות וארגונים > מזון / משקאות",
        "חברות וארגונים > תקשורת",
        "חברות וארגונים > ביוטכנולוגיה",
        "חברות וארגונים > מחשבים / טכנולוגיה",
        "חברות וארגונים > אינטרנט / תוכנה",
        "חברות וארגונים > נסיעות / פנאי",
        "חברות וארגונים > ארגון קהילתי",
        "חברות וארגונים > ארגון פוליטי חברות וארגונים",
        "חברות וארגונים > בית כנסת / כנסיה / מסגד / ארגון דתי",
        "חברות וארגונים > ארגון",
        "חברות וארגונים > בית הספר",
        "חברות וארגונים > אוניברסיטה",
        "חברות וארגונים > ארגון ללא כוונת רווח",
        "חברות וארגונים > ארגון ממשלתי ",
        "חברות וארגונים > מפלגה פוליטית",
        "חברות וארגונים > בית ספר תיכון ",
        "עסקים מקומיים > מועדון ג'אז",
        "עסקים מקומיים > משפט לנוער",
        "עסקים מקומיים > רק בשביל כיף",
        "עסקים מקומיים > חנות תכשיטים",
        "עסקים מקומיים > ספק תכשיטים",
        "עסקים מקומיים > שירות ניקיון",
        "עסקים מקומיים > חטיבת ביניים",
        "עסקים מקומיים > מסעדה יפנית",
        "עסקים מקומיים > שייק & בר מיצים",
        "עסקים מקומיים > Hot Dog משותף",
        "עסקים מקומיים > עיצוב אתרים",
        "עסקים מקומיים > פיתוח אינטרנט",
        "עסקים מקומיים > תכנון חתונה",
        "עסקים מקומיים > שירות קידוחים",
        "עסקים מקומיים >לבוש רשמי / פורמלי",
        "עסקים מקומיים > בר יין",
        "עסקים מקומיים > טפטים",
        "עסקים מקומיים > חנות פאות",
        "",
        "עסקים מקומיים > פארק מים",
        "עסקים מקומיים > בריאות אישה",
        "עסקים מקומיים > שירותי כתיבה",
        "עסקים מקומיים > ניהול פסולות",
        "עסקים מקומיים > אירוע",
        "עסקים מקומיים > Eyewear",
        "עסקים מקומיים > טיולים אקולוגיים",
        "עסקים מקומיים > חינוך",
        "עסקים מקומיים > Esthethics",
        "עסקים מקומיים > בידור",
        "עסקים מקומיים > חשמלאי",
        "עסקים מקומיים > גן אירועים",
        'עסקים מקומיים > עורך דין נדל"ן',
        "עסקים מקומיים > תכנון אירועים",
        "עסקים מקומיים > נהר",
        "עסקים מקומיים > רודיאו",
        "עסקים מקומיים > Resort",
        "עסקים מקומיים > גגן",
        "עסקים מקומיים > ראפטינג",
        "עסקים מקומיים > RV Park",
        "עסקים מקומיים > רכבת",
        "עסקים מקומיים > רובוטיקה",
        "עסקים מקומיים > מאגר",
        "עסקים מקומיים > טיפוס צוקים",
        "עסקים מקומיים > רכבת הרים",
        "עסקים מקומיים > חדר ישיבות",
        "עסקים מקומיים > בית תה",
        "עסקים מקומיים > שירות חירום בצדי הדרכים",
        "עסקים מקומיים > מכוניות מירוץ",
        "עסקים מקומיים > מירוץ מסלול",
        "עסקים מקומיים > מסעדת ראמן",
        "עסקים מקומיים >  בית המשפט",
        "עסקים מקומיים > רדיו וציוד תקשורת",
        "עסקים מקומיים > רכבת תחתית ותחנת רכבת קלה",
        "עסקים מקומיים > מטווח",
        "עסקים מקומיים > מסלול נהיגה",
        "עסקים מקומיים > תיקון קראוונים",
        "עסקים מקומיים > סוכנות קראוונים",
        "עסקים מקומיים > מונית",
        "עסקים מקומיים > טניס",
        "עסקים מקומיים > תיאטרון",
        "עסקים מקומיים > הדרכה",
        "עסקים מקומיים > טקסטיל",
        "עסקים מקומיים > חנות צעצועים",
        "עסקים מקומיים > Theme Park",
        "עסקים מקומיים > מתרגם",
        "עסקים מקומיים > מדריך טיולים",
        "עסקים מקומיים > יוגה ופילאטיס",
        "עסקים מקומיים > ארגון נוער",
        "עסקים מקומיים > חנות יוגורט קפוא",
        "עסקים מקומיים > Farm העירוני",
        "עסקים מקומיים > שירות ריפוד",
        "עסקים מקומיים > שירותים ציבוריים",
        "עסקים מקומיים > מכללה ואוניברסיטה",
        "עסקים מקומיים > Inn",
        "עסקים מקומיים > החלקה על קרח",
        "עסקים מקומיים > מכונות קרח",
        "עסקים מקומיים > אינטרנט קפה",
        "עסקים מקומיים > סוכן ביטוח",
        "עסקים מקומיים >  מסעדה אירית",
        "עסקים מקומיים > יועץ תדמית",
        "עסקים מקומיים > סוכן ביטוח",
        "עסקים מקומיים > גלידה",
        "עסקים מקומיים > אחר",
        "עסקים מקומיים > אוקיינוס",
        "עסקים מקומיים > גינקולוגיה ומיילדות",
        "עסקים מקומיים > תזמורת",
        "עסקים מקומיים > אונקולוג",
        "עסקים מקומיים > אופטומטריסט",
        "עסקים מקומיים > ארגון",
        "עסקים מקומיים > חנות Outlet",
        "עסקים מקומיים > ציוד משרדי",
        "עסקים מקומיים > פאב",
        "עסקים מקומיים > פארק",
        "עסקים מקומיים > נמל",
        "עסקים מקומיים > שרברב / אינסטלטור",
        "עסקים מקומיים > חנייה",
        "עסקים מקומיים > צייר",
        "עסקים מקומיים > מדיום",
        "עסקים מקומיים > בית מרקחת",
        "עסקים מקומיים > פלסטיקה",
        "עסקים מקומיים > חנות לחיות מחמד",
        "עסקים מקומיים > פיינטבול",
        "עסקים מקומיים > מפלגת מרכז",
        "עסקים מקומיים > ציוד למסיבות",
        "עסקים מקומיים > סיירת ואבטחה",
        "עסקים מקומיים > מסעדה פקיסטנית",
        "עסקים מקומיים > & דרכון שירות ויזה",
        "עסקים מקומיים > Arcade",
        "עסקים מקומיים > חץ וקשת",
        "עסקים מקומיים > נמל תעופה",
        "עסקים מקומיים > חברת תעופה",
        "עסקים מקומיים > אלרגיה",
        "עסקים מקומיים > שמאי",
        "עסקים מקומיים > שעשועים",
        "עסקים מקומיים > אדריכל",
        "עסקים מקומיים > אודיטוריום",
        "עסקים מקומיים > בית ספר לאמנות",
        "עסקים מקומיים > דירה & Condo בניין",
        "עסקים מקומיים > מוצרי חשמל",
        "עסקים מקומיים > הכנסייה האפוסטולית",
        "עסקים מקומיים > שמאי מקרקעין",
        "עסקים מקומיים > חינוך מבוגרים",
        "עסקים מקומיים > שירות אימוץ",
        "עסקים מקומיים > סוכנות פרסום",
        "עסקים מקומיים > משאבי התמכרות",
        "עסקים מקומיים > בידור למבוגר",
        "עסקים מקומיים > הדרכת קבלה",
        "עסקים מקומיים > שירות פרסום",
        "עסקים מקומיים > מנהל בריאות",
        "עסקים מקומיים > כנסיית Adventist יום השביעית",
        "עסקים מקומיים > בריאות מנהל",
        "עסקים מקומיים > מסעדה אפריקאית",
        "עסקים מקומיים > הכנסייה האפיסקופלית המתודיסטית אפריקאית",
        "עסקים מקומיים > שירות חקלאי",
        'עסקים מקומיים > סוכן נדל"ן',
        "עסקים מקומיים > סוכנות דוגמנות",
        "עסקים מקומיים > סוכנות תיירות",
        "עסקים מקומיים > בריאות סוכנות",
        "עסקים מקומיים > סוכנות תעסוקה",
        "עסקים מקומיים > חשב",
        "עסקים מקומיים > דיקור סיני",
        "עסקים מקומיים > חיים פעילים",
        "עסקים מקומיים > חנות אבזרים",
        "עסקים מקומיים > חלקי חילוף ואביזרים רכב",
        "עסקים מקומיים > רכב חלקים ואביזרים",
        "עסקים מקומיים > שירותי הפלות",
        "עסקים מקומיים > ספא",
        "עסקים מקומיים > מדינה",
        "עסקים מקומיים > רחוב",
        "עסקים מקומיים > בית ספר",
        "עסקים מקומיים > אחסון",
        "עסקים מקומיים > סטארט-אפ  Startup",
        "עסקים מקומיים > מודד",
        "עסקים מקומיים > בגדי ים",
        "עסקים מקומיים > סימפוניה",
        "עסקים מקומיים > אבטחה",
        "עסקים מקומיים >  DJדי ג'י",
        "עסקים מקומיים > משרד הרישוי",
        "עסקים מקומיים > מעדנייה",
        "עסקים מקומיים > מעונות",
        "עסקים מקומיים > דיינר",
        "עסקים מקומיים > דוקטור",
        "עסקים מקומיים > רופא שיניים",
        "עסקים מקומיים > ספא",
        "עסקים מקומיים > בית מרקחת",
        "עסקים מקומיים > Farm חווה",
        "עסקים מקומיים > דייג",
        "עסקים מקומיים > חנות פרחים",
        "עסקים מקומיים > דוכן מזון Stand",
        "עסקים מקומיים > משאית אוכל",
        "עסקים מקומיים > יריד",
        "עסקים מקומיים > אח",
        "עסקים מקומיים > שוק הפשפשים",
        "עסקים מקומיים > חדר כושר",
        "עסקים מקומיים > Gay Bar",
        "עסקים מקומיים > גנן",
        "עסקים מקומיים > בר / מסעדה",
        "עסקים מקומיים > חנות מתנות",
        "עסקים מקומיים > קארטינג Go Karting",
        "עסקים מקומיים > מגרש גולף",
        "עסקים מקומיים > תחנת דלק",
        "עסקים מקומיים > בית",
        "עסקים מקומיים > Hotel - בית מלון",
        "עסקים מקומיים > אכסניה",
        "עסקים מקומיים > סוסים",
        "עסקים מקומיים > אוטוסטרדה",
        "עסקים מקומיים > בית חולים",
        "עסקים מקומיים > בריאות וספא",
        "עסקים מקומיים > מספרה",
        "עסקים מקומיים > כלי בית",
        "עסקים מקומיים > עיצוב בית",
        "עסקים מקומיים > מלונה",
        "עסקים מקומיים > קריוקי",
        "עסקים מקומיים > חומרי גלם לבישול",
        "עסקים מקומיים > מסעדה כשרה",
        "עסקים מקומיים > מסעדה קוריאנית",
        "עסקים מקומיים > בניית מטבח",
        "עסקים מקומיים > אגם",
        "עסקים מקומיים > הלוואות",
        "עסקים מקומיים > טרקלין",
        "עסקים מקומיים > לינה",
        "עסקים מקומיים > ספרייה",
        "עסקים מקומיים > לוביסט",
        "עסקים מקומיים > נקודת ציון",
        "עסקים מקומיים > תג לייזר",
        "עסקים מקומיים > מסגר",
        "עסקים מקומיים > גן חיות & Aquarium",
        "עסקים מקומיים > פינת ליטוף",
        "עסקים מקומיים > מחנה",
        "עסקים מקומיים > קפה",
        "עסקים מקומיים > עיר",
        "עסקים מקומיים > בקתה",
        "עסקים מקומיים > אנשי דת",
        "עסקים מקומיים > קליניקה",
        "עסקים מקומיים > כנסייה",
        "עסקים מקומיים > קזינו",
        "עסקים מקומיים > מחוז",
        "עסקים מקומיים > קרוז",
        "עסקים מקומיים > משחקי וידאו",
        "עסקים מקומיים > רופא וטרינר",
        "עסקים מקומיים > חנות וינטג '",
        "עסקים מקומיים > השכרת בית נופש",
        "עסקים מקומיים > מסעדה וייטנאמית",
        "עסקים מקומיים > ממכר שירות מכונות",
        "עסקים מקומיים > מסעדה צמחונית וטבעונית",
        "עסקים מקומיים > עתיקות / וינטג'",
        "עסקים מקומיים > חנות פירות וירקות",
        "עסקים מקומיים > אצטדיון ספורט",
        "עסקים מקומיים > בר",
        "עסקים מקומיים > בנק",
        "עסקים מקומיים > חוף ים",
        "עסקים מקומיים > מאפייה",
        "עסקים מקומיים > גשר",
        "עסקים מקומיים > מבשלה",
        "עסקים מקומיים > שייט בסירה",
        "עסקים מקומיים > איטליז",
        "עסקים מקומיים > קו אוטובוס",
        "עסקים מקומיים > שחרור בערבות",
        "עסקים מקומיים > בייביסיטר",
        "עסקים מקומיים > Bar & Grill גריל בר",
        "עסקים מקומיים > כלוב",
        "עסקים מקומיים > הכנסייה הבפטיסטית",
        "עסקים מקומיים > נני / בייביסיטר",
        "עסקים מקומיים > סיעוד",
        "עסקים מקומיים > חיי לילה",
        "עסקים מקומיים > עיתון",
        "עסקים מקומיים > מועדון הלילה",
        "עסקים מקומיים > סלון ציפורניים",
        "עסקים מקומיים > תזונאי",
        "עסקים מקומיים > בית אבות",
        "עסקים מקומיים > שכונה",
        "עסקים מקומיים > נוטריון ציבורי",
        "עסקים מקומיים > Mover שירותי הובלה",
        "עסקים מקומיים > Motel  מוטל",
        "עסקים מקומיים > מסגד",
        "עסקים מקומיים > מוזיאון",
        "עסקים מקומיים > שוק",
        "עסקים מקומיים > מרינה",
        "עסקים מקומיים > מתכות",
        "עסקים מקומיים > בנייה",
        "עסקים מקומיים > עיסוי",
        "קולנוע > שחקן / במאי",
        "קולנוע > סרט",
        "קולנוע > המפיק",
        "קולנוע > תסריטאי",
        "קולנוע > Studio",
        "קולנוע > קולנוע",
        "קולנוע > פרס הטלוויזיה / קולנוע",
        "קולנוע > דמות בדיונית",
        "קולנוע > שחקני קולנוע",
        "מוזיקה > אלבום",
        "מוזיקה > שיר",
        "מוזיקה > מוסיקאי / להקה",
        "מוזיקה > וידאו קליפ",
        "מוזיקה > סיבוב הופעות",
        "מוזיקה > מקומות להופעות",
        "מוזיקה > תחנת רדיו",
        "מוזיקה > חברת תקליטים",
        "מוזיקה > פרס מוזיקה",
        "מוזיקה > תרשים מוזיקה",
        "אחר > קהילה",
        "אחר > רק בשביל הכיף",
        "אנשים > שחקן / במאי",
        "אנשים > מפיק",
        "אנשים > תסריטאי",
        "אנשים > דמות בדיונית",
        "אנשים > שחקן קולנוע",
        "אנשים > מוסיקאי / להקה",
        "אנשים > מחבר",
        "אנשים > ספורטאי",
        "אנשים > אמן",
        "אנשים > איש ציבור",
        "אנשים > עיתונאי",
        "אנשים > אישיות חדשות",
        "אנשים > שף",
        "אנשים > איש עסקים",
        "אנשים > קומיקאי",
        "אנשים > בדרן",
        "אנשים > מורה",
        "אנשים > רקדנית",
        "אנשים > מעצב",
        "אנשים > צלם",
        "אנשים > יזם",
        "אנשים > פוליטיקאי",
        "אנשים > פקיד ממשלתי",
        "אנשים > מאמן",
        "אנשים > חיות מחמד",
        "ספורט > ספורט",
        "ספורט > ליגת ספורט",
        "ספורט > קבוצת ספורט מקצועית ",
        "ספורט > מאמן",
        "ספורט > קבוצה חובבנים ",
        "ספורט > בית ספר ספורט ",
        "ספורט >  אירוע ספורט",
        "ספורט > מתחם ספורט",
        "טלוויזיה > שחקן / במאי",
        "טלוויזיה > תסריטאי",
        "טלוויזיה > אולפנים",
        "טלוויזיה > טלוויזיה / פרס סרט",
        "טלוויזיה > דמות בדיונית",
        "טלוויזיה > שחקן קולנוע",
        "טלוויזיה > תכנית טלוויזיה",
        "טלוויזיה > רשת טלוויזיה ",
        "טלוויזיה > ערוץ טלוויזיה",
        "טלוויזיה > פרק",
        "טלוויזיה > עונת טלוויזיונית",
        "אתרי אינטרנט ובלוגים > בלוג אישי,",
        "אתרי אינטרנט ובלוגים > אתר מדעי הרוח / אומנויות",
        "אתרי אינטרנט ובלוגים > אתר עסקים / כלכלה",
        "אתרי אינטרנט ובלוגים > אתר אינטרנט / מחשבים",
        "אתרי אינטרנט ובלוגים > אתר אינטרנט חינוך",
        "אתרי אינטרנט ובלוגים > אתר בידור",
        "אתרי אינטרנט ובלוגים > אתר ממשלה",
        "אתרי אינטרנט ובלוגים > אתר בריאות / בריאות",
        "אתרי אינטרנט ובלוגים > דף בית / אתר גן",
        "אתרי אינטרנט ובלוגים > אתר חדשות / מדיה",
        "אתרי אינטרנט ובלוגים > אתר ספורט",
        "אתרי אינטרנט ובלוגים > אתר התייחסות",
        "אתרי אינטרנט ובלוגים > אתר אינטרנט אזורי",
        "אתרי אינטרנט ובלוגים > אתר מדע",
        "אתרי אינטרנט ובלוגים > אתר חברה / תרבות",
        "אתרי אינטרנט ובלוגים > אתר תיירות מקומי",
        "אתרי אינטרנט ובלוגים > אתרים של בני נוער של / ילדים",
        "אתרי אינטרנט ובלוגים > אתר אישי",
"Non-governmental Organization (ngo)",
        "Restaurant/cafe",
        "Food & Restaurant",
        "Beer Garden",
        "Brewery ",
        "Cyber Cafe",
        "Cafeteria",
        "Coffee Shop",
        "Cupcake Shop",
        "Donuts & Bagels",
        "Farmers Market",
        "Frozen Yogurth Shop",
        "Restaurant",
        "Restaurant Wholesale",
        "Salad Bar",
        "Smoothie Bar & Juice Bar",
        "Sports Bar",
        "Shopping & Retail",
        "Antique Store",
        "Auction House",
        "Arts & Crafts Supply Store",
        "Big Box Retailer",
        "Bike Shop",
        "Bookstore",
        "Religious Bookstore",
        "Camera Store",
        "Comic Store",
        "Clothing Store",
        "Accesories Store",
        "Bridal Shop",
        "Children's Clothing Store",
        "Costume Shop",
        "Men's Clothing Store",
        "Shoe Store",
        "Sportswear",
        "Women's Clothing Store",
        "Computer Store",
        "Electronics Store",
        "Computers & Electronics ",
        "Convenience Store",
        "Lottery Store",
        "Tobacco Store",
        "Cosmetics & Beauty Supply",
        "Department Store",
        "Discount Store",
        "Dry Cleaner",
        "Dvd & Video Store",
        "Food & Grocery",
        "Candy Store",
        "Liquor Store",
        "Meat Shop",
        "Specialty Grocery Store",
        "Health Food Store",
        "Grocery Store",
        "Furniture Repair",
        "Furniture Store",
        "Blinds & Curtains",
        "Cabinets & Countertops",
        "Carpenter",
        "Carpet & Flooring Store",
        "Carpet Cleaner",
        "Cleaning Service",
        "Concrete Contractor",
        "Construction Service & Supply",
        "Contractor",
        "Garage Door Services",
        "Glass Service",
        "Hardware Store",
        "Hardware & Tools Service",
        "Heating, Ventilating & Air Conditioning",
        "Home Cleaning",
        "Home Security",
        "Interior Designer",
        "Landscaping",
        "Lighting Fixtures",
        "Mattresses & Bedding",
        "Mobile Homes",
        "Pest Control",
        "Portable Toilet Rentals",
        "Property Management",
        "Sewer Service",
        "Solar Energy Service",
        "Storage ",
        "Swimming Pool Maintenance",
        "Tools Service",
        "Home Improvement",
        "Laundromat",
        "Luggage Service",
        "Mobile Phone Shop",
        "Music Store",
        "Musical Instrument Store",
        "Outdoor Equipement Store",
        "Shopping District",
        "Shopping Mall",
        "Signs & Banner Service",
        "Sporting Goods Store",
        "Wholesale & Supply Store",
        "Winery & Vineyard",
        "Tapas Bar & Restaurant",
        "Dance Club",
        "Hookah Lounge",
        "Professional Services ",
        "Fashion Designer",
        "Graphic Design",
        "Market Research Consultant",
        "Marketing Consultant",
        "Music Production",
        "Photographic Services & Equipment",
        "Public Relations",
        "Screen Printing & Embroidery",
        "Sports Promoter",
        "Trophies & Engraving",
        "Web Design ",
        "Auto Body Shop",
        "Auto Glass",
        "Automobile Leasing",
        "Automotive Consultant",
        "Automotive Customizing",
        "Automotive Parts & Accesories",
        "Automotive Repair",
        "Automotive Storage",
        "Automotive Wholesaler",
        "Car Dealership",
        "Car Parts & Accesories",
        "Car Wash & Detailing",
        "Motorcycle Repair",
        "Motorcycles",
        "Tire Dealer",
        "Business Consultant",
        "Medical & Health",
        "Law Practice",
        "Event Planning",
        "Bands & Musicians",
        "Bartending Service",
        "Caterer",
        "Currency Exchange",
        "Investing Service",
        "Mortgage Brokers",
        "Tax Preparation",
        "Personal Trainer",
        "Personal Coaching",
        "Dog Walker",
        "Dog Training",
        "Family Doctor",
        "Tanning Salon",
        "Beauty Salon",
        "Ticket Sales",
        "School Transportation",
        "Culinary School",
        "Driving School",
        "Language School",
        "Spa, Beauty & Personal Care",
        "Medical Spa",
        "Dental Equipment",
        "Dance Instruction",
        "Dating Service",
        "Day Care & Preschool",
        "Nutricionist",
        "Makeup Artist",
        "Martial Arts",
        "Maid & Butler",
        "Medical Lab",
        "Medical Supplies",
        "Physical Fitness",
        "Physical Trainer",
        "Laser Hair Removal ",
        "Tattoo & Piercing",
        "Pizza Place",
        "Plastic Surgery",
        "Podiatrist",
        "Sandwich Shop",
        "Music Lessons & Instruction",
        "Cooking Lesson",
        "Technical Institute",
        "Sports Instruction",
        "Traffic School",
        "Skin Care ",
        "Fitness Center",
        "Acupuncture ",
        "Alternative & Holistic Health",
        "Chiropractor",
        "Counseling & Mental Health",
        "Physical Therapist",
        "Real Estate Service",
        "Pet Groomer",
        "Pet Sitter",
        "Animal Shelter",
        "Swimming Pool"
    ];

    $('#cat-selector .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
    {
        name: 'categories',
        displayKey: 'value',
        source: substringMatcher(categories)
    });

    $('.clear-input').each(function (index){
        input = jQuery(this).prev();
        $(input).on('change keyup paste', function() {
            if($(this).val().length>0) {
                jQuery(this).next().show();
            }
            else {
                jQuery(this).next().hide();
            }
        });
        $(this).bind('click', function (e){
            $(this).hide();
            input = jQuery(this).prev();
            input.val('');
            wrapper = jQuery(this).parent().parent().parent();
            jQuery('.search-results-container', wrapper).hide();
        });
    });
    $(".to-social-page").on('click',function(e){
        var busniess_name = $(".business_name").val();
        if(busniess_name !== ""){
            site_url  = 'http://otonomic.com/sandbox/developers/omri/social_accounts_finder.php?q='+busniess_name;
                $.ajax({
                type: "POST",
                url: site_url,
                error: function (data, status, jqxhr) {
                    $(".social_searching_msg").fadeOut();                    
                }
                }).done(function(data){
                    console.log(data);
                    var parsed_data = $.parseJSON(data);
                    console.log(parsed_data.results);
                    $.each(parsed_data.results, function(key, value) {
                                                        
                            if(key.toLowerCase().indexOf("facebook.com") > -1){
                                $("#social_media_facebook").val(value);
                            }
                            if(key.toLowerCase().indexOf("yelp") > -1){
                                $("#social_media_yelp").val(value);
                            }
                            if(key.toLowerCase().indexOf("instagram") > -1){
                                $("#social_media_instagram").val(value);
                            }
                            if(key.toLowerCase().indexOf("youtube") > -1){
                                $("#social_media_youtube").val(value);
                            }
                            if(key.toLowerCase().indexOf("twitter") > -1){
                                $("#social_media_twitter").val(value);
                            }
                            if(key.toLowerCase().indexOf("linkedin") > -1){
                                $("#social_media_linkedin").val(value);
                            }
                            if(key.toLowerCase().indexOf("flickr") > -1){
                                $("#social_media_flickr").val(value);
                            }
                            if(key.toLowerCase().indexOf("play.google.com") > -1){
                                $("#social_media_googleplus").val(value);
                            }
                            if(key.toLowerCase().indexOf("pinterest") > -1){
                                $("#social_media_pinterest").val(value);
                            }                            
                    });
                    $(".social_searching_msg").fadeOut();
                });
        }
    });

    $('.business_name').on('blur', function(e) {
        // TODO: Add code here

    })
})(jQuery, window);


/* required functions */
function timed_submit(submit_function, submit_parameter) {
    if (window.is_blog_ready == 1) {
        submit_function();
    } else {
        window[submit_parameter] = 1;
    }
}

function callback(data) {
	window.is_blog_ready = 1;

	if (data.redirect.indexOf("http://") < 0) {
		data.redirect = "http://" + data.redirect;
	}

	if (data.site_url.indexOf("http://") < 0) {
		data.site_url = "http://" + data.site_url;
	}

	if (data.status == 'fail') {
		window.location = data.site_url;
		track_event('Account Manage', 'Site Exists', data.message);
		ga('set', 'metric6', '1');
        track_virtual_pageview('site_exists');

	} else {
		var page_type = window.page_type || 'Fan Page';
		track_event('Account Manage', 'Site Created', page_type);
		ga('set', 'metric4', '1');
        track_virtual_pageview('site_created');
	}

    <!-- START Facebook Pixel Tracking for Site created-->
	window._fbq = window._fbq || [];
    if(!is_localhost()) {
	    window._fbq.push(['track', facebook_site_created_pixel_id, {'value':'0.00', 'currency':'USD'}]);
    }
    <!-- END Facebook Pixel Tracking -->

	window.site_url = data.site_url;
	// window.blog_redirect = data.redirect;
    window.blog_redirect = data.site_url;
	window.blog_id = data.blog_id;
	window.token = data.token;

	jQuery('#oto-web-url').html('<a href="'+data.redirect+'">'+data.site_url+'</a>');

    if( data.status === 'fail') {
        alert( data.message);
        window.location.replace(data.redirect);
        return;
    }

	//blog_created();

    redirect_to_website();
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getFacebookPageAddress(page_id) {
	var facebook_query_page_url = "https://graph.facebook.com/" + page_id;
	$.get(facebook_query_page_url, function (data) {
		if (data.location != undefined && data.location.latitude != undefined && data.location.longitude != undefined) {
			delete data.location.latitude;
			delete data.location.longitude;
		}
		var address_parts = [];

		for (var x in data.location) {
			address_parts.push(data.location[x]);
		}

		var phone = (data.phone) ? data.phone : "";
		var address = (address_parts.join(", ")) ? address_parts.join(", ") : "";
		var email = (data.email) ? (data.email) : "";

		if (data.likes != undefined) {
			window.page_type = 'Fan Page';
		} else {
			window.page_type = 'Personal Page';
		}
		window.parsed_page_data = {
			'phone': phone,
			'address': address,
			'email': email
		}
	}, "json");
}

function is_localhost() {
	if (location.host == 'otonomic.test' || location.host == 'localhost') {
		return true;
	}

	return false;
}




function createWebsiteUsingAjax(page_id) {
	var request_data = {};
	request_data.theme = "dreamthemeVC";
    request_data.lang = "he_IL";
	request_data.facebook_id = encodeURIComponent(page_id);

	// var request_url = "http://wp.otonomic.com/migration/index.php?" + $.param(request_data);
	localhost = is_localhost();

	var request_url;
    request_url = builder_domain+"/migration/index.php";

	return $.ajax({
		url: request_url,
		type: "GET",
		dataType: "jsonp",
		data: request_data,
		jsonp: "callback",
		jsonpCallback: "callback"
	});
}

function send_template() {
    var skin = window.skin || '';
    track_event('Loading Page', 'Select Template', skin);
    return post_WP_settings({ skin: skin }, 'Select Template');
}

function blog_created() {
    window.callbacks = window.callbacks || [];
    $.each( window.callbacks, function(index, callback_function) {
        window[callback_function]();
    });

	send_user_fb_details();
	send_user_authorized_channel();

	return;
}

function redirect_to_website() {
	if(window.do_redirect == 1 && window.is_blog_ready == 1) {
        window.location.replace(window.blog_redirect);
	}
}


function post_WP_settings(data, tracking_action, endpoint) {
    endpoint = endpoint  || 'settings.set_many';
    tracking_action = tracking_action  || data;

    return request = $.ajax({
        type: "POST",
        url: window.site_url + '/?json=' + endpoint,
        data: { values: data },
        success: function (data, status, jqxhr) {
            if (jqxhr.status == 307) {
                $.post(window.site_url + '/?json=settings.set_many', { values: values_changes });
                track_event('Loading Page', tracking_action, '307');
                return;
            }
            if (data.status == "ok") {
                track_event('Loading Page', tracking_action, 'Success');
            } else {
                track_event('Loading Page', tracking_action, 'Failure: data.respond.msg: ' + (data.respond && data.respond.msg));
            }
        },
        complete: function (jqxhr, status) {
            if (status !== 'success') {
                track_event('Loading Page', tracking_action, 'Failure: ' + status);
            }
        }

//		statusCode: {
//			200: function (data_or_jqxhr, status, jqxhr_or_err) {debugger;
//				return;
//			},
//			307: function (data_or_jqxhr, status, jqxhr_or_err) {debugger;
//				$.post(window.site_url + '/?json=settings.set_many', { values: values_changes });
//			}
//		}
    });
}

function enqueue_submit(setting, value, callback_function) {    
    window[setting] = value;

    if(window.is_blog_ready) {
        window[callback_function]();

    } else {
        window.callbacks = window.callbacks || [];
        window.callbacks.push(callback_function);
    }
}

function send_site_category() {
	_facebook_category = window.facebook_category;
	var values_changes = { facebook_category: _facebook_category };
    return post_WP_settings(values_changes, 'Send Site Category');
}

function send_user_fb_details()
{
	fb_user_auth = getParameterByName('fb_user_auth');
	fb_user_id = getParameterByName('fb_user_id');
	fb_user_t = getParameterByName('fb_user_t');

	if(fb_user_auth == 'yes')
	{
		var settings_data = {
			wp_otonomic_blog_connected: 'yes',
			otonomic_connected_fb_user_id: fb_user_id,
			otonomic_connected_fb_user_token: fb_user_t
		};
		post_WP_settings(settings_data, 'FB Connected');
	}
}

function send_user_authorized_channel()
{
	if(window.authorized_channel.length>0) {
		jQuery.each(window.authorized_channel, function (key, value) {
			var channel = value['channel'];
			var auth_data = value['auth_data'];
			console.log(channel);
			console.log(auth_data);
			var settings_data = {};
			settings_data[channel] = auth_data;

			if(channel == 'Facebook')
			{
				post_WP_settings({wp_otonomic_blog_connected: 'yes'}, 'FB Connected');
			}
			post_WP_settings(settings_data, 'User authorized channel');
			delete window.authorized_channel[channel];
		});
	}
}

function userConnected(channel,auth_data){
    track_event('Loading Page', 'Social channel connected', channel);

    social_network = channel+"_user_auth";
	
	window.authorized_channel.push({
            'channel': channel, 
            'auth_data':  auth_data
	});
	//send_user_authorized_channel();
	timed_submit(send_user_authorized_channel, 'user_authorized_channel');
    $('#authorize_'+channel).addClass('connected');
	$('#authorize_'+channel).append('<img class="social-check" src="images/social-check.png">');
}

jQuery(document).ready(function($){
    $('.wizard-image-gallery').otoImageGallery({
        multiple:false,
        on_select: function ($element, $attachment){
            // get the base structure to clone
            var parent = $($element).parent().parent();
            //$('.gallery-selected-images', parent).html('');
            $.each($attachment, function (index, val){
                var clone = $('.clone .selected-image', parent).clone();
                $('img', clone).attr('src', val);
                $('input', clone).val(val);
                $('.gallery-selected-images', parent).append(clone);
            });
        }
    });
    $(document).on('click', '.remove-selected-image', function (e){
        var parent = $(this).parent();
        $(parent).remove();
    });
});