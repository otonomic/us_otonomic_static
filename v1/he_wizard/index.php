<?php
include_once('../shared/MediaModal/Init.class.php');
$config = array(
    /*'gallery_api_url' => site_url('/api/gallery/get_images/'),
    'fb_page_id'=>get_option('facebook_id'),*/
    'fb_token'=>'389314351133865|O4FgcprDMY0k6rxRUO-KOkWuVoU',
    'fb_user_token'=>'CAAUlWzuUGiYBAO7BZA8ek15RMSy8LNdD38wWlO9ssi6fPjdUFl3dSdxiB6eddwtZBWEEied2cPV8UmYH9zTqr1x2hhJCtRx60w0ZA4tdHziZAZCBzEUEFSFsMLXDnZCTZAQW3RETBAbtto9sIhiDjvwnIxeVSrbYY9ICTfOvLlxniqjnNWDK3tuxwTRBcUzQWvIxA3ZCeVBQgkYcYUE7V08p'
);
$otoGalleryInit = new \OTO\Modules\MediaModal\Init($config);
$otoGalleryInit->init();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-scalable=0">
    <meta name="description" content="Otonomic Site Creation Page">
    <meta name="author" content="Otonomic">
    <link rel="shortcut icon" href="favicon.ico">

    <title>Otonomic is creating your site...</title>

      <!-- Bootstrap core CSS -->
      <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

      <!-- Glyphicons -->
      <link href="fonts/Glyphicons-WebFont/glyphicons.css" rel="stylesheet">

      <!--<link rel='stylesheet' id='sb_instagram_icons-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css?ver=4.2.0' type='text/css' media='all' />-->

      <!-- Aller font -->
      <link href="fonts/Aller-WebFont/aller_font.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wp-loading-page.css" rel="stylesheet">

      <!--<script src="js/jquery-1.11.1.min.js"></script>-->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

      <!--<script src="js/bootstrap.min.js"></script>-->
      <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

      <script src="js/typeahead.jquery.min.js"></script>

      <?php $otoGalleryInit->outputScript(); ?>
      <?php $otoGalleryInit->outputStyles(); ?>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

      <![endif]-->

      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-37736198-1', 'auto');
          ga('set', 'dimension4', window.location.hostname); // Site url
          ga('set', 'dimension5', 'otonomic marketing site'); // Site Type
          ga('send', 'pageview');
      </script>

      <script src="/v1/js/otonomic-analytics.js?v=1.0"></script>

      <style>
          body {
              direction: rtl;
              text-align: right;
          }
/*
          .bg-image {
              left: 0;
              right: 635px;
          }
*/
          .content-panel {
              text-align: right;
          }
          .btn-ttc-orange .glyphicon, .btn-ttc-clear .glyphicon {
              margin-left: 10px;
              margin-right: 0;
          }
          input, textarea, keygen, select, button {
              text-align: right;
          }
          .radio label, .checkbox label {
              padding-right: 20px;
          }
          .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
              margin-right: -20px;
          }

          .btn-add-on {
              padding-right: 60px;
              padding-left: 0;
              text-align: right;
          }
          .btn-add-on .glyphicons {
              left: 20px;
              right: auto;
          }
          .btn-checkbox {
              background-position-x: calc(100% - 20px);
          }

          .btn-add-on:hover {
              padding-left: inherit;
              padding-right: 60px;
          }
          .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
              float: right;
          }
          #twitter .form-control-feedback, #yelp .form-control-feedback, #youtube .form-control-feedback, #instagram .form-control-feedback, #facebook .form-control-feedback, #linkedin .form-control-feedback, #flickr .form-control-feedback, #googleplus .form-control-feedback, #pinterest .form-control-feedback {
              left: 15px;
              right: auto;
          }

          body.rtl .btn-choose-template span.glyphicons.ok_2 {
              margin-left: 5px;
              margin-top: -4px;
          }
          body.rtl .search-results-container {
              text-align: right;
              direction: rtl;
          }
          body.rtl .has-feedback .form-control {
              padding-left: 42.5px;
              padding-right: 10px;
          }
          body.rtl .pull-opposite {
              float:left;
          }

		  .btn-checkbox, .btn-checkbox:hover {
		  padding: 25px 54px 25px 18px;
		  }
	.btn-device, .btn-device:hover {
  padding-right: 74px;
  padding-left: 38px;
}

		  .form-group {
	  margin-top: 15px;
	}



          .notice {
              display: none;
              color: red;
              font-weight: bold;
          }
          .required {
              color: red;
          }


          @media (max-width: 1000px) {
              .bg-image-5 {
                  background-position-x: calc(50% + 130px);
              }
              .bg-image-4 {
                  background-position-x: calc(50% - -130px);
              }
              .bg-image-7 {
                  background-position-x: calc(50% - -40px);
              }
              .bg-image-social-new {
                  background-position-x: calc(50% + 20px);
              }
              .bg-image-8 {
                  background-position-x: calc(50% + 0px);
              }

          }



      </style>
  </head>

  <body class="wp-lp rtl">
      <!-- Google Tag Manager -->
      <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK43MV"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WK43MV');</script>
      <!-- End Google Tag Manager -->


  <!-- Facebook SDK -->
  <div id="fb-root"></div>
  <script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=575528525876858&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- /Facebook SDK -->
  
    <!-- Intro    ========================================================== -->
    <div class="container-fluid">
      <form id="User_site_creation">

      <input type="hidden" name="source" id="source" value="form">
      <input type="hidden" name="theme"  id="theme"  value="dreamthemeVC">
      <input type="hidden" name="lang"  id="lang"  value="he_IL">

      <!-- Stage ================================================ -->
<!--
      <div id="intro" class="row installer-stage">
        <div class="bg-image hidden-xs"></div>
        <div class="col-xs-12">
          <div class="text-center">
            <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
            <h1 class="title">
                צרו אתר עבור
            </h1>
            <p class="site-name" id="ot-fb-name">
העסק שלכם
            </p>
            <h2>
                ב-5 צעדים בלבד!
            </h2>
            <a href="#" onclick="return false;" class="btn btn-ttc-blue btn-next">
בואו נצא לדרך
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
          </div>
        </div>
      </div>
    -->

      <!-- Stage ================================================ -->
      <div id="stage-name-category" class="row installer-stage">
          <div class="bg-image bg-image-5 hidden-xs" style="
            background-image: url(images/bg5.jpg);
          "></div>
          <div class="content-panel">
              <h1 class="title">
                  בואו ניצור אתר שמתאים לכל מכשיר
              </h1>
              <h3>
                  מלאו מספר פרטים ואנחנו כבר נעשה את העבודה בשבילכם.
              </h3>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <label for="productName">
                                  שם העסק
                              <span class="required"> * </span>
                              </label>
                              <div id="name-notice" class="notice">
                                  אנא מלאו שדה זה
                              </div>
                              <textarea rows="3" id="name" name="name" class="form-control business_name" autocomplete="off" placeholder="לדוגמא ישראל ישראלי עיצוב שיער"></textarea>
                          </div>

                          <div class="form-group">
                              <label for="fb_category">קטגוריה</label>
                              <span class="required"> * </span>
                              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" class="tooltip" title="התחילו להקליד ורשימת קטגוריות מתאימות תופיע. לחצו על המתאימה ביותר.
"></i>
                              <div id="category-notice" class="notice">
                                  אנא מלאו שדה זה
                              </div>
                              <div id="cat-selector" class="">
                                  <input name="category" class="typeahead form-control pulse-background" type="text" placeholder="התחילו להקליד כדי לראות תוצאות"    id="category">
                                  <!-- span class="glyphicon glyphicon-search form-control-feedback"></span -->
                              </div>
                          </div>

                          <div class="form-group action-buttons">
                              <a href="#" onclick="return false;" class="btn-next pull-opposite btn btn-ttc-orange btn-lg validate-required" data-required-fields="#name,#category">
                                  <span class="glyphicon glyphicon-ok"></span>
                                  הבא
                              </a>
                          </div>
                      </div>
                  </div>
          </div>
      </div>

      <!-- Stage ================================================ -->
      <div id="stage-contact-details" class="row hidden installer-stage">
          <div class="bg-image bg-image-4 hidden-xs" style="
            background-image: url(images/bg4.jpg);
          "></div>
          <div class="content-panel">
              <h1 class="title">
                  הכניסו פרטים, בכדי שאפשר יהיה למצוא את העסק שלכם בקלות
              </h1>
              <!--<form role="form" id="business-details" action="">-->
              <div class="row">
                  <div class="col-xs-12">
                      <div class="form-group">
                          <label for="email">אימייל</label>
                          <span class="required"> * </span>
                          <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" class="tooltip" title="כתובת האימייל תופיע באתר שלך. פניות ליצירת קשר ישלחו אל כתובת זו."></i>
                          <div id="email-notice" class="notice">
                              אנא מלאו שדה זה
                          </div>
                          <div id="email-valid-notice" class="notice">
אנא הזינו כתובת אימייל תקינה
                          </div>
                          <input type="email" class="form-control" id="email" name="email" value="">
                      </div>

                      <div class="form-group">
                          <label for="productName">כתובת
                              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" 
                                 title="האתר שלך יציג את כתובתך ומפה דינמית של גוגל, כדי שהלקוחות יוכלו למצוא אותך בקלות." class="tooltip"></i>
                          </label>
                          <textarea rows="3" id="address" name="address" class="form-control" autocomplete="off" placeholder=""></textarea>
                      </div>

                      <div class="form-group">
                          <label for="phone">טלפון</label>
                          <input type="text" class="form-control" id="phone" name="phone" value="">
                      </div>
<!--
                      <div class="checkbox">
                          <label for="show_opening_hours"><input type="checkbox" id="show_opening_hours" name="show_opening_hours">
                              הראה שעות פתיחה
                          </label>
                      </div>
-->
                      <div class="action-buttons">
                          <a href="#" onclick="return false;" class="btn-next pull-opposite btn btn-ttc-orange btn-lg to-social-page validate-required" data-required-fields="#email">
                              <span class="glyphicon glyphicon-ok"></span>
                              הבא
                          </a>
                          <a href="#" onclick="return false;" class="btn btn-ttc-clear btn-back">
                              <span class="glyphicons undo"></span>
                              הקודם
                          </a>
                      </div>
                  </div>
              </div>
              <!--</form>-->
          </div>
      </div>

      <!-- Stage ================================================ -->
      <div id="stage-apps" class="row hidden installer-stage">
          <div class="bg-image bg-image-7 hidden-xs" style="
            background-image: url(images/bg7.jpg);
          "></div>
          <div class="content-panel">
              <div class="">
                  <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
                  <h1 class="title">
                      מה עוד תרצו להוסיף לאתר?
                  </h1>
                  <h2>
                  </h2>
                  <div class="row">

                      <div class="col-xs-12">
                          <button id="option-online-store" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Store">
                              <div class="text-type-1">
                                  למכור מוצרים אונליין
                              </div>
                              <small>
                                  קבל הזמנות בחנות אלקטרונית
                              </small>
                              <span class="glyphicons shop"></span>
                          </button>
                      </div>

                      <div class="col-xs-12">
                          <button id="option-booking" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Booking">
                              <div class="text-type-1">
                                  לקבל הזמנות לתורים
                              </div>
                              <small>
                                  אפשר להזמין פגישות או תורים
                              </small>
                              <span class="glyphicons calendar"></span>
                          </button>
                      </div>

<!--
                      <div class="col-xs-12">
                          <button id="option-portfolio" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="I don’t need these features">
                              <div class="text-type-1">
                                  להציג תיק עבודות
                              </div>
                              <small>
                                  הצג פרויקטים נבחרים
                              </small>
                              <span class="glyphicons picture"></span>
                          </button>
                      </div>

                      <div class="col-xs-12">
                          <button id="options-blog" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="I don’t need these features">
                              <div class="text-type-1">
                                  להשיג עוד קוראים לבלוג
                              </div>
                              <small>
                                  פרסם פוסטים ומאמרים בקלות
                              </small>
                              <span class="glyphicons book_open"></span>
                          </button>
                      </div>
                      -->
                      <input id="option-online-store"   type="hidden" name="modules[store][status]"        value="0"/>
                      <input id="option-booking"        type="hidden" name="modules[booking][status]"      value="0"/>
                      <input id="option-portfolio"      type="hidden" name="modules[portfolio][status]"    value="0"/>
                      <input id="options-blog"          type="hidden" name="modules[blog][status]"         value="0"/>
                  </div>

                  <hr>

                  <div class="action-buttons">
                      <a href="#" onclick="return false;" class="btn btn-ttc-orange js-stage3-next btn-next pull-opposite next-btn" data-required-fields="">
                          הבא
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a href="#" onclick="return false;" class="btn btn-ttc-clear btn-back">
                          <span class="glyphicons undo"></span>
                          הקודם
                      </a>
                  </div>
              </div>
          </div>
      </div>


      <!-- Stage ================================================ -->
      <div id="choose-template" class="row hidden installer-stage">
          <div class="content-panel">
              <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
              <div class="hidden-xs">
                  <h1 class="title">
                      עיצוב האתר
                  </h1>
                  <h2>
                      בחרו תבנית שאתם אוהבים – תמיד תוכלו לשנות בהמשך.
                  </h2>
              </div>
              <div class="visible-xs">
                  <h1 class="title">
                      בחרו תבנית שאתם אוהבים
                  </h1>
                  <p>
                      תמיד תוכלו לשנות בהמשך
                  </p>
              </div>
              <div class="row">

                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="curly-beige">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/curly-beige.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>


                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="dream-salon">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-salon.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="dream-fitness">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-fitness.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>



                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="blonde-rays">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/blonde-rays.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>
              </div>




              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="igloo">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/igloo.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="fluffy-strokes">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/fluffy-strokes.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>

              </div>



              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="el-greco">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/el-greco.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>


                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="cousteau">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/cousteau.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                                  <span class="glyphicons ok_2"></span>
                                  בחר
                              </button>
                          </div>
                      </div>
                  </div>
              </div>




              <input type="hidden" name="skin" value="" />




              <div class="action-buttons">
                  <a href="#" onclick="return false;" class="btn btn-ttc-clear btn-back">
                      <span class="glyphicons undo"></span>
                      הקודם
                  </a>
              </div>
          </div>
      </div>

      <!-- Stage  ========================================================== -->
      <div id="stage-auto-update" class="row hidden installer-stage">
          <div class="bg-image bg-image-social-new hidden-xs" style="
                background-image: url(https://otonomic-static.s3.amazonaws.com/images/installer/installer-social-new.jpg);
              "></div>

          <div class="content-panel">
              <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
              <h1 class="title">
                  האתר מתעדכן באופן אוטומטי
              </h1>
              <h2>
                  כל פוסט שאתם מעלים בפייסבוק או בכל רשת אחרת שתבחרו יעלה בצורה אוטומטית לאתר שלכם
              </h2>
              <p class="social_searching_msg">

              </p>

              <p>
                  חפש את העסק שלך ברשתות הבאות:
              </p>
              <!--
              <div class="row">
                  <div class="col-xs-12">
                      <button class="btn social-btn facebook-btn selected" data-analytics-action="Social" data-analytics-label="Facebook"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/facebook-icon.svg"></button>
                      <button class="btn social-btn twitter-btn" data-analytics-action="Social" data-analytics-label="Twitter"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/twitter-icon.svg"></button>
                      <button class="btn social-btn instagram-btn" data-analytics-action="Social" data-analytics-label="Instagram"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/instagram-icon.svg"></button>
                      <button class="btn social-btn googleplus-btn" data-analytics-action="Social" data-analytics-label="Google plus"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/googleplus-icon.svg"></button>
                  </div>
              </div>
              -->

              <div class="row">
                  <div class="col-xs-12">

                      <!-- START Instagram -->
                      <div class="form-group social-media-field" id="instagram">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_instagram"><i class="fa fa-instagram"></i>
                                      אינסטגרם
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameInstagram.php" id="social_media_instagram" name="social[instagram]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-instagram"></div>
                              </div>
                          </div>
                      </div>
                      <!-- END Instagram -->

                      <!-- START YouTube -->
                      <div class="form-group social-media-field" id="youtube">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_youtube"><i class="fa fa-youtube"></i>
                                      יוטיוב
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameYoutube.php" id="social_media_youtube" name="social[youtube]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-youtube"></div>
                              </div>
                          </div>
                      </div>
                      <!-- END YouTube -->


                      <!-- START Twitter -->
                      <div class="form-group social-media-field" id="twitter">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_twitter"><i class="fa fa-twitter"></i>
                                      טוויטר
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameTwitter.php" id="social_media_twitter" name="social[twitter]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-twitter"></div>
                              </div>
                          </div>
                      </div>
                      <!-- END Twitter -->

                          <!-- START LinkedIn -->
<!--                      <div class="form-group social-media-field" id="linkedin">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_linkedin"><i class="fa fa-linkedin"></i>
                                      לינקדאין
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameLinkedin.php" id="social_media_linkedin" name="social[linkedin]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-linkedin"></div>
                              </div>
                          </div>
                      </div>
-->
                      <!-- END LinkedIn -->

                      <!-- START Flickr -->
<!--
                      <div class="form-group social-media-field" id="flickr">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_flickr"><i class="fa fa-flickr"></i>
                                      פליקר
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameFlickr.php" id="social_media_flickr" name="social[flickr]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-flickr"></div>
                              </div>
                          </div>
                      </div>
-->
                      <!-- END Flickr -->


                      <!-- START Google+ -->
<!--
                      <div class="form-group social-media-field" id="googleplus">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_googleplus"><i class="fa fa-google-plus"></i>
                                      גוגל+
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameGoogleplus.php" id="social_media_googleplus" name="social[googleplus]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-google-plus"></div>
                              </div>
                          </div>
                      </div>
-->
                      <!-- END Google+ -->

                      <!-- START Pinterest -->
<!--
                      <div class="form-group social-media-field" id="pinterest">
                          <div class="row">
                              <div class="col-xs-3">
                                  <label for="social_media_pinterest"><i class="fa fa-pinterest"></i>
                                      פינטרסט
                                  </label>
                              </div>
                              <div class="col-xs-9 has-feedback">
                                  <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernamePinterest.php" id="social_media_pinterest" name="social[pinterest]" value="">
                                  <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="search-results-container" id="search-results-pinterest"></div>
                              </div>
                          </div>
                      </div>
-->
                      <!-- END Pinterest -->

                  </div>

                  <div class="action-buttons col-xs-12">
                      <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-opposite btn-next">
                          הבא
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a href="#" onclick="return false;" class="btn btn-ttc-clear btn-back">
                          <span class="glyphicons undo"></span>
                          הקודם
                      </a>
                  </div>

              </div>
          </div>
      </div>




      <!-- Stage ================================================ -->
      <div id="stage-images" class="row hidden installer-stage">
          <div class="bg-image bg-image-8 hidden-xs" style="
            background-image: url(images/bg8.jpg);
          "></div>
          <div class="content-panel">
              <h1 class="title">
הוסיפו קצת מגע אישי!
              </h1>
              <h3>
השתמשו במצלמה כדי להוסיף תמונות שלכם ושל העסק שלכם. התמונות תופענה באתר.
                  תוכלו גם להעלות קבצים השמורים מקומית או לבחור תמונות מאינסטגרם, פליקר ומקורות אחרים.
              </h3>
              <div class="row">
                  <div class="col-xs-12">

                      <div class="form-group clearfix">
                          <div class="text-center">
                              <a href="#" class="btn btn-success wizard-image-gallery" id="select-gallery-image">
                                  <i class="glyphicons plus"></i>
    הוסיפו תמונות
                              </a>
                          </div>

                          <div class="gallery-selected-images row"></div>
                          <div class="clone hidden">
                              <div class="selected-image col-xs-3">
                                  <img src="" class="img-responsive" />
                                  <input type="hidden" name="gallery_image[]" />
                                  <a href="javascript:void(0)" class="remove-selected-image">x</a>
                              </div>
                          </div>
                      </div>

                      <div class="form-group action-buttons">
                          <a href="#" onclick="return false;" class="pull-opposite btn btn-ttc-orange btn-lg js-switch-to-congratz " data-required-fields=""><span class="glyphicon glyphicon-ok"></span>
הבא
                          </a>
                          <a href="#" onclick="return false;" class="btn btn-ttc-clear btn-back">
                              <span class="glyphicons undo"></span>
הקודם
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>






      <!-- Congratz ========================================================== -->
      <div id="congratz" class="row hidden text-center">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
        <div class="upper-content">
          <h1 class="congratz-title">
              <span class="site-name" id="ot-fb-name">
              האתר שלכם
              </span>
              יהיה מוכן בעוד
              <span id="counter">
                  10 שניות
              </span>
          </h1>
          <div class="fb-like" data-href="https://www.facebook.com/otonomic" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
          <img class="oto-anima" src="https://otonomic-static.s3.amazonaws.com/images/installer/ottoHoverLoop.gif">
        </div>
        <div class="lower-content">
          <h3 id="oto-web-url" class="hidden">http://wp.otonomic.com/newsite</h3>
          <p class="tos">
              בהמשך הגלישה באתר אתה מאשר את
               <a target="_blank" href="http://otonomic.com/terms/" id="link-tos">
                   תנאי השימוש
               </a>
          </p>
        </div>
      </div>
        </form>
    </div><!-- /.container -->

    <script src="js/main.js?v=1.0.2"></script>





    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: "373931652687761",
                status: true,
                cookie: true,
                xfbml: true
            });

            window.fbAsyncInit.fbLoaded.resolve();
            checkConnectedWithFacebook();
        };
        window.fbAsyncInit.fbLoaded = jQuery.Deferred();
    </script>

    <script type="text/javascript">
      var base_url = 'http://otonomic.com/hybridauth/twitter.php';

      jQuery(document).ready(function($) {
          trackFacebookPixel('viewed_installer');
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6021618382030', {'value':'0.00','currency':'USD'}]);

          // Online store / booking buttons
          $('.btn-add-on').click(function(){
              var $this = $(this);
              
              if($this.hasClass('btn-uncheck-others') && !$this.hasClass('checked')) {
                $('.btn-add-on').removeClass('checked');
              } else {
                $('.btn-uncheck-others').removeClass('checked');
              }
              $this.toggleClass('checked');
              // $this.parents('.installer-stage').find('.next-btn').removeClass('disabled').html('Continue <span class="glyphicon glyphicon-chevron-left"></span>');
              
              if($this.hasClass('checked')){
                  var option_id = $this.attr('id');
                  $('input#'+option_id).val('1');
              }else{
                  var option_id = $this.attr('id');
                  $('input#'+option_id).val('0');
              }
        });

          $('#show_opening_hours').click(function() {
              $('#opening-hours').toggle();
          });


          var path_socialmedia_library = "/shared/lib/socialmedia/";
          jQuery('.enable-suggest').each(function(index){
              var wrapper = jQuery(this).parent().parent().parent();
              //jQuery(wrapper).append('<div class="search-results-container" />');
              jQuery(this).on('keyup', function() {
                  var $this = jQuery(this);
                  var searchval = $this.val();
                  //wrapper = jQuery(this).parent();
                  if(searchval.length > 2) {

                      jQuery('.search-results-container', wrapper).html('מחפש...').show();
                      jQuery.get(path_socialmedia_library + jQuery(this).attr('data-suggest-url') +"?format=html&search_box="+searchval, function(data) {
                          jQuery('.search-results-container', wrapper).html(data);
                      });
                  } else {
                      jQuery('.search-results-container', wrapper).html('').hide();
                  }
              });
          });
          jQuery('.search-results-container').on('click', '.media.selectable', function() {
              var value = jQuery(this).attr('data-value');
              var wrapper = jQuery(this).parent().parent().parent().parent();
              jQuery('input', wrapper).val(value);
              jQuery('.search-results-container', wrapper).hide();
          });

          jQuery('input, textarea').on('focus', function() {
              jQuery('html, body').animate({
                  scrollTop: jQuery(this).offset().top - 30
              }, 1000);
          });
      })
    </script>
      <!-- Google Code for Sites Created Conversion Page -->
      <script type="text/javascript">
          /* <![CDATA[ */
          var google_conversion_id = 959969618;
          var google_conversion_language = "en";
          var google_conversion_format = "3";
          var google_conversion_color = "ffffff";
          var google_conversion_label = "yMKFCM2tmGEQ0vLfyQM";
          var google_remarketing_only = false;
          /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
          <div style="display:inline;">
              <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/959969618/?label=yMKFCM2tmGEQ0vLfyQM&amp;guid=ON&amp;script=0"/>
          </div>
      </noscript>
  </body>
</html>
