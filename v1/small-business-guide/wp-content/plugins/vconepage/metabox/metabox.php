<?php
function vconepage_metabox() {
	add_meta_box( 'vconepage_meta', __( 'VC One Page Options', 'vconepage' ), 'vconepage_meta_callback', 'page' );
}
add_action( 'add_meta_boxes', 'vconepage_metabox' );

/**
 * Outputs the content of the meta box
 */
function vconepage_meta_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'vconepage_nonce' );
	$vconepage_stored_meta = get_post_meta( $post->ID );
	?>

	<p>
		<label for="meta-favicon" class="vconepage-row-title"><?php _e( 'Favicon', 'vconepage' )?></label>
		<input type="text" name="meta-favicon" id="meta-image" value="<?php if ( isset ( $vconepage_stored_meta['meta-favicon'] ) ) echo $vconepage_stored_meta['meta-favicon'][0]; ?>" />
		<input type="button" id="meta-image-button" class="button" value="<?php _e( 'Choose or Upload an Image', 'vconepage' )?>" />
	</p>

	<h2 class="vconepage-title"><?php _e('SEO','vconepage'); ?></h2>

	<p>
		<label for="meta-title" class="vconepage-row-title"><?php _e( 'Title (Leave empty if you want get wordpress page title)', 'vconepage' )?></label>
		<input type="text" name="meta-title" id="meta-title" value="<?php if ( isset ( $vconepage_stored_meta['meta-title'] ) ) echo $vconepage_stored_meta['meta-title'][0]; ?>" />
	</p>

	<p>
		<label for="meta-keyword" class="vconepage-row-title"><?php _e( 'Keywords', 'vconepage' )?></label>
		<input type="text" name="meta-keyword" id="meta-keyword" value="<?php if ( isset ( $vconepage_stored_meta['meta-keyword'] ) ) echo $vconepage_stored_meta['meta-keyword'][0]; ?>" />
	</p>

    <p>
		<label for="meta-description" class="vconepage-row-title"><?php _e( 'Description', 'vconepage' )?></label>
		<textarea name="meta-description" id="meta-description"><?php if ( isset ( $vconepage_stored_meta['meta-description'] ) ) echo $vconepage_stored_meta['meta-description'][0]; ?></textarea>
	</p>
    
    <h2 class="vconepage-title"><?php _e('CUSTOM CODE','vconepage'); ?></h2>

    <p>
		<label for="meta-custom-css" class="vconepage-row-title"><?php _e( 'Custom CSS', 'vconepage' )?></label>
		<textarea name="meta-custom-css" id="meta-custom-css"><?php if ( isset ( $vconepage_stored_meta['meta-custom-css'] ) ) echo $vconepage_stored_meta['meta-custom-css'][0]; ?></textarea>
	</p>
	
    <p>
		<label for="meta-custom-js" class="vconepage-row-title"><?php _e( 'Custom Js (Analitycs)', 'vconepage' )?></label>
		<textarea name="meta-custom-js" id="meta-custom-js"><?php if ( isset ( $vconepage_stored_meta['meta-custom-js'] ) ) echo $vconepage_stored_meta['meta-custom-js'][0]; ?></textarea>
	</p>
    
	<?php
}



/**
 * Saves the custom meta input
 */
function vconepage_meta_save( $post_id ) {
 
	// Checks save status
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'vconepage_nonce' ] ) && wp_verify_nonce( $_POST[ 'vconepage_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
	// Exits script depending on save status
	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}
 
	// SEO
	if( isset( $_POST[ 'meta-title' ] ) ) {
		update_post_meta( $post_id, 'meta-title', sanitize_text_field( $_POST[ 'meta-title' ] ) );
	}	
	if( isset( $_POST[ 'meta-keyword' ] ) ) {
		update_post_meta( $post_id, 'meta-keyword', sanitize_text_field( $_POST[ 'meta-keyword' ] ) );
	}
	if( isset( $_POST[ 'meta-description' ] ) ) {
		update_post_meta( $post_id, 'meta-description', sanitize_text_field( $_POST[ 'meta-description' ] ) );
	}
	
	// Custom Code
	if( isset( $_POST[ 'meta-custom-css' ] ) ) {
		update_post_meta( $post_id, 'meta-custom-css', sanitize_text_field( $_POST[ 'meta-custom-css' ] ) );
	}	
	if( isset( $_POST[ 'meta-custom-js' ] ) ) {
		update_post_meta( $post_id, 'meta-custom-js', sanitize_text_field( $_POST[ 'meta-custom-js' ] ) );
	}

	// Favicon
	if( isset( $_POST[ 'meta-favicon' ] ) ) {
		update_post_meta( $post_id, 'meta-favicon', $_POST[ 'meta-favicon' ] );
	}

}
add_action( 'save_post', 'vconepage_meta_save' );


/**
 * Adds the meta box stylesheet when appropriate
 */
function vconepage_admin_style(){
	global $typenow;
	if( $typenow == 'page' ) {
		wp_enqueue_style( 'vconepage_metabox_style', plugin_dir_url( __FILE__ ) . 'metabox-style.css' );
	}
}
add_action( 'admin_enqueue_scripts', 'vconepage_admin_style' );

/**
 * Loads the image management javascript
 */
function vconepage_image_enqueue() {
	global $typenow;
	if( $typenow == 'page' ) {
		wp_enqueue_media();
		wp_register_script( 'vconepage-metabox-image', plugin_dir_url( __FILE__ ) . 'metabox-image.js', array( 'jquery' ) );
		wp_localize_script( 'vconepage-metabox-image', 'meta_image',
			array(
				'title' => __( 'Choose or Upload an Image', 'vconepage' ),
				'button' => __( 'Use this image', 'vconepage' ),
			)
		);
		wp_enqueue_script( 'vconepage-metabox-image' );
	}
}
add_action( 'admin_enqueue_scripts', 'vconepage_image_enqueue' );
