<?php
/*
File: functions.php
Description: Plugin Functions
Author: Ad-theme.com
Author URI: http://www.ad-theme.com
Compatibility: WP 3.9.x - WP 4.0.x - WP 4.1.x
*/


add_action('admin_menu', 'vconepage_menus');

function vconepage_menus() {
	
	add_menu_page('VC ONE PAGE', 'VC ONE PAGE', 'manage_options', 'vconepage-main-page', 'vconepage_main_page', plugins_url( 'vconepage/assets/img/main_icon.png' ));
		
}

function vconepage_main_page() {
	
	
	if (!current_user_can('manage_options'))  {

		wp_die( __('You do not have sufficient permissions to access this page.', 'vconepage') );

	}
	
	echo '<div class="vcop_row">
			<h2 class="adsidebarpanel-title">'.__('VISUAL COMPOSER ONE PAGE SETTINGS','vconepage').'</h2>
		  </div>';
	
?>


<?php
	
	
	echo '<style type="text/css">
			  .vcop_row {
					width:100%;
					padding:25px 25px 0 25px;
					box-sizing:border-box;  
			  }
			  .vcop_addon, .vcop_capability {
					width:50%;
					float:left;  
			  }
			  .vc_addon_title {
				display: inline-block;
				width: 95px;
			  }
		  </style>';
	
	if (!empty($_POST['update'])) {
	
	// Check Posted Options	
	if(empty($_POST['vcop_team_custom_type_active'])) {$vcop_team_custom_type_active = '';} else { $vcop_team_custom_type_active = $_POST['vcop_team_custom_type_active']; }
	if(empty($_POST['vcop_map_active'])) {$vcop_map_active = '';} else { $vcop_map_active = $_POST['vcop_map_active']; }
	if(empty($_POST['vcop_chart_active'])) {$vcop_chart_active = '';} else { $vcop_chart_active = $_POST['vcop_chart_active']; }
	if(empty($_POST['vcop_icon_active'])) {$vcop_icon_active = '';} else { $vcop_icon_active = $_POST['vcop_icon_active']; }
	if(empty($_POST['vcop_boxmessage_active'])) {$vcop_boxmessage_active = '';} else { $vcop_boxmessage_active = $_POST['vcop_boxmessage_active']; }
	if(empty($_POST['vcop_countdown_active'])) {$vcop_countdown_active = '';} else { $vcop_countdown_active = $_POST['vcop_countdown_active']; }
	if(empty($_POST['vcop_social_active'])) {$vcop_social_active = '';} else { $vcop_social_active = $_POST['vcop_social_active']; }
	if(empty($_POST['vcop_heading_active'])) {$vcop_heading_active = '';} else { $vcop_heading_active = $_POST['vcop_heading_active']; }
	if(empty($_POST['vcop_buttons_active'])) {$vcop_buttons_active = '';} else { $vcop_buttons_active = $_POST['vcop_buttons_active']; }
	if(empty($_POST['vcop_counter_active'])) {$vcop_counter_active = '';} else { $vcop_counter_active = $_POST['vcop_counter_active']; }
	if(empty($_POST['vcop_portfolio_active'])) {$vcop_portfolio_active = '';} else { $vcop_portfolio_active = $_POST['vcop_portfolio_active']; }
	if(empty($_POST['vcop_grid_active'])) {$vcop_grid_active = '';} else { $vcop_grid_active = $_POST['vcop_grid_active']; }
	if(empty($_POST['vcop_gallery_active'])) {$vcop_gallery_active = '';} else { $vcop_gallery_active = $_POST['vcop_gallery_active']; }
	if(empty($_POST['vcop_carousel_active'])) {$vcop_carousel_active = '';} else { $vcop_carousel_active = $_POST['vcop_carousel_active']; }
	if(empty($_POST['vcop_list_active'])) {$vcop_list_active = '';} else { $vcop_list_active = $_POST['vcop_list_active']; }
	if(empty($_POST['vcop_timeline_active'])) {$vcop_timeline_active = '';} else { $vcop_timeline_active = $_POST['vcop_timeline_active']; }
	if(empty($_POST['vcop_postlist_active'])) {$postlist_active = '';} else { $vcop_postlist_active = $_POST['vcop_postlist_active']; }
	if(empty($_POST['vcop_space_active'])) {$space_active = '';} else { $vcop_space_active = $_POST['vcop_space_active']; }
	if(empty($_POST['vcop_singleimage_active'])) {$singleimage_active = '';} else { $vcop_singleimage_active = $_POST['vcop_singleimage_active']; }
	if(empty($_POST['vcop_contactform_active'])) {$contactform_active = '';} else { $vcop_contactform_active = $_POST['vcop_contactform_active']; }
	if(empty($_POST['vcop_woocommerce_active'])) {$woocommerce_active = '';} else { $vcop_woocommerce_active = $_POST['vcop_woocommerce_active']; }
	if(empty($_POST['vcop_templates_active'])) {$vcop_templates_active = '';} else { $vcop_templates_active = $_POST['vcop_templates_active']; }
	if(empty($_POST['vconepage_number_menu'])) {$vconepage_number_menu = '';} else { $vconepage_number_menu = $_POST['vconepage_number_menu']; }

	// Update Options
	update_option('vcop_team_custom_type_active', $vcop_team_custom_type_active );	
	update_option('vcop_map_active', $vcop_map_active );
	update_option('vcop_chart_active', $vcop_chart_active );
	update_option('vcop_icon_active', $vcop_icon_active );
	update_option('vcop_boxmessage_active', $vcop_boxmessage_active );
	update_option('vcop_countdown_active', $vcop_countdown_active );
	update_option('vcop_social_active', $vcop_social_active );
	update_option('vcop_heading_active', $vcop_heading_active );	
	update_option('vcop_buttons_active', $vcop_buttons_active );
	update_option('vcop_counter_active', $vcop_counter_active );
	update_option('vcop_portfolio_active', $vcop_portfolio_active );
	update_option('vcop_grid_active', $vcop_grid_active );
	update_option('vcop_gallery_active', $vcop_gallery_active );
	update_option('vcop_carousel_active', $vcop_carousel_active );
	update_option('vcop_list_active', $vcop_list_active );
	update_option('vcop_timeline_active', $vcop_timeline_active );
	update_option('vcop_postlist_active', $vcop_postlist_active );
	update_option('vcop_space_active', $vcop_space_active );		
	update_option('vcop_singleimage_active', $vcop_singleimage_active );
	update_option('vcop_contactform_active', $vcop_contactform_active );
	update_option('vcop_woocommerce_active', $vcop_woocommerce_active );
	update_option('vcop_templates_active', $vcop_templates_active );	
	update_option('vconepage_number_menu', $vconepage_number_menu );
		
	}
	// Check Saved Options
	
	$vcop_map_active = get_option( 'vcop_map_active', '' );
	update_option('vcop_map_active', $vcop_map_active );	

	$vcop_chart_active = get_option( 'vcop_chart_active', '' );
	update_option('vcop_chart_active', $vcop_chart_active );	

	$vcop_icon_active = get_option( 'vcop_icon_active', '' );
	update_option('vcop_icon_active', $vcop_icon_active );	

	$vcop_boxmessage_active = get_option( 'vcop_boxmessage_active', '' );
	update_option('vcop_boxmessage_active', $vcop_boxmessage_active );

	$vcop_countdown_active = get_option( 'vcop_countdown_active', '' );
	update_option('vcop_countdown_active', $vcop_countdown_active );

	$vcop_social_active = get_option( 'vcop_social_active', '' );
	update_option('vcop_social_active', $vcop_social_active );

	$vcop_heading_active = get_option( 'vcop_heading_active', '' );
	update_option('vcop_heading_active', $vcop_heading_active );

	$vcop_buttons_active = get_option( 'vcop_buttons_active', '' );
	update_option('buttons_active', $vcop_buttons_active );

	$vcop_counter_active = get_option( 'vcop_counter_active', '' );
	update_option('vcop_counter_active', $vcop_counter_active );

	$vcop_portfolio_active = get_option( 'vcop_portfolio_active', '' );
	update_option('vcop_portfolio_active', $vcop_portfolio_active );

	$vcop_grid_active = get_option( 'vcop_grid_active', '' );
	update_option('vcop_grid_active', $vcop_grid_active );

	$vcop_gallery_active = get_option( 'vcop_gallery_active', '' );
	update_option('vcop_gallery_active', $vcop_gallery_active );

	$vcop_carousel_active = get_option( 'vcop_carousel_active', '' );
	update_option('vcop_carousel_active', $vcop_carousel_active );

	$vcop_list_active = get_option( 'vcop_list_active', '' );
	update_option('vcop_list_active', $vcop_list_active );

	$vcop_timeline_active = get_option( 'vcop_timeline_active', '' );
	update_option('vcop_timeline_active', $vcop_timeline_active );

	$vcop_postlist_active = get_option( 'vcop_postlist_active', '' );
	update_option('vcop_postlist_active', $vcop_postlist_active );

	$vcop_space_active = get_option( 'vcop_space_active', '' );
	update_option('vcop_space_active', $vcop_space_active );

	$vcop_singleimage_active = get_option( 'vcop_singleimage_active', '' );
	update_option('vcop_singleimage_active', $vcop_singleimage_active );
																		
	$vcop_team_custom_type_active = get_option( 'vcop_team_custom_type_active', '' );
	update_option('vcop_team_custom_type_active', $vcop_team_custom_type_active );	

	$vcop_contactform_active = get_option( 'vcop_contactform_active', '' );
	update_option('vcop_contactform_active', $vcop_contactform_active );

	$vcop_woocommerce_active = get_option( 'vcop_woocommerce_active', '' );
	update_option('vcop_woocommerce_active', $vcop_woocommerce_active );

	$vcop_templates_active = get_option( 'vcop_templates_active', '' );
	update_option('vcop_templates_active', $vcop_templates_active );

	$vconepage_number_menu = get_option( 'vconepage_number_menu', '' );
	update_option('vconepage_number_menu', $vconepage_number_menu );
		
	// Load option fields
	echo '<form method="POST" action="'.$_SERVER['REQUEST_URI'].'">';
	?>
    
    <div class="vcop_row">
    
        <div class="vcop_addon">  
            <span class="vc_addon_title"><?php _e('Team: ', 'vconepage'); ?></span>
            <select id="vcop_team_custom_type_active" name="vcop_team_custom_type_active">
                <option value="active" <?php if ($vcop_team_custom_type_active == 'active') { echo 'selected'; } ?>>Active
                <option value="deactive" <?php if ($vcop_team_custom_type_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select>
        </div>
   
        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Map: ', 'vconepage'); ?></span>
            <select id="vcop_map_active" name="vcop_map_active">
            <option value="active" <?php if ($vcop_map_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_map_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>       

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Chart: ', 'vconepage'); ?></span>
            <select id="vcop_chart_active" name="vcop_chart_active">
            <option value="active" <?php if ($vcop_chart_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_chart_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>   

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Icon: ', 'vconepage'); ?></span>
            <select id="vcop_icon_active" name="vcop_icon_active">
            <option value="active" <?php if ($vcop_icon_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_icon_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div> 

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Box message: ', 'vconepage'); ?></span>
            <select id="vcop_boxmessage_active" name="vcop_boxmessage_active">
            <option value="active" <?php if ($vcop_boxmessage_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_boxmessage_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Countdown: ', 'vconepage'); ?></span>
            <select id="vcop_countdown_active" name="vcop_countdown_active">
            <option value="active" <?php if ($vcop_countdown_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_countdown_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Social: ', 'vconepage'); ?></span>
            <select id="vcop_social_active" name="vcop_social_active">
            <option value="active" <?php if ($vcop_social_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_social_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Heading: ', 'vconepage'); ?></span>
            <select id="vcop_heading_active" name="vcop_heading_active">
            <option value="active" <?php if ($vcop_heading_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_heading_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Buttons: ', 'vconepage'); ?></span>
            <select id="vcop_buttons_active" name="vcop_buttons_active">
            <option value="active" <?php if ($vcop_buttons_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_buttons_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Counter: ', 'vconepage'); ?></span>
            <select id="vcop_counter_active" name="vcop_counter_active">
            <option value="active" <?php if ($vcop_counter_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_counter_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Portfolio: ', 'vconepage'); ?></span>
            <select id="vcop_portfolio_active" name="vcop_portfolio_active">
            <option value="active" <?php if ($vcop_portfolio_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_portfolio_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Grid: ', 'vconepage'); ?></span>
            <select id="vcop_grid_active" name="vcop_grid_active">
            <option value="active" <?php if ($vcop_grid_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_grid_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Gallery: ', 'vconepage'); ?></span>
            <select id="vcop_gallery_active" name="vcop_gallery_active">
            <option value="active" <?php if ($vcop_gallery_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_gallery_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Carousel: ', 'vconepage'); ?></span>
            <select id="vcop_carousel_active" name="vcop_carousel_active">
            <option value="active" <?php if ($vcop_carousel_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_carousel_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('List: ', 'vconepage'); ?></span>
            <select id="vcop_list_active" name="vcop_list_active">
            <option value="active" <?php if ($vcop_list_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_list_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Timeline: ', 'vconepage'); ?></span>
            <select id="vcop_timeline_active" name="vcop_timeline_active">
            <option value="active" <?php if ($vcop_timeline_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_timeline_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Postlist: ', 'vconepage'); ?></span>
            <select id="vcop_postlist_active" name="vcop_postlist_active">
            <option value="active" <?php if ($vcop_postlist_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_postlist_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Space: ', 'vconepage'); ?></span>
            <select id="vcop_space_active" name="vcop_space_active">
            <option value="active" <?php if ($vcop_space_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_space_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>
        
        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Single Image: ', 'vconepage'); ?></span>
            <select id="vcop_singleimage_active" name="vcop_singleimage_active">
            <option value="active" <?php if ($vcop_singleimage_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_singleimage_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>        

		<?php if ( !defined( 'WPCF7_VERSION' ) ) {
				$vcop_contact_form_display = 'style="display:none;"';
		} else {
			$vcop_contact_form_display = '';
		}
		?>

        <div class="vcop_addon" <?php echo $vcop_contact_form_display; ?>>
			<span class="vc_addon_title"><?php _e('Contact Form 7: ', 'vconepage'); ?></span>
            <select id="vcop_contactform_active" name="vcop_contactform_active">
            <option value="active" <?php if ($vcop_contactform_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_contactform_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div> 

		<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
				$vcop_woocommerce_form_display = '';
		} else {
			$vcop_woocommerce_form_display = 'style="display:none;"';
		}
		?>

        <div class="vcop_addon" <?php echo $vcop_woocommerce_form_display; ?>>
			<span class="vc_addon_title"><?php _e('Woocommerce: ', 'vconepage'); ?></span>
            <select id="vcop_woocommerce_active" name="vcop_woocommerce_active">
            <option value="active" <?php if ($vcop_woocommerce_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_woocommerce_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div> 

        <div class="vcop_addon">
			<span class="vc_addon_title"><?php _e('Templates: ', 'vconepage'); ?></span>
            <select id="vcop_templates_active" name="vcop_templates_active">
            <option value="active" <?php if ($vcop_templates_active == 'active') { echo 'selected'; } ?>>Active
            <option value="deactive" <?php if ($vcop_templates_active == 'deactive') { echo 'selected'; } ?>>Deactive
            </select> 
        </div>
        
		<div class="clear"></div>
        
        
        <h2><?php _e('Number Menu','adonepage'); ?></h2>
        <p><?php _e('If you want more menu position for different menu in each vc one page you need to add a number of menu you want show','adonepage'); ?></p>
     	<?php _e('Enter Number Menu (ex: 5): ', 'vconepage'); ?><input type="text" class="vconepage_number_menu" name="vconepage_number_menu" value="<?php echo $vconepage_number_menu; ?>">
       
        
        
    </div>
	<?php
	echo '<div class="vcop_row"><p><input type="hidden" name="update" value="update">
	<input type="submit" value="'.__('Save Changes', 'vconepage').'" class="button-primary" id="submit" name="submit">
	</p>';
	echo '</form>';
	echo '</div>';
}

// POST TYPE

$vcop_team_active = get_option( 'vcop_team_custom_type_active', '' );
	
if(empty($vcop_team_active)) { $vcop_team_active = 'active'; }
if($vcop_team_active == 'active') {
	
	// Post Type Registration
	add_action( 'init', 'vconepage_register_posttype' );
	
	function vconepage_register_posttype() {
			
			$labels = array(
				'name'               => __('VC Team', 'vconepage'),
				'singular_name'      => __('VC Team', 'vconepage'),
				'add_new'            => __('Add New User Team', 'vconepage'),
				'add_new_item'       => __('Add New User Team', 'vconepage'),
				'edit_item'          => __('Edit User Team', 'vconepage'),
				'new_item'           => __('New User Team', 'vconepage'),
				'all_items'          => __('All User Team', 'vconepage'),
				'view_item'          => __('View User Team', 'vconepage'),
				'search_items'       => __('Search User Team', 'vconepage'),
				'not_found'          => __('No User Team found', 'vconepage'),
				'not_found_in_trash' => __('No User Team found in Trash', 'vconepage'),
				'parent_item_colon'  => '',
				'menu_name'          => __('VC Team', 'vconepage')
			  );
			
			  $args = array(
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'vconepage_team' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => null,		
				'menu_icon'			 => plugins_url( 'vconepage/assets/img/main_icon.png' ),
				'supports'           => array( 'title' , 'editor' , 'thumbnail' )
			  );
			
			  register_post_type( 'vconepage_team', $args );

	}

} // #TEAM ACTIVE