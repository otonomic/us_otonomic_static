/* 
File: header.js
Plugin: VC ONE PAGE
Author: AD-Theme.com
*/



jQuery(function($){
	 $(document).ready(function() {
		 
		var menu = new cbpTooltipMenu( document.getElementById( 'cbp-tm-menu' ) );
		/* STICKY MENU TOP */
		var stickyNavTop = $('.vcop-header').offset().top;  
		  
		var stickyNav = function(){  
		var scrollTop = $(window).scrollTop();  
			   
		if (scrollTop > (stickyNavTop)) {   
			$('.vcop-header-top.vcmp-header-sticky-container').addClass('vcop-sticky');
			$('.vcop-header-top.vcmp-header-sticky-container').removeClass('vcop-nosticky');
			$('.vcop-header-top.vcmp-header-sticky-container.vcmp-header-sticky-v2').fadeIn();
		} else {  
			$('.vcop-header-top.vcmp-header-sticky-container').removeClass('vcop-sticky');
			$('.vcop-header-top.vcmp-header-sticky-container').addClass('vcop-nosticky');
			$('.vcop-header-top.vcmp-header-sticky-container.vcmp-header-sticky-v2').fadeOut();   
		}  
		};  
		  
		stickyNav();  
		  
		$(window).scroll(function() {  
			stickyNav();  
		});  

		/* STICKY MENU BOTTOM */
		if($('.vcop-header').hasClass('vcop-header-bottom')) {
			var stickyNavBottom = $('.vcop-header-bottom').offset().top;  
		}
		var stickyNav_bottom = function(){  
		var scrollTop_bottom = $(window).scrollTop();  
			   
		if (scrollTop_bottom > (stickyNavBottom)) {   			 
			$('.vcop-header-bottom.vcmp-header-sticky-container.vcmp-header-sticky').addClass('vcop-nosticky').fadeOut(100);
			$('.vcop-header-bottom.vcop-header-sticky.vcmp-header-sticky').fadeIn(1000);
			$('.vcop-header-bottom.vcmp-header-sticky-container.vcmp-header-sticky-v2').fadeIn();
		} else { 		
			$('.vcop-header-bottom.vcmp-header-sticky-container.vcmp-header-sticky').addClass('vcop-nosticky').fadeIn(1000);
			$('.vcop-header-bottom.vcop-header-sticky.vcmp-header-sticky').fadeOut(100);
			$('.vcop-header-bottom.vcmp-header-sticky-container.vcmp-header-sticky-v2').fadeOut();			   
		}  
		};  
		  
		stickyNav_bottom();  
		  
		$(window).scroll(function() {  
			stickyNav_bottom();  
		});  

		$('.sub-menu').removeClass('sub-menu').addClass('cbp-tm-submenu');
		$('.vc_column_container .vcop-header').parent().parent().addClass('vcop-menu-container');

		/* MENU ANIMATE BUTTON */
		$('.vcop-header .vcop-menu a[href*=#]').click(function(event){
				$('html, body').animate({
						scrollTop: $( $.attr(this, 'href') ).offset().top
				}, 500);
				event.preventDefault();
		});
	
	});

});