<?php
/*
File: metabox_team.php
Description: Register Metabox for Team
Plugin: VC One Page
Author: Ad-theme.com
*/

/* SUBTIBLE USER */

function vconepage_team_subtitle_add_custom_box() {

    $screens = array( 'vconepage_team' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'vconepage_team_subtitle',
            __( 'Subtitle', 'vconepage' ),
            'vconepage_team_subtitle_inner_custom_box',
            $screen, 'normal', 'high'
        );
    }
}
add_action( 'add_meta_boxes', 'vconepage_team_subtitle_add_custom_box' );

function vconepage_team_subtitle_inner_custom_box( $post ) {
    
	wp_nonce_field( 'vconepage_team_subtitle_inner_custom_box', 'vconepage_team_subtitle_inner_custom_box_nonce' );
	$subtitle = get_post_meta( $post->ID, '_subtitle', true );

	echo '<label for="vconepage_team_subtitle">';
		   _e( "Subtitle (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_subtitle" name="vconepage_team_subtitle" value="' . esc_attr( $subtitle ) . '" size="25" />';  
	
	echo '<br>';

}

	
function vconepage_team_subtitle_save_postdata( $post_id ) {
  if ( ! isset( $_POST['vconepage_team_subtitle_inner_custom_box_nonce'] ) )
    return $post_id;
  $nonce = $_POST['vconepage_team_subtitle_inner_custom_box_nonce'];
  if ( ! wp_verify_nonce( $nonce, 'vconepage_team_subtitle_inner_custom_box' ) )
      return $post_id;
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return $post_id;
  if ( 'page' == $_POST['post_type'] ) {

    if ( ! current_user_can( 'edit_page', $post_id ) )
        return $post_id; 
  } else {
    if ( ! current_user_can( 'edit_post', $post_id ) )
        return $post_id;
  }
	$vconepage_team_subtitle 	= sanitize_text_field( $_POST['vconepage_team_subtitle'] );
	update_post_meta( $post_id, '_subtitle', $vconepage_team_subtitle );
}
add_action( 'save_post', 'vconepage_team_subtitle_save_postdata' );

/* SOCIAL USER */

function vconepage_team_add_custom_box() {

    $screens = array( 'vconepage_team' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'vconepage_team_sectionid',
            __( 'Social User', 'vconepage' ),
            'vconepage_team_inner_custom_box',
            $screen, 'normal', 'high'
        );
    }
}
add_action( 'add_meta_boxes', 'vconepage_team_add_custom_box' );


function vconepage_team_inner_custom_box( $post ) {

    wp_nonce_field( 'vconepage_team_inner_custom_box', 'vconepage_team_inner_custom_box_nonce' );
	$facebook = get_post_meta( $post->ID, '_facebook', true );
	$twitter = get_post_meta( $post->ID, '_twitter', true );
	$googleplus = get_post_meta( $post->ID, '_googleplus', true );
	$linkedin = get_post_meta( $post->ID, '_linkedin', true );
	$dribble = get_post_meta( $post->ID, '_dribble', true );			  
    $youtube = get_post_meta( $post->ID, '_youtube', true );
    $email = get_post_meta( $post->ID, '_email', true );
	
	echo '<label for="vconepage_team_facebook">';
		   _e( "Facebook (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_facebook" name="vconepage_team_facebook" value="' . esc_attr( $facebook ) . '" size="25" />';  
	
	echo '<br>';
	
	echo '<label for="vconepage_team_twitter">';
		   _e( "Twitter (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_twitter" name="vconepage_team_twitter" value="' . esc_attr( $twitter ) . '" size="25" />';  
	
	echo '<br>';

	echo '<label for="vconepage_team_googleplus">';
		   _e( "Google Plus (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_googleplus" name="vconepage_team_googleplus" value="' . esc_attr( $googleplus ) . '" size="25" />';  
	
	echo '<br>';

	echo '<label for="vconepage_team_linkedin">';
		   _e( "Linkedin (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_linkedin" name="vconepage_team_linkedin" value="' . esc_attr( $linkedin ) . '" size="25" />';  
	
	echo '<br>';

	echo '<label for="vconepage_team_dribble">';
		   _e( "Dribble (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_dribble" name="vconepage_team_dribble" value="' . esc_attr( $dribble ) . '" size="25" />';  
	
	echo '<br>';

	echo '<label for="vconepage_team_youtube">';
		   _e( "Youtube (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_youtube" name="vconepage_team_youtube" value="' . esc_attr( $youtube ) . '" size="25" />';  
	
	echo '<br>';

	echo '<label for="vconepage_team_email">';
		   _e( "Email (Leave empty if you don't want use it)", 'vconepage' );
	echo '</label> ';
	echo '<input type="text" id="vconepage_team_email" name="vconepage_team_email" value="' . esc_attr( $email ) . '" size="25" />';  
	
	echo '<br>';


}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function vconepage_team_save_postdata( $post_id ) {

  /*
   * We need to verify this came from the our screen and with proper authorization,
   * because save_post can be triggered at other times.
   */

  // Check if our nonce is set.
  if ( ! isset( $_POST['vconepage_team_inner_custom_box_nonce'] ) )
    return $post_id;

  $nonce = $_POST['vconepage_team_inner_custom_box_nonce'];

  // Verify that the nonce is valid.
  if ( ! wp_verify_nonce( $nonce, 'vconepage_team_inner_custom_box' ) )
      return $post_id;

  // If this is an autosave, our form has not been submitted, so we don't want to do anything.
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return $post_id;

  // Check the user's permissions.
  if ( 'page' == $_POST['post_type'] ) {

    if ( ! current_user_can( 'edit_page', $post_id ) )
        return $post_id;
  
  } else {

    if ( ! current_user_can( 'edit_post', $post_id ) )
        return $post_id;
  }

  /* OK, its safe for us to save the data now. */
  
  // Sanitize user input.
  $vconepage_team_facebook 	= sanitize_text_field( $_POST['vconepage_team_facebook'] );
  $vconepage_team_twitter 		= sanitize_text_field( $_POST['vconepage_team_twitter'] );
  $vconepage_team_googleplus 	= sanitize_text_field( $_POST['vconepage_team_googleplus'] );
  $vconepage_team_linkedin  	= sanitize_text_field( $_POST['vconepage_team_linkedin'] );
  $vconepage_team_dribble 		= sanitize_text_field( $_POST['vconepage_team_dribble'] );
  $vconepage_team_youtube 		= sanitize_text_field( $_POST['vconepage_team_youtube'] );
  $vconepage_team_email 		= sanitize_text_field( $_POST['vconepage_team_email'] );  
   
  // Update the meta field in the database.
  update_post_meta( $post_id, '_facebook', $vconepage_team_facebook );  
  update_post_meta( $post_id, '_twitter', $vconepage_team_twitter );
  update_post_meta( $post_id, '_googleplus', $vconepage_team_googleplus );  
  update_post_meta( $post_id, '_linkedin', $vconepage_team_linkedin );  
  update_post_meta( $post_id, '_dribble', $vconepage_team_dribble );  
  update_post_meta( $post_id, '_youtube', $vconepage_team_youtube );  
  update_post_meta( $post_id, '_email', $vconepage_team_email );  
                     
}
add_action( 'save_post', 'vconepage_team_save_postdata' );