<?php
/*
File: functions-general.php
Description: Plugin Functions General
Author: Ad-theme.com
Author URI: http://www.ad-theme.com
Compatibility: WP 3.9.x - WP 4.0.x - WP 4.1.x
*/

// ADD SIZE

function vconepage_add_image_sizes() {

	add_image_size( 'vconepage-masonry', 500 );
	add_image_size( 'vconepage-grid', 800 , 800 , true);
	add_image_size( 'vconepage-rectangle', 800 , 500 , true);
	add_image_size( 'vconepage-fullscreen', 1200 , 800 , true);
	add_image_size( 'vconepage-parallax', 2560 , 1440 , true);

}

add_action( 'init', 'vconepage_add_image_sizes' );

/**************************************************************************/
/************************ FUNCTION POST INFO ******************************/
/**************************************************************************/

function vconepage_thumbs($thumbs_size) {
	global $post;
	if(has_post_thumbnail()){ 
			$id_post = get_the_id();					
			$single_image = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), $thumbs_size );	 					 
		} else {               
             $single_image[0] = AD_VCOP_URL .'assets/img/no-img.png';                 
    }	
	$return =	'<img class="vcop-thumbs" src="'.$single_image[0].'" alt="'.get_the_title().'">';
	return $return;
}

function vconepage_thumbs_link($masonrygrid) {
	global $post;
	if(has_post_thumbnail()){ 
			$id_post = get_the_id();					
			$single_image = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), $masonrygrid );	 					 
		} else {               
             $single_image[0] = AD_fcg_URL .'assets/img/no-img.png';                 
    }	
	$return = $single_image[0];
	return $return;
}

function vconepage_excerpt($excerpt) {
	$return = substr(get_the_excerpt(), 0, $excerpt);
	return $return;
}

function vconepage_post_info(
							 $display_date,
							 $display_comments,
						     $display_author,
						     $display_category,
						     $display_views,
						     $vcop_query_source,
							 $vcop_query_posts_type,
							 $date_format) {	   
		   global $post;		   
		   $return = '';
		   if($display_date == 'true') {
		   
		   		$return .= '<span class="admp-date"><i class="adtheme-icon-calendar"></i>' . get_the_date($date_format) . '</span>'; 
			
		   }
		    
		   if($display_author == 'true') {	   
		   		$return .= '<span class="admp-author"><i class="adtheme-icon-user"></i><a href="'.get_author_posts_url( get_the_author_meta( 'ID' ) ).'">'.get_the_author_meta( 'display_name' ).'</a></span>'; 
		   } 	
		   	   
		   if($display_comments == 'true') {
           	   
           		$return .= '<span class="admp-comments"><i class="adtheme-icon-bubble"></i><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>';
		   
		   }
		    		    
		   if($display_category == 'true') {
		   		$return .= '<span class="admp-category"><i class="adtheme-icon-tag"></i>'.vconepage_category($vcop_query_source,$vcop_query_posts_type).'</span>'; 
		   }
			
		   if($display_views == 'true') {        
		   $return .= '<span class="admp-views"><i class="adtheme-icon-eye"></i>'.vconepage_get_post_views(get_the_ID()).'</span>';
		   }
		   
		   return $return; 		   
}

function vconepage_share() {
	global $post;
	$pinterestimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	
		
	$return = '<div class="vcop-share-container">
        <div class="vcop-share-item"><a target="_blank" class="adtheme-icon-facebook" href="http://www.facebook.com/sharer.php?u='.get_the_permalink().'&amp;t='.get_the_title().'" title="'.__('Click to share this post on Facebook','admegaposts').'"></a></div>
		<div class="vcop-share-item"><a target="_blank" class="adtheme-icon-twitter" href="http://twitter.com/home?status='.get_the_permalink().'" title="'.__('Click to share this post on Twitter','admegaposts').'"></a></div>
        <div class="vcop-share-item"><a target="_blank" class="adtheme-icon-googleplus" href="https://plus.google.com/share?url='.get_the_permalink().'" title="'.__('Click to share this post on Google+','admegaposts').'"></a></div>
        <div class="vcop-share-item"><a target="_blank" class="adtheme-icon-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.get_the_permalink().'" title="'.__('Click to share this post on Linkedin','admegaposts').'"></a></div>
    	<div class="vcop-share-item"><a target="_blank" class="adtheme-icon-pinterest" href="http://pinterest.com/pin/create/button/?url='.urlencode(get_permalink($post->ID)).'&media='.$pinterestimage[0].'&description='.get_the_title().'" title="'.__('Click to share this post on Pinterest','admegaposts').'"></a></div>
    </div>';
	
    return $return;
    
}

function vconepage_category($vcop_query_source,$vcop_query_posts_type) {
		$separator = ', ';
		$output = '';	
	if($vcop_query_source=='wp_posts') {
		$categories = get_the_category();
		if($categories){
			foreach($categories as $category) {
				$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s",'vconepage' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
			}
		}
	} elseif($vcop_query_source=='wp_custom_posts_type') {
		global $post;
		$taxonomy_names = get_object_taxonomies( $vcop_query_posts_type );
		$term_list = wp_get_post_terms($post->ID,$taxonomy_names);
		if($term_list){
			foreach ($term_list as $tax_term) {
				$output .= '<a href="' . esc_attr(get_term_link($tax_term, $vcop_query_posts_type)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a>'.$separator;
			}
		}
	}
	$return = trim($output, $separator);
	return $return;
}

/**************************************************************************/
/************************** FUNCTION VIEW *********************************/
/**************************************************************************/

function vconepage_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
		$view = __('Views','vconepage');
        return "0";
    }
	//$views = __('Views','adt');
	$count_final = $count; 
	//$count_final .= ' ' . $views;
    return $count_final;
}

function vconepage_set_post_views() {
	if ( is_single() ) {
	global $post;
	$postID = $post->ID;	
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
	}
}
add_filter( 'wp_footer', 'vconepage_set_post_views', 200000 );

/**************************************************************************/
/********************** END FUNCTION VIEW *********************************/
/**************************************************************************/

function vcop_portfolio_style ($instance, 
						  $vcop_portfolio_type, 
						  $vcop_portfolio_title_fs, 
						  $vcop_portfolio_content_fs, 
						  $vcop_portfolio_background_color, 
						  $vcop_portfolio_secondary_color, 
						  $vcop_portfolio_font_color, 
						  $vcop_portfolio_a_color, 
						  $vcop_portfolio_over_color) {
			
				$return = '';
				$return .= '
				 .vcop-portfolio-'.$instance.' {
					font-size: '.$vcop_portfolio_content_fs.';
					color:'.$vcop_portfolio_font_color.';					 
				 }
				 .vcop-portfolio-'.$instance.' h1 {
					font-size: '.$vcop_portfolio_title_fs.';
				 }
				 .vcop-portfolio-'.$instance.' a {
					color:'.$vcop_portfolio_a_color.';
				 }
				 .vcop-portfolio-'.$instance.' a:hover {
					color:'.$vcop_portfolio_over_color.';
				 }					 				 
				 .vcop-portfolio-'.$instance.' .vcop-item,
				 .vcop-portfolio-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-controls ul li {
					background:'.$vcop_portfolio_background_color.';
				}
				.vcop-portfolio.vcop-controls-'.$instance.' ul li {
					background:'.$vcop_portfolio_background_color.';
					color:'.$vcop_portfolio_font_color.';	
				}
				.vcop-portfolio.vcop-controls-'.$instance.' ul li:hover, 
				.vcop-portfolio.vcop-controls-'.$instance.' ul li.active {
					color:'.$vcop_portfolio_over_color.';
				}
				.vcop-portfolio-'.$instance.'.vcop-pagination .vcmp-pagination .current,
				.vcop-portfolio-'.$instance.'.vcop-pagination .vcmp-pagination a {
					background:'.$vcop_portfolio_background_color.';
					color:'.$vcop_portfolio_font_color.';
				}
				.vcop-portfolio-'.$instance.'.vcop-pagination a:hover,
				.vcop-portfolio-'.$instance.'.vcop-pagination .vcmp-pagination .current {	
					color:'.$vcop_portfolio_over_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_portfolio_secondary_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-item .vcop-title {
					background:'.$vcop_portfolio_secondary_color.';
					color:'.$vcop_portfolio_background_color.';
				}				
				.vcop-portfolio-'.$instance.' .vcop-container-thumbs .vcop-info-date {
					background:'.$vcop_portfolio_secondary_color.';
					color:'.$vcop_portfolio_background_color.';
					font-size:'.$vcop_portfolio_content_fs.';
				}
				.vcop-portfolio-'.$instance.'.vcop-pagination a {
					background:'.$vcop_portfolio_background_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_portfolio_secondary_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					color:'.$vcop_portfolio_a_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					border-color:'.$vcop_portfolio_a_color.';
				}				
				.vcop-portfolio-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover {
					color:'.$vcop_portfolio_over_color.';
				}
				.vcop-controls.vcop-controls-'.$instance.' .active, 
				.vcop-controls.vcop-controls-'.$instance.' .filter:hover {
					color:'.$vcop_portfolio_over_color.';
				}
				.vcop-portfolio-'.$instance.' .vcop-share-container {
					border-top-color:'.$vcop_portfolio_font_color.';
				}
				'; 
			return $return;			
}

function vcop_grid_style ($instance, 
						  $vcop_grid_type, 
						  $vcop_grid_title_fs, 
						  $vcop_grid_content_fs, 
						  $vcop_grid_background_color, 
						  $vcop_grid_secondary_color, 
						  $vcop_grid_font_color, 
						  $vcop_grid_a_color, 
						  $vcop_grid_over_color) {
			
				$return = '';
				$return .= '
				 .vcop-grid-'.$instance.' {
					font-size: '.$vcop_grid_content_fs.';
					color:'.$vcop_grid_font_color.';					 
				 }
				 .vcop-grid-'.$instance.' h1 {
					font-size: '.$vcop_grid_title_fs.'!important;
				 }
				 .vcop-grid-'.$instance.' a {
					color:'.$vcop_grid_a_color.';
				 }
				 .vcop-grid-'.$instance.' a:hover {
					color:'.$vcop_grid_over_color.';
				 }					 				 
				 .vcop-grid-'.$instance.' .vcop-item,
				 .vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-search,
				 .vcop-controls ul li {
					background:'.$vcop_grid_background_color.';
				}
				.vcop-grid.vcop-controls-'.$instance.' ul li {
					background:'.$vcop_grid_background_color.';
					color:'.$vcop_grid_font_color.';	
				}
				.vcop-grid.vcop-controls-'.$instance.' ul li:hover, 
				.vcop-grid.vcop-controls-'.$instance.' ul li.active {
					color:'.$vcop_grid_over_color.';
				}
				.vcop-grid-'.$instance.'.vcop-pagination .vcmp-pagination .current,
				.vcop-grid-'.$instance.'.vcop-pagination .vcmp-pagination a {
					background:'.$vcop_grid_background_color.';
					color:'.$vcop_grid_font_color.';
				}
				.vcop-grid-'.$instance.'.vcop-pagination a:hover,
				.vcop-grid-'.$instance.'.vcop-pagination .vcmp-pagination .current {	
					color:'.$vcop_grid_over_color.';
				}
				.vcop-grid-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_grid_secondary_color.';
				}
				.vcop-grid-'.$instance.' .vcop-item .vcop-title {
					background:'.$vcop_grid_secondary_color.';
					color:'.$vcop_grid_background_color.';
				}				
				.vcop-grid-'.$instance.' .vcop-container-thumbs .vcop-info-date {
					background:'.$vcop_grid_secondary_color.';
					color:'.$vcop_grid_background_color.';
					font-size:'.$vcop_grid_content_fs.';
				}
				.vcop-grid-'.$instance.'.vcop-pagination a {
					background:'.$vcop_grid_background_color.';
				}
				.vcop-grid-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_grid_secondary_color.';
				}
				.vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					color:'.$vcop_grid_a_color.';
				}
				.vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					border-color:'.$vcop_grid_a_color.';
				}				
				.vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover,
				.vcop-grid-'.$instance.' .vcop-image-over .adtheme-icon-search:hover {
					color:'.$vcop_grid_over_color.';
				}
				.vcop-controls.vcop-controls-'.$instance.' .active, 
				.vcop-controls.vcop-controls-'.$instance.' .filter:hover {
					color:'.$vcop_grid_over_color.';
				}
				'; 
			return $return;			
}

function vcop_gallery_style ($instance, 
						  $vcop_gallery_type, 
						  $vcop_gallery_title_fs, 
						  $vcop_gallery_background_color, 
						  $vcop_gallery_secondary_color, 
						  $vcop_gallery_font_color, 
						  $vcop_gallery_a_color, 
						  $vcop_gallery_over_color) {
			
				$return = '';
				$return .= '
				 .vcop-gallery-'.$instance.' {
					color:'.$vcop_gallery_font_color.';					 
				 }
				 .vcop-gallery-'.$instance.' h1 {
					font-size: '.$vcop_gallery_title_fs.'!important;
				 }
				 .vcop-gallery-'.$instance.' a {
					color:'.$vcop_gallery_a_color.';
				 }
				 .vcop-gallery-'.$instance.' a:hover {
					color:'.$vcop_gallery_over_color.';
				 }					 				 
				 .vcop-gallery-'.$instance.' .vcop-item,
				 .vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-search,
				 .vcop-controls ul li {
					background:'.$vcop_gallery_background_color.';
				}
				.vcop-gallery-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_gallery_secondary_color.';
				}
				.vcop-gallery-'.$instance.' .vcop-item .vcop-title {
					background:'.$vcop_gallery_secondary_color.';
					color:'.$vcop_gallery_background_color.';
				}
				.vcop-gallery-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_gallery_secondary_color.';
				}
				.vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					color:'.$vcop_gallery_a_color.';
				}
				.vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					border-color:'.$vcop_gallery_a_color.';
				}				
				.vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover,
				.vcop-gallery-'.$instance.' .vcop-image-over .adtheme-icon-search:hover {
					color:'.$vcop_gallery_over_color.';
				}
				.vcop-controls.vcop-controls-'.$instance.' .active, 
				.vcop-controls.vcop-controls-'.$instance.' .filter:hover {
					color:'.$vcop_gallery_over_color.';
				}
				'; 
			return $return;			
}

function vcop_carousel_style ($instance,
						  $vcop_carousel_item_padding, 
						  $vcop_carousel_type, 
						  $vcop_carousel_title_fs, 
						  $vcop_carousel_background_color, 
						  $vcop_carousel_secondary_color, 
						  $vcop_carousel_font_color, 
						  $vcop_carousel_a_color, 
						  $vcop_carousel_over_color
						  ) {
			
				$return = '';
				$return .= '
				 .vcop-carousel-'.$instance.' {
					color:'.$vcop_carousel_font_color.';					 
				 }
				 .vcop-carousel-'.$instance.' h1 {
					font-size: '.$vcop_carousel_title_fs.'!important;
				 }
				 .vcop-carousel-'.$instance.' a {
					color:'.$vcop_carousel_a_color.';
				 }
				 .vcop-carousel-'.$instance.' a:hover {
					color:'.$vcop_carousel_over_color.';
				 }					 				 
				 .vcop-carousel-'.$instance.' .vcop-item,
				 .vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-search,
				 .vcop-controls ul li {
					background:'.$vcop_carousel_background_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_carousel_secondary_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-item .vcop-title {
					background:'.$vcop_carousel_secondary_color.';
					color:'.$vcop_carousel_background_color.';
				}				
				.vcop-carousel-'.$instance.' .vcop-pagination a {
					background:'.$vcop_carousel_background_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_carousel_secondary_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					color:'.$vcop_carousel_a_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-plus {
					border-color:'.$vcop_carousel_a_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-search {
					color:'.$vcop_carousel_a_color.';
				}
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-search {
					border-color:'.$vcop_carousel_a_color.';
				}								
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover,
				.vcop-carousel-'.$instance.' .vcop-image-over .adtheme-icon-search:hover {
					color:'.$vcop_carousel_over_color.';
				}
				.vcop-carousel-'.$instance.' .owl-item	{
					padding:'.$vcop_carousel_item_padding.';
				}
				.vcop-carousel.owl-theme .owl-controls .owl-buttons div {
					background:'.$vcop_carousel_secondary_color.';
				}';
				
				if($vcop_carousel_item_padding != '' || !empty($vcop_carousel_item_padding)) {
				
					$return .= '.vcop-carousel .owl-buttons .owl-prev {
						left:'.$vcop_carousel_item_padding.'!important;	
					}
					.vcop-carousel .owl-buttons .owl-next {
						right:'.$vcop_carousel_item_padding.'!important;
					}
					'; 
				
				}
			return $return;			
}

function vcop_timeline_style($instance, 
						$vcop_timeline_type, 
						$vcop_timeline_title_fs,  
						$vcop_timeline_background_color, 
						$vcop_timeline_line_color, 
						$vcop_timeline_font_color, 
						$vcop_timeline_a_color, 
						$vcop_timeline_over_color) {
			
			$return = '.vcop-timeline-'.$instance.' {
						color:'.$vcop_timeline_font_color.';						
			}
			.vcop-timeline-'.$instance.' a {
						color:'.$vcop_timeline_a_color.';						
			}
			.vcop-timeline-'.$instance.' a:hover {
						color:'.$vcop_timeline_over_color.';						
			}			
			.vcop-timeline-'.$instance.' .vcop-timeline-content,
			.vcop-timeline-'.$instance.' .vcop_format_icon > span {
						background:'.$vcop_timeline_background_color.';
			}
			.vcop-timeline-'.$instance.' .vcop-timeline-block:nth-child(1n) .vcop-timeline-content::before {
						border-left-color:'.$vcop_timeline_background_color.';
			}
			.vcop-timeline-'.$instance.' .vcop-timeline-block:nth-child(even) .vcop-timeline-content::before {
						border-right-color:'.$vcop_timeline_background_color.'!important;	
			}
			.vcop-timeline-'.$instance.' #vcop-timeline:before {
						background:'.$vcop_timeline_line_color.';
			}
			.vcop-timeline-'.$instance.' .vcop-timeline-content .vcop-read-more {
						background:'.$vcop_timeline_font_color.';
			';

			return $return;
								
}

function vcop_postlist_style($instance, 
						$vcop_postlist_type, 
						$vcop_postlist_title_fs,   
						$vcop_postlist_font_color, 
						$vcop_postlist_a_color, 
						$vcop_postlist_over_color) {

			$return = '.vcop-postlist-'.$instance.' {
						color:'.$vcop_postlist_font_color.';						
			}
			.vcop-postlist-'.$instance.' a {
						color:'.$vcop_postlist_a_color.';						
			}
			.vcop-postlist-'.$instance.' a:hover {
						color:'.$vcop_postlist_over_color.';						
			}
			.vcop-postlist-'.$instance.' .vcop-postlist-title {
						font-size:'.$vcop_postlist_title_fs.';
			}
			';	

		return $return;							
}

function vcop_woocarousel_style ($instance,
						   $vcop_woocarousel_item_padding, 
						   $vcop_woocarousel_type, 
						   $vcop_woocarousel_title_fs, 
						   $vcop_woocarousel_background_color, 
						   $vcop_woocarousel_secondary_color, 
						   $vcop_woocarousel_font_color, 
						   $vcop_woocarousel_a_color, 
						   $vcop_woocarousel_over_color
						   ) {
			
				$return = '';
				$return .= '
				 .vcop-woocarousel-'.$instance.' {
					color:'.$vcop_woocarousel_font_color.';					 
				 }
				 .vcop-woocarousel-'.$instance.' h1 {
					font-size: '.$vcop_woocarousel_title_fs.'!important;
				 }
				 .vcop-woocarousel-'.$instance.' a {
					color:'.$vcop_woocarousel_a_color.';
				 }
				 .vcop-woocarousel-'.$instance.' a:hover {
					color:'.$vcop_woocarousel_over_color.';
				 }					 				 
				 .vcop-woocarousel-'.$instance.' .vcop-item,
				 .vcop-woocarousel-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-woocarousel-'.$instance.' .vcop-image-over .adtheme-icon-cart,
				 .vcop-controls ul li {
					background:'.$vcop_woocarousel_secondary_color.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-price  {				
					font-size:'.$vcop_woocarousel_title_fs.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_woocarousel_background_color.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-item .vcop-title,
				.vcop-woocarousel-'.$instance.' .vcop-item .vcop-item-sale {
					background:'.$vcop_woocarousel_background_color.';
					color:'.$vcop_woocarousel_font_color.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-item .vcop-price {
					color:'.$vcop_woocarousel_font_color.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-pagination a {
					background:'.$vcop_woocarousel_secondary_color.';
				}
				.vcop-woocarousel-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_woocarousel_background_color.';
				}								
				.vcop-woocarousel-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover,
				.vcop-woocarousel-'.$instance.' .vcop-image-over .adtheme-icon-cart:hover {
					color:'.$vcop_woocarousel_over_color.';
				}
				.vcop-woocarousel-'.$instance.' .owl-item	{
					padding:'.$vcop_woocarousel_item_padding.';
				}
				.vcop-woocarousel.owl-theme .owl-controls .owl-buttons div {
					background:'.$vcop_woocarousel_secondary_color.';
				}';
				
				if($vcop_woocarousel_item_padding != '' || !empty($vcop_woocarousel_item_padding)) {
				
					$return .= '.vcop-woocarousel .owl-buttons .owl-prev {
						left:'.$vcop_woocarousel_item_padding.'!important;	
					}
					.vcop-woocarousel .owl-buttons .owl-next {
						right:'.$vcop_woocarousel_item_padding.'!important;
					}
					'; 
				
				}
			return $return;			
}

function vcop_wooproductdisplay_style ($instance, 
						   $vcop_wooproductdisplay_type, 
						   $vcop_wooproductdisplay_title_fs, 
						   $vcop_wooproductdisplay_background_color, 
						   $vcop_wooproductdisplay_secondary_color, 
						   $vcop_wooproductdisplay_font_color, 
						   $vcop_wooproductdisplay_a_color, 
						   $vcop_wooproductdisplay_over_color
						   ) {
			
				$return = '';
				$return .= '
				 .vcop-wooproductdisplay-'.$instance.' {
					color:'.$vcop_wooproductdisplay_font_color.';					 
				 }
				 .vcop-wooproductdisplay-'.$instance.' h1 {
					font-size: '.$vcop_wooproductdisplay_title_fs.'!important;
				 }
				 .vcop-wooproductdisplay-'.$instance.' a {
					color:'.$vcop_wooproductdisplay_a_color.';
				 }
				 .vcop-wooproductdisplay-'.$instance.' a:hover {
					color:'.$vcop_wooproductdisplay_over_color.';
				 }					 				 
				 .vcop-wooproductdisplay-'.$instance.' .vcop-item,
				 .vcop-wooproductdisplay-'.$instance.' .vcop-image-over .adtheme-icon-plus,
				 .vcop-wooproductdisplay-'.$instance.' .vcop-image-over .adtheme-icon-cart,
				 .vcop-controls ul li {
					background:'.$vcop_wooproductdisplay_secondary_color.';
				}			
				.vcop-wooproductdisplay.vcop-controls-'.$instance.' ul li {
					background:'.$vcop_wooproductdisplay_secondary_color.';
					color:'.$vcop_wooproductdisplay_font_color.';	
				}				
				.vcop-wooproductdisplay.vcop-controls-'.$instance.' ul li:hover, 
				.vcop-wooproductdisplay.vcop-controls-'.$instance.' ul li.active {
					color:'.$vcop_wooproductdisplay_over_color.';
				}
				.vcop-wooproductdisplay-'.$instance.' .vcop-price {				
					font-size:'.$vcop_wooproductdisplay_title_fs.';
				}				
				.vcop-wooproductdisplay-'.$instance.'.vcop-pagination .vcmp-pagination .current,
				.vcop-wooproductdisplay-'.$instance.'.vcop-pagination .vcmp-pagination a {
					background:'.$vcop_wooproductdisplay_secondary_color.';
					color:'.$vcop_wooproductdisplay_font_color.';
				}
				.vcop-wooproductdisplay-'.$instance.'.vcop-pagination a:hover,
				.vcop-wooproductdisplay-'.$instance.'.vcop-pagination .vcmp-pagination .current {	
					color:'.$vcop_wooproductdisplay_over_color.';
				}
				.vcop-wooproductdisplay-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_wooproductdisplay_background_color.';
				}
				.vcop-wooproductdisplay-'.$instance.' .vcop-item .vcop-title,
				.vcop-wooproductdisplay-'.$instance.' .vcop-item .vcop-item-sale {
					background:'.$vcop_wooproductdisplay_background_color.';
					color:'.$vcop_wooproductdisplay_font_color.';
				}
				.vcop-wooproductdisplay-'.$instance.' .vcop-item .vcop-price {
					color:'.$vcop_wooproductdisplay_font_color.';
				}
				.vcop-wooproductdisplay-'.$instance.'.vcop-pagination a {
					background:'.$vcop_wooproductdisplay_secondary_color.';
				}
				.vcop-wooproductdisplay-'.$instance.' .vcop-item:hover .vcop-image-over {
					background:'.$vcop_wooproductdisplay_background_color.';
				}								
				.vcop-wooproductdisplay-'.$instance.' .vcop-image-over .adtheme-icon-plus:hover,
				.vcop-wooproductdisplay-'.$instance.' .vcop-image-over .adtheme-icon-cart:hover {
					color:'.$vcop_wooproductdisplay_over_color.';
				}
				.vcop-woocontrols.vcop-controls-'.$instance.' .active, 
				.vcop-woocontrols.vcop-controls-'.$instance.' .filter:hover {
					color:'.$vcop_wooproductdisplay_over_color.';
				}
				.vcop-wooproductdisplay.owl-theme .owl-controls .owl-buttons div {
					background:'.$vcop_wooproductdisplay_background_color.';
				}';
			return $return;			
}

function vcop_wootab_style($instance, 
					  $vcop_wootab_fs,  
					  $vcop_wootab_background_color, 
					  $vcop_wootab_secondary_color, 
					  $vcop_wootab_font_color, 
					  $vcop_wootab_a_color, 
					  $vcop_wootab_over_color) {
				$return = '';
				$return .= '.vcop-wootab-'.$instance.' {
					color:'.$vcop_wootab_font_color.';					 
				 }
				 .vcop-wootab-'.$instance.' {
					font-size: '.$vcop_wootab_fs.'!important;
				 }
				 .vcop-wootab-'.$instance.' a {
					color:'.$vcop_wootab_a_color.';
				 }
				 .vcop-wootab-'.$instance.' a:hover {
					color:'.$vcop_wootab_over_color.';
				 }					 				 			
				.vcop-wootab.vcop-controls-'.$instance.' ul li {
					background:'.$vcop_wootab_secondary_color.';
					color:'.$vcop_wootab_a_color.';	
				}				
				.vcop-wootab.vcop-controls-'.$instance.' ul li:hover, 
				.vcop-wootab.vcop-controls-'.$instance.' ul li.active {
					color:'.$vcop_wootab_over_color.';
				}				
				.vcop-wootab-'.$instance.'.vcop-pagination .vcmp-pagination .current,
				.vcop-wootab-'.$instance.'.vcop-pagination .vcmp-pagination a {
					background:'.$vcop_wootab_secondary_color.';
					color:'.$vcop_wootab_a_color.';
				}
				.vcop-wootab-'.$instance.'.vcop-pagination a:hover,
				.vcop-wootab-'.$instance.'.vcop-pagination .vcmp-pagination .current {	
					color:'.$vcop_wootab_over_color.';
				}
				.vcop-wootab-'.$instance.'.vcop-pagination a {
					background:'.$vcop_wootab_secondary_color.';
				}							
				.vcop-wootab-'.$instance.' .vcop-header-container {
					background:'.$vcop_wootab_background_color.';
					border-color:'.$vcop_wootab_font_color.';
				}
				.vcop-wootab-'.$instance.' .add_to_cart_button,
				.vcop-wootab-'.$instance.' .plus_show,
				.vcop-wootab-'.$instance.' .wc-forward,
				.vcop-wootab-'.$instance.' .vcop-item-sale {
					background:'.$vcop_wootab_secondary_color.';
				}				
				';
			return $return;			
	
								
}

function vcop_query($vcop_query_source,
					$vcop_query_sticky_posts, 
					$vcop_query_posts_type, 
					$vcop_query_categories,
					$vcop_query_order, 
					$vcop_query_orderby, 
					$vcop_query_pagination, 
					$vcop_query_number, 
					$vcop_query_posts_for_page,
					$vcop_query_pagination) {
					
					if($vcop_query_source == 'wp_custom_posts_type') {
						$vcop_query_sticky_posts = 'allposts';
					}
					
					if($vcop_query_sticky_posts == 'allposts') {
					
						$query = 'post_type=Post&post_status=publish&orderby='.$vcop_query_orderby.'&order='.$vcop_query_order.'';						
						
						// CUSTOM POST TYPE
						if($vcop_query_source == 'wp_custom_posts_type') {
							$query .= '&post_type='.$vcop_query_posts_type.'';
						}
						
						// CATEGORIES
						if($vcop_query_categories != '' && !empty($vcop_query_categories) && $vcop_query_source == 'wp_custom_posts_type') {
							$taxonomy_names = get_object_taxonomies( $vcmp_query_posts_type );
							$query .= '&'.$taxonomy_names[0].'='.$vcmp_query_categories.'';	
						}

						if($vcop_query_categories != '' && !empty($vcop_query_categories) && $vcop_query_source == 'wp_posts') {
							$query .= '&category_name='.$vcmp_query_categories.'';	
						}
							
						if($vcop_query_pagination == 'yes') {
							$query .= '&posts_per_page='.$vcop_query_posts_for_page.'';	
						} else {
							if($vcop_query_number == '') { $vcop_query_number = '-1'; }
							$query .= '&posts_per_page='.$vcop_query_number.'';
						}
					
						// PAGINATION		
						if($vcop_query_pagination == 'yes') {
							if ( get_query_var('paged') ) {
								$paged = get_query_var('paged');
							
							} elseif ( get_query_var('page') ) {			
								$paged = get_query_var('page');			
							} else {			
								$paged = 1;			
							}			
							$query .= '&paged='.$paged.'';
						}
						// #PAGINATION	
					
					} else {
						

						if($vcop_query_pagination == 'yes') {
							$vcop_query_number = $vcop_query_posts_for_page;	
						} else {
							if($vcop_query_number == '') { $vcop_query_number = '-1'; }
							$vcop_query_number = $vcop_query_number;
						}

						// PAGINATION		
						
						if ( get_query_var('paged') ) {
							$paged = get_query_var('paged');							
						} elseif ( get_query_var('page') ) {			
							$paged = get_query_var('page');			
						} else {			
							$paged = 1;			
						}			
						
						// #PAGINATION	
											
						/* STICKY POST DA FARE ARRAY PER SCRITTURA IN ARRAY */
					
						$sticky = get_option( 'sticky_posts' );
						$sticky = array_slice( $sticky, 0, 5 );
						$query = array(
							'post_type' => 'post',
							'post_status' => 'publish',
							'orderby' 	=> $vcop_query_orderby,
							'order' => $vcop_query_order,
							'category_name' => $vcop_query_categories,
							'posts_per_page' => $vcop_query_number,
							'paged' => $paged, 
							'post__in'  => $sticky,
							'ignore_sticky_posts' => 1
						);
					
						
					}
					
					
					return $query;	
}

function vcop_query_woocommerce($vcop_query_categories,
								$vcop_query_order, 
								$vcop_query_orderby, 
								$vcop_query_pagination, 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination) {
					
					
						$query = 'post_type=Post&post_status=publish&orderby='.$vcop_query_orderby.'&order='.$vcop_query_order.'';						
												
						// CUSTOM POST TYPE
						$query .= '&post_type=product';
						
						// CATEGORIES
						if($vcop_query_categories != '' || !empty($vcop_query_categories)) {
							$taxonomy_names = get_object_taxonomies( 'product' );
							$query .= '&product_cat='.$vcop_query_categories.'';	
						}
							
						if($vcop_query_pagination == 'yes') {
							$query .= '&posts_per_page='.$vcop_query_posts_for_page.'';	
						} else {
							if($vcop_query_number == '') { $vcop_query_number = '-1'; }
							$query .= '&posts_per_page='.$vcop_query_number.'';
						}
					
						// PAGINATION		
						if($vcop_query_pagination == 'yes') {
							if ( get_query_var('paged') ) {
								$paged = get_query_var('paged');
							
							} elseif ( get_query_var('page') ) {			
								$paged = get_query_var('page');			
							} else {			
								$paged = 1;			
							}			
							$query .= '&paged='.$paged.'';
						}
						// #PAGINATION					

						if($vcop_query_pagination == 'yes') {
							$vcop_query_number = $vcop_query_posts_for_page;	
						} else {
							if($vcop_query_number == '') { $vcop_query_number = '-1'; }
							$vcop_query_number = $vcop_query_number;
						}					
					
					return $query;	
}

function vcop_numeric_pagination($pages = '', $range = 2,$loop)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         $pages = $loop->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
	
	 $return = '';
	
     if(1 != $pages)
     {		 	
         $return .= "<div class='vcmp-pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) $return .=  "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) $return .=  "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 $return .=  ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) $return .= "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $return .=  "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         $return .=  "</div>\n";
     }
	 
	 return $return;
}

function vcop_filter_js($instance,$type) {
			
			$return = '';
			$return .= '<script type="text/javascript">';
			$return .= "jQuery(function($){
	  					$(document).ready(function(){
							$(function(){
							$('.vcop-".$type."-".$instance."').mixitup({
							targetSelector: '.vcop-item',
							filterSelector: '.filter',
							sortSelector: '.sort',
							buttonEvent: 'click',
							effects: ['fade','scale','rotateX','rotateY','rotateZ'],
							listEffects: null,
							easing: 'smooth',
							layoutMode: 'grid',
							targetDisplayGrid: 'inline-block',
							targetDisplayList: 'block',
							gridClass: '',
							listClass: '',
							transitionSpeed: 600,
							showOnLoad: 'all',
							sortOnLoad: false,
							multiFilter: false,
							filterLogic: 'or',
							resizeContainer: true,
							minHeight: 0,
							failClass: 'fail',
							perspectiveDistance: '3000',
							perspectiveOrigin: '50% 50%',
							animateGridList: true,
							onMixLoad: null,
							onMixStart: null,
							onMixEnd: null
						});
						});
						});
						});";		
			$return .= '</script>';
	
	return $return;
}

function vcop_filter_item($instance,$type,$vcop_query_source,$vcop_query_categories,$vcop_query_posts_type) {


		$return = '<div class="'.$type.' vcop-controls vcop-controls-'.$instance.'">
					<ul>
						<li data-filter="all" class="filter active">'.__('All','vconepage').'</li>';
		
		/* WP POST */
		if($vcop_query_source == 'wp_posts') {
			// ALL CATEGORY
			if(empty($vcop_query_categories) || $vcop_query_categories == '') {
						$categories = get_categories();
						foreach ( $categories as $category ) {
								$return .= '<li data-filter="' . $category->slug . '" class="filter">' . $category->name . '</li>';
						}
			} else {
						$wp_custom_taxonomy_cat_split = explode(",", $vcmp_query_categories);

						foreach ( $wp_custom_taxonomy_cat_split as $category ) {
								$idObj = get_category_by_slug($category);
  								$name = $idObj->term_id;
								$return .= '<li data-filter="' . $category . '" class="filter">' . get_cat_name($name) . '</li>';
						}
			}
		}
		/* #WP POST */

		/* WP CUSTOM POST */
		if($vcop_query_source == 'wp_custom_posts_type') {
			// ALL CATEGORY
			$taxonomy_names = get_object_taxonomies( $vcop_query_posts_type );
			
			// CHECK WOOCOMMERCE
			if($vcop_query_posts_type == 'product' && class_exists('Woocommerce')) {
					$taxonomy_names[0]= 'product_cat';
			}
			// #CHECK WOOCOMMERCE
			 
			if(!empty($taxonomy_names)) {
				if(empty($vcop_query_categories)) {
							$categories = get_categories('taxonomy='.$taxonomy_names[0].'');
	
							foreach ( $categories as $category ) {
									$return .= '<li data-filter="' . $category->slug . '" class="filter">' . $category->name . '</li>';
							}
				} else {
						$wp_custom_taxonomy_cat_split = explode(",", $vcop_query_categories);
							foreach ( $wp_custom_taxonomy_cat_split as $category ) {
									$category_single = get_term_by('name',$category,$taxonomy_names[0]);								
									$return .= '<li data-filter="' . $category_single->slug . '" class="filter">' . $category . '</li>';
							}
				}
			}
		}
		/* #WP CUSTOM POST */		
		
		
		
		
		
		$return .= '</ul></div>';
		
		
	return $return;
}

function vcop_filter_item_div($vcop_query_source,$vcop_query_categories,$vcop_query_posts_type) {

	/* WP POST */
	if($vcop_query_source == 'wp_posts') {
		if(empty($vcop_query_categories) || $vcop_query_categories == '') {
			$category = get_the_category();
			$return = '<div data-cat="'.$category[0]->slug.'" class="vcop-item '.$category[0]->slug.'" style=" display: inline-block; opacity: 1;">';
		} else {
			$cat_array = get_the_category();
			$cat_list = '';
			$cat_list2 = array();
				foreach ( $cat_array as $category ) {
						$cat_list .= $category->slug.' ';
						$cat_list2[] = $category->name;
				}
			$return = '<div data-cat="'.$cat_list.'" class="vcop-item '.$cat_list.'" style=" display: inline-block; opacity: 1;">';				
		}
	}
	/* #WP POST */

	/* WP CUSTOM POST */
	if($vcop_query_source == 'wp_custom_posts_type') {
					$taxonomy_names = get_object_taxonomies( $vcop_query_posts_type );
					
					// CHECK WOOCOMMERCE
					if($vcop_query_posts_type == 'product' && class_exists('Woocommerce')) {
						$taxonomy_names[0]= 'product_cat';
					}
					// #CHECK WOOCOMMERCE 
						
					if(!empty($taxonomy_names)) {					
						$cat_array = wp_get_post_terms( get_the_ID(), $taxonomy_names[0]);
						$cat_list = '';
						$cat_list2 = array();
						foreach ( $cat_array as $category ) {
							$cat_list .= $category->slug.' ';
							$cat_list2[] = $category->name;
						}
						$return = '<div data-cat="'.$cat_list.'" class="vcop-item '.$cat_list.'" style=" display: inline-block; opacity: 1;">';								
					} else {
						$return = '<div class="vcop-item" style=" display: inline-block; opacity: 1;">';
					}
	}	
	/* #WP CUSTOM POST */

	
	return $return;	
}

function vconepage_format_icon() {
	$id_post = get_the_id(); 
    $format = get_post_format( $id_post );   
    if (empty($format)) { $format = 'standard'; }
	if ($format == 'standard') { $return = '<span class="adtheme-icon-file3"></span>'; }
	if ($format == 'aside') { $return = '<span class="adtheme-icon-file"></span>'; }
	if ($format == 'link') { $return = '<span class="adtheme-icon-attachment"></span>'; }   
    if ($format == 'gallery') { $return = '<span class="adtheme-icon-images"></span>'; }
    if ($format == 'video') { $return = '<span class="adtheme-icon-youtube"></span>'; }
    if ($format == 'audio') { $return = '<span class="adtheme-icon-headphones"></span>'; }
    if ($format == 'image') { $return = '<span class="adtheme-icon-image2"></span>'; } 
    if ($format == 'quote') { $return = '<span class="adtheme-icon-quotes-left"></span>'; }
    if ($format == 'status') { $return = '<span class="adtheme-icon-bubble2"></span>'; }
	
	return $return;	
}

function vconepage_content() {
	global $post;
    $format = get_post_format( get_the_id() );   
    if (empty($format)) { $format = 'standard'; }
	$return = '';
	if($format == 'video') {
		if(strpos($post->post_content, '[video') !== false) {
			$video_part1 = explode("[video",$post->post_content);
			$video_part2 = explode("[/video]",$video_part1[1]);
			$video = '[video '.$video_part2[0].'[/video]'; 
			$return .= '<div class="vcop-timeline-video">'.do_shortcode($video).'</div>';
		} elseif(strpos($post->post_content, '[embed') !== false) {
			global $wp_embed;
			$video_part1 = explode("[embed]",$post->post_content);
			$video_part2 = explode("[/embed]",$video_part1[1]);
			$video = '[embed]'.$video_part2[0].'[/embed]'; 
			$return .= '<div class="vcop-timeline-video">'.$wp_embed->run_shortcode($video).'</div>';				
		}
	}
	if($format == 'audio') {
		if(strpos($post->post_content, '[audio') !== false) {
			$audio_part1 = explode("[audio",$post->post_content);
			$audio_part2 = explode("[/audio]",$audio_part1[1]);
			$audio = '[audio '.$audio_part2[0].'[/audio]'; 
			$return .= '<div class="vcop-timeline-audio">'.do_shortcode($audio).'</div>';
		} elseif(strpos($post->post_content, '[embed') !== false) {
			global $wp_embed;
			$audio_part1 = explode("[embed]",$post->post_content);
			$audio_part2 = explode("[/embed]",$audio_part1[1]);
			$audio = '[embed]'.$audio_part2[0].'[/embed]'; 
			$return .= '<div class="vcop-timeline-video">'.$wp_embed->run_shortcode($audio).'</div>';				
		}
	}
	if($format == 'image' || 
	   $format == 'standard' || 
	   $format == 'aside' || 
	   $format == 'quote' || 
	   $format == 'status') {
		$return .= '<span class="vcop-timeline-image">'.vconepage_thumbs('vconepage-rectangle').'</span>';
	}

	return $return;
}

// MENU
if (!function_exists('vconepage_setup')):

    function vconepage_setup() {
		register_nav_menus(
		array(
			'vconepage-menu' => __('VC One Page Menu', 'adonepage')
		)
		);
		$vconepage_number_menu = get_option( 'vconepage_number_menu', '' );
		if(!empty($vconepage_number_menu)) {
			for($i=1;$i<=$vconepage_number_menu;$i++) {
			
				register_nav_menus(
					array(
						'vconepage-menu-'.$i.'' => __('VC One Page Menu '.$i.'', 'adonepage')
					)
				);
					
				
			}
		}
	}
	
endif;
add_action('after_setup_theme', 'vconepage_setup');