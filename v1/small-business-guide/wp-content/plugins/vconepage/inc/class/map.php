<?php
/* CLASS: Map */

class vconepage_map_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_mapWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_map', array( $this, 'vconepage_map_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_map_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_mapWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }
		
		include('animate-list.php');
		
        vc_map( array(
            "name" => __("Map", 'vconepage'),
            "description" => __("Add google Map", 'vconepage'),
            "base" => "vcop_map",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/map.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type", 'vconepage'),
                  "param_name" => "vcop_type",
                  "value" => array(
									'Satellite' => 'satellite',
									'Terrain' => 'terrain',
									'Hybrid' => 'hybrid',
									'Roadmap' => 'roadmap'
				   ),
				   'admin_label' => true
				),
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Latitude",'vconepage'),
				  "param_name" => "vcop_latidude",
				  'admin_label' => true
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Longitude",'vconepage'),
				  "param_name" => "vcop_longitude",
			  ),			  
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Zoom", 'vconepage'),
                  "param_name" => "vcop_zoom",
				  "admin_label" => true,
                  "value" => array(
									'2',
									'3',
									'4',
									'5',
									'6',
									'7',
									'8',
									'9',
									'10',
									'11',
									'12',
									'13',
									'14',
									'15',
									'16',
									'17',
									'18',
									'19',
									'20',
									'21'																														
				   )
               ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Custom Marker", 'vconepage'),
                  "param_name" => "vcop_marker",
                  "value" => array(
									__('On','vconepage') => 'on',
									__('Off','vconepage') 	  => 'off'
				   )
				),
			  array(
				  "type" => "attach_image",
				  "class" => "",
				  "heading" => __("Image Custom Marker",'vconepage'),
				  "param_name" => "vcop_marker_image",
				  'dependency' => array(
						'element' => 'vcop_marker',
						'value' => array( 'on' )
				  ),				  
			  ),				
								   			  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Width",'vconepage'),
				  "param_name" => "vcop_width",
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Height",'vconepage'),
				  "param_name" => "vcop_height",
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Message",'vconepage'),
				  "param_name" => "vcop_messagge",
			  ),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Zoom Control", 'vconepage'),
                  "param_name" => "vcop_zoomcontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Scale Control", 'vconepage'),
                  "param_name" => "vcop_scalecontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Street View Control", 'vconepage'),
                  "param_name" => "vcop_streetviewcontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Rotate Control", 'vconepage'),
                  "param_name" => "vcop_rotatecontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Map Type Control", 'vconepage'),
                  "param_name" => "vcop_maptypecontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Pan Control", 'vconepage'),
                  "param_name" => "vcop_pancontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Overview Map Control", 'vconepage'),
                  "param_name" => "vcop_overviewmapcontrol",
                  "value" => array(
									__('Off','vconepage') => 'false',
									__('On','vconepage')  => 'true'
				   ),
				   "group" => "Advanced Options"
				),																												  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_map_loadCssAndJs() {

		// Main
		wp_register_style( 'vcop-map',  AD_VCOP_URL . 'assets/css/map.css' );
			
    }	
    /*
    Show notice if your plugin is activated but Visual Composer is not
    */
    public function showVcVersionNotice() {
        $plugin_data = get_plugin_data(__FILE__);
        echo '
        <div class="updated">
          <p>'.sprintf(__('Visual Composer Mega Pack <strong>%s</strong> requires <strong><a href="http://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=ad-theme" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vcop'), $plugin_data['Name']).'</p>
        </div>';
    }
}
// Finally initialize code
new vconepage_map_class();