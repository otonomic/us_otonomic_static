<?php

$pattern_list = array(
			'Assault' 					=> 	'd-assault', 
			'Binding Dark' 				=> 	'd-binding_dark',
			'Black Denim' 				=>  'd-black_denim',
			'Black Linen'     			=>  'd-black-Linen',
			'Dark Brick Wall'			=>  'd-dark_brick_wall',
			'Dark Dotted'				=>  'd-dark_dotted',
			'Dark Fish Skin'			=>  'd-dark_fish_skin',
			'Dark Matter'				=>  'd-dark_matter',
			'Dark Tire'					=>  'd-dark_Tire',
			'Debut Dark'				=>  'd-debut_dark',
			'Office'					=>  'd-office',
			'Grey Wash Wall'			=>  'd-grey_wash_wall',
			'Px'						=>  'd-px_by_Gre3g',
			'Bg Noise Lg'				=>  'l-bgnoise_lg',
			'Bright Squares'			=>	'l-bright_squares',
			'Debut Light'				=>  'l-debut_light',
			'Furley Bg'					=>  'l-furley_bg',
			'Grid Noise'				=>  'l-grid_noise',
			'Light Checkered Tiles' 	=> 'l-light_checkered_tiles',
			'Light Noise Diagonal'		=> 'l-light_noise_diagonal',
			'Light Wool'				=> 'l-light_wool',
			'Pw Maze White'				=> 'l-pw_maze_white',
			'Retro Intro'				=> 'l-retro_intro',
			'Shl'						=> 'l-shl',
			'Point'						=> 'point',
			'Point small'				=> 'point_small'		
);

?>