<?php
/* CLASS: header */

class vconepage_header_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_headerWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_header', array( $this, 'vconepage_header_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_header_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_headerWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }
		
		$menu_list = get_registered_nav_menus();
		
		$menu_list1 = array();
		foreach($menu_list as $location => $description) {
			$menu_list1[] = $description;
		}
	
		$menu_list2 = array();
		foreach($menu_list as $location => $description) {
			$menu_list2[] = $location;
		}	
	
		$menu_list_revenge = array_combine($menu_list1, $menu_list2);
				
		include('animate-list.php');
		
        vc_map( array(
            "name" => __("Style Page + Header", 'vconepage'),
            "description" => __("Style, Logo and Menu", 'vconepage'),
            "base" => "vcop_header",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/header.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(
						  
				// STYLE
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Layout Type", 'vconepage'),
                  "param_name" => "vcop_layout_type",
                  "value" => array(
				  					__('Full Width','vconepage') 	=> 'vcop-full-width',
									__('Boxed','vconepage')  		=> 'vcop-boxed'
				   ),				   			   
				),					

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Background Type", 'vconepage'),
                  "param_name" => "vcop_boxed_background_type",
                  "value" => array(
				  					__('Color','vconepage') 	=> 'color',
									__('Image','vconepage')  	=> 'image'
				   ),
				  "dependency" => array(
							'element' => 'vcop_layout_type',
							'value' => array( 'vcop-boxed' )
				  ),				   				   			   
				),
				
			  	array(
					"type" => "colorpicker",
					"class" => "",
					"heading" =>  __("Background Color", 'vconepage'),
					"param_name" => "vcop_boxed_background_color",
					"value" => '#FC615D',
				  	"dependency" => array(
							'element' => 'vcop_boxed_background_type',
							'value' => array( 'color' )
				  	),					
			  	),				
				
			  	array(
					"type" => "attach_image",
					"class" => "",
					"heading" =>  __("Background Image", 'vconepage'),
					"param_name" => "vcop_boxed_background_image",
				  	"dependency" => array(
							'element' => 'vcop_boxed_background_type',
							'value' => array( 'image' )
				  	),					
			  	),				

			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Wrapper Width (px)",'vconepage'),
				  "param_name" => "vcop_boxed_wrapper_width",
				  "description" => "Enter the pixel of wrapper for example: 1200",
				  "value" => '1200',
				  "dependency" => array(
							'element' => 'vcop_layout_type',
							'value' => array( 'vcop-boxed' )
				  ),				  					  
			  	),
				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Page Font Type", 'vconepage'),
                  "param_name" => "vcop_page_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_page_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_page_font_type',
							'value' => array( 'vcop-google-font' )
					),
				),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Page Font Color",
					"param_name" => "vcop_page_font_color",
					"value" => '#FC615D',
			  ),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Page Link Color",
					"param_name" => "vcop_page_font_color_link",
					"value" => '#FC615D',
			  ),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Page Over Color",
					"param_name" => "vcop_page_font_color_over_link",
					"value" => '#FC615D',
			  ),
			  
			  // HEADER
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Position", 'vconepage'),
                  "param_name" => "vcop_header_fixed_relative_position",
                  "value" => array( 
				  					__('Fixed','vconepage') 	    => 'vcop-header-fixed',
									__('Relative','vconepage') 		=> 'vcop-header-relative',								
				   ),				   
				   "group" => "Header"	
			  ),			  
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Header Position", 'vconepage'),
                  "param_name" => "vcop_header_position",
                  "value" => array( 
				  					__('Top','vconepage') 	    => 'vcop-header-top',
									__('Bottom','vconepage') 	=> 'vcop-header-bottom',								
				   ),
				  "dependency" => array(
						'element' => 'vcop_header_fixed_relative_position',
						'value' => array( 'vcop-header-fixed' )
				  ),				   
				   "group" => "Header"	
			  ),			

			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Header Type", 'vconepage'),
                  "param_name" => "vcop_header_type",
                  "value" => array( 
				  					__('Normal','vconepage') 							=> 'vcmp-header-normal',
									__('Sticky','vconepage') 							=> 'vcmp-header-sticky',				
									__('Sticky (First Slide no header)','vconepage')  	=> 'vcmp-header-sticky-v2',								
				   ),
				   "group" => "Header"	
			  ),
			  			
			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Background Color",
					"param_name" => "vcop_header_background_color",
					"value" => '#FC615D',
					"group" => "Header"	
			  ),

			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Padding",'vconepage'),
				  "param_name" => "vcop_header_padding",
				  "description" => "ie: 50px or 20px 10px 20px 10px",
				  "group" => 'Header',					  
			  ),

			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Margin",'vconepage'),
				  "param_name" => "vcop_header_margin",
				  "description" => "ie: 50px or 20px 10px 20px 10px",
				  "group" => 'Header',					  
			  ),
			  			
			  // LOGO
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Logo", 'vconepage'),
                  "param_name" => "vcop_header_logo_active",
                  "value" => array(
									__('Show','vconepage') 	 	=> 'on',
									__('Hidden','vconepage') 	=> 'off',									
				   ),
				   "group" => 'Logo',
			  ),
			  			  
			  array(
				  "type" => "attach_image",
				  "class" => "",
				  "heading" => __("Logo",'vconepage'),
				  "param_name" => "vcop_header_logo",
				  "group" => 'Logo',				  
			  ), 
			                
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Size", 'vconepage'),
                  "param_name" => "vcop_header_logo_position",
                  "value" => array(
									__('Left','vconepage') 	 	=> 'vcop-logo-left',
									__('Right','vconepage') 	=> 'vcop-logo-right',
									__('Top','vconepage')  		=> 'vcop-logo-top',
									__('Bottom','vconepage')  	=> 'vcop-logo-bottom',									
				   ),
				   "group" => 'Logo',
			  ),

              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Align", 'vconepage'),
                  "param_name" => "vcop_header_logo_align",
                  "value" => array(
									__('Left','vconepage') 		=> 'left',
									__('Right','vconepage') 	=> 'right',
									__('Center','vconepage') 	=> 'center',									
				   ),
				  "group" => 'Logo',				   
			  ),

			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_header_logo_link",
                  "value" => array(
									__('Home Page','vconepage')   => 'home-page',
									__('Same Page','vconepage')   => 'same-page',
									__('Custom Url','vconepage')  => 'custom-url',									
				   ),
				   "group" => 'Logo',
			  ),

			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Custom Url",'vconepage'),
				  "param_name" => "vcop_header_logo_custom_url",
				  "description" => "ex http://www.ad-theme.com",					  
				  'dependency' => array(
						'element' => 'vcop_header_logo_link',
						'value' => array( 'custom-url' )
				  ),
				  "group" => 'Logo',					  
			  ),
			  
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target", 'vconepage'),
                  "param_name" => "vcop_header_logo_target",
                  "value" => array(
									__('Blank (New Window)','vconepage') => '_blank',
									__('Self (Same Window)','vconepage') => '_self'
				   ),
				  "group" => 'Logo',				   
			  ),
			  	
			  // MENU	
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Menu Position", 'vconepage'),
                  "param_name" => "vcop_header_menu_list",
                  "value" => $menu_list_revenge,
				  "group" => 'Menu',				   
			  ),

              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Align", 'vconepage'),
                  "param_name" => "vcop_header_menu_text_align",
                  "value" => array(
									__('Left','vconepage') 		=> 'left',
									__('Right','vconepage') 	=> 'right',
									__('Center','vconepage') 	=> 'center',									
				   ),
				  "group" => 'Menu',				   
			  ),

              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Weight", 'vconepage'),
                  "param_name" => "vcop_header_menu_font_weight",
                  "value" => array(
									__('Normal','vconepage') 	=> 'normal',
									__('Bold','vconepage') 		=> 'bold'
				   ),
				  "group" => 'Menu',				   
			  ),			  

              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Trasform", 'vconepage'),
                  "param_name" => "vcop_header_menu_text_transform",
                  "value" => array(
									__('Normal','vconepage') 		=> 'none',
									__('Uppercase','vconepage') 	=> 'uppercase',
									__('Lowercase','vconepage') 	=> 'lowercase',
									__('Capitalize','vconepage') 	=> 'capitalize',									
				   ),
				  "group" => 'Menu',				   
			  ),

              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Decoration", 'vconepage'),
                  "param_name" => "vcop_header_menu_text_decoration",
                  "value" => array(
									__('None','vconepage') 		=> 'none',
									__('Underline','vconepage') 	=> 'underline',
									__('Overline','vconepage') 		=> 'overline',									
				   ),
				  "group" => 'Menu',				   
			  ),

			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Font Size",'vconepage'),
				  "param_name" => "vcop_header_menu_font_size",
				  "description" => "ie: 20px",
				  "group" => 'Menu',					  
			  ),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Menu Font Color",
					"param_name" => "vcop_header_menu_font_color",
					"value" => '#FC615D',
					"group" => 'Menu',
			  ),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Menu Link Color",
					"param_name" => "vcop_header_menu_font_color_link",
					"value" => '#FC615D',
					"group" => 'Menu',
			  ),

			  array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => "Menu Over Color",
					"param_name" => "vcop_header_menu_font_color_over_link",
					"value" => '#FC615D',
					"group" => 'Menu',
			  ),
			  	  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_header_loadCssAndJs() {

		// Main
		wp_register_style( 'vcop-header',  AD_VCOP_URL . 'assets/css/header.css' );
		wp_register_style( 'vcop-menu',  AD_VCOP_URL . 'assets/css/menu.css' );
		wp_register_script( 'vcop-menu-js',  AD_VCOP_URL . 'assets/js/menu.js' , array('jquery'), '', true);
		wp_register_script( 'vcop-mobile-menu-js',  AD_VCOP_URL . 'assets/js/jquery.mobilemenu.js' , array('jquery'), '', true);
		wp_register_script( 'vcop-menu-modernizr-js',  AD_VCOP_URL . 'assets/js/menu.modernizr.custom.js' , array('jquery'), '', true);
		wp_register_script( 'vcop-header-js',  AD_VCOP_URL . 'assets/js/header.js' , array('jquery'), '', true);
			
    }	
}
// Finally initialize code
new vconepage_header_class();