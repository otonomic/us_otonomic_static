<?php
/* CLASS: singleimage */

class vconepage_singleimage_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_singleimageWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_singleimage', array( $this, 'vconepage_singleimage_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_singleimage_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_singleimageWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('animate-list.php');
		
        vc_map( array(
            "name" => __("Single Image", 'vconepage'),
            "description" => __("Add Image with effect", 'vconepage'),
            "base" => "vcop_singleimage",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/singleimage.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(
			  array(
				  "type" => "attach_image",
				  "class" => "",
				  "heading" => __("Image Custom Marker",'vconepage'),
				  "param_name" => "vcop_singleimage_image",				  
			  ), 
			               
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Size", 'vconepage'),
                  "param_name" => "vcop_singleimage_size",
                  "value" => array(
									__('Thumbnail','vconepage')  => 'thumbnail',
									__('Medium','vconepage') 	  => 'large',
									__('Large','vconepage')  	  => 'large',
									__('Full','vconepage') 	  => 'full',									
				   ),
			  ),
			  
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Align", 'vconepage'),
                  "param_name" => "vcop_singleimage_align",
                  "value" => array(
									__('Center','vconepage')  	  => 'center',
									__('Left','vconepage') 	  => 'left',
									__('Right','vconepage')  	  => 'right',									
				   ),
			  ),			  
			  				
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_singleimage_link",
                  "value" => array(
									__('On','vconepage') => 'on',
									__('Off','vconepage') 	  => 'off'
				   ),
			  ),
			  				
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Url",'vconepage'),
				  "param_name" => "vcop_singleimage_url",
				  "description" => "ex http://www.ad-theme.com",					  
				  'dependency' => array(
						'element' => 'vcop_singleimage_link',
						'value' => array( 'on' )
				  ),					  
			  ),
			  				
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target", 'vconepage'),
                  "param_name" => "vcop_singleimage_target",
                  "value" => array(
									__('Blank (New Window)','vconepage') => '_blank',
									__('Self (Same Window)','vconepage') => '_self'
				   ),
				  'dependency' => array(
						'element' => 'vcop_singleimage_link',
						'value' => array( 'on' )
				  ),				   
			  ),				
				
			  // ANIMATION				   			  
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on', 
									
				   ),
				   "group" => "Animation"
			  ),			  
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
			  ),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_singleimage_loadCssAndJs() {

		// Main
		wp_register_style( 'vcop-singleimage',  AD_VCOP_URL . 'assets/css/singleimage.css' );
			
    }	
}
// Finally initialize code
new vconepage_singleimage_class();