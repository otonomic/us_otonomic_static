<?php
/* CLASS: social */

class vconepage_social_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_socialWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_social', array( $this, 'vconepage_social_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_social_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_socialWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("Social", 'vconepage'),
            "description" => __("Add your social", 'vconepage'),
            "base" => "vcop_social",
            "class" => "",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/social.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  		  						  	  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Social Type", 'vconepage'),
                  "param_name" => "vcop_social_type",
                  "value" => array(
									__('Social Button','vconepage')  	=> 'social-button',
									__('Social Share','vconepage') 	=> 'social-share',
				  ),
				  "admin_label" => true, 
				),
				
				/* BUTTON */
				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Facebook Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_fb",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Twitter Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_tw",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Linkedin Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_in",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Google Plus Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_gplus",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Pinterest Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_pi",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Youtube Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_yt",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Flickr Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_fl",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Tumblr Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_tu",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),				
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Rss Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_rss",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Vimeo Link",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_vi",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Email Address",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_email",
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-button' )
				  ),				  				  
				),

				/* #BUTTON */

				/* SHARE */
				
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Facebook Share",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_share_fb",
                  "value" => array(
									__('Show','vconepage')  	=> 'yes',
									__('Hidden','vconepage') 	=> 'no',
				  ),				  
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-share' )
				  ),				  				  
				),				
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Twitter Share",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_share_tw",
                  "value" => array(
									__('Show','vconepage')  	=> 'yes',
									__('Hidden','vconepage') 	=> 'no',
				  ),				  
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-share' )
				  ),				  				  
				),	
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Google Plus Share",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_share_gplus",
                  "value" => array(
									__('Show','vconepage')  	=> 'yes',
									__('Hidden','vconepage') 	=> 'no',
				  ),				  
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-share' )
				  ),				  				  
				),						
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Linkedin Share",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_share_in",
                  "value" => array(
									__('Show','vconepage')  	=> 'yes',
									__('Hidden','vconepage') 	=> 'no',
				  ),				  
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-share' )
				  ),				  				  
				),							
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Pinterest Share",'vconepage'),
				  "description" => "Leave empty if you want disable it",
				  "param_name" => "vcop_social_share_pi",
                  "value" => array(
									__('Show','vconepage')  	=> 'yes',
									__('Hidden','vconepage') 	=> 'no',
				  ),				  
				  "dependency" => array(
							'element' => 'vcop_social_type',
							'value' => array( 'social-share' )
				  ),				  				  
				),							
				
				
				
				/* #SHARE */


				/* STYLE */
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Style",'vconepage'),
				  "param_name" => "vcop_social_style",
                  "value" => array(
									__('Style 1','vconepage')  	=> 'style1',
									__('Style 2','vconepage')  	=> 'style2',																		
				  ),
				  "group" => "Style"				  			  				  
				),	
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Color", 'vcfg'),
					  "param_name" => "vcop_social_color",
					  "value" => '#FC615D',					  
					  "group" => "Style"				  
				),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Over Color", 'vcfg'),
					  "param_name" => "vcop_social_over_color",
					  "value" => '#FC615D',				  
					  "group" => "Style"				  
				),
				array(
				  "type" => "dropdown",
				  "class" => "",
				  "heading" => __("Custom Style",'vconepage'),
				  "param_name" => "vcop_social_style_type",
                  "value" => array(
									__('Default','vconepage')  	=> 'default',
									__('Custom','vconepage')  	=> 'custom',																		
				  ),
				  "group" => "Style"				  			  				  
				),					
			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Font Size",'vconepage'),
				  "param_name" => "vcop_social_style_font_size",
				  "description" => "ex 50px",
				  "dependency" => array(
							'element' => 'vcop_social_style_type',
							'value' => array( 'custom' )
				  ),				  
				  "group" => "Style"					  		  
			  	),
			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Margin",'vconepage'),
				  "param_name" => "vcop_social_style_margin",
				  "description" => "ex 50px",
				  "dependency" => array(
							'element' => 'vcop_social_style_type',
							'value' => array( 'custom' )
				  ),				  
				  "group" => "Style"					  		  
			  	),
								
				/* #STYLE */
				
				/* ANIMATION */
												 				 												  			  			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			    

			  /* #ANIMATION */
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_social_loadCssAndJs() {
		
		wp_register_style( 'vcop-social',  AD_VCOP_URL . 'assets/css/social.css' );		
					
    }	
}
// Finally initialize code
new vconepage_social_class();