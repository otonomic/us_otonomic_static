<?php
/* CLASS: gallery */

class vconepage_gallery_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_galleryWithVC' ) );
        add_shortcode( 'vcop_gallery', array( $this, 'vconepage_gallery_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_gallery_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_galleryWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Gallery", 'vconepage'),
            "description" => __("Add your gallery", 'vconepage'),
            "base" => "vcop_gallery",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/gallery.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Gallery Type", 'vconepage'),
                  "param_name" => "vcop_gallery_type",
                  "value" => array(
				  					__('Style 1','vconepage') 	=> 'vcop-gallery-style1',
									__('Style 2','vconepage')  => 'vcop-gallery-style2',
									__('Style 3','vconepage')  => 'vcop-gallery-style3',															
				   ),			   
				),		

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Columns", 'vconepage'),
                  "param_name" => "vcop_gallery_columns",
                  "value" => array(
				  					__('1','vconepage') 	=> '1',
									__('2','vconepage')  	=> '2',
				  					__('3','vconepage') 	=> '3',
									__('4','vconepage')  	=> '4',
				  					__('5','vconepage') 	=> '5',
									__('6','vconepage')  	=> '6',
				  					__('7','vconepage') 	=> '7',
									__('8','vconepage')  	=> '8',																											
				   ),				   				   			   
				),										

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_gallery_link",
                  "value" => array(
				  					__('Image','vconepage') 	=> 'image',
									__('Custom Url','vconepage')  => 'custom_url',															
				   ),			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target Link", 'vconepage'),
                  "param_name" => "vcop_gallery_target_link",
                  "value" => array(
				  					__('Same Window','vconepage')  => '_self',	
				  					__('New Window','vconepage') 	=> '_blank'
																							
				   ),
				  "dependency" => array(
							'element' => 'vcop_gallery_link',
							'value' => array( 'custom_url' )
					),				   			   
				),

                array(
                  "type" => "attach_images",
                  "class" => "",
                  "heading" => __("Upload Your Images", 'vconepage'),
                  "param_name" => "vcop_gallery_images"
                ),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_gallery_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_gallery_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_gallery_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_gallery_title_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_gallery_background_color",
					  "value" => 'rgba(255,255,255,1)',				  
					  "group" => "Style"					  					  				  
				  ),			  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Secondary Color", 'vconepage'),
					  "param_name" => "vcop_gallery_secondary_color",
					  "value" => 'rgba(0,0,0,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_gallery_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_gallery_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_gallery_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_gallery_loadCssAndJs() {	
		wp_register_style( 'vcop-gallery',  AD_VCOP_URL . 'assets/css/gallery.css' );
		wp_register_style( 'vcop-magnific-popup',  AD_VCOP_URL . 'assets/css/magnific-popup.css' );
		wp_register_script( 'vcop-magnific-popup-js',  AD_VCOP_URL . 'assets/js/jquery.magnific-popup.js' , array('jquery'), '', true);
    }	
}
// Finally initialize code
new vconepage_gallery_class();