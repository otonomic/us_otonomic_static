<?php
/* CLASS: timeline */

class vconepage_timeline_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_timelineWithVC' ) );
        add_shortcode( 'vcop_timeline', array( $this, 'vconepage_timeline_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_timeline_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_timelineWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Timeline", 'vconepage'),
            "description" => __("Add your timeline", 'vconepage'),
            "base" => "vcop_timeline",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/timeline.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Timeline Type", 'vconepage'),
                  "param_name" => "vcop_timeline_type",
                  "value" => array(
				  					__('Style 1','vconepage') 	=> 'vcop-timeline-style1',
									__('Style 2','vconepage')  => 'vcop-timeline-style2',															
				   ),			   
				),		

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Timeline Source", 'vconepage'),
                  "param_name" => "vcop_timeline_source",
                  "value" => array(
				  					__('Posts/Custom Posts','vconepage') 	=> 'vcop_posts',
									__('Gallery Image','vconepage')  		=> 'vcop_gallery',																										
				   ),				   				   			   
				),	
													
				// POSTS
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Date Format", 'vconepage'),
                  "param_name" => "vcop_timeline_date_format",
                  "value" => array(
				  					__('November 6, 2010 12:50 am','vconepage') 		=> 'F j, Y g:i a',
									__('November 6, 2010','vconepage')  				=> 'F j, Y',
				  					__('November, 2010','vconepage') 					=> 'F, Y',
									__('12:50 am','vconepage')  						=> 'g:i a',
				  					__('12:50:48 am','vconepage') 						=> 'g:i:s a',
									__('Saturday, November 6th, 2010','vconepage')  	=> 'l, F jS, Y',
				  					__('Nov 6, 2010 @ 0:50','vconepage') 				=> 'M j, Y @ G:i',
									__('2010/11/06 at 12:50 AM','vconepage')  			=> 'Y/m/d \a\t g:i A',
				  					__('2010/11/06 at 12:50am','vconepage') 			=> 'Y/m/d \a\t g:ia',
									__('2010/11/06 12:50:48 AM','vconepage')  			=> 'Y/m/d g:i:s A',
									__('2010/11/06','vconepage')  						=> 'Y/m/d',																																														
				   ),					   					   				   			   
				),				
								
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Title", 'vconepage'),
                  "param_name" => "vcop_timeline_show_title",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Image", 'vconepage'),
                  "param_name" => "vcop_timeline_show_image",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Excerpt", 'vconepage'),
                  "param_name" => "vcop_timeline_show_excerpt",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
					),					   			   			   
				),
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Charpter Excerpt",'vconepage'),
				  "param_name" => "vcop_timeline_excerpt_number",
				  "description" => "ex: 150. Leave Empty for default value",
				  "dependency" => array(
							'element' => 'vcop_timeline_show_excerpt',
							'value' => array( 'true' )
				  ),				  					  				  					  				  			  				  
			    ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Description", 'vconepage'),
                  "param_name" => "vcop_timeline_show_description",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_gallery' )
					),					   			   			   
				),													
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Button", 'vconepage'),
                  "param_name" => "vcop_timeline_show_button",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
					),				   			   
				),					
				
				// GALLERY		
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_timeline_link",
                  "value" => array(
				  					__('Image','vconepage') 	=> 'image',
									__('Custom Url','vconepage')  => 'custom_url',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_gallery' )
					),				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target Link", 'vconepage'),
                  "param_name" => "vcop_timeline_target_link",
                  "value" => array(
				  					__('Same Window','vconepage')  => '_self',	
				  					__('New Window','vconepage') 	=> '_blank'
																							
				   ),
				  "dependency" => array(
							'element' => 'vcop_timeline_link',
							'value' => array( 'custom_url' )
				  ),				   			   
				),

                array(
                  "type" => "attach_images",
                  "class" => "",
                  "heading" => __("Upload Your Images", 'vconepage'),
                  "param_name" => "vcop_timeline_images",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_gallery' )
				  ),				  
                ),

				// QUERY 
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Source", 'vconepage'),
                  "param_name" => "vcop_query_source",
                  "value" => array(
				  					__('Wordpress Posts','vconepage') 		=> 'wp_posts',
									__('Custom Posts Type','vconepage')  	=> 'wp_custom_posts_type',
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("All Posts/Sticky posts", 'vconepage'),
                  "param_name" => "vcop_query_sticky_posts",
                  "value" => array(
				  					__('All Posts','vconepage') 		 	=> 'allposts',
									__('Only Sticky Posts','vconepage')  	=> 'onlystickyposts',
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_source',
							'value' => array( 'wp_posts' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => __("Select Post Type Source",'vconepage'),
				  "param_name" => "vcop_query_posts_type",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_source',
							'value' => array( 'wp_custom_posts_type' )
				  ),					  				  					  				  			  				  
			    ),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Categories",'vconepage'),
				  "param_name" => "vcop_query_categories",
				  "description" => "Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order", 'vconepage'),
                  "param_name" => "vcop_query_order",
                  "value" => array(
				  					__('DESC','vconepage')  	=> 'DESC',
				  					__('ASC','vconepage') 		=> 'ASC'
									
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order By", 'vconepage'),
                  "param_name" => "vcop_query_orderby",
                  "value" => array(
				  					__('Date','vconepage') 			=> 'date',
									__('ID','vconepage')  				=> 'ID',
				  					__('Author','vconepage') 			=> 'author',
									__('Title','vconepage')  			=> 'title',
				  					__('Name','vconepage') 			=> 'name',
									__('Modified','vconepage')  		=> 'modified',
				  					__('Parent','vconepage') 			=> 'parent',
									__('Rand','vconepage')  			=> 'rand',
									__('Comments Count','vconepage')  	=> 'comment_count',
									__('None','vconepage')  			=> 'none',																																													
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts",'vconepage'),
				  "param_name" => "vcop_query_number",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_timeline_source',
							'value' => array( 'vcop_posts' )
				  ),					  				  					  				  			  				  
			    ),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_timeline_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_timeline_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_timeline_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_timeline_title_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_timeline_background_color",
					  "value" => 'rgba(255,255,255,1)',				  
					  "group" => "Style"					  					  				  
				  ),			  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Line Color", 'vconepage'),
					  "param_name" => "vcop_timeline_line_color",
					  "value" => 'rgba(0,0,0,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_timeline_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_timeline_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_timeline_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_timeline_loadCssAndJs() {	
		wp_register_style( 'vcop-timeline',  AD_VCOP_URL . 'assets/css/timeline.css' );
		wp_register_style( 'vcop-magnific-popup',  AD_VCOP_URL . 'assets/css/magnific-popup.css' );
		wp_register_script( 'vcop-magnific-popup-js',  AD_VCOP_URL . 'assets/js/jquery.magnific-popup.js' , array('jquery'), '', true);				
    }	
}
// Finally initialize code
new vconepage_timeline_class();