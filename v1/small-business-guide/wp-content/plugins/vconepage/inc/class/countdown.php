<?php
/* CLASS: countdown */

class vconepage_countdown_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_countdownWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_countdown', array( $this, 'vconepage_countdown_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_countdown_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_countdownWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("Countdown", 'vconepage'),
            "description" => __("Add your Countdown", 'vconepage'),
            "base" => "vcop_countdown",
            "class" => "",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/countdown.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  		  						  	  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Year (format: YYYY)",'vconepage'),
				  "param_name" => "vcop_countdown_year"				  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Month (format: MM)",'vconepage'),
				  "param_name" => "vcop_countdown_month"				  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Day (format: DD)",'vconepage'),
				  "param_name" => "vcop_countdown_day"				  
			  ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type Coundown", 'vconepage'),
                  "param_name" => "vcop_countdown_type",
                  "value" => array(
									__('382 Days 05:44:12','vconepage') => 'type1',
									__('25 weeks 4 days 23 hr 57 min 56 sec','vconepage') 	  => 'type2',
									__('47 hr 59 min 14 sec','vconepage') => 'type3',
				   )
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Style", 'vconepage'),
                  "param_name" => "vcop_countdown_style",
                  "value" => array(
									__('Style 1','vconepage') => 'style1',
									__('Style 2','vconepage') 	  => 'style2',
									__('Style 3','vconepage') => 'style3',
				   )
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Active Custom Style", 'vconepage'),
                  "param_name" => "vcop_countdown_style_active",
                  "value" => array(
									__('Default style','vconepage')  	=> 'off',
									__('Custom Style','vconepage') 	=> 'on',
				  ),
				  "group" => "Style" 
				),				
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Main Color", 'vcfg'),
					  "param_name" => "vcop_countdown_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
					  ),					  
					  "group" => "Style"				  
				),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Secondary Color", 'vcfg'),
					  "param_name" => "vcop_countdown_secondary_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
					  ),					  
					  "group" => "Style"				  
				),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vcfg'),
					  "param_name" => "vcop_countdown_background_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
					  ),					  
					  "group" => "Style"				  
				),				
			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Font Size",'vconepage'),
				  "param_name" => "vcop_countdown_font_size",
				  "description" => "ex 50px",
				  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
				  ),				  
				  "group" => "Style"					  		  
			  	),
			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Padding",'vconepage'),
				  "param_name" => "vcop_countdown_padding",
				  "description" => "ex 50px",
				  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
				  ),				  
				  "group" => "Style"					  		  
			  	),	
			  	array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Margin",'vconepage'),
				  "param_name" => "vcop_countdown_margin",
				  "description" => "ex 50px",
				  "dependency" => array(
							'element' => 'vcop_countdown_style_active',
							'value' => array( 'on' )
				  ),				  
				  "group" => "Style"					  		  
			  	),											 				 												  			  			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			    
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_countdown_loadCssAndJs() {
		
		// Main
		wp_register_style( 'vcop-countdown',  AD_VCOP_URL . 'assets/css/countdown.css' );		
		wp_register_script( 'vcop-countdown-js',  AD_VCOP_URL . 'assets/js/jquery.countdown.min.js' , array('jquery'), '', true);

					
    }	
}
// Finally initialize code
new vconepage_countdown_class();