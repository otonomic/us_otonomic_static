<?php
/* CLASS: Space */

class vconepage_space_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_spaceWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_space', array( $this, 'vconepage_space_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_space_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_spaceWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
        vc_map( array(
            "name" => __("Space", 'vconepage'),
            "description" => __("Add Space", 'vconepage'),
            "base" => "vcop_space",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/space.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Height Space",'vconepage'),
				  "description" => 'Ex: 100px',
				  "param_name" => "vcop_height",
				  'admin_label' => true
			  ),			  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_space_loadCssAndJs() {

		// Main
		wp_register_style( 'vcop-space',  AD_VCOP_URL . 'assets/css/space.css' );
			
    }	
}
// Finally initialize code
new vconepage_space_class();