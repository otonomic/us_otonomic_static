<?php
/* CLASS: wootab */

class vconepage_wootab_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_wootabWithVC' ) );
        add_shortcode( 'vcop_wootab', array( $this, 'vconepage_wootab_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_wootab_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_wootabWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Woo Product in Tab", 'vconepage'),
            "description" => __("Woocommerce Product in Tab", 'vconepage'),
            "base" => "vcop_wootab",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/wootab.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(
								
				// POSTS
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Filter", 'vconepage'),
                  "param_name" => "vcop_wootab_filter",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  	=> 'false',															
				   ),			   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Image", 'vconepage'),
                  "param_name" => "vcop_wootab_image",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'image_show',
									__('Hidden','vconepage')  	=> 'image_hidden',															
				   ),			   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Title", 'vconepage'),
                  "param_name" => "vcop_wootab_name",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'name_show',
									__('Hidden','vconepage')  	=> 'name_hidden',															
				   ),			   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Stars", 'vconepage'),
                  "param_name" => "vcop_wootab_stars",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'stars_show',
									__('Hidden','vconepage')  	=> 'stars_hidden',															
				   ),			   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Stock", 'vconepage'),
                  "param_name" => "vcop_wootab_stock",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'stock_show',
									__('Hidden','vconepage')  	=> 'stock_hidden',															
				   ),			   			   
				),	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Price", 'vconepage'),
                  "param_name" => "vcop_wootab_price",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'price_show',
									__('Hidden','vconepage')  	=> 'price_hidden',															
				   ),			   			   
				),																													
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Cart", 'vconepage'),
                  "param_name" => "vcop_wootab_cart",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'cart_show',
									__('Hidden','vconepage')  	=> 'cart_hidden',															
				   ),			   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Plus", 'vconepage'),
                  "param_name" => "vcop_wootab_plus",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'plus_show',
									__('Hidden','vconepage')  	=> 'plus_hidden',															
				   ),			   			   
				),				

				// QUERY 
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Categories",'vconepage'),
				  "param_name" => "vcop_query_categories",
				  "description" => "Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)",
				  "group" => "Query",				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order", 'vconepage'),
                  "param_name" => "vcop_query_order",
                  "value" => array(
				  					__('DESC','vconepage')  	=> 'DESC',
				  					__('ASC','vconepage') 		=> 'ASC'
									
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order By", 'vconepage'),
                  "param_name" => "vcop_query_orderby",
                  "value" => array(
				  					__('Date','vconepage') 			=> 'date',
									__('ID','vconepage')  				=> 'ID',
				  					__('Author','vconepage') 			=> 'author',
									__('Title','vconepage')  			=> 'title',
				  					__('Name','vconepage') 			=> 'name',
									__('Modified','vconepage')  		=> 'modified',
				  					__('Parent','vconepage') 			=> 'parent',
									__('Rand','vconepage')  			=> 'rand',
									__('Comments Count','vconepage')  	=> 'comment_count',
									__('None','vconepage')  			=> 'none',																																													
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination", 'vconepage'),
                  "param_name" => "vcop_query_pagination",
                  "value" => array(
				  					__('Yes','vconepage') 		=> 'yes',
									__('No','vconepage')  		=> 'no',
				   ),
				   "group" => "Query"				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts",'vconepage'),
				  "param_name" => "vcop_query_number",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'no' )
				  ),					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination Type", 'vconepage'),
                  "param_name" => "vcop_query_pagination_type",
                  "value" => array(
				  					__('Numeric','vconepage') 		=> 'numeric',
									__('Normal','vconepage')  		=> 'normal',
				  ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),				   				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts For Page",'vconepage'),
				  "param_name" => "vcop_query_posts_for_page",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),					  				  					  				  			  				  
			    ),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_wootab_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_wootab_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_wootab_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_wootab_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_wootab_background_color",
					  "value" => 'rgba(255,255,255,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),			  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Secondary Color", 'vconepage'),
					  "param_name" => "vcop_wootab_secondary_color",
					  "value" => 'rgba(0,0,0,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wootab_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wootab_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wootab_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_wootab_loadCssAndJs() {	
		wp_register_style( 'vcop-wootab',  AD_VCOP_URL . 'assets/css/wootab.css' );
		wp_register_script( 'vcop-filter-js',  AD_VCOP_URL . 'assets/js/jquery.mixitup.js' , array('jquery'), '', true);		
    }	
}
// Finally initialize code
new vconepage_wootab_class();