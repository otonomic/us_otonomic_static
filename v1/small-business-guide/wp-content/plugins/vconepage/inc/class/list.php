<?php
/* CLASS: list */

class vconepage_list_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_listWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_list', array( $this, 'vconepage_list_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_list_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_listWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("List", 'vconepage'),
            "description" => __("Add your list", 'vconepage'),
            "base" => "vcop_list",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/list.png'),
			"admin_enqueue_css" => array(plugins_url('vconepage/assets/css/general/fonts.css')),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(             
			   array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Icon", 'vconepage'),
                  "param_name" => "vcop_list",
				  "admin_label" => true,
                  "value" => $adtheme_icons_list,
               ),			   				  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Float", 'vconepage'),
                  "param_name" => "vcop_list_float",
                  "value" => array(
									__('Left','vconepage')  	=> 'left',
									__('Right','vconepage') 	=> 'right',
									__('None','vconepage') 	=> 'none'
				   )
				),
               array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_list_link",
                  "value" => array(
									__('off','vconepage') => 'off',
									__('On','vconepage') 	  => 'on'
				   )
				),	
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Link Url",'vconepage'),
				  "param_name" => "vcop_list_link_url",
				  "description" => "Add link url ex: http://www.ad-theme.com",					  
				  'dependency' => array(
						'element' => 'vcop_list_link',
						'value' => array( 'on' )
				  ),					  
			  ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link Url Target", 'vconepage'),
                  "param_name" => "vcop_list_link_target",
                  "value" => array(
									__('Self (Same windows)','vconepage') => '_self',
									__('New Windows','vconepage') 	  => '_blank'
				   ),
				  'dependency' => array(
						'element' => 'vcop_list_link',
						'value' => array( 'on' )
				  ),					   
				),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Title Text",'vconepage'),
				  "param_name" => "vcop_list_title_text",				  
			  ),					
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Content Text",'vconepage'),
				  "param_name" => "vcop_list_content_text",				  
			  ),

				
			  // STYLE	
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_list_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
			  ),				
			  array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_list_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_list_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
			  ), 
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Align", 'vconepage'),
                  "param_name" => "vcop_list_font_align",
                  "value" => array(
				  					__('Left','vconepage') 	=> 'left',
									__('Right','vconepage')  	=> 'right',
									__('Center','vconepage')  	=> 'center',
				   ),
				   "group" => "Style"				   			   
			  ),			  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Title Size (px)",'vconepage'),
				  "param_name" => "vcop_list_title_size",
				  "description" => "ex 15",
				  "group" => "Style"				  
			  ),			               
			  array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Title Color", 'vcfg'),
                  "param_name" => "vcop_list_title_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Content Size (px)",'vconepage'),
				  "param_name" => "vcop_list_content_size",
				  "description" => "ex 15",
				  "group" => "Style"				  
			  ),			               
			  array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Content Color", 'vcfg'),
                  "param_name" => "vcop_list_content_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),			  			  										  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_list_icon_size",
				  "description" => "ex 15",
				  "group" => "Style"				  
			  ),			  
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Icon Color", 'vcfg'),
                  "param_name" => "vcop_list_icon_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Icon Hover Color", 'vcfg'),
                  "param_name" => "vcop_list_icon_over_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),  						  		  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Margin",'vconepage'),
				  "param_name" => "vcop_list_icon_margin",
				  "description" => "ex 2px or 2px 2px 2px 2px",
				  "group" => "Style"				  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Padding",'vconepage'),
				  "param_name" => "vcop_list_icon_padding",
				  "description" => "ex 2px or 2px 2px 2px 2px",
				  "group" => "Style"				  
			  ),
 				
				// ANIMATION  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_list_loadCssAndJs() {
					
    }	
}
// Finally initialize code
new vconepage_list_class();