<?php
$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = '';
extract(shortcode_atts(array(
    'el_class'        => '',
    'bg_image'        => '',
    'bg_color'        => '',
    'bg_image_repeat' => '',
    'font_color'      => '',
    'padding'         => '',
    'margin_bottom'   => '',
    'css' => '',
	'vcop_row_name' => '',
	'vcop_row_wrapper_active' => '',
	'vcop_row_wrapper_value' => '',
	'type_background' => '',
	'type_video' => '',
	'vcop_parallax_image' => '',
	'vcop_video_url' => '',
	'vcop_video_youtube_sound' => '',
	'vcop_video_vimeo_id' => '',
	'vcop_video_vimeo_sound' => '',
	'vcop_video_local_sound' => '',
	'vcop_video_image_cover' => '',
	'vcop_video_image_cover_upload' => '',
	'vcop_slide_images' => '',
	'vcop_height_row' => '',
	'vcop_padding' => '',
	'vcop_align_text' => '',
	'background_pattern' => '', 
	'background_pattern_type' => '',
	'background_pattern_image' => '',
	'background_pattern_color' => '',
	'background_pattern_image_opacity' => '',
	'background_pattern_padding_active' => '',
	'background_pattern_padding_left' => '',
	'background_pattern_padding_top' => '',
	'responsive_mode' => 'true' 
), $atts));
	$instance = rand(1,1000000000);

wp_enqueue_script( 'wpb_composer_front_js' );
wp_enqueue_style('vcop-row');
// STYLE ROW
$row_style = $vcop_height_row . ' ' .$vcop_padding;
$responsive_mode_class = '';
if($responsive_mode == 'true') {
	$responsive_mode_class = 'vcop_responsive_mode vcop_responsive_pattern';
}
if($vcop_row_wrapper_active == 'vcop_row_wrapper') {
	$output .= '<style type="text/css">
					.vcop-'.$instance.'.vcop_row_wrapper > .vcop_wrapper { width:'.$vcop_row_wrapper_value.'; margin:0 auto; }
				</style>';
}

/************************************************************************************/
/*********************************** PATTERN ****************************************/
/************************************************************************************/

if($background_pattern == 'on') {
	if($background_pattern_type == 'image') {
		$style = 'background-image:url(\'' . AD_VCOP_URL . 'assets/img/pattern/'. $background_pattern_image . '.png\');';
		if($background_pattern_image_opacity == '') { $background_pattern_image_opacity = '0.5'; }	
		$style .= 'opacity:'.$background_pattern_image_opacity.';';		
	} else {
		$style = 'background-color:'. $background_pattern_color . ';';
	}
	if($background_pattern_padding_active == 'on') {	
		$style .= 'margin-left:-'.$background_pattern_padding_left.'px;';
	}
	
	$pattern = '<div class="vcop-pattern" style="'.$style.'"></div>';
}
$el_class = $this->getExtraClass($el_class);

/************************************************************************************/
/******************************** TYPE PARALLAX *************************************/
/************************************************************************************/

if($type_background == 'parallax') {
	wp_enqueue_script('vcop-parallax-js');
	wp_enqueue_style('vcop-parallax-css');
	$output .= '<script>jQuery(function($){
		$(document).ready(function() {		
			$(\'.parallax\').each(function(){
				$(this).parallax(); 
			});
		});
	});</script>';
	
	$parallax_image_url = wp_get_attachment_image_src( $vcop_parallax_image, 'vconepage-parallax' );
	
	$el_class = $this->getExtraClass($el_class);
	
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	
	$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' '.$type_background.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.' style="background:url('.$parallax_image_url[0].')">';
	$output .= '<div class="vcop_wrapper">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div>';
	if($background_pattern == 'on' ) {
		$output .= $pattern;
	}	
	$output .= '</div>'.$this->endBlockComment('row');		
	
}

/************************************************************************************/
/******************************** TYPE VIDEO ****************************************/
/************************************************************************************/

if($type_background == 'video' || $type_background == 'video_bg_element') {

	// YOUTUBE
	if($type_video == 'youtube') {	
	wp_enqueue_script('vcop-ytplayer-js');
	wp_enqueue_style('vcop-ytplayer-css');
		if($type_background == 'video') { $container = 'body'; } else { $container = '.background-video-'.$instance.''; }
		if(empty($vcop_video_youtube_sound)) { $vcop_video_youtube_sound = 'false'; }	
	
	$el_class = $this->getExtraClass($el_class);
	
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' '.$type_background.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.'>';
	$output .= '<div class="vcop_wrapper">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div>';
	
	if($type_background == 'video') {
		$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'full' );
		$output .= '<style type="text/css">';
		$output .= 'body { background-image:url('.$cover_image_url[0].'); background-position:fixed; background-size:cover; }';
		$output .= '</style>';
	} 
	if($type_background == 'video_bg_element') {
		$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'full' );
		$output .= '<style type="text/css">';
		$output .= '.video_bg_element { position:relative; }';
		$output .= '.background-video-'.$instance.' { position:absolute; width:100%; height:100%; top:0;left:0; background-image:url('.$cover_image_url[0].'); background-position:fixed; background-size:cover;}';
		$output .= '.vc_column_container .wpb_wrapper { position:relative; z-index:1; }';
		$output .= '</style>';
	}
	
	$output .= '<div class="background-video-'.$instance.'">
        	<script>
			jQuery(document).ready(function($){
				  $(".player'.$instance.'").YTPlayer();
			});
			</script>
			<a id="P1" class="player'.$instance.'" style="display:block; margin: auto; background: rgba(0,0,0,0.5)" data-property="{videoURL:\''.$vcop_video_url.'\',containment:\''.$container.'\',startAt:0,mute:'.$vcop_video_youtube_sound.',autoPlay:true,loop:true,opacity:1,showYTLogo:false}"></a>			
	</div>';
	if($background_pattern == 'on' ) {
		$output .= $pattern;
	}
	$output .= '</div>'.$this->endBlockComment('row');		
	
	}
	// VIMEO
	if($type_video == 'vimeo') {	
		wp_enqueue_script('vcop-vimeo-js');
		$el_class = $this->getExtraClass($el_class);
	
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
		$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
		$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' '.$type_background.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.'>';
		$output .= '<div class="vcop_wrapper">';
		$output .= wpb_js_remove_wpautop($content); 
		$output .= '</div>';
			
		if($type_background == 'video') {
			$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'vconepage-parallax' );
			$output .= '<style type="text/css">';
			$output .= 'body { background-image:url('.$cover_image_url[0].'); background-position:fixed; background-size:cover; }';
			$output .= '#okplayer { height: 100%!important;left:0%!important;top:0%!important;width:100%!important; }'; 			
			$output .= '</style>';
		} 
		if($type_background == 'video_bg_element') {
			$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'vconepage-parallax' );
			$output .= '<style type="text/css">';
			$output .= '.video_bg_element { position:relative; }';
			$output .= '.background-video-'.$instance.' { position:absolute!important; width:100%; height:100%; top:0;left:0; background-image:url('.$cover_image_url[0].'); background-position:fixed; background-size:cover;}';
			$output .= '.vc_column_container .wpb_wrapper { position:relative; z-index:1; }';
			$output .= '#okplayer { z-index:0!important;height: 100%!important;left:0%!important;top:0%!important;width:100%!important; }'; 
			$output .= '</style>';
		}
		if(	$type_background == 'video_bg_element' ) {	
			$output .= '
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$(".background-video-'.$instance.'").okvideo({ source: \''.$vcop_video_vimeo_id.'\',
							volume: '.$vcop_video_vimeo_sound.',
							loop: true,
							hd:true,
							adproof: true,
							annotations: false
						 });
				});
			</script>';	
		
			$output .= '<div class="background-video-'.$instance.'"></div>';
		} else {
			$output .= '
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$.okvideo({ source: \''.$vcop_video_vimeo_id.'\',
							volume: '.$vcop_video_vimeo_sound.',
							loop: true,
							hd:true,
							adproof: true,
							annotations: false
						 });
				});
			</script>';				
			
		}
		if($background_pattern == 'on' ) {
			$output .= $pattern;
		}		
		$output .= '</div>'.$this->endBlockComment('row');
	}
	
	// LOCAL
	
	if($type_video == 'local') {
		
		$el_class = $this->getExtraClass($el_class);
	
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
		$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
		$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' '.$type_background.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.'>';
		$output .= '<div class="vcop_wrapper">';
		$output .= wpb_js_remove_wpautop($content);		
		$output .= '</div>';
			
		if($type_background == 'video') {
			$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'full' );
			$output .= '<style type="text/css">';
			$output .= 'video#bgvid {
							position: fixed; right: 0; bottom: 0;
							min-width: 100%; min-height: 100%;
							width: auto; height: auto; z-index: -100;
							background: url('.$cover_image_url[0].') no-repeat;
							background-size: cover;
							margin:0;
						}';
			$output .= '</style>';
			$output .= '<video '.$vcop_video_local_sound.' autoplay loop id="bgvid">
							<source src="'.$vcop_video_url.'">
						</video>';
		} else {
			$cover_image_url = wp_get_attachment_image_src( $vcop_video_image_cover_upload, 'full' );
			$output .= '<style type="text/css">';
			$output .= '.video_bg_element { position:relative; }';
			$output .= '.background-video-'.$instance.' { position:absolute!important; width:100%; height:100%; top:0;left:0; background-image:url('.$cover_image_url[0].'); background-position:fixed; background-size:cover;overflow:hidden;}';
			$output .= '.vc_column_container .wpb_wrapper { position:relative; z-index:1; }';
			$output .= '#bgvid { z-index:0!important;left:0%!important;top:0%!important;width:100%!important;}'; 
			$output .= '</style>';			
			$output .= '<div class="background-video-'.$instance.'"><video '.$vcop_video_local_sound.' autoplay loop id="bgvid">
							<source src="'.$vcop_video_url.'">
						</video></div>';			
			
			
			
		}
		if($background_pattern == 'on' ) {
			$output .= $pattern;
		}		
		$output .= '</div>'.$this->endBlockComment('row');
			
	}

}
/************************************************************************************/
/*********************************** TYPE SLIDE *************************************/
/************************************************************************************/

if($type_background == 'slide' || $type_background == 'slide_bg_element') {	

	wp_enqueue_script('vcop-maximage-js');
	wp_enqueue_script('vcop-cycle-js');
	wp_enqueue_style('vcop-maximage-css');
	
	$el_class = $this->getExtraClass($el_class);
	
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' '.$type_background.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.'>';
	$output .= '<div class="vcop_wrapper">';
	$output .= wpb_js_remove_wpautop($content);		
	$output .= '</div>';
	
	if($type_background == 'slide') {
	
		$output .= '<style type="text/css">
		#maximage { position:fixed!important; }
		</style>';
		$output .= '
			<script type="text/javascript" charset="utf-8">
				jQuery(document).ready(function($){
					// Trigger maximage
					jQuery(\'#maximage.slide-number-'.$instance.'\').maximage();
				});
			</script>';
		$output .= '<div id="maximage" class="slide-number-'.$instance.'">';
		$images = explode( ',', $vcop_slide_images );
			foreach ( $images as $id ) {
				$cover_image_url = wp_get_attachment_image_src( $id, 'vconepage-parallax' );
				$output .= '<img src="'.$cover_image_url[0].'">';
			}
		$output .= '</div>';

	} else {
		
		$output .= '<style type="text/css">
		.container-slider { position:absolute!important; width:100%; height:100%; top:0;left:0 }
		.slide_bg_element { position:relative; }
		.vc_column_container .wpb_wrapper { position:relative; z-index:1; }
		body .mc-cycle {z-index:0!important; }
		</style>';
		$output .= '
			<script type="text/javascript" charset="utf-8">
				jQuery(document).ready(function($){
					// Trigger maximage
					$(\'#maximage-'.$instance.'\').maximage({
					fillElement: \'.container-slider\',
					backgroundSize: \'contain\'
				});
				});
			</script>';
		$output .= '<div class="container-slider"><div id="maximage-'.$instance.'" class="slide-number-'.$instance.'">';
		$images = explode( ',', $vcop_slide_images );
			foreach ( $images as $id ) {
				$cover_image_url = wp_get_attachment_image_src( $id, 'vconepage-parallax' );
				$output .= '<img src="'.$cover_image_url[0].'">';
			}
		$output .= '</div></div>';				
		
	}
	if($background_pattern == 'on' ) {
		$output .= $pattern;
	}
	$output .= '</div>'.$this->endBlockComment('row');

}

/************************************************************************************/
/********************************** TYPE NORMAL *************************************/
/************************************************************************************/

if($type_background == 'vc_default' || empty($type_background)) {	
	$el_class = $this->getExtraClass($el_class);
	
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	$output .= '<div id="'.$vcop_row_name.'" class="'.$css_class.' '.$vcop_align_text.' '.$row_style.' vcop-row vcop-'.$instance.' '.$responsive_mode_class.' '.$vcop_row_wrapper_active.'" '.$style.'>';	
	$output .= '<div class="vcop_wrapper">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div>';
	if($background_pattern == 'on' ) {
		$output .= $pattern;
	}	

	
	$output .= '</div>'.$this->endBlockComment('row');
}


echo $output;