<?php
/* CLASS: backtotop */

class vconepage_backtotop_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_backtotopWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_backtotop', array( $this, 'vconepage_backtotop_function' ) );

    }
 
    public function integrate_vconepage_backtotopWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		
        vc_map( array(
            "name" => __("Back To Top", 'vconepage'),
            "description" => __("Add Back To Top Button", 'vconepage'),
            "base" => "vcop_backtotop",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/backtotop.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type", 'vconepage'),
                  "param_name" => "vcop_backtotop_type",
                  "value" => array(
				  					__('Icon','vconepage')  				=> 'vcop-backtotop-icon',
				  					__('Text','vconepage') 					=> 'vcop-backtotop-text',
									__('Both (Icon + Text)','vconepage') 	=> 'vcop-backtotop-icon-text'
				   ),				   			   
				),	
								
			    array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Icon", 'vconepage'),
					  "param_name" => "vcop_backtotop_icon",
					  "value" => $adtheme_icons_list,					  
					  "dependency" => array(
							'element' => 'vcop_backtotop_type',
							'value' => array( 'vcop-backtotop-icon' , 'vcop-backtotop-icon-text' )
					  ),				  
				 ),			
				 	
				array(
					'type' => 'textfield',
					'heading' => __( 'Text', 'vconepage' ),
					'param_name' => 'vcop_backtotop_text',
					'dependency' => array(
							'element' => 'vcop_backtotop_type',
							'value' => array( 'vcop-backtotop-text' , 'vcop-backtotop-icon-text' )
					),										
				),			
					
				/* STYLE */				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_backtotop_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				  "dependency" => array(
							'element' => 'vcop_backtotop_type',
							'value' => array( 'vcop-backtotop-text' , 'vcop-backtotop-icon-text' )
					),				   
				  "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_backtotop_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_backtotop_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_backtotop_icon_size",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_backtotop_type',
							'value' => array( 'vcop-backtotop-icon' , 'vcop-backtotop-icon-text' )
					),				  
				  "group" => "Style"				  			  				  
			    ),				
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Text Size (px)",'vconepage'),
				  "param_name" => "vcop_backtotop_size",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_backtotop_type',
							'value' => array( 'vcop-backtotop-text' , 'vcop-backtotop-icon-text' )
					),				  
				  "group" => "Style"				  			  				  
			    ),				
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Text / Icon Color", 'vconepage'),
					  "param_name" => "vcop_backtotop_text_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ), 				
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_backtotop_background_color",
					  "value" => '#FFF',
					  "group" => "Style"			  
				 ),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Padding",'vconepage'),
				  "param_name" => "vcop_backtotop_padding",
				  "description" => "ex 20px",
				  "value" => "20px",					  				  
				  "group" => "Style"				  			  				  
			    ),					 
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Position", 'vconepage'),
                  "param_name" => "vcop_backtotop_position",
                  "value" => array(
				  					__('Right','vconepage')  				=> 'right',
				  					__('Left','vconepage') 					=> 'left'
				   ),
				   "group" => "Style"				   			   
				),					 				
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Position Right",'vconepage'),
				  "param_name" => "vcop_backtotop_right",
				  "description" => "ex 30px",
				  "value" => "30px",
				  "dependency" => array(
							'element' => 'vcop_backtotop_position',
							'value' => array( 'right' )
					),					  				  
				  "group" => "Style"				  			  				  
			    ),											
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Position Left",'vconepage'),
				  "param_name" => "vcop_backtotop_left",
				  "description" => "ex 30px",
				  "value" => "30px",
				  "dependency" => array(
							'element' => 'vcop_backtotop_position',
							'value' => array( 'left' )
					),				  				  
				  "group" => "Style"				  			  				  
			    ),								
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Position Bottom",'vconepage'),
				  "param_name" => "vcop_backtotop_bottom",
				  "value" => "30px",
				  "description" => "ex 30px",				  
				  "group" => "Style"				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Border", 'vconepage'),
                  "param_name" => "vcop_backtotop_border",
                  "value" => array(
				  					__('Square','vconepage') => 'square',
									__('Round','vconepage')  => 'round'
									
				   )
				),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Round Pixel",'vconepage'),
				  "param_name" => "vcop_backtotop_border_round_value",
				  "description" => "ex 30px or 10%",
				  "value" => "10px",
				  "dependency" => array(
							'element' => 'vcop_backtotop_border',
							'value' => array( 'round' )
					),				  				  
				  "group" => "Style"				  			  				  
			    ),
																								  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
}
// Finally initialize code
new vconepage_backtotop_class();