<?php
/* CLASS: Counter */

class vconepage_counter_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_counterWithVC' ) );
        add_shortcode( 'vcop_counter', array( $this, 'vconepage_counter_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_counter_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_counterWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Counter", 'vconepage'),
            "description" => __("Add your counter", 'vconepage'),
            "base" => "vcop_counter",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/counter.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Counter Type", 'vconepage'),
                  "param_name" => "vcop_counter_type",
                  "value" => array(
				  					__('Counter Normal','vconepage') 						=> 'counter-normal',
									__('Counter with Icon on Top','vconepage')  			=> 'counter-icon',
									__('Counter with line','vconepage')  					=> 'counter-line',							
				   ),			   
				),		

				array(
				    'type' => 'textfield',
					'class' => '',
					'heading' => __( 'Number', 'vconepage' ),
					'param_name' => 'vcop_counter_number',
					'description' => __('Enter number value: ex 30','vconepage'),
					'admin_label' => true,
					'value' => '',														
				),	
					
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Below Counter", 'vconepage'),
                  "param_name" => "vcop_counter_text_active",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'text-show',
									__('Hidden','vconepage')  		=> 'text-hidden',
				   ),				   			   
				),
				
				array(
				    'type' => 'textfield',
					'class' => '',
					'heading' => __( 'Text', 'vconepage' ),
					'param_name' => 'vcop_counter_text',
					'value' => '',
					'dependency' => array(
							'element' => 'vcop_counter_text_active',
							'value' => array( 'text-show' )
					),															
				),						
				
			   /* ICON */		    			           
			    array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Icon", 'vconepage'),
					  "param_name" => "vcop_counter_icon",
					  "value" => $adtheme_icons_list,					  
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-icon' )
					  ),				  
				 ),
			    array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Icon Style", 'vconepage'),
					  "param_name" => "vcop_counter_icon_style",
                  	  "value" => array(
					  				__('No Background','vconepage') 		=> 'counter-icon-no-bg',
				  					__('Background Square','vconepage') 	=> 'counter-icon-square',
									__('Background Rounded','vconepage') 	=> 'counter-icon-rounded',
				      ),					  
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-icon' )
					  ),				  
				 ),
				 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_counter_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_counter_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_counter_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Text Size (px)",'vconepage'),
				  "param_name" => "vcop_counter_size",
				  "description" => "ex 15px",				  
				  "group" => "Style"				  			  				  
			    ),
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_counter_icon_size",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-icon' )
				  ),				  				  
				  "group" => "Style"				  			  				  
			    ),													
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Text Size (px)",'vconepage'),
				  "param_name" => "vcop_counter_text_size",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_counter_text_active',
							'value' => array( 'text-show' )
				  ),				  
				  "group" => "Style"				  			  				  
			    ),
			    array(
				  "type" => "colorpicker",
				  "class" => "",
				  "heading" => __("Text Color", 'vconepage'),
				  "param_name" => "vcop_counter_text_color",
				  "value" => '#FC615D',
				  "dependency" => array(
							'element' => 'vcop_counter_text_active',
							'value' => array( 'text-show' )
				  ),				  
				  "group" => "Style"				  			  				  
			    ),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Align", 'vconepage'),
                  "param_name" => "vcop_counter_align",
                  "value" => array(
				  					__('Center','vconepage') 	=> 'vcop-align-center',
									__('Left','vconepage')  	=> 'vcop-align-left',
									__('Right','vconepage')  	=> 'vcop-align-right',
				   ),
				   "group" => "Style"				   			   
				),																				
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Main Color", 'vconepage'),
					  "param_name" => "vcop_counter_main_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Icon Color", 'vconepage'),
					  "param_name" => "vcop_counter_icon_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-icon' )
					  ),					  
					  "group" => "Style"					  					  				  
				  ),				 
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Icon Color", 'vconepage'),
					  "param_name" => "vcop_counter_background_icon_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-icon' )
					  ),					  
					  "group" => "Style"					  					  				  
				  ),				   
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Line Color", 'vconepage'),
					  "param_name" => "vcop_counter_line_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'counter-line' )
					  ),					  
					  "group" => "Style"					  					  				  
				  ),			  
				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_counter_loadCssAndJs() {	
		wp_register_style( 'vcop-counter',  AD_VCOP_URL . 'assets/css/counter.css' );
		wp_register_script( 'vcop-counter-js',  AD_VCOP_URL . 'assets/js/jquery.countTo.js' , array('jquery'), '', true);
    }	
}
// Finally initialize code
new vconepage_counter_class();