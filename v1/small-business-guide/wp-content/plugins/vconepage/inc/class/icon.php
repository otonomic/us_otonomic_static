<?php
/* CLASS: icon */

class vconepage_icon_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_iconWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_icon', array( $this, 'vconepage_icon_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_icon_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_iconWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("Icon", 'vconepage'),
            "description" => __("Add your icon", 'vconepage'),
            "base" => "vcop_icon",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/icon.png'),
			"admin_enqueue_css" => array(plugins_url('vconepage/assets/css/general/fonts.css')),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(             
			   array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Icon", 'vconepage'),
                  "param_name" => "vcop_icon",
				  "admin_label" => true,
                  "value" => $adtheme_icons_list,
               ),			   				  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Float", 'vconepage'),
                  "param_name" => "vcop_icon_float",
                  "value" => array(
									__('Left','vconepage')  	=> 'left',
									__('Right','vconepage') 	=> 'right',
									__('None','vconepage') 	=> 'none'
				   )
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Align", 'vconepage'),
                  "param_name" => "vcop_icon_align",
                  "value" => array(
				  					__('Center','vconepage') 	=> 'vcop-icon-align-center',
									__('Left','vconepage')  	=> 'vcop-icon-align-left',
									__('Right','vconepage') 	=> 'vcop-icon-align-right'
									
				   ),
				  "dependency" => array(
							'element' => 'vcop_icon_float',
							'value' => array( 'none' )
				  ),				   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_link",
                  "value" => array(
									__('off','vconepage') => 'off',
									__('On','vconepage') 	  => 'on'
				   )
				),	
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Link Url",'vconepage'),
				  "param_name" => "vcop_link_url",
				  "description" => "Add link url ex: http://www.ad-theme.com",					  
				  'dependency' => array(
						'element' => 'vcop_link',
						'value' => array( 'on' )
				  ),					  
			  ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link Url Target", 'vconepage'),
                  "param_name" => "vcop_target",
                  "value" => array(
									__('Self (Same windows)','vconepage') => '_self',
									__('New Windows','vconepage') 	  => '_blank'
				   ),
				  'dependency' => array(
						'element' => 'vcop_link',
						'value' => array( 'on' )
				  ),					   
				),	
			  // STYLE 	
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_size",
				  "description" => "ex 15",
				  "group" => "Style"				  
			  ),			                
			  array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Icon Color", 'vcfg'),
                  "param_name" => "vcop_icon_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Icon Hover Color", 'vcfg'),
                  "param_name" => "vcop_icon_over_color",
                  "value" => '#FC615D',
				  "group" => "Style"
              ),					  		  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Margin",'vconepage'),
				  "param_name" => "vcop_margin",
				  "description" => "ex 2px or 2px 2px 2px 2px",
				  "group" => "Style"				  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Padding",'vconepage'),
				  "param_name" => "vcop_padding",
				  "description" => "ex 2px or 2px 2px 2px 2px",
				  "group" => "Style"				  
			  ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Background", 'vconepage'),
                  "param_name" => "vcop_background_active",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Style"
				),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vcfg'),
					  "param_name" => "vcop_background_color",
					  "value" => '#FC615D',
					  "group" => "Style",
					  "dependency" => array(
							'element' => 'vcop_background_active',
							'value' => array( 'on' )
					  ),					  
				),
				array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Background Type", 'vconepage'),
					  "param_name" => "vcop_background_type",
					  "value" => array(
										__('Square','vconepage') 	=> 'square',
										__('Round','vconepage') 	=> 'round'
										
					   ),
					  "dependency" => array(
							'element' => 'vcop_background_active',
							'value' => array( 'on' )
					  ),
					  "group" => "Style",					  
				),				
				array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Border", 'vconepage'),
					  "param_name" => "vcop_border_active",
					  "value" => array(
										__('Off','vconepage') 	  => 'off',
										__('On','vconepage') => 'on'
										
					   ),
					  "group" => "Style",					  
				),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Border Color", 'vcfg'),
					  "param_name" => "vcop_border_color",
					  "value" => '#FC615D',
					  "group" => "Style",
					  "dependency" => array(
							'element' => 'vcop_border_active',
							'value' => array( 'on' )
					  ),					  
				),
			  array(
				 	 "type" => "textfield",
				  	 "class" => "",
				  	 "heading" => __("Border With",'vconepage'),
				  	 "param_name" => "vcop_border_width",
				  	 "description" => "ex 2px",
				  	 "group" => "Style",
					 "dependency" => array(
							'element' => 'vcop_border_active',
							'value' => array( 'on' )
					 ),				  
			  ),				  
				array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Border Style", 'vconepage'),
					  "param_name" => "vcop_border_style",
					  "value" => array(
				  					__('Solid','vconepage')  		=> 'solid',
				  					__('Dotted','vconepage') 		=> 'dotted',
									__('Dashed','vconepage') 		=> 'dashed',
									__('Double','vconepage') 		=> 'double',
									__('Inset','vconepage') 		=> 'inset',
									__('Outset','vconepage') 		=> 'outset'
										
					   ),
					  "dependency" => array(
							'element' => 'vcop_border_active',
							'value' => array( 'on' )
					  ),
					  "group" => "Style",					  
				),
				array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Background Type", 'vconepage'),
					  "param_name" => "vcop_background_border_type",
					  "value" => array(
										__('Square','vconepage') 	=> 'square',
										__('Round','vconepage') 	=> 'round'
										
					   ),
					  "dependency" => array(
							'element' => 'vcop_border_active',
							'value' => array( 'on' )
					  ),
					  "group" => "Style",					  
				),							  																					  
			  // ANIMATION											  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_icon_loadCssAndJs() {
		wp_register_style( 'vcop-icon',  AD_VCOP_URL . 'assets/css/icon.css' );					
    }	
}
// Finally initialize code
new vconepage_icon_class();