<?php
/* CLASS: Chart */

class vconepage_chart_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_chartWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_chart', array( $this, 'vconepage_chart_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_chart_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_chartWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("Chart", 'vconepage'),
            "description" => __("Pie Chart/Bar Chart", 'vconepage'),
            "base" => "vcop_chart",
            "class" => "",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/chart.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type", 'vconepage'),
                  "param_name" => "vcop_type",
				  "admin_label" => true,
                  "value" => array(
									__('Pie Chart (Circle)','vconepage') => 'pie_chart',
									__('Progress Bar','vconepage') 	  => 'progress_bar'
				   )
				),
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Bar Color", 'vcfg'),
                  "param_name" => "vcop_bar_color",
                  "value" => '#FC615D'
              ),
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Track Color", 'vcfg'),
                  "param_name" => "vcop_track_color",
                  "value" => '#FC615D'
              ),			  						  
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Percent", 'vconepage'),
                  "param_name" => "vcop_percent",
                  "value" => array(
				  					'1',
									'2',
									'3',
									'4',
									'5',
									'6',
									'7',
									'8',
									'9',
									'10',
									'11',
									'12',
									'13',
									'14',
									'15',
									'16',
									'17',
									'18',
									'19',
									'20',
									'21',
				  					'22',
									'23',
									'24',
									'25',
									'26',
									'27',
									'28',
									'29',
									'30',
									'31',
									'32',
									'33',
									'34',
									'35',
									'36',
									'37',
									'38',
									'39',
									'40',
									'41',
									'42',
				  					'43',
									'44',
									'45',
									'46',
									'47',
									'48',
									'49',
									'50',
									'51',
									'52',
									'53',
									'54',
									'55',
									'56',
									'57',
									'58',
									'59',
									'60',
									'61',
									'62',
									'63',
				  					'64',
									'65',
									'66',
									'67',
									'68',
									'69',
									'70',
									'71',
									'72',
									'73',
									'74',
									'75',
									'76',
									'77',
									'78',
									'79',
									'80',
									'81',
									'82',
									'83',
									'84',
				  					'85',
									'86',
									'87',
									'88',
									'89',
									'90',
									'91',
									'92',
									'93',
									'94',
									'95',
									'96',
									'97',
									'98',
									'99',
									'100'																																																																		
				   )
               ),			  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Width",'vconepage'),
				  "param_name" => "vcop_bar_width",
				  "description" => __('example: 400','vconepage'),
				  'dependency' => array(
						'element' => 'vcop_type',
						'value' => array( 'pie_chart' )
				  ),				  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Width",'vconepage'),
				  "param_name" => "vcop_pie_width",
				  "description" => __('example: 400px or 100%','vconepage'),
				  'dependency' => array(
						'element' => 'vcop_type',
						'value' => array( 'progress_bar' )
				  ),				  
			  ),
              array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Style", 'vconepage'),
                  "param_name" => "vcop_style",
                  "value" => array(
									'Style 1' => 'style1',
									'Style 2' => 'style2'																	
				   ),
				  'dependency' => array(
						'element' => 'vcop_type',
						'value' => array( 'progress_bar' )
				  ),				   			   				   
              ),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			    
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_chart_loadCssAndJs() {
		
		// Main
		wp_register_style( 'vcop-chart',  AD_VCOP_URL . 'assets/css/charts.css' );		
		wp_register_style( 'vcop-normalize',  AD_VCOP_URL . 'assets/css/normalize.css' );
		wp_register_style( 'vcop-animations',  AD_VCOP_URL . 'assets/css/animations.min.css' );
		wp_register_script( 'vcop-chart-js',  AD_VCOP_URL . 'assets/js/jquery.easypiechart.min.js' , array('jquery'), '', true);

					
    }	
}
// Finally initialize code
new vconepage_chart_class();