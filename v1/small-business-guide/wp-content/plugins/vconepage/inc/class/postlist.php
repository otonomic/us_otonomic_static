<?php
/* CLASS: postlist */

class vconepage_postlist_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_postlistWithVC' ) );
        add_shortcode( 'vcop_postlist', array( $this, 'vconepage_postlist_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_postlist_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_postlistWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Post list", 'vconepage'),
            "description" => __("Add your postlist", 'vconepage'),
            "base" => "vcop_postlist",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/postlist.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(		
							
				// POSTS									
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Title", 'vconepage'),
                  "param_name" => "vcop_postlist_show_title",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Image", 'vconepage'),
                  "param_name" => "vcop_postlist_show_image",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Excerpt", 'vconepage'),
                  "param_name" => "vcop_postlist_show_excerpt",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),					   			   			   
				),
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Charpter Excerpt",'vconepage'),
				  "param_name" => "vcop_postlist_excerpt_number",
				  "description" => "ex: 150. Leave Empty for default value",
				  "dependency" => array(
							'element' => 'vcop_postlist_show_excerpt',
							'value' => array( 'true' )
				  ),				  					  				  					  				  			  				  
			    ),											
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Button", 'vconepage'),
                  "param_name" => "vcop_postlist_show_button",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  => 'false',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_postlist_source',
							'value' => array( 'vcop_posts' )
					),				   			   
				),					


				// DISPLAY
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Style Type", 'vconepage'),
                  "param_name" => "vcop_postlist_style_type",
                  "value" => array(
				  					__('Circle','vconepage') 		=> 'vcop-circle',
									__('Square','vconepage')  		=> 'vcop-square',
				  					__('Upper Roman','vconepage') 	=> 'vcop-upper-roman',
									__('Lower Alpha','vconepage')  => 'vcop-lower-alpha',
				  					__('Decimal','vconepage') 		=> 'vcop-decimal',
									__('None','vconepage')  		=> 'vcop-none',																		
				   ),
				   "group" => "Display",				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Date", 'vconepage'),
                  "param_name" => "vcop_postlist_date",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'true',
									__('Hidden','vconepage')  		=> 'false',
				   ),
				   "group" => "Display",				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Date Format", 'vconepage'),
                  "param_name" => "vcop_postlist_date_format",
                  "value" => array(
				  					__('November 6, 2010 12:50 am','vconepage') 		=> 'F j, Y g:i a',
									__('November 6, 2010','vconepage')  				=> 'F j, Y',
				  					__('November, 2010','vconepage') 					=> 'F, Y',
									__('12:50 am','vconepage')  						=> 'g:i a',
				  					__('12:50:48 am','vconepage') 						=> 'g:i:s a',
									__('Saturday, November 6th, 2010','vconepage')  	=> 'l, F jS, Y',
				  					__('Nov 6, 2010 @ 0:50','vconepage') 				=> 'M j, Y @ G:i',
									__('2010/11/06 at 12:50 AM','vconepage')  			=> 'Y/m/d \a\t g:i A',
				  					__('2010/11/06 at 12:50am','vconepage') 			=> 'Y/m/d \a\t g:ia',
									__('2010/11/06 12:50:48 AM','vconepage')  			=> 'Y/m/d g:i:s A',
									__('2010/11/06','vconepage')  						=> 'Y/m/d',																																														
				   ),
				  "group" => "Display",
				  "dependency" => array(
							'element' => 'vcop_postlist_date',
							'value' => array( 'true' )
					),				   				   					   					   				   			   
				),	

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Comments", 'vconepage'),
                  "param_name" => "vcop_postlist_comments",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'true',
									__('Hidden','vconepage')  		=> 'false',
				   ),
				   "group" => "Display",				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Author", 'vconepage'),
                  "param_name" => "vcop_postlist_author",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'true',
									__('Hidden','vconepage')  		=> 'false',
				   ),
				   "group" => "Display",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Category", 'vconepage'),
                  "param_name" => "vcop_postlist_category",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'true',
									__('Hidden','vconepage')  		=> 'false',
				   ),
				   "group" => "Display",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Views", 'vconepage'),
                  "param_name" => "vcop_postlist_views",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'true',
									__('Hidden','vconepage')  		=> 'false',
				   ),
				   "group" => "Display",				   				   			   
				),

				// QUERY 
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Source", 'vconepage'),
                  "param_name" => "vcop_query_source",
                  "value" => array(
				  					__('Wordpress Posts','vconepage') 		=> 'wp_posts',
									__('Custom Posts Type','vconepage')  	=> 'wp_custom_posts_type',
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("All Posts/Sticky posts", 'vconepage'),
                  "param_name" => "vcop_query_sticky_posts",
                  "value" => array(
				  					__('All Posts','vconepage') 		 	=> 'allposts',
									__('Only Sticky Posts','vconepage')  	=> 'onlystickyposts',
				   ),
				  "group" => "Query",					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => __("Select Post Type Source",'vconepage'),
				  "param_name" => "vcop_query_posts_type",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_source',
							'value' => array( 'wp_custom_posts_type' )
				  ),					  				  					  				  			  				  
			    ),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Categories",'vconepage'),
				  "param_name" => "vcop_query_categories",
				  "description" => "Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)",
				  "group" => "Query",				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order", 'vconepage'),
                  "param_name" => "vcop_query_order",
                  "value" => array(
				  					__('DESC','vconepage')  	=> 'DESC',
				  					__('ASC','vconepage') 		=> 'ASC'
									
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order By", 'vconepage'),
                  "param_name" => "vcop_query_orderby",
                  "value" => array(
				  					__('Date','vconepage') 			=> 'date',
									__('ID','vconepage')  				=> 'ID',
				  					__('Author','vconepage') 			=> 'author',
									__('Title','vconepage')  			=> 'title',
				  					__('Name','vconepage') 			=> 'name',
									__('Modified','vconepage')  		=> 'modified',
				  					__('Parent','vconepage') 			=> 'parent',
									__('Rand','vconepage')  			=> 'rand',
									__('Comments Count','vconepage')  	=> 'comment_count',
									__('None','vconepage')  			=> 'none',																																													
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination", 'vconepage'),
                  "param_name" => "vcop_query_pagination",
                  "value" => array(
				  					__('Yes','vconepage') 		=> 'yes',
									__('No','vconepage')  		=> 'no',
				   ),
				   "group" => "Query"				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts",'vconepage'),
				  "param_name" => "vcop_query_number",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'no' )
				  ),					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination Type", 'vconepage'),
                  "param_name" => "vcop_query_pagination_type",
                  "value" => array(
				  					__('Numeric','vconepage') 		=> 'numeric',
									__('Normal','vconepage')  		=> 'normal',
				  ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),				   				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts For Page",'vconepage'),
				  "param_name" => "vcop_query_posts_for_page",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),					  				  					  				  			  				  
			    ),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_postlist_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_postlist_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_postlist_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_postlist_title_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),									  				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_postlist_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_postlist_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_postlist_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_postlist_loadCssAndJs() {	
		wp_register_style( 'vcop-postlist',  AD_VCOP_URL . 'assets/css/postlist.css' );
		wp_register_style( 'vcop-magnific-popup',  AD_VCOP_URL . 'assets/css/magnific-popup.css' );
		wp_register_script( 'vcop-magnific-popup-js',  AD_VCOP_URL . 'assets/js/jquery.magnific-popup.js' , array('jquery'), '', true);				
    }	
}
// Finally initialize code
new vconepage_postlist_class();