<?php
/* CLASS: Team */

class vconepage_team_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_teamWithVC' ) );
        add_shortcode( 'vcop_team', array( $this, 'vconepage_team_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_team_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_teamWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Team", 'vconepage'),
            "description" => __("Add your user team", 'vconepage'),
            "base" => "vcop_team",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/team.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Team Type", 'vconepage'),
                  "param_name" => "vcop_team_type",
                  "value" => array(
				  					__('Grid Style 1','vconepage') 		=> 'vcop-team-grid-style1',
									__('Grid Style 2','vconepage')  		=> 'vcop-team-grid-style2',
									__('Carousel Style 1','vconepage')  	=> 'vcop-team-carousel-style1',							
									__('Carousel Style 2','vconepage')  	=> 'vcop-team-carousel-style2'																
				   ),			   
				),		

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Columns", 'vconepage'),
                  "param_name" => "vcop_team_columns",
                  "value" => array(
				  					__('1','vconepage') 	=> '1',
									__('2','vconepage')  	=> '2',
				  					__('3','vconepage') 	=> '3',
									__('4','vconepage')  	=> '4',
				  					__('5','vconepage') 	=> '5',
									__('6','vconepage')  	=> '6',
				  					__('7','vconepage') 	=> '7',
									__('8','vconepage')  	=> '8',																											
				   ),
				  "dependency" => array(
							'element' => 'vcop_team_type',
							'value' => array( 'vcop-team-grid-style1', 'vcop-team-grid-style2' )
				  ),				   				   			   
				),
					
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Excerpt", 'vconepage'),
                  "param_name" => "vcop_team_excerpt",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'excerpt-show',
									__('Hidden','vconepage')  		=> 'excerpt-hidden',
				   ),				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Experpt",'vconepage'),
				  "param_name" => "vcop_team_excerpt_number",
				  "description" => "ex 50. Leave empty for default value",				  
				  "dependency" => array(
							'element' => 'vcop_team_excerpt',
							'value' => array( 'excerpt-show' )
				  ),					  				  			  				  
			    ),	
				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Title", 'vconepage'),
                  "param_name" => "vcop_team_title",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'title-show',
									__('Hidden','vconepage')  		=> 'title-hidden',
				   ),				   			   
				),										
				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Sub Title", 'vconepage'),
                  "param_name" => "vcop_team_subtitle",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'subtitle-show',
									__('Hidden','vconepage')  		=> 'subtitle-hidden',
				   ),				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Social", 'vconepage'),
                  "param_name" => "vcop_team_social",
                  "value" => array(
				  					__('Show','vconepage') 		=> 'social-show',
									__('Hidden','vconepage')  		=> 'social-hidden',
				   ),				   			   
				),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_team_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_team_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_team_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
				
				/* EXCERPT */
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Excerpt Text Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_team_excerpt_size",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",
				  "dependency" 	=> array(
							'element' => 'vcop_team_excerpt',
							'value' => array( 'excerpt-show' )
				  ),				  				  			  				  
			    ),	
				array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Excerpt Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_team_excerpt_color",
					  "value" 		 	=> '#333',
					  "group" 		 	=> "Style",
				  		"dependency" 	=> array(
							'element' 		=> 'vcop_team_excerpt',
							'value' 		=> array( 'excerpt-show' )
				  ),					  			  
				 ),
				
				/* TITLE */
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title Text Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_team_title_size",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",
				  "dependency" 	=> array(
							'element' => 'vcop_team_title',
							'value' => array( 'title-show' )
				  ),				  				  			  				  
			    ),	
				array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Title Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_team_title_color",
					  "value" 		 	=> '#999',
					  "group" 		 	=> "Style",
				  		"dependency" 	=> array(
							'element' 		=> 'vcop_team_title',
							'value' 		=> array( 'title-show' )
				  ),					  			  
				 ),
				
				/* SUBTITLE */
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Subtitle Text Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_team_subtitle_size",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",
				  "dependency" 	=> array(
							'element' => 'vcop_team_subtitle',
							'value' => array( 'subtitle-show' )
				  ),				  				  			  				  
			    ),	
				array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Subtitle Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_team_subtitle_color",
					  "value" 		 	=> '#999',
					  "group" 		 	=> "Style",
				  		"dependency" 	=> array(
							'element' 		=> 'vcop_team_subtitle',
							'value' 		=> array( 'subtitle-show' )
				  ),					  			  
				 ),				

				/* SOCIAL */
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Social Text Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_team_social_size",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",
				  "dependency" 	=> array(
							'element' => 'vcop_team_social',
							'value' => array( 'social-show' )
				  ),				  				  			  				  
			    ),	
				array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Social Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_team_social_color",
					  "value" 		 	=> '#999',
					  "group" 		 	=> "Style",
				  		"dependency" 	=> array(
							'element' 		=> 'vcop_team_social',
							'value' 		=> array( 'social-show' )
				  ),					  			  
				 ),				
				
				 /* BACKGROUND */ 																						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_team_background_color",
					  "value" => '#FFF',				  
					  "group" => "Style"					  					  				  
				  ),
				 
				 /* LINE STYLE 1 */ 				   
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Line Color", 'vconepage'),
					  "param_name" => "vcop_team_line_color",
					  "value" => '#999',
					  "dependency" => array(
							'element' => 'vcop_counter_type',
							'value' => array( 'vcop-team-grid-style1' , 'vcop-team-carousel-style1' )
					  ),					  
					  "group" => "Style"					  					  				  
				  ),			  

				 /* CAROUSEL */ 				   	
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Number Item Show", 'vconepage'),
                  "param_name" => "vcop_team_carousel_item",
                  "value" => array(
				  					__('1','vconepage') 	=> '1',
									__('2','vconepage')  	=> '2',
				  					__('3','vconepage') 	=> '3',
									__('4','vconepage')  	=> '4',
				  					__('5','vconepage') 	=> '5',
									__('6','vconepage')  	=> '6',
				  					__('7','vconepage') 	=> '7',
									__('8','vconepage')  	=> '8',																											
				   ),
				  "dependency" => array(
							'element' => 'vcop_team_type',
							'value' => array( 'vcop-team-carousel-style1', 'vcop-team-carousel-style2' )
				  ),
				  "group" => "Carousel Settings"				   				   			   
				),
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Navigation", 'vconepage'),
                  "param_name" => "vcop_team_carousel_navigation",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true', 
									__('Hidden','vconepage')  	=> 'false'																										
				   ),
				  "dependency" => array(
							'element' => 'vcop_team_type',
							'value' => array( 'vcop-team-carousel-style1', 'vcop-team-carousel-style2' )
				  ),
				  "group" => "Carousel Settings"				   				   			   
				),
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination", 'vconepage'),
                  "param_name" => "vcop_team_carousel_pagination",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true', 
									__('Hidden','vconepage')  	=> 'false'																										
				   ),
				  "dependency" => array(
							'element' => 'vcop_team_type',
							'value' => array( 'vcop-team-carousel-style1', 'vcop-team-carousel-style2' )
				  ),
				  "group" => "Carousel Settings"				   				   			   
				),
				array(
					  "type" => "textfield",
					  "class" => "",
					  "heading" => __("Autoplay (ms)",'vconepage'),
					  "param_name" => "vcop_team_carousel_autoplay",
					  "description" => "ex 1000 (Insert 0 if you want disable autoplay)",					  
					  "dependency" => array(
								'element' => 'vcop_team_type',
								'value' => array( 'vcop-team-carousel-style1', 'vcop-team-carousel-style2' )
					  ),
					  "group" => "Carousel Settings"					  
				),	
				array(
					  "type" => "textfield",
					  "class" => "",
					  "heading" => __("Item Padding",'vconepage'),
					  "param_name" => "vcop_team_carousel_item_padding",
					  "description" => "ex 10px or 2px 2px 2px 2px (Leave empty for no padding)",					  
					  "dependency" => array(
								'element' => 'vcop_team_type',
								'value' => array( 'vcop-team-carousel-style1', 'vcop-team-carousel-style2' )
					  ),
					  "group" => "Carousel Settings"					  
				),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_team_loadCssAndJs() {	
		wp_register_style( 'vcop-team',  AD_VCOP_URL . 'assets/css/team.css' );
		wp_register_script( 'vcop-team-js',  AD_VCOP_URL . 'assets/js/jquery.countTo.js' , array('jquery'), '', true);
		wp_register_style( 'vcop-owl-carousel',  AD_VCOP_URL . 'assets/css/owl.carousel.css' );
		wp_register_style( 'vcop-owl-theme',  AD_VCOP_URL . 'assets/css/owl.theme.css' );
		wp_register_script( 'vcop-owl-carousel-js',  AD_VCOP_URL . 'assets/js/owl.carousel.min.js' , array('jquery'), '', true);
    }	
}
// Finally initialize code
new vconepage_team_class();
