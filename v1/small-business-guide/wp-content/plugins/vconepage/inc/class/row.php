<?php
/* CLASS: Row Extended */

add_action( 'wp_enqueue_scripts', 'vconepage_parallax_loadCssAndJs');

function vconepage_parallax_loadCssAndJs() {
	 wp_register_script( 'vcop-parallax-js',  AD_VCOP_URL . 'assets/js/jquery.parallax-1.1.3.js' , array('jquery'), '', true);
	 wp_register_style( 'vcop-parallax-css',  AD_VCOP_URL . 'assets/css/parallax.css' );
	 // VIDEO BG - YOUTUBE
	 wp_register_script( 'vcop-ytplayer-js',  AD_VCOP_URL . 'assets/js/jquery.mb.YTPlayer.js');
	 wp_register_style( 'vcop-ytplayer-css',  AD_VCOP_URL . 'assets/css/YTPlayer.css' );	 
	 // VIDEO BG - VIMEO
	 wp_register_script( 'vcop-vimeo-js',  AD_VCOP_URL . 'assets/js/okvideo.min.js');	
	 // SLIDE BACKGROUND
	 wp_register_script( 'vcop-maximage-js',  AD_VCOP_URL . 'assets/js/jquery.maximage.js');
	 wp_register_script( 'vcop-cycle-js',  AD_VCOP_URL . 'assets/js/jquery.cycle.all.js');
	 wp_register_style( 'vcop-maximage-css',  AD_VCOP_URL . 'assets/css/jquery.maximage.css' );
	 // ROW CSS
	 wp_register_style( 'vcop-row',  AD_VCOP_URL . 'assets/css/row.css' );	 
}

vc_add_param("vc_row", array(
	"type" => "textfield",
	"class" => "",
	"heading" => "Row Name",
	"description" => "Row Name",
	"param_name" => "vcop_row_name",
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Wrapper",
	"param_name" => "vcop_row_wrapper_active",
	"value" => array(
		"On" 	=> "vcop_row_wrapper", 
		"Off" 	=> "off",	
	)
));
vc_add_param("vc_row", array(
	"type" => "textfield",
	"class" => "",
	"heading" => "Wrapper Px",
	"description" => "Enter value wrapper in px for example: 1200px",
	"param_name" => "vcop_row_wrapper_value",
	'dependency' => array(
		'element' => 'vcop_row_wrapper_active',
		'value' => array( 'vcop_row_wrapper' )
	),	
));

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Type",
	"param_name" => "type_background",
	"value" => array(
		"Normal (Vc Default)" => "vc_default", 
		"Parallax" => "parallax",
		"Video Background Body" => "video",
		"Video Background Element" => "video_bg_element",
		"Slide Background Body" => "slide",
		"Slide Background Element" => "slide_bg_element",		
	)
));
vc_add_param("vc_row", array(
	"type" => "attach_image",
	"class" => "",
	"heading" => "Upload Image Parallax",
	"description" => "If you add image in Design option, you need to remove this image for make parallax effect. And set Theme default",
	"param_name" => "vcop_parallax_image",
	'dependency' => array(
		'element' => 'type_background',
		'value' => array( 'parallax' )
	),
	"group" => "Background Type",	
));	
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Type",
	"param_name" => "type_video",
	"value" => array(
		"Local" => "local",
		"Youtube" => "youtube", 
		"Vimeo" => "vimeo",		
	),
	'dependency' => array(
		'element' => 'type_background',
		'value' => array( 'video','video_bg_element' )
	),	
));
vc_add_param("vc_row", array(			  
		"type" => "textfield",
		"group" => "Background Type",
		"class" => "",
		"heading" => __("Url Video",'vconepage'),
		"param_name" => "vcop_video_url",
		'dependency' => array(
			'element' => 'type_video',
			'value' => array( 'youtube','local' )
		),				  
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Youtube Sound",
	"param_name" => "vcop_video_youtube_sound",
	"value" => array(
		"On" => "false",
		"Off" => "true",		
	),
	'dependency' => array(
		'element' => 'type_video',
		'value' => array( 'youtube' )
	),	
));


// VIMEO
vc_add_param("vc_row", array(			  
		"type" => "textfield",
		"group" => "Background Type",
		"class" => "",
		"heading" => __("Vimeo Video ID",'vconepage'),
		"param_name" => "vcop_video_vimeo_id",
		'dependency' => array(
			'element' => 'type_video',
			'value' => array( 'vimeo' )
		),				  
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Vimeo Sound",
	"param_name" => "vcop_video_vimeo_sound",
	"value" => array(
		"On" => "90",
		"Off" => "0",		
	),
	'dependency' => array(
		'element' => 'type_video',
		'value' => array( 'vimeo' )
	),	
));

// LOCAL
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Local Sound",
	"param_name" => "vcop_video_local_sound",
	"value" => array(
		"On" => "",
		"Off" => "mute",		
	),
	'dependency' => array(
		'element' => 'type_video',
		'value' => array( 'local' )
	),	
));

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Image Cover",
	"param_name" => "vcop_video_image_cover",
	"value" => array(
		"Off" => "off",
		"On" => "on"				
	),
	'dependency' => array(
		'element' => 'type_background',
		'value' => array( 'video','video_bg_element' )
	),	
));	
vc_add_param("vc_row", array(
	"type" => "attach_image",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Upload Image Cover",
	"param_name" => "vcop_video_image_cover_upload",
	'dependency' => array(
		'element' => 'vcop_video_image_cover',
		'value' => array( 'on' )
	),	
));	

// SLIDE
vc_add_param("vc_row", array(
	"type" => "attach_images",
	"group" => "Background Type",
	"class" => "",
	"heading" => "Upload Images",
	"param_name" => "vcop_slide_images",
	'dependency' => array(
		'element' => 'type_background',
		'value' => array( 'slide','slide_bg_element' )
	),	
));	

include('pattern-list.php');

// ADD PATTERN
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Pattern",
	"param_name" => "background_pattern",
	"value" => array(		
		"Off" => "off",	
		"On" => "on" 	
	)
));

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Pattern Type",
	"param_name" => "background_pattern_type",
	"value" => array(		
		"Image" => "image",	
		"Color" => "color" 	
	),
	'dependency' => array(
		'element' => 'background_pattern',
		'value' => array( 'on' )
	),
));

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Select Pattern Image",
	"param_name" => "background_pattern_image",
	"value" => $pattern_list,
	'dependency' => array(
		'element' => 'background_pattern_type',
		'value' => array( 'image' )
	),	
));

vc_add_param("vc_row", array(
	"type" => "colorpicker",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Pattern Color",
	"param_name" => "background_pattern_color",
	"value" => '#FC615D',
	'dependency' => array(
		'element' => 'background_pattern_type',
		'value' => array( 'color' )
	),	
));

vc_add_param("vc_row", array(
	"type" => "textfield",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Pattern Opacity",
	"description" => "Enter value between 0.1 - 1 (ie 0.5)",
	"param_name" => "background_pattern_image_opacity",
	'dependency' => array(
		'element' => 'background_pattern_type',
		'value' => array( 'image' )
	),	
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Padding",
	"description" => "Select on if in design options you used a padding",	
	"param_name" => "background_pattern_padding_active",
	"value" => array(		
		"Off" => "off",	
		"On" => "on" 	
	),
	'dependency' => array(
		'element' => 'background_pattern',
		'value' => array( 'on' )
	),			
));
vc_add_param("vc_row", array(
	"type" => "textfield",
	"group" => "Pattern",
	"class" => "",
	"heading" => "Padding Left",
	"description" => "Enter the same Padding Left if you added it in design options (example: 100)",
	"param_name" => "background_pattern_padding_left",
	'dependency' => array(
		'element' => 'background_pattern_padding_active',
		'value' => array( 'on' )
	),	
));

// STYLE
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Advanced Style",
	"class" => "",
	"heading" => "Height Row",
	"param_name" => "vcop_height_row",
	"value" => array(
		"Content (Normal)" => "vcop_normal",
		"100%" => "vcop_full",		
	),	
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Advanced Style",
	"class" => "",
	"heading" => "Padding and Margin Beetween Rows",
	"param_name" => "vcop_padding",
	"value" => array(
		"Default" 	 	=> "vcop_padding_default",
		"Null (zero)" 	=> "vcop_padding_null",		
	),	
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Advanced Style",
	"class" => "",
	"heading" => "Align Text On Row",
	"param_name" => "vcop_align_text",
	"value" => array(
		"Default"		=> "vcop_align_default",
		"Center" 	 	=> "vcop_align_center",
		"Left" 			=> "vcop_align_left",
		"Right" 		=> "vcop_align_right",		
	),	
));
vc_add_param("vc_row", array(
	"type" => "dropdown",
	"group" => "Advanced Style",
	"class" => "",
	"heading" => "Responsive Mode",
	"description" => "Reset all padding in responsive mode for better display and use default value",
	"param_name" => "responsive_mode",
	"value" => array(	
		"Off" 	=> "false",	
		"On"		=> "true",	
	),	
));				