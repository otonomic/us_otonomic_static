<?php
/* CLASS: contactform */

class vconepage_contactform_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_contactformWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_contactform', array( $this, 'vconepage_contactform_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_contactform_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_contactformWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('animate-list.php');
		
        vc_map( array(
            "name" => __("Contact Form 7", 'vconepage'),
            "description" => __("Add Contact Form", 'vconepage'),
            "base" => "vcop_contactform",
            "class" => "",
            "icon" => plugins_url('vconepage/assets/img/contactform.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(			  			
				array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Contact Form 7 ID",'vconepage'),
				  "description" => 'enter your contact form code',
				  "param_name" => "vcop_contactform_shortcodes_id",
				  'admin_label' => true
			  ),
			  /* STYLE */	
			   array(
				 "type" => "dropdown",
				 "class" => "",
				 "heading" => __("Border Input", 'vconepage'),
				 "param_name" => "vcop_contactform_border_type",
             	 "value" => array(
				  					__('Square','vconepage') => 'vcop-cf7-border-square',
									__('Round','vconepage')  => 'vcop-cf7-border-round',
				 ),				  
				),			  			  	
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Text Color", 'vconepage'),
					  "param_name" => "vcop_contactform_text_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ), 					
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Field Color", 'vconepage'),
					  "param_name" => "vcop_contactform_background_field_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Container Color", 'vconepage'),
					  "param_name" => "vcop_contactform_background_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),				  					
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Border Color", 'vconepage'),
					  "param_name" => "vcop_contactform_border_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ), 					
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Active Color", 'vconepage'),
					  "param_name" => "vcop_contactform_active_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),
				 						  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),					  			  			  		  			  
			)  
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_contactform_loadCssAndJs() {

		// Main
		wp_register_style( 'vcop-contactform',  AD_VCOP_URL . 'assets/css/contactform7.css' );
			
    }	
}
// Finally initialize code
new vconepage_contactform_class();