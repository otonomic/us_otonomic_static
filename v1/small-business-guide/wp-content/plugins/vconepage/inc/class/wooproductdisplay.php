<?php
/* CLASS: wooproductdisplay */

class vconepage_wooproductdisplay_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_wooproductdisplayWithVC' ) );
        add_shortcode( 'vcop_wooproductdisplay', array( $this, 'vconepage_wooproductdisplay_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_wooproductdisplay_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_wooproductdisplayWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Woo Product Display", 'vconepage'),
            "description" => __("Woocommerce Product Display", 'vconepage'),
            "base" => "vcop_wooproductdisplay",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/wooproductdisplay.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(													
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Product Display Type", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_type",
                  "value" => array(
				  					__('Style 1','vconepage') 	=> 'vcop-wooproductdisplay-style1',
									__('Style 2','vconepage')  => 'vcop-wooproductdisplay-style2',
									__('Style 3','vconepage')  => 'vcop-wooproductdisplay-style3',
									__('Style 4','vconepage')  => 'vcop-wooproductdisplay-style4',
									__('Style 5','vconepage')  => 'vcop-wooproductdisplay-style5',
									__('Style 6','vconepage')  => 'vcop-wooproductdisplay-style6',															
				   ),			   
				),	
								
				// POSTS
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Columns", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_columns",
                  "value" => array(
				  					__('1','vconepage') 	=> '1',
									__('2','vconepage')  	=> '2',
				  					__('3','vconepage') 	=> '3',
									__('4','vconepage')  	=> '4',
				  					__('5','vconepage') 	=> '5',
									__('6','vconepage')  	=> '6',
				  					__('7','vconepage') 	=> '7',
									__('8','vconepage')  	=> '8',																											
				   ),				   				   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Filter", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_filter",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true',
									__('Hidden','vconepage')  	=> 'false',															
				   ),			   			   
				),									
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Cart", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_posts_cart",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'cart_show',
									__('Hidden','vconepage')  	=> 'cart_hidden',															
				   ),			   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Plus", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_posts_plus",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'plus_show',
									__('Hidden','vconepage')  	=> 'plus_hidden',															
				   ),			   			   
				),				

				// QUERY 
			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Categories",'vconepage'),
				  "param_name" => "vcop_query_categories",
				  "description" => "Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)",
				  "group" => "Query",				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order", 'vconepage'),
                  "param_name" => "vcop_query_order",
                  "value" => array(
				  					__('DESC','vconepage')  	=> 'DESC',
				  					__('ASC','vconepage') 		=> 'ASC'
									
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order By", 'vconepage'),
                  "param_name" => "vcop_query_orderby",
                  "value" => array(
				  					__('Date','vconepage') 			=> 'date',
									__('ID','vconepage')  				=> 'ID',
				  					__('Author','vconepage') 			=> 'author',
									__('Title','vconepage')  			=> 'title',
				  					__('Name','vconepage') 			=> 'name',
									__('Modified','vconepage')  		=> 'modified',
				  					__('Parent','vconepage') 			=> 'parent',
									__('Rand','vconepage')  			=> 'rand',
									__('Comments Count','vconepage')  	=> 'comment_count',
									__('None','vconepage')  			=> 'none',																																													
				   ),
				  "group" => "Query",				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination", 'vconepage'),
                  "param_name" => "vcop_query_pagination",
                  "value" => array(
				  					__('Yes','vconepage') 		=> 'yes',
									__('No','vconepage')  		=> 'no',
				   ),
				   "group" => "Query"				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts",'vconepage'),
				  "param_name" => "vcop_query_number",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'no' )
				  ),					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination Type", 'vconepage'),
                  "param_name" => "vcop_query_pagination_type",
                  "value" => array(
				  					__('Numeric','vconepage') 		=> 'numeric',
									__('Normal','vconepage')  		=> 'normal',
				  ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),				   				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts For Page",'vconepage'),
				  "param_name" => "vcop_query_posts_for_page",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_pagination',
							'value' => array( 'yes' )
				  ),					  				  					  				  			  				  
			    ),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_wooproductdisplay_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_wooproductdisplay_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_wooproductdisplay_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_wooproductdisplay_title_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_wooproductdisplay_background_color",
					  "value" => 'rgba(255,255,255,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),			  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Secondary Color", 'vconepage'),
					  "param_name" => "vcop_wooproductdisplay_secondary_color",
					  "value" => 'rgba(0,0,0,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wooproductdisplay_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wooproductdisplay_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_wooproductdisplay_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_wooproductdisplay_loadCssAndJs() {	
		wp_register_style( 'vcop-wooproductdisplay',  AD_VCOP_URL . 'assets/css/wooproductdisplay.css' );
		wp_register_script( 'vcop-filter-js',  AD_VCOP_URL . 'assets/js/jquery.mixitup.js' , array('jquery'), '', true);		
    }	
}
// Finally initialize code
new vconepage_wooproductdisplay_class();