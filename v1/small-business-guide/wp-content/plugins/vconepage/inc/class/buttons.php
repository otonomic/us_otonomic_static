<?php
/* CLASS: Buttons */

class vconepage_buttons_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_buttonsWithVC' ) );
        add_shortcode( 'vcop_buttons', array( $this, 'vconepage_buttons_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_buttons_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_buttonsWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Button", 'vconepage'),
            "description" => __("Add your button", 'vconepage'),
            "base" => "vcop_buttons",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/button.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Button Type", 'vconepage'),
                  "param_name" => "vcop_button_type",
                  "value" => array(
				  					__('Flat Button Mini','vconepage') 						=> 'flat-button-mini',
									__('Flat Button Small','vconepage')  					=> 'flat-button-small',
									__('Flat Button Medium','vconepage')  					=> 'flat-button-medium',
									__('Flat Button Large','vconepage')  					=> 'flat-button-large',
									__('Backgraund Transparent Button Mini','vconepage') 	=> 'bg-none-button-mini',
									__('Backgraund Transparent Button Small','vconepage') 	=> 'bg-none-button-small',
									__('Backgraund Transparent Button Medium','vconepage') 	=> 'bg-none-button-medium',
									__('Backgraund Transparent Button Large','vconepage') 	=> 'bg-none-button-large',
				  					__('3D Button Mini','vconepage') 						=> 'd-button-mini',
									__('3D Button Small','vconepage')  						=> 'd-button-small',
									__('3D Button Medium','vconepage')  					=> 'd-button-medium',
									__('3D Button Large','vconepage')  						=> 'd-button-large',
									__('Custom','vconepage')  								=> 'button-custom',																	
				   ),			   
				),			

				array(
					'type' => 'textarea',
					'heading' => __( 'Text', 'vconepage' ),
					'param_name' => 'vcop_button_text',
					'admin_label' => true,
					'value' => __( 'This is custom heading element with Google Fonts', 'vconepage' ),					
				),

				array(
				    'type' => 'textfield',
					'class' => '',
					'heading' => __( 'Url Button', 'vconepage' ),
					'param_name' => 'vcop_button_url',
				    'description' => 'ex http://www.google.com',
					'value' => '',										
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target Button", 'vconepage'),
                  "param_name" => "vcop_button_target_url",
                  "value" => array(
				  					__('Self (Same Window)','vconepage') 		=> '_self',
									__('Blank (New Window)','vconepage')  		=> '_blank',
				   ),				   			   
				),									
				

			   /* ICON */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Icon Show", 'vconepage'),
                  "param_name" => "vcop_button_icon_show",
                  "value" => array(
				  					__('Hidden','vconepage')  	=> 'vcop-icon-hidden',
				  					__('Show','vconepage') 	=> 'vcop-icon-show'
									
				   ),				   			   
				),			    			           
			    array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Icon", 'vconepage'),
					  "param_name" => "vcop_button_icon",
					  "value" => $adtheme_icons_list,					  
					  "dependency" => array(
							'element' => 'vcop_button_icon_show',
							'value' => array( 'vcop-icon-show' )
					  ),				  
				 ),
					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_button_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_button_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_button_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),					
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Text Size (px)",'vconepage'),
				  "param_name" => "vcop_button_size",
				  "description" => "ex 15px",
				  "group" => "Style"				  			  				  
			    ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Align", 'vconepage'),
                  "param_name" => "vcop_button_align",
                  "value" => array(
				  					__('Center','vconepage') 	=> 'vcop-align-center',
									__('Left','vconepage')  	=> 'vcop-align-left',
									__('Right','vconepage')  	=> 'vcop-align-right',
				   ),
				   "group" => "Style"				   			   
				),
				/* BUTTON CUSTOM */
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Padding (px)",'vconepage'),
				  "param_name" => "vcop_button_custom_padding",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
				 ),				  
				  "group" => "Style"				  			  				  
			    ),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Margin (px)",'vconepage'),
				  "param_name" => "vcop_button_custom_margin",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
				 ),				  
				  "group" => "Style"				  			  				  
			    ),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Border Radius (ex: 10px)",'vconepage'),
				  "param_name" => "vcop_button_custom_border_radius",
				  "description" => "ex 15px",
				  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
				 ),				  
				  "group" => "Style"				  			  				  
			    ),
				/* #BUTTON CUSTOM */
				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Button Display", 'vconepage'),
                  "param_name" => "vcop_button_display",
                  "value" => array(
				  					__('Block','vconepage') 	=> 'block',
									__('Inline','vconepage')  	=> 'inline-block'
				   ),
				   "group" => "Style"				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Border Type", 'vconepage'),
                  "param_name" => "vcop_button_border_type",
                  "value" => array(
				  					__('Square','vconepage')  		=> 'vcop-border-square',
				  					__('Round','vconepage') 		=> 'vcop-border-round'
									
				   ),
				   "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array(
										'bg-none-button-mini', 
										'bg-none-button-small',
										'bg-none-button-medium',
										'bg-none-button-large',
										'flat-button-mini', 
										'flat-button-small',
										'flat-button-medium',
										'flat-button-large',							
										'd-button-mini', 
										'd-button-small',
										'd-button-medium',
										'd-button-large'																													
									)
				   ),				   
				   "group" => "Style"				   			   
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Border Style", 'vconepage'),
                  "param_name" => "vcop_button_border_style",
                  "value" => array(
				  					__('Solid','vconepage')  		=> 'solid',
				  					__('Dotted','vconepage') 		=> 'dotted',
									__('Dashed','vconepage') 		=> 'dashed',
									__('Double','vconepage') 		=> 'double',
									__('Inset','vconepage') 		=> 'inset',
									__('Outset','vconepage') 		=> 'outset',									
				   ),
				   "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array(
										'bg-none-button-mini', 
										'bg-none-button-small',
										'bg-none-button-medium',
										'bg-none-button-large'																			
									)
				   ),				   
				   "group" => "Style"				   			   
				),																			
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Text Color", 'vconepage'),
					  "param_name" => "vcop_button_text_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Over Color", 'vconepage'),
					  "param_name" => "vcop_button_over_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array(
										'bg-none-button-mini', 
										'bg-none-button-small',
										'bg-none-button-medium',
										'bg-none-button-large',																		
									)
					  ),					  
					  "group" => "Style"					  					  				  
				  ),
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_button_background_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 
										'flat-button-mini', 
										'flat-button-small',
										'flat-button-medium',
										'flat-button-large',							
										'd-button-mini', 
										'd-button-small',
										'd-button-medium',
										'd-button-large',
										'button-custom',
									)
					  ),					  
					  "group" => "Style"					  					  				  
				  ),				  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Border Color", 'vconepage'),
					  "param_name" => "vcop_button_border_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 
										'd-button-mini', 
										'd-button-small',
										'd-button-medium',
										'd-button-large',
									)
					  ),						  
					  "group" => "Style"					  					  				  
				  ),


                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Border Show", 'vconepage'),
                  "param_name" => "vcop_button_custom_border_active",
                  "value" => array(
				  					__('On','vconepage')  		=> 'on',
				  					__('Off','vconepage') 		=> 'off'									
				 			),				 
				  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
					  ),
				  "group" => "Style"  	
				 ),	
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Border Width",'vconepage'),
				  "param_name" => "vcop_button_custom_border_width",
				  "description" => "ex 1px",
				  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
					  ),			  
				  "group" => "Style"				  			  				  
			    ),					 		 
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Border Color", 'vconepage'),
					  "param_name" => "vcop_button_custom_border_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
					  ),						  
					  "group" => "Style"					  					  				  
				  ),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Border Style", 'vconepage'),
                  "param_name" => "vcop_button_custom_border_style",
                  "value" => array(
				  					__('Solid','vconepage')  		=> 'solid',
				  					__('Dotted','vconepage') 		=> 'dotted',
									__('Dashed','vconepage') 		=> 'dashed',
									__('Double','vconepage') 		=> 'double',
									__('Inset','vconepage') 		=> 'inset',
									__('Outset','vconepage') 		=> 'outset',									
				   ),
				  "dependency" => array(
								'element' => 'vcop_button_type',
								'value' => array( 'button-custom' )
				  			),			   
				  "group" => "Style"				   			   
				),	
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Text Over Color", 'vconepage'),
					  "param_name" => "vcop_button_custom_text_over_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
					  ),						  
					  "group" => "Style"					  					  				  
				  ),
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Over Color", 'vconepage'),
					  "param_name" => "vcop_button_custom_background_over_color",
					  "value" => '#FC615D',
					  "dependency" => array(
							'element' => 'vcop_button_type',
							'value' => array( 'button-custom' )
					  ),						  
					  "group" => "Style"					  					  				  
				  ),
				  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_buttons_loadCssAndJs() {	
		wp_register_style( 'vcop-buttons',  AD_VCOP_URL . 'assets/css/buttons.css' );
    }	
}
// Finally initialize code
new vconepage_buttons_class();