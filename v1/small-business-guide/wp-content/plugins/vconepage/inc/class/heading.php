<?php
/* CLASS: Heading */

class vconepage_heading_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_headingWithVC' ) );
        add_shortcode( 'vcop_heading', array( $this, 'vconepage_heading_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_heading_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_headingWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Heading", 'vconepage'),
            "description" => __("Add your heading", 'vconepage'),
            "base" => "vcop_heading",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/heading.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Heading Type", 'vconepage'),
                  "param_name" => "vcop_heading_type",
                  "value" => array(
				  					__('Normal','vconepage') 					=> 'normal',
									__('Double Line Top','vconepage')  		=> 'double-line-top',
									__('Double Line Middle','vconepage')  		=> 'double-line-middle',
									__('Double Line Bottom','vconepage')  		=> 'double-line-bottom',
									__('Line Top - Line Bottom','vconepage') 	=> 'line-top-bottom',
									__('Dashed Bottom','vconepage') 			=> 'dashed-bottom',
									__('Bottom Line with Icon','vconepage') 	=> 'bottom-line-with-icon',
									__('Heading with Icon','vconepage') 		=> 'heading-with-icon',
				   ),			   
				),			

				array(
					'type' => 'textarea',
					'heading' => __( 'Text', 'vconepage' ),
					'param_name' => 'vcop_heading_text',
					'admin_label' => true,
					'value' => __( 'This is custom heading element with Google Fonts', 'vconepage' ),
				),
				
				/* HEADING ALIGN */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Text Align", 'vconepage'),
                  "param_name" => "vcop_heading_align",
                  "value" => array(
				  					__('Center','vconepage') 	=> 'vcop-align-center',
									__('Left','vconepage')  	=> 'vcop-align-left',
									__('Right','vconepage')  	=> 'vcop-align-right',
				   ),
				  "dependency" => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'normal',
										'line-top-bottom',
										'dashed-bottom',
										'bottom-line-with-icon',
										'heading-with-icon',			
							)
				   ),					   			   
				),										

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Display", 'vconepage'),
                  "param_name" => "vcop_heading_display",
                  "value" => array(
				  					__('Block','vconepage') 		=> 'block',
									__('Inline-Block','vconepage') => 'inline-block',
				   ),
				  "dependency" => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'normal',
										'line-top-bottom',
										'dashed-bottom',
										'bottom-line-with-icon',
										'heading-with-icon',			
							)
				   ),					   			   
				),
			
				/* LINE SEPARATOR */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Line Separator", 'vconepage'),
                  "param_name" => "vcop_heading_line_separator",
                  "value" => array(
				  					__('Small','vconepage') 	=> 'vcop-line-separator-small',
									__('Medium','vconepage')  	=> 'vcop-line-separator-medium',
									__('Big','vconepage')  	=> 'vcop-line-separator-big',
				   ),
				  "dependency" => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'dashed-bottom'			
							)
				   ),					   			   
				),	

			   /* ICON */ 			           
			   array(
					  "type" => "dropdown",
					  "class" => "",
					  "heading" => __("Icon", 'vconepage'),
					  "param_name" => "vcop_heading_icon",
					  "value" => $adtheme_icons_list,
					  'dependency' => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'bottom-line-with-icon', 
										'heading-with-icon',
										)
					  ),				  
				),
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Line Separator", 'vconepage'),
                  "param_name" => "vcop_heading_icon_position",
                  "value" => array(
				  					__('Bottom','vconepage') 	=> 'vcop-icon-bottom',
									__('Left','vconepage')  	=> 'vcop-icon-left',
									__('Right','vconepage')  	=> 'vcop-icon-right',
				   ),
					  'dependency' => array(
							'element' => 'vcop_heading_type',
							'value' => array( 'heading-with-icon' )
					  ),					   			   
				),					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_heading_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_heading_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_heading_font_type',
							'value' => array( 'vcop-google-font' )
					),						
					"group" => "Style"
				),	
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Text Size (px)",'vconepage'),
				  "param_name" => "vcop_heading_size",
				  "description" => "ex 15px",
				  "group" => "Style"				  			  				  
			    ),	
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Text Line Height",'vconepage'),
				  "param_name" => "vcop_heading_line_height",
				  "description" => "ex 1.8",			  
				  "group" => "Style"				  			  				  
			    ),							
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Text Color", 'vconepage'),
					  "param_name" => "vcop_heading_text_color",
					  "value" => '#FC615D',
					  "group" => "Style"			  
				 ),  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Line Color", 'vconepage'),
					  "param_name" => "vcop_heading_line_color",
					  "value" => '#FC615D',
					  'dependency' => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'double-line-top',
										'double-line-middle',
										'double-line-bottom',
										'line-top-bottom',
										'dashed-bottom',
										'bottom-line-with-icon'			
							)
					  ),
					 "group" => "Style"					  					  				  
				  ),
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_heading_icon_size",
				  "description" => "ex 15px",
				  'dependency' => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'heading-with-icon'			
							)
				  ),				  
				  "group" => "Style"				  			  				  
			    ),				  
				array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Icon Color", 'vconepage'),
					  "param_name" => "vcop_heading_icon_color",
					  "value" => '#FC615D',
					  'dependency' => array(
							'element' => 'vcop_heading_type',
							'value' => array( 
										'heading-with-icon'			
							)
					  ),
					 "group" => "Style"					  					  				  
				 ),
				  				  
				 /* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			    array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			    ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_heading_loadCssAndJs() {	
		wp_register_style( 'vcop-heading',  AD_VCOP_URL . 'assets/css/heading.css' );
    }	
}
new vconepage_heading_class();