<?php
/* CLASS: Box Message */

class vconepage_boxmessage_class {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrate_vconepage_boxmessageWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'vcop_boxmessage', array( $this, 'vconepage_boxmessage_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_boxmessage_loadCssAndJs' ) );

    }
 
    public function integrate_vconepage_boxmessageWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
        
		vc_map( array(
            "name" => __("Box Message", 'vconepage'),
            "description" => __("Add your message box", 'vconepage'),
            "base" => "vcop_boxmessage",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/box-message.png'),
			"admin_enqueue_css" => array(plugins_url('vconepage/assets/css/general/fonts.css')),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type Box Message", 'vconepage'),
                  "param_name" => "vcop_boxmessagge_type",
                  "value" => array(
									__('Default','vconepage')  	=> 'default',
									__('Custom','vconepage') 	=> 'custom',
				   )
				),			
			
			
			
				// DEFAULT		
			
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Type Default Message", 'vconepage'),
                  "param_name" => "vcop_boxmessagge_default_type",
                  "value" => array(
									__('Info','vconepage')  	=> 'wpb_alert-info',
									__('Alert','vconepage') 	=> 'wpb_alert-warning',
									__('Success','vconepage') 	=> 'wpb_alert-success',
									__('Error','vconepage') 	=> 'wpb_alert-danger',
				   ),
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'default' )
				  ),				   
				),			
			
			
				// BOTH
				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Style", 'vconepage'),
                  "param_name" => "vcop_boxmessagge_default_style",
                  "value" => array(
									__('Rounded','vconepage')  	=> 'vc_alert_rounded',
									__('Square','vconepage') 	=> 'vc_alert_square',
									__('Round','vconepage') 	=> 'vc_alert_round',
									__('Outlined','vconepage') 	=> 'vc_alert_outlined',
									__('3D','vconepage') 	=> 'vc_alert_3d',
									__('Square Outlined','vconepage') 	=> 'vc_alert_square_outlined',									
				   ),				   
				),				

			
			  // CUSTOM
			             
			  array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Icon", 'vconepage'),
                  "param_name" => "vcop_boxmessagge_custom_icon",
				  "admin_label" => true,
                  "value" => $adtheme_icons_list,
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),				  
              ),
			   				  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Icon Float", 'vconepage'),
                  "param_name" => "vcop_boxmessagge_custom_icon_float",
                  "value" => array(
									__('Left','vconepage')  	=> 'left',
									__('Right','vconepage') 	=> 'right',
									__('None','vconepage') 	=> 'none'
				   ),
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),				   
				),              
 

		
				
				// BOTH
			  	array(
				  "type" => "textarea",
				  "class" => "",
				  "heading" => __("Text Message",'vconepage'),
				  "param_name" => "vcop_boxmessagge_text",				  
			 	),				
				
				
				// STYLE
				
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Size (px)",'vconepage'),
				  "param_name" => "vcop_boxmessagge_custom_size",
				  "description" => "ex 15",
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  				  
			  ),				
              array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Icon Color", 'vcfg'),
                  "param_name" => "vcop_boxmessagge_custom_icon_color",
                  "value" => '#FC615D',
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  
              ),					  		  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Margin",'vconepage'),
				  "param_name" => "vcop_boxmessagge_custom_margin",
				  "description" => "ex 2px or 2px 2px 2px 2px",		
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  		  
			  ),
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Icon Padding",'vconepage'),
				  "param_name" => "vcop_boxmessagge_custom_padding",
				  "description" => "ex 2px or 2px 2px 2px 2px",
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  				  
			  ),				
			 array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Background Color", 'vcfg'),
                  "param_name" => "vcop_boxmessagge_custom_background_color",
                  "value" => '#FC615D',
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  
              ), 				
				
			 array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => __("Border Color only For 3d Box", 'vcfg'),
                  "param_name" => "vcop_boxmessagge_custom_background_border_3d_color",
                  "value" => '#FC615D',
				  'dependency' => array(
						'element' => 'vcop_boxmessagge_type',
						'value' => array( 'custom' )
				  ),
				  "group" => "Style"				  
              ),
			  				
				// ANIMATION								  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(
				  					__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
									
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );
    }	
	
    public function vconepage_boxmessage_loadCssAndJs() {
		
		// Main		
		wp_register_style( 'vcop-fonts',  AD_VCOP_URL . 'assets/css/general/fonts.css' );
					
    }	
}
// Finally initialize code
new vconepage_boxmessage_class();