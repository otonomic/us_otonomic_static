<?php
/* CLASS: carousel */

class vconepage_carousel_class {
    function __construct() {
        add_action( 'init', array( $this, 'integrate_vconepage_carouselWithVC' ) );
        add_shortcode( 'vcop_carousel', array( $this, 'vconepage_carousel_function' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'vconepage_carousel_loadCssAndJs' ) );
    }
 
    public function integrate_vconepage_carouselWithVC() {
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
		
		include('icons-list.php');
		include('animate-list.php');
		
		vc_map( array(
            "name" => __("Carousel", 'vconepage'),
            "description" => __("Add your carousel", 'vconepage'),
            "base" => "vcop_carousel",
            "class" => "adtheme",
            "controls" => "full",
            "icon" => plugins_url('vconepage/assets/img/carousel.png'),
            "category" => __('VC One Page', 'js_composer'),
            "params" => array(	
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Carousel Type", 'vconepage'),
                  "param_name" => "vcop_carousel_type",
                  "value" => array(
				  					__('Style 1','vconepage') 	=> 'vcop-carousel-style1',
									__('Style 2','vconepage')  => 'vcop-carousel-style2',
									__('Style 3','vconepage')  => 'vcop-carousel-style3',															
				   ),			   
				),		

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Carousel Source", 'vconepage'),
                  "param_name" => "vcop_carousel_source",
                  "value" => array(
				  					__('Posts/Custom Posts','vconepage') 	=> 'vcop_posts',
									__('Gallery Image','vconepage')  		=> 'vcop_gallery',																										
				   ),				   				   			   
				),										
				
				
				
				// POSTS
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Zoom", 'vconepage'),
                  "param_name" => "vcop_carousel_posts_zoom",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'zoom_show',
									__('Hidden','vconepage')  => 'zoom_hidden',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
					),				   			   
				),				
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Show Plus", 'vconepage'),
                  "param_name" => "vcop_carousel_posts_plus",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'plus_show',
									__('Hidden','vconepage')  => 'plus_hidden',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
					),				   			   
				),				
				
				
				
				// GALLERY		
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Link", 'vconepage'),
                  "param_name" => "vcop_carousel_link",
                  "value" => array(
				  					__('Image','vconepage') 	=> 'image',
									__('Custom Url','vconepage')  => 'custom_url',															
				   ),
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_gallery' )
					),				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Target Link", 'vconepage'),
                  "param_name" => "vcop_carousel_target_link",
                  "value" => array(
				  					__('Same Window','vconepage')  => '_self',	
				  					__('New Window','vconepage') 	=> '_blank'
																							
				   ),
				  "dependency" => array(
							'element' => 'vcop_carousel_link',
							'value' => array( 'custom_url' )
				  ),				   			   
				),

                array(
                  "type" => "attach_images",
                  "class" => "",
                  "heading" => __("Upload Your Images", 'vconepage'),
                  "param_name" => "vcop_carousel_images",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_gallery' )
				  ),				  
                ),

				// QUERY 
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Source", 'vconepage'),
                  "param_name" => "vcop_query_source",
                  "value" => array(
				  					__('Wordpress Posts','vconepage') 		=> 'wp_posts',
									__('Custom Posts Type','vconepage')  	=> 'wp_custom_posts_type',
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("All Posts/Sticky posts", 'vconepage'),
                  "param_name" => "vcop_query_sticky_posts",
                  "value" => array(
				  					__('All Posts','vconepage') 		 	=> 'allposts',
									__('Only Sticky Posts','vconepage')  	=> 'onlystickyposts',
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_source',
							'value' => array( 'wp_posts' )
				  ),					   				   			   
				),

			    array(
				  "type" => "posttypes", 
				  "class" => "",
				  "heading" => __("Select Post Type Source",'vconepage'),
				  "param_name" => "vcop_query_posts_type",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_query_source',
							'value' => array( 'wp_custom_posts_type' )
				  ),					  				  					  				  			  				  
			    ),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Categories",'vconepage'),
				  "param_name" => "vcop_query_categories",
				  "description" => "Write Categories slug separate by comma(,) for example: cat-slug1, cat-slug2, cat-slug3 (Leave empty for all categories)",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
				  ),				  					  				  					  				  			  				  
			    ),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order", 'vconepage'),
                  "param_name" => "vcop_query_order",
                  "value" => array(
				  					__('DESC','vconepage')  	=> 'DESC',
				  					__('ASC','vconepage') 		=> 'ASC'
									
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Order By", 'vconepage'),
                  "param_name" => "vcop_query_orderby",
                  "value" => array(
				  					__('Date','vconepage') 			=> 'date',
									__('ID','vconepage')  				=> 'ID',
				  					__('Author','vconepage') 			=> 'author',
									__('Title','vconepage')  			=> 'title',
				  					__('Name','vconepage') 			=> 'name',
									__('Modified','vconepage')  		=> 'modified',
				  					__('Parent','vconepage') 			=> 'parent',
									__('Rand','vconepage')  			=> 'rand',
									__('Comments Count','vconepage')  	=> 'comment_count',
									__('None','vconepage')  			=> 'none',																																													
				   ),
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
				  ),				   				   			   
				),

			    array(
				  "type" => "textfield", 
				  "class" => "",
				  "heading" => __("Number Posts",'vconepage'),
				  "param_name" => "vcop_query_number",
				  "description" => "ex 10",
				  "group" => "Query",
				  "dependency" => array(
							'element' => 'vcop_carousel_source',
							'value' => array( 'vcop_posts' )
				  ),					  				  					  				  			  				  
			    ),

				 /* CAROUSEL */ 				   	
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Number Item Show", 'vconepage'),
                  "param_name" => "vcop_carousel_item",
                  "value" => array(
				  					__('1','vconepage') 	=> '1',
									__('2','vconepage')  	=> '2',
				  					__('3','vconepage') 	=> '3',
									__('4','vconepage')  	=> '4',
				  					__('5','vconepage') 	=> '5',
									__('6','vconepage')  	=> '6',
				  					__('7','vconepage') 	=> '7',
									__('8','vconepage')  	=> '8',																											
				   ),
				  "group" => "Carousel Settings"				   				   			   
				),
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Navigation", 'vconepage'),
                  "param_name" => "vcop_carousel_navigation",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true', 
									__('Hidden','vconepage')  	=> 'false'																										
				   ),
				  "group" => "Carousel Settings"				   				   			   
				),
                 array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Pagination", 'vconepage'),
                  "param_name" => "vcop_carousel_pagination",
                  "value" => array(
				  					__('Show','vconepage') 	=> 'true', 
									__('Hidden','vconepage')  	=> 'false'																										
				   ),
				  "group" => "Carousel Settings"				   				   			   
				),
				array(
					  "type" => "textfield",
					  "class" => "",
					  "heading" => __("Autoplay (ms)",'vconepage'),
					  "param_name" => "vcop_carousel_autoplay",
					  "description" => "ex 1000 (Insert 0 if you want disable autoplay)",					  
					  "group" => "Carousel Settings"					  
				),	
				array(
					  "type" => "textfield",
					  "class" => "",
					  "heading" => __("Item Padding",'vconepage'),
					  "param_name" => "vcop_carousel_item_padding",
					  "description" => "ex 10px or 2px 2px 2px 2px (Leave empty for no padding)",
					  "group" => "Carousel Settings"					  
				),
								 					  
           		/* STYLE */
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Font Type", 'vconepage'),
                  "param_name" => "vcop_carousel_font_type",
                  "value" => array(
				  					__('Default Site','vconepage') 			=> 'vcop-font-default',
									__('Google Fonts Custom','vconepage')  	=> 'vcop-google-font'
				   ),
				   "group" => "Style"				   			   
				),				
				array(
					'type' => 'google_fonts',
					'param_name' => 'vcop_carousel_google_fonts',
					'value' => 'font_family:Abril%20Fatface%3A400|font_style:400%20regular%3A400%3Anormal',
					'settings' => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'vconepage' ),
							'font_style_description' => __( 'Select font styling.', 'vconepage' )
						)
					),
					'dependency' => array(
							'element' => 'vcop_carousel_font_type',
							'value' => array( 'vcop-google-font' )
					),					
					"group" => "Style"
				),
			    array(
				  "type" 		=> "textfield", 
				  "class" 		=> "",
				  "heading" 	=> __("Title font Size (px)",'vconepage'),
				  "param_name" 	=> "vcop_carousel_title_fs",
				  "description" => "ex 15px",				  
				  "group" 		=> "Style",			  				  			  				  
			    ),						
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Background Color", 'vconepage'),
					  "param_name" => "vcop_carousel_background_color",
					  "value" => 'rgba(255,255,255,1)',				  
					  "group" => "Style"					  					  				  
				  ),			  
				 array(
					  "type" => "colorpicker",
					  "class" => "",
					  "heading" => __("Secondary Color", 'vconepage'),
					  "param_name" => "vcop_carousel_secondary_color",
					  "value" => 'rgba(0,0,0,0.3)',				  
					  "group" => "Style"					  					  				  
				  ),				  				
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Font Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_carousel_font_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Link Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_carousel_a_color",
					  "value" 		 	=> 'rgba(56,56,56,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
				 array(
					  "type" 			=> "colorpicker",
					  "class" 			=> "",
					  "heading"			=> __("Over Color", 'vconepage'),
					  "param_name" 	 	=> "vcop_carousel_over_color",
					  "value" 		 	=> 'rgba(138,138,138,1)',
					  "group" 		 	=> "Style"				  			  
				 ),
							  				  				  
				/* ANIMATION */  												  							  		  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate", 'vconepage'),
                  "param_name" => "vcop_animate",
                  "value" => array(									
									__('Off','vconepage') 	  => 'off',
									__('On','vconepage') => 'on'
				   ),
				   "group" => "Animation"
				),			  
                array(
                  "type" => "dropdown",
                  "class" => "",
                  "heading" => __("Animate Effects", 'vconepage'),
                  "param_name" => "vcop_animate_effect",
                  "value" => $animate_effect,
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "group" => "Animation"				   
				),				  
			  array(
				  "type" => "textfield",
				  "class" => "",
				  "heading" => __("Animate Delay (ms)",'vconepage'),
				  "param_name" => "vcop_delay",
				  "description" => "ex 1000",					  
				  'dependency' => array(
						'element' => 'vcop_animate',
						'value' => array( 'on' )
				  ),
				  "value" => "0",
				  "group" => "Animation"					  
			  ),			  
			  			  
			)
		)		  			  			  			  			  			  
	  );	  
    }	
	
    public function vconepage_carousel_loadCssAndJs() {	
		wp_register_style( 'vcop-carousel',  AD_VCOP_URL . 'assets/css/carousel.css' );
		wp_register_style( 'vcop-magnific-popup',  AD_VCOP_URL . 'assets/css/magnific-popup.css' );
		wp_register_script( 'vcop-magnific-popup-js',  AD_VCOP_URL . 'assets/js/jquery.magnific-popup.js' , array('jquery'), '', true);
		wp_register_style( 'vcop-owl-carousel',  AD_VCOP_URL . 'assets/css/owl.carousel.css' );
		wp_register_style( 'vcop-owl-theme',  AD_VCOP_URL . 'assets/css/owl.theme.css' );
		wp_register_script( 'vcop-owl-carousel-js',  AD_VCOP_URL . 'assets/js/owl.carousel.min.js' , array('jquery'), '', true);		
    }	
}
// Finally initialize code
new vconepage_carousel_class();