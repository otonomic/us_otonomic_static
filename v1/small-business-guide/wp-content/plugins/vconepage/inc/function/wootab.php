<?php
/*
File: wootab.php
Description: Function wootab
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_wootab_function extends vconepage_wootab_class {
public function vconepage_wootab_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_wootab_filter" => 'false',
				"vcop_wootab_image" => '',
				"vcop_wootab_name" => '',
				"vcop_wootab_stars" => '',
				"vcop_wootab_stock" => '',
				"vcop_wootab_price" => '',
				"vcop_wootab_cart" => '',
				"vcop_wootab_plus" => '',				

				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',
				
				"vcop_wootab_font_type" => '',
				"vcop_wootab_google_fonts" => '',
				"vcop_wootab_fs" => '',
				"vcop_wootab_background_color" => '',
				"vcop_wootab_secondary_color" => '',
				"vcop_wootab_font_color" => '',
				"vcop_wootab_a_color" => '',												
				"vcop_wootab_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_wootab_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_wootab_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_wootab_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_wootab_font_type == 'vcop-google-font') {
				$return .=  '.vcop-wootab-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_wootab_style($instance,
							$vcop_wootab_fs,  
							$vcop_wootab_background_color, 
							$vcop_wootab_secondary_color, 
							$vcop_wootab_font_color, 
							$vcop_wootab_a_color, 
							$vcop_wootab_over_color);
						
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );				
			wp_enqueue_style( 'vcop-wootab' );						

			if($vcop_wootab_filter == 'true') {
				
				wp_enqueue_script( 'vcop-filter-js' );	
				
				$return .= vcop_filter_js($instance,'wootab');
				
				$return .= vcop_filter_item($instance,
											'vcop-wootab',
											'wp_custom_posts_type',
											$vcop_query_categories,
											'product');	
			
			}				
			
			$return .= '<div class="vcop-wootab vcop-'.$instance.' vcop-wootab-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
				if($vcop_wootab_filter == 'true') {
					$return .= ' vcop-filter';	
				}				
			$return .= '">';
			
			$return .= '<div class="vcop-item vcop-woo-title-table">';
			$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
			
			if($vcop_wootab_image == 'image_show') {
			$return .= '<div class="vcop-woo-image">'.__('Image Product','vconepage').'</div>';
			}
			
			if($vcop_wootab_name == 'name_show') {
			$return .= '<div class="vcop-woo-name">'.__('Name','vconepage').'</div>';
			}
			
			if($vcop_wootab_stars == 'stars_show') {
				$return .= '<div class="vcop-woo-stars">'.__('Stars','vconepage').'</div>';
			}
			
			if($vcop_wootab_stock == 'stock_show') {
				$return .= '<div class="vcop-woo-stock">'.__('Stock','vconepage').'</div>';
			}
			
			if($vcop_wootab_price == 'price_show') {
				$return .= '<div class="vcop-woo-price">'.__('Price','vconepage').'</div>';
			}
			
			if($vcop_wootab_cart == 'cart_show' && $vcop_wootab_plus == 'plus_show') {
				$return .= '<div class="vcop-woo-link">'.__('More Info','vconepage').'</div>';
			}
			
			
			$return .= '<div class="vcop-clear"></div></div></div></div>';
			
			/**************************************************/
			/****************** POSTS SOURCE ******************/
			/**************************************************/
			
			// LOOP QUERY
			$query = vcop_query_woocommerce($vcop_query_categories, 
											$vcop_query_order, 
											$vcop_query_orderby, 
											$vcop_query_pagination, 
											$vcop_query_number, 
											$vcop_query_posts_for_page,
											$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				$id_post = get_the_id();
				$product = new WC_Product( $id_post );
				$price = $product->regular_price;
				$price_sales = $product->sale_price;
				$symbol = get_woocommerce_currency_symbol();
								
				if($vcop_wootab_filter == 'true') {
					
					$return .= vcop_filter_item_div('wp_custom_posts_type',$vcop_query_categories,'product');
					
				} else {
				
					$return .= '<div class="vcop-item">';
				
				}
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				
				// IMAGE PRODUCT
				if($vcop_wootab_image == 'image_show') {
					$return .= '<div class="vcop-woo-image">';
						if(!empty($price_sales) || $price_sales != '') {
							$return .= '<span class="vcop-item-sale">'.__('Sale','vconepage').'</span>';	
						}				
					$return .= vconepage_thumbs('vconepage-rectangle');
					$return .= '</div>';
				}
				
				if($vcop_wootab_name == 'name_show') {
					// WOO NAME
					$return .= '<div class="vcop-woo-name">';
					$return .= '<a href='.$link.'>'.get_the_title().'</a>';
					$return .= $product->get_categories( ', ', '<span>' . _n( 'Category:', 'Categories:', sizeof( get_the_terms( $id_post, 'product_cat' ) ), 'vconepage' ) . ' ', '.</span>' );
					$return .= '</div>';
				}
				
				if($vcop_wootab_stars == 'stars_show') {
					// WOO STARS
					$return .= '<div class="vcop-woo-stars woocommerce">';
					$average = $product->get_average_rating();
	
					$return .= '<div class="star-rating"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'woocommerce' ).'</span></div>';
	
					$return .= '</div>';
				}
				
				if($vcop_wootab_stock == 'stock_show') {
					// WOO STOCK
					$return .= '<div class="vcop-woo-stock">';
					// Availability
					$availability = $product->get_availability();
					if ($availability['availability']) :
						$return .= apply_filters( 'woocommerce_stock_html', '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>', $availability['availability'] );
					else:
						$return .= __('No in stock','vconepage');
					endif;
					$return .= '</div>';
				}
				
				if($vcop_wootab_price == 'price_show') {
					// WOO PRICE
					$return .= '<div class="vcop-woo-price">';
						$return .= '<span class="vcop-price"><span class="vcop-regular-price';
						
						if(!empty($price_sales) || $price_sales != '') {
							$return .= ' vcop-line-through';	
						}
						
						$return .= '">'. $price . $symbol .'</span>';
						
						if(!empty($price_sales) || $price_sales != '') {
							$return .= '<span class="vcop-sale-price">'. $price_sales . $symbol .'</span>';
						}
						$return .= '</span>';									
					$return .= '</div>';
				}
				
				// WOO ADD TO CART - READ MORE
				$return .= '<div class="vcop-woo-link">';
					if($vcop_wootab_cart == 'cart_show') {			
						$return .= '<a class="add_to_cart_button product_type_simple added" data-product_sku="" data-product_id="'.$id_post.'" rel="nofollow" href="?add-to-cart='.$id_post.'">'.__('Add to Cart','vconepage').'</a>';
					}
					if($vcop_wootab_plus == 'plus_show') {			
						$return .= '<a href="'.$link.'" class="plus_show">'.__('Read More','vconepage').'</a>';
					}								
				$return .= '</div>';
								
				$return .= '<div class="vcop-clear"></div></div>';										
				$return .=	'</div>';											
				$return .= '</div>'; // #vcop-ITEM
			endwhile;
			} // #LOOP QUERY				

				// PAGINATION 
				if($vcop_query_pagination == 'yes') {
					$return .= '<div class="vcop-clear"></div><div class="vcop-wootab-'.$instance.' vcop-pagination">';
					if($vcop_query_pagination_type == 'numeric') {
						$return .= vcop_numeric_pagination($pages = '', $range = 2,$loop);
					} else {
						$return .= get_next_posts_link( 'Older posts', $loop->max_num_pages );
						$return .= get_previous_posts_link( 'Newer posts' );				
					}
					$return .= '</div>';
				}
				// #PAGINATION
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_wootab_function();
?>