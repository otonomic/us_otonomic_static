<?php
/*
File: map.php
Description: Function Map
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_map_function extends vconepage_map_class {
public function vconepage_map_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_type" => 'road', 
				"vcop_latidude" => '36.394757', 
				"vcop_longitude" => '-105.600586', 
				"vcop_zoom" => '9', 
				"vcop_messagge" => 'This is the message...', 
				"vcop_width" => '300px', 
				"vcop_height" => '300px',
				"vcop_marker" => 'off',
				"vcop_marker_image" => '',
				"vcop_zoomcontrol" => 'false',
				"vcop_scalecontrol" => 'false',
				"vcop_streetviewcontrol" => 'false',
				"vcop_rotatecontrol" => 'false',
				"vcop_maptypecontrol" => 'false',
				"vcop_pancontrol" => 'false', 
				"vcop_overviewmapcontrol" => 'false',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800'), 
				$atts)
	);

	/* LOAD CSS */	
	wp_enqueue_style( 'vcop-map' );

	$id = 'vconepage-'.$instance;
	wp_enqueue_script( 'vcop-map-js',  'http://maps.google.com/maps/api/js?sensor=false'); 
	
    $mapType = '';
    if($vcop_type == "satellite") {
        $mapType = "SATELLITE";
	} elseif($vcop_type == "terrain") {
        $mapType = "TERRAIN"; 
	} elseif($vcop_type == "hybrid") {
        $mapType = "HYBRID";
	} elseif($vcop_type == "roadmap") {
        $mapType = "ROADMAP"; 
	}
	
    echo '<!-- Google Map -->
        <script type="text/javascript"> 
		jQuery(document).ready(function($){
        $(document).ready(function() {
          function initializeGoogleMap() {
     
              var myLatlng = new google.maps.LatLng('.$vcop_latidude.','.$vcop_longitude.');
              var myOptions = {
                center: myLatlng, 
                zoom: '.$vcop_zoom.',
                mapTypeId: google.maps.MapTypeId.'.$mapType.',
				panControl:'.$vcop_pancontrol.',
				zoomControl:'.$vcop_zoomcontrol.',
				mapTypeControl:'.$vcop_maptypecontrol.',
				scaleControl:'.$vcop_scalecontrol.',
				streetViewControl:'.$vcop_streetviewcontrol.',
				overviewMapControl:'.$vcop_overviewmapcontrol.',
				rotateControl:'.$vcop_rotatecontrol.'		
              };
              var map = new google.maps.Map(document.getElementById("'.$id.'"), myOptions);
     
              var contentString = "'.$vcop_messagge.'";
              var infowindow = new google.maps.InfoWindow({
                  content: contentString
              });
               
              var marker = new google.maps.Marker({
                  position: myLatlng,';
		if($vcop_marker == 'on') {	
			$image_attributes = wp_get_attachment_image_src( $vcop_marker_image, 'full' );	  
			echo 'icon: \''.$image_attributes[0].'\'';
		}
		echo '});
               
              google.maps.event.addListener(marker, "click", function() {
                  infowindow.open(map,marker);
              });
               
              marker.setMap(map);
             
          }
          initializeGoogleMap();
     
         });
		});
        </script>';
     
	 
	 	if($vcop_animate == 'on') {
        	$return = '<div class="map-container animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'" style="height:'.$vcop_height.';"><div id="'.$id.'" style="width:'.$vcop_width.'; height:'.$vcop_height.';" class="googleMap"></div></div>';
		} else {
        	$return = '<div class="map-container" style="height:'.$vcop_height.';"><div id="'.$id.'" style="width:'.$vcop_width.'; height:'.$vcop_height.';" class="googleMap"></div></div>';			
		}
		return $return;
	} 
}
new vcop_map_function();
?>