<?php
/*
File: header.php
Description: Function header
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_header_function extends vconepage_header_class {
public function vconepage_header_function($atts)
{
	static $instance = 0;
	$instance++;
		
    extract(
		shortcode_atts(
			array(
				"vcop_layout_type" => '',
				"vcop_boxed_background_type" => '',
				"vcop_boxed_background_color" => '',
				"vcop_boxed_background_image" => '',
				"vcop_boxed_wrapper_width" => '',
				"vcop_page_font_type" => '', 
				"vcop_page_google_fonts" => '', 
				"vcop_page_font_color" => '', 
				"vcop_page_font_color_link" => '',
				"vcop_page_font_color_over_link" => '',
				"vcop_header_fixed_relative_position" => '', 
				"vcop_header_position" => '',
				"vcop_header_type" => '', 
				"vcop_header_background_color" => '',
				'vcop_header_padding' => '',
				'vcop_header_margin' => '',
				"vcop_header_logo_active" => '',
				"vcop_header_logo" => '',
				"vcop_header_logo_align" => '', 
				"vcop_header_logo_position" => '',
				"vcop_header_logo_link" => '', 
				"vcop_header_logo_custom_url" => '',							
				"vcop_header_logo_target" => '', 
				"vcop_header_menu_list" => '',
				"vcop_header_menu_text_align" => '',
				"vcop_header_menu_font_weight" => '',
				"vcop_header_menu_text_transform" => '',
				"vcop_header_menu_text_decoration" => '',
				"vcop_header_menu_font_size" => '', 
				"vcop_header_menu_font_color" => '',
				"vcop_header_menu_font_color_link" => '', 
				"vcop_header_menu_font_color_over_link" => '',				
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800'), 
				$atts)
	);
	
	wp_enqueue_style( 'vcop-header' );
	wp_enqueue_style( 'vcop-menu' );
	wp_enqueue_script( 'vcop-menu-js' );
	wp_enqueue_script( 'vcop-mobile-menu-js' );
	wp_enqueue_script( 'vcop-menu-modernizr-js' );
	wp_enqueue_script( 'vcop-header-js' );

	if($vcop_page_font_type == 'vcop-google-font') {

			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_page_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_page_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/

			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');	

	}
	
	$return = '<style type="text/css">';
	
	if($vcop_layout_type == 'vcop-boxed') {
		if($vcop_boxed_background_type == 'color') {
			$return .= 'body {
				background:'.$vcop_boxed_background_color.';
			}';
		} else {
			$vcop_boxed_background_image_url = wp_get_attachment_image_src( $vcop_boxed_background_image , 'full');
			$return .= 'body {
				background:url('.$vcop_boxed_background_image_url[0].') fixed center;
				background-size:cover;
			}';			
		}
		$return .= 'body {
				width:'.$vcop_boxed_wrapper_width.'px;
				margin:50px auto;			
			}';	
	}
	
	
	if($vcop_page_font_type == 'vcop-google-font') {
		$return .=  'body {
								font-family:'.$font_name_slip.';
								font-style:'.$font_style.';
								font-weight:'.$font_weight.';
						  }';	
	}		
	
	$return .= 'body {
					color:'.$vcop_page_font_color.';
				}
				a {
					color:'.$vcop_page_font_color_link.';
				}
				a:hover {
					color:'.$vcop_page_font_color_over_link.';
				}';

	$return .= '.vcop-header { 
			background:'.$vcop_header_background_color.'; 
			padding:'.$vcop_header_padding.';
			margin:'.$vcop_header_margin.';		
	}
	.vcop-logo {
			text-align:'.$vcop_header_logo_align.';
	}			
	.vcop-menu {
			color:'.$vcop_header_menu_font_color.';
			text-align:'.$vcop_header_menu_text_align.';
	}
	.vcop-menu .cbp-tm-menu > li > a {
			font-weight:'.$vcop_header_menu_font_weight.';
			font-size:'.$vcop_header_menu_font_size.';
			text-transform:'.$vcop_header_menu_text_transform.';
			text-decoration:'.$vcop_header_menu_text_decoration.'!important;
			color:'.$vcop_header_menu_font_color_link.';
	}
	.vcop-menu a:hover {
			color:'.$vcop_header_menu_font_color_over_link.'!important;
	}';
	
	if($vcop_layout_type == 'vcop-boxed') {
		$return .= 'body .vcop-header {
				width:'.$vcop_boxed_wrapper_width.'px!important;		
				left:50%!important;
				margin-left:-'.($vcop_boxed_wrapper_width/2).'px!important;
				top:50px;
		}';	
		$return .= '@media screen and (max-width: 1023px) {
			body {
				width:100%;
				margin:0 auto;
			}
			body .vcop-header {
				width:100%!important;		
				left:0%!important;
				margin-left:0!important;
				top:0px;
			}
		}';
	}
		
	$return .= '</style>';
	
	$return .= "<script type=\"text/javascript\">
			jQuery(function($){
				$(document).ready(function(){
					$('#cbp-tm-menu').mobileMenu({topOptionText: '".__('Select Page','vconepage')."', groupPageText: '".__('Main','vconepage')."',switchWidth: 1023});
				});
			});
	</script>";
	
	$logo_image_url = wp_get_attachment_image_src( $vcop_header_logo , 'medium');	
	
	if($vcop_header_logo_link == 'home-page') {
		$logo_url = get_home_url();
	} elseif ($vcop_header_logo_link == 'same-page') {
		$logo_url = '';
	} elseif ($vcop_header_logo_link == 'custom-url') {
		$logo_url = $vcop_header_logo_custom_url;
	}
	
	$vcop_header_type_value = $vcop_header_type;
	
	if($vcop_header_type == 'vcmp-header-sticky' || $vcop_header_type == 'vcmp-header-sticky-v2') {
		$vcop_header_sticky_active = 'vcmp-header-sticky-container';
	} else {
		$vcop_header_sticky_active = '';
	}

	$return .= '<header class="vcop-header '.$vcop_layout_type.' '.$vcop_header_fixed_relative_position.' '.$vcop_header_position.' '.$vcop_header_logo_position.' '.$vcop_header_sticky_active.' '.$vcop_header_type.'">';
	
	if($vcop_header_position == 'vcop-header-top' || $vcop_header_position == 'vcop-header-bottom') {
		if(($vcop_header_logo_position == 'vcop-logo-left' || $vcop_header_logo_position == 'vcop-logo-top') && $vcop_header_logo_active == 'on') {
			$return .= '<div class="vcop-logo"><a href="'.$logo_url.'" target="'.$vcop_header_logo_target.'"><img src="'.$logo_image_url[0].'"></a></div>';
		}
		$return .= '<div class="vcop-menu">';
		if ( function_exists( 'wp_nav_menu' ) && has_nav_menu($vcop_header_menu_list) )  {
					$return .= 	wp_nav_menu(array(
							'container'       => 'ul', 
							'theme_location'  => $vcop_header_menu_list,
							'container_class' => false, 
							'container_id'    => false,
							'menu_class'      => 'cbp-tm-menu', 
							'fallback_cb'	  => 'nav_fallback',
							'menu_id'         => 'cbp-tm-menu',
							'echo'			  => false, 
						)
						);
					 }
		$return .= '</div>';
		$return .= '<div id="vcop-menu-responsive"></div>';		
		if(($vcop_header_logo_position == 'vcop-logo-right' || $vcop_header_logo_position == 'vcop-logo-bottom') && $vcop_header_logo_active == 'on') {
			$return .= '<div class="vcop-logo"><a href="'.$logo_url.'" target="'.$vcop_header_logo_target.'"><img src="'.$logo_image_url[0].'"></a></div>';
		}					 
	}
	
	$return .= '<div class="vcop-clear"></div></header>';
	
	
	/* HEADER BOTTOM */
	
	if($vcop_header_position == 'vcop-header-bottom' && $vcop_header_type == 'vcmp-header-sticky') {
		
	$return .= '<header class="vcop-header '.$vcop_layout_type.' vcop-header-sticky vcop-header-fixed '.$vcop_header_position.' '.$vcop_header_logo_position.' '.$vcop_header_type.'">';
	
	if($vcop_header_position == 'vcop-header-top' || $vcop_header_position == 'vcop-header-bottom') {
		if(($vcop_header_logo_position == 'vcop-logo-left' || $vcop_header_logo_position == 'vcop-logo-top') && $vcop_header_logo_active == 'on') {
			$return .= '<div class="vcop-logo"><a href="'.$logo_url.'" target="'.$vcop_header_logo_target.'"><img src="'.$logo_image_url[0].'"></a></div>';
		}
		$return .= '<div class="vcop-menu">';
		if ( function_exists( 'wp_nav_menu' ) && has_nav_menu($vcop_header_menu_list) )  {
					$return .= 	wp_nav_menu(array(
							'container'       => 'ul', 
							'theme_location'  => $vcop_header_menu_list,
							'container_class' => false, 
							'container_id'    => false,
							'menu_class'      => 'cbp-tm-menu', 
							'fallback_cb'	  => 'nav_fallback',
							'menu_id'         => 'cbp-tm-menu',
							'echo'			  => false, 
						)
						);
					 }
		$return .= '</div>';
		$return .= '<div id="vcop-menu-responsive"></div>';
		if(($vcop_header_logo_position == 'vcop-logo-right' || $vcop_header_logo_position == 'vcop-logo-bottom') && $vcop_header_logo_active == 'on') {
			$return .= '<div class="vcop-logo"><a href="'.$logo_url.'" target="'.$vcop_header_logo_target.'"><img src="'.$logo_image_url[0].'"></a></div>';
		}					 
	}
	
	$return .= '<div class="vcop-clear"></div></header>';		
		
		
	}
	
	/* #HEADER BOTTOM */
	
	
	
	if($instance == '1') {
		return $return;
	}

	} 
}
new vcop_header_function();
?>