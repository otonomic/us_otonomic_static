<?php
/*
File: postlist.php
Description: Function Post list
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_postlist_function extends vconepage_postlist_class {
public function vconepage_postlist_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_postlist_type" => '',				
				"vcop_postlist_show_title" => '',
				"vcop_postlist_show_image" => '',
				"vcop_postlist_show_excerpt" => '',
				"vcop_postlist_excerpt_number" => '300',
				"vcop_postlist_show_button" => '',				
				
				"vcop_postlist_style_type" => '',
				"vcop_postlist_date" => '',
				"vcop_postlist_date_format" => '',
				"vcop_postlist_comments" => '',
				"vcop_postlist_author" => '',
				"vcop_postlist_category" => '',
				"vcop_postlist_views" => '',
				"vcop_postlist_social" => '',
				
				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',
				
				"vcop_postlist_font_type" => '',
				"vcop_postlist_google_fonts" => '',
				"vcop_postlist_title_fs" => '',
				"vcop_postlist_font_color" => '',
				"vcop_postlist_a_color" => '',												
				"vcop_postlist_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_postlist_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_postlist_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_postlist_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_postlist_font_type == 'vcop-google-font') {
				$return .=  '.vcop-postlist-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_postlist_style($instance, 
							$vcop_postlist_type, 
							$vcop_postlist_title_fs,  
							$vcop_postlist_font_color, 
							$vcop_postlist_a_color, 
							$vcop_postlist_over_color);
						
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );			
			wp_enqueue_style( 'vcop-postlist' );			
			
			$return .= '<div class="vcop-postlist vcop-'.$instance.' '.$vcop_postlist_type.' vcop-postlist-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
			$return .= '"><ul>';
			
			// LOOP QUERY
			$query = vcop_query($vcop_query_source,
								$vcop_query_sticky_posts, 
								$vcop_query_posts_type, 
								$vcop_query_categories, 
								$vcop_query_order, 
								$vcop_query_orderby, 
								$vcop_query_pagination, 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				
				$return .= '<li class="'.$vcop_postlist_style_type.'">';

				$return .= vconepage_post_info($vcop_postlist_date,$vcop_postlist_comments,$vcop_postlist_author,$vcop_postlist_category,$vcop_postlist_views,$vcop_query_source,$vcop_query_posts_type,$vcop_postlist_date_format);				 		
				
				if($vcop_postlist_show_title == 'true') {
					$return .= '<a class="vcop-postlist-title" href="'.$link.'">'.get_the_title().'</a>';
				}
				if($vcop_postlist_show_image == 'true') {
					$return .= '<div class="vcop-postlist-image">'.vconepage_thumbs('vconepage-rectangle').'</div>';
				}
				if($vcop_postlist_show_excerpt == 'true') {
					$return .= '<p class="vcop-postlist-text">'.vconepage_excerpt($vcop_postlist_excerpt_number).'';
				}
				if($vcop_postlist_show_button == 'true') {
					$return .= '<a class="vcop-postlist-readmore" href="'.$link.'">'.__('Read More','vconepage').'</a>';
				}
				if($vcop_postlist_show_excerpt == 'true') {
					$return .= '</p>';
				}
				$return .= '</li>';
				
				
				
			endwhile;
			} // #LOOP QUERY				
			
										
			$return .= '</ul>';

				// PAGINATION 
				if($vcop_query_pagination == 'yes') {
					$return .= '<div class="vcop-clear"></div><div class="vcop-pagination">';
					if($vcop_query_pagination_type == 'numeric') {
						$return .= vcop_numeric_pagination($pages = '', $range = 2,$loop);
					} else {
						$return .= get_next_posts_link( 'Older posts', $loop->max_num_pages );
						$return .= get_previous_posts_link( 'Newer posts' );				
					}
					$return .= '</div>';
				}
				// #PAGINATION			
			
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_postlist_function();
?>