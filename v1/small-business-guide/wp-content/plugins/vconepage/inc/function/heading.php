<?php
/*
File: heading.php
Description: Function Heading
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_heading_function extends vconepage_heading_class {
public function vconepage_heading_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_heading_type" => '',				
				"vcop_heading_text" => '',
				"vcop_heading_font_type" => '',
				"vcop_heading_google_fonts" => '',
				"vcop_heading_size" => '',
				"vcop_heading_line_height" => '',
				"vcop_heading_align" => '',
				"vcop_heading_display" => '',
				"vcop_heading_text_color" => '',
				"vcop_heading_line_color" => '',
				"vcop_heading_icon_color" => '',
				"vcop_heading_line_separator" => '',
				"vcop_heading_icon_size" => '',
				"vcop_heading_icon" => '', 
				"vcop_heading_icon_position" => '',   
				"vcop_animate" => '',
				"vcop_animate_effect" => '',
				"vcop_delay" => '' 
				), 
				$atts)
	);
	if($vcop_heading_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_heading_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_heading_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
	}		
	
			$return = '<style type="text/css">';
	
			if(empty($vcop_heading_line_height) || $vcop_heading_line_height == '') {
				$vcop_heading_line_height = '1.3';	
			}
	
			if($vcop_heading_font_type == 'vcop-google-font') {		
					$return .= '.'.$vcop_heading_type.'.heading-'.$instance.' {						
									font-family:'.$font_name_slip.';
									font-style:'.$font_style.';
									font-weight:'.$font_weight.';
							  }';
			}
	
			$return .= '.'.$vcop_heading_type.'.heading-'.$instance.' {
				font-size:'.$vcop_heading_size.';
				color:'.$vcop_heading_text_color.';
				display:'.$vcop_heading_display.';
				margin:2px 0; 
				line-height:'.$vcop_heading_line_height.';
			}';
	
			if($vcop_heading_display == 'inline-block') {
				$return .= 	'.'.$vcop_heading_type.'.heading-'.$instance.' {
					padding:0 5px;		
				}';
			}
	
			if($vcop_heading_type == 'double-line-top' || 
			   $vcop_heading_type == 'double-line-middle' || 
			   $vcop_heading_type == 'double-line-bottom') {				  
				$return .= '.'.$vcop_heading_type.'.heading-'.$instance.' span:before, 
							.'.$vcop_heading_type.'.heading-'.$instance.' span:after {
									border-top-color:'.$vcop_heading_line_color.';
									border-bottom-color:'.$vcop_heading_line_color.';
							}';
			}
			
			if($vcop_heading_type == 'line-top-bottom') {
				$return .= '.line-top-bottom.heading-'.$instance.' span {
					border-top-color:'.$vcop_heading_line_color.';
					border-bottom-color:'.$vcop_heading_line_color.';
				}';
			}

			if($vcop_heading_type == 'dashed-bottom') {
				$return .= '.dashed-bottom.heading-'.$instance.' .vcop-separator {
					background-color:'.$vcop_heading_line_color.';
				}';
			}

			if($vcop_heading_type == 'bottom-line-with-icon') {
				$return .= '.bottom-line-with-icon.heading-'.$instance.' span:before, 
							.bottom-line-with-icon.heading-'.$instance.' span:after {
								border-color:'.$vcop_heading_line_color.';
							}
							.bottom-line-with-icon.heading-'.$instance.' i {
								color:'.$vcop_heading_line_color.';
							}
							';
			}
			
			if($vcop_heading_type == 'heading-with-icon') {
				
				$margin_left = explode("px",$vcop_heading_icon_size);
				
				$return .= '.heading-with-icon.heading-'.$instance.' i {
								color:'.$vcop_heading_icon_color.';								
							}
							.heading-with-icon.heading-'.$instance.' span i::before {
								font-size:'.$vcop_heading_icon_size.'!important;
								margin-left:-'.($margin_left[0]/2).'px!important;
								line-height:'.$vcop_heading_icon_size.'!important;
								width:'.$vcop_heading_icon_size.'!important;
							}
							';
			}
					
			$return .= '</style>';
			
			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_style( 'vcop-heading' );		

			if($vcop_animate == 'on') { // ANIMATION ON
				
				if($vcop_heading_type == 'normal' || $vcop_heading_type == 'line-top-bottom') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'"><span>'.$vcop_heading_text.'</span></h2>';				
				
				} elseif($vcop_heading_type == 'dashed-bottom') {
					$return .= '<h2 class="'.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
									<span>'.$vcop_heading_text.'</span>
									<span class="vcop-separator '.$vcop_heading_line_separator.'"></span>
								</h2>';	
															
				} elseif($vcop_heading_type == 'bottom-line-with-icon') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
									<span>'.$vcop_heading_text.'<i class="'.$vcop_heading_icon.'"></i></span>
								</h2>';								
				
				} elseif($vcop_heading_type == 'heading-with-icon') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' '.$vcop_heading_icon_position.' heading-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
									<span>';
								if(	$vcop_heading_icon_position == 'vcop-icon-left' ) {
										$return .= '<i class="'.$vcop_heading_icon.'"></i>';
								}
								
									$return .= $vcop_heading_text;
									
								if($vcop_heading_icon_position == 'vcop-icon-right' || 
								   $vcop_heading_icon_position == 'vcop-icon-bottom') {
										$return .= '<i class="'.$vcop_heading_icon.'"></i>';									
								}
					$return .= '</span></h2>';
													
				} else {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' heading-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'"><span>'.$vcop_heading_text.'</span></h2>';				
				
				}
				
			} else { // ANIMATION OFF
				
				if($vcop_heading_type == 'normal' || $vcop_heading_type == 'line-top-bottom') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.'"><span>'.$vcop_heading_text.'</span></h2>';									
				
				} elseif($vcop_heading_type == 'dashed-bottom') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.'">
									<span>'.$vcop_heading_text.'</span>
									<span class="vcop-separator '.$vcop_heading_line_separator.'"></span>
								</h2>';								
				
				} elseif($vcop_heading_type == 'bottom-line-with-icon') {
					$return .= '<h2 class="'.$vcop_heading_type.' '.$vcop_heading_align.' heading-'.$instance.'">
									<span>'.$vcop_heading_text.'<i class="'.$vcop_heading_icon.'"></i></span>
								</h2>';								
				
				} elseif($vcop_heading_type == 'heading-with-icon') {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' '.$vcop_heading_align.' '.$vcop_heading_icon_position.' heading-'.$instance.'">
									<span>';
								if(	$vcop_heading_icon_position == 'vcop-icon-left' ) {
										$return .= '<i class="'.$vcop_heading_icon.'"></i>';
								}
								
									$return .= $vcop_heading_text;
									
								if($vcop_heading_icon_position == 'vcop-icon-right' || 
								   $vcop_heading_icon_position == 'vcop-icon-bottom') {
										$return .= '<i class="'.$vcop_heading_icon.'"></i>';									
								}
					$return .= '</span></h2>';
													
				} else {
					
					$return .= '<h2 class="vcop-heading '.$vcop_heading_type.' heading-'.$instance.'"><span>'.$vcop_heading_text.'</span></h2>';				
				
				}
				
			} // #ANIMATION
			return $return;
    } 
}
new vcop_heading_function();
?>