<?php
/*
File: icon.php
Description: Function icon
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_icon_function extends vconepage_icon_class {
public function vconepage_icon_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_icon" => 'adtheme-icon-home', 
				"vcop_size" => '15',
				"vcop_icon_float" => 'none',
				"vcop_icon_align" => '',
				"vcop_icon_color" => '#000FFF',
				"vcop_icon_over_color" => '#000FFF',  
				"vcop_margin" => '20px', 
				"vcop_padding" => '20px',
				"vcop_link" => 'off',
				"vcop_link_url" => '',
				"vcop_target" => '_blank', 
				"vcop_background_active" => 'off',
				"vcop_background_color" => '',
				"vcop_background_type" => 'square',
				"vcop_border_active" => 'off',
				"vcop_border_color" => '',
				"vcop_border_width" => '',
				"vcop_border_style" => '',
				"vcop_background_border_type" => '', 
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	
	wp_enqueue_style( 'vcop-icon' );
	
	$return = '';
	
	if($vcop_background_active == 'on' || $vcop_border_active == 'on') {
	
		$return .= '<style type="text/css">';
	
			if($vcop_background_active == 'on') {		
				$return .= '.adtheme-icon-'.$instance.' i {
						background:'.$vcop_background_color.';
				}';
				if($vcop_background_type == 'round') {
					$return .= '.adtheme-icon-'.$instance.' i {
							border-radius:50%;
					}';
				}
			}
			if($vcop_border_active == 'on') {
					$return .= 	'.adtheme-icon-'.$instance.' i {
							border:'.$vcop_border_width.' '.$vcop_border_style.' '.$vcop_border_color.';
					}';
					$return .= 	'.adtheme-icon-'.$instance.' i:hover {
							border:'.$vcop_border_width.' '.$vcop_border_style.' '.$vcop_icon_over_color.';
					}';					
				if($vcop_background_border_type == 'round') {
					$return .= '.adtheme-icon-'.$instance.' i {
							border-radius:50%;
					}
					';
				}	
			}
	
		$return .= '</style>';
	
	}
		if($vcop_animate == 'on') {
			$return .= '<span class="adtheme-icon adtheme-icon-'.$instance.' '.$vcop_icon_align.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';			
				if($vcop_link == 'on') {
					$return .= '<a href="'.$vcop_link_url.'" target="'.$vcop_target.'">';
				}
				$return	.= '<i class="'.$vcop_icon.'" style="font-size:'.$vcop_size.';color:'.$vcop_icon_color.';margin:'.$vcop_margin.';padding:'.$vcop_padding.';display:inline-block;float:'.$vcop_icon_float.';" onMouseOver="this.style.color=\''.$vcop_icon_over_color.'\'" onMouseOut="this.style.color=\''.$vcop_icon_color.'\'"></i>';
				if($vcop_link == 'on') {
					$return .= '</a>';
				}
			$return .= '</span>';
		} else {
			$return .= '<span class="adtheme-icon adtheme-icon-'.$instance.' '.$vcop_icon_align.'">';			
				if($vcop_link == 'on') {
					$return .= '<a href="'.$vcop_link_url.'" target="'.$vcop_target.'">';
				}		
				$return	.= '<i class="'.$vcop_icon.'" style="font-size:'.$vcop_size.';color:'.$vcop_icon_color.';margin:'.$vcop_margin.';padding:'.$vcop_padding.';display:inline-block;float:'.$vcop_icon_float.';" onMouseOver="this.style.color=\''.$vcop_icon_over_color.'\'" onMouseOut="this.style.color=\''.$vcop_icon_color.'\'"></i>';
				if($vcop_link == 'on') {
					$return .= '</a>';
				}
			$return .= '</span>';		
		}
		return $return;
    } 
}
new vcop_icon_function();
?>