<?php
/*
File: buttons.php
Description: Function buttons
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_buttons_function extends vconepage_buttons_class {
public function vconepage_buttons_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_button_type" => 'flat-button-mini', 
				"vcop_button_text" => '',
				"vcop_button_url" => '',
				"vcop_button_target_url" => '',
				"vcop_button_align" => '',
				"vcop_button_custom_padding" => '',
				"vcop_button_custom_margin" => '',
				"vcop_button_custom_border_radius" => '',
				"vcop_button_display" => '', 
				"vcop_button_icon_show" => '',
				"vcop_button_icon" => '',  
				"vcop_button_font_type" => '', 
				"vcop_button_google_fonts" => '',
				"vcop_button_size" => 'off',
				"vcop_button_text_color" => '',
				"vcop_button_over_color" => '',
				"vcop_button_background_color" => '',
				"vcop_button_border_color" => '',
				"vcop_button_border_type" => '',
				"vcop_button_border_style" => '',
				"vcop_button_custom_border_active" => '',
				"vcop_button_custom_border_color" => '',
				"vcop_button_custom_border_width" => '', 
				"vcop_button_custom_border_style" => '',
				"vcop_button_custom_text_over_color" => '',
				"vcop_button_custom_background_over_color" => '',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	if($vcop_button_font_type == 'vcop-google-font') {

			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_button_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_button_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/

			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');	

	}

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	
	wp_enqueue_style( 'vcop-buttons' );	
	
	$return = '<style type="text/css">';
	if($vcop_button_font_type == 'vcop-google-font') {
		$return .=  '.vcop-button.vcop-button-'.$instance.' a {
								font-size:'.$vcop_button_size.'; 
								font-family:'.$font_name_slip.';
								font-style:'.$font_style.';
								font-weight:'.$font_weight.';
						  }';	
	} else {
		$return .=  '.vcop-button.vcop-button-'.$instance.' a,
		 			 .vcop-button.vcop-button-'.$instance.' i {
								font-size:'.$vcop_button_size.'; 
						  }';		
	}
	$return .= '.vcop-button.vcop-button-'.$instance.' {
					display:'.$vcop_button_display.';
				}
				.vcop-button.vcop-button-'.$instance.' i {
					font-size:'.$vcop_button_size.'; 
				}';
	if($vcop_button_type == 'flat-button-mini' || $vcop_button_type == 'flat-button-small' || $vcop_button_type == 'flat-button-medium' || $vcop_button_type == 'flat-button-large') {								
		$return .= '.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a {
						color:'.$vcop_button_text_color.';
						background:'.$vcop_button_background_color.';
						border-color:'.$vcop_button_background_color.';
						border-style:'.$vcop_button_border_style.';
					}
					.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a:hover {
						color:'.$vcop_button_background_color.';
						background:'.$vcop_button_text_color.';
						border-color:'.$vcop_button_text_color.';					
					}
					';
	};
	if($vcop_button_type == 'bg-none-button-mini' || $vcop_button_type == 'bg-none-button-small' || $vcop_button_type == 'bg-none-button-medium' || $vcop_button_type == 'bg-none-button-large') {								
		$return .= '.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a {
						color:'.$vcop_button_text_color.';
						border-color:'.$vcop_button_text_color.';
						border-style:'.$vcop_button_border_style.';
					}
					.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a:hover {
						color:'.$vcop_button_over_color.';
						border-color:'.$vcop_button_over_color.';					
					}
					';
	};
	if($vcop_button_type == 'd-button-mini' || $vcop_button_type == 'd-button-small' || $vcop_button_type == 'd-button-medium' || $vcop_button_type == 'd-button-large') {								
		
		if($vcop_button_type == 'd-button-mini') { $shadow_size = '4'; }
		if($vcop_button_type == 'd-button-small') { $shadow_size = '6'; }
		if($vcop_button_type == 'd-button-medium') { $shadow_size = '8'; }
		if($vcop_button_type == 'd-button-large') { $shadow_size = '10'; }
		
		
		$return .= '.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a {
						color:'.$vcop_button_text_color.';
						background:'.$vcop_button_background_color.';
						box-shadow: 0 '.$shadow_size.'px 0 '.$vcop_button_border_color.';
					}
					.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a:hover {
						box-shadow: 0 2px 0 '.$vcop_button_border_color.';					
					}
					';
	};
	if($vcop_button_type == 'button-custom') {
		$return .= '.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a {
						color:'.$vcop_button_text_color.';
						background:'.$vcop_button_background_color.';
						padding:'.$vcop_button_custom_padding.';
						margin:'.$vcop_button_custom_margin.';
						border-radius:'.$vcop_button_custom_border_radius.';
					}
					.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a:hover {
						color:'.$vcop_button_custom_text_over_color.';
						background:'.$vcop_button_custom_background_over_color.';
						border-color:'.$vcop_button_text_color.';					
					}
					';
		if($vcop_button_custom_border_active == 'on') {
			$return .= '.vcop-button.'.$vcop_button_type.'.vcop-button-'.$instance.' a {
						border-color:'.$vcop_button_custom_border_color.';	
						border-width:'.$vcop_button_custom_border_width.';
						border-style:'.$vcop_button_custom_border_style.';			
			}';
		}
	}
	$return .= '</style>';
	
	$vcop_inline_block = '';
	if($vcop_button_display == 'inline-block') {
		$vcop_inline_block .= 'vcop-inline-block';
	}
	
		if($vcop_animate == 'on') {
			$return .= '<span class="vcop-button '.$vcop_button_type.' '.$vcop_inline_block.' '.$vcop_button_border_type.' '.$vcop_button_align.' vcop-button-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
				$return .= '<a href="'.$vcop_button_url.'" target="'.$vcop_button_target_url.'">';
				
				if($vcop_button_icon_show == 'vcop-icon-show') { 
					$return	.= '<i class="'.$vcop_button_icon.'"></i>';
				}			
					
				$return .= $vcop_button_text . '</a>';
			$return .= '</span>';
		} else {
			$return .= '<span class="vcop-button '.$vcop_button_type.' '.$vcop_inline_block.' '.$vcop_button_border_type.' '.$vcop_button_align.' vcop-button-'.$instance.'">';
				$return .= '<a href="'.$vcop_button_url.'" target="'.$vcop_button_target_url.'">';
				
				if($vcop_button_icon_show == 'vcop-icon-show') {
					$return	.= '<i class="'.$vcop_button_icon.'"></i>';
				}				
					
				$return .= $vcop_button_text . '</a>';
			$return .= '</span>';	
		}
		return $return;
    } 
}
new vcop_buttons_function();
?>