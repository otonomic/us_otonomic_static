<?php
/*
File: space.php
Description: Function space
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_space_function extends vconepage_space_class {
public function vconepage_space_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_height" => '300px',
				), 
				$atts)
	);
		
		$return = '<div class="vcop-space" style="height:'.$vcop_height.'"></div>';
		
		return $return;
	} 
}
new vcop_space_function();
?>