<?php
/*
File: social.php
Description: Function Social
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_social_function extends vconepage_social_class {
public function vconepage_social_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_social_type" 					=> '',				
				"vcop_social_fb" 					=> '',
				"vcop_social_tw" 					=> '',
				"vcop_social_in" 					=> '', 
				"vcop_social_gplus" 				=> '', 
				"vcop_social_pi" 					=> '', 
				"vcop_social_yt" 					=> '', 
				"vcop_social_fl" 					=> '', 
				"vcop_social_tu" 					=> '',
				"vcop_social_rss" 					=> '',				
				"vcop_social_vi" 					=> '',
				"vcop_social_email" 				=> '',
				"vcop_social_share_fb" 				=> '',
				"vcop_social_share_tw" 				=> '',
				"vcop_social_share_in" 				=> '', 
				"vcop_social_share_gplus" 			=> '', 
				"vcop_social_share_pi" 				=> '',				
				"vcop_social_style" 				=> 'style1',
				"vcop_social_color"					=> '',
				"vcop_social_over_color" 			=> '',
				"vcop_social_style_type"			=> '',
				"vcop_social_style_font_size"		=> '',
				"vcop_social_style_margin" 			=> '',															
				"vcop_animate" 			=> 'on',
				"vcop_animate_effect" 	=> 'fade-in',
				"vcop_delay" 			=> '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	
	
	wp_enqueue_style( 'vcop-social' );
		
		$return = '';

		$return .= '<style type="text/css">';
		$return .= '.social-'.$instance.' a {
					color:'.$vcop_social_color.';					
		}';
		$return .= '.social-'.$instance.' a:hover {
					color:'.$vcop_social_over_color.';					
		}';		
			if($vcop_social_style_type == 'custom') {
					$return .= '.social-'.$instance.' .ad-social a {
						margin:'.$vcop_social_style_margin.';
						font-size:'.$vcop_social_style_font_size.';					
					}';					
			}
		$return .= '</style>';
		
		
		if($vcop_animate == 'on') {
			$return .=  '<div class="social-'.$instance.' social-'.$vcop_social_style.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';   
		} else {
			$return .= '<div class="social-'.$instance.' social-'.$vcop_social_style.'">';	
		}

		$return .= '<div class="ad-social adtheme-icon">';

		/* SOCIAL BUTTON */

		if($vcop_social_type == 'social-button') {

			if($vcop_social_style == 'style1') {
		
				if($vcop_social_fb != '') {
						$return .= '<span class="ad-fb"><a class="adtheme-icon-facebook" href="'.$vcop_social_fb.'"></a></span>';
				}
				if($vcop_social_tw != '') {
						$return .= '<span class="ad-tw"><a class="adtheme-icon-twitter" href="'.$vcop_social_tw.'"></a></span>';
				}
				if($vcop_social_in != '') {
						$return .= '<span class="ad-in"><a class="adtheme-icon-linkedin2" href="'.$vcop_social_in.'"></a></span>';
				}	
				if($vcop_social_gplus != '') {
						$return .= '<span class="ad-gplus"><a class="adtheme-icon-googleplus" href="'.$vcop_social_gplus.'"></a></span>';
				}
				if($vcop_social_pi != '') {
						$return .= '<span class="ad-pi"><a class="adtheme-icon-pinterest3" href="'.$vcop_social_pi.'"></a></span>';
				}	
				if($vcop_social_yt != '') {
						$return .= '<span class="ad-yt"><a class="adtheme-icon-youtube2" href="'.$vcop_social_yt.'"></a></span>';
				}
				if($vcop_social_fl != '') {
						$return .= '<span class="ad-fl"><a class="adtheme-icon-flickr" href="'.$vcop_social_yt.'"></a></span>';
				}	
				if($vcop_social_tu != '') {
						$return .= '<span class="ad-tu"><a class="adtheme-icon-tumblr" href="'.$vcop_social_tu.'"></a></span>';
				}
				if($vcop_social_rss != '') {
						$return .= '<span class="ad-rss"><a class="adtheme-icon-feed2" href="'.$vcop_social_rss.'"></a></span>';
				}
				if($vcop_social_vi != '') {
						$return .= '<span class="ad-vimeo"><a class="adtheme-icon-vimeo" href="'.$vcop_social_vi.'"></a></span>';
				}		
				if($vcop_social_email != '') {
						$return .= '<span class="ad-co"><a class="adtheme-icon-mail2" href="mailto'.$vcop_social_email.'"></a></span>';
				}						
			
			}

			if($vcop_social_style == 'style2') {
				
				if($vcop_social_fb != '') {
						$return .= '<span class="ad-fb"><a class="adtheme-icon-facebook2" href="'.$vcop_social_fb.'"></a></span>';
				}
				if($vcop_social_tw != '') {
						$return .= '<span class="ad-tw"><a class="adtheme-icon-twitter2" href="'.$vcop_social_tw.'"></a></span>';
				}
				if($vcop_social_in != '') {
						$return .= '<span class="ad-in"><a class="adtheme-icon-linkedin" href="'.$vcop_social_in.'"></a></span>';
				}	
				if($vcop_social_gplus != '') {
						$return .= '<span class="ad-gplus"><a class="adtheme-icon-googleplus3" href="'.$vcop_social_gplus.'"></a></span>';
				}
				if($vcop_social_pi != '') {
						$return .= '<span class="ad-pi"><a class="adtheme-icon-pinterest2" href="'.$vcop_social_pi.'"></a></span>';
				}	
				if($vcop_social_yt != '') {
						$return .= '<span class="ad-yt"><a class="adtheme-icon-youtube" href="'.$vcop_social_yt.'"></a></span>';
				}
				if($vcop_social_fl != '') {
						$return .= '<span class="ad-fl"><a class="adtheme-icon-flickr3" href="'.$vcop_social_yt.'"></a></span>';
				}	
				if($vcop_social_tu != '') {
						$return .= '<span class="ad-tu"><a class="adtheme-icon-tumblr2" href="'.$vcop_social_tu.'"></a></span>';
				}
				if($vcop_social_rss != '') {
						$return .= '<span class="ad-rss"><a class="adtheme-icon-feed3" href="'.$vcop_social_rss.'"></a></span>';
				}
				if($vcop_social_vi != '') {
						$return .= '<span class="ad-vimeo"><a class="adtheme-icon-vimeo2" href="'.$vcop_social_vi.'"></a></span>';
				}		
				if($vcop_social_email != '') {
						$return .= '<span class="ad-co"><a class="adtheme-icon-mail" href="mailto'.$vcop_social_email.'"></a></span>';
				}						
			
			}
				
		} else { /* SOCIAL SHARE */
			
				global $post;
				$pinterestimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	

				if($vcop_social_style == 'style1') {
	
					if($vcop_social_share_fb == 'yes') {	
					
					$return .= '<a target="_blank" class="adtheme-icon-facebook2" href="http://www.facebook.com/sharer.php?u='.get_permalink().'&amp;t='.get_the_title().'" title="'.__('Click to share this post on Facebook','vconepage').'"></a>';
					
					}
					if($vcop_social_share_tw == 'yes') {	
					
					$return .= '<a target="_blank" class="adtheme-icon-twitter" href="http://twitter.com/home?status='.get_permalink().'" title="'. __('Click to share this post on Twitter','vconepage').'"></a>';
					
					}
					if($vcop_social_share_gplus == 'yes') {		
					
					$return .= '<a target="_blank" class="adtheme-icon-googleplus2" href="https://plus.google.com/share?url='.get_permalink().'" title="'. __('Click to share this post on Google+','vconepage').'"></a>';
			
					}
					if($vcop_social_share_in == 'yes') {
			
					$return .= '<a target="_blank" class="adtheme-icon-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'" title="'. __('Click to share this post on Linkedin','vconepage').'"></a>';
			
					}
					if($vcop_social_share_pi == 'yes') {
			
					$return .= '<a target="_blank" class="adtheme-icon-pinterest2" href="http://pinterest.com/pin/create/button/?url='.urlencode(get_permalink($post->ID)).'&media='.$pinterestimage[0].'&description='.get_the_title().'" title="'. __('Click to share this post on Pinterest','vconepage').'"></a>';
			
					}			
				}

				if($vcop_social_style == 'style2') {
	
					if($vcop_social_share_fb == 'yes') {	
					
					$return .= '<a target="_blank" class="adtheme-icon-facebook" href="http://www.facebook.com/sharer.php?u='.get_permalink().'&amp;t='.get_the_title().'" title="'.__('Click to share this post on Facebook','vconepage').'"></a>';
					
					}
					if($vcop_social_share_tw == 'yes') {	
					
					$return .= '<a target="_blank" class="adtheme-icon-twitter2" href="http://twitter.com/home?status='.get_permalink().'" title="'. __('Click to share this post on Twitter','vconepage').'"></a>';
					
					}
					if($vcop_social_share_gplus == 'yes') {		
					
					$return .= '<a target="_blank" class="adtheme-icon-googleplus" href="https://plus.google.com/share?url='.get_permalink().'" title="'. __('Click to share this post on Google+','vconepage').'"></a>';
			
					}
					if($vcop_social_share_in == 'yes') {
			
					$return .= '<a target="_blank" class="adtheme-icon-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'" title="'. __('Click to share this post on Linkedin','vconepage').'"></a>';
			
					}
					if($vcop_social_share_pi == 'yes') {
			
					$return .= '<a target="_blank" class="adtheme-icon-pinterest" href="http://pinterest.com/pin/create/button/?url='.urlencode(get_permalink($post->ID)).'&media='.$pinterestimage[0].'&description='.get_the_title().'" title="'. __('Click to share this post on Pinterest','vconepage').'"></a>';
			
					}			
				}
				
		}
		
		
		$return .= '</div>'; /* #DIV SOCIAL */

		$return .= '</div>'; /* #DIV ANIMATION */
		
			
		return $return;
    } 
}
new vcop_social_function();
?>