<?php
/*
File: backtotop.php
Description: Function backtotop
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_backtotop_function extends vconepage_backtotop_class {
public function vconepage_backtotop_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_backtotop_style" 				=> '',
				"vcop_backtotop_type" 				=> '',
				"vcop_backtotop_icon" 				=> '',
				"vcop_backtotop_text" 				=> '',
				"vcop_backtotop_font_type" 			=> '',
				"vcop_backtotop_google_fonts" 		=> '',
				"vcop_backtotop_icon_size" 			=> '',
				"vcop_backtotop_size" 				=> '',
				"vcop_backtotop_text_color" 		=> '',
				"vcop_backtotop_background_color" 	=> '',
				"vcop_backtotop_padding" 			=> '',
				"vcop_backtotop_position" 			=> '',
				"vcop_backtotop_right" 				=> '',
				"vcop_backtotop_left" 				=> '',
				"vcop_backtotop_bottom" 			=> '',
				"vcop_backtotop_border"				=> '',
				"vcop_backtotop_border_round_value" => '',												
				), 
				$atts)
	);

	if($vcop_backtotop_font_type == 'vcop-google-font') {

			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_backtotop_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_backtotop_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/

			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');	

	}
		
		$return = '<style type="text/css">
				#vcop-backtop .vcop-backtop {
					width: auto;
					color:'.$vcop_backtotop_text_color.';
					background: '.$vcop_backtotop_background_color.';
					text-align: center;
					padding: '.$vcop_backtotop_padding.';
					position: fixed; 
					bottom: '.$vcop_backtotop_bottom.';
					cursor: pointer;
					display: none;
					font-size: '.$vcop_backtotop_size.';
					-webkit-transition: all 0.7s ease;
  					-moz-transition: all 0.7s ease;
  					-o-transition: all 0.7s ease;
  					transition: all 0.7s ease;						
				}	
				#vcop-backtop .vcop-backtop i {
					font-size: '.$vcop_backtotop_icon_size.';
					line-height:1;
				}';
		if($vcop_backtotop_position == 'right') {
			$return .= '#vcop-backtop .vcop-backtop {
					right: '.$vcop_backtotop_right.';
			}';
		} else {
			$return .= '#vcop-backtop .vcop-backtop {
					left: '.$vcop_backtotop_left.';
			}';			
		}	
		
		$return .= '#vcop-backtop .vcop-backtop:hover {
					color:'.$vcop_backtotop_background_color.';
					background: '.$vcop_backtotop_text_color.';
					-webkit-transition: all 0.7s ease;
  					-moz-transition: all 0.7s ease;
  					-o-transition: all 0.7s ease;
  					transition: all 0.7s ease;						
		}';
		
		if($vcop_backtotop_font_type == 'vcop-google-font') {
		$return .=  '#vcop-backtop .vcop-backtop {
								font-family:'.$font_name_slip.';
								font-style:'.$font_style.';
								font-weight:'.$font_weight.';
						  }';	
		}		

		if($vcop_backtotop_border == 'round') {
		$return .=  '#vcop-backtop .vcop-backtop {
								border-radius:'.$vcop_backtotop_border_round_value.';
						  }';	
		}
			
		$return .= '</style>';
		
		$return .= "<script type=\"text/javascript\">			
			jQuery(document).ready(function($){
				$(window).scroll(function(){
					if ($(this).scrollTop() > 50) {
						$('#vcop-backtop .vcop-backtop').fadeIn('slow');
					} else {
						$('#vcop-backtop .vcop-backtop').fadeOut('slow');
					}
				});
				$('#vcop-backtop .vcop-backtop').click(function(){
					$(\"html, body\").animate({ scrollTop: 0 }, 500);        
					return false;
				});
			});
			</script>";
		
		$return .= '<div class="'.$vcop_backtotop_style.'" id="vcop-backtop">
        				<a href="#" class="vcop-backtop">';
		if($vcop_backtotop_type == 'vcop-backtotop-icon-text' || $vcop_backtotop_type == 'vcop-backtotop-icon') {
				$return .= '<i class="'.$vcop_backtotop_icon.'"></i>';
		}
		if($vcop_backtotop_type == 'vcop-backtotop-icon-text' || $vcop_backtotop_type == 'vcop-backtotop-text') {
				$return .= $vcop_backtotop_text;
		}			
		$return .= '</a>
         	</div> ';
		if($instance == '1') {
			return $return;
		}
	} 
}
new vcop_backtotop_function();
?>