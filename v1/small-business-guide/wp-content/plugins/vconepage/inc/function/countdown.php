<?php
/*
File: countdown.php
Description: Function Countdown
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_countdown_function extends vconepage_countdown_class {
public function vconepage_countdown_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_countdown_year" => '',
				"vcop_countdown_month" => '',
				"vcop_countdown_day" => '',	
				"vcop_countdown_type" => '',
				"vcop_countdown_style" => 'style1',
				"vcop_countdown_style_active" => 'off',
				"vcop_countdown_color" => '',
				"vcop_countdown_secondary_color" => '',
				"vcop_countdown_background_color" => '',
				"vcop_countdown_font_size" => '',
				"vcop_countdown_padding" => '',
				"vcop_countdown_margin" => '',			
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	
	
	wp_enqueue_style( 'vcop-countdown' );
	wp_enqueue_script( 'vcop-countdown-js' );
		
		$return = '';
		
		if($vcop_countdown_type == 'type1') {		
			$return = '<script type=\'text/javascript\'>
						jQuery(document).ready(function($){
							$(\'[data-countdown]\').each(function() {
								var $this = $(this), finalDate = $(this).data(\'countdown\');
								$this.countdown(finalDate, function(event) {
									$this.html(event.strftime(\'<span class="vcop-day">%D days</span> <span class="vcop-time">%H:%M:%S</span>\'));
								});
							});
						});	
					   </script>';			   
		}
		
		if($vcop_countdown_type == 'type2') {
			$return = '<script type=\'text/javascript\'>
						jQuery(document).ready(function($){
							$(\'[data-countdown]\').each(function() {
								var $this = $(this), finalDate = $(this).data(\'countdown\');
								$this.countdown(finalDate, function(event) {
									$this.html(event.strftime(\'\'
										+ \'<span>%-w week%!w</span>\'
										+ \'<span>%-d day%!d</span>\'
										+ \'<span>%H hr</span>\'
										+ \'<span>%M min</span>\'
										+ \'<span>%S sec</span>\'));
								});
							});
						});	
					   </script>';
		}

		if($vcop_countdown_type == 'type3') {
			$return = '<script type=\'text/javascript\'>
						jQuery(document).ready(function($){
							$(\'#clock-'.$instance.'\').countdown("'.$vcop_countdown_year.'/'.$vcop_countdown_month.'/'.$vcop_countdown_day.'", function(event) {
							  var totalHours = event.offset.totalDays * 24 + event.offset.hours;
							  $(this).html(event.strftime(\'<span>\' + totalHours + \' hr</span> <span>%M min</span> <span>%S sec</span>\'));
							});
						});	
					   </script>';
		}
		
		if($vcop_countdown_style_active == 'on') {
			if($vcop_countdown_type == 'type1') {
				$return .= '<style type="text/css">';
				$return .= '.countdown-'.$instance.' .'.$vcop_countdown_type.'.'.$vcop_countdown_style.' { 
								font-size:'.$vcop_countdown_font_size.'; 
								color:'.$vcop_countdown_secondary_color.' 
							}'; 
				$return .= '.countdown-'.$instance.' .'.$vcop_countdown_type.'.'.$vcop_countdown_style.' .vcop-day, 
							.countdown-'.$instance.' .'.$vcop_countdown_type.'.'.$vcop_countdown_style.' .vcop-time {
								border-color:'.$vcop_countdown_color.';	
								padding:'.$vcop_countdown_padding.';
								background:'.$vcop_countdown_background_color.';
								margin:'.$vcop_countdown_margin.';								
							}';
				$return .= '</style>';		
			}
			if($vcop_countdown_type == 'type2' || $vcop_countdown_type == 'type3') {
				$return .= '<style type="text/css">';
				$return .= '.countdown-'.$instance.' .'.$vcop_countdown_type.'.'.$vcop_countdown_style.' { 
								font-size:'.$vcop_countdown_font_size.'; 
								color:'.$vcop_countdown_secondary_color.' 
							}'; 
				$return .= '.countdown-'.$instance.' .'.$vcop_countdown_type.'.'.$vcop_countdown_style.' span {
								border-color:'.$vcop_countdown_color.';	
								padding:'.$vcop_countdown_padding.';
								background:'.$vcop_countdown_background_color.';
								margin:'.$vcop_countdown_margin.';
							}';
				$return .= '</style>';		
			}		
		}
		
		if($vcop_animate == 'on') {
			$return .=  '<div class="countdown-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';   
		} else {
			$return .= '<div class="countdown-'.$instance.'">';	
		}
		$return .= '<div id="clock-'.$instance.'" class="vcop_clock '.$vcop_countdown_type.' '.$vcop_countdown_style.'" data-countdown="'.$vcop_countdown_year.'/'.$vcop_countdown_month.'/'.$vcop_countdown_day.'"></div><div class="adclear"></div>';	
		$return .= '</div>';
		
			
		return $return;
    } 
}
new vcop_countdown_function();
?>