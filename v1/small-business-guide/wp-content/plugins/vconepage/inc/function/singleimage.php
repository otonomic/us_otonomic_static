<?php
/*
File: singleimage.php
Description: Function singleimage
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_singleimage_function extends vconepage_singleimage_class {
public function vconepage_singleimage_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_singleimage_image" => '', 
				"vcop_singleimage_link" => '', 
				"vcop_singleimage_size" => '', 
				"vcop_singleimage_align" => '',
				"vcop_singleimage_url" => '', 
				"vcop_singleimage_target" => '',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800'), 
				$atts)
	);
	
	$image_url = wp_get_attachment_image_src( $vcop_singleimage_image, $vcop_singleimage_size );
	
	$return = '<style type="text/css">
			.vcop-image-'.$instance.' {
				text-align:'.$vcop_singleimage_align.';
				display:block;
				margin:40px 0;
			}
			.vcop-image-'.$instance.' img {
				max-width:100%;
				height:auto;				
			}
			</style>';
		
	if($vcop_animate == 'on') {
		$return .= '<span class="vcop-image vcop-image-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
	} else {
		$return .= '<span class="vcop-image vcop-image-'.$instance.'">';
	}
	
	if($vcop_singleimage_link == 'on') {
		$return .= '<a href="'.$vcop_singleimage_url.'" target="'.$vcop_singleimage_target.'">';
	}
	
	$return .= '<img src="'.$image_url[0].'" width="'.$image_url[1].'" height="'.$image_url[2].'">';
	
	if($vcop_singleimage_link == 'on') {
		$return .= '</a>';         
	}
	
	$return .= '</span>';
	return $return;
	
	
	
	} 
}
new vcop_singleimage_function();
?>