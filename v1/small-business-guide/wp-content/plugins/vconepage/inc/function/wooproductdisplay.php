<?php
/*
File: wooproductdisplay.php
Description: Function wooproductdisplay
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_wooproductdisplay_function extends vconepage_wooproductdisplay_class {
public function vconepage_wooproductdisplay_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_wooproductdisplay_type" => '',
				"vcop_wooproductdisplay_columns" => '',
				"vcop_wooproductdisplay_filter" => 'false',
				"vcop_wooproductdisplay_posts_cart" => '',
				"vcop_wooproductdisplay_posts_plus" => '',	
				"vcop_wooproductdisplay_link" => '',
				"vcop_wooproductdisplay_target_link" => '',
				"vcop_wooproductdisplay_images" => '',				

				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',
				
				"vcop_wooproductdisplay_font_type" => '',
				"vcop_wooproductdisplay_google_fonts" => '',
				"vcop_wooproductdisplay_title_fs" => '',
				"vcop_wooproductdisplay_background_color" => '',
				"vcop_wooproductdisplay_secondary_color" => '',
				"vcop_wooproductdisplay_font_color" => '',
				"vcop_wooproductdisplay_a_color" => '',												
				"vcop_wooproductdisplay_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_wooproductdisplay_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_wooproductdisplay_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_wooproductdisplay_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_wooproductdisplay_font_type == 'vcop-google-font') {
				$return .=  '.vcop-wooproductdisplay-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_wooproductdisplay_style($instance,
							$vcop_wooproductdisplay_type, 
							$vcop_wooproductdisplay_title_fs,  
							$vcop_wooproductdisplay_background_color, 
							$vcop_wooproductdisplay_secondary_color, 
							$vcop_wooproductdisplay_font_color, 
							$vcop_wooproductdisplay_a_color, 
							$vcop_wooproductdisplay_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );				
			wp_enqueue_style( 'vcop-wooproductdisplay' );						

			if($vcop_wooproductdisplay_filter == 'true') {
				
				wp_enqueue_script( 'vcop-filter-js' );	
				
				$return .= vcop_filter_js($instance,'wooproductdisplay');
				
				$return .= vcop_filter_item($instance,
											'vcop-wooproductdisplay',
											'wp_custom_posts_type',
											$vcop_query_categories,
											'product');	
			
			}				
			
			$return .= '<div class="vcop-wooproductdisplay vcop-'.$instance.' '.$vcop_wooproductdisplay_type.' vcop-wooproductdisplay-'.$instance.' vcop-columns-'.$vcop_wooproductdisplay_columns.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
				if($vcop_wooproductdisplay_filter == 'true') {
					$return .= ' vcop-filter';	
				}				
			$return .= '">';
			
			/**************************************************/
			/****************** POSTS SOURCE ******************/
			/**************************************************/
			
			// LOOP QUERY
			$query = vcop_query_woocommerce($vcop_query_categories, 
											$vcop_query_order, 
											$vcop_query_orderby, 
											$vcop_query_pagination, 
											$vcop_query_number, 
											$vcop_query_posts_for_page,
											$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				$id_post = get_the_id();
				$product = new WC_Product( $id_post );
				$price = $product->regular_price;
				$price_sales = $product->sale_price;
				$symbol = get_woocommerce_currency_symbol();
								
				if($vcop_wooproductdisplay_filter == 'true') {
					
					$return .= vcop_filter_item_div('wp_custom_posts_type',$vcop_query_categories,'product');
					
				} else {
				
					$return .= '<div class="vcop-item">';
				
				}
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				$return .= vconepage_thumbs('vconepage-rectangle');
				
				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style4' || $vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style5') {
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-item-sale">'.__('Sale','vconepage').'</span>';	
					}
					
					$return .= '<h3 class="vcop-price"><span class="vcop-regular-price';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= ' vcop-line-through';	
					}
					
					$return .= '">'. $price . $symbol .'</span>';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-sale-price">'. $price_sales . $symbol .'</span>';
					}
					$return .= '</h3>';
				}
				
				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style2' || $vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style4' || $vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style5') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				$return .= '</div>';
				if($vcop_wooproductdisplay_posts_cart == 'cart_show' && $vcop_wooproductdisplay_posts_plus == 'plus_show' && $vcop_wooproductdisplay_type != 'vcop-wooproductdisplay-style6') {
					$return .= '<div class="vcop-image-over vcop-both-icon">';	
				} else {
					$return .= '<div class="vcop-image-over">';
				}
				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style3') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				if($vcop_wooproductdisplay_posts_cart == 'cart_show' && $vcop_wooproductdisplay_type != 'vcop-wooproductdisplay-style6') {			
					$return .= '<a class="add_to_cart_button product_type_simple added adtheme-icon-cart" data-product_sku="" data-product_id="'.$id_post.'" rel="nofollow" href="?add-to-cart='.$id_post.'"></a>';
				}
				if($vcop_wooproductdisplay_posts_plus == 'plus_show') {			
					$return .= '<a href="'.$link.'" class="adtheme-icon-plus"></a>';
				}
								
				$return .= '</div>';										
				$return .=	'</div>';
				
				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style6') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}				
														
				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style5' || $vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style6') {
					$return .= '<div class="vcop-product-excerpt">'.get_the_excerpt().'</div>';					
				}

				if($vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style6') {
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-item-sale">'.__('Sale','vconepage').'</span>';	
					}
					
					$return .= '<h3 class="vcop-price"><span class="vcop-regular-price';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= ' vcop-line-through';	
					}
					
					$return .= '">'. $price . $symbol .'</span>';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-sale-price">'. $price_sales . $symbol .'</span>';

					}
					if($vcop_wooproductdisplay_posts_cart == 'cart_show' && $vcop_wooproductdisplay_type == 'vcop-wooproductdisplay-style6') {			
								$return .= '<a class="add_to_cart_button product_type_simple added adtheme-icon-cart" data-product_sku="" data-product_id="'.$id_post.'" rel="nofollow" href="?add-to-cart='.$id_post.'"></a>';
					}						
					$return .= '</h3>';										
				}
				

								
				$return .= '</div>'; // #vcop-ITEM
			endwhile;
			} // #LOOP QUERY				

				// PAGINATION 
				if($vcop_query_pagination == 'yes') {
					$return .= '<div class="vcop-clear"></div><div class="vcop-wooproductdisplay-'.$instance.' vcop-pagination">';
					if($vcop_query_pagination_type == 'numeric') {
						$return .= vcop_numeric_pagination($pages = '', $range = 2,$loop);
					} else {
						$return .= get_next_posts_link( 'Older posts', $loop->max_num_pages );
						$return .= get_previous_posts_link( 'Newer posts' );				
					}
					$return .= '</div>';
				}
				// #PAGINATION
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_wooproductdisplay_function();
?>