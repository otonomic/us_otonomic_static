<?php
/*
File: list.php
Description: Function list
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_list_function extends vconepage_list_class {
public function vconepage_list_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_list" => 'adtheme-icon-home', 
				"vcop_list_float" => 'none',
				"vcop_list_link" => '',
				"vcop_list_link_url" => '',
				"vcop_list_link_target" => '',
				"vcop_list_title_text" => '',
				"vcop_list_content_text" => '',
				
				"vcop_list_font_type" => '',
				"vcop_list_google_fonts" => '',				
				"vcop_list_font_align" => '',
				"vcop_list_title_size" => '20px',								 
				"vcop_list_title_color" => '#000FFF',
				"vcop_list_content_size" => '15px',								 
				"vcop_list_content_color" => '#000FFF',				
				"vcop_list_icon_size" => '50px',
				"vcop_list_icon_color" => '',				
				"vcop_list_icon_over_color" => '#000FFF', 			 
				"vcop_list_icon_margin" => '5px', 
				"vcop_list_icon_padding" => '5px',
  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	

	if($vcop_list_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_list_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_list_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
	
		$return = '<style type="text/css">';
		if($vcop_list_font_type == 'vcop-google-font') {
				$return .=  '.vcop_list-'.$instance.' .vcop_list_text { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
		}			
		$return .=	'.vcop_list_title_text {
						display:block;
					}
					.vcop_list-'.$instance.' {
						text-align:'.$vcop_list_font_align.';
					}
					.vcop_list-'.$instance.' .vcop_list_text .vcop_list_title_text {
						font-size:'.$vcop_list_title_size.';
						color:'.$vcop_list_title_color.';	
					}					
					.vcop_list-'.$instance.' .vcop_list_text .vcop_list_content_text {
						font-size:'.$vcop_list_content_size.';
						color:'.$vcop_list_content_color.';	
					}';			
		$return .= '</style>';
	
		if($vcop_animate == 'on') {
					
			$return .= '<div class="vcop_list-'.$instance.' adtheme-icon animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';							
				if($vcop_list_link == 'on') {
					$return .= '<a href="'.$vcop_list_link_url.'" target="'.$vcop_list_link_target.'">';
				}
				$return	.= '<i class="'.$vcop_list.'" style="font-size:'.$vcop_list_icon_size.';color:'.$vcop_list_icon_color.';margin:'.$vcop_list_icon_margin.';padding:'.$vcop_list_icon_padding.';display:inline-block;float:'.$vcop_list_float.';" onMouseOver="this.style.color=\''.$vcop_list_icon_over_color.'\'" onMouseOut="this.style.color=\''.$vcop_list_icon_color.'\'"></i>';
				if($vcop_list_link == 'on') {
					$return .= '</a>';
				}			
			
			$return .= '<div class="vcop_list_text">
							<span class="vcop_list_title_text">'.$vcop_list_title_text.'</span>
							<span class="vcop_list_content_text">'.$vcop_list_content_text.'</span>
						</div>';
			
			$return .= '</div>';
			
		} else {
			
			$return .= '<div class="vcop_list-'.$instance.' adtheme-icon">';			
				if($vcop_list_link == 'on') {
					$return .= '<a href="'.$vcop_list_link_url.'" target="'.$vcop_list_link_target.'">';
				}
				$return	.= '<i class="'.$vcop_list.'" style="font-size:'.$vcop_list_icon_size.';color:'.$vcop_list_icon_color.';margin:'.$vcop_list_icon_margin.';padding:'.$vcop_list_icon_padding.';display:inline-block;float:'.$vcop_list_float.';" onMouseOver="this.style.color=\''.$vcop_list_icon_over_color.'\'" onMouseOut="this.style.color=\''.$vcop_list_icon_color.'\'"></i>';
				if($vcop_list_link == 'on') {
					$return .= '</a>';
				}			
			
			$return .= '<div class="vcop_list_text">
							<span class="vcop_list_title_text">'.$vcop_list_title_text.'</span>
							<span class="vcop_list_content_text">'.$vcop_list_content_text.'</span>
						</div>';
			
			$return .= '</div>';		
		}
		return $return;
    } 
}
new vcop_list_function();
?>