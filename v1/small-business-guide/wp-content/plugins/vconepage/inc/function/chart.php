<?php
/*
File: chart.php
Description: Function chart
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_chart_function extends vconepage_chart_class {
public function vconepage_chart_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_type" => 'pie_chart', 
				"vcop_bar_color" => '#FC615D', 
				"vcop_track_color" => '#000FFF', 
				"vcop_percent" => '50', 
				"vcop_bar_width" => '300', 
				"vcop_pie_width" => '300', 
				"vcop_style" => 'style1',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-chart' );
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_script( 'jquery' );	
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );
	wp_enqueue_script( 'vcop-chart-js' );	
	
	// PIE CHART
	
	if($vcop_type == 'pie_chart') {
		$responsive_on = '';
		if(empty($vcop_bar_width) || $vcop_bar_width == '') { $vcop_bar_width = '300'; }
		if($vcop_bar_width >= 300) { $responsive_on = 'vcop_chart_responsive'; }
		if($vcop_animate == 'on') {
			$return = '<div class="chart-container animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
		} else {
			$return = '<div class="chart-container">';
		}
		
		$return .= '<div class="chart-item-container hidden '.$responsive_on.'"><div class="chart rand'.$instance.' easyPieChart" data-percent="'.$vcop_percent.'" data-color="'.$vcop_bar_color.'" data-colortrack="'.$vcop_track_color.'"><span class="percent">'.$vcop_percent.'</span></div></div></div>';
		$return .= '<style type="text/css">
		.chart.rand'.$instance.' {
			width: '.$vcop_bar_width.'px;
			height: '.$vcop_bar_width.'px;
		}
		.rand'.$instance.' .percent {
			line-height: '.$vcop_bar_width.'px;
			color:'.$vcop_bar_color.';
		}
		</style>';		
	
		$return .= "<script>
		jQuery(document).ready(function($){
			jQuery('.chart-container').appear(function() {
				var easy_pie_chart = {};
				jQuery('.chart-item-container').removeClass('hidden');
				jQuery('.chart.rand".$instance."').each(function() {
				var text_span = jQuery(this).children('span');
				jQuery(this).easyPieChart(jQuery.extend(true, {}, easy_pie_chart, {
				size : ".$vcop_bar_width.",
				animate : 2000,
				lineWidth : 6,
				lineCap : 'square',
				barColor : jQuery(this).data('color'),
				lineWidth : 10,
				trackColor : jQuery(this).data('colortrack'),
				scaleColor : false,
				onStep : function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				}
				}));
			});	 
		 });
		}); 				
		</script>
	";
	
	}
	
	// BAR CHART
	
	if($vcop_type == 'progress_bar') {
		
		if($vcop_bar_width != '100%') { $vcop_bar_width = $vcop_bar_width.'px'; }	
		$return = '<style type="text/css">
		#bar-'.$instance.' .bar {
			background:'.$vcop_bar_color.';
		}
		#bar-'.$instance.'.bar-main-container {
			width:'.$vcop_pie_width.';
		}
		#bar-'.$instance.'.style1 .bar-percentage {
			background:'.$vcop_track_color.';
		}
		#bar-'.$instance.'.style2 .bar-percentage {
			color:'.$vcop_track_color.';
		}				
		</style>';
		$return .= "
		<script>
		jQuery(document).ready(function($){
			jQuery('#bar-".$instance."').appear(function() {
				$('.bar-percentage[data-percentage]').each(function () {
				  var progress = $(this);
				  var percentage = Math.ceil($(this).attr('data-percentage'));
				  $({countNum: 0}).animate({countNum: percentage}, {
					duration: 2000,
					easing:'linear',
					step: function() {
					  // What todo on every count
					var pct = '';
					if(percentage == 0){
					  pct = Math.floor(this.countNum) + '%';
					}else{
					  pct = Math.floor(this.countNum+1) + '%';
					}
					progress.text(pct) && progress.siblings().children().css('width',pct);
					}
				  });
				});
			});
		});
		</script>
		";
		if($vcop_animate == 'on') {
			$return .= '<div id="bar-'.$instance.'" class="bar-main-container azure '.$vcop_style.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
		} else {
			$return .= '<div id="bar-'.$instance.'" class="bar-main-container azure '.$vcop_style.'">';
		}
			$return .=	'<div class="wrap">
				  <div class="bar-percentage" data-percentage="'.$vcop_percent.'"></div>
				  <div class="bar-container">
					<div class="bar"></div>
				  </div>
				</div>
			  </div>';	
		
		}
		return $return;	
    } 
}
new vcop_chart_function();
?>