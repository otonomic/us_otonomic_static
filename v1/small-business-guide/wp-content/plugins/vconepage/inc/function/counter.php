<?php
/*
File: counter.php
Description: Function counter
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_counter_function extends vconepage_counter_class {
public function vconepage_counter_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_counter_type" => '',
				"vcop_counter_number" => '',
				"vcop_counter_text_active" => '', 
				"vcop_counter_text" => '',
				"vcop_counter_align" => '',
				"vcop_counter_icon" => '', 
				"vcop_counter_icon_style" => '', 
				"vcop_counter_font_type" => '', 
				"vcop_counter_google_fonts" => '',
				"vcop_counter_size" => '',
				"vcop_counter_text_size" => '',
				"vcop_counter_text_color" => '',
				"vcop_counter_icon_size" => '',
				"vcop_counter_main_color" => '',
				"vcop_counter_icon_color" => '',
				"vcop_counter_background_icon_color" => '',
				"vcop_counter_line_color" => '',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	if($vcop_counter_font_type == 'vcop-google-font') {

			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_counter_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_counter_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/

			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');	

	}

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );
	wp_enqueue_script( 'vcop-counter-js' );		
	wp_enqueue_style( 'vcop-counter' );	
	
	$return = '<style type="text/css">';
	if($vcop_counter_font_type == 'vcop-google-font') {
		$return .=  '.container-counter.vcop-counter-'.$instance.' {
								font-size:'.$vcop_counter_size.'; 
								font-family:'.$font_name_slip.';
								font-style:'.$font_style.';
								font-weight:'.$font_weight.';
						  }';	
	} else {
		$return .=  '.container-counter.vcop-counter-'.$instance.' {
								font-size:'.$vcop_counter_size.'; 
						  }';		
	}
	$return .=  '.container-counter.vcop-counter-'.$instance.' i {
			font-size:'.$vcop_counter_icon_size.';
	}';
	if($vcop_counter_text_active == 'text-show') {
		$return .= '.container-counter.vcop-counter-'.$instance.' .counter-text {
								font-size:'.$vcop_counter_text_size.';
								color:'.$vcop_counter_text_color.'; 
		}';
	}
	if($vcop_counter_type == 'counter-icon') {
		if($vcop_counter_icon_style == 'counter-icon-no-bg') {
			$return .= '.container-counter.vcop-counter-'.$instance.' i {
				color:'.$vcop_counter_icon_color.';
			}';			
		} else {
			$return .= '.container-counter.vcop-counter-'.$instance.' i {
				background:'.$vcop_counter_background_icon_color.';
				color:'.$vcop_counter_icon_color.';
			}';
		}
	}	
	$return .= '.container-counter.vcop-counter-'.$instance.' .timer {
			color:'.$vcop_counter_main_color.'; 
	}';
	if($vcop_counter_type == 'counter-line') {
		$return .= '.container-counter.vcop-counter-'.$instance.' .counter-line {
			background:'.$vcop_counter_line_color.';
		}';
	}	
	$return .= '</style>';
	
	$return .= '<script type="text/javascript">
	jQuery(function($){
					/* COUNTER */
				  // custom formatting example 
				$(\'.container-counter\').appear(function() {
					  $(\'#counter'.$instance.'\').data(\'countToOptions\', {
						formatter: function (value, options) {
						  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, \'\');
						} 
					  });			  				  				  
					  // start all the timers
					  $(\'.timer\').each(count);
					  
					  function count(options) {
						var $this = $(this);
						options = $.extend({}, options || {}, $this.data(\'countToOptions\') || {});
						$this.countTo(options);
					  }				
					});	
	});		
					</script>';
					
	
	
		if($vcop_animate == 'on') {

			$return .= '<div class="container-counter '.$vcop_counter_align.' vcop-counter-'.$instance.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';

				if($vcop_counter_type == 'counter-icon') {	
						$return .= '<i class="'.$vcop_counter_icon.' '.$vcop_counter_icon_style.'"></i>';
				}
			
			$return .= '<span class="timer" id="counter'.$instance.'" data-to="'.$vcop_counter_number.'" data-speed="10000"></span>';

				if($vcop_counter_type == 'counter-line') {	
						$return .= '<span class="counter-line"></span>';
				}
				
				if($vcop_counter_text_active == 'text-show') {	
						$return .= '<span class="counter-text">'.$vcop_counter_text.'</span>';
				}
			
			$return .= '</div>';			

		} else {

			$return .= '<div class="container-counter '.$vcop_counter_align.' vcop-counter-'.$instance.'">';

				if($vcop_counter_type == 'counter-icon') {	
						$return .= '<i class="'.$vcop_counter_icon.' '.$vcop_counter_icon_style.'"></i>';
				}
			
			$return .= '<span class="timer" id="counter'.$instance.'" data-to="'.$vcop_counter_number.'" data-speed="10000"></span>';

				if($vcop_counter_type == 'counter-line') {	
						$return .= '<span class="counter-line"></span>';
				}
				
				if($vcop_counter_text_active == 'text-show') {	
						$return .= '<span class="counter-text">'.$vcop_counter_text.'</span>';
				}
			
			$return .= '</div>';	
	
		}
		return $return;
    } 
}
new vcop_counter_function();
?>