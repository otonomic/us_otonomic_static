<?php
/*
File: timeline.php
Description: Function timeline
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_timeline_function extends vconepage_timeline_class {
public function vconepage_timeline_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_timeline_type" => '',				
				"vcop_timeline_source" => '',
				"vcop_timeline_date_format" => '',
				"vcop_timeline_show_title" => '',
				"vcop_timeline_show_image" => '',
				"vcop_timeline_show_excerpt" => '',
				"vcop_timeline_excerpt_number" => '300',
				"vcop_timeline_show_description" => '',
				"vcop_timeline_show_button" => '',
				"vcop_timeline_link" => '',
				"vcop_timeline_target_link" => '',
				"vcop_timeline_images" => '',				

				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',
				
				"vcop_timeline_font_type" => '',
				"vcop_timeline_google_fonts" => '',
				"vcop_timeline_title_fs" => '',
				"vcop_timeline_background_color" => '',
				"vcop_timeline_line_color" => '',
				"vcop_timeline_font_color" => '',
				"vcop_timeline_a_color" => '',												
				"vcop_timeline_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_timeline_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_timeline_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_timeline_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_timeline_font_type == 'vcop-google-font') {
				$return .=  '.vcop-timeline-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_timeline_style($instance, 
							$vcop_timeline_type, 
							$vcop_timeline_title_fs,  
							$vcop_timeline_background_color, 
							$vcop_timeline_line_color, 
							$vcop_timeline_font_color, 
							$vcop_timeline_a_color, 
							$vcop_timeline_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_script( 'vcop-magnific-popup-js' );		
			wp_enqueue_style( 'vcop-timeline' );
			wp_enqueue_style( 'vcop-magnific-popup' );	
			
			if($vcop_timeline_link != 'custom_url') {
				$return .= "<script type=\"text/javascript\">	
								jQuery(document).ready(function($){
									$('.vcop-timeline-".$instance." .vcop-zoom-image').magnificPopup({
										type: 'image',
											gallery:{
												enabled:true
											}
									});
								});
							</script>";
			}
			
			if($vcop_timeline_type == 'vcop_gallery') {
				wp_enqueue_style( 'vcop-owl-carousel' );
				wp_enqueue_style( 'vcop-owl-theme' );
				wp_enqueue_script( 'vcop-owl-carousel-js' );
				$return .= "<script type=\"text/javascript\">	
								jQuery(document).ready(function($){
									$('.vcop-timeline-".$instance." .vcop-zoom-image').magnificPopup({
										type: 'image',
											gallery:{
												enabled:true
											}
									});
								});
							</script>";			
			}
			
			
			
			$return .= '<div class="vcop-timeline vcop-'.$instance.' '.$vcop_timeline_source.' '.$vcop_timeline_type.' vcop-timeline-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
			$return .= '"><section id="vcop-timeline" class="vcop-container">';
	
			/**************************************************/
			/**************** GALLERY SOURCE ******************/
			/**************************************************/
			
			if($vcop_timeline_source == 'vcop_gallery') {
			
			$images = explode( ',', $vcop_timeline_images );
	
			foreach ( $images as $id ) {			
				$image_attributes = wp_get_attachment_image_src( $id, 'vconepage-fullscreen' );
					if($vcop_timeline_link == 'custom_url') {					
						$link = get_post_meta( $id, '_custom_url', true );
						$target = 'target="'.$vcop_timeline_target_link.'"';						
					} else {
						$link = $image_attributes[0];
						$target = '';
					}
				$attachment_caption_array = get_post( $id );
				$attachment_caption	= $attachment_caption_array->post_excerpt;
				$attachment_description = $attachment_caption_array->post_content;
				$attachment_date = $attachment_caption_array->post_date;
				if(empty($attachment_caption)) { $attachment_caption = ' '; }
				
				$return .= '<div class="vcop-timeline-block">
								<div class="vcop-timeline-img animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
									<div class="vcop_format_icon"><span class="adtheme-icon-image2"></span></div>
								</div>';
				$return .= '<div class="vcop-timeline-content animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
				if($vcop_timeline_show_title == 'true') {
						$return .= '<h2><a href="'.$link.'" '.$target.'>'.$attachment_caption.'</a></h2>';
				}
				if($vcop_timeline_show_image == 'true') {
						$return .= '<div class="vcop-timeline-image">
										<img class="vcop-thumbs" src="'.$image_attributes[0].'">
										<div class="vcop-timeline-zoom">
											<a class="adtheme-icon-search vcop-zoom-image" href="'.$link.'" '.$target.'></a>
										</div>
									</div>';
				}
				if($vcop_timeline_show_description == 'true') {
					$return .= '<p>'.$attachment_description.'</p>';
				}				
				$return .= '<span class="vcop-date">'.get_the_date($vcop_timeline_date_format).'</span>
							</div>
							</div>';		
				
				}
			
			} else {
			
			/**************************************************/
			/****************** POSTS SOURCE ******************/
			/**************************************************/
			
			// LOOP QUERY
			$query = vcop_query($vcop_query_source,
								$vcop_query_sticky_posts, 
								$vcop_query_posts_type, 
								$vcop_query_categories, 
								$vcop_query_order, 
								$vcop_query_orderby, 
								'no', 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				
				$return .= '<div class="vcop-timeline-block">
								<div class="vcop-timeline-img animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
									<div class="vcop_format_icon">'.vconepage_format_icon().'</div>
								</div>';
				$return .= '<div class="vcop-timeline-content animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">';
				if($vcop_timeline_show_title == 'true') {
						$return .= '<h2><a href="'.$link.'">'.get_the_title().'</a></h2>';
				}
				if($vcop_timeline_show_image == 'true') {
						$return .= vconepage_content();
				}
				if($vcop_timeline_show_excerpt == 'true') {
					$return .= '<p>'.vconepage_excerpt($vcop_timeline_excerpt_number).'</p>';
				}
				if($vcop_timeline_show_button == 'true') {
					$return .= '<a href="'.$link.'" class="vcop-read-more">'.__('Read More','vconepage').'</a>';
				}
				
				$return .= '<span class="vcop-date">'.get_the_date($vcop_timeline_date_format).'</span>
							</div>
							</div>';			

			endwhile;
			} // #LOOP QUERY				
			
			} // #POSTS SOURCE
										
			$return .= '</section></div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_timeline_function();
?>