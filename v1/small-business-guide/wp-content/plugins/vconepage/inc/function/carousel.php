<?php
/*
File: carousel.php
Description: Function carousel
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_carousel_function extends vconepage_carousel_class {
public function vconepage_carousel_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_carousel_type" => '',
				"vcop_carousel_source" => '',
				"vcop_carousel_posts_zoom" => '',
				"vcop_carousel_posts_plus" => '',	
				"vcop_carousel_link" => '',
				"vcop_carousel_target_link" => '',
				"vcop_carousel_images" => '',				

				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',

				"vcop_carousel_item" => '',
				"vcop_carousel_navigation" => '',
				"vcop_carousel_pagination" => '',
				"vcop_carousel_autoplay" => '',
				"vcop_carousel_item_padding" => '', 
				
				"vcop_carousel_font_type" => '',
				"vcop_carousel_google_fonts" => '',
				"vcop_carousel_title_fs" => '',
				"vcop_carousel_background_color" => '',
				"vcop_carousel_secondary_color" => '',
				"vcop_carousel_font_color" => '',
				"vcop_carousel_a_color" => '',												
				"vcop_carousel_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_carousel_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_carousel_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_carousel_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_carousel_font_type == 'vcop-google-font') {
				$return .=  '.vcop-carousel-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_carousel_style($instance,
							$vcop_carousel_item_padding, 
							$vcop_carousel_type, 
							$vcop_carousel_title_fs,  
							$vcop_carousel_background_color, 
							$vcop_carousel_secondary_color, 
							$vcop_carousel_font_color, 
							$vcop_carousel_a_color, 
							$vcop_carousel_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_script( 'vcop-magnific-popup-js' );			
			wp_enqueue_style( 'vcop-carousel' );			
			wp_enqueue_style( 'vcop-magnific-popup' );
			wp_enqueue_style( 'vcop-owl-carousel' );
			wp_enqueue_style( 'vcop-owl-theme' );
			wp_enqueue_script( 'vcop-owl-carousel-js' );			

			if(empty($vcop_carousel_autoplay) || $vcop_carousel_autoplay == '0' || $vcop_carousel_autoplay == '') {
				$vcop_carousel_autoplay = 'false';
			}

			$return .= '
			<script>
			jQuery(document).ready(function($){
      			$(".vcop-carousel-'.$instance.'").owlCarousel({
					items : '.$vcop_carousel_item.',
					autoPlay: '.$vcop_carousel_autoplay.',
					navigation: '.$vcop_carousel_navigation.',
					pagination: '.$vcop_carousel_pagination.',
					itemsTablet: [1025,2],
					itemsMobile: [479,1],
					navigationText: [\'<i class="adtheme-icon-arrow-left"></i>\',\'<i class="adtheme-icon-arrow-right"></i>\'] 				
				});
			});
			</script>';
					
			if($vcop_carousel_link != 'custom_url') {
				$return .= "<script type=\"text/javascript\">	
								jQuery(document).ready(function($){
									$('.vcop-carousel-".$instance." .vcop-zoom-image').magnificPopup({
										type: 'image',
											gallery:{
												enabled:true
											}
									});
								});
							</script>";
			}
			
			
			$return .= '<div class="vcop-carousel vcop-'.$instance.' '.$vcop_carousel_type.' vcop-carousel-'.$instance.' owl-carousel';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
			$return .= '">';
	
			/**************************************************/
			/**************** GALLERY SOURCE ******************/
			/**************************************************/
			
			if($vcop_carousel_source == 'vcop_gallery') {
			
			$images = explode( ',', $vcop_carousel_images );
	
			foreach ( $images as $id ) {			
				$image_attributes = wp_get_attachment_image_src( $id, 'vconepage-fullscreen' );
					if($vcop_carousel_link == 'custom_url') {					
						$link = get_post_meta( $id, '_custom_url', true );
						$target = 'target="'.$vcop_carousel_target_link.'"';						
					} else {
						$link = $image_attributes[0];
						$target = '';
					}
				$attachment_caption_array = get_post( $id );
				$attachment_caption	= $attachment_caption_array->post_excerpt;
				if(empty($attachment_caption)) { $attachment_caption = ' '; }
				
				$return .= '<div class="vcop-item">';
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				if($vcop_carousel_link == 'custom_url') {
					$return .= '<a href="'.$link.'" '.$target.'>';	
				}
					$return .= '<img src="'.$image_attributes[0].'">';
				if($vcop_carousel_link == 'custom_url') {
					$return .= '</a>';	
				}				
				if($vcop_carousel_type == 'vcop-carousel-style2') {
					$return .= '<h1 class="vcop-title">'.$attachment_caption.'</h1>';							
				}
				$return .= '</div>';
				if($vcop_carousel_link != 'custom_url') {	
					$return .= '<div class="vcop-image-over">';
					if($vcop_carousel_type == 'vcop-carousel-style3') {
						$return .= '<h1 class="vcop-title">'.$attachment_caption.'</h1>';							
					}		
						$return .= '<a href="'.$link.'" class="adtheme-icon-search vcop-zoom-image" title="'.$attachment_caption.'" '.$target.'></a>';			
						$return .= '</div>';
				}										
				$return .=	'</div>';			
				$return .= '</div>'; // #vcop-ITEM			
				
				}
			
			} else {
			
			/**************************************************/
			/****************** POSTS SOURCE ******************/
			/**************************************************/
			
			// LOOP QUERY
			$query = vcop_query($vcop_query_source,
								$vcop_query_sticky_posts, 
								$vcop_query_posts_type, 
								$vcop_query_categories, 
								$vcop_query_order, 
								$vcop_query_orderby, 
								'no', 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				
				$return .= '<div class="vcop-item">';
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				$return .= vconepage_thumbs('vconepage-rectangle');
				if($vcop_carousel_type == 'vcop-carousel-style2') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				$return .= '</div>';
				if($vcop_carousel_posts_zoom == 'zoom_show' && $vcop_carousel_posts_plus == 'plus_show') {
					$return .= '<div class="vcop-image-over vcop-both-icon">';	
				} else {
					$return .= '<div class="vcop-image-over">';
				}
				if($vcop_carousel_type == 'vcop-carousel-style3') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				if($vcop_carousel_posts_zoom == 'zoom_show') {			
					$return .= '<a href="'.vconepage_thumbs_link('vconepage-fullscreen').'" class="adtheme-icon-search vcop-zoom-image" title="'.get_the_title().'"></a>';
				}
				if($vcop_carousel_posts_plus == 'plus_show') {			
					$return .= '<a href="'.$link.'" class="adtheme-icon-plus"></a>';
				}
								
				$return .= '</div>';										
				$return .=	'</div>';			
				$return .= '</div>'; // #vcop-ITEM			
									
			
			endwhile;
			} // #LOOP QUERY				
			
			} // #POSTS SOURCE
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_carousel_function();
?>