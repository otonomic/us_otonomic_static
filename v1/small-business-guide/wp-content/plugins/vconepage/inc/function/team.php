<?php
/*
File: team.php
Description: Function Team
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_team_function extends vconepage_team_class {
public function vconepage_team_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_team_type" => '',
				"vcop_team_columns" => '',	
				"vcop_team_excerpt" => '',
				"vcop_excerpt" => '200',			
				"vcop_team_title" => '',
				"vcop_team_subtitle" => '',	
				"vcop_team_social" => '',
				"vcop_team_font_type" => '',
				"vcop_team_google_fonts" => '',
				"vcop_team_excerpt_size" => '',
				"vcop_team_excerpt_color" => '',
				"vcop_team_title_size" => '',
				"vcop_team_title_color" => '',
				"vcop_team_subtitle_size" => '',
				"vcop_team_subtitle_color" => '',
				"vcop_team_social_size" => '',
				"vcop_team_social_color" => '',
				"vcop_team_background_color" => '',												
				"vcop_team_line_color" => '',
				"vcop_team_carousel_item" => '',
				"vcop_team_carousel_navigation" => '',
				"vcop_team_carousel_pagination" => '',
				"vcop_team_carousel_autoplay" => '',
				"vcop_team_carousel_item_padding" => '',  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_team_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_team_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_team_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_team_font_type == 'vcop-google-font') {
				$return .=  '.vcop-team-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}
			if($vcop_team_excerpt == 'excerpt-show') {
				$return .=  '.vcop-team-'.$instance.' .vcop-excerpt-container p {
										font-size:'.$vcop_team_excerpt_size.';
										color:'.$vcop_team_excerpt_color.';
				}';
			}
			if($vcop_team_title == 'title-show') {
				$return .=  '.vcop-team-'.$instance.' .vcop-title {
										font-size:'.$vcop_team_title_size.';
										color:'.$vcop_team_title_color.';
				}';
			}
			if($vcop_team_subtitle == 'subtitle-show') {
				$return .=  '.vcop-team-'.$instance.' .vcop-subtitle {
										font-size:'.$vcop_team_subtitle_size.';
										color:'.$vcop_team_subtitle_color.';
				}';
			}
			if($vcop_team_social == 'social-show') {
				$return .=  '.vcop-team-'.$instance.' .vcop-social-container .vcop-social-icon a {
										font-size:'.$vcop_team_social_size.';
										color:'.$vcop_team_social_color.';
				}
				.vcop-team-'.$instance.' .vcop-social-container .vcop-social-icon a i {
										font-size:'.$vcop_team_social_size.';
				}				
				';
			}	
				$return .=  '.vcop-team-'.$instance.' .vcop-separator-team {
										background-color:'.$vcop_team_line_color.';
				}';			
			if($vcop_team_type == 'vcop-team-grid-style1' || $vcop_team_type == 'vcop-team-carousel-style1') {					
				$return .=  '.vcop-team-'.$instance.' .vcop-excerpt {
										background:'.$vcop_team_background_color.';
				}';	
			}
			if($vcop_team_type == 'vcop-team-grid-style2' || $vcop_team_type == 'vcop-team-carousel-style2') {					
				$return .=  '.vcop-team-'.$instance.' .vcop-item {
										background:'.$vcop_team_background_color.';
				}';	
			}			
			if($vcop_team_type == 'vcop-team-carousel-style1' || $vcop_team_type == 'vcop-team-carousel-style2') {
				$return .= '.vcop-team-'.$instance.' .vcop-item	{
										padding:'.$vcop_team_carousel_item_padding.';
				}';			
			}								
			$return .= '</style>';
			
			/* CAROUSEL SCRIPT */
			if($vcop_team_type == 'vcop-team-carousel-style1' || $vcop_team_type == 'vcop-team-carousel-style2') {
				if(empty($vcop_team_carousel_autoplay) || $vcop_team_carousel_autoplay == '0' || $vcop_team_carousel_autoplay == '') {
					$vcop_team_carousel_autoplay = 'false';
				}
			$return .= '
			<script>
			jQuery(document).ready(function($){
      			$(".vcop-team-'.$instance.'").owlCarousel({
					items : '.$vcop_team_carousel_item.',
					autoPlay: '.$vcop_team_carousel_autoplay.',
					navigation: '.$vcop_team_carousel_navigation.',
					pagination: '.$vcop_team_carousel_pagination.',
					itemsTablet: [1025,2],
					itemsMobile: [479,1],
					navigationText: [\'<i class="adtheme-prev"></i>\',\'<i class="adtheme-next"></i>\'] 				
				});
			});
			</script>';
			}	
			/* #CAROUSEL SCRIPT */
			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );				
			wp_enqueue_style( 'vcop-team' );			
			
			if($vcop_team_type == 'vcop-team-carousel-style1' || $vcop_team_type == 'vcop-team-carousel-style2') {
				wp_enqueue_style( 'vcop-owl-carousel' );
				wp_enqueue_style( 'vcop-owl-theme' );
				wp_enqueue_script( 'vcop-owl-carousel-js' );
			}
			
			
			
			if($vcop_team_type == 'vcop-team-carousel-style1' || $vcop_team_type == 'vcop-team-carousel-style2') {
				$return .= '<div class="'.$vcop_team_type.' vcop-team-'.$instance.' owl-carousel';	
			} else {
				$return .= '<div class="'.$vcop_team_type.' vcop-columns-'.$vcop_team_columns.' vcop-team-'.$instance.'';
			}
			$return .= '">';
			
			// LOOP QUERY
			$query = 'post_type=Post&posts_per_page=-1&post_status=publish&post_type=vconepage_team';

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();
				
				$facebook 	= get_post_meta( get_the_id(), '_facebook', true );
				$twitter 	= get_post_meta( get_the_id(), '_twitter', true );
				$googleplus = get_post_meta( get_the_id(), '_googleplus', true );
				$linkedin 	= get_post_meta( get_the_id(), '_linkedin', true );
				$dribble 	= get_post_meta( get_the_id(), '_dribble', true );
				$youtube 	= get_post_meta( get_the_id(), '_youtube', true );
				$email 		= get_post_meta( get_the_id(), '_email', true );
				$subtitle 	= get_post_meta( get_the_id(), '_subtitle', true );
				
				
				$return .= '<div class="vcop-item';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
				$return .= '">';
				$return .= '<div class="vcop-container-thumbs">';
				$return .= vconepage_thumbs('vconepage-rectangle');
				
				/* STYLE 1 */
				if($vcop_team_type == 'vcop-team-grid-style1' || $vcop_team_type == 'vcop-team-carousel-style1') {
					if($vcop_team_excerpt == 'excerpt-show') {
						if(empty($vcop_excerpt)) { $vcop_excerpt = '200'; }
						$return .= '<div class="vcop-excerpt"><div class="vcop-excerpt-container"><p>'.vconepage_excerpt($vcop_excerpt).'</p></div></div>';
					}
				}
				/* #STYLE 1 */
				
				$return .= '</div>';
				
				if($vcop_team_title == 'title-show') {
					$return .= '<h3 class="vcop-title">'.get_the_title().'</h3>';
				}
				
				if($vcop_team_subtitle == 'subtitle-show') {
					$return .= '<span class="vcop-subtitle">'.$subtitle.'</span>';
				}

				/* STYLE 2 */
				if($vcop_team_type == 'vcop-team-grid-style2' || $vcop_team_type == 'vcop-team-carousel-style2') {
					if($vcop_team_excerpt == 'excerpt-show') {
						if(empty($vcop_excerpt)) { $vcop_excerpt = '200'; }
						$return .= '<div class="vcop-excerpt"><div class="vcop-excerpt-container"><p>'.vconepage_excerpt($vcop_excerpt).'</p></div></div>';
					}
				}
				/* #STYLE 2 */				

				$return .= '<span class="vcop-separator-team"></span>';
				
				/* SOCIAL SHOW/HIDDEN */
				
				if($vcop_team_social == 'social-show') {
				
				$return .= '<div class="vcop-social-container">';
				
					if(!empty($facebook)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$facebook.'"><i class="adtheme-icon-facebook"></i></a>
										</span>'; 
					}
					if(!empty($twitter)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$twitter.'"><i class="adtheme-icon-twitter"></i></a>
										</span>'; 
					}
					if(!empty($googleplus)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$googleplus.'"><i class="adtheme-icon-googleplus"></i></a>
										</span>'; 
					}
					if(!empty($linkedin)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$linkedin.'"><i class="adtheme-icon-linkedin2"></i></a>
										</span>'; 
					}
					if(!empty($dribble)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$dribble.'"><i class="adtheme-icon-dribble"></i></a>
										</span>'; 
					}
					if(!empty($youtube)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="'.$youtube.'"><i class="adtheme-icon-youtube2"></i></a>
										</span>'; 
					}																				
					if(!empty($email)) { 
							$return .= '<span class="vcop-social-icon">
											<a href="mailto:'.$email.'"><i class="adtheme-icon-mail2"></i></a>
										</span>'; 
					}
				
				$return .= '</div>';
				
				} // #SOCIAL SHOW/HIDDEN
				
				$return .= '</div>';
			
			endwhile;
			}
			
			$return .= '</div>';
			wp_reset_query();
			return $return;
    } 
}

new vcop_team_function();

?>