<?php
/*
File: boxmessage.php
Description: Function Box Message
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_boxmessage_function extends vconepage_boxmessage_class {
public function vconepage_boxmessage_function($atts)
{
	$instance = rand(1,100000);
    extract(
		shortcode_atts(
			array(
				"vcop_boxmessagge_type" => 'default',
				"vcop_boxmessagge_default_type" => '',
				"vcop_boxmessagge_default_style" => '',				
				"vcop_boxmessagge_custom_icon" => 'adtheme-icon-home', 
				"vcop_boxmessagge_custom_size" => '15',
				"vcop_boxmessagge_custom_icon_float" => 'none', 
				"vcop_boxmessagge_custom_icon_color" => '#000FFF',
				"vcop_boxmessagge_custom_background_color" => '#000FFF',  
				"vcop_boxmessagge_custom_margin" => '5px', 
				"vcop_boxmessagge_custom_padding" => '5px',
				"vcop_boxmessagge_custom_background_border_3d_color" => '',
				"vcop_boxmessagge_text" => '',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);

	/* LOAD CSS && JS */	
	wp_enqueue_style( 'vcop-normalize' );
	wp_enqueue_style( 'vcop-animations' );
	wp_enqueue_style( 'vcop-fonts' );
	wp_enqueue_script( 'vcop-appear-js' );
	wp_enqueue_script( 'vcop-animate-js' );	
	
	
	$return = '';
	
	// DEFAULT
	if($vcop_boxmessagge_type == 'default') {
		if($vcop_animate == 'on') {
			$return .= '<div class="wpb_alert wpb_content_element '.$vcop_boxmessagge_default_type.' '.$vcop_boxmessagge_default_style.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
							<div class="messagebox_text">'.wpb_js_remove_wpautop($vcop_boxmessagge_text, true).'</div>
						</div>';
		} else {
			$return .= '<div class="wpb_alert wpb_content_element '.$vcop_boxmessagge_default_type.' '.$vcop_boxmessagge_default_style.'">
							<div class="messagebox_text">'.wpb_js_remove_wpautop($vcop_boxmessagge_text, true).'</div>
						</div>';				
		}
		
	}
	
	// CUSTOM
	if($vcop_boxmessagge_type == 'custom') {
		if($vcop_animate == 'on') {
			$return .= '<style type="text/css">
							.boxmessage-'.$instance.' .boxmessage_text {
								background:none!important;
								text-shadow:0 0 0;
								color:'.$vcop_boxmessagge_custom_icon_color.';
							}
							.boxmessage-'.$instance.' {
								background-color:'.$vcop_boxmessagge_custom_background_color.'!important;							
								border-color:'.$vcop_boxmessagge_custom_background_color.'!important;
																						
							}
							.boxmessage-'.$instance.' i {
								float:'.$vcop_boxmessagge_custom_icon_float.';
								margin:'.$vcop_boxmessagge_custom_margin.';
								padding:'.$vcop_boxmessagge_custom_padding.';
								color:'.$vcop_boxmessagge_custom_icon_color.';
								font-size:'.$vcop_boxmessagge_custom_size.';
							}';
			if($vcop_boxmessagge_default_style == 'vc_alert_3d') {				
					$return .= '.boxmessage-'.$instance.' {
								box-shadow:0 5px 0 '.$vcop_boxmessagge_custom_background_border_3d_color.';
					}';
			}			
			$return .=	'</style>';
			
			$return .= '<div class="boxmessage-'.$instance.' wpb_alert wpb_content_element '.$vcop_boxmessagge_default_style.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">
								<div class="boxmessage_text"><i class="'.$vcop_boxmessagge_custom_icon.'"></i>'.wpb_js_remove_wpautop($vcop_boxmessagge_text, true).'</div>
						</div>';
		} else {
			$return .= '<style type="text/css">
							.boxmessage-'.$instance.' .boxmessage_text {
								background:none!important;
								text-shadow:0 0 0;
								color:'.$vcop_boxmessagge_custom_icon_color.';
							}
							.boxmessage-'.$instance.' {
								background-color:'.$vcop_boxmessagge_custom_background_color.'!important;							
								border-color:'.$vcop_boxmessagge_custom_background_color.'!important;														
							}
							.boxmessage-'.$instance.' i {
								float:'.$vcop_boxmessagge_custom_icon_float.';
								margin:'.$vcop_boxmessagge_custom_margin.';
								padding:'.$vcop_boxmessagge_custom_padding.';
								color:'.$vcop_boxmessagge_custom_icon_color.';
								font-size:'.$vcop_boxmessagge_custom_size.';
							}';
			if($vcop_boxmessagge_default_style == 'vc_alert_3d') {				
					$return .= '.boxmessage-'.$instance.' {
								box-shadow:0 5px 0 '.$vcop_boxmessagge_custom_background_border_3d_color.';
					}';
			}							
			$return .=	'</style>';

			$return .= '<div class="boxmessage-'.$instance.' wpb_alert wpb_content_element '.$vcop_boxmessagge_default_style.'">
								<div class="boxmessage_text"><i class="'.$vcop_boxmessagge_custom_icon.'"></i>'.wpb_js_remove_wpautop($vcop_boxmessagge_text, true).'</div>
						</div>';		
		}
	}
		
		return $return;
    } 
}
new vcop_boxmessage_function();
?>