<?php
/*
File: portfolio.php
Description: Function portfolio
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_portfolio_function extends vconepage_portfolio_class {
public function vconepage_portfolio_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_portfolio_type" => '',
				"vcop_portfolio_columns" => '',	
				"vcop_portfolio_excerpt" => '',
				"vcop_portfolio_excerpt_number" => '200',
				"vcop_portfolio_filter" => '',
				"vcop_portfolio_title" => '',
				"vcop_portfolio_date" => '',
				"vcop_portfolio_date_format" => '',
				"vcop_portfolio_comments" => '',
				"vcop_portfolio_author" => '',
				"vcop_portfolio_category" => '',
				"vcop_portfolio_views" => '',
				"vcop_portfolio_social" => '',				
				"vcop_portfolio_font_type" => '',
				"vcop_portfolio_google_fonts" => '',
				"vcop_portfolio_title_fs" => '',
				"vcop_portfolio_content_fs" => '',
				"vcop_portfolio_background_color" => '',
				"vcop_portfolio_secondary_color" => '',
				"vcop_portfolio_font_color" => '',
				"vcop_portfolio_a_color" => '',												
				"vcop_portfolio_over_color" => '',
				
				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',				
				
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_portfolio_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_portfolio_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_portfolio_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_portfolio_font_type == 'vcop-google-font') {
				$return .=  '.vcop-portfolio-'.$instance.' { 
										font-family:'.$font_name_slip.'!important;
										font-style:'.$font_style.'!important;
										font-weight:'.$font_weight.'!important;
								  }
							.vcop-portfolio-'.$instance.' h1 { 
										font-family:'.$font_name_slip.'!important;
										font-style:'.$font_style.'!important;
										font-weight:'.$font_weight.'!important;
								  }	  
								  ';	
			}		
			
			if($vcop_portfolio_excerpt 	== 'false' &&
			   $vcop_portfolio_comments == 'false' && 
			   $vcop_portfolio_author 	== 'false' && 
			   $vcop_portfolio_category == 'false' && 
			   $vcop_portfolio_views 	== 'false' && 
			   $vcop_portfolio_social 	== 'false') {
				$return .=  '.vcop-portfolio-'.$instance.' .vcop-item .vcop-container-portfolio {
					padding:0!important;
				}';
			}
			   	
			$return .= vcop_portfolio_style($instance, 
							$vcop_portfolio_type, 
							$vcop_portfolio_title_fs, 
							$vcop_portfolio_content_fs, 
							$vcop_portfolio_background_color, 
							$vcop_portfolio_secondary_color, 
							$vcop_portfolio_font_color, 
							$vcop_portfolio_a_color, 
							$vcop_portfolio_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );				
			wp_enqueue_style( 'vcop-portfolio' );			
			
			
			
			if($vcop_portfolio_filter == 'true') {
				
				wp_enqueue_script( 'vcop-filter-js' );	
				
				$return .= vcop_filter_js($instance,'portfolio');
				
				$return .= vcop_filter_item($instance,
											'vcop-portfolio',
											$vcop_query_source,
											$vcop_query_categories,
											$vcop_query_posts_type);	
			
			}			
			
			
			
			
			
			$return .= '<div class="vcop-portfolio vcop-'.$instance.' '.$vcop_portfolio_type.' vcop-columns-'.$vcop_portfolio_columns.' vcop-portfolio-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
				if($vcop_portfolio_filter == 'true') {
					$return .= ' vcop-filter';	
				}
			$return .= '">';
			
			// LOOP QUERY
			$query = vcop_query($vcop_query_source,
								$vcop_query_sticky_posts, 
								$vcop_query_posts_type, 
								$vcop_query_categories, 
								$vcop_query_order, 
								$vcop_query_orderby, 
								$vcop_query_pagination, 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();				
				$link = get_permalink();
				
				if($vcop_portfolio_filter == 'true') {
					
					$return .= vcop_filter_item_div($vcop_query_source,$vcop_query_categories,$vcop_query_posts_type);
					
				} else {
				
					$return .= '<div class="vcop-item">';
				
				}
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				if($vcop_portfolio_date == 'true') {
					$return .= '<span class="vcop-info-date">'.vconepage_post_info(true,false,false,false,false,$vcop_portfolio_category,$vcop_portfolio_views,$vcop_portfolio_date_format).'</span>';				
				}
				$return .= vconepage_thumbs('vconepage-rectangle');
				$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				$return .= '</div>';
				$return .= '<div class="vcop-image-over"><a href="'.$link.'" class="adtheme-icon-plus"></a>
							</div></div>';				
				$return .= '<div class="vcop-container-portfolio">
								<p class="vcop-info">'.vconepage_post_info(false,$vcop_portfolio_comments,$vcop_portfolio_author,$vcop_portfolio_category,$vcop_portfolio_views,$vcop_query_source,$vcop_query_posts_type,$vcop_portfolio_date_format).'</p>';						
				if($vcop_portfolio_excerpt == 'true') {				
							$return .=	'<p class="vcop-text">'.vconepage_excerpt($vcop_portfolio_excerpt_number).'...</p>';
				}
				if($vcop_portfolio_social == 'true') {				
							$return .= '<p class="vcop-share">'.vconepage_share().'</p>';
				}
				$return .=	'</div>';			
				$return .= '</div>'; // #vcop-ITEM
				
				endwhile;
				}
				
				// PAGINATION 
				if($vcop_query_pagination == 'yes') {
					$return .= '<div class="vcop-clear"></div><div class="vcop-portfolio-'.$instance.' vcop-pagination">';
					if($vcop_query_pagination_type == 'numeric') {
						$return .= vcop_numeric_pagination($pages = '', $range = 2,$loop);
					} else {
						$return .= get_next_posts_link( 'Older posts', $loop->max_num_pages );
						$return .= get_previous_posts_link( 'Newer posts' );				
					}
					$return .= '</div>';
				}
				// #PAGINATION
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_portfolio_function();
?>