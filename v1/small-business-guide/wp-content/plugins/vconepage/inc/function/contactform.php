<?php
/*
File: contactform.php
Description: Function contactform
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_contactform_function extends vconepage_contactform_class {
public function vconepage_contactform_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_contactform_style" => '',
				"vcop_contactform_shortcodes_id" => '',
				"vcop_contactform_border_type" => '',
				"vcop_contactform_text_color" => '',
				"vcop_contactform_background_field_color" => '',
				"vcop_contactform_background_color" => '',
				"vcop_contactform_border_color" => '',
				"vcop_contactform_active_color" => '',
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
		wp_enqueue_style( 'vcop-contactform' );
	
		$return = '<style type="text/css">';
		$return .= '.vcop-cf7-'.$instance.' {
				color:'.$vcop_contactform_text_color.'!important;
				background:'.$vcop_contactform_background_color.';
		}
		.vcop-cf7-'.$instance.' input,
		.vcop-cf7-'.$instance.' textarea {
				color:'.$vcop_contactform_text_color.'!important;
		}
		.vcop-cf7-'.$instance.' input,
		.vcop-cf7-'.$instance.' textarea {
			background:'.$vcop_contactform_background_field_color.';
			border:1px solid '.$vcop_contactform_border_color.';
		}
		.vcop-cf7-'.$instance.' input:focus,
		.vcop-cf7-'.$instance.' textarea:focus {
			border:1px solid '.$vcop_contactform_active_color.';
		}
		.vcop-cf7-'.$instance.' input[type="submit"]:hover {
			border:1px solid '.$vcop_contactform_active_color.';	
		}
		';
		
		
		$return .= '</style>';
	
	
		if($vcop_animate == 'on') {
			$return .= '<div class="vcop-contactform vcop-cf7-'.$instance.' '.$vcop_contactform_border_type.' '.$vcop_contactform_style.' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'">'.do_shortcode('[contact-form-7 id="'.$vcop_contactform_shortcodes_id.'"]').'</div>';
		} else {
			$return .= '<div class="vcop-contactform vcop-cf7-'.$instance.' '.$vcop_contactform_border_type.' '.$vcop_contactform_style.'">'.do_shortcode('[contact-form-7 id="'.$vcop_contactform_shortcodes_id.'"]').'</div>';			
		}
		return $return;
	} 
}
new vcop_contactform_function();
?>