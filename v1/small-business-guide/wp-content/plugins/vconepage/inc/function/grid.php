<?php
/*
File: grid.php
Description: Function grid
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_grid_function extends vconepage_grid_class {
public function vconepage_grid_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_grid_type" => '',
				"vcop_grid_columns" => '',	
				"vcop_grid_excerpt" => '',
				"vcop_grid_excerpt_number" => '200',
				"vcop_grid_filter" => '',
				"vcop_grid_title" => '',
				"vcop_grid_date" => '',
				"vcop_grid_date_format" => '',
				"vcop_grid_comments" => '',
				"vcop_grid_author" => '',
				"vcop_grid_category" => '',
				"vcop_grid_views" => '',
				"vcop_grid_social" => '',
				
				"vcop_grid_font_type" => '',
				"vcop_grid_google_fonts" => '',
				"vcop_grid_title_fs" => '',
				"vcop_grid_content_fs" => '',
				"vcop_grid_background_color" => '',
				"vcop_grid_secondary_color" => '',
				"vcop_grid_font_color" => '',
				"vcop_grid_a_color" => '',												
				"vcop_grid_over_color" => '',
				
				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',				
				
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_grid_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_grid_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_grid_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_grid_font_type == 'vcop-google-font') {
				$return .=  '.vcop-grid-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}
			if($vcop_grid_excerpt == 'excerpt-show') {
				$return .=  '.vcop-grid-'.$instance.' .vcop-excerpt-container p {
										font-size:'.$vcop_grid_excerpt_size.';
										color:'.$vcop_grid_excerpt_color.';
				}';
			}			
			
			$return .= vcop_grid_style($instance, 
							$vcop_grid_type, 
							$vcop_grid_title_fs, 
							$vcop_grid_content_fs, 
							$vcop_grid_background_color, 
							$vcop_grid_secondary_color, 
							$vcop_grid_font_color, 
							$vcop_grid_a_color, 
							$vcop_grid_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_script( 'vcop-magnific-popup-js' );			
			wp_enqueue_style( 'vcop-grid' );			
			wp_enqueue_style( 'vcop-magnific-popup' );
			
			
			if($vcop_grid_filter == 'true') {
				
				wp_enqueue_script( 'vcop-filter-js' );	
				
				$return .= vcop_filter_js($instance,'grid');
				
				$return .= vcop_filter_item($instance,
											'vcop-grid',
											$vcop_query_source,
											$vcop_query_categories,
											$vcop_query_posts_type);	
			
			}			
			
			$return .= "<script type=\"text/javascript\">	
							jQuery(document).ready(function($){
								$('.vcop-grid-".$instance." .vcop-zoom-image').magnificPopup({
									type: 'image',
										gallery:{
											enabled:true
										}
								});
							});
						</script>";
			
			
			
			$return .= '<div class="vcop-grid vcop-'.$instance.' '.$vcop_grid_type.' vcop-columns-'.$vcop_grid_columns.' vcop-grid-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
				if($vcop_grid_filter == 'true') {
					$return .= ' vcop-filter';	
				}
			$return .= '">';
			
			// LOOP QUERY
			$query = vcop_query($vcop_query_source,
								$vcop_query_sticky_posts, 
								$vcop_query_posts_type, 
								$vcop_query_categories, 
								$vcop_query_order, 
								$vcop_query_orderby, 
								$vcop_query_pagination, 
								$vcop_query_number, 
								$vcop_query_posts_for_page,
								$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();				
				$link = get_permalink();
				if($vcop_grid_filter == 'true') {
					
					$return .= vcop_filter_item_div($vcop_query_source,$vcop_query_categories,$vcop_query_posts_type);
					
				} else {
				
					$return .= '<div class="vcop-item">';
				
				}
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				$return .= vconepage_thumbs('vconepage-rectangle');
				if($vcop_grid_title == 'true' && $vcop_grid_type == 'vcop-grid-style2') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				$return .= '</div>';
				$return .= '<div class="vcop-image-over">';
								if($vcop_grid_title == 'true' && $vcop_grid_type == 'vcop-grid-style1') {
									$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
								}				
				$return .= '<a href="'.vconepage_thumbs_link('vconepage-fullscreen').'" class="adtheme-icon-search vcop-zoom-image" title="'.get_the_title().'"></a>
							<a href="'.$link.'" class="adtheme-icon-plus"></a>
							</div>';										
				$return .=	'</div>';			
				$return .= '</div>'; // #vcop-ITEM			
				
				endwhile;
				}
				
				// PAGINATION 
				if($vcop_query_pagination == 'yes') {
					$return .= '<div class="vcop-clear"></div><div class="vcop-grid-'.$instance.' vcop-pagination">';
					if($vcop_query_pagination_type == 'numeric') {
						$return .= vcop_numeric_pagination($pages = '', $range = 2,$loop);
					} else {
						$return .= get_next_posts_link( 'Older posts', $loop->max_num_pages );
						$return .= get_previous_posts_link( 'Newer posts' );				
					}
					$return .= '</div>';
				}
				// #PAGINATION
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_grid_function();
?>