<?php
/*
File: gallery.php
Description: Function gallery
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_gallery_function extends vconepage_gallery_class {
public function vconepage_gallery_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_gallery_type" => '',
				"vcop_gallery_columns" => '',	
				"vcop_gallery_link" => '',
				"vcop_gallery_target_link" => '',
				"vcop_gallery_images" => '',				
				
				"vcop_gallery_font_type" => '',
				"vcop_gallery_google_fonts" => '',
				"vcop_gallery_title_fs" => '',
				"vcop_gallery_background_color" => '',
				"vcop_gallery_secondary_color" => '',
				"vcop_gallery_font_color" => '',
				"vcop_gallery_a_color" => '',												
				"vcop_gallery_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_gallery_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_gallery_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_gallery_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_gallery_font_type == 'vcop-google-font') {
				$return .=  '.vcop-gallery-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_gallery_style($instance, 
							$vcop_gallery_type, 
							$vcop_gallery_title_fs,  
							$vcop_gallery_background_color, 
							$vcop_gallery_secondary_color, 
							$vcop_gallery_font_color, 
							$vcop_gallery_a_color, 
							$vcop_gallery_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_script( 'vcop-magnific-popup-js' );			
			wp_enqueue_style( 'vcop-gallery' );			
			wp_enqueue_style( 'vcop-magnific-popup' );
			
					
			if($vcop_gallery_link != 'custom_url') {
				$return .= "<script type=\"text/javascript\">	
								jQuery(document).ready(function($){
									$('.vcop-gallery-".$instance." .vcop-zoom-image').magnificPopup({
										type: 'image',
											gallery:{
												enabled:true
											}
									});
								});
							</script>";
			}
			
			
			$return .= '<div class="vcop-gallery vcop-'.$instance.' '.$vcop_gallery_type.' vcop-columns-'.$vcop_gallery_columns.' vcop-gallery-'.$instance.'';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
			$return .= '">';
			
			$images = explode( ',', $vcop_gallery_images );
	
			foreach ( $images as $id ) {			
				$image_attributes = wp_get_attachment_image_src( $id, 'vconepage-fullscreen' );
					if($vcop_gallery_link == 'custom_url') {					
						$link = get_post_meta( $id, '_custom_url', true );
						$target = 'target="'.$vcop_gallery_target_link.'"';						
					} else {
						$link = $image_attributes[0];
						$target = '';
					}
				$attachment_caption_array = get_post( $id );
				$attachment_caption	= $attachment_caption_array->post_excerpt;
				if(empty($attachment_caption)) { $attachment_caption = ' '; }
				
				$return .= '<div class="vcop-item">';
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				$return .= '<img src="'.$image_attributes[0].'">';
				if($vcop_gallery_type == 'vcop-gallery-style2') {
					$return .= '<h1 class="vcop-title">'.$attachment_caption.'</h1>';							
				}
				$return .= '</div>';
				$return .= '<div class="vcop-image-over">';
				if($vcop_gallery_type == 'vcop-gallery-style3') {
					$return .= '<h1 class="vcop-title">'.$attachment_caption.'</h1>';							
				}			
				$return .= '<a href="'.$link.'" class="adtheme-icon-search vcop-zoom-image" title="'.$attachment_caption.'" '.$target.'></a>
							</div>';										
				$return .=	'</div>';			
				$return .= '</div>'; // #vcop-ITEM			
				
				}
				
										
			$return .= '</div>';
			
			return $return;		
    } 
}
new vcop_gallery_function();
?>