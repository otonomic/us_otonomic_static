<?php
/*
File: woocarousel.php
Description: Function woocarousel
Plugin: VC One Page
Author: Ad-theme.com
*/
class vcop_woocarousel_function extends vconepage_woocarousel_class {
public function vconepage_woocarousel_function($atts)
{
	static $instance = 0;
	$instance++;
    extract(
		shortcode_atts(
			array(
				"vcop_woocarousel_type" => '',
				"vcop_woocarousel_source" => '',
				"vcop_woocarousel_posts_cart" => '',
				"vcop_woocarousel_posts_plus" => '',	
				"vcop_woocarousel_link" => '',
				"vcop_woocarousel_target_link" => '',
				"vcop_woocarousel_images" => '',				

				"vcop_query_source" => '',
				"vcop_query_sticky_posts" => '',
				"vcop_query_posts_type" => '',
				"vcop_query_categories" => '',
				"vcop_query_order" => '',
				"vcop_query_orderby" => '',
				"vcop_query_pagination" => '',
				"vcop_query_pagination_type" => '',
				"vcop_query_number" => '',
				"vcop_query_posts_for_page" => '',

				"vcop_woocarousel_item" => '',
				"vcop_woocarousel_navigation" => '',
				"vcop_woocarousel_pagination" => '',
				"vcop_woocarousel_autoplay" => '',
				"vcop_woocarousel_item_padding" => '', 
				
				"vcop_woocarousel_font_type" => '',
				"vcop_woocarousel_google_fonts" => '',
				"vcop_woocarousel_title_fs" => '',
				"vcop_woocarousel_background_color" => '',
				"vcop_woocarousel_secondary_color" => '',
				"vcop_woocarousel_font_color" => '',
				"vcop_woocarousel_a_color" => '',												
				"vcop_woocarousel_over_color" => '',											
				  
				"vcop_animate" => 'on',
				"vcop_animate_effect" => 'fade-in',
				"vcop_delay" => '800' 
				), 
				$atts)
	);
	
	if($vcop_woocarousel_font_type == 'vcop-google-font') {
			/********************************************************/
			/******************** GOOGLE FONT ***********************/
			/********************************************************/
			
			$google_fonts_data = explode( '|', $vcop_woocarousel_google_fonts); // SPLIT | 
			$google_fonts_data_value = explode( ':', $vcop_woocarousel_google_fonts); // SPLIT :	
			$gfont_font_value = explode( '|', $google_fonts_data_value[1]); // SPLIT |
			
			$gfont_font = $gfont_font_value[0]; // VALUE FOR GET GOOGLE FONT
					
			/* FONT FAMILY */
			/* SPLIT %3 */
			$google_fonts_family_value_part1 = explode( '%3A', $gfont_font);
			/* SPLIT %2 */
			$google_fonts_family_value_part2 = explode( '%2C', $google_fonts_family_value_part1[1]);
			
			/* FONT STYLE */			
			$gfont_style = $google_fonts_data_value[2]; // GET FONT STYLE
			/* SPLIT %3 */
			$google_fonts_style_value = explode( '%3A', $gfont_style);
			
			$font_name = $google_fonts_family_value_part1[0];
			$font_name_string = explode( '%20', $google_fonts_family_value_part1[0]);
			$font_style = $google_fonts_style_value[2];
			$font_weight = $google_fonts_style_value[1];
			
			$font_name_slip = '';
			foreach($font_name_string as $name) {
				$font_name_slip .= $name . ' ';
			}
			
			/********************************************************/
			/******************** #GOOGLE FONT **********************/
			/********************************************************/
			
			wp_enqueue_style( 'vc_google_fonts_'.$instance.'', 'http://fonts.googleapis.com/css?family='.$gfont_font.'');			
	}
			
			$return = '';
			$return .= '<style type="text/css">';
			if($vcop_woocarousel_font_type == 'vcop-google-font') {
				$return .=  '.vcop-woocarousel-'.$instance.' { 
										font-family:'.$font_name_slip.';
										font-style:'.$font_style.';
										font-weight:'.$font_weight.';
								  }';	
			}			
			
			$return .= vcop_woocarousel_style($instance,
							$vcop_woocarousel_item_padding, 
							$vcop_woocarousel_type, 
							$vcop_woocarousel_title_fs,  
							$vcop_woocarousel_background_color, 
							$vcop_woocarousel_secondary_color, 
							$vcop_woocarousel_font_color, 
							$vcop_woocarousel_a_color, 
							$vcop_woocarousel_over_color);
							
			$return .= '</style>';
			

			
			/* LOAD CSS && JS */	
			wp_enqueue_style( 'vcop-normalize' );
			wp_enqueue_style( 'vcop-columns' );
			wp_enqueue_style( 'vcop-animations' );
			wp_enqueue_style( 'vcop-fonts' );
			wp_enqueue_script( 'vcop-appear-js' );
			wp_enqueue_script( 'vcop-animate-js' );	
			wp_enqueue_script( 'vcop-magnific-popup-js' );			
			wp_enqueue_style( 'vcop-woocarousel' );			
			wp_enqueue_style( 'vcop-magnific-popup' );
			wp_enqueue_style( 'vcop-owl-carousel' );
			wp_enqueue_style( 'vcop-owl-theme' );
			wp_enqueue_script( 'vcop-owl-carousel-js' );			

			if(empty($vcop_woocarousel_autoplay) || $vcop_woocarousel_autoplay == '0' || $vcop_woocarousel_autoplay == '') {
				$vcop_woocarousel_autoplay = 'false';
			}

			$return .= '
			<script>
			jQuery(document).ready(function($){
      			$(".vcop-woocarousel-'.$instance.'").owlCarousel({
					items : '.$vcop_woocarousel_item.',
					autoPlay: '.$vcop_woocarousel_autoplay.',
					navigation: '.$vcop_woocarousel_navigation.',
					pagination: '.$vcop_woocarousel_pagination.',
					itemsTablet: [1025,2],
					itemsMobile: [479,1],
					navigationText: [\'<i class="adtheme-icon-arrow-left"></i>\',\'<i class="adtheme-icon-arrow-right"></i>\'] 				
				});
			});
			</script>';
					
			if($vcop_woocarousel_link != 'custom_url') {
				$return .= "<script type=\"text/javascript\">	
								jQuery(document).ready(function($){
									$('.vcop-woocarousel-".$instance." .vcop-cart-image').magnificPopup({
										type: 'image',
											gallery:{
												enabled:true
											}
									});
								});
							</script>";
			}
			
			
			$return .= '<div class="vcop-woocarousel vcop-'.$instance.' '.$vcop_woocarousel_type.' vcop-woocarousel-'.$instance.' owl-carousel';
				if($vcop_animate == 'on') { // ANIMATION ON
					$return .= ' animate-in" data-anim-type="'.$vcop_animate_effect.'" data-anim-delay="'.$vcop_delay.'';
				}
			$return .= '">';
			
			/**************************************************/
			/****************** POSTS SOURCE ******************/
			/**************************************************/
			
			// LOOP QUERY
			$query = vcop_query_woocommerce($vcop_query_categories, 
											$vcop_query_order, 
											$vcop_query_orderby, 
											'no', 
											$vcop_query_number, 
											$vcop_query_posts_for_page,
											$vcop_query_pagination);

			$loop = new WP_Query($query);	
			if($loop) { 
			while ( $loop->have_posts() ) : $loop->the_post();	
				$link = get_permalink();
				$id_post = get_the_id();
				$product = new WC_Product( $id_post );
				$price = $product->regular_price;
				$price_sales = $product->sale_price;
				$symbol = get_woocommerce_currency_symbol();				
				$return .= '<div class="vcop-item">';
				
				$return .= '<div class="vcop-container-thumbs"><div class="vcop-header-container">';
				$return .= vconepage_thumbs('vconepage-rectangle');
				
				if($vcop_woocarousel_type == 'vcop-woocarousel-style4' || $vcop_woocarousel_type == 'vcop-woocarousel-style5') {
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-item-sale">'.__('Sale','vconepage').'</span>';	
					}
					
					$return .= '<h3 class="vcop-price"><span class="vcop-regular-price';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= ' vcop-line-through';	
					}
					
					$return .= '">'. $price . $symbol .'</span>';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-sale-price">'. $price_sales . $symbol .'</span>';
					}
					$return .= '</h3>';
				}
				
				if($vcop_woocarousel_type == 'vcop-woocarousel-style2' || $vcop_woocarousel_type == 'vcop-woocarousel-style4' || $vcop_woocarousel_type == 'vcop-woocarousel-style5') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				$return .= '</div>';
				if($vcop_woocarousel_posts_cart == 'cart_show' && $vcop_woocarousel_posts_plus == 'plus_show' && $vcop_woocarousel_type != 'vcop-woocarousel-style6') {
					$return .= '<div class="vcop-image-over vcop-both-icon">';	
				} else {
					$return .= '<div class="vcop-image-over">';
				}
				if($vcop_woocarousel_type == 'vcop-woocarousel-style3') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}
				if($vcop_woocarousel_posts_cart == 'cart_show' && $vcop_woocarousel_type != 'vcop-woocarousel-style6') {			
					$return .= '<a class="add_to_cart_button product_type_simple added adtheme-icon-cart" data-product_sku="" data-product_id="'.$id_post.'" rel="nofollow" href="?add-to-cart='.$id_post.'"></a>';
				}
				if($vcop_woocarousel_posts_plus == 'plus_show') {			
					$return .= '<a href="'.$link.'" class="adtheme-icon-plus"></a>';
				}
								
				$return .= '</div>';										
				$return .=	'</div>';
				
				if($vcop_woocarousel_type == 'vcop-woocarousel-style6') {
					$return .= '<h1 class="vcop-title">'.get_the_title().'</h1>';							
				}				
														
				if($vcop_woocarousel_type == 'vcop-woocarousel-style5' || $vcop_woocarousel_type == 'vcop-woocarousel-style6') {
					$return .= '<div class="vcop-product-excerpt">'.get_the_excerpt().'</div>';					
				}

				if($vcop_woocarousel_type == 'vcop-woocarousel-style6') {
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-item-sale">'.__('Sale','vconepage').'</span>';	
					}
					
					$return .= '<h3 class="vcop-price"><span class="vcop-regular-price';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= ' vcop-line-through';	
					}
					
					$return .= '">'. $price . $symbol .'</span>';
					
					if(!empty($price_sales) || $price_sales != '') {
						$return .= '<span class="vcop-sale-price">'. $price_sales . $symbol .'</span>';

					}
					if($vcop_woocarousel_posts_cart == 'cart_show' && $vcop_woocarousel_type == 'vcop-woocarousel-style6') {			
								$return .= '<a class="add_to_cart_button product_type_simple added adtheme-icon-cart" data-product_sku="" data-product_id="'.$id_post.'" rel="nofollow" href="?add-to-cart='.$id_post.'"></a>';
					}						
					$return .= '</h3>';										
				}
				

								
				$return .= '</div>'; // #vcop-ITEM
			endwhile;
			} // #LOOP QUERY				
										
			$return .= '</div>';
			wp_reset_query();
			return $return;		
    } 
}
new vcop_woocarousel_function();
?>