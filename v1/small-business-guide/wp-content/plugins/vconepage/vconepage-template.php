<?php
/*
File: vconepage-template.php
Template Name: VC ONE PAGE
Author: Ad-theme.com
*/
while (have_posts()) : the_post(); 
$id = get_the_id();
?>
<!DOCTYPE html>
<html>
<head> 
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php 
	/************************************/
	/*********** PAGE TITLE *************/
	/************************************/
	$title = get_post_meta($id,'meta-title',true);
	if(empty($title) || $title == '') { ?>
    	<title><?php echo the_title(); ?></title>
    <?php } else { ?>
    	<title><?php echo $title; ?></title>
    <?php } 
	/************************************/
	/*********** #PAGE TITLE ************/
	/************************************/	
	?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <meta name="keywords" content="<?php echo get_post_meta($id,'meta-keyword',true); ?>">
    <meta name="description" content="<?php echo get_post_meta($id,'meta-description',true); ?>">
    <link rel="shortcut icon" href="<?php echo get_post_meta($id,'meta-favicon',true); ?>">
	<?php wp_head(); ?>
</head>
<body id="vconepage" <?php body_class(); ?> class="<?php echo get_post_permalink( $ad_onepage_id ); ?>">
<?php 

echo the_content(); 

?>    
    <?php endwhile; ?>
    <?php wp_footer(); ?>
    <?php 
	/************************************/
	/*********** CUSTOM CODE ************/
	/************************************/	
	$custom_js = get_post_meta($id,'meta-custom-js',true);	
	if(!empty($custom_js) || $custom_js != '') { ?>
		<script type="text/javascript">
			<?php echo $custom_js; ?>
		</script>
	<?php }
	$custom_css = get_post_meta($id,'meta-custom-css',true);	
	if(!empty($custom_css) || $custom_css != '') { ?>
		<style type="text/css">
			<?php echo $custom_css; ?>
		</style>
	<?php }  
	/************************************/
	/*********** #CUSTOM CODE ***********/
	/************************************/	
	?> 
    <link rel="stylesheet" type="text/css" href="<?php echo AD_VCOP_URL; ?>assets/css/main.css" />      
</body>
</html>