<?php
/*
Plugin Name: Visual Composer One Page Builder
Plugin URI: http://plugins.ad-theme.com/vconepage/
Description: Visual Composer One Page Builder - Build your one page using visual composer
Author: Ad-theme.com
Version: 1.0
Author URI: http://www.ad-theme.com
Compatibility: WP 3.9.x - WP 4.0.x - WP 4.1.x - WP 4.2.x
*/

// Basic plugin definitions 
define ('PLG_NAME_VCONEPAGE', 'vconepage');
define( 'PLG_VERSION_VCONEPAGE', '1.0' );
define( 'AD_VCOP_URL', WP_PLUGIN_URL . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));
define( 'AD_VCOP_DIR', WP_PLUGIN_DIR . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));

// LANGUAGE
add_action('init', 'vconepage_localization_init');
function vconepage_localization_init() {
    $path = dirname(plugin_basename( __FILE__ )) . '/languages/';
    $loaded = load_plugin_textdomain( 'vconepage', false, $path);
}

// don't load directly
if (!defined('ABSPATH')) die('-1');


// GENERAL
include('templates.php');
include('functions.php');
include('functions-general.php');
include('metabox/metabox.php');


add_action( 'wp_enqueue_scripts', 'vconepage_animation_loadCssAndJs' );
function vconepage_animation_loadCssAndJs() {
		
		wp_register_style( 'vcop-normalize',  AD_VCOP_URL . 'assets/css/normalize.css' );
		wp_register_style( 'vcop-columns',  AD_VCOP_URL . 'assets/css/columns.css' );
		wp_register_style( 'vcop-animations',  AD_VCOP_URL . 'assets/css/animations.min.css' );
		wp_register_style( 'vcop-fonts',  AD_VCOP_URL . 'assets/css/general/fonts.css' );
		wp_enqueue_script( 'jquery' );
		wp_register_script( 'vcop-appear-js',  AD_VCOP_URL . 'assets/js/appear.min.js' , array('jquery'), '', true);				
		wp_register_script( 'vcop-animate-js',  AD_VCOP_URL . 'assets/js/animations.min.js' , array('jquery'), '', true);	
		
}

/* CHANGE DEFAULT SHORTCODES DIR */
if ( defined( 'WPB_VC_VERSION' ) && in_array( 'vconepage/vconepage.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ))) ) {
	$dir = AD_VCOP_DIR . 'inc/class/vc_templates';
	vc_set_shortcodes_templates_dir( $dir );
	include('inc/class/row.php');
}

// MAIN ELEMENT
	include('inc/class/header.php');
	include('inc/function/header.php');
	
	include('inc/class/backtotop.php');
	include('inc/function/backtotop.php');	
	
// TEMPLATE
$templates_active = get_option( 'vcop_templates_active' );
if(empty($templates_active) || $templates_active == '' || !isset($templates_active)) { $templates_active = 'active'; }
if($templates_active == 'active') { 
	include('templates-demo.php');
}

// ADDON
$map_active = get_option( 'map_active' );
if(empty($map_active) || $map_active == '' || !isset($map_active)) { $map_active = 'active'; }
if($map_active == 'active') { 
	include('inc/class/map.php');
	include('inc/function/map.php');
}
$chart_active = get_option( 'chart_active' );
if(empty($chart_active) || $chart_active == '' || !isset($chart_active)) { $chart_active = 'active'; }
if($chart_active == 'active') {
	include('inc/class/chart.php');
	include('inc/function/chart.php');
}
$icon_active = get_option( 'icon_active' );
if(empty($icon_active) || $icon_active == '' || !isset($icon_active)) { $icon_active = 'active'; }
if($icon_active == 'active') {
	include('inc/class/icon.php');
	include('inc/function/icon.php');
}
$boxmessage_active = get_option( 'boxmessage_active' );
if(empty($boxmessage_active) || $boxmessage_active == '' || !isset($boxmessage_active)) { $boxmessage_active = 'active'; }
if($boxmessage_active == 'active') {
	include('inc/class/boxmessage.php');
	include('inc/function/boxmessage.php');
}
$countdown_active = get_option( 'countdown_active' );
if(empty($countdown_active) || $countdown_active == '' || !isset($countdown_active)) { $countdown_active = 'active'; }
if($countdown_active == 'active') {
	include('inc/class/countdown.php');
	include('inc/function/countdown.php');
}
$social_active = get_option( 'social_active' );
if(empty($social_active) || $social_active == '' || !isset($social_active)) { $social_active = 'active'; }
if($social_active == 'active') {
	include('inc/class/social.php');	
	include('inc/function/social.php');
}
$heading_active = get_option( 'heading_active' );
if(empty($heading_active) || $heading_active == '' || !isset($heading_active)) { $heading_active = 'active'; }
if($heading_active == 'active') {
	include('inc/class/heading.php');
	include('inc/function/heading.php');
}
$buttons_active = get_option( 'buttons_active' );
if(empty($buttons_active) || $buttons_active == '' || !isset($buttons_active)) { $buttons_active = 'active'; }
if($buttons_active == 'active') {
	include('inc/class/buttons.php');
	include('inc/function/buttons.php');
}
$counter_active = get_option( 'counter_active' );
if(empty($counter_active) || $counter_active == '' || !isset($counter_active)) { $counter_active = 'active'; }
if($counter_active == 'active') {
	include('inc/class/counter.php');
	include('inc/function/counter.php');
}
$team_active = get_option( 'team_custom_type_active' );
if(empty($team_active) || $team_active == '' || !isset($team_active)) { $team_active = 'active'; }
if($team_active == 'active') {
			include('inc/class/team.php');
			include('inc/function/team.php');
			include('metabox_team.php');
}
$portfolio_active = get_option( 'portfolio_active' );
if(empty($portfolio_active) || $portfolio_active == '' || !isset($portfolio_active)) { $portfolio_active = 'active'; }
if($portfolio_active == 'active') {
	include('inc/class/portfolio.php');
	include('inc/function/portfolio.php');
}
$grid_active = get_option( 'grid_active' );
if(empty($grid_active) || $grid_active == '' || !isset($grid_active)) { $grid_active = 'active'; }
if($grid_active == 'active') {
	include('inc/class/grid.php');
	include('inc/function/grid.php');
}
$gallery_active = get_option( 'gallery_active' );
if(empty($gallery_active) || $gallery_active == '' || !isset($gallery_active)) { $gallery_active = 'active'; }
if($gallery_active == 'active') {
	include('inc/class/gallery.php');
	include('inc/function/gallery.php');
}
include('image_url_field_gallery.php');
$carousel_active = get_option( 'carousel_active' );
if(empty($carousel_active) || $carousel_active == '' || !isset($carousel_active)) { $carousel_active = 'active'; }
if($carousel_active == 'active') {
	include('inc/class/carousel.php');
	include('inc/function/carousel.php');
}
$list_active = get_option( 'list_active' );
if(empty($list_active) || $list_active == '' || !isset($list_active)) { $list_active = 'active'; }
if($list_active == 'active') {
	include('inc/class/list.php');
	include('inc/function/list.php');
}
$timeline_active = get_option( 'timeline_active' );
if(empty($timeline_active) || $timeline_active == '' || !isset($timeline_active)) { $timeline_active = 'active'; }
if($timeline_active == 'active') {
	include('inc/class/timeline.php');
	include('inc/function/timeline.php');
}
$postlist_active = get_option( 'postlist_active' );
if(empty($postlist_active) || $postlist_active == '' || !isset($postlist_active)) { $postlist_active = 'active'; }
if($postlist_active == 'active') {
	include('inc/class/postlist.php');
	include('inc/function/postlist.php');
}
$space_active = get_option( 'space_active' );
if(empty($space_active) || $space_active == '' || !isset($space_active)) { $space_active = 'active'; }
if($space_active == 'active') {
	include('inc/class/space.php');
	include('inc/function/space.php');
}
$singleimage_active = get_option( 'singleimage_active' );
if(empty($singleimage_active) || $singleimage_active == '' || !isset($singleimage_active)) { $singleimage_active = 'active'; }
if($singleimage_active == 'active') {
	include('inc/class/singleimage.php');
	include('inc/function/singleimage.php');
}

$contactform_active = get_option( 'contactform_active' );
if(empty($contactform_active) || $contactform_active == '' || !isset($contactform_active)) { $contactform_active = 'active'; }


if ( defined( 'WPCF7_VERSION' ) && $contactform_active == 'active') {
	include('inc/class/contactform.php');
	include('inc/function/contactform.php');
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	include('inc/class/woocarousel.php');
	include('inc/function/woocarousel.php');
	include('inc/class/wooproductdisplay.php');
	include('inc/function/wooproductdisplay.php');
	include('inc/class/wootab.php');
	include('inc/function/wootab.php');			
}
