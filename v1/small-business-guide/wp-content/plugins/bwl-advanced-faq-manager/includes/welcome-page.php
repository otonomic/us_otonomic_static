<style type="text/css">
    
    .about-wrap{
        font-family: "verdana", sans-serif, serif;
    }

    .about-wrap ul,
    .about-wrap li,
    .about-wrap p {

        font-size: 13px;

    }
    .about-wrap h2 {

        margin: 0;
        padding: 0;

    }

    .developer-info{

        font-size: 16px;
    }

    .welcome-hr{
        border:0px;
        border-bottom: 1px solid #cccccc;
    }
    
</style>

<div class="wrap about-wrap">
        
    <h2>BWL Advanced FAQ Manager v <?php echo get_option(BWL_ADVANCED_FAQ_VERSION_KEY); ?></h2>
    
    <p class="developer-info">        
            
            By Md. Mahbub Alam Khan<br>
            
            Web Site: <a href="http://bluewindlab.net" target="_blank">www.bluewindlab.net</a><br>
        
    </p>
    
    <hr class="welcome-hr"/>
    
    <p>
        Thanks for Using BWL Advanced FAQ Manager. I hope you enjoy using it, as much as I enjoyed creating it.
        <a href="http://twitter.com/bluewindlab" target="_blank">Follow me</a> if you'd like to stay up to date with the plugins I create. Your support help me a lot to create new plugins.
    </p>
    
    <h2>About BWL Advanced FAQ Manager</h2>
    
    <p>BWL Advanced FAQ Manager Plugin makes it easy to create FAQ sections on your WordPress powered blog. 
            Simply activate, add FAQ items, then display them on a post or page by using a shortcode.
            Also you can show your faq items in sidebar as widget. 
            Cool sorting features gives you sort FAQ items according to your need.
    </p>
    
    <h2>Features:</h2>
    
    <ul>
        <li>- Zero Configuration Required.</li>
        <li>- Unlimited FAQ With Live Search.</li>
        <li>- 7 different FAQ Themes.</li>
        <li>- Front End FAQ Ask Form.</li>
        <li>- FAQ Option Panel.</li>
        <li>- FAQ Widget & Shortcodes.</li>        
        <li>- FAQ Sorting & Rating.</li>
        <li>- Responsive Layout.</li>
        <li>- Ready for localization.</li>
        <li>- <a href="https://wpml.org/2014/05/week-compatible-newave-compass-quite-nice-booking-bwl/" target="_blank">WPML</a> Compatible.</li>
        
    </ul>
    
    <hr class="welcome-hr"/>
    
    <h2>More Plugins By BlueWindLab:</h2>
    <ul>
        <li><a href="http://codecanyon.net/item/bwl-poll-manager/7787100" target="_blank">BWL Poll Manager</a></li>
        <li><a href="http://codecanyon.net/item/bwl-pro-voting-manager/7616885" target="_blank">BWL Pro Voting Manager</a></li>
        <li><a href="http://codecanyon.net/item/bwl-post-to-breaking-news-manager/6335199" target="_blank">BWL Post To Breaking News Manager</a></li>
    </ul>
    
    <hr class="welcome-hr"/>

    <p>
        If you need help with the plugin, give our <strong>BWL Advanced FAQ Manager</strong> documentation a read.
        If you have any questions that are beyond the scope of this help file, please feel free to email via my user page contact form <a href="<?php echo DEVELOPER_CONTACT ?>">here</a>. 
    </p>
    
    
    
</div> 