/*
Copyright (c) 2013 Copyscape / Indigo Stream Technologies (www.copyscape.com)
License: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

var minPub = document.getElementById("minor-publishing-actions");

var copyscapeCheck = document.createElement('button');
copyscapeCheck.id = 'copyscape_check';
copyscapeCheck.name = 'save';
copyscapeCheck.innerHTML = 'Copyscape Check';
copyscapeCheck.className = 'preview button';
copyscapeCheck.onclick = function() {
	document.getElementById('copyscape_check').innerHTML = '<span style = "color:green"><b>Checking with Copyscape</b></span>';
	document.getElementById('copyscape_check').style.border = 'none';
	document.getElementById('copyscape_button').click();
};

var copyscapeButton = document.createElement('input');
copyscapeButton.type = 'submit';
copyscapeButton.id = 'copyscape_button';
copyscapeButton.name = 'save';
copyscapeButton.value = 'Copyscape Check';
copyscapeButton.className = 'preview button';
copyscapeButton.style.display = "none";

var copyscapeClear = document.createElement('div');
copyscapeClear.className = 'clear';

var copyscapeDiv = document.createElement('div');
copyscapeDiv.id = 'copyscape-action';

var copyscapeP = document.createElement('p');

if( minPub != null ) {
	if( minPub.childNodes.length > 0 )
		copyscapeDiv = minPub.insertBefore( copyscapeDiv, minPub.childNodes[0] );
	else copyscapeDiv = minPub.appendChild( copyscapeDiv );
	
	if( copyscapeDiv != null ) {
		copyscapeDiv.appendChild(copyscapeButton);
		copyscapeDiv.appendChild(copyscapeCheck);
		copyscapeDiv.appendChild(copyscapeP);
		copyscapeP.appendChild(copyscapeClear);
	}
}