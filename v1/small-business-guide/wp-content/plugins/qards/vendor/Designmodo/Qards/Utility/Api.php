<?php
/*
 * This file is part of the Designmodo WordPress Plugin.
 *
 * (c) Designmodo Inc. <info@designmodo.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Designmodo\Qards\Utility;

use Designmodo\Qards\Page\Layout\Component\Component;
use Designmodo\Qards\Page\Layout\Layout;
use Designmodo\Qards\Http\Http;
use Designmodo\Qards\CodeTidy\CodeTidy;
use Designmodo\Qards\Page\Layout\Component\Template\Template;
/**
 * Api implements API handler.
 */
class Api
{

    /**
     * API handler
     *
     * @param bool $quietMode TRUE - return result; FALSE - print result and exit.
     * @return array
     */
    static public function handler($quietMode = false)
    {
        try {
            // Get method
            $method = $_REQUEST['method'];
            // Check permission
            $publicMethods = array('layout.css.get', 'component.css.get', 'settings.globalcss.get', 'font.get', 'subscriber.add');
            if (!in_array($method, $publicMethods) && !current_user_can('edit_posts')) {
                throw new \Exception('You must be authorized and have permission "edit_posts" to call "' . $method . '"', 768423);
            }
            // Get params
            if (! empty($_REQUEST['params'])) {
                if (! is_array($_REQUEST['params'])) {
                    $params = json_decode(stripslashes($_REQUEST['params']), true);
                } else {
                    $params = $_REQUEST['params'];
                }
            } elseif ($input = file_get_contents('php://input')) {
                $payload = json_decode($input, true);
                $params = $payload['params'];
            }
            $params = (!empty($params) && is_array($params)) ? $params : array();
            if (!empty($params)) {
                $stripslashesDeepFunction = function($value) use ( &$stripslashesDeepFunction ) {
                    $value = is_array($value) ? array_map($stripslashesDeepFunction, $value) : stripslashes($value);
                    return $value;
                };
                $params = $stripslashesDeepFunction($params);
            }

            // Call action
            $actionName = self::underscoredToLowerCamelcase(str_replace('.', '_', $method)) . 'Action';
            try {
                $reflector = new \ReflectionMethod(__CLASS__, $actionName);
            } catch (\Exception $e) {
                throw new \Exception('API method "' . $method . '" does not exist.', 645912);
            }

            $args = array();
            foreach ($reflector->getParameters() as $param) {
                $name = $param->getName();
                $isArgumentGiven = array_key_exists(self::camelcasedToUnderscored($name), $params);
                if (! $isArgumentGiven && ! $param->isDefaultValueAvailable()) {
                    throw new \Exception('Parameter "' . self::camelcasedToUnderscored($name) . '" is mandatory for API method "' . $method . '", but was not provided.', 371248);
                }
                $args[$param->getPosition()] = $isArgumentGiven ? $params[self::camelcasedToUnderscored($name)] : $param->getDefaultValue();
            }
            $result = array(
                'result' => call_user_func_array(array(__CLASS__, $actionName), $args)
            );
        } catch (\Exception $e) {
            $result = array(
                'error' => array(
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                )
            );
        }

        if ($quietMode) {
            return $result;
        } else {
            echo json_encode($result);
            exit();
        }
    }

    /**
     * Convert underscored string to lower-camelcase format
     *
     * @param string $underscored
     * @return string
     */
    static protected function underscoredToLowerCamelcase($underscored)
    {
        return preg_replace_callback('/_(.?)/', function($m) {return strtoupper($m[1]);}, $underscored);
    }

    /**
     * Convert camelcase string to underscored format
     *
     * @param string $camelcased
     * @return string
     */
    static protected function camelcasedToUnderscored($camelcased)
    {
        return strtolower(preg_replace('/([^A-Z])([A-Z])/', "$1_$2", $camelcased));
    }

    /**
     * Reset component model to default
     * component.model.reset
     *
     * @param int $componentId
     * @return bool
     */
    static protected function componentModelResetAction($componentId)
    {
        $component = new Component($componentId);
        Timber::clearCache();
        return (bool)$component->restore();
    }

    /**
     * Get component thumb
     * component.thumb.get
     *
     * @param array $componentIds
     * @return array
     */
    static protected function componentThumbGetAction($componentIds)
    {
        $thumbs = array();
        foreach ($componentIds as $componentId) {
            $component = new Component($componentId);
            $thumbs[$componentId] = $component->getThumb();
        }
        return $thumbs;
    }

    /**
     * Get layout CSS
     * layout.css.get
     *
     * @param int $layoutId
     * @return void
     */
    static protected function layoutCssGetAction($layoutId)
    {
        $layout = new Layout($layoutId);
        header('Content-Type:' . Http::CONTENT_TYPE_CSS);
        echo $layout->render(Http::CONTENT_TYPE_CSS);
        exit;
    }

    /**
     * Get component CSS
     * component.css.get
     *
     * @param array $componentIds
     * @param string $format
     * @return string
     */
    static protected function componentCssGetAction($componentIds, $format = 'css')
    {
        $result = '';
        $layout = new Layout(null);
        if (!is_array($componentIds) && !$componentIds = json_decode($componentIds, true)) {
            $result = '/* Request error */';
        } else {
            $layout->removeComponents();
            foreach ($componentIds as $componentId) {
                $layout->appendComponent(new Component($componentId));
            }
            $result = $layout->render(Http::CONTENT_TYPE_CSS);
        }
        if (strtolower($format) == 'json') {
            return $result;
        } else {
            header('Content-Type:' . Http::CONTENT_TYPE_CSS);
            echo $result;
            exit;
        }
    }

    /**
     * Set custom component CSS
     * component.customcss.set
     *
     * @param int $componentId
     * @param string $customCss
     * @return bool|int
     */
    static protected function componentCustomcssSetAction($componentId, $customCss)
    {
        require_once DM_BASE_PATH . '/vendor/Oyejorge/Less/Less.php';
        $parser = new \Less_Parser();
        try {
            $parser->parse(
                '#test' .
                ' { ' .
                $customCss .
                ' }'
            );
        } catch (\Exception $e) {
            throw new \Exception('CSS is not valid: ' . $e->getMessage(), 713434);
        }
        $component = new Component($componentId);
        $component->setCustomCss($customCss);
        return $component->save();
    }

    /**
     * Get custom component CSS
     * component.customcss.get
     *
     * @param int $componentId
     * @return string
     */
    static protected function componentCustomcssGetAction($componentId)
    {
        $component = new Component($componentId);
        return $component->getCustomCss();
    }

    /**
     * Save component data model
     * component.model.save
     *
     * @param array $models
     * @return boolean
     */
    static protected function componentModelSaveAction($models)
    {
        $result = true;
        foreach ($models as $componentId => $model) {
            $component = new Component($componentId);
            $component->setModel($model);
            $result = (bool)$component->save() && $result;
        }
        Timber::clearCache();
        return $result;
    }

    /**
     * Get component model
     * component.model.get
     *
     * @param int $componentId
     * @return array
     */
    static protected function componentModelGetAction($componentIds)
    {
        if (!is_array($componentIds)) {
            throw new \Exception('Component.model.get component_ids must be an array.', 827455);
        }
        $result = array();
        foreach ($componentIds as $componentId) {
            $component = new Component($componentId);
            $result[$componentId] = $component->getModel();
        }
        return $result;
    }

    /**
     * Create new component
     * component.create
     *
     * @param string $templateId
     * @param array $model
     * @param array $componentIds CSS of that components will be added
     * @return array
     */
    static protected function componentCreateAction($templateId, $model = array(), $componentIds = array())
    {
        $component = new Component();
        $component->setTemplate(new Template($templateId));
        $segmants = explode(DM_TEMPLATE_DELIMETER, $templateId);
        if (in_array($segmants[0], array('footer', 'menu')) && Settings::get(Settings::SETTING_LOGO)) {
            $cmodel = $component->getModel();
            if (!empty($cmodel['brand']['src']['base'])) {
                $cmodel['brand']['src']['base'] = Settings::get(Settings::SETTING_LOGO);
                $component->setModel($cmodel);
            }
        }
        if (! empty($model)) {
            $component->setModel($model);
        }
        $componentId = $component->save();
        if ($componentId) {
            $tpl = self::componentTemplateGetAction($componentId);
            $models = self::componentModelGetAction(array($componentId));
            $componentIds[] = $componentId;
            $layoutCss = self::componentCssGetAction($componentIds, 'json');
            return array(
                'component_id' => $componentId,
//                 'css' => $tpl['css'],
                'layout_css' => $layoutCss,
                'custom_css' => $tpl['custom_css'],
                'html' => $tpl['html'],
                'template_id' => $tpl['id'],
                'js' => $tpl['js'],
                'model' => $models[$componentId],
                'is_custom' => $tpl['is_custom']
            );
        } else {
            throw new \Exception('Component was not created.', 324973);
        }
    }

    /**
     * Create a duplicate of component
     * component.duplicate
     *
     * @param int $componentId
     * @param array $model
     * @return array
     */
    static protected function componentDuplicateAction($componentId, $model)
    {
        $component = new Component($componentId);
        $component = $component->duplicate();
        $component->setModel($model);
        $component->save();

        $tpl = self::componentTemplateGetAction($component->getId());
        $layoutCss = self::componentCssGetAction(array($component->getId()), 'json');
        return array(
            'component_id' => $component->getId(),
            'css' => $tpl['css'],
            'layout_css' => $layoutCss,
            'custom_css' => $tpl['custom_css'],
            'html' => $tpl['html'],
            'template_id' => $tpl['id'],
            'js' => $tpl['js'],
            'model' => $component->getModel(),
            'is_custom' => $tpl['is_custom']
        );
    }

    /**
     * Get list of components
     * components.get
     *
     * @param string $templateId Filter by template
     * @return array
     */
    static protected function componentsGetAction($templateId = null)
    {
        $params = array();
        if ($templateId) {
            $filter = ' && `template_id` = %s ';
            $params[] = $templateId;
        }
        $components = Db::getAll(
            'SELECT * FROM `' . Db::getPluginTableName(Db::TABLE_COMPONENT) . '`
            WHERE is_system = 0 && is_hidden = 0 ' . $filter . '
            ORDER BY `id` DESC',
            $params
        );
        return $components;
    }

    /**
     * Save layout
     * layout.save
     *
     * @param int $layoutId
     * @param array $components
     * @param bool $isPreview
     * @return int Layout ID
     */
    static protected function layoutSaveAction($layoutId, $components, $isPreview = false)
    {
        $layout = new Layout($layoutId);
        $layout->removeComponents();
        foreach ($components as $component) {
            $componentId = ! empty($component['id']) ? $component['id'] : null;
            if ($componentId) {
                $thumb = ! empty($component['thumb']) ? $component['thumb'] : null;
                $component = new Component($componentId);
                if ($thumb) {
                    $component->setThumb($thumb);
                    $component->save();
                }
                $layout->appendComponent($component);
            }
        }
        $layoutId = $layout->save($isPreview);

        $postId = Db::getColumn('SELECT post_id FROM  `' . Db::getWpDb()->postmeta . '` WHERE  `meta_key` LIKE  "_qards_page_layout" AND  `meta_value` =  %d', array($layoutId));

        if ($postId) {
            $content = $layout->render(Http::CONTENT_TYPE_HTML, false);
            wp_update_post(array('ID' => $postId, 'post_content' => $content));
        }

        // Clear old unrelated components
        Cleaner::cleanDb();
        Timber::clearCache();
        if ($layoutId) {
            return $layoutId;
        } else {
            throw new \Exception('Layout was not saved.', 764373);
        }
    }

    /**
     * Get list of layout components
     * layout.components.get
     *
     * @param unknown $layoutId
     * @return multitype:NULL
     */
    static protected function layoutComponentsGetAction($layoutId)
    {
        $layout = new Layout($layoutId);
        $result = array();
        foreach ($layout->getComponents() as $component) {
            $result[] = $component->toArray();
        }
        return $result;
    }

    /**
     * Get template of the component
     * component.template.get
     *
     * @param int $componentId
     * @param bool $pureHtml
     * @param array $model
     * @return array
     */
    static protected function componentTemplateGetAction($componentId, $pureHtml = false, $model = null)
    {
        $result = array();
        if ($pureHtml) {
            Context::getInstance()->set('edit_mode', false);
        } else {
            Context::getInstance()->set('edit_mode', true);
        }
        $layout = new Layout(null);
        $component = new Component($componentId);
        $layout->setComponents(array($component));

        if (\is_array($model)) {
            Context::getInstance()->set('edit_mode', false);
            $componentNew = $component->duplicate();
            $componentNew->setModel($model);
            $componentNew->save();
            $layout->setComponents(array($componentNew));
//             $componentNew = new Component();
//             $componentNew->setTemplate($component->getTemplate());
//             $componentNew->setModel($model);
//             $componentNew->setCustomCss($component->getCustomCss());
//             $componentNew->save();
//             $layout->setComponents(array($componentNew));
        }

        $result = array(
            'id' => $component->getTemplate()->getId(),
            'html' => $layout->render(Http::CONTENT_TYPE_HTML, false),
            'css' => $layout->render(Http::CONTENT_TYPE_CSS, false),
            'custom_css' => $component->getCustomCss(),
            'js' =>  $component->getTemplate()->getJs(),
            'is_custom' => $component->getTemplate()->isCustom()
        );
        if ($pureHtml) {
            $codeTydyObj = new CodeTidy();
            $result['html'] = $codeTydyObj->parseHtml($result['html']);
            $result['custom_css'] = $codeTydyObj->parseCss($result['custom_css']);
        }
        return $result;
    }

    /**
     * Get permalink
     * permalink.get
     *
     * @param int $postId
     * @param array $query
     * @return string
     */
    static protected function permalinkGetAction($postId, $query = array())
    {
        return add_query_arg($query, get_permalink($postId));
    }

    /**
     * Get HTML for preview
     * layout.preview.get
     *
     * @param array $components array(array('component_id' => '1', 'template_id' => 'header.header1', 'model' => array()), array('template_id' => 'header.header2'))
     * @param bool $withHeaderFooter Add header and footer parts
     * @return string
     */
    static protected function layoutPreviewGetAction($components)
    {
        Timber::clearCache();
        $layout = new Layout();
        $components = is_array($components) ? $components : array();
        foreach ($components as $componentData) {
            $component = new Component();
            $templateId = empty($componentData['template_id']) ? null : $componentData['template_id'];
            $component->setTemplate(new Template($templateId));
            if (isset($componentData['model']) && is_array($componentData['model'])) {
                $component->setModel($componentData['model']);
            }
            if (isset($componentData['component_id'])) {
                $origComponent = new Component($componentData['component_id']);
                $component->setCustomCss($origComponent->getCustomCss());
            }
            if ($component->save()) {
                $layout->appendComponent($component);
            } else {
                throw new \Exception('Can not create component by template "' . $componentData['template_id'] . '".', 823764);
            }
        }
        $layout->save(true);
        return array(
            'layout_id' => $layout->getId(),
            'html' => $layout->render(Http::CONTENT_TYPE_HTML, false),
            'css' => $layout->render(Http::CONTENT_TYPE_CSS, false)
        );
    }

    /**
     * Save component for preview
     * component.preview.set
     *
     * @param array $components array(array('component_id' => int, 'html' => '...', 'custom_css' => '...'))
     * @return int layout id
     */
    static protected function componentPreviewSetAction($components)
    {
        $components = is_array($components) ? $components : array();
        $layout = new Layout();
        foreach ($components as $data) {
            $origComponentId = $data['component_id'];
            $tpl = $data['html'];
            $customCss = $data['custom_css'];
            $origComponent = new Component($origComponentId);

            $component = $origComponent->duplicate();
            $component->setCustomCss($customCss);
            if ($tpl) {
                $template = $component->getTemplate()->duplicate();
                $template->setTpl($tpl);
                $template->save();
                $component->setTemplate($template);
            }

            if ($component->save()) {
                $layout->appendComponent($component);
            } else {
                throw new \Exception('Can not create component.', 734341);
            }
        }
        $layout->save(true);
        return array(
            'layout_id' => $layout->getId()
        );
    }

    /**
     * Get component for preview
     * component.preview.get
     *
     * @param int $layoutId layout id
     * @return void
     */
    static protected function componentPreviewGetAction($layoutId)
    {
        add_action('wp_print_styles',  array('Designmodo\Qards\Utility\StyleScriptFilter', 'filterStyles'));
        add_action('wp_print_scripts', array('Designmodo\Qards\Utility\StyleScriptFilter', 'filterScripts'));
        $layout = new Layout($layoutId);
        Context::getInstance()->set('current_layout_id', $layoutId);
        $fontList = $layout->getFonts();
        Context::getInstance()->set('current_component_ids', $layout->getComponents());
        Context::getInstance()->set('fontList', $fontList);
        echo $layout->render(Http::CONTENT_TYPE_HTML);
        exit;
    }

    /**
     * Set post title
     * post.title.set
     *
     * @param int $postId
     * @param string $title
     * @return bool
     */
    static protected function postTitleSetAction($postId, $title)
    {
        Timber::clearCache();
//         $postId = Db::getColumn('SELECT post_id FROM  `' . Db::getWpDb()->postmeta . '` WHERE  `meta_key` LIKE  "_qards_page_layout" AND  `meta_value` =  %d', array($layoutId));
        return (bool)wp_update_post(array('ID' => $postId, 'post_title' => $title));
//         return (bool)Db::update(
//             Db::getWpDb()->posts,
//             array('post_title' => $title),
//             array('ID' => $postId)
//         );
    }

    /**
     * Get global CSS
     * settings.globalcss.get
     *
     * @return void
     */
    static protected function settingsGlobalcssGetAction()
    {
        header('Content-Type:' . Http::CONTENT_TYPE_CSS);
        echo Settings::get(Settings::SETTING_GLOBAL_CSS);
        exit;
    }

    /**
     * Add\Update template
     * template.save
     *
     * @param string $templateId optional
     * @param string $html
     * @param string $css
     * @return bool
     */
    static protected function templateSaveAction($templateId, $html, $css)
    {
        $template = new Template($templateId);
//         $template->setCss($css);
        $template->setTpl($html);
        return $template->save();
    }

    /**
     * Get Fonts list
     * font.get
     *
     * @return string
     */
    static protected function fontGetAction()
    {
        $file = @file_get_contents(DM_TPL_JS_CONFIG_PATH.DM_DS.'fonts.json');
        if (!$file) {
            throw new \Exception('No proper json file in' . DM_TPL_JS_CONFIG_PATH.DM_DS.'fonts.json', 723747);
        }
        header('Content-Type:' . Http::CONTENT_TYPE_JSON);
        // FIXME change to normal json response
        echo $file;
        exit;
    }

    /**
     * Duplicate post
     * page.duplicate
     *
     * @param int $postId
     * @param string $status
     * @throws \Exception
     * @return int
     */
    static protected function pageDuplicateAction($postId, $status = 'draft')
    {
        if (!current_user_can('edit_posts')) {
            wp_die(__('This user have no permisson to "edit_posts".'));
            exit;
        }
        $postId = Duplicate::duplicatePost((int)$postId, $status, get_current_user_id());
        if (!$postId) {
            throw new \Exception(__('This post could not be duplicated.'), 327412);
        }
        return $postId;
    }

    /**
     * Convert post to/from qards
     * page.convert
     *
     * @param int $postId
     * @param int $qardsMode 0 - not qards, 1 - qards, 3 - embed
     * @return bool
     */
    static protected function pageConvertAction($postId, $qardsMode)
    {
        $post = get_post($postId);
        if (!$post) {
            throw new \Exception('Page not found.', 923134);
        }
        // Get currnt layout
        $layoutId = Post::getOpt($post->ID, Post::OPT_PAGE_LAYOUT);
        if ($post->post_status == 'auto-draft') {
            wp_update_post(array('ID' => $post->ID, 'post_status' => 'draft'));
        }

        $convertToQards = function ($post) {
            // Get currnt layout
            $layoutId = Post::getOpt($post->ID, Post::OPT_PAGE_LAYOUT);
            // Attache layout
            $layoutId = Post::getOpt($post->ID, Post::OPT_OLD_PAGE_LAYOUT);
            if (!$layoutId) {
                $layout = new Layout();
                $layoutId = $layout->save();
            }

            Post::setOpt($post->ID, Post::OPT_PAGE_LAYOUT, $layoutId);

            // Save original content
            Post::setOpt($post->ID, Post::OPT_ORIG_PAGE_CONTENT, $post->post_content);
            Post::deleteOpt($post->ID, Post::OPT_OLD_PAGE_LAYOUT);
            $layout = new Layout($layoutId);
            $content = $layout->render(Http::CONTENT_TYPE_HTML, false);
            wp_update_post(array('ID' => $post->ID, 'post_content' => $content));
        };
        $convertToPost = function ($post) {
            // Get currnt layout
            $layoutId = Post::getOpt($post->ID, Post::OPT_PAGE_LAYOUT);
            // Back original content
            wp_update_post(
                array(
                    'ID' => $post->ID,
                    'post_content' => Post::getOpt($post->ID, Post::OPT_ORIG_PAGE_CONTENT)
                )
            );
            // Deattache layout
            Post::setOpt($post->ID, Post::OPT_OLD_PAGE_LAYOUT, $layoutId);
            Post::deleteOpt($post->ID, Post::OPT_PAGE_LAYOUT);
            Post::deleteOpt($post->ID, Post::OPT_QARDS_MODE);
            Post::deleteOpt($post->ID, Post::OPT_ORIG_PAGE_CONTENT);
        };
        $convertToEmbed = function ($post) {
            // Get currnt layout
            $layoutId = Post::getOpt($post->ID, Post::OPT_PAGE_LAYOUT);
            // Attache layout
            $layoutId = Post::getOpt($post->ID, Post::OPT_OLD_PAGE_LAYOUT);
            if (!$layoutId) {
                $layout = new Layout();
                $layoutId = $layout->save();
            }
            Post::setOpt($post->ID, Post::OPT_PAGE_LAYOUT, $layoutId);
            // Save original content
            Post::setOpt($post->ID, Post::OPT_ORIG_PAGE_CONTENT, $post->post_content);

            // Flag as embed
            Post::setOpt($post->ID, Post::OPT_QARDS_MODE, 'embed');
            // Set content
//             wp_update_post(
//                 array(
//                     'ID' => $post->ID,
//                     'post_content' => '[qards_inline id="' . $post->ID . '"]'
//                 )
//             );

            Post::deleteOpt($post->ID, Post::OPT_OLD_PAGE_LAYOUT);
            $layout = new Layout($layoutId);
            $content = $layout->render(Http::CONTENT_TYPE_HTML, false);
            wp_update_post(array('ID' => $post->ID, 'post_content' => $content));
        };

        switch ((int)$qardsMode) {
            case 0: // Convert to post
                if ($layoutId) {
                    $convertToPost($post);
                }
                break;
            case 1: // Convert to qards
                if ($layoutId) {
                    $convertToPost($post);
                }
                $convertToQards($post);
                break;
            case 3: // Convert to embed
                if ($layoutId) {
                    $convertToPost($post);
                }
                $convertToEmbed($post);
                break;
            default:
                throw new \Exception('Unknown mode given.', 834132);
                break;
        }
        return true;
    }

    /**
     * Remove attached image
     * attachement.delete
     *
     * @param string $attachementUrl
     * @return bool
     */
    static protected function attachementDeleteAction($attachementUrl)
    {
        // Normalize URL
        $address = explode('/', $attachementUrl);
        $keys = array_keys($address, '..');
        foreach($keys AS $keypos => $key) {
            array_splice($address, $key - ($keypos * 2 + 1), 2);
        }
        $address = implode('/', $address);
        $attachementUrl = str_replace('./', '', $address);

        // Get attach id by url
        $image = Db::getRow('SELECT * FROM `' . Db::getWpDb()->posts  . '` WHERE post_type="attachment" AND guid = %s', array($attachementUrl));
        if (empty($image['ID'])) {
            throw new \Exception('This attachment can not be deleted.', 247614);
        }
        $attachmentid = $image['ID'];

        // Delete by WP
        if (wp_delete_attachment( $attachmentid, true) === false) {
            throw new \Exception('Can\'t delete record from filesystem, please, check file permissions of your upload folder: '.wp_upload_dir() . '.', 234824);
        }
        return true;
    }

    /**
     * Registrantion of new subscriber
     * subscriber.add
     *
     * @param string $email
     * @return bool
     */
    static protected function subscriberAddAction($email)
    {
        return Subscription::addSubscriber($email);
    }

    /**
     * Registrantion of new subscriber
     * subscribers.export
     *
     * @return void
     */
    static protected function subscribersExportAction()
    {
        Subscription::export();
        exit;
    }

    /**
     * Get settings values
     * settings.get
     *
     * @param array $settings
     * @return array
     */
    static protected function settingsGetAction($settings)
    {
        if (!is_array($settings)) {
            throw new \Exception('Error in settings.get method: settings must be an array.', 912847);
        }
        $result = array();
        foreach ($settings as $setting) {
            $result[$setting] = Settings::get($setting);
        }
        return $result;
    }

    /**
     * Set settings values
     * settings.set
     *
     * @param string $settings
     * @param string $val
     * @return bool
     */
    static protected function settingsSetAction($settings)
    {
        if (!is_array($settings)) {
            throw new \Exception('Error in settings.set method: settings must be an array.', 112847);
        }
        foreach ($settings as $setting => $val) {
            Settings::set($setting, $val);
        }
        return true;
    }

    /**
     * Reset settings values
     * settings.reset
     *
     * @return bool
     */
    static protected function settingsResetAction()
    {
        Settings::reset();
        return true;
    }

    /**
     * Upload file
     * attachement.upload
     *
     * @param array $sizes
     * @throws \Exception
     * @return array
     */
    static protected function attachementUploadAction($sizes = array())
    {
        if (! empty($_FILES)) {
            $allowedMimeTypes = array(
                'image/jpeg',
                'image/pjpeg',
                'image/png',
                'image/x-png',
                'image/gif',
                'image/svg+xml',
                'video/mp4',
                'video/ogg',
                'video/webm'
            );
            if (!in_array($_FILES['file']['type'], $allowedMimeTypes)) {
                throw new \Exception($_FILES['file']['type'] . ' is not allowed file type.', 813223);
            }
            if(!current_user_can('upload_files')) {
                throw new \Exception('You are have no the upload_files permission.', 876554);
            }
            require_once (ABSPATH . 'wp-admin/includes/admin.php');

            $id = \media_handle_upload('file', 0);
            unset($_FILES);
            if (\is_wp_error($id)) {
                throw new \Exception('There was an error uploading your file. Make sure that file is png/jpeg/gif and not greater than ' . ini_get('upload_max_filesize'), 323335);
            }
            $result = array(
            );
            if (! empty($sizes)) {
                foreach ($sizes as $key => $size) {
                    $maxWidth = ! empty($size['max_width']) ? $size['max_width'] : null;
                    $maxHeight = ! empty($size['max_height']) ? $size['max_height'] : null;
                    $crop = ! empty($size['crop']) ? true : false;
                    if ($maxWidth || $maxHeight) {
                        $image = wp_get_image_editor((get_attached_file($id))); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                        if (! is_wp_error($image)) {
                            //wp_delete_attachment($id, true);
                            $image->resize($maxWidth, $maxHeight, $crop);
                            $imageData = $image->save($image->generate_filename($image->get_suffix() . '-' . rand(10000, 99999)));
                            $wp_upload_dir = wp_upload_dir();
                            $attachment = array(
                                'guid' => $wp_upload_dir['url'] . '/' . basename($imageData['path']),
                                'post_mime_type' => $imageData['mime-type'],
                                'post_title' => preg_replace('/\.[^.]+$/', '', basename($imageData['path'])),
                                'post_content' => '',
                                'post_status' => 'inherit'
                            );
                            $resizedId = wp_insert_attachment($attachment, $imageData['path']);
                            $attachData = wp_generate_attachment_metadata($resizedId, $imageData['path']);
                            wp_update_attachment_metadata($resizedId, $attachData);
                            $result[$key] = str_replace(content_url(), '../..', wp_get_attachment_url($resizedId));
                        }
                    }
                }
            }
            $result['original'] = str_replace(content_url(), '../..', wp_get_attachment_url($id));
        } else {
            throw new \Exception('Upload error.', 97245);
        }
        return $result;
    }
}
