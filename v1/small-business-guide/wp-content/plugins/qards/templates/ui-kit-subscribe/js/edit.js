window.dmTemplateInit = window.dmTemplateInit || {};

window.dmTemplateInit['subscribe'] = function(component, edit) {
    if (!edit) {
        $('.button-subscribe', component).on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                subscribe = $this.closest('.subscribe'),
                email = $('.email-subscribe', subscribe).val();

            subscribe.removeClass('error done');
            subscribe.addClass('loading');

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                data: {
                    action: 'dm_api',
                    method: 'subscriber.add',
                    params: {
                        email: email
                    }
                },
                dataType: 'json',
                success: function(response) {
                    subscribe.removeClass('loading');

                    if ('result' in response) {
                        subscribe.addClass('done');
                    } else if ('error' in response) {
                        subscribe.addClass('error');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("API failed: " + textStatus + ": " + errorThrown);
                    subscribe.removeClass('loading error done');
                }
            });
        })
    }
};
