window.dmTemplateInit = window.dmTemplateInit || {};

window.dmTemplateInit['cover'] = function(component, edit) {
	function resizeCover() {
		var parentWindowWidth = $('body').data('parent-window-width');
		var currentWindowWidth = $(document).width();
		if (component.find('.container').length) {
			var paddingTop = component.find('.holder').css('padding-top').replace('px', '');
			if ($('body.spaceforsticky').exists()) paddingTop = parseFloat(paddingTop) + 61;
		}
		var contentHeight = component.find('.content').outerHeight();
		component.find('.cover').css({minHeight: contentHeight + (paddingTop * 2)});
	}

	if (component.find('.cover').exists()) {
		//resize covers
		resizeCover();
		$(window).on('load blur resizeEnd', function() {
			resizeCover();
		});
		$('body').on('keyup', '[contenteditable]', function() {
			resizeCover();
		});
		//if it's not edit mode
		if (!edit) {
			//VIDEO BG
			var backgroundHolder = component.find('.cover.video.youtube').find('.background');

			backgroundHolder.YTPlayer();
			// EFFECTS FOR COVER
			if (!window.isMobile) {
				if (component.find('.cover.parallax:not(.video)').exists()) {
					//prepare
					component.find('.background').wrap('<div class="background-wrapper"/>')
					var once = 0;
					$(window).on('scroll load', function() {
						component.find('.parallax').each(function(index, element) {

							var elementsPosition = $(element).offset();
							var scrollTop = $(document).scrollTop(); if (scrollTop < 0) scrollTop = 0;
							var windowHeight = $(window).height();
							var scale = (scrollTop - elementsPosition.top) / windowHeight;
							var propotion = windowHeight / 10;
							var contentElement = $(element).find('.content');

							if ((scale > -1) && (scale < 1)) {
								//paralax effect on scroll
								var textPosition = (propotion * (scrollTop - elementsPosition.top) / windowHeight);
								$(contentElement).css('-webkit-transform', 'translateY(' + textPosition + 'px)')
									.css('-moz-transform', 'translateY(' + textPosition + 'px)')
									.css('-ms-transform', 'translateY(' + textPosition + 'px)')
									.css('-o-transform', 'translateY(' + textPosition + 'px)')
									.css('transform', 'translateY(' + textPosition + 'px)')
									.css('opacity', 1 - scale * 0.5);

								if (scale > 0) {
									var endOpacity = Math.abs(scale - 1) + 0.2;
									if (endOpacity > 1) endOpacity = 1;
									$(element).find('.background-wrapper').css('opacity', endOpacity);
									$(contentElement).css('opacity', Math.abs(scale - 1));
								} else {
									var endOpacity = 1 - Math.abs(scale) + 0.2;
									if (endOpacity > 1) endOpacity = 1;
									$(element).find('.background-wrapper').css('opacity', endOpacity);
									$(contentElement).css('opacity', 1 - Math.abs(scale));
								}
							}

						});
					});
				}
			}
		} else {
			var backgroundHolder = component.find('.cover.video.youtube').find('.background');

			if(component.hasClass('custom-block')) {
				backgroundHolder.YTPlayer();
				backgroundHolder.on("YTPStart", function(e){
					backgroundHolder.pauseYTP();
				});
			}
		}
	}
};
