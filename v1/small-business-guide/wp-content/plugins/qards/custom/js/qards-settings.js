(function($) {
    $(function() {
        var qardsWPSettings = '#qards-settings';

        // Store data of all fields except checkboxes and fileuploads
        // It needs to compare initial data with data that has been changed (obj qardsWPSettingsDataToSave)
        var qardsWPSettingsInitData = {
            siteTitle: '',
            googleKey: '',
            typeKitID: '',
            editingCSS: '',
            editingHead: '',
            editingHeader: '',
            editingFooter: ''
        };

        // Store data of all fields except checkboxes and fileuploads, they will save once after upload
        // It needs to save data
        var qardsWPSettingsDataToSave = {};

        /* Populate Settings Init Data */
        populateSettingInitData();

        /* Initial set up for preview images */
        $('.preview-img', qardsWPSettings).each(function() {
            var $this = $(this);

            if($this.data('preview-src')) {
                $this.closest('.tab-row').removeClass('no-image');
            }
        });

        /* Initial set up for TypeKitID */
        if($('#typeKitID', qardsWPSettings).val()) {
            $('#typeKitID', qardsWPSettings).closest('.tab-row').addClass('has-typekit');
        }

        /* Checkboxes */
        $('.checkbox', qardsWPSettings).each(function() {
            var $this = $(this);

            $this.on('change', function() {
                var checkboxSetting = {};

                checkboxSetting[$this.attr('name')] = $this.prop('checked');

                apiCall(checkboxSetting);
            });
        });

        /* File uploader */
        $('.fileupload', qardsWPSettings).fileupload({
            url: ajaxurl + '?action=dm_api&method=attachement.upload',
            paramName: 'file',
            dataType: 'json',
            send: function(e, data) {
                var maxFileSize = $(this).data('file-size');

                if (maxFileSize && data.total > maxFileSize) {
                    alert('Image file size has been exceeded.');
                    return false;
                }
            },
            done: function (e, data) {
                var $this = $(this),
                    parent = $this.closest('.tab-row'),
                    previewImg = parent.find('.preview-img'),
                    imageSetting = {};

                if(data.result.result && data.result.result.original) {
                    imageSetting[$this.attr('name')] = data.result.result.original;

                    apiCall(imageSetting, function() {
                        previewImg.attr('src', pluginUrlAdmin + data.result.result.original);
                        parent.removeClass('no-image');
                    });
                }
            }
        });
        $('.remove-image', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                imageSetting = {};

            imageSetting[$this.closest('.tab-row').find('.fileupload').attr('name')] = '';

            apiCall(imageSetting, function() {
                $this.closest('.tab-row').addClass('no-image').find('.preview-img').attr('src', '');
            });
        });

        /* Export Subscribers */
        $('#exportSubscribers', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            location.href = ajaxurl + '?action=dm_api&method=subscribers.export';
        });

        /* Edit Button */
        $('.edit-button', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                parent = $this.closest('.tab-row'),
                codeEditor = parent.find('.code-editor'),
                editableText = parent.find('.editable-text');

            parent.addClass('changes');

            if(editableText.length) {
                editableText.focus();
            }

            if(codeEditor.length) {
                var codeEditorInstance = codeEditor.find('.CodeMirror')[0].CodeMirror;

                codeEditor.addClass('show');
                codeEditorInstance.refresh();
                codeEditorInstance.focus();
            }
        });

        /* Editable text */
        $('.editable-text', qardsWPSettings).each(function() {
            var editableText = $(this);

            editableText.flexText();
            editableText.on('keypress', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
            });
            editableText.on('keyup', function(e) {
                var $this = $(this),
                    value = $this.val();

                dirtyCheck($this.attr('id'), value);

                if (e.keyCode == 13) {
                    if($this.attr('id') !== 'typeKitID') {
                        saveChanges($this.attr('id'));
                    } else {
                        // check if it's valid typeKitID
                        typeKitValidation(value, $this);
                    }
                }
            });
            editableText.on('focus', function() {
                var $this = $(this),
                    id = $this.attr('id'),
                    textarea = document.getElementById(id);

                $this.addClass('focus');

                moveCursorToEnd(textarea);
                setTimeout(function() {
                    moveCursorToEnd(textarea);
                }, 1);
            });
            editableText.on('blur', function() {
                var $this = $(this);

                $this.removeClass('focus');
            });
        });

        /* Remove TypeKitID */
        $('.remove-typekitId', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            removeTypeKitId($(this));
        });

        /* Init CodeMirror */
        $('.code-editor-textarea', qardsWPSettings).each(function(index) {
            initCodeMirror($(this).attr('id'), $(this).data('code-mode'));
        });

        /* Save button */
        $('.save-button', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                parent = $this.closest('.tab-row'),
                el = parent.find('.text-field'),
                id = el.attr('id'),
                name = el.attr('name');

            if(id !== 'typeKitID') {
                saveChanges(id);
            } else {
                // check if it's valid typeKitID
                typeKitValidation(el.val(), el);
            }
        });

        /* Cancel button */
        $('.cancel-button', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                parent = $this.closest('.tab-row'),
                el = parent.find('.text-field'),
                id = el.attr('id'),
                name = el.attr('name');

            parent.removeClass('changes');
            parent.find('.code-editor').removeClass('show');

            if(name in qardsWPSettingsDataToSave) {
                if(el.hasClass('editable-text')) {
                    el.val(qardsWPSettingsInitData[id]);
                    parent.find('.flex-text-wrap span').text(qardsWPSettingsInitData[id]);
                }
                if(el.hasClass('code-editor-textarea')) {
                    parent.find('.CodeMirror')[0].CodeMirror.setValue(qardsWPSettingsInitData[id]);
                }
                delete qardsWPSettingsDataToSave[name];
            }
        });

        /* Reset Settings */
        $('#resetSettings', qardsWPSettings).on('click', function(e) {
            e.preventDefault();

            if(confirm('Are you sure you want to reset all settings to defaults?')) {
                $.ajax({
                    url: ajaxurl,
                    method: 'POST',
                    data: {
                        action: 'dm_api',
                        method: 'settings.reset',
                        params: {}
                    },
                    dataType: 'json',
                    success: function(response) {
                        if ('result' in response) {
                            location.reload();
                        } else if ('error' in response) {
                            alert(response.error.code, response.error.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert(textStatus, errorThrown);
                    }
                });
            }
        });

        function removeTypeKitId($this) {
            var el = $('#typeKitID', qardsWPSettings),
                name = el.attr('name'),
                parent = $this.closest('.tab-row'),
                removeIDSetting = {};

            removeIDSetting[name] = '';

            apiCall(removeIDSetting, function() {
                qardsWPSettingsInitData['typeKitID'] = '';
                delete qardsWPSettingsDataToSave[name];
                el.val('');
                parent.removeClass('changes has-typekit');
            });
        }

        function initCodeMirror(id, mode) {
            var editor = CodeMirror.fromTextArea(document.getElementById(id), {
                lineNumbers: true,
                mode: mode,
                lineWrapping: true,
                scrollbarStyle: 'simple',
                tabSize: 2
            });
            var totalLines = editor.lineCount();
            var totalChars = editor.getTextArea().value.length;
            editor.autoFormatRange({
                line: 0,
                ch: 0
            }, {
                line:totalLines,
                ch:totalChars
            });

            editor.on('change', function(obj){
                dirtyCheck(id, obj.getValue());
            });
        }

        function saveChanges(id) {
            var el = $('#' + id, qardsWPSettings),
                name = el.attr('name'),
                parent = el.closest('.tab-row'),
                changes = {};

            el.blur();
            if(name in qardsWPSettingsDataToSave) {
                changes[name] = qardsWPSettingsDataToSave[name];

                apiCall(changes, function() {
                    qardsWPSettingsInitData[id] = changes[name];
                    delete qardsWPSettingsDataToSave[name];
                    parent.removeClass('changes');
                    parent.find('.code-editor').removeClass('show');
                });
            } else {
                parent.removeClass('changes');
                parent.find('.code-editor').removeClass('show');
            }
        }

        function revertTypeKit($this) {
            var parent = $this.closest('.tab-row');

            $this.val(qardsWPSettingsInitData['typeKitID']);
            parent.find('.flex-text-wrap span').text(qardsWPSettingsInitData['typeKitID']);

            if(qardsWPSettingsInitData['typeKitID']) {
                parent.addClass('has-typekit');
            } else {
                parent.removeClass('has-typekit');
            }

            dirtyCheck($this.attr('id'), $this.val());
            parent.removeClass('changes');
            $this.blur();
        }

        /* Check data between qardsWPSettingsInitData and new value */
        function dirtyCheck(id, value) {
            var el = $('#' + id, qardsWPSettings),
                name = el.attr('name'),
                parent = el.closest('.tab-row');

            if(qardsWPSettingsInitData[id] !== value) {
                qardsWPSettingsDataToSave[name] = value;
            } else {
                delete qardsWPSettingsDataToSave[name];
            }

            if(name in qardsWPSettingsDataToSave) {
                parent.addClass('changes');
            }
        }

        function populateSettingInitData() {
            for(var i in qardsWPSettingsInitData) {
                if($('#' + i).hasClass('code-editor-textarea')) {
                    qardsWPSettingsInitData[i] = $('#' + i).siblings('.CodeMirror')[0] ? $('#' + i).siblings('.CodeMirror')[0].CodeMirror.getValue() : $('#' + i).val();
                } else {
                    qardsWPSettingsInitData[i] = $('#' + i).val();
                }
            }
        }

        function moveCursorToEnd(el) {
            if (typeof el.selectionStart == "number") {
                el.selectionStart = el.selectionEnd = el.value.length;
            } else if (typeof el.createTextRange != "undefined") {
                el.focus();
                var range = el.createTextRange();
                range.collapse(false);
                range.select();
            }
        }

         function typeKitValidation(value, $this) {
            if($this.attr('name') in qardsWPSettingsDataToSave) {
                if(value) {
                    $.ajax({
                        url: 'https://typekit.com/api/v1/json/kits/' + value + '/published',
                        method: 'GET',
                        data: {},
                        dataType: 'jsonp'
                    })
                    .done(function(response) {
                        if ('kit' in response) {
                            saveChanges($this.attr('id'));
                            $this.closest('.tab-row').addClass('has-typekit');
                        } else if ('errors' in response) {
                            if(response.errors[0].toLowerCase() === 'not found') {
                                alert('Kit "' + value + '" not found or not published.');
                            } else {
                                alert(response.errors[0]);
                            }
                            revertTypeKit($this);
                        }
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        alert(textStatus, errorThrown);
                        revertTypeKit($this);
                    });
                } else {
                    removeTypeKitId($this);
                }
            } else {
                $this.closest('.tab-row').removeClass('changes');
                $this.blur();
            }
        }

        function apiCall(data, success, failure) {
            var defaultSuccess = function(result) {
                console.log(result);
            };
            var defaultFailure = function(code, msg) {
                alert("API failed: " + code + ": " + msg);
            };
            var success = success || defaultSuccess;
            var failure = failure || defaultFailure;

            $.ajax({
                url: ajaxurl,
                method: 'POST',
                data: {
                    action: 'dm_api',
                    method: 'settings.set',
                    params: JSON.stringify({
                        settings: data
                    })
                },
                dataType: 'json'
            })
            .done(function(response) {
                if ('result' in response) {
                    success(response.result);
                } else if ('error' in response) {
                    failure(response.error.code, response.error.message);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                failure(textStatus, errorThrown);
            });
        }

        $(window).on('beforeunload', function() {
            if(!$.isEmptyObject(qardsWPSettingsDataToSave)) {
                return 'Please save you changes before leaving.';
            }
        });

        /* Prevent body scroll if code editor has scroll */
        (function() {
            var trapElement,
                scrollableDist,
                trapClassName = 'trapScroll-enabled',
                trapSelector = '.CodeMirror-scroll';

            var trapWheel = function(e){
                if (!$('body').hasClass(trapClassName)) {
                    return;
                } else {
                    var curScrollPos = trapElement.scrollTop(),
                        wheelEvent = e.originalEvent,
                        dY = wheelEvent.deltaY;

                    if((dY > 0 && curScrollPos >= scrollableDist) || (dY < 0 && curScrollPos <= 0)) {
                        return false;
                    }
                }
            };

            $(document)
                .on('wheel', trapWheel)
                .on('mouseleave', trapSelector, function(){
                    $('body').removeClass(trapClassName);
                })
                .on('mouseenter', trapSelector, function(){
                    trapElement = $(this);

                    var containerHeight = trapElement.outerHeight(),
                        contentHeight = trapElement[0].scrollHeight;

                    scrollableDist = contentHeight - containerHeight;

                    if(contentHeight > containerHeight) {
                        $('body').addClass(trapClassName);
                    }
                });
        })();

        /* Make qardsWPSettings visible once all JS was loaded */
        (function() {
            $(qardsWPSettings).removeClass('invisible');
        })();
    });
})(jQuery);
