<?php 
/*
* template name: contact
*/
?>
<div class="contact-page">
	<?php get_template_part('templates/page', 'header'); ?>
	<?php get_template_part('templates/content', 'page'); ?>
	<div class="row social-channels">
		<div class="col-md-3">
			<h4>Or use our socials</h4>
		</div>
		<div class="col-md-9">
			<?php get_template_part('templates/social-buttons'); ?>
		</div>
	</div>
</div>
	


