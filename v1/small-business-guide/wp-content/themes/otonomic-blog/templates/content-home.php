<div class="row">
    <div class="col-xs-12">
        <?php
        wp_reset_query();
        global $my_query, $paged;
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args=array(
                'paged' => $paged,
                'posts_per_page' => 10,
                //'caller_get_posts'=>1
            );
        $my_query = new WP_Query($args);

        $i=0;
        while ($my_query->have_posts()) : $my_query->the_post();
            $i++;
        ?>

            <div class="media blog-item white-holder" itemscope="" itemtype="http://schema.org/Article">
            <?php if ( has_post_thumbnail() ): // check if the post has a Post Thumbnail assigned to it. ?>
                    <a class="pull-left thumb-wrap" href="<?php the_permalink(); ?>" rel="bookmark">
                        <?php the_post_thumbnail('homepage-thumb',array( 'alt' => false, 'title' => get_the_title(), 'itemprop' => 'image' )); ?>
                    </a>
            <?php endif; ?>

            <div class="media-body">
                <h3 itemprop="name" class="media-heading">
                    <a href="<?php the_permalink(); ?>" title="Read more" itemprop="url">
                        <?php the_title(); ?>
                    </a>
                </h3>

                <p class="post-meta">
                    By <?php the_author_link(); ?>  •  <span itemprop="dateCreated" datetime=""><?= get_the_date(); ?></span>
                    <meta itemprop="author" content="<?= get_the_author() ?>">
                </p>
                <hr style="margin-bottom:20px;margin-top:-8px;">
                <?php
                    echo substr(get_the_content(),0,1000);
                    if(strlen(get_the_content())>1000):
                ?>
                    <br>
                    <a href="<?php the_permalink(); ?>" class="btn btn-oto-orange pull-right">Read more..</a>
                <?php endif; ?>
            </div>

            <div class="post-shares">
                 <?php
                 $val=file_get_contents("http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls=".get_permalink());

                $value = json_decode($val);
                $share_count = $value[0]->share_count;
                if($share_count) {
                    echo $share_count." Shares";
                }
                ?>
            </div>
        </div>

            <?php if($i%2==0): //display button after every 2 blogs ?>
            <div class="create-website-block" align="center">
                <span>1-click website from your facebook page</span>
                <a href="http://otonomic.com/" class="btn btn-oto-orange">Create Your Website Now</a>

             </div>
        <?php endif; ?>

        <?php endwhile; ?>

        <nav class="post-nav">
            <ul class="pager">
              <li class="previous"><?php next_posts_link( '&larr; Older posts', $my_query->max_num_pages); //next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
              <li class="next"><?php previous_posts_link(__('Newer posts &rarr;',$my_query->max_num_pages)); ?></li>
            </ul>
        </nav>

        <?php wp_reset_postdata(); ?>
    </div>
</div>
