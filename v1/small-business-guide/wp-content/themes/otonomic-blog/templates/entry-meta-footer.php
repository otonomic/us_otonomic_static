<div class="post-meta">
    <span class="post-meta-author"><span class="glyphicons old_man"></span><?php the_author_link(); ?></span>
    <span class="post-meta-tags"><span class="glyphicons bookmark"></span><?php echo get_the_tag_list('',', ',''); ?></span>
    <span class="post-meta-categories"><span class="glyphicons show_big_thumbnails"></span><?php echo get_the_category_list(', '); ?></span>
</div>