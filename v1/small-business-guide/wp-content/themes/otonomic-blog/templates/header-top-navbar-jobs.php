<!-- Side Navbar -->
<?php get_template_part('templates/side-navbar'); ?>
<div class="navbar navbar-fixed-top" role="navigation" id="header">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" id="menu-toggle" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".sidebar-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="container">
                <a class="" href="https://otonomic.com"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-jobs.png" alt="otonomic.com"></a>
                <div class="navbar-right" >
                    <div id="page-like">
                    <div class="fb-like hidden-sm hidden-xs" data-href="http://www.facebook.com/otonomic/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>