<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
    <!--[if lt IE 8]>
<div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
</div>
<![endif]-->
    <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
        get_template_part('templates/header-top-navbar-jobs');
    } else {
//        get_template_part('templates/header');
    }
    ?>
    
    <div id="page-heading">
        <div class="container">
            <div class="row-fluid">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <h1 class="page-title"><?php echo get_the_title(); ?></h1>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/team.png" class="heading-img" />
                </div>
            </div>
        </div>
    </div><!-- #page-heading ends -->
    <div id="content">
        <div class="container">
            <div class="row-fluid">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <?php the_content(); ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div id="share">
                        <ul class="social-share">
                            <li class="google-plus">
                                <div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60"></div>
                            </li>
                            <li class="facebook">
                                <!-- div class="fb-share-button" data-href="http://www.facebook.com/otonomic/"></div -->
                                <a name="fb_share" type="box_count">Share</a>
                            </li>
                            <li class="tweeter">
                                <a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a>
                            </li>
                            <li class="linkedin">
                                <script type="IN/Share" data-counter="top"></script>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #content ends -->
    <div id="footer">
        <div class="row-fluid">
            <div class="span7 offset5">
                <div class="pull-right align-right">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tablet-dude.png" id="footer-overlap" />
                </div>
            </div>
        </div>
    </div><!-- #footer ends -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=373931652687761&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <script type="text/javascript">
        (function() {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/platform.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
    </script>
    <script>
        !function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, 'script', 'twitter-wjs');
    </script>
    <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
    <script src="//platform.linkedin.com/in.js" type="text/javascript">
    <?php get_template_part('templates/footer'); ?>
</body>
</html>