<?php
/*  ----------------------------------------------------------------------------
    WordPress booster framework - this is our theme framework - all the content and settings are there
    It is not necessary to include it in the child theme only if you want to use the API
*/
if (!defined('TD_THEME_WP_BOOSTER')) {
	include TEMPLATEPATH . '/includes/td_wordpres_booster.php';
}
add_action( 'init', 'blog_feature_custom_post' );
add_action( 'init', 'create_posttype' );

function create_posttype() {
    register_post_type( 'list_jobs',
        array(
            'labels' => array(
                'name' => __( 'Jobs Listing' ),
                'singular_name' => __( 'Job' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'jobs'),
        )
    );
    flush_rewrite_rules();
}
function blog_feature_custom_post() {
    $labels = array(
        'name'               => _x( 'Features', 'post type general name' ),
        'singular_name'      => _x( 'Feature', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'Feature' ),
        'add_new_item'       => __( 'Add New Feature' ),
        'edit_item'          => __( 'Edit Feature' ),
        'new_item'           => __( 'New Feature' ),
        'all_items'          => __( 'All Features' ),
        'view_item'          => __( 'View Feature' ),
        'search_items'       => __( 'Search Features' ),
        'not_found'          => __( 'No Feature found' ),
        'not_found_in_trash' => __( 'No Feature found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Feature'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our Features and Feature specific data',
        'public'        => true,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ,'page-attributes','post-formats' ),
        'has_archive'   => true,
    );
    register_post_type( 'feature', $args );
    $labels = array(
        'name' => _x( 'Feature Category', 'taxonomy general name' ),
        'singular_name' => _x( 'Feature Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Feature Category' ),
        'popular_items' => __( 'Popular Feature Category' ),
        'all_items' => __( 'All Feature Category' ),
        'parent_item' => __( 'Parent Feature Category' ),
        'parent_item_colon' => __( 'Parent Feature Category:' ),
        'edit_item' => __( 'Edit Feature Category' ),
        'update_item' => __( 'Update Feature Category' ),
        'add_new_item' => __( 'Add New Feature Category' ),
        'new_item_name' => __( 'New Feature Category Name' ),
    );
    register_taxonomy('feature_category',array('feature'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'feature_category' ),
    ));
}
