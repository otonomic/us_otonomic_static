<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'small_business_guide');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '56#0BM]%a59(7*e4*');

/** MySQL hostname */
define('DB_HOST', 'dbmaster.otonomic.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7+x|v $?5=nC4Rx.CF2Zp<JdzK dge(T[?0q{RKkEE+!UBd#Uh1<(&z#Tz9T-(-7');
define('SECURE_AUTH_KEY',  '74)0O89z#Z>:FdV(qX=tL+S7DrZL%7kp5).sV[kP2Z+Q.)NJgM%<7{|u]&wAkVF ');
define('LOGGED_IN_KEY',    'Pa7*@`Xo&LI;-|%CPjO{+<m]@G|(;hOn}F9;%M0*e)&,OoKb-xmB3GCp(10g1^Eb');
define('NONCE_KEY',        '=Offr|aWHAm&4X47L*?f1[O>V4k4-@nBz.l9.5;5],tst]`#l3xd7&rgbAn8y[-U');
define('AUTH_SALT',        'g@v2i$FWbDU-BSY[oWFx<Hj;?7[TdcM85{z^n*:j+RH]]&.v-kZ(-+6+_.%B4-?f');
define('SECURE_AUTH_SALT', '>BG&QE!pI3ur5%/37Cytv]xU>TLo?*:Y/wYBpo/]Fu[el[INJD@. HR9^$Wb{pes');
define('LOGGED_IN_SALT',   'gzzF|g_e&S}p,cU0A)5K)}r*s)+x>(/=bof~JMDQL&N#J)q ij0_J7Fif}NAg,qa');
define('NONCE_SALT',       'cOrKxq;&A+wA$ZYh3:$^S9Q%h2D~;oqiJIZEYeRi ,0hge0TdT1NKI@GZhXeX9M~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
