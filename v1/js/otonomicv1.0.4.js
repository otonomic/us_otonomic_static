    var WEBROOT = '/';
    var full_path = 'http://builder.page2site.com/';
    var nextpageURL = 'http://www.page2site.com/shared/facebook_fanpages.php';
    var debug_enabled = 0;

    var connect_path = '/shared/';
    var reference_time = new Date().getTime();
    var user_facebook_id;
    var fanpage_permissions = 'manage_pages,email,offline_access';
    var personal_permissions = 'user_location,user_about_me,user_photos,user_events,user_videos';
    var num_reject_basic_permissions = 1;


    function get_facebook_id_from_url(identifier) {
        if($result = getNumberFromString(identifier)) {
            return $result;
        }

        $pattern = /facebook.com\/([a-zA-Z0-9\/._-]*)/i;

        if( $matches = identifier.match($pattern)) {
            $result = $matches[1];

        } else {
            $pattern = /(\/)?([a-zA-Z0-9\/._-]*)/;
            if( $matches = identifier.match($pattern)) {
                $result = $matches[0];
            }
        }

        return $result.replace('/,- ', '');
    }

    function getNumberFromString(string) {
        $pattern = /\/([0-9]+[^a-zA-Z?&])/;

        if( $matches = string.match($pattern)) {
            return $matches[1];
        }
        return '';
    }



    function currentUrl() {
        return current_url = location.protocol + '//' + location.host + location.pathname;
    }
        

    jQuery(document).ready(function(){
        jQuery('#btn_go').tipsy({
            gravity:  'se',//jQuery.fn.tipsy.autoNS,
            trigger: 'manual'
        });
    });

    jQuery(document).ready(function($) {
        // Track time since the page is loaded in the P2S database
        var time_counter = 0;
        var next_record_time = 1;
        //p2sTrack('', 'Page Loaded', '0 seconds');

        /*
        var page_loaded_interval_func = setInterval(function() {
            time_counter++;
            if(time_counter >= next_record_time) {
                next_record_time *= 2;
                p2sTrack('LandingPage', 'Page Loaded', ''+time_counter+' seconds');
            }
        }, 1000);
        */
    });

    jQuery(document).ready(function($) {
        // trackFBConnect('Search Page', 'Document Ready');

        $("#btn_video").click(function(e) {
            e.preventDefault();

            $.fancybox({
                'padding'       : 0,
                'autoScale'     : false,
                'transitionIn'  : 'none',
                'transitionOut' : 'none',
                'title'         : this.title,
                'width'         : 680,
                'height'        : 495,
                'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type'          : 'swf',
                'swf'           : {
                    'wmode'        : 'transparent',
                    'allowfullscreen'   : 'true'
                }
            });

            //return false;
        });

        /* Disabled for marketing Website*/
        //$('#main_search_box').focus();

        window.scrollTo(0,0);
    });

    jQuery(document).ready(function($){
        jQuery(document).on('click', ".tracking_enabled", function (e) {
            var element = jQuery(this);
            var category = (typeof(element.attr("data-ga-category")) != "undefined") ? element.attr("data-ga-category") : '';
            var event = (typeof(element.attr("data-ga-event")) != "undefined") ? element.attr("data-ga-event") : 'Click';
            var label = (typeof(element.attr("data-ga-label")) != "undefined") ? element.attr("data-ga-label") : '';
            var value = (typeof(element.attr("data-ga-value")) != "undefined") ? element.attr("data-ga-value") : null;

            trackEvent(category, event, label, value);
            p2sTrack(category, event, label, value);        //by default make ajax call

            var href = $(this).attr("href");
            var target = $(this).attr("target");
            e.preventDefault(); // don't open the link yet
            setTimeout(function() { // now wait 300 milliseconds...
                window.open(href,(!target?"_self":target)); // ...and open the link as usual
            },300);
        });

        jQuery(document).on('click', '.track_event', function (e) {
            //e.preventDefault();
            var element = jQuery(this);
            var category = (typeof(element.attr("data-ga-category")) != "undefined") ? element.attr("data-ga-category") : '';
            var event = (typeof(element.attr("data-ga-event")) != "undefined") ? element.attr("data-ga-event") : '';
            var label = (typeof(element.attr("data-ga-label")) != "undefined") ? element.attr("data-ga-label") : '';
            var value = (typeof(element.attr("data-ga-value")) != "undefined") ? element.attr("data-ga-value") : null;
            var ajax_track = element.attr("data-ajax-track") || 1;

            if (value == null && element.hasClass("measure_time")) {
                value = new Date().getTime() - reference_time;
            }

            trackEvent(category, event, label, value);
            if (ajax_track == 1) {
                p2sTrack(category, event, label, value);
            }

            /*var href = $(this).attr("href");
             var target = $(this).attr("target");
             e.preventDefault(); // don't open the link yet
             setTimeout(function() { // now wait 300 milliseconds...
             window.open(href,(!target?"_self":target)); // ...and open the link as usual
             },300);*/
        });

        $(".track_hover").hoverIntent(function(e){
            var element = jQuery(this);
            var category = (typeof(element.attr("data-ga-category")) != "undefined") ? element.attr("data-ga-category") : '';
            var event = (typeof(element.attr("data-ga-event-hover")) != "undefined") ? element.attr("data-ga-event-hover") : '';
            var label = (typeof(element.attr("data-ga-label")) != "undefined") ? element.attr("data-ga-label") : '';
            var value = (typeof(element.attr("data-ga-value")) != "undefined") ? element.attr("data-ga-value") : null;
            var ajax_track = element.attr("data-ajax-track") || 1;

            if (value == null && element.hasClass("measure_time")) {
                value = new Date().getTime() - reference_time;
            }

            trackEvent(category, event, label, value);
            if (ajax_track == 1) {
                p2sTrack(category, event, label, value);
            }
        }, function(e){
            return false;
        });

    });
    
    function trackEvent(category, action, label, value, non_interaction) {
        if(typeof(jQuery)=='undefined') { return; }

        if (typeof(value) == 'undefined') {
            value = null;
        }
        if (typeof(non_interaction) == 'undefined') {
            non_interaction = false;
        }
        category = jQuery.camelCase(category);
        action = jQuery.camelCase(action);
        value = Number(value);

        /* Track in GA */
        if (typeof(ga) != 'undefined') {
            ga('send', {
                'hitType': 'event',
                'eventCategory': category,
                'eventAction': action,
                'eventLabel': label,
                'eventValue': value
            });

            /*
            _gaq.push(['_trackEvent', category, action, label, value, non_interaction])
             */

        } else {
            p2sTrack('Warning', 'Missing GA tags', category + ' >>> ' + action + ' >>> ' + label + ' >>> ' + value);
            console.log('Warning! Missing GA tags');
        }

        /* Track in Kissmetrics */
        if (typeof(_kmq) != 'undefined') {
            _kmq.push(['record', category + '.' + event, {'label': label, 'value': value}]);
        }
    }

    function trackSocial(category, event, label) {
        if (typeof(_gaq) != 'undefined') {
            category = jQuery.camelCase(category);
            event = jQuery.camelCase(event);
            _gaq.push(['_trackSocial', category, event, label])
        }
    }

    function p2sTrack(category, event, label, value) {
        var data = {category: category, event: event, label: label, value: value};
        makeAjaxTrackCall(data);

        if (typeof label == "undefined") {
            value = null;
        }

        if (typeof value == "undefined") {
            value = null;
        }


        if (typeof(_paq) != 'undefined') {
            _paq.push(['trackEvent', category, event, label, value ]);
        }
    }

    function trackFBConnect(category, event, label, value) {
        if (typeof value == "undefined") {
            value = null;
        }
        p2sTrack(category, event, label, value);
        trackEvent(category, event, label, value);
    }

    function makeAjaxTrackCall(data, callback) {
        console.log("Tracking to Otonomic DB is disabled.")
        return;

/*
        var url = WEBROOT + 'code/sites/track_click/';
        if (typeof site_id != "undefined" && site_id) {
            url += site_id + '/';
        };

        jQuery.ajax({
            url: url,
            data: data,
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                if (callback != undefined) {
                    callback(data);
                }
            }
        });
        */
    }

    function objectToArray(obj) {
        var arr = [];
        if (typeof obj != "object") {
            return obj;
        }
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(key);
            }
        }

        return arr;
    }

    function trackPageView(url) {
        if (typeof(ga) != 'undefined') {
            // _gaq.push(['_trackPageview', '/virtual_pageviews/' + url]);

            ga('send', 'pageview', '/virtual_pageviews/' + url);

            console.log('Tracked page view: '+url);
        }
    }
    
    /*Author Id : SGWDP0043
	Date : 7/7/2015
	Desc : Whenever a user gets to otonomic.com for the first time, save a cookie locally with his referral info , assuming
	the url params would be utm_source, utm_medium, utm_term, utm_content, utm_campaign , setting there values with for cookie*/

	//utm_source, utm_medium, utm_term, utm_content, utm_campaign
	var cookieName = [
		"utm_source",
		"utm_medium",
		"utm_term",
		"utm_content",
		"utm_campaign",
	];
	//the name of the url paramater you are expecting that holds the code you wish to capture
	//for example, http://www.test.com?couponCode=BIGDISCOUNT your URL Parameter would be couponCode and the
	//cookie value that will be stored is BIGDISCOUNT
	var URLParameterName = [
		"utm_source",
		"utm_medium",
		"utm_term",
		"utm_content",
		"utm_campaign",
	];
	//how many days you want the cookie to be valid for on the users machine
	var cookiePersistDays = 7 ,
	cookiePath = '',    // use '' for default
	cookieDomain = "."+window.location.hostname, // leading . will allow subdomains to also access cookie
	cookieSecure = false;

	/************************ -------------------------- *******************************/
	//Cookie Functions
	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		}
		var cookie_string = name + "=" + value + expires;
		//document.cookie =  + "; path=/";
		cookie_string += "; path=" + (cookiePath.length ? cookiePath : '/');
		cookie_string += "; domain=" + (cookieDomain.length ? cookieDomain : window.location.hostname);
		if ( cookieSecure ) {
			cookie_string += "; secure";
		}
		document.cookie = cookie_string;
	}
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') { c = c.substring(1, c.length); }
			if (c.indexOf(nameEQ) === 0) { return c.substring(nameEQ.length, c.length);}
		}
		return null;
	}
	function eraseCookie(name) {
		createCookie(name, "", -1);
	}
	/************************ -------------------------- *******************************/
	// HTML Encode Functions
	function encode(me) {
		return me.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	}
	function decode(me) {
		return me.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>');
	}
	/************************ -------------------------- *******************************/
	//helper function to trim whitespace
	function trim(str) {
		if (str === null) { return ""; }
		var newstr;
		newstr = str.replace(/^\s*/, "").replace(/\s*$/, "");
		newstr = newstr.replace(/\s{2,}/, " ");
		return newstr;
	}
	//Retrieve a query string parameter
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results === null) {
			return "";
		} else {
			return decodeURIComponent(results[1].replace(/\+/g, " "));
		}
	}

	/************************ -------------------------- *******************************/
	//Extract the code from the URL based on the defined parameter name
	function CaptureCode() {
		for(var i=0 ; i< URLParameterName.length ; i++ )
		{
			 var q = getParameterByName(URLParameterName[i]);
			if (q !== null && q !== "") {
				eraseCookie(cookieName[i]);
				createCookie(cookieName[i], q, cookiePersistDays);
			}
		}
	}
	//Save a code from an action, not a URL, using the defined variables
	function SaveCode(code) {
		if (code !== null && code !== "") {
			eraseCookie(cookieName);
			createCookie(cookieName, code, cookiePersistDays);
		}
	}
	//Save a code overriding the default variables
	function SaveCodeManually(_cookieName, _code, _persistDays) {
		if (_cookieName !== null && _cookieName !== "" && _code !== null && _code !== "") {
			eraseCookie(_cookieName);
			createCookie(_cookieName, _code, _persistDays);
		}
	}
	//This will return the stored cookie value
	function GetCode() {
		return readCookie(cookieName);
	}
	//This will return the stored cookie value from the specified cookie name
	function GetCodeByName(_cookieName) {
		return readCookie(_cookieName);
	}
	/************************ -------------------------- *******************************/
	CaptureCode();

/*End script for setting cookies by Author Id : SGWDP0043*/
