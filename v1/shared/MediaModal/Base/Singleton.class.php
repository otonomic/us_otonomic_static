<?php
namespace OTO\Modules\MediaModal\Base;
class Singleton
{
	protected static $instances;

	protected function __construct() { }

	public static function get_instance($param=false)
	{
		$class = get_called_class();
		if (!isset(self::$instances[$class])) {
			self::$instances[$class] = new $class($param);
		}
		return self::$instances[$class];
	}

}