<?php
namespace OTO\Modules\MediaModal\Base;
abstract class Tab extends \OTO\Modules\MediaModal\Base\Singleton {
    abstract public function getTitle();

    public function getControls() {
        return '';
    }

    public function getTabObjects( $page, $count, $extra ) {
        return false;
    }

    public function getTabHTML( $page, $count, $extra ){
        return false;
    }

}