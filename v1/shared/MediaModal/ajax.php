<?php
session_start();
require_once('Init.class.php');
$config = json_decode(base64_decode($_REQUEST['config']), true);
$MediaModal = new \OTO\Modules\MediaModal\Init($config);
$MediaModal->init();

$ajax =  \OTO\Modules\MediaModal\Ajax::get_instance();
$ajax->handleRequest();