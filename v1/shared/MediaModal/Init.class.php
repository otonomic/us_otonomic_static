<?php
namespace OTO\Modules\MediaModal;
require_once('Config.class.php');
class Init {
    var $config;
    function __construct($config) {
        $this->config = $config;
    }

    function init() {

        \OTO\Modules\MediaModal\Config::get_instance($this->config);

        //add_action('wp_enqueue_scripts', array($this, 'enqueueScript'));
        //add_action('admin_enqueue_scripts', array($this, 'enqueueScript'));

        //add_action('wp_enqueue_scripts', array($this, 'enqueueStyles'));
        //add_action('admin_enqueue_scripts', array($this, 'enqueueStyles'));

        //add_filter('otonomic_media_modal_tabs', array($this, 'addTabs'));

        $this->registerAjaxActions();
    }

    function addTabs($tabs) {
        //$facebookObj = \OTO\Modules\MediaModal\Tabs\Facebook::get_instance();
        //$tabs['facebook'] = $facebookObj;
        //return $tabs;
    }

    function registerAjaxActions() {

        $otoGallery = \OTO\Modules\MediaModal\Gallery::get_instance();
        $otoGalleryAjax = \OTO\Modules\MediaModal\Ajax::get_instance();

        $otoGalleryAjax->addAction('otonomic_media_modal', array($otoGallery, 'renderModal'));
        $otoGalleryAjax->addAction('otonomic_media_modal_content', array($otoGallery, 'renderTab'));
        $otoGalleryAjax->addAction('otonomic_media_modal_content_page', array($otoGallery, 'getTabPage'));

    }

    function outputStyles() {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        ?>
        <link rel="stylesheet" href="<?php echo $otoGalleryConfig->module_url; ?>/assets/js/colorbox/theme2/colorbox.css">
        <?php
    }

    function outputScript() {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        ?>
        <script type="text/javascript" src="<?php echo $otoGalleryConfig->module_url.'/assets/js/colorbox/jquery.colorbox-min.js' ?>?ver=1.5.14"></script>
        <script type="text/javascript" src="<?php echo $otoGalleryConfig->module_url.'/assets/js/oto-media-modal.js' ?>?ver=0.0.1"></script>
        <script>
            window.otoMediaModal = {};
            window.otoMediaModal.modal_url = "<?php echo $otoGalleryConfig->ajax_url; ?>?action=otonomic_media_modal&config=<?php echo base64_encode(json_encode($this->config)); ?>";
        </script>
        <?php
    }

}