jQuery(document).ready(function ($){
    $('#otonomic-media-modal .media-link').bind('click', function (e){
        $('#otonomic-media-modal .media-link').parent().removeClass("active");
        $(this).parent().addClass("active");
        var tab = $(this).attr('data-type');
        $('#gallery-tab-content').html('');
        $('#loader').show();
        var the_data = {
            action: 'otonomic_media_modal_content',
            tab: tab
        };
        $.post(oto_media_ajax, the_data).done(function(response) {
            $('#gallery-tab-content').html(response);
            $('#loader').hide();
        });
        //alert(oto_media_ajax);
    });
    $('#otonomic-media-select-images').bind('click', function (e){
        var selected_images = [];
        jQuery('.media-wrapper.selected').each( function (index){
            selected_images.push( jQuery(this).attr('data-image') );
        });
        parent.oto.gallery.send.attachment(selected_images);
        parent.jQuery.fn.colorbox.close();
    });
    $('#otonomic-media-cancel-modal').bind('click', function (e){
        parent.jQuery.fn.colorbox.close();
    });
    $(document).on('click', '#param-go', function (e){
        var params = {};
        $('#gallery-tab-content #tab-body .row').html('');
        $('#loader').show();
        jQuery('.tab-param').each( function (index){
            params[jQuery(this).attr('name')] = jQuery(this).val();
        });
        jQuery('#tab-load-more').attr('data-tab-extra', JSON.stringify(params));
        /* Now load tab data */
        var page = 1;
        var tab = jQuery('#tab-load-more').attr('data-tab-id');

        var the_data = {
            action: 'otonomic_media_modal_content_page',
            extra: JSON.stringify(params),
            tab: tab,
            page: page
        };
        $.post(oto_media_ajax, the_data).done(function(response) {
            if(response.length<=0)
                jQuery('#tab-load-more').hide();
            else
                jQuery('#tab-load-more').show();
            
            $('#gallery-tab-content #tab-body .row').html(response);
            $('#loader').hide();
        });
    });
    $(document).on('click', '#tab-load-more', function (e){
        var page = parseInt(jQuery(this).attr('data-page-no'));
        var extra = jQuery(this).attr('data-tab-extra');
        var tab = jQuery(this).attr('data-tab-id');
        var the_data = {
            action: 'otonomic_media_modal_content_page',
            extra: extra,
            tab: tab,
            page: page
        };
        $.post(oto_media_ajax, the_data).done(function(response) {
            if(response.length<=0)
                jQuery('#tab-load-more').hide();
            else
                jQuery('#tab-load-more').show();

            $('#gallery-tab-content #tab-body .row').append(response);
        });
        page++;
        jQuery(this).attr('data-page-no', page);
    });
    
    $(document).on('keypress','input.tab-param.form-control',function(e){
        if(e.keyCode == 13) {
              $('#param-go').click();
        }
    });
    
});