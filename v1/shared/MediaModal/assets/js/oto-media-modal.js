window.oto = {};
window.oto.gallery = {};
window.oto.gallery.send = {};
window.oto.gallery.send.attachment = {};

(function($) {

    $.otoImageGallery = function(element, options) {

        var defaults = {
            multiple: false,
            title: 'Add Image',
            button_text: 'Add Image',
            pre_selected_images: false,
            on_select: function() {}
        };

        var plugin = this;

        plugin.settings = {};

        plugin._gallery = false;

        var $element = $(element),
            element = element;

        plugin.init = function() {
            plugin.settings = $.extend({}, defaults, options);
            plugin.settings.pre_selected_images = String(plugin.settings.pre_selected_images);
            plugin.bindClick();
        };

        plugin.bindClick = function() {
            $(element).bind('click touchstart', function (e){
                e.preventDefault();
                plugin.openGallery();
            });
        };

        plugin.openGallery = function() {
            window.oto.gallery.send.attachment = plugin.on_select;
            width = "80%";
            height = "80%";
            if (window.matchMedia) {
                width700Check = window.matchMedia("(max-width: 700px)");
                if (width700Check.matches){
                    width = "100%";
                    height = "100%";
                }
            }
            /* now open the colorbox iframe window */
            $.colorbox({
                href:otoMediaModal.modal_url+'&title='+plugin.settings.title+'&button_text='+plugin.settings.button_text+'&multiple='+plugin.settings.multiple,
                width: width,
                height: height,
                iframe: true,
                overlayClose: false,
                closeButton: false
            });

        };
        plugin.on_select = function(attachment) {
            plugin.settings.on_select($element, attachment);

        };
        plugin.init();
    };

    $.fn.otoImageGallery = function(options) {

        return this.each(function() {
            if (undefined == $(this).data('otoImageGallery')) {
                var data_options = $(this).data();
                var merged_options = $.extend(options, data_options);
                var plugin = new $.otoImageGallery(this, merged_options);
                $(this).data('otoImageGallery', plugin);
            }
        });
    }
})(jQuery);

jQuery(document).ready(function($){

    $('.oto-image-gallery').otoImageGallery({
        multiple:false,
        on_select: function ($element, $attachment){
            console.log($element);
            console.log($attachment);
        }
    });
});