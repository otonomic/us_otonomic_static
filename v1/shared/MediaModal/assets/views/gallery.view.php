<?php
$otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
$tab = @$_GET['tab'];
if(empty($tab)) {
    $tab = 'camera';
}
$config = json_decode(base64_decode($_REQUEST['config']), true);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            Media Modal
        </title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo $otoGalleryConfig->module_url.'/assets/css/media-modal.css'; ?>" type="text/css" media="all">

        <script>
            window.oto_media_ajax = "<?php echo $otoGalleryConfig->ajax_url; ?>?config=<?php echo base64_encode(json_encode($config)); ?>";
        </script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?php echo $otoGalleryConfig->module_url; ?>/assets/js/fileupload/js/vendor/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="<?php echo $otoGalleryConfig->module_url.'/assets/js/jquery-migrate.min.js' ?>?ver=1.2.1"></script>
        <!-- The Templates plugin is included to render the upload/download listings -->
        <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- blueimp Gallery script -->
        <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

        <script type="text/javascript" src="<?php echo $otoGalleryConfig->module_url.'/assets/js/media-modal.js'; ?>?ver=4.1.1"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div id="otonomic-media-modal">
                        <div id="gallery-title">
                            <h2><?php echo $_GET['title']; ?></h2>
                        </div>
                        <div id="gallery-tabs">
                            <?php echo $tabs; ?>
                        </div>
                        <div id="gallery-tab-content">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <footer id="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <button id="otonomic-media-cancel-modal" class="btn btn-default">
                                <span class="glyphicon glyphicon-remove"></span> Cancel
                            </button>

                            <button id="otonomic-media-select-images" class="btn btn-success">
                                <span class="glyphicon glyphicon-ok"></span> <?php echo $_GET['button_text']; ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div id="loader"></div>
        <script>
            jQuery(document).ready(function ($){
                $('#tab-<?php echo $tab ?>').trigger('click');
            });
        </script>
    </body>
</html>
