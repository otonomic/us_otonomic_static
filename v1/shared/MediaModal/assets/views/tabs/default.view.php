<div id="tab-header">
    <h3><?php echo $title; ?></h3>
</div>
<div id="tab-controls">
    <div class="row">
        <?php echo $tabObject->getControls(); ?>
    </div>
</div>
<div id="tab-body">
    <div class="row">
        <?php include('partial/default/grid.view.php'); ?>
    </div>
</div>
<div id="tab-footer" class="text-center">
    <a href="javascript:void(0);" id="tab-load-more" data-page-no="2" data-tab-extra="<?php echo $extra_load_more ?>" data-tab-id="<?php echo $tab; ?>" class="btn btn-default">Load More</a>
</div>
<script>
    jQuery(document).ready(function ($){
        $('#tab-body').on('mousedown', '.media-wrapper', function(e){

            if(!e.ctrlKey) {
                jQuery('.media-wrapper').removeClass('selected');
            }
            if($(this).hasClass('selected'))
                jQuery(this).removeClass('selected');
            else
                jQuery(this).addClass('selected');

        });
    });
</script>