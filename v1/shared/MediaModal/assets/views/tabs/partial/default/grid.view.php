<?php if(count($images)): ?>
    <?php foreach($images as $image): ?>
        <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
            <div class="media-wrapper" data-image="<?php echo $image['image']; ?>">
                <div class="image-wrapper">
                    <div class="centered">
                        <img src="<?php echo $image['thumb']; ?>" class="img-responsive" />
                    </div>
                </div>
                <div class="image-title"><?php echo $image['title']; ?></div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>