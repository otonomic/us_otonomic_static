<?php
namespace OTO\Modules\MediaModal;
require_once('Base/Singleton.class.php');
class Config extends \OTO\Modules\MediaModal\Base\Singleton {
    var $ds;
    var $module_folder;
    var $module_url;
    var $module_path;
    var $ajax_url;
    var $fb_page_id;
    var $fb_user_token;

    var $gallery_tabs;
    var $gallery_api_url;

    function __construct($config) {

        $this->ds = '/';

        $this->setModulePath();
        $this->setModuleFolder();
        $this->setModuleURL();

        $this->ajax_url = $this->module_url.'/ajax.php';

        $this->includeFiles();

        $uploadObj = \OTO\Modules\MediaModal\Tabs\Upload::get_instance();
        $cameraObj = \OTO\Modules\MediaModal\Tabs\Camera::get_instance();
        //$galleryObj = \OTO\Modules\MediaModal\Tabs\Gallery::get_instance();
        //$facebookObj = \OTO\Modules\MediaModal\Tabs\Facebook::get_instance();
        $flickrObj = \OTO\Modules\MediaModal\Tabs\Flickr::get_instance();
        $instagramObj = \OTO\Modules\MediaModal\Tabs\Instagram::get_instance();
        $backgroundPatternsObj = \OTO\Modules\MediaModal\Tabs\BackgroundPatterns::get_instance();
        $facebookUserObj = \OTO\Modules\MediaModal\Tabs\FacebookUser::get_instance();

        $this->gallery_api_url = isset($config['gallery_api_url'])?$config['gallery_api_url']:'';
        $this->fb_page_id = isset($config['fb_page_id'])?$config['fb_page_id']:'';
        $this->fb_token = isset($config['fb_token'])?$config['fb_token']:'';
        $this->fb_user_token = isset($config['fb_user_token'])?$config['fb_user_token']:'';

        $this->gallery_tabs = array(
			'camera' => $cameraObj,
            'upload' => $uploadObj,
            /*'gallery' => $galleryObj,
            'facebook' => $facebookObj,*/
            'flickr' => $flickrObj,
            'instagram' => $instagramObj,
            'backgroundpatterns' => $backgroundPatternsObj,
            'facebookuser' => $facebookUserObj
        );


    }

    private function setModulePath() {
        $path = pathinfo(__FILE__);
        $root_path = $path['dirname'] . $this->ds ;
        $root_path = str_replace('\\','/',$root_path);
        $root_path = str_replace('//','/',$root_path);
        $this->module_path =  $root_path;
    }

    private function setModuleFolder() {
        $tbase = $this->ds . substr( $this->module_path,strlen($_SERVER['DOCUMENT_ROOT']) );
        $tbase .= $this->ds;
/*
        if(strlen($tbase) == 0)
        {
            $tbase = DS;
        }
*/
        $tbase = str_replace('\\','/',$tbase);
        $tbase = str_replace('//','/',$tbase);
        $this->module_folder = $tbase;
    }

    private function setModuleURL() {
        $this->module_url = "http://".$_SERVER['HTTP_HOST'].$this->module_folder;
    }



    private function includeFiles() {

        $required_files = array(
            'Base/Tab.class.php',
            'Gallery.class.php',
            'Ajax.class.php',
            'Tabs/Upload.class.php',
            'Tabs/Camera.class.php',
            'Tabs/Gallery.class.php',
            'Tabs/Facebook.class.php',
            'Tabs/Flickr.class.php',
            'Tabs/Instagram.class.php',
            'Tabs/BackgroundPatterns.class.php',
            'Tabs/FacebookUser.class.php'
        );
        foreach($required_files as $file) {
            require_once($file);
        }

    }
}
