<?php
namespace OTO\Modules\MediaModal;
class Gallery extends \OTO\Modules\MediaModal\Base\Singleton {

    private $tabs;
    private $record_per_page;

    function __construct() {
        $this->record_per_page = 18;
        $this->initTabs();
    }

    function initTabs() {

        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();

        $this->tabs = $otoGalleryConfig->gallery_tabs;
        //$filtered_tabs = apply_filters('otonomic_media_modal_tabs', $this->tabs);

        //if(is_array($filtered_tabs) && count($filtered_tabs)>0)
        //    $this->tabs = $filtered_tabs;
    }

    function getTabPage() {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        $extra = (array)json_decode(@$_REQUEST['extra']);
        $tab = $_REQUEST['tab'];
        $page = ($_REQUEST['page'])?$_REQUEST['page']:1;
        $count = $this->record_per_page;
        $extra = ($extra)?$extra:array();
        $tabObject = $this->tabs[$tab];

        $images = $tabObject->getTabObjects($page, $count, $extra);
        $extra_load_more = json_encode($extra);
        ob_start();
        require_once($otoGalleryConfig->module_path.'/assets/views/tabs/partial/default/grid.view.php');
        $html = ob_get_clean();

        echo $html;
        exit;
    }

    function renderTab() {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        $extra = (array)json_decode(@$_REQUEST['extra']);
        $tab = $_REQUEST['tab'];
        $page = (@$_REQUEST['page'])?$_REQUEST['page']:1;
        $count = $this->record_per_page;
        $extra = ($extra)?$extra:array();

        $tabObject = $this->tabs[$tab];
        $html = $tabObject->getTabHTML($page, $count, $extra);
        $title = $tabObject->getTitle();
        $extra_load_more = json_encode($extra);
        if(!$html) {
            /* Try getting image array */
            $images = $tabObject->getTabObjects($page, $count, $extra);
            ob_start();
            require_once($otoGalleryConfig->module_path.'/assets/views/tabs/default.view.php');
            $html = ob_get_clean();
        }
        echo $html;
        exit;
    }

    function renderModal() {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        /* Lets provide options to render the tabs here on this page */
        $tabs = [];
        foreach($this->tabs as $key=>$tab) {
            if(!is_a($tab, '\OTO\Modules\MediaModal\Base\Tab'))
                continue;
            $tabs[] = '<li><a href="javascript:void(0);" class="media-link" id="tab-'.$key.'" data-type="'.$key.'">'.$tab->getTitle().'</a></li>';
        }
        $tabs_list = implode('', $tabs);
        $tabs = '<ul class="hidden-xs nav nav-tabs" role="tablist">'.$tabs_list.'</ul>';
        $tabs .= '<ul class="hidden-sm hidden-md hidden-lg nav nav-tabs" role="tablist"><li role="presentation" class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
      Select <span class="caret"></span>
    </a><ul class="dropdown-menu" role="menu">'.$tabs_list.'</ul></li></ul>';
        require_once($otoGalleryConfig->module_path.'/assets/views/gallery.view.php');
        exit;
    }

    function getTabsToRender() {

    }

}