<?php
namespace OTO\Modules\MediaModal;
class Ajax extends \OTO\Modules\MediaModal\Base\Singleton {
    var $actions;
    function __construct() {
        $this->actions = array();
    }
    function addAction($action, $callable) {

        if(is_callable($callable)) {
            $this->actions[$action] = $callable;
        }

    }
    function handleRequest() {
        if(isset($_REQUEST['action'])) {
            $action = $_REQUEST['action'];
            if(isset($this->actions[$action])) {
                call_user_func($this->actions[$action]);
            }
        }
    }
}