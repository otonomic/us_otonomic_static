<?php
namespace OTO\Modules\MediaModal\Tabs;
class FacebookUser extends \OTO\Modules\MediaModal\Base\Tab {
    public function getTitle() {
        return "<i class='fa fa-facebook-official'></i> Facebook Users";
    }
    
    public function getControls() {
        $output = <<<E
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" placeholder="Search" name="search" class="tab-param form-control" />
                </div>
                <div class="form-group">
                    <input type="button" id="param-go" value="Search" class="btn btn-primary" />
                </div>
            </div>
        </div>
E;
        return $output;
    }
    
    public function getTabObjects( $page, $count, $extra ) {
        $images = array();
        $total_images = $page * $count;
        if(isset($extra['search']) && !empty($extra['search'])){
            $data = $this->getData($extra['search']);
            $batch = $this->ProcessBatch($data);
            $images = array_merge($images,$batch);
            if(isset($data['paging']['next']) && $total_images > count($images)){
                $next = $data['paging']['next'];
                while($total_images > count($images)){
                    $nextrequest = file_get_contents($next);
                    $nextBatch = json_decode($nextrequest,TRUE);
                    $nextBatchImages = $this->ProcessBatch($nextBatch);
                    $images = array_merge($images,$nextBatchImages);
                    if(isset($nextBatch['paging']['next'])){
                       $next = $nextBatch['paging']['next']; 
                    }else{
                        break;
                    }
                }
            }
        }
        $pagination = array_slice($images, ($page-1)*$count, $count);
        return $pagination;
    }
    
    public function ProcessBatch($data){
        $images = array();
        foreach($data['data'] as $user){
            $images[]= array(
                'title' => $user['name'],
                'image' => 'http://graph.facebook.com/'.$user['id'].'/picture?type=large',
                'thumb' => 'http://graph.facebook.com/'.$user['id'].'/picture?type=normal',
            );
        }
        return $images;
    }
    
    public function getData($tag){
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        $url = 'https://graph.facebook.com/v2.2/search?q='.urlencode($tag).'&type=user&access_token='.$otoGalleryConfig->fb_user_token;
        $data = file_get_contents($url);
        $fbData = json_decode($data,TRUE);
        return $fbData;
    }
}
