<?php
namespace OTO\Modules\MediaModal\Tabs;
class Flickr extends \OTO\Modules\MediaModal\Base\Tab {

    private $apikey = 'b5245e9b9cbaeecee26ea278bfa20253';


    public function getTitle() {
        return "<i class='fa fa-flickr'></i> Flickr";
    }
    
    public function getControls() {
        $output = <<<E
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" placeholder="Search" name="search" class="tab-param form-control" />
                </div>
                <div class="form-group">
                    <input type="button" id="param-go" value="Search" class="btn btn-primary" />
                </div>
            </div>
        </div>
E;
        return $output;
    }
    
    public function getTabObjects( $page, $count, $extra ) {
        $images = array();
        if(isset($extra['search']) && !empty($extra['search'])){
            $data = $this->getData($page, $count, $extra['search']);
            foreach($data['photos']['photo'] as $photo){
                $url = 'https://farm'.$photo['farm'].'.staticflickr.com/'.$photo['server'].'/'.$photo['id'].'_'.$photo['secret'];
                $images[]= array(
                    'title' => '',
                    'image' => $url.'_b.jpg',
                    'thumb' => $url.'_m.jpg',
                );
            }
        }
        return $images;
    }
    
    public function getData($page, $count, $term){
        $base_url = "https://api.flickr.com/services/rest/?";
        $params = array(
            'api_key'	=> $this->apikey,
            'tags' => $term,
            'page' => $page,
            'per_page' => $count,
            'method'	=> 'flickr.photos.search',
            'format'	=> 'json',
            'nojsoncallback' => 1
        );
        $url = $base_url.http_build_query($params);
        $request = file_get_contents($url);
        return json_decode($request,TRUE);
    }
}
?>
