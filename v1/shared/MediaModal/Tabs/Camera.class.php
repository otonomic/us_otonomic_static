<?php
namespace OTO\Modules\MediaModal\Tabs;
class Camera extends \OTO\Modules\MediaModal\Base\Tab {

    public function getTitle() {
        return "<i class='fa fa-camera'></i> Camera";
    }

    public function getTabHtml( $page, $count, $extra ) {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        ob_start();
        require_once($otoGalleryConfig->module_path.'/assets/views/tabs/camera.view.php');
        $output = ob_get_clean();
        return $output;
    }
}