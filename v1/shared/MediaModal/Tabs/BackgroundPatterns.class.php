<?php
namespace OTO\Modules\MediaModal\Tabs;
class BackgroundPatterns extends \OTO\Modules\MediaModal\Base\Tab{
    public function getTitle() {
        return "<i class='fa fa-star'></i> Background Patterns";
    }
    
    public function getTabObjects( $page, $count, $extra ) {
        $data = $this->getData();
        $images = $this->processBatch($data);
        $pagination = array_slice($images, ($page-1)*$count, $count);
        return $pagination;
    }
    
    public function getData(){
        $url = 'http://www.colourlovers.com/api/patterns/top?format=json';
        $request = file_get_contents($url);
        $data = json_decode($request,TRUE);
        return $data;
    }
    
    public function processBatch($data){
        $images = array();
        foreach($data as $pattern){
            $images[]= array(
                'title' => $pattern['title'],
                'image' => $pattern['imageUrl'],
                'thumb' => $pattern['imageUrl'],
            );
        }
        return $images;
    }
}
?>
