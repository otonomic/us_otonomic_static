<?php
namespace OTO\Modules\MediaModal\Tabs;
class Instagram extends \OTO\Modules\MediaModal\Base\Tab {
    
    private $apikey = '44171713.10d405b.25e232f920f94ac9907d8c0ea34ce1de';
    
    public function getTitle() {
        return "<i class='fa fa-instagram'></i> Instagram";
    }
    
    public function getControls() {
        $output = <<<E
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" placeholder="Search" name="search" class="tab-param form-control" />
                </div>
                <div class="form-group">
                    <input type="button" id="param-go" value="Search" class="btn btn-primary" />
                </div>
            </div>
        </div>
E;
        return $output;
    }
    
    public function getTabObjects( $page, $count, $extra ) {
        $images = array();
        $total_images = $page*$count;
        if(isset($extra['search']) && !empty($extra['search'])){
            $data = $this->getData($extra['search']);
            $batch = $this->processBatch($data);
            $images = array_merge($images,$batch);
            if($total_images > count($images)){
                $nexturl = $data['pagination']['next_url'];
                while($nexturl){
                    $nextRequest = file_get_contents($nexturl);
                    $nextBatch = json_decode($nextRequest,TRUE);
                    if(isset($nextBatch['pagination']['next_url'])){
                        $nexturl = $nextBatch['pagination']['next_url'];
                    }else{
                        $nexturl = FALSE;
                    }
                    $batch = $this->processBatch($nextBatch);
                    $images = array_merge($images,$batch);
                    if($total_images < count($images)){
                        break;
                    }
                }
            }
        }
        $pagination = array_slice($images, ($page-1)*$count, $count);
        return $pagination;
    }
    
    public function processBatch($data){
        foreach($data['data'] as $photo){
            $title = '';
            if(isset($photo['caption']['text'])){
                $title = substr($photo['caption']['text'], 0, 50).'..';
            }
            $images[]= array(
                'title' => $title,
                'image' => $photo['images']['standard_resolution']['url'],
                'thumb' => $photo['images']['thumbnail']['url'],
            );
        }
        return $images;
    }
    
    public function getData($tag){
//        $url = "https://api.instagram.com/v1/tags/search?q=".$tag."&access_token=".$this->apikey;
        $url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?access_token='.$this->apikey.'&count=100';        
        $request = file_get_contents($url);
        $data = json_decode($request,TRUE);
        return $data;
    }
}
?>
