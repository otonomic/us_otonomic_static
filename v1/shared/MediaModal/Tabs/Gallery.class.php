<?php
namespace OTO\Modules\MediaModal\Tabs;
class Gallery extends \OTO\Modules\MediaModal\Base\Tab {

    var $config;
    public function getTitle() {
        return "<i class='fa fa-picture-o'></i> Gallery";
    }

    public function getTabObjects( $page, $count, $extra ) {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        $data = json_decode(file_get_contents($otoGalleryConfig->gallery_api_url."?page={$page}&count={$count}"), true);
        return $data['respond'];
    }
}