<?php
namespace OTO\Modules\MediaModal\Tabs;
class Facebook extends \OTO\Modules\MediaModal\Base\Tab {

    public function getTitle() {
        return "<i class='fa fa-facebook'></i> Facebook";
    }
    /*public function getControls() {
        $output = <<<E
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" placeholder="Search" name="search" class="tab-param form-control" />
                </div>
                <div class="form-group">
                    <input type="button" id="param-go" value="Search" class="btn btn-primary" />
                </div>
            </div>
        </div>
E;
        return $output;
    }*/

    public function getTabObjects( $page, $count, $extra ) {
        $fbData = $this->getFbImages();
        $total_images = $page * $count;
        $images = array();
        $break = false;
        foreach($fbData['data'] as $album){
            if(count($album['photos']['data']) > 0){
                $batch = $this->ProcessBatch($album['photos']);
                $images = array_merge($images,$batch);
                if(isset($album['photos']['paging']['next']) && $total_images > count($images)){
                    $next = $album['photos']['paging']['next'];
                    while($total_images > count($images)){
                        $request = file_get_contents($next);
                        $fbData = json_decode($request,TRUE);
                        $nextBatch = $this->ProcessBatch($fbData);
                        $images = array_merge($images,$nextBatch);
                        if(isset($fbData['paging']['next'])){
                            $next = $fbData['paging']['next'];
                        }else{
                            break;
                        }
                    }
                    if($total_images < count($images)){
                        $break = true;
                    }
                }else{
                    $break = true;
                }
            }
            if($break){
                break;
            }
        }
        $pagination = array_slice($images, ($page-1)*$count, $count);
        return $pagination;
    }
    
    public function ProcessBatch($data){
        $images = array();
        foreach($data['data'] as $photo){
            if(isset($photo['caption']) && $photo['caption'] != ''){
                $title = $photo['caption'];
            }else{
               $title = ''; 
            }
            $images[]= array(
                'title' => $title,
                'image' => 'http://graph.facebook.com/'.$photo['id'].'/picture?type=thumbnail',
                'thumb' => 'http://graph.facebook.com/'.$photo['id'].'/picture?type=normal',
            );
        }
        return $images;
    }
    
    public function getFbImages(){
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        $albums_url = "https://graph.facebook.com/v2.0/{$otoGalleryConfig->fb_page_id}/albums?access_token={$otoGalleryConfig->fb_token}&fields=id,name,link,cover_photo,type,description,count,photos.limit(100).fields(id,picture,images,source,caption,link,width,height,created_time,updated_time,name)";
        $data = file_get_contents($albums_url);
        $fbData = json_decode($data,TRUE);
        return $fbData;
    }
}