<?php
namespace OTO\Modules\MediaModal\Tabs;
class Upload extends \OTO\Modules\MediaModal\Base\Tab {

    public function getTitle() {
        return "<i class='fa fa-cloud-upload'></i> Upload";
    }

    public function getTabHtml( $page, $count, $extra ) {
        $otoGalleryConfig = \OTO\Modules\MediaModal\Config::get_instance();
        ob_start();
        require_once($otoGalleryConfig->module_path.'/assets/views/tabs/upload.view.php');
        $output = ob_get_clean();
        return $output;
    }
}