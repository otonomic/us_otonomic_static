(function($) {
    $.otoFBLogin = function(element, options) {

        var defaults = {
            template: {
                appRejected:{
                    view:'/app-rejected.html',
                    callback:false,
                    popup: {
                        load: true,
                        width: '400px',
                        height: false,
                        opacity: '0.9',
                        transition: 'elastic',
                        overlayClose: '',
                        fixed: '1',
                        closeButton: '1'
                    }
                },
                premissionRejected:{
                    view:'/premission-rejected.html',
                    callback:false,
                    popup: {
                        load: true,
                        width: '400px',
                        height: false,
                        opacity: '0.9',
                        transition: 'elastic',
                        overlayClose: '',
                        fixed: '1',
                        closeButton: '1'
                    }
                },
                selectPage:{
                    view:'/select-page.html',
                    callback:false,
                    popup: {
                        load: true,
                        width: '400px',
                        height: false,
                        opacity: '0.9',
                        transition: 'elastic',
                        overlayClose: '',
                        fixed: '1',
                        closeButton: '1'
                    }
                }
            },
            permissions: 'email,public_profile,pages_show_list',
            onProcessStart: false,
            onPageSelect: false
        };

        var plugin = this;
        plugin.settings = {};
        plugin.settings.callback = {};

        var $element = $(element),
            element = element;

        plugin.init = function() {
            plugin.settings = $.extend(true, {}, defaults, options);
            plugin.settings.callback = {};
            plugin.settings.active = {};
            plugin.settings.callback.selectpage = false;
            plugin.settings.active.popup = false;
            plugin.bindClick();
        };

        plugin.bindClick = function() {
            $(element).bind('click touchstart', function (e){
                e.preventDefault();
                $.colorbox.close();

                if(plugin.settings.onProcessStart)
                    plugin.settings.onProcessStart();

                FB.login(function(response){
                    plugin.fbStatusChangeCallback(response);
                }, {scope: plugin.settings.permissions});

            });
        };
        plugin.fbStatusChangeCallback = function(response){
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                plugin.checkFBPermissions()
            } else {
                plugin.unAuthorized();
            }
        };
        plugin.checkFBPermissions = function() {

            FB.api(
                "/me/permissions/",
                function (response) {
                    var permissions = true;
                    $(response.data).each(function(index,pem){
                        if(pem.status == 'declined') {
                            permissions = false;
                        }
                    });
                    if(permissions) {
                        plugin.fbGetUserDetails();
                    } else {
                        plugin.premissionRequired();
                    }
                }
            );
        };
        plugin.premissionRequired = function() {
            plugin.loadPopup('premissionRejected', plugin.bindLogin);
        };
        plugin.unAuthorized = function(){
            plugin.loadPopup('appRejected', plugin.bindLogin);
        };
        plugin.bindLogin = function(){
            $('#oto-fb-root .popup .fboto-login').otoFBLogin(plugin.settings);
        };
        plugin.fbGetUserDetails = function() {

            var access_token =   FB.getAuthResponse()['accessToken'];
            fields = [
                'id',
                'name',
                'email'
            ];
            plugin.settings.fb_user = {};
            plugin.settings.fb_pages = {};
            plugin.settings.callback.selectpage = false;
            FB.api('/me', {fields: fields}, function(response) {
                plugin.settings.fb_user = response;
                plugin.renderSelectPage();
            });
            FB.api('/me/accounts', { limit: 100, fields:'access_token,category,name,id,is_published,perms'}, function(response) {
                plugin.settings.fb_pages = response;
                plugin.renderSelectPage();
            });
            plugin.renderSelectPage();
        };
        plugin.renderSelectPage = function() {
            if(plugin.settings.callback.selectpage)
                return;
            if($.isEmptyObject(plugin.settings.fb_user) || $.isEmptyObject(plugin.settings.fb_pages))
                return;

            plugin.loadPopup('selectPage', plugin.selectPageLoaded);
            plugin.settings.callback.selectpage = true;

        };
        plugin.selectPageLoaded = function() {
            var html = $("#oto-fb-root .popup").html();
            html = html.replace(/{{fb-name}}/g, plugin.settings.fb_user.name);
            html = html.replace(/{{fb-email}}/g, plugin.settings.fb_user.email);
            html = html.replace(/{{fb-id}}/g, plugin.settings.fb_user.id);

            var page = $('<div />');
            var pageTemplate = parseBetween('{{fb-pages}}', '{{/fb-pages}}', html);
            $(plugin.settings.fb_pages.data).each( function(index, value){
                if(value.is_published) {
                    var tpage;
                    tpage = pageTemplate;
                    tpage = tpage.replace(/{{page-id}}/g, value.id);
                    tpage = tpage.replace(/{{page-name}}/g, value.name);
                    tpage = tpage.replace(/{{page-image}}/g, "https://graph.facebook.com/" + value.id + "/picture/");

                    tpage = $(tpage);
                    tpage.addClass('oto-fb-page');
                    tpage.attr('data-fb-id', value.id);
                    page.append(tpage);
                }
            });
            page = page.wrap('<div/>').parent().html();
            html = html.replace("{{fb-pages}}"+pageTemplate+"{{/fb-pages}}", page);

            $("#oto-fb-root .popup").html(html);
            $('#oto-fb-root .popup .oto-fb-page').bind('click', function(e){
                $.colorbox.close();

                if(plugin.settings.onPageSelect)
                    plugin.settings.onPageSelect($(this).attr('data-fb-id'), plugin.settings.fb_user, plugin.settings.fb_pages);

            });
            if($('#oto-fb-root .popup .oto-fb-page').length == 1) {
                window.setTimeout(function(){
                    $('.otofblogin-popup .popup .oto-fb-page:first').trigger('click');
                }, 100);
            }
        };

        parseBetween = function(beginString, endString, originalString) {
            var beginIndex = originalString.indexOf(beginString);
            if (beginIndex === -1) {
                return null;
            }
            var beginStringLength = beginString.length;
            var substringBeginIndex = beginIndex + beginStringLength;
            var substringEndIndex = originalString.indexOf(endString, substringBeginIndex);
            if (substringEndIndex === -1) {
                return null;
            }
            return originalString.substring(substringBeginIndex, substringEndIndex);
        };

        plugin.modalCallback = function(event) {
            if(plugin.settings.active.popup) {
                if (plugin.settings.template[plugin.settings.active.popup].callback)
                    plugin.settings.template[plugin.settings.active.popup].callback(event, plugin.settings.active.popup);
            }
        };

        plugin.loadPopup = function(name, callback) {
            plugin.settings.active.popup = name;
            view_path =  plugin.settings.template[name].view;
            $('#oto-fb-root .popup').load(view_path, function (){
                callback();
                //window.setTimeout( callback, 100 );
                if(plugin.settings.template[name].popup.load) {
                    $.colorbox({
                        width: plugin.settings.template[name].popup.width,
                        height: plugin.settings.template[name].popup.height,
                        opacity: plugin.settings.template[name].popup.opacity,
                        transition: plugin.settings.template[name].popup.transition,
                        overlayClose: plugin.settings.template[name].popup.overlayClose,
                        fixed: plugin.settings.template[name].popup.fixed,
                        closeButton: plugin.settings.template[name].popup.closeButton,
                        maxWidth: '100%',
                        maxHeight: '100%',
                        inline: true,
                        href: '#oto-fb-root .popup',
                        className: 'otofblogin-popup',
                        onOpen: function () {
                            plugin.modalCallback('modalLoading');
                        },
                        onComplete: function () {
                            plugin.modalCallback('modalLoaded');
                        },
                        onClosed: function () {
                            plugin.modalCallback('modalClosed');
                        }
                    });
                } else {
                    plugin.modalCallback('modalSkipped');
                }
            });
        };

        plugin.init();
    };

    $.fn.otoFBLogin = function(options) {

        if(!$('#oto-fb-root').length) {
            $.ajaxSetup ({
                cache: false
            });
            $('body').append('<div id="oto-fb-root" style="display: none !important;"><div class="popup"></div></div>');
        }

        return this.each(function() {
            if (undefined == $(this).data('otoFBLogin')) {
                var data_options = $(this).data();
                var merged_options = $.extend(options, data_options);
                var plugin = new $.otoFBLogin(this, merged_options);
                $(this).data('otoFBLogin', plugin);
            }
        });
    }

})(jQuery);