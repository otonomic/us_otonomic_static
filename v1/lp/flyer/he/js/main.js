var builder_domain;
if (is_localhost()) {
    builder_domain = "http://wp.test";
} else {
    builder_domain = 'http://wp.'+window.location.hostname.replace('www.', '');
}
function is_localhost() {
    if (location.host == 'otonomic.test' || location.host == 'localhost') {
        return true;
    }
    return false;
}
jQuery(document).ready(function($){
    $('#collector').submit(function(e){
        $('#btn-submit').val('שולח...');
        e.preventDefault();
        $.ajax({
            url: builder_domain+'/api/ajax/email_collector',
            data: ({name :  $('#name').val(), phone: $('#phone').val(), email: $('#email').val()}),
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                console.log(response);
                $('#btn-submit').val('שלח');
                if(response.status == 'error') {
                    alert('אופס! משהו השתבש. אנא נסו שנית, כתבו ל-support@otonomic.com, או חייגו 03-5599918');
                } else {
                    alert("תודה! קיבלנו את פנייתך. ניצור קשר בהקדם.");
                }


            }
        });
    });
});