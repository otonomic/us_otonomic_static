<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Otonomic - Trainers</title>
    <link rel="shortcut icon" href="favicon.ico">

    <meta property="og:title" content="Free website for your hair salon - only 45 Hours left!"/>
    <meta property="og:site_name" content="Otonomic"/>
    <meta property="og:description"
          content="Otonomic turns your Facebook business page into a website."/>
    <meta property="og:url" content="http://www.otonomic.com/lp/ilan/hairstylists/twoCalls/"/>
    <meta property="og:image" content="http://www.otonomic.com/images/hairstyleWebsite-theme-154x113_4x.jpg"/>

    <!-- Bootstrap -->
    <link href="css/styles.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" async="" src="http://cdn.luckyorange.com/w.js"></script>
    <script src="/v1/js/otonomic-analytics.js"></script>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=373931652687761&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="container-fluid">
      <div class="row">
          <header class="header">
            <img class="logo-img" src="images/otonomic-logo.png" alt="otonomic.com">
            <div class="fb-like pull-right" data-href="https://www.facebook.com/otonomic" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
          </header>
          <div class="splash">
            <div class="splash-title-container">
              <h1 class="splash-title">Big ass call to action title</h1>
              <ul class="splash-sub-title">
                <li><span class="pink-text">1.</span> Enter your business name</li>
                <li><span class="pink-text">2.</span> Get your website</li>
                <li><span class="pink-text">3.</span> Edit your website’s content</li>
                <li><span class="pink-text">4.</span> Publish your awsome website</li>
              </ul>
            </div>
            <img class="splash-img hidden-xs hidden-sm" src="images/splash-img.jpg">
            <!-- Search -->
            <div class="search-container">
              <form class="form-inline" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Type your Facebook page name (or URL)">
                </div>
                <button type="submit" class="btn btn-success">Get my Website!</button>
              </form>
            </div>
            <div class="splash-bottom-text">Join <span class="pink-text"
            >123500</span> hair salons all over the world</div>
          </div>
      </div>
    </div>
    <div class="features">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-4">
            <div class="feature">
              <img src="images/img-1.svg">
              <h3>Self-updating from facebook</h3>
              <p>It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather.</p>
            </div>
          </div>
          <div class="col-xs-12 col-md-4">
            <div class="feature">
              <img src="images/img-2.svg">
              <h3>Web, tablet and mobile ready</h3>
              <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.</p>
            </div>
          </div>
          <div class="col-xs-12 col-md-4">
            <div class="feature">
              <img src="images/img-3.svg">
              <h3>Web, tablet and mobile ready</h3>
              <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="testimonials">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-5 col-md-offset-1">
            <div class="media">
              <div class="media-left">
                <img src="images/testi-img-1.jpg" alt="Ashley Millsaps ">
              </div>
              <div class="media-body">
                <h4 class="media-heading">Grymm</h4>
                <div class="media-rating">
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                </div>
                <p>I've had my website for a little over a month now and my clients absolutely love it!!! They have told me that it's extremely easy to navigate and it looks awesome! I couldn't have had a better website!</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-5">
            <div class="media">
              <div class="media-left">
                <img src="images/testi-img-2.jpg" alt="Jasmine A. Cox">
              </div>
              <div class="media-body">
                <h4 class="media-heading">Shawna Todd</h4>
                <div class="media-rating">
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star active"></span>
                  <span class="glyphicon glyphicon-star"></span>
                </div>
                <p>I had been wanting a website for my small business for too long. I never found the time or energy to get it done. I'm so grateful, I wish I had done it sooner.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
          <div class="splash2">
            <div class="splash-title-container">
              <h1 class="splash-title">Big ass call to action title N0 2</h1>
            </div>
            <img class="splash-img hidden-xs hidden-sm" src="images/splash-img-2.jpg">
            <!-- Search -->
            <div class="search-container">
              <form class="form-inline" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Enter your Facebook page name (or URL)">
                </div>
                <button type="submit" class="btn btn-success">Get my Website!</button>
              </form>
            </div>
            <div class="splash-bottom-text">Trusted by more than <span class="pink-text"
            >30000</span> small Businesses in the U.S</div>
          </div>
      </div>
      <div class="row">
        <footer>
          <div class="footer-text">
            <a href="http://otonomic.com/terms" target="_blank">Terms of Use</a> | 
            <a href="http://otonomic.com/pdfs/Otonomic_Privacy_Policy.pdf" target="_blank">Privacy Policy</a> | 
            <a href="http://support.otonomic.com/" target="_blank">FAQ</a>   |   
            <a href="mailto:info@otonomic.com" target="_blank">Contact</a>
            © 2015
          </div>
          <div class="social-channels">
              <a class="social-btn facebook-btn" target="_blank" href="https://www.facebook.com/otonomic"><img src="images/facebook-icon.svg"></a>
              <a class="social-btn twitter-btn" target="_blank" href="https://twitter.com/otonomic"><img src="images/twitter-icon.svg"></a>
              <a class="social-btn linkedin-btn" target="_blank" href="https://www.linkedin.com/company/otonomic"><img src="images/linkedin-icon.svg"></a>
              <a class="social-btn googleplus-btn" target="_blank" href="https://plus.google.com/+Otonomic/about"><img src="images/googleplus-icon.svg"></a>
          </div>
        </footer>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>

    <!-- Search box template -->
    <div style="display: none">
      <div class="t_box">
          <div class="msgbox">
              <div class="header">
                  <a href="#" class="close_btn close-search" onclick="closeSearch('.search-wrapper'); return false;"><span class="glyphicon glyphicon-remove"></span></a>
                  <span class="msg_info">We weren't able to find this page on Facebook</span>
              </div>

              <div class="body_info">
                  <h1 class="first_msg">Refine your search</h1>
                  <p class="first_msg_desc">e.g. "my business" instead of "mybusiness"</p>
                  <p class="or_msg">Or</p>
                  <h1>Enter the full Facebook address of your business</h1>
                  <p style="display: inline-block;">e.g.: "https://www.facebook.com/pages/Jessicas-Pastries"</p>
                  <a href="#" id="how_do_i">How do I do that?</a>
                  <p class="or_msg">Or</p>
                  <a href="/shared/facebook_login.php" class="facebook_connect track_event measure_time" id="fb_connector" data-ga-category="LandingPage" data-ga-event="Connect with Facebook" data-ga-label="Search explanation box" data-ajax-track="1">Connect
                  </a>
                <h1 style="line-height: 34px; float:left;">So we can find your page for you.</h1>
              </div>
          </div>

          <div class="steps">
              <ul>
                  <li>
                      <h1>Step 1 </h1> | <span>Go to your Facebook business page</span>
                  </li>
                  <li>
                      <h1>Step 2 </h1> | <span>Copy the address shown in your browser</span>
                      <p>(starts with "https://www.facebook.com")</p>
                  </li>
                  <li>
                      <h1>Step 3 </h1> | <span>Paste the address in the search box above.</span>
                  </li>
                  <li>
                      <h1>Step 4 </h1> | <span>Click "See my website"</span>
                  </li>
              </ul>
          </div>
      </div>

      <div class="search_progress hidden" style="position: absolute; left: -35px;">
          <span class="msg_info">Search in progress, please wait...!!!</span>
      </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="https://d2zxr4ixkv1lwq.cloudfront.net/lp/js/jquery.jsonp-2.4.0.min.js" defer></script>

    <link rel="stylesheet" type="text/css" href="css/searchResults.css">
    <script type="text/javascript" src="/v1/js/search_filterv1.0.4-wp.js?v=1.0.4"></script>
    <link href="http://d2zxr4ixkv1lwq.cloudfront.net/lp/css/tipsy.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://d2zxr4ixkv1lwq.cloudfront.net/lp/js/jquery.tipsy.js" defer></script>
    <script type="text/javascript" src="/v1/js/otonomicv1.0.4.js"></script>

    <script type="text/javascript" src="/v1/js/functions.js"></script>
  </body>
</html>