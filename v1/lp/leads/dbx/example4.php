<!DOCTYPE html>
<html lang="en"
      class="sem-landing-page teams_create_page media-desktop"
      xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script src="//cdn.optimizely.com/js/326727683.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create your own Website - make your own website >> Otonomic</title>
    <meta name="title" content="Create your own Website - make your own website >> Otonomic">
    <meta name="description"
          content="make your own website free and immediately! With Otonomic you will create your own website in seconds by simply turning your Social presence page to a website. Try it out!">
    <meta name="keywords" content="make your own website, create your own website, build your own website">
    <link rel="shortcut icon" href="images/favicon.ico" />

    <meta property="og:title" content="Give your business the website it deserves!"/>
    <meta property="og:site_name" content="otonomic"/>
    <meta property="og:description"
          content="Do you need a website for your business? Otonomic will create one for you in seconds, just try it out!"/>
    <meta property="og:url" content="http://www.otonomic.com"/>
    <meta property="og:image" content="http://otonomic.com/images/logo148x148.png"/>

    <meta itemprop="name" content="Otonomic">
    <meta itemprop="description"
          content="Do you need a website for your business? Otonomic will create one for you in seconds, just try it out!">
    <meta itemprop="image" content="http://otonomic.com/images/logo148x148.png">

    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible"/>
    <link href="http://otonomic.com/images/favicon.ico" rel="shortcut icon"/>

    <!-- dbx original css -->
    <link href="css/main.css" type="text/css" rel="stylesheet"/>
    <link href="css/business_marketing.css" type="text/css" rel="stylesheet"/>

    <link href="/w3c/p3p.xml" rel="P3Pv1"/>

    <!--[if lt IE 9]>
    <script src="/static/javascript/external/html5shiv.js"></script>
    <script src="/static/javascript/external/es5-shim.min.js"></script>
    <script src="/static/javascript/external/es5-sham.min.js"></script><![endif]-->


    <link href="/css/searchResults.css" rel="stylesheet">
    <link href="css/otonomic-dbx-lp.css" rel="stylesheet">

    <script src="/v1/js/otonomic-analytics.js"></script>
    <!-- START Facebook Pixel Tracking -->
    <!-- Facebook Conversion Code for User visited homepage -->
    <script>
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6022084216630', {'value':'0.00','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022084216630&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
    <!-- END Facebook Pixel Tracking -->


    <style>
        .sem-landing-page .right-side #alternatives {
            margin-top: 10px;
        }
        .body-headline {
            line-height: 1.2em;
        }
        #db-logo {
            width:auto;
        }
        .customer-list li img {
            width: inherit;
        }
        .sem-landing-page .feature-row .subfeature {
            width: 235px;
        }

        .p2s_fanpages .fanpage img.media-object {
            height: 50px;
            width: 50px;
        }
        .p2s_fanpages .media p {
            font-size: 1.1em;
            line-height: 1.1em;
        }
        p.media-address {
            font-size: 12px !important;
            padding-left: 0 !important;
            margin-left: 0 !important;
        }
        .p2s_fanpages .media {
            min-height: 65px;
        }

        .p2s_fanpages .media {
            background-color: #FFF;
        }


        .p2s_fanpages .search_results {
            width: 260px;
            margin-bottom: 10px;
            border: 1px #BDC4C9 solid;
            overflow-x: hidden;
        }

        .p2s_fanpages .media {
            background-color: #F2F0F1;
        }
    </style>
</head>

<body class="en business-2013-style otonomic-styling" dir="ltr">
<div id="header" class="header">
    <div class="header-inner">
        <ul class="actionables right">
            <li class="contact-item">
                <img src="images/header-phone-icon.svg"/>
                Call us at <span class="phone-number">+1•844•otonomic</span>
            </li>
        </ul>
        <div class="bizlogo">
            <div class="lr-container">
                <a href="/business" class="lfloat"><img
                    src="images/otonomic-logo-dark.png"
                    alt="Otonomic Website Building" id="db-logo"/>
                </a> 
            </div>
        </div>
    </div>
</div>
<div id="outer-frame">
    <div id="page-content">
        <div class="business-2013-style">
            <!-- Main Content ============================================================ -->
            <!-- Option 3 ================================================================ -->
            <div class="clearfix ">
                <div class="left-side main-content"><h1>Let us create a site for you!</h1>
                    <div class="subheader">Easily get more customers with your own professional site. <br/>
                        We'll create a site using content from your Facebook page.</div>
                    <div class="body">
                        <table class="features-table">
                            <tr>
                                <td><img src="images/cup-icon.svg"/></td>
                                <td>Professional Developer that works on your site</td>
                            </tr>
                            <tr>
                                <td><img src="images/nameplate-icon.svg"/></td>
                                <td>Your own .com domain + email address <br/>
                                    <small>e.g. http://mybusiness.com and me@mybusiness.com</small>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="images/store-icon.svg"/></td>
                                <td>Custom setup of an Online Store, Appointments Scheduling tool and other useful tools</td>
                            </tr>
                            <tr>
                                <td><img src="images/lifesaver-wheel-icon.svg"/></td>
                                <td>Our top notch support</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="right-side">
                    <img src="images/laptop-girl-1.png" style="margin: 90px 0 0 90px;">
                </div>
            </div>
            <!-- Option 6 ================================================================ -->
            <div class="try-today-row subheader option-2 single-field zero-margin">
                <form name="1161" id="1161" method="POST" novalidate=""
                      action="http://otonomic.com/thank_you.php"
                      class="contact_form salesforce-form clearfix">
                    <p class="orange-text">Leave your details and we’ll create your site for you. You only pay if you wish to keep it. <small>Offer valid until Nov. 1, 2014</small></p>
                    <input type="hidden" name="page_id" id="frm_facebook_page_id"/>
                    <div id="Fanpage-field" class="sick-input half">
                        <input type="text" id="main_search_box" data-attr="center" name="page_name"
                               onClick="searchBoxClick('#main_search_box');"
                               onKeyup="searchBoxKeyUp('#main_search_box','#search_wrapper_main','.close-search');"
                               class="form-control main_search_box mobile-placeholder2 LoNotSensitive"
                               autocomplete="off" placeholder="Your Facebook page name">
                        <div class="tb search-wrapper  p2s_fanpages" id="search_wrapper_main" data-attr="center"></div>
                        <div style="position:relative;">
                            <span class="icon_clear close-search" onClick="closeSearch('#search_wrapper_main','center')" style="display: none;">
                                <span class="glyphicon glyphicon-remove"></span>
                            </span>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-oto-orange" value="Create My Site" tabindex="5">
                    <p class="small-gray-text">By entering your contact information, you agreeto Otonomic’s <a href="#">Privacy Policy and Terms.</a></p>
                </form>
            </div>
            
            <!-- Testimonials ============================================================ -->
            <div class="quote"><img src="images/quote.png"/>
                Otonomic offered me what I need at the time I needed it, a well designed, informative website that I update effortlessly wherever I travel.
                <img src="images/quote_right.png"/>

                <div class="author"><img src="images/brian.png">Brian O'Callaghan, <a href="http://dublinacupunctureclinic.com" target="_blank">dublinacupunctureclinic.com</a></div>
            </div>
            <!-- Features ================================================================ -->
            <div class="feature-row">
                <div class="subfeature"><img src="images/feature1.png"/>
                    <div class="body-headline">Stunning Templates</div>
                    <p class="description">Select a beautiful template for you site from one of our beautiful designs.</p>
                </div>
                <div class="subfeature"><img src="images/feature2.png"/>
                    <div class="body-headline">Self Updating</div>
                    <p class="description">Whenever you post something on your Facebook page, it immediately appears on your Otonomic site.</p>
                </div>
                <div class="subfeature"><img src="images/feature3.png"/>
                    <div class="body-headline">Desktop, tablet and mobile ready</div>
                    <p class="description">Reach clients wherever they go, on all platforms and all devices in all sizes.</p>
                </div>
            </div>
            <div class="subheader">
                As seen on:
            </div>
            <div class="business-customers">
                <img src="images/seen-on.jpg">
            </div>
        </div>
    </div>
</div>


<noscript><p><img src="http://a.otonomic.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="/v1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/v1/js/placeholders.min.js"></script>
<script type="text/javascript" src="/v1/js/jquery.scrollstop.js"></script>
<script type="text/javascript" src="/v1/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="/v1/js/jquery.scrollsnap.js"></script>
<script type="text/javascript" src="/v1/js/jquery.touchSwipe.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="/v1/js/main.js"></script>
<script type="text/javascript" src="//cherne.net/brian/resources/jquery.hoverIntent.minified.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<link href="http://d2zxr4ixkv1lwq.cloudfront.net/lp/css/tipsy.css" rel="stylesheet" type="text/css" />

<!-- // Facebook -->
<div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({ appId: "373931652687761",status: true,cookie: true,xfbml: true});

        window.fbAsyncInit.fbLoaded.resolve();
        //checkConnectedWithFacebook();
    };

    window.fbAsyncInit.fbLoaded = jQuery.Deferred();

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js&appId=373931652687761";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- // Google Plus -->
<script type="text/javascript">
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
</script>

<script>
    AUTO_FOCUS = false;
    SEARCH_PICTURE_SIZE = 80;
</script>

<script type="text/javascript">
    alert('sjssjs');
    jQuery(document).ready(function($) {
        $('#search_wrapper_main').on('click', '.media.search-results-item', function(e) {
            e.preventDefault();
            $('#main_search_box').value($(this).attr('data-result-number'));
        });
    }
</script>

<link href="/css/tipsy.css" rel="stylesheet" type="text/css">
<link href="/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/v1/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/v1/js/search_filterv1.0.4-wp.js"></script>
<script type="text/javascript" src="/v1/js/jquery.jsonp-2.4.0.min.js"></script>
<script type="text/javascript" src="/v1/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="/v1/js/otonomicv1.0.4.js"></script>

<script>
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    if( getParameterByName('msg') == 'site-deleted' ) {
        alert('Site was deleted successully.');
    }
</script>

<script type="text/javascript" src="/v1/js/functions.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#search_wrapper_main').on('click', '.media.search-results-item', function(e) {
            e.preventDefault();
            $('#frm_facebook_page_id').val( $(this).attr('data-facebook-page-id') );
            $('#main_search_box').val( $(this).attr('data-facebook-page-name') );
            $('.search_results').hide();
        });

        $('input[type=submit]').click(function(event){

            var search_box = $(this).parent().find('.main_search_box');
            if (search_box.val().length < 3) {
                event.preventDefault();
                search_box.focus();
            }
        });
    });
</script>

</body>
</html>