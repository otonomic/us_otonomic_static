<!DOCTYPE html>
<!--[if lte IE 8]> <html class="ie8" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html lang="en">             <!--<![endif]-->
<head>
    <script src="//cdn.optimizely.com/js/326727683.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Otonomic - turn your Facebook page into a professional website</title>
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" type="text/css" href="css/style.css?v=0.0.1">
    <style>
        .search_results .close-search{ display: block !important;}
    </style>

    <meta property="og:title" content="Free website for your hair salon - only 45 Hours left!"/>
    <meta property="og:site_name" content="Otonomic"/>
    <meta property="og:description"
          content="Otonomic turns your Facebook business page into a website."/>
    <meta property="og:url" content="http://www.otonomic.com/lp/ilan/hairstylists/twoCalls/"/>
    <meta property="og:image" content="http://www.otonomic.com/images/personalTrainersWebsite-theme-154x113_4x.jpg"/>

    <link rel="stylesheet" type="text/css" href="css/media-queries.css?v=0.0.3" />
    <script type="text/javascript" async="" src="http://cdn.luckyorange.com/w.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
    <script type="text/javascript" src="js/main.js?v=1.0"></script>
    <script src="/v1/js/otonomic-analytics.js"></script>

</head>

<body>
<div class="csszoom" id="smartphone-guy">
<div class="wrapper">
    <div id="main">
        <div class="container2">
            <header>
                <div class="logo"><img src="images/logo.png" alt=" " /></div>
                <div class="top_text">We take your Facebook page and automatically turn it into a Web & Mobile website!</div>
            </header>
            <div class="top_wraper2">
                <h2 class="js-heading-text heading_text">Get your free <br>and beautiful website!</h2>
                <p class="text2">How? it’s easy!</p>
                <ul>
                    <li class="active"><span id="step-1" class="step">1</span> Enter your Facebook business page</li>
                    <li><span id="step-2" class="step">2</span> Share this promotion with your friends</li>
                    <li><span id="step-3" class="step">3</span> Sit tight while we get your website ready.</li>
                </ul>
                <p class="js-text3 text3" style="opacity: 0;">Spread the word</p>
                <!-- Search input field -->
                <div class="p2s_fanpages">
                  <div class="search-field form-search">
                    <input id="main_search_box" type="text" 
                    class="form-control main_search_box LoNotSensitive"
                    data-attr="center"
                    onClick="searchBoxClick('#main_search_box');" 
                    onKeyup="searchBoxKeyUp('#main_search_box','#search_wrapper_main','.close-search');"
                    placeholder="Type your Facebook page name (or URL)">
                    <span class="input-group-btn">
                      <button id="btn_go" class="btn btn_go" data-attr="center" data-target-field="main_search_box" type="button">Get my website</button>
                    </span>
                    <span class="close-search" onClick="closeSearch('#search_wrapper_main','center')" style="display: none;">
                      <img src="/shared/fanpages/images/close.png" width="32" height="32">
                    </span>
                  </div>
                  <div class="tb search-wrapper" id="search_wrapper_main" data-attr="center"></div>
                </div>
                <!-- Share Buttons -->
                <div class="js-social-shares social-shares-container" style="display:none;">
                    <a href="javascript:void(0)" class="fb-share" onclick="shareOnFB();">
                        <img src="images/facebook-icon.svg">
                        Share on facebook
                    </a>
                    <a class="tweet" href="https://twitter.com/intent/tweet?url=http://otonomic.com/&via=otonomic&text=Only 45 Hours: FREE website for your hair salon: Otonomic turns your Facebook business page into a website">
                        <img src="images/twitter-icon.svg">
                        Share on twitter
                    </a>             
                </div>
            </div>
            <div style="clear:both"></div>
            <div class="testimonial">
                <div class="no_of_website_counting"><span class="counting_no">30,218</span><span>Websites created, and counting...</span></div>
                <div class="testimonials_wrap">
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user1.png" alt=" " /></div>
                        <blockquote><span>I was surprised by how easy it was to create and customize the website.</span></blockquote>
                    </div>
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user2.png" alt=" " /></div>
                        <blockquote><span>After years of putting off building a site, I finally have one that works and looks great.</span></blockquote>
                    </div>
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user3.png" alt=" " /></div>
                        <blockquote><span>Updating my website is a breeze. Love how every post appears on my site immediately.</span></blockquote>
                    </div>
                </div>
            </div>
            <div class="footer_text"><a href="http://otonomic.com/terms" target="_blank">Terms of Use</a> | <a href="http://otonomic.com/pdfs/Otonomic_Privacy_Policy.pdf" target="_blank">Privacy Policy</a> | <a href="http://support.otonomic.com/" target="_blank">FAQ</a>   |   <a href="mailto:info@otonomic.com" target="_blank">Contact</a>     © 2015
                <div class="social-channels">
                    <a class="social-btn facebook-btn" target="_blank" href="https://www.facebook.com/otonomic"><img src="images/facebook-icon.svg"></a>
                    <a class="social-btn twitter-btn" target="_blank" href="https://twitter.com/otonomic"><img src="images/twitter-icon.svg"></a>
                    <a class="social-btn linkedin-btn" target="_blank" href="https://www.linkedin.com/company/otonomic"><img src="images/linkedin-icon.svg"></a>
                    <a class="social-btn googleplus-btn" target="_blank" href="https://plus.google.com/+Otonomic/about"><img src="images/googleplus-icon.svg"></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

    <!-- Search box template -->
    <div style="display: none">
      <div class="t_box">
          <div class="msgbox">
              <div class="header">
                  <a href="#" class="close_btn close-search" onclick="closeSearch('.search-wrapper'); return false;"><span class="glyphicon glyphicon-remove"></span></a>
                  <span class="msg_info">We weren't able to find this page on Facebook</span>
              </div>

              <div class="body_info">
                  <h1 class="first_msg">Refine your search</h1>
                  <p class="first_msg_desc">e.g. "my business" instead of "mybusiness"</p>
                  <p class="or_msg">Or</p>
                  <h1>Enter the full Facebook address of your business</h1>
                  <p style="display: inline-block;">e.g.: "https://www.facebook.com/pages/Jessicas-Pastries"</p>
                  <a href="#" id="how_do_i">How do I do that?</a>
                  <p class="or_msg">Or</p>
                  <a href="/shared/facebook_login.php" class="facebook_connect track_event measure_time" id="fb_connector" data-ga-category="LandingPage" data-ga-event="Connect with Facebook" data-ga-label="Search explanation box" data-ajax-track="1">Connect
                  </a>
                <h1 style="line-height: 34px; float:left;">So we can find your page for you.</h1>
              </div>
          </div>

          <div class="steps">
              <ul>
                  <li>
                      <h1>Step 1 </h1> | <span>Go to your Facebook business page</span>
                  </li>
                  <li>
                      <h1>Step 2 </h1> | <span>Copy the address shown in your browser</span>
                      <p>(starts with "https://www.facebook.com")</p>
                  </li>
                  <li>
                      <h1>Step 3 </h1> | <span>Paste the address in the search box above.</span>
                  </li>
                  <li>
                      <h1>Step 4 </h1> | <span>Click "See my website"</span>
                  </li>
              </ul>
          </div>
      </div>

      <div class="search_progress hidden" style="position: absolute; left: -35px;">
          <span class="msg_info">Search in progress, please wait...!!!</span>
      </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="https://d2zxr4ixkv1lwq.cloudfront.net/lp/js/jquery.jsonp-2.4.0.min.js" defer></script>

    <link rel="stylesheet" type="text/css" href="css/searchResults.css">
    <script type="text/javascript" src="/v1/js/search_filterv1.0.4-wp.js?v=1.0.4"></script>
    <link href="http://d2zxr4ixkv1lwq.cloudfront.net/lp/css/tipsy.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://d2zxr4ixkv1lwq.cloudfront.net/lp/js/jquery.tipsy.js" defer></script>
    <script type="text/javascript" src="/v1/js/otonomicv1.0.4.js"></script>

    <script type="text/javascript" src="/v1/js/functions.js"></script>

</body>
</html>
