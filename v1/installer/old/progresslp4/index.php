<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Otonomic Site Creation Page">
    <meta name="author" content="Otonomic">
    <link rel="shortcut icon" href="favicon.ico">

    <title>Otonomic is creating your site...</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wp-loading-page.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/v1/js/otonomic-analytics.js?v=1.0"></script>
  </head>

  <body class="wp-lp">

  <!-- Facebook SDK -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=575528525876858&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- /Facebook SDK -->

    <!-- Intro    ========================================================== -->
    <div class="container-fluid">

      <div id="intro" class="row installer-stage">
        <div class="bg-image hidden-xs"></div>
        <div class="col-xs-12">
          <div class="text-center">
            <img class="logo" src="images/otonomic-logo-dark.png">
            <h1 class="title">Create a brand new website for </h1>
            <p class="site-name" class="ot-fb-name">YOUR BUSINESS</p>
            <h2 >in just 5 clicks!</h2>
            <a href="#" onclick="return false;" class="btn btn-ttc-blue js-intro-next">
                Let the magic begin!<span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
        </div>
      </div>

      <!-- Stage 1 ========================================================== -->
      <div id="stage-1" class="row hidden installer-stage">
        <div class="bg-image hidden-xs "><img src="images/bg6.jpg"></div>
        <div class="content-panel">
          <img class="logo" src="images/otonomic-logo-dark.png">
          <h1 class="title">Automatic updates, faster than a buzzcut.</h1>
          <p>Otonomic uses your existing Facebook page to build your website.</p>
          <p class="">You can instantly integrate other social media networks to your site by clicking below:</p>
          <p class="visible-xs"><b>Connect With</b></p>
          <div id="social_connect_hybrid">
            <a id="authorize_Facebook" class="js-connect-facebook social-btn facebook-btn" href="#"><img src="images/facebook-icon.svg"><span class="hidden-xs"> Connect with </span>Facebook</a>
            <a id="authorize_Twitter" class="js-connect-twitter social-btn twitter-btn" href="#"><img src="images/twitter-icon.svg"><span class="hidden-xs"> Connect with </span>Twitter</a>
            <a id="authorize_Instagram" class="js-connect-instagram social-btn instagram-btn" href="#"><img src="images/instagram-icon.svg"><span class="hidden-xs"> Connect with </span>Instagram</a>
            <!-- <a id="authorize_LinkedIn" class="js-connect-tumbler social-btn yelp-btn" href="#"><img src="images/yelp-icon.svg"></a> -->
          </div>
          <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right js-stage1-next">
            Continue
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>

      <!-- Stage 2 ========================================================== -->
      <div id="stage-2" class="row hidden installer-stage">
        <div class="bg-image hidden-xs"><img src="images/bg3.jpg"></div>
        <div class="content-panel">
          <img class="logo" src="images/otonomic-logo-dark.png">
          <h1 class="title">Get ready to be found.</h1>
          <p>Meet more clients looking for a hot new look when you appear on Google search results.</p>
          <img class="stage-img visible-xs visible-sm" src="images/image1.png">
          <p> You're 3 clicks away from a brand new website!</p>
          <a href="#" class="btn btn-ttc-orange pull-right js-stage2-next">
            Click here
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>

      <!-- Stage 3 ========================================================== -->
      <div id="stage-3" class="row hidden">
        <div class="bg-image hidden-xs"><img src="images/bg4.jpg"></div>
        <div class="content-panel">
          <img class="logo" src="images/otonomic-logo-dark.png">
          <h1 class="title">Looking good!</h1>
          <p>Your website's right for every screen: desktop, laptop, tablet and mobile</p>
          <img class="stage-img" src="images/image2.png" style="display: block; width: 240px; margin-left: auto; margin-right: auto;">
          <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right js-stage3-next">
            2 more clicks!
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>

        <!-- Stage 4 ========================================================== -->
      <div id="stage-4" class="row hidden installer-stage">
        <div class="bg-image hidden-xs"><img src="images/bg5.jpg"></div>
        <div class="content-panel">
          <img class="logo" src="images/otonomic-logo-dark.png">
          <h1 class="title">Would you like to offer Online Booking?</h1>
          <p>(You can always change your mind)</p>
              <a href="#" onclick="return false;" class="js-stage4-next submit-booking btn btn-ttc-orange pull-right glyph-on-left"><span class="glyphicon glyphicon-ok"></span>Yes</a>
              <a href="#" onclick="return false;" style="margin-right: 10px;" class="js-stage4-next submit-skip-booking btn btn-ttc-orange pull-right glyph-on-left"><span class="glyphicon glyphicon-remove"></span>No</a>
        </div>
      </div>

      <!-- Congratz ========================================================== -->
      <div id="congratz" class="row hidden text-center installer-stage">
        <img class="logo" src="images/otonomic-logo-dark.png">
        <div class="upper-content">
          <p class="site-name" class="ot-fb-name">YOUR BUSINESS</p>
          <h1 class="congratz-title">website will be ready in <span id="counter">7 seconds</span></h1>
          <div class="fb-like" data-href="https://www.facebook.com/otonomic" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
          <img class="oto-anima" src="images/ottoHoverLoop.gif">
        </div>
        <div class="lower-content">
          <h3 id="oto-web-url" class="hidden">http://wp.otonomic.com/newsite</h3>
          <p class="tos">
            You hereby agree to Otonomic's <a href="http://otonomic.com/terms" target="_blank" id="link-tos">Terms of Service</a>
          </p>
        </div>
      </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="/v1/js/loading_page4.js?v=0.52"></script>


    <script type="text/javascript">
    var base_url = 'http://otonomic.com/hybridauth/twitter.php';
    //var local_url = 'http://otonomic.test/hybridauth/twitter.php';
    $(document).ready(function(){

        trackFacebookPixel('viewed_installer');
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6021618382030', {'value':'0.00','currency':'USD'}]);


        $('#social_connect_hybrid a').click(function(){
            var type = $(this).attr('id').split('_');

            track_event('Loading Page', 'Social Connect', type[1]);

            var url = base_url+"?social="+type[1];
            window.open(
                url,
                "hybridauth_social_sign_on",
                "location=0,status=0,scrollbars=1,width=800,height=500"
            );
            return false;
        });

        // Change social buttons appearance depending on screen width
        var changeWidth = function(){
          console.log('Resized');
          if ( $(window).width() < 480 ){
            $('#social_connect_hybrid').addClass('btn-group-justified btn-group');
            $('#social_connect_hybrid a').addClass('btn');
          } else {
            $('.btn-group-vertical').removeClass('btn-group-justified btn-group');
            $('#social_connect_hybrid a').removeClass('btn');
          }
        };
        $(window).resize(changeWidth());
    });
    </script>
  </body>
</html>
