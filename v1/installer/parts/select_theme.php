<!-- Choose a template ================================================ -->
<div id="stage-choose-template" class="row hidden installer-stage">
    <div class="content-panel">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
        <div class="hidden-xs">
            <h1 class="title">
                <?= _('Your website, your vision.') ?>
            </h1>
            <h2>
                <?= _('Choose a template that you like. You can switch anytime.') ?>
            </h2>
        </div>
        <div class="visible-xs">
            <h1 class="title">
                <?= _('Choose a template that you like:') ?>
            </h1>
            <p>
                <?= _('(You can switch anytime)') ?>
            </p>
        </div>
        <div class="row">


            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-right">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="curly-beige">
                        <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/curly-beige.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-left center-block">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="dream-salon">
                        <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-salon.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-right">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="dream-fitness">
                        <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-fitness.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>



            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-left center-block">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="blonde-rays">
                        <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/blonde-rays.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-right">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="igloo">
                        <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/igloo.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-left">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="fluffy-strokes">
                        <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/fluffy-strokes.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>

        </div>



        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-right">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="el-greco">
                        <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/el-greco.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-6">
                <div class="template-conrainer pull-left center-block">
                    <div class="overlay hidden-xs hidden-md hidden-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                    <a href="#" class="btn-choose-template" data-option-value="cousteau">
                        <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/cousteau.png">
                    </a>
                    <div class="visible-xs visible-md visible-sm">
                        <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                            <span class="glyphicons ok_2"></span>
                            <?= _('Select') ?>
                        </button>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
