<!-- Stage 1 ========================================================== -->
<div id="stage-seo" class="row hidden installer-stage">
    <div class="bg-image hidden-xs ">
        <img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg3.jpg">
    </div>
    <div class="content-panel">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">

        <div class="hidden-xs">
            <h1 class="title"><?= _('Get your business ready to be found') ?></h1>
            <h2><?= _('Do you want your website listed on the following search engines and directories?') ?></h2>
            <div class="row">
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Google" data-analytics-action="Search engines" data-analytics-label="Google"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/google.png"></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Yahoo" data-analytics-action="Search engines" data-analytics-label="Yahoo"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/yahoo.png"></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Bing" data-analytics-action="Search engines" data-analytics-label="Bing"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/bing.png"></button>
                </div>
            </div>
            <hr>
            <a href="#stage-2" class="btn btn-ttc-orange pull-right js-stage1-next">
                <?= _('Continue') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <a href="#intro" class="btn btn-ttc-clear pull-right btn-back">
                <span class="glyphicons undo"></span>
                <?= _('Back') ?>
            </a>
        </div>

        <div class="visible-xs">
            <h1 class="title">
                <?= _('Get your business ready to be found.') ?>
            </h1>
            <p>
                <?= _('We will get your website listed on Google, Yahoo, Bing and other search engines and directories.') ?>
            </p>
            <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right js-stage1-next">
                <?= _('Continue') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>

