<!-- Stage 3 ========================================================== -->
<div id="stage-apps" class="row hidden installer-stage">
    <div class="bg-image hidden-xs"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg7.jpg"></div>
    <div class="content-panel">
        <div class="hidden-xs">
            <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
            <h1 class="title">
                <?= _("Keep your business open 24/7") ?>
            </h1>
            <div class="row">
                <!--
                            <div class="col-xs-12">
                              <button id="option-online-store" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Store"><span class="text-type-1">Online Store</span> sell products & services online <span class="glyphicons shop"></span></button>
                            </div>
                -->
                <div class="col-xs-12">
                    <button id="option-booking" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Booking"><span class="text-type-1">Online Booking</span> let your clients book online <span class="glyphicons calendar"></span></button>
                </div>
                <div class="col-xs-12">
                    <button class="btn btn-block btn-ttc-white btn-checkbox btn-add-on btn-uncheck-others" data-analytics-action="Addons" data-analytics-label="I don’t need these features">
                        <?= _('I don’t need it<br/>
                        <small>You can always add this later</small>') ?>
                        <span class="glyphicons remove"></span>
                    </button>
                </div>
            </div>
            <hr>
            <a href="#stage-auto-update" class="btn btn-ttc-orange pull-right disabled js-stage3-next next-btn">
                <?= _('Select') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <a href="#stage-2" class="btn btn-ttc-clear btn-back pull-right">
                <span class="glyphicons undo"></span>
                <?= _('Back') ?>
            </a>
        </div>
        <div class="visible-xs">
            <h1 class="title">
                <?= _("Your website is open 24/7:") ?>
            </h1>
            <p class="text-left"><span class="glyphicons ok_2" style="vertical-align: text-bottom;"></span>
                <?= _("Online Store: Sell your products & Services Online") ?>
            </p>
            <p class="text-left"><span class="glyphicons ok_2" style="vertical-align: text-bottom;"></span>
                <?= _("Online Booking: Let your clients book appointments") ?>
            </p>
            <a href="#stage-auto-update"  class="btn btn-ttc-orange pull-right js-stage3-next">
                <?= _('Continue') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>

