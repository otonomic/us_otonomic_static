<!-- Congratz ========================================================== -->
<div id="congratz" class="row hidden installer-stage text-center">
    <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
    <div class="upper-content">
        <p class="site-name" id="ot-fb-name">
            <?= _('Your business') ?>
        </p>
        <h1 class="congratz-title">
            <?= sprintf( _('website will be ready in %s7 seconds%s'), '<span id="counter">', '</span>'); ?>
        </h1>
        <img class="oto-anima" src="https://otonomic-static.s3.amazonaws.com/images/installer/ottoHoverLoop.gif">
        <div class="fb-like" data-href="https://www.facebook.com/otonomic" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
    </div>
    <div class="lower-content">
        <h3 id="oto-web-url" class="hidden">http://wp.otonomic.com/newsite</h3>
        <p class="tos">
            <?= sprintf( _('By continuing to use the service, you accept the Otonomic %sTerms of Service%s'), '<a target="_blank" href="/pdfs/Otonomic_Terms_of_Service.pdf" id="link-tos">', '</a>') ?>
        </p>
    </div>
</div>

