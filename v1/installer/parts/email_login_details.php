<!-- Email Screen ========================================================== -->
<div id="stage-email-login-details" class="row hidden installer-stage">
    <div class="bg-image hidden-xs ">
        <img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg2.jpg">
    </div>
    <div class="content-panel">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">

        <div class="">
            <h1 class="title">
                <?= _('Congratulations! Your site is ready.') ?>
            </h1>
            <h2>
                <?= _('Fill in your email address and we’ll send you your username and password.<br/>Once you’re done, we’ll take you to your new site.') ?>
            </h2>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <span class="required"> * </span>
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" class="tooltip" title="The email address will appear on your site. Contact messages will also be emailed to this address."></i>
                        <div id="email-notice" class="notice" style="display: none">
                            <?= _('Please fill this field') ?>
                        </div>
                        <div id="email-notice-2" class="notice" style="display: none">
                            <?= _('Please enter valid email') ?>
                        </div>
                        <input type="email" class="form-control" id="email" name="email" value="">
                    </div>
                </div>
            </div>
            <hr>
            <a href="#" onclick="return false;" id="js-stage-email-next" class="btn btn-ttc-orange pull-right">
                <?= _('Send me the login details') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>

    </div>
</div>

