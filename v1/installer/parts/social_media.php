<!-- Stage  ========================================================== -->
<div id="stage-social-media" class="row hidden installer-stage">
<div class="bg-image hidden-xs" style="
                background-image: url(https://otonomic-static.s3.amazonaws.com/images/installer/installer-social-new.jpg);
                background-position-x: 57%;
              "></div>

<div class="content-panel">
    <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
    <h1 class="title">
        <?= _("Every social media post added to your website, in real time.") ?>
    </h1>
    <h2>
        <?= _('Promote your website with every post, picture, and video.') ?>
    </h2>
    <p class="social_searching_msg">
    </p>

    <p>
        <?= _('Add your business’ social media accounts to your website:') ?>
    </p>

    <div class="row">
        <div class="col-xs-12">

            <!-- START Instagram -->
            <div class="form-group social-media-field" id="instagram">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="social_media_instagram"><i class="fa fa-instagram"></i>
                            <?= _('Instagram') ?>
                        </label>
                    </div>
                    <div class="col-xs-9 has-feedback">
                        <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameInstagram.php" id="social_media_instagram" name="social[instagram]" value="">
                        <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-results-container" id="search-results-instagram"></div>
                    </div>
                </div>
            </div>
            <!-- END Instagram -->

            <!-- START YouTube -->
            <div class="form-group social-media-field" id="youtube">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="social_media_youtube"><i class="fa fa-youtube"></i>
                            <?= _('Youtube') ?>
                        </label>
                    </div>
                    <div class="col-xs-9 has-feedback">
                        <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameYoutube.php" id="social_media_youtube" name="social[youtube]" value="">
                        <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-results-container" id="search-results-youtube"></div>
                    </div>
                </div>
            </div>
            <!-- END YouTube -->

            <!-- START Twitter -->
            <div class="form-group social-media-field" id="twitter">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="social_media_twitter"><i class="fa fa-twitter"></i>
                            <?= _('Twitter') ?>
                        </label>
                    </div>
                    <div class="col-xs-9 has-feedback">
                        <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameTwitter.php" id="social_media_twitter" name="social[twitter]" value="">
                        <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-results-container" id="search-results-twitter"></div>
                    </div>
                </div>
            </div>
            <!-- END Twitter -->

            <!-- START LinkedIn -->
            <!--                      <div class="form-group social-media-field" id="linkedin">
                                      <div class="row">
                                          <div class="col-xs-3">
                                              <label for="social_media_linkedin"><i class="fa fa-linkedin"></i>
                                                  LinkedIn
                                              </label>
                                          </div>
                                          <div class="col-xs-9 has-feedback">
                                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameLinkedin.php" id="social_media_linkedin" name="social[linkedin]" value="">
                                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                          </div>
                                      </div>

                                      <div class="row">
                                          <div class="col-xs-12">
                                              <div class="search-results-container" id="search-results-linkedin"></div>
                                          </div>
                                      </div>
                                  </div>
            -->
            <!-- END LinkedIn -->

            <!-- START Flickr -->
            <!--
                                  <div class="form-group social-media-field" id="flickr">
                                      <div class="row">
                                          <div class="col-xs-3">
                                              <label for="social_media_flickr"><i class="fa fa-flickr"></i>
                                                  Flickr
                                              </label>
                                          </div>
                                          <div class="col-xs-9 has-feedback">
                                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameFlickr.php" id="social_media_flickr" name="social[flickr]" value="">
                                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                          </div>
                                      </div>

                                      <div class="row">
                                          <div class="col-xs-12">
                                              <div class="search-results-container" id="search-results-flickr"></div>
                                          </div>
                                      </div>
                                  </div>
            -->
            <!-- END Flickr -->


            <!-- START Google+ -->
            <!--
                                  <div class="form-group social-media-field" id="googleplus">
                                      <div class="row">
                                          <div class="col-xs-3">
                                              <label for="social_media_googleplus"><i class="fa fa-google-plus"></i>
                                                  Google+
                                              </label>
                                          </div>
                                          <div class="col-xs-9 has-feedback">
                                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameGoogleplus.php" id="social_media_googleplus" name="social[googleplus]" value="">
                                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                          </div>
                                      </div>

                                      <div class="row">
                                          <div class="col-xs-12">
                                              <div class="search-results-container" id="search-results-google-plus"></div>
                                          </div>
                                      </div>
                                  </div>
            -->
            <!-- END Google+ -->

            <!-- START Pinterest -->
            <!--
                                  <div class="form-group social-media-field" id="pinterest">
                                      <div class="row">
                                          <div class="col-xs-3">
                                              <label for="social_media_pinterest"><i class="fa fa-pinterest"></i>
                                                  Pinterest
                                              </label>
                                          </div>
                                          <div class="col-xs-9 has-feedback">
                                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernamePinterest.php" id="social_media_pinterest" name="social[pinterest]" value="">
                                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                          </div>
                                      </div>

                                      <div class="row">
                                          <div class="col-xs-12">
                                              <div class="search-results-container" id="search-results-pinterest"></div>
                                          </div>
                                      </div>
                                  </div>
            -->
            <!-- END Pinterest -->

        </div>

        <div class="action-buttons col-xs-12">
            <a href="#stage-3" class="btn btn-ttc-clear btn-back">
                <span class="glyphicons undo"></span>
                <?= _('Back') ?>
            </a>
            <a href="#choose-template" class="btn btn-ttc-orange pull-opposite btn-next">
                <span class="glyphicon glyphicon-ok"></span>
                <?= _('Next') ?>
            </a>
        </div>

    </div>
</div>
</div>

