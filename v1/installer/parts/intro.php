<div id="intro" class="row installer-stage">
    <div class="bg-image hidden-xs"></div>
    <div class="col-xs-12">
        <div class="text-center">
            <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
            <h1 class="title"><?= _('Create a website for ') ?></h1>
            <p class="site-name" id="ot-fb-name"><?= _('YOUR BUSINESS') ?></p>
            <h2 ><?= _('in just 5 clicks!') ?></h2>
            <a href="#stage-1" class="btn btn-ttc-blue js-intro-next">
                <?= sprintf(_('Let the magic begin! %s'), '<span class="glyphicon glyphicon-chevron-right"></span>'); ?>
            </a>
        </div>
    </div>
</div>

