<script>

    /* MouseStats */
    function track_mousestats() {
        var MouseStats_Commands = MouseStats_Commands ? MouseStats_Commands : [];
        (function () {
            if(document.getElementById('MouseStatsTrackingScript') == undefined) {
                var mousestats_script = document.createElement('script');
                mousestats_script.type = 'text/javascript';
                mousestats_script.id = 'MouseStatsTrackingScript';
                mousestats_script.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www2') + '.mousestats.com/js/5/2/5227660694399312196.js?' + Math.floor(new Date().getTime()/600000);
                mousestats_script.async = true;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mousestats_script);
            } })();
    }
    /* END MouseStats */


</script>