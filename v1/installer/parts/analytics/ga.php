<script>
/* Google Analytics */
function track_google_analytics() {
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-37736198-1', 'auto');
ga('set', 'dimension4', window.location.hostname); // Site url
ga('set', 'dimension5', 'otonomic marketing site'); // Site Type
ga('send', 'pageview');
}
/* END Google Analytics */
</script>