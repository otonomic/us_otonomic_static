<!-- Stage 2 ========================================================== -->
<div id="stage-responsive" class="row hidden installer-stage">
    <div class="bg-image hidden-xs"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg4.jpg"></div>
    <div class="content-panel">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
        <div class="hidden-xs">
            <h1 class="title">
                <?= _('Looking good!') ?>
            </h1>
            <h2>
                <?= _('Your website looks right on every screen.') ?>
            </h2>
            <p>
                <?= _('It pays off to look great on every format, clients and search engines take notice and put your business above the rest.</br>Select the devices for an optimized version of your site:') ?>
            </p>
            <div class="row">
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Desktop"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/desktop.png"></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Tablet"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/tablet.png"></button>
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Mobile"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/mobile.png"></button>
                </div>
            </div>
            <hr>
            <a href="#stage-3" class="btn btn-ttc-orange pull-right js-stage2-next next-btn">
                <?= _('Continue') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <a href="#stage-1" class="btn btn-ttc-clear btn-back pull-right">
                <span class="glyphicons undo"></span>
                <?= _('Back') ?>
            </a>
        </div>
        <div class="visible-xs">
            <h1 class="title">
                <?= _('Your website looks right on every screen.') ?>
            </h1>
            <img src="https://otonomic-static.s3.amazonaws.com/images/installer/image2.png" style="width: 40%;">
            <p>
                <?= _('Looking good on mobile, tablet and desktop, provides for easier navigation and puts your website above the rest.') ?>
            </p>
            <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right next-btn js-stage2-next">
                <?= _('Continue') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>

