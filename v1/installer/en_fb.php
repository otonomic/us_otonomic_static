<?php
$locale = 'he_IL';
$domain = 'fb_installer';
putenv('LC_ALL=' . $locale);
setlocale(LC_ALL, $locale);
define('LOCALE_DIR', __DIR__.'/languages');
bindtextdomain($domain, LOCALE_DIR);
textdomain($domain);
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-scalable=0">
    <meta name="description" content="Otonomic Site Creation Page">
    <meta name="author" content="Otonomic">
    <link rel="shortcut icon" href="favicon.ico">

    <title><?= _('Otonomic is creating your site...') ?></title>

    <!-- Bootstrap core CSS -->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!--<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

    <!-- Glyphicons -->
    <link href="fonts/Glyphicons-WebFont/glyphicons.css" rel="stylesheet">
    <!-- Aller font -->
    <link href="fonts/Aller-WebFont/aller_font.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wp-loading-page.css" rel="stylesheet">

      <!--<script src="js/jquery-1.11.1.min.js"></script>-->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.js"></script>

      <!--<script src="js/bootstrap.min.js"></script>-->
      <!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

      <![endif]-->
      <script src="/v1/js/jquery.ba-bbq.min.js"></script>

      <?php include_once(__DIR__ . '/parts/analytics/otonomic-analytics.php'); ?>

      <style>
          .notice {
              display: none;
              color: red;
              font-weight: bold;
          }
          .required {
              color: red;
          }
      </style>
  </head>

  <body class="wp-lp">

  <?php include(__DIR__ . '/parts/analytics/gtm.php'); ?>

  <?php include(__DIR__ . '/parts/facebook_sdk.php'); ?>

    <!-- Intro    ========================================================== -->
    <div class="container-fluid">

        <?php include(__DIR__ . '/parts/intro.php'); ?>

        <?php include(__DIR__ . '/parts/seo.php'); ?>

        <?php include(__DIR__ . '/parts/responsive.php'); ?>

        <?php include(__DIR__ . '/parts/apps.php'); ?>

        <?php include(__DIR__ . '/parts/social_media.php'); ?>

        <?php include(__DIR__ . '/parts/select_theme.php'); ?>

        <?php include(__DIR__ . '/parts/congratz.php'); ?>

        <?php include(__DIR__ . '/parts/email_login_details.php'); ?>
    </div><!-- /.container -->

    <script src="/v1/js/installer.js"></script>

    <script type="text/javascript">
        trackFacebookPixel('viewed_installer');
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6021618382030', {'value':'0.00','currency':'USD'}]);


        /*
       var base_url = '//otonomic.com/hybridauth/twitter.php';
      jQuery(document).ready(function($) {
          $('#social_connect_hybrid a').click(function(){
              var type = $(this).attr('id').split('_');

              track_event('Loading Page', 'Social Connect', type[1]);

              var url = base_url+"?social="+type[1];
              window.open(
                      url,
                      "hybridauth_social_sign_on",
                      "location=0,status=0,scrollbars=1,width=800,height=500"
              );
              return false;
          });
      });
*/

      jQuery(document).ready(function($) {
          // Change social buttons appearance depending on screen width
            var changeWidth = function(){
            console.log('Resized');
            if ( $(window).width() < 480 ){
                $('#social_connect_hybrid').addClass('btn-group-justified btn-group');
                $('#social_connect_hybrid a').addClass('btn');
              } else {
                $('.btn-group-vertical').removeClass('btn-group-justified btn-group');
                $('#social_connect_hybrid a').removeClass('btn');
              }
            };
            $(window).resize(changeWidth());

          // Search engine buttons
          $('.btn-search-engine').click(function(){
            // $('.btn-search-engine').removeClass('checked');
            $(this).toggleClass('checked');

              var engineName =$(this).data('engine');
            $('.js-stage1-next').removeClass('disabled').html('Rank high on search engines, got it!');
          });

          // Devices buttons
          $('.btn-device').click(function(){
            // $('.btn-device').removeClass('checked');
            $(this).toggleClass('checked');
            $(this).parents('.installer-stage').find('.next-btn').removeClass('disabled').html('Continue <span class="glyphicon glyphicon-chevron-right"></span>');
          });

          // Online store / booking buttons
          $('.btn-add-on').click(function(){
              var $this = $(this);
              if($this.hasClass('btn-uncheck-others') && !$this.hasClass('checked')) {
                $('.btn-add-on').removeClass('checked');
              } else {
                $('.btn-uncheck-others').removeClass('checked');
              }
              $this.toggleClass('checked');
              $this.parents('.installer-stage').find('.next-btn').removeClass('disabled').html('Continue <span class="glyphicon glyphicon-chevron-right"></span>');
          });

          // Devices buttons
          $('.social-btn').click(function(){
              $(this).toggleClass('selected');
          });

          var path_socialmedia_library = "/shared/lib/socialmedia/";
          jQuery('.enable-suggest').each(function(index){
              var wrapper = jQuery(this).parent().parent().parent();
              //jQuery(wrapper).append('<div class="search-results-container" />');
              jQuery(this).on('keyup', function() {
                  var $this = jQuery(this);
                  var searchval = $this.val();
                  //wrapper = jQuery(this).parent();
                  if(searchval.length > 2) {

                      jQuery('.search-results-container', wrapper).html('מחפש...').show();
                      jQuery.get(path_socialmedia_library + jQuery(this).attr('data-suggest-url') +"?format=html&search_box="+searchval, function(data) {
                          jQuery('.search-results-container', wrapper).html(data);
                      });
                  } else {
                      jQuery('.search-results-container', wrapper).html('').hide();
                  }
              });
          });
          jQuery('.search-results-container').on('click', '.media.selectable', function() {
              var value = jQuery(this).attr('data-value');
              var wrapper = jQuery(this).parent().parent().parent().parent();
              jQuery('input', wrapper).val(value);
              jQuery('.search-results-container', wrapper).hide();
          });
      })
    </script>

  </body>
</html>
