<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ignore_user_abort(true);
set_time_limit(0);

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');
$base_dir = __DIR__.'/';


include_once $base_dir . 'config/fb.php';
include_once $base_dir . 'helpers/Curl.php';
include_once $base_dir . 'helpers/Database.php';
include_once $base_dir . 'WhizBizMigrate.php';

$whizObj = new WhizBizMigrate();

if(isset($_GET['type']) && $_GET['type' == 'category']){
    $catgories_data = $whizObj->getJsonCategories();
    $check = $whizObj->addCategories($catgories_data);
}elseif(isset($_GET['type']) && $_GET['type'] == 'location'){
    $cities_data = $whizObj->getJsonCities();
    $check = $whizObj->addCities($cities_data);
}



echo json_encode(
        array(
            'type' => 'success',
            'import' => $_GET['type']
        )
    );

?>
