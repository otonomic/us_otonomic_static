<?php
/**
* Project:     Shopfin
* File:        database.php
*
* Database class. Several useful DB functions
*
* @copyright 2008 Shopfin.com, LLC
* @author Michiel van der Blonk <blonkm@gmail.com>
*/

class Database
{
	public $_conn;   // the opened connection
	public $_lastId; // the last inserted auto-inc value
	public $debug = false; // debug will output SQL statements, but not execute

	const DB_NOW = 'NOW()';
	
	// escape string and add quotes for insertion
	function q($s)
	{
		$this->connect();
		if(get_magic_quotes_gpc())
		  $s = stripslashes($s);
		/* highly debatable since this could all be valid input 
		$badWords = "(delete)|(update)|(union)|(insert)|(drop)|(http)|(--)";
		$string = eregi_replace($badWords, "", $string);
		*/
		if (phpversion() >= '4.3.0')
		  $s = mysqli_real_escape_string($this->_conn,$s);
		else
		  $s = mysqli_escape_string($this->_conn,$s);
		return "'" . $s . "'";
	}
	
	// getters & setters
	function getConn()        { return $this->_conn;    }
	function getLastId()      { return $this->_lastId;    }
	function setLastId($newValue=null) {
    if ($newValue!=null)
      $this->_lastId = $newValue;
    else
      $this->_lastId = mysqli_insert_id();
  }
	
	/**
	* open database connection
	*
	* @access  public
	* @return  int  handle on success, false on fail
	*/
	function connect()
	{
            $basepath = dirname(dirname(__FILE__));
	  require ($basepath.'/config/db.php');
	  $this->_conn = mysqli_connect($config->dbServer, $config->dbUserName, $config->dbPassword)
	    or trigger_error("Cannot connect to the server", E_USER_ERROR);
	  mysqli_select_db($this->_conn,$config->dbName)
	    or trigger_error("Cannot open database", E_USER_ERROR);
      
	  mysqli_query($this->_conn,'SET time_zone="'.APP_TIME_ZONE_DB.'"');
	  return $this->_conn;
	}

	/**
	* close database connection
	*
	* @access  public
	* @return  void
	*/
	function disconnect()
	{
	  if ($this->getConn())
	    mysqli_close($this->_conn);
	}
	
	/**
	* get one record
	*
	* @param   string  $sql SQL query
	* @access  public
	* @return  array   array of columns, or false on fail
	*/
	function getRecord($sql, $className='stdClass')
	{
	  $this->connect();
	  $result = mysqli_query($this->_conn,$sql);
	  if ($result)
	  {
	    $row = mysqli_fetch_object($result);
	    mysqli_free_result($result);
	    $this->disconnect();
	    return $row;
	  }
	  else
	  {
	    $this->disconnect();
	    return false;
	  }
	}
	
	/**
	* get multiple records
	*
	* @param   string  $sql SQL query
	* @access  public
	* @return  array   array of objects, or false on fail
	*/
	function getRecords($sql, $key='', $removeKey = false, $className='stdClass') {
		$rows = Array();
		$this->connect();
		$result = mysqli_query($this->_conn,$sql);
		if ($result){
			while ($row = mysqli_fetch_object($result, $className)) { //echo ++$i."-";
				if ($key==''){
					$rows[] = $row;
				} else {
					$k = $row->$key;
					$rows[$k] = $row;
					if ($removeKey)
						unset($rows[$k]->$key);
				}
			}
			mysqli_free_result($result);
			$this->disconnect();
			return $rows;
		} else {
			$this->disconnect();
			return false;
		}
	}

	/**
	* insert one record
	*
	* @param   string  $table the table to update
	* @param   array	$data the data as key,value pairs
	* @access  public
	* @return  boolean   true on success, false otherwise
	*/
	function insert($table, $data)
	{	
		$result = false;
		$this->connect();
    // fix for empty values or you'll get ',,,'
    // in the query, which fails
		foreach ($data as $key=>$value)
			if (empty($value) && !is_numeric($value))
				$data[$key] = 'NULL';
		$sql = "INSERT INTO $table (@keys) VALUES (@values)";
		$sql = str_replace("@keys", join(',', array_keys($data)), $sql);
		$sql = str_replace("@values", join(',', $data), $sql);
   	 	if ($this->debug)
			echo $sql . '<br/ >';
		else
			$result = mysqli_query($this->_conn,$sql);
 	    if ($result)
			$this->setLastId(mysqli_insert_id($this->_conn));
		
		$this->disconnect();
		return $result; 
	}

	/**
	* update one record
	*
	* @param   string  $table the table to update
	* @param   array	$data the data as key,value pairs
	* @access  public
	* @return  boolean   true on success, false otherwise
	*/
	function update($table, $data, $id='id')
	{	
		$result = false;
		$this->connect();
		$sql = "UPDATE $table SET @pairs WHERE @constraint";
		$pairs = array();
		foreach ($data as $key=>$value){
			if (is_null($value))
				$pairs[$key] = $key . ' = NULL';
			else
				$pairs[$key] = $key . '=' . $value;
		}

		if(is_array($id)){
			foreach($id as $w){
				$ws[] = $pairs[$w];
			}
			$sql = str_replace("@constraint", implode(' AND ',$ws), $sql);
		} else {
			$sql = str_replace("@constraint", $pairs[$id], $sql);
			unset($pairs[$id]);
		}

		$sql = str_replace("@pairs", join(',', $pairs), $sql);
	    if ($this->debug)
			echo $sql . '<br/>';
		else
			$result = mysqli_query($this->_conn,$sql);

		$this->disconnect();
		return $result; 
	}

	/**
	* delete one record
	*
	* @param   string  $table the table to delete record from
	* @param   array	$data the data as key,value pairs
	* @access  public
	* @return  boolean   true on success, false otherwise
	*/
	function delete($table, $data, $id='id')
	{	
		$result = false;
		$this->connect();
		$sql = "DELETE FROM $table WHERE @constraint";
		$pairs = array();
		foreach ($data as $key=>$value)
			$pairs[$key] = $key . '=' . $value;
		$sql = str_replace("@constraint", $pairs[$id], $sql);
		if ($this->debug)
			echo $sql . '<br />';
		else
		$result = mysqli_query($this->_conn,$sql);
		$this->disconnect();
		return $result; 
	}
 
	// just do a simple query
 	function doQuery($sql) {
		$result = false;
		$this->connect();
		if ($this->debug)
			echo $sql . '<br />';
		else
			$result = mysqli_query($this->_conn,$sql);
		
		$this->disconnect();
		return $result; 
	}
}
?>