<?php
class Page2site {
    /**
     * Generate password,
     * composed of digits of letters.
     *
     * @param integer $length Length of password
     * @return string $password Password
     */
    /**
     * Password generator function
     *
     */
    public static function generatePassword($length = 8){
        // inicializa variables
        $password = "";
        $i = 0;
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        // agrega random
        while ($i < $length){
            $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        return $password;
    }

    public static function utf8_urldecode($str) {
        $str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
        return html_entity_decode($str,null,'UTF-8');;
    }

    /**
     * Helper function to parse array with two keys-value pairs into an associate array
     *
     * For example, running:
     * 		$input = array(
     * 			array('id' => 'apple', 'name' => 'foo'),
     * 			array('id' => 'banana', 'name' => 'bar')
     * 		);
     * 		$result = processArrayToAssicateArray($input, 'id', 'name');
     *
     * Will return:
     * 		$result = array(
     * 			'apple' => 'foo',
     * 			'banana' => 'bar'
     * 		);
     *
     * @param array $rows Input array
     * @param string $element View element with html template for message
     * @param array $options Message options, e.g. class
     * @return array Resulting associate array
     */
    public static function processArrayToAssociateArray($rows=null, $key, $value) {
        if(empty($rows)) {
            return array();
        }
        $result = array();

        foreach($rows as $row) {
            $result[$row[$key]] = $row[$value];
        }
        return $result;
    }

    /**
     * Fetch first occurence of numbers from string
     *
     * @param string $string Input string
     * @return string String with contained numbers
     */
    public static function getNumberFromString($string) {
        $pattern = "/\/([0-9]+[^a-zA-Z?&])/";

        if(preg_match($pattern, $string, $matches) > 0) {
            return $matches[1];
        }
        return '';

        // Alternative code - not tested
        //return preg_replace("(\/[0-9]+)", '', $string); // ditch anything that is not a number
    }

    public static function cleanSiteAddress($url)
    {
        if($result = Page2site::getNumberFromString($url))
        {
            return $result;
        }

        $pattern = '/facebook.com\/([a-zA-Z0-9\/._-]*)/i';


        if(preg_match($pattern, $url, $matches))
        {
            $result = array_pop($matches);

        }
        else
        {
            $pattern = '/(\/)?([a-zA-Z0-9\/._-]*)/';
            if(preg_match($pattern, $url, $matches))
            {
                $result = array_pop($matches);
            }
        }

	    $pattern = '((?:[a-z][a-z0-9_]*))(\\/)';
	    if ($c=preg_match_all ("/".$pattern."/is", $result, $matches))
	    {
		    $result=$matches[1][0];
	    }

        return trim($result, '/,- ');
    }

    public static function removeAccents($str) {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return str_replace($a, $b, $str);
    }





    public static function setEventAnalytics($data, $type='event') {
        $data = array_merge(
            array('type'=>$type, 'category'=>'', 'event'=>'', 'label'=>'', 'value'=>''),
            $data
        );
        $_SESSION['GoogleAnalytics'][] = $data;
        return true;
    }

    public static function clearEventAnalytics() {
        $_SESSION['GoogleAnalytics'] = null;
    }



    public static function getFlashMessage($clear = true) {
        if(empty($_SESSION['Message']['flash'])) {
            return array('message'=>'', 'element'=>'default', 'params'=>[]);
        }
        $result = $_SESSION['Message']['flash']['message'];
        if($clear) { $_SESSION['Message'] = []; }
        return $result;
    }

    public static function setFlashMessage($data) {
        $_SESSION['Message']['flash']['message'] .= "<br/>".$data;
        $_SESSION['Message']['flash']['element'] .= "default";
        return true;
    }

    public static function clearFlashMessage() {
        $_SESSION['Message'] = null;
    }

    public static function getEventAnalytics($clear = true) {
        if(empty($_SESSION['GoogleAnalytics'])) { return array(); }
        $result = $_SESSION['GoogleAnalytics'];
        if($clear) { $_SESSION['GoogleAnalytics'] = null; }
        return $result;
    }





    public static function parseTidy($content, $config = array(), $encode = 'UTF8'){
        if(trim($content) == ''){
            return $content;
        }

        $config = array_merge(
            array('indent' => FALSE,
                'output-xhtml' => FALSE,
                'wrap' => 200,
                'show-body-only'=>true), $config);
        $tidy = new tidy;
        $tidy->parseString($content, $config, $encode);
        $tidy->cleanRepair();

        if(isset($tidy->value) && $tidy->value != ''){
            return $tidy->value;
        }

        return false;
    }

    public static function objectToArray($d) {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
             * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map('self::objectToArray', $d);
        }
        else {
            // Return array
            return $d;
        }
    }

    public static function getSiteAddress($data, $is_allow_custom_domain = true) {
        if(!is_array($data)) {
            return false;
        }
        if(empty($data['id'])) { return '';}

        if(LOCALHOST) {
            // return PAGE2SITE_FOLDER.'sites/home/'.$data['id'];
            return URL_BUILDER.'/sites/home/'.$data['id'];
        }
        if($data['domain'] && $is_allow_custom_domain) {
            return 'http://'.$data['domain'];
        }
        if($data['slug']) {
            return 'http://'.$data['slug'].'.'.MAIN_DOMAIN;
        }

        return URL_BUILDER.'/sites/home/'.$data['id'];
    }

    public static function getFacebookProfilePicture($facebook_id, $size = 'square', $width = null, $height = null) {
        // Options are small, normal, large, square
        $result = 'http://graph.facebook.com/'.$facebook_id.'/picture?type='.$size;
        if($width) { $result .= '&width='.$width; }
        if($height) { $result .= '&height='.$height; }
        return $result;
    }

    public static function getFacebookProfilePictureFromFacebookUrl($facebook_url) {
        return str_replace('www.facebook.com','graph.facebook.com', trim($facebook_url,'/')).'/picture/';
    }

    public static function getFacebookUrl($facebook_id) {
        return 'http://www.facebook.com/'.$facebook_id;
    }

    public static function get1DimensionArray($twoDimensionArray, $use_key_prefix = true){
        $oneDimensionArray = array();
        foreach ($twoDimensionArray as $key1 => $val1){
            foreach ($val1 as $key2 => $val2){
                if($use_key_prefix && $key1 != '0') {
                    $oneDimensionArray[$key1 . "_" . $key2] = $val2;
                } else {
                    $oneDimensionArray[$key2] = $val2;
                }
            }
        }
        return $oneDimensionArray;
    }

/*
    public static function encrypt($plaintext, $encryption_key = null){
        if(is_null($encryption_key)) {
            $encryption_key = SECURITY_KEY;
        }

        return Security::rijndael($plaintext, $encryption_key, 'encrypt');

        $key = pack('H*', $encryption_key);

        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        # use an explicit encoding for the plain text
        $plaintext_utf8 = utf8_encode($plaintext);

        # creates a cipher text compatible with AES (Rijndael block size = 128)
        # to keep the text confidential
        # only suitable for encoded input that never ends with value 00h
        # (because of default zero padding)
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext_utf8, MCRYPT_MODE_CBC, $iv);

        # prepend the IV for it to be available for decryption
        $ciphertext = $iv . $ciphertext;

        # encode the resulting cipher text so it can be represented by a string
        return base64_encode($ciphertext);
    }

    public static function decrypt($ciphertext_base64, $encryption_key = null){
        if(is_null($encryption_key)) {
            $encryption_key = Configure::read("Security.key");
        }

        return Security::rijndael($ciphertext_base64, $encryption_key, 'decrypt');

        $key = pack('H*', $encryption_key);

        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $ciphertext_dec = base64_decode($ciphertext_base64);

        # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
        $iv_dec = substr($ciphertext_dec, 0, $iv_size);

        # retrieves the cipher text (everything except the $iv_size in the front)
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);

        # may remove 00h valued characters from end of plain text
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec),chr(0));
    }
*/

    // Build an URL
    // The parts of the second URL will be merged into the first according to the flags argument.
    //
    // This function modified version of pecl::http_build_url
    // @param   mixed           (Part(s) of) an URL in form of a string or associative array like parse_url() returns
    // @param   mixed           Same as the first argument
    // @param   int             A bitmask of binary or'ed HTTP_URL constants (Optional)HTTP_URL_REPLACE is the default
    // @param   array           If set, it will be filled with the parts of the composed url like parse_url() would return
    public static function http_build_url($url, $parts=array(), $flags=1, &$new_url=false)
    {
        if (!function_exists('http_build_url')){
            define('HTTP_URL_REPLACE', 1);              // Replace every part of the first URL when there's one of the second URL
            define('HTTP_URL_JOIN_PATH', 2);            // Join relative paths
            define('HTTP_URL_JOIN_QUERY', 4);           // Join query strings
            define('HTTP_URL_STRIP_USER', 8);           // Strip any user authentication information
            define('HTTP_URL_STRIP_PASS', 16);          // Strip any password authentication information
            define('HTTP_URL_STRIP_AUTH', 32);          // Strip any authentication information
            define('HTTP_URL_STRIP_PORT', 64);          // Strip explicit port numbers
            define('HTTP_URL_STRIP_PATH', 128);         // Strip complete path
            define('HTTP_URL_STRIP_QUERY', 256);        // Strip query string
            define('HTTP_URL_STRIP_FRAGMENT', 512);     // Strip any fragments (#identifier)
            define('HTTP_URL_STRIP_ALL', 1024);         // Strip anything but scheme and host
        }

        $keys = array('user','pass','port','path','query','fragment');

        // HTTP_URL_STRIP_ALL becomes all the HTTP_URL_STRIP_Xs
        if ($flags & HTTP_URL_STRIP_ALL)
        {
            $flags |= HTTP_URL_STRIP_USER;
            $flags |= HTTP_URL_STRIP_PASS;
            $flags |= HTTP_URL_STRIP_PORT;
            $flags |= HTTP_URL_STRIP_PATH;
            $flags |= HTTP_URL_STRIP_QUERY;
            $flags |= HTTP_URL_STRIP_FRAGMENT;
        }
        // HTTP_URL_STRIP_AUTH becomes HTTP_URL_STRIP_USER and HTTP_URL_STRIP_PASS
        else if ($flags & HTTP_URL_STRIP_AUTH)
        {
            $flags |= HTTP_URL_STRIP_USER;
            $flags |= HTTP_URL_STRIP_PASS;
        }

        // Parse the original URLpar
        $parse_url = parse_url($url);

        // Scheme and Host are always replaced
        if (isset($parts['scheme']))
            $parse_url['scheme'] = $parts['scheme'];
        if (isset($parts['host']))
            $parse_url['host'] = $parts['host'];

        // (If applicable) Replace the original URL with it's new parts
        if ($flags & HTTP_URL_REPLACE)
        {
            foreach ($keys as $key)
            {
                if (isset($parts[$key]))
                    $parse_url[$key] = $parts[$key];
            }
        }
        else
        {
            // Join the original URL path with the new path
            if (isset($parts['path']) && ($flags & HTTP_URL_JOIN_PATH))
            {
                if (isset($parse_url['path']))
                    $parse_url['path'] = rtrim(str_replace(basename($parse_url['path']), '', $parse_url['path']), '/') . '/' . ltrim($parts['path'], '/');
                else
                    $parse_url['path'] = $parts['path'];
            }

            // Join the original query string with the new query string
            if (isset($parts['query']) && ($flags & HTTP_URL_JOIN_QUERY))
            {
                if (isset($parse_url['query']))
                    $parse_url['query'] .= '&' . $parts['query'];
                else
                    $parse_url['query'] = $parts['query'];
            }
        }

        // Strips all the applicable sections of the URL
        // Note: Scheme and Host are never stripped
        foreach ($keys as $key)
        {
            if ($flags & (int)constant('HTTP_URL_STRIP_' . strtoupper($key)))
                unset($parse_url[$key]);
        }


        $new_url = $parse_url;

        return
            ((isset($parse_url['scheme'])) ? $parse_url['scheme'] . ':' : '')
            .((isset($parse_url['scheme']) || isset($parse_url['user']) || isset($parse_url['host'])) ? '//' : '')
            .((isset($parse_url['user'])) ? $parse_url['user'] . ((isset($parse_url['pass'])) ? ':' . $parse_url['pass'] : '') .'@' : '')
            .((isset($parse_url['host'])) ? $parse_url['host'] : '')
            .((isset($parse_url['port'])) ? ':' . $parse_url['port'] : '')
            .((isset($parse_url['path'])) ? $parse_url['path'] : '')
            .((isset($parse_url['query'])) ? '?' . $parse_url['query'] : '')
            .((isset($parse_url['fragment'])) ? '#' . $parse_url['fragment'] : '')
            ;
    }

    public static function sanitizeSqlString($str = '') {
        if($str) {
            $str = str_replace(';', ' ', $str);
            $str = str_ireplace('delete', ' ', $str);
            $str = str_ireplace('update', ' ', $str);
            $str = str_ireplace('drop', ' ', $str);
        } else {
            $str = '1 = 1';
        }

        return $str;
    }

    public static function clientIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function readCSVFile($filename){
        $result = array();
        $csv_data = array();
        $delim = ','; $enc = '"';$line = "\n";
        $csv_file = str_getcsv ( file_get_contents( $filename ), $line );
        foreach( $csv_file as $row ){
            $csv_data[] = str_getcsv( $row, $delim, $enc );
        }

        $header = array_shift($csv_data);
        $result['header'] = $header;
        foreach($csv_data as $key=>$value){
            $temp = array();
            foreach($value as $k=>$v){
                $temp[str_replace(' ', '_', strtolower($header[$k]))] = $v;
            }
            $result['data'][] = $temp;
        }
        return $result;
    }

    public static function addFacebookProfilePicturesToArray(&$rows) {
        foreach ($rows as &$row) {
            $row['Site']['picture'] = self::getFacebookProfilePicture($row['Site']['page_id']);
        }
        return true;
    }

    public static function getUserEmailArrayForToField($user) {
        if(isset($user['User'])) { $user = $user['User']; }

        if(!empty($user['firstname'])) {
            $username = $user['firstname'];
        } else {
            $username = $user['username'];
        }

        return array(
            'email' => $user['email'],
            'name' => $username,
            'id' => $user['id']
        );
    }

    public static function getDomain() {
        if(!empty($_SERVER['HTTP_HOST'])) {
            return $_SERVER['HTTP_HOST'];
        } else {
            return $_SERVER['SERVER_NAME'];
        }
    }

    public static function getCurrentUrl() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public static function getReferer() {
        return isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : null;
    }

    /**
     * Function: sanitize
     * Returns a sanitized string, typically for URLs.
     *
     * Parameters:
     *     $string - The string to sanitize.
     *     $force_lowercase - Force the string to lowercase?
     *     $anal - If set to *true*, will remove all non-alphanumeric characters.
     */
    public static function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "Ã¢â‚¬â€�", "Ã¢â‚¬â€œ", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    public static function setAbTest($test_name, $ga_slot = '1') {
        $trial_mode = false;
        if(isset($_GET['abtest_trial_mode'])) { $trial_mode = true; }
        $test = new phpab($test_name, $trial_mode);
        $test->set_ga_slot(''.$ga_slot);
        return $test;
    }

    public static function trackAbVariation($test, $ga_slot = '1') {
        $trial_mode = false;
        if(isset($_GET['abtest_trial_mode'])) { $trial_mode = true; }
        if(!$trial_mode) { /*return ''; */}

        $test_name = $test->get_test_name();
        return "<script>
                jQuery(document).ready(function($) {
                    p2sTrack('AB_test', 'Variation selected', '{$test_name} >> {$test->get_user_segment()}');
                    trackEvent('AB_test', 'Variation selected', '{$test_name} >> {$test->get_user_segment()}');
                });
                </script>
        ";
    }

    public static function addURLParameters($input_url, $parameters = array()) {
        $url_data = parse_url($input_url);
        if(!isset($url_data["query"]))
            $url_data["query"]="";

        $params = array();
        parse_str($url_data['query'], $params);
        foreach($parameters as $key => $value){
            $params[$key] = $value;
        }
        $url_data['query'] = http_build_query($params);
        $url="";
        if(isset($url_data['host']))
        {
            $url .= $url_data['scheme'] . '://';
            if (isset($url_data['user'])) {
                $url .= $url_data['user'];
                if (isset($url_data['pass'])) {
                    $url .= ':' . $url_data['pass'];
                }
                $url .= '@';
            }
            $url .= $url_data['host'];
            if (isset($url_data['port'])) {
                $url .= ':' . $url_data['port'];
            }
        }
        $url .= $url_data['path'];
        if (isset($url_data['query'])) {
            $url .= '?' . $url_data['query'];
        }
        if (isset($url_data['fragment'])) {
            $url .= '#' . $url_data['fragment'];
        }
        return $url;
    }

    static function getRoundPriceAndFraction($in) {
        $result = [
            'round' => floor($in),
            'fraction' => round(fmod($in,1)*100)
        ];
        if(strval($result['fraction'])=='0') {
            $result['fraction'] = '00';
        }
        return $result;
    }

    public static function slugify($name)
    {
        /* Remove special characters */
        $alias=strtolower($name);
        $old_pattern = array("/[^a-zA-Z0-9\/]/", "/_+/", "/_$/");
        $new_pattern = array("-", "-", "");
        $alias = strtolower(preg_replace($old_pattern, $new_pattern , $alias));
        return $alias;
    }

    public static function slugify2($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv'))
        {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
            return false;
        }

        return $text;
    }
}




class Configure {
    static $values = [];

    static function init() {
        self::write('Plimus.action_url', "https://www.plimus.com/jsp/buynow.jsp");
        self::write('Plans.Basic.PlimusContractID.monthly', '2327127');
        self::write('Plans.Basic.PlimusContractID.yearly', '2327121');
        self::write('Plans.Business.PlimusContractID.monthly', '2327131');
        self::write('Plans.Business.PlimusContractID.yearly', '2327123');
        self::write('Plans.VIP.PlimusContractID.monthly', '2327133');
        self::write('Plans.VIP.PlimusContractID.yearly', '2327125');

        self::write('Plans.Basic.cost.monthly', '4.9');
        self::write('Plans.Basic.cost.yearly', ['year'=>'48', 'month'=>'4']);
        self::write('Plans.Business.cost.monthly', '9.9');
        self::write('Plans.Business.cost.yearly', ['year'=>'96', 'month'=>'8']);
        self::write('Plans.VIP.cost.monthly', '35.9');
        self::write('Plans.VIP.cost.yearly', ['year'=>'359.9', 'month'=>'29.9']);

    }

    public static function read($key) {
        if( isset(self::$values[$key])) {
            return self::$values[$key];
        }

        return null;
    }

    public static function write($key, $value) {
        return self::$values[$key] = $value;
    }
}

Configure::init();
?>