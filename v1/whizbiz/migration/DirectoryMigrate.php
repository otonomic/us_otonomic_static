<?php
class DirectoryMigrate {
	private $blog_id;
	private $user_id = 0;
    public $slug;

	public $page;

    private $theme;

	public function __construct($identifier = '', $theme = '') {
        if($identifier) {
            $this->page = $this->fetch_page_data($identifier);
//            echo "<pre>";
//            print_r($this->page);
            // $this->slug = Page2site::slugify( $this->page->basic->slug);
            // TODO: Rewrite slugify2 so it works on Hebrew pages as well
            $this->slug = Page2site::slugify2( $this->page->basic->slug);
            if( !$this->slug) { $this->slug = 's'.$this->page->basic->id; }
        }
    }

    function add_listing() {
        $check = $this->check_listing_exists();
        if($check) {
            return false;
        }
        $listing_id = $this->create_listing();
	    //$this->set_featured_image($listing_id, $this->page->facebook->picture->data->url);
	    $this->set_featured_image($listing_id, 'http://graph.facebook.com/'.$this->page->facebook->id.'/picture?type=large');
        $this->create_listing_options($listing_id);
        $this->add_listing_category($listing_id);
        $this->add_listing_reviews($listing_id);
        $this->add_listing_images($listing_id);
        return $listing_id;
    }

    private function fetch_page_data($fb_id)
    {
        $page = self::download_page_data($fb_id);
        return $this->parse_page_data($page);
    }


    private static function download_page_data($fb_id)
    {

        $fb_name_identifier = Page2site::cleanSiteAddress($fb_id);

        $facebook_basic_url = "https://graph.facebook.com/". $fb_name_identifier ."?access_token=". FB_TOKEN."&fields=id,username,name,link,picture,likes,talking_about_count,cover,website,phone,location,company_overview,general_info,personal_info,mission,personal_interests,about,description,bio,genre,founded,band_members,members,general_manager,hometown,current_location,band_interests,hours,category,category_list";

        $facebook_email_url = "http://otodirectory.test/migration/fb_email.php?page_id=" . $fb_name_identifier;

        $posts_url = "http://builder.otonomic.com/resource/fb_id:". $fb_name_identifier ."/media:facebook/resources:post";

        $testimonials_url = "http://wp.otonomic.com/migration/helpers/FacebookReviews.php?url=".urlencode("https://m.facebook.com/page/reviews.php?id=". $fb_name_identifier);
        
        //$albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields(id,name,link,cover_photo,type,description,count,photos.fields(id,picture,source,images,caption,modified)";
        $albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields=id,name,link,cover_photo,type,description,count,photos.fields(id,picture,images,source,caption,link,width,height,created_time,updated_time,name)";

        $videos_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/videos?access_token=". USER_TOKEN;

        $notes_url = "https://graph.facebook.com/v1.0/". $fb_name_identifier ."/notes?access_token=". FB_TOKEN;

        $respond = Curl::multiRequest(array($facebook_basic_url, $facebook_email_url, $testimonials_url));
        
        if(empty($respond[0])) {
            $respond[0] = file_get_contents($facebook_basic_url);
        }

        foreach($respond as $key=>$value):
            $respond[$key] = json_decode($value);
        endforeach;


        $page = new stdClass();
        $page->facebook = $respond[0];

        $page->facebook->email = $respond[1]->result;

//        $page->posts = $respond[1]->data->post;
        $page->testimonials = $respond[2]->facebook_reviews;
//        $page->albums = isset($respond[3]->data) ? $respond[3]->data : null;
//        $page->videos = isset($respond[4]->data) ? $respond[4]->data : null;
//        $page->notes = isset($respond[5]->data) ? $respond[5]->data : [];

//        $page->posts = self::parse_posts($page);
//        $page->albums = self::parse_albums($page);
        
        if( isset($page->facebook->error)) {
            self::email_admin_about_facebook_graph_error();
        }
        return $page;
    }

	private static function email_admin_about_facebook_graph_error()
	{
		return false;
	}
    
    public function check_listing_exists(){
        global $wpdb;
        $sql = "select count(*) count from wp_postmeta where meta_key = 'facebook_id' and meta_value ='".$this->page->facebook->id."'";
        $data = $wpdb->get_results($sql);
        if($data[0]->count > 0){
            return true;
        } else{
            return false;
        }
    }
    
    public function create_listing(){
        global $wpdb;
        $page = $this->page->facebook;

	    $post_date = date('Y-m-d H:i:s');

        $listing = array(
            'post_title' => $page->name,
            'post_content' => $this->get_listing_content(),
            'post_name'=> wp_unique_post_slug(sanitize_title_with_dashes($page->name), 0, 'publish', 'listing', 0),
            'post_status' => 'publish',
            'comment_status' => 'open',
            'ping_status' => 'open',
            'post_type' => 'listing',
        );

        if($created_time = $page->created_time){
            $created_date = $this->parse_date($created_time);
        } else {
            $created_date = $this->parse_date($post_date);
        }
        $modified_date = $this->parse_date($created_date);
        $listing['post_date'] = $created_date;
        $listing['post_date_gmt'] = $created_date;
        $listing['post_modified'] = $modified_date;
        $listing['post_modified_gmt'] = $modified_date;

        $insert = $wpdb->insert( 'wp_posts', $listing );
        $listing_id = $wpdb->insert_id;
        
        return $listing_id;
    }
    
    function get_listing_content() {
        return $this->page->facebook->about." <br> ".$this->page->facebook->description;
    }
    
    
    public function create_listing_options($listing_id){
        global $wpdb;
        $page = $this->page->facebook;
        
        $city_id = 0;
        $state_id = 0;
        $country_id = 0;
        
        $country_data = $this->get_country_by_name($page->location->country);
        if($country_data){
            $country_id = $country_data->country_id;
        }
        
        $state_data = $this->get_state_by_code($page->location->state, $country_id);
        if($state_data){
            $state_id = $state_data->zones_id;
        }
        
        $city_data = $this->get_city_by_name($page->location->city,$state_id,$country_id);
        if($city_data){
            $city_id = $city_data->city_id;
        }
        
        $options = array(
            'address' => $this->page->basic->address,
            'geo_latitude' => $page->location->latitude,
            'geo_longitude' => $page->location->longitude,
            'facebook' => $page->link,
            'website' => $page->website,
            'country_id' => $country_id,
            'zones_id' => $state_id,
            'post_city_id' => $city_id,
            'facebook_id' => $page->id,
            'phone' => $page->phone,
            'email' => $page->email,
            'listing_timing' => nl2br($page->hours['str'])
        );
        otonomic_insert_postmeta($listing_id, $options);
        /*
        foreach($options as $key => $value) {
            add_post_meta($listing_id, $key, $value);
        }
        */
    }
	public function set_featured_image($listing_id, $image_url)
	{
		$thumbid = $this->add_remote_image($listing_id, $image_url) ;
		set_post_thumbnail( $listing_id, $thumbid );
		$options = array(
			'listing_logo' => $image_url,
		);
		otonomic_insert_postmeta($listing_id, $options);
		return true;
	}
	public function add_remote_image($listing_id, $image_url)
	{
		if(!$listing_id) {
			$listing_id = 0;
		}

		if(empty($image_url)) {
			return false;
		}

		$image_post = array(
			'post_name'      => $image_url,
			'post_content'   => $image_url,
			'post_excerpt'   => '',
			'post_parent'    => $listing_id,
			'post_status'    => 'inherit',
			'post_type' 	 => 'attachment',
			'post_mime_type' => 'image/jpeg',
			'guid'           => $image_url,
//            'filter'        => true
		);
		$image_id = otonomic_insert_post( $image_post);
		// insert meta for the photos
		$imagemeta = array(
			'file' => $image_url,
			'sizes' => array
			(
				'thumbnail' => array
				(
					'file' => $image_url,
					'mime-type' => 'image/jpeg'
				),
				'medium' => array
				(
					'file'=> $image_url,
					'mime-type' => 'image/jpeg'
				),
				'large' => array
				(
					'file' => $image_url,
					'mime-type' => 'image/jpeg'
				)
			));

		$post_meta = [
			'_wp_attached_file' => $image_url,
			'_wp_attachment_metadata' => $imagemeta,
			'external_link' => $image_url
		];

		$img_atts = [];
		$post_meta['attributes'] = $img_atts;

		otonomic_insert_postmeta( $image_id, $post_meta);

		return $image_id;
	}

    public function add_listing_reviews($listing_id){
        global $wpdb;
        $reviews = $this->page->testimonials;
        if($reviews){
            $review_count = 0;
            $ratings_count = 0;
            $total_rating = 0;
            $post_date = date('Y-m-d H:i:s');
            foreach ($reviews as $review) {
                if(!$review->text){
                    continue;
                }
                $insert_review = array(
                    'comment_post_ID' => $listing_id,
                    'comment_author' => $review->user_name,
                    'comment_content' => $review->text,
                    'comment_approved' => 1,
                    'comment_author_url' => $review->user_link,
                    'comment_date' => $post_date,
                    'comment_date_gmt' => $post_date,
                    'user_id' => 9999
                );
                $wpdb->insert('wp_comments',$insert_review);
                $review_id = $wpdb->insert_id;
                $review_count++;

                if($review->rating){
                    $insert_rating = array(
                        'rating_postid' => $listing_id,
                        'rating_rating' => $review->rating,
                        'comment_id' => $review_id
                    );
                    $wpdb->insert('wp_ratings',$insert_rating);
                    $ratings_count++;
                    $total_rating += $review->rating;
                }
                
                $comment_meta = array(
                    'user_picture'=>$review->user_picture
                        );
                otonomic_insert_commentmeta($review_id, $comment_meta);
            }
            
            if($review_count > 0){
                $update_listing = array(
                    'comment_count' => $review_count
                );
                $where_listing = array(
                    'ID' => $listing_id
                );
                $wpdb->update( 'wp_posts', $update_listing, $where_listing);
            }
            
            if($ratings_count > 0){
                $average_rating = $total_rating/$ratings_count;
            }else{
                $average_rating = 0;
            }
            $update_listing_options = array(
                    'average_rating' => $average_rating,
                    'rating_count' => $ratings_count
                );
            otonomic_insert_postmeta($listing_id, $update_listing_options);
        }
    }
    
    public function add_listing_category($listing_id){
        global $wpdb;
//        $page = $this->page->basic->category;
        
        $prepare_query = "";
        $categories = $this->get_listing_category_terms();
        $i = 1;
        foreach ($categories as $category) {
            if(!$category){
                continue;
            }
            if($i == 1){
                $or = "";
            }else{
                $or = " or ";
            }
            $prepare_query .= $or."wp_terms.name like '%".$category."%'";
            $i++;
        }
        $sql = "SELECT *  FROM wp_terms join wp_term_taxonomy on wp_terms.term_id = wp_term_taxonomy.term_id where ".$prepare_query;
        $category_ids = $wpdb->get_results($sql);
        $cats_assigned = array();
        $parent_cats = array();
        if($category_ids){
            foreach($category_ids as $category_id){
                if($category_id->taxonomy != 'listingcategory'){
                    continue;
                }
                $this->add_term_relationship($listing_id,$category_id->term_id);
                $cats_assigned[] = $category_id->term_id;
                
                if($category_id->parent > 0 && !in_array($category_id->parent, $cats_assigned)){
                    $this->add_term_relationship($listing_id,$category_id->parent);
                    $cats_assigned[] = $category_id->parent;
                }
            }
        }
    }
    
    public function add_term_relationship($listing_id,$taxonomy_id){
        global $wpdb;
        $insert_data = array(
            'object_id' => $listing_id,
            'term_taxonomy_id' => $taxonomy_id
        );
        $wpdb->insert('wp_term_relationships',$insert_data);
    }
    
    // Currently inserting only cover image
    public function add_listing_images($listing_id){
        $photo = $this->page->facebook->cover;
        $my_post = array(
            'post_name'      => $photo->id,
            'post_content'   => '',
            'post_excerpt'   => '',
            'post_parent'    => $listing_id,
            'post_status'    => 'inherit',
            'post_type' 	 => 'attachment',
            'post_mime_type' => 'image/jpeg',
            'guid'           => $photo->source,
        );
        $post_id = wp_insert_post($my_post);
        // insert meta for the photos
        $imagemeta = array(
            'file' => $photo->source,
            'sizes' => array
            (
                'thumbnail' => array
                (
                    'file' => $photo->source,
                    'mime-type' => 'image/jpeg'
                ),
                'medium' => array
                (
                    'file'=> $photo->source,
                    'mime-type' => 'image/jpeg'
                ),
                'large' => array
                (
                    'file' => $photo->source,
                    'mime-type' => 'image/jpeg'
                )
            ));
        add_post_meta($post_id, '_wp_attached_file', $photo->source);
        add_post_meta($post_id,'_wp_attachment_metadata', $imagemeta);
    }
    
    public function get_listing_category_terms(){
        $page_data = $this->page->basic;
        $categories = explode('/', $page_data->category);
        
        $categories_other = json_decode($page_data->category_list,TRUE);
        foreach($categories_other as $cat){
            $categories[] = $cat['name'];
        }
        return $categories;
    }
















    function ping_search_engines()
    {
        $sitemapurl = urlencode( home_url( 'sitemap_index.xml' ) );

        if(!WP_DEBUG && !LOCALHOST) {
            wp_remote_get( 'http://www.google.com/webmasters/tools/ping?sitemap=' . $sitemapurl );
            wp_remote_get( 'http://www.bing.com/ping?sitemap=' . $sitemapurl );
        }
    }

    function schedule_crons()
    {
        $OtonomicCrons = OtonomicCrons::get_instance();
        $Otonomic_First_Session = Otonomic_First_Session::get_instance();

        $date_after_12_days = date('Y-m-d H:i:s', strtotime("+12 days"));

        $OtonomicCrons->AddCron('populate-suggested-domain', 'SITE_CREATION', array($Otonomic_First_Session, "save_suggested_domains"), OtonomicCrons::ONCE);
        $OtonomicCrons->AddCron('12-days-since-site-created', 'SITE_CREATION', array('otonomic_transactional_email', 'email_12_days_since_site_created'), OtonomicCrons::ONCE,null,$date_after_12_days);

    }

    function regenerate_thumbs()
    {
        $OtonomicTheme = OtonomicTheme::get_instance();
        $OtonomicTheme->regenerate_thumbnails();
    }

    function get_ip_data($ip) {
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        if($query && $query['status'] == 'success') {
            return json_encode($query);
        } else {
            return '';
        }
    }

    function get_site_address() {
        if(SUBDOMAIN_INSTALL) {
            return $this->slug .".". DOMAIN_FOR_SITE_CREATION;

        } else {
            return DOMAIN_FOR_SITE_CREATION . '/' . $this->slug;
        }
    }

    function get_user_id() {
        return $this->user_id;
    }

    function get_token()
    {
        $token = md5($this->page->facebook->id.sha1(SALT));
        return $token;
    }





//class GeolocationParser
    public function add_cities($cities,$state_type="state"){
        $countries = array();
        $states = array();
        $count_inserted = 0;
        $count_updated = 0;
        $country_skip = 0;
        $state_skip = 0;
        global $wpdb;
        
        foreach($cities as $city_key => $city_data){
            $city = $city_data['city'];
            if($city == "City"){
                continue;
            }
            
            $state_id = 0;
            $country_id = 0;
            $state = $city_data['state'];
            $country = $city_data['country'];
            
            if(in_array($country, $countries)){
                $country_id = array_search($country, $countries);
            }else{
                $country_data = $this->get_country_by_name($country);
                    if($country_data){
                        $country_id = $country_data->country_id;
                        $countries[$country_id] = $country;
                    }
            }
            
            if(in_array($state, $states)){
                $state_id = array_search($state, $states);
            }else{
                if($state_type == 'code'){
                    $state_data = $this->get_state_by_code($state,$country_id);
                }else{
                    $state_data = $this->get_state_by_name($state);
                }
                if($state_data){
                    $state_id = $state_data->zones_id;
                    $states[$state_id] = $state;
                }
            }
            
            $is_city = $this->get_city_by_name($city,$state_id,$country_id);
            
            if(!$is_city){
                if($country_id && $state_id){
                    $insert_record = array(
                        'country_id' => $country_id,
                        'zones_id' => $state_id,
                        'cityname' => $city,
                        'city_slug' => sanitize_title_with_dashes($city),
                        'lat' => $city_data['latitude'],
                        'lng' => $city_data['longitude'],
                        'scall_factor' => 13,
                        'map_type' => 'ROADMAP',
                        'post_type' => 'listing',
                        'categories' => 'all,',
                        'is_default' => 0
                    );
                    $count_inserted++;
                    $this->add_city($insert_record);
                }else{
                    if(!$country_id){
                        $country_skip++;
                    }elseif(!$state_id){
                        $state_skip++;
                    }
                }
                
            }else{
                $update_rec = array(
                    'lat' => $city_data['latitude'],
                    'lng' => $city_data['longitude']
                );
                $where = array(
                    'city_id' => $is_city->city_id
                );
                $wpdb->update( 'wp_multicity', $update_rec, $where);
                $count_updated++;
            }
        }
        
        $result = "Total citites - ".count($cities);
        $result .= "<br> Cities inserted - ".$count_inserted;
        $result .= "<br> Cities Updated - ".$count_updated;
        $result .= "<br> Cities skipped (States) - ".$state_skip;
        $result .= "<br> Cities skipped (countries) - ".$country_skip;
        return $result;
    }
    
    public function add_city($city){
        global $wpdb;
        $wpdb->insert( 'wp_multicity', $city );
        return TRUE;
    }
    
    public function get_city_by_name($city,$state_id,$country_id){
        global $wpdb;
        $sql = "select * from wp_multicity where country_id = '".$country_id."' and zones_id = '".$state_id."' and cityname like '".$city."'";
        $data = $wpdb->get_row($sql);
        return $data;
    }
    
    public function get_country_by_name($country){
        global $wpdb;
        $country_sql = "select * from wp_countries where country_name like '".$country."'";
        $country_data = $wpdb->get_row($country_sql);
        return $country_data;
    }
    
    public function get_all_countries(){
        global $wpdb;
        $sql = "select * from wp_countries";
        $data = $wpdb->get_results($sql,'ARRAY_A');
        return $data;
    }
    
    public function get_state_by_name($state){
        global $wpdb;
        $sql = "select * from wp_zones where zone_name like '".$state."'";
        $data = $wpdb->get_row($sql);
        return $data;
    }
    
    public function get_state_by_code($state,$country_id){
        global $wpdb;
        $sql = "select * from wp_zones where zone_code like '".$state."' and country_id = ".intval($country_id);
        $data = $wpdb->get_row($sql);
        return $data;
    }
    
    public function get_states_by_country($country_id){
        global $wpdb;
        $sql = "select * from wp_zones where country_id = ".intval($country_id);
        $data = $wpdb->get_results($sql);
        return $data;
    }
//}












// class FacebookParser {

    protected static function parse_page_data($page) {
        $page->basic = new StdClass;

        $slug = isset($page->facebook->username) ? $page->facebook->username : null;

        if( !$slug ) {
            $slug = sanitize_title($page->facebook->name);
            $page->facebook->username = $page->facebook->id;
        }
        if( !$slug ) {
            $slug = 'site'. $page->facebook->id;
        }
        $page->basic->slug = $slug;

        if(isset($page->facebook->hours)) {
            $page->facebook->hours = self::parse_facebook_hours($page->facebook->hours);
        }

        $address = array();
        $location = null;
        $email = '';
        if(isset($page->facebook)) {
            if (isset($page->facebook->location)) {
                $location = $page->facebook->location;
                $address[] = $location->street;
                $address[] = $location->city;
                $address[] = $location->state;
                $address[] = $location->zip;
                $address[] = $location->country;
            }
            if (isset($page->facebook->email)) {
                $email = $page->facebook->email;
            }
        }
        if( !$address && isset($page->facebook->current_location)) {
            $address = (array)$page->facebook->current_location;
        }
        $address = implode(", ", $address);
        $page->basic->address   = $address;

        $page->basic->location = $location;
        $page->basic->phone = $page->facebook->phone;
        $page->basic->email = $email;
        $page->basic->website = $page->facebook->website;

        $page->basic->facebook  = $page->facebook->link;
        $page->basic->twitter   = '';
        $page->basic->linkedin  = '';
        $page->basic->pinterest = '';
        $page->basic->instagram = '';
        $page->basic->youtube   = '';

        $page->basic->category =  isset($page->facebook->category) ? $page->facebook->category : '';
        $page->basic->category_list =  isset($page->facebook->category_list) ? json_encode($page->facebook->category_list) : '';

        // TODO: Add suggested domain code from CakePHP
        $page->basic->suggested_domain = $page->basic->slug . '.com';

        return $page;
    }

    protected static function parse_posts($page) {
	    if(count($page->posts)>0)
	    {
        foreach($page->posts as &$post) {
            if($post->type == 'link') {
                $post->picture = preg_replace('/http.*\/http/','http',urldecode($post->picture));
            }

            if(empty($post->message)) {
                if(isset($post->name)) { $post->message = $post->name; }
                if(isset($post->title)) { $post->message = $post->title; }
            }
        }
	    }
        return $page->posts;
    }

    protected static function parse_albums($page) {
        if(count($page->albums)>0) {
            foreach($page->albums as &$album) {
                if(empty($album->description)) {
                    $album->description = "";
                }
            }
        }
        return $page->albums;
    }

    protected static function parse_facebook_hours($hours) {
        $result = "";
        $result_array = [];
        $prev_record_day = "";
        foreach($hours as $key => $value) {
            $start_time = $end_time = null;
            $day = substr($key,0,3);

            if(!$prev_record_day) {
                $result .= ucfirst($day) . ": ";

            } else {
                if($day != $prev_record_day) {
                    $result .= "\n\r".ucfirst($day). ": ";
                } else {
                    // $result .= "-";
                }
            }

            if(substr($key,-4)=="open") {
                $result .= $value . " - ";
                $start_time = $value;
                $result_array[$day]['start'] = $start_time;

            } else {
                $result .= $value;
                $end_time = $value;

                $result_array[$day]['end'] = $end_time;
            }

            $prev_record_day = $day;
        }

        return [
            'str' => $result,
            'array' => $result_array
        ];
    }





    protected function parse_date($date = null) {
        if($date){
            $date_timestamp = strtotime($date);
            return date('Y-m-d H:i:s', $date_timestamp);

        } else {
            return date("Y-m-d H:i:s", 0);
        }
    }
}













function otonomic_insert_post($post) {
    global $wpdb;

    $table = $wpdb->prefix.'posts';

    /* create unique post slug */
    $post_name = $post['post_name'];
    if(empty($post_name))
    {
        $post_name = sanitize_title_with_dashes( $post['post_title'] );
    }
    if(empty($post_name))
    {
        $post_name = 'temp';
    }
    $temp_post_name = $post_name;
    $i=0;
    do
    {
        if($i>0)
        {
            $temp_post_name = $post_name.'-'.$i;
        }
        $sql = "select * from {$table} where post_name = '{$temp_post_name}'";
        $data = $wpdb->get_results($sql);
        $i++;
    }while(count($data)>0);

    $post['post_name'] = $temp_post_name;

    //$post_name = $temp_post_name;

    if(!isset($post['post_author']))
        $post['post_author'] = get_current_user_id();

    if( $wpdb->insert( $table, $post))
    {
        $post_id = $wpdb->insert_id;


        return $post_id;
    } else {
        return false;
    }
}

function otonomic_insert_postmeta( $post_id, $post_meta) {
    global $wpdb;

    $sql_arr  =[];
    foreach($post_meta as $key=>$value) {
        if($value === false) { $value = 0; }
        if(is_array($value)) {
            $value = serialize($value);
        }
        $sql_arr[] = "( {$post_id}, '{$key}', '{$value}' )";
    }

    $table = $wpdb->prefix.'postmeta';
    $sql = "INSERT INTO {$table}
        (`post_id`, `meta_key`, `meta_value`)
        VALUES ".implode(', ', $sql_arr). ";";

    global $wpdb;
    return $wpdb->query( $sql);
}

function otonomic_insert_commentmeta($comment_id, $comment_meta){
    global $wpdb;

    $sql_arr  =[];
    foreach($comment_meta as $key=>$value) {
        if($value === false) { $value = 0; }
        if(is_array($value)) {
            $value = serialize($value);
        }
        $sql_arr[] = "( {$comment_id}, '{$key}', '{$value}' )";
    }

    $table = $wpdb->prefix.'commentmeta';
    $sql = "INSERT INTO {$table}
        (`comment_id`, `meta_key`, `meta_value`)
        VALUES ".implode(', ', $sql_arr). ";";
    global $wpdb;
    return $wpdb->query( $sql);
}
