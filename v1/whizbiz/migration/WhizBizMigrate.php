<?php

class WhizBizMigrate{
    
    public $listingID;
    public $fbPageId;
    
    public $page;
    
    function __construct($fbID) {
        $this->fbPageId = $fbID;
    }
    
    function getJsonCategories(){
        $data = file_get_contents(__DIR__.'/data/categories.json');
        $categories = json_decode($data);
        return $categories;
    }
    
    function addCategories($cats){
        $db = new Database();
        $i = 0;
        
        foreach($cats as $cat){
            $check_sql = "select * from dbc_categories where title = ".$db->q($cat);
            $record = $db->getRecord($check_sql);
            if(!$record){
                $data = array(
                    'title' => $db->q($cat),
                    'status' => 1,
                );
                $db->insert('dbc_categories', $data);
            }
            $i++;
        }
    }
    
    function getJsonCities($country='USA'){
        $data = file_get_contents(__DIR__.'/data/'.$country.'citites.json');
        $cities = json_decode($data,TRUE);
        return $cities;
    }
    
    function addCities($cities,$country='USA'){
        $db = new Database();
        $sql = 'select * from dbc_locations where name = '.$db->q($country).' and type = "country"';
        $country_data = $db->getRecord($sql);
        if($country_data){
            $countryID = $country_data->id;
        }else{
            $country = array(
               'name' => $db->q($country),
               'type' => $db->q('country'),
                'status' => 1
            );
            $db->insert('dbc_locations', $country);
            $countryID = $db->getLastId();
        }
        $states = array();
        foreach($cities as $cityData){
            if(!isset($states[$cityData['state']])){
                $get_state_sql = 'select * from dbc_locations where name = '.$db->q($cityData['state']).' and type = "state"';
                $stateData = $db->getRecord($get_state_sql);
                if($stateData){
                    $states[$stateData->name] = $stateData->id;
                }else{
                    $state = array(
                        'name' => $db->q($cityData['state']),
                         'type' => $db->q('state'),
                         'status' => 1,
                        'parent_country' => $countryID,
                        'parent' => $countryID
                    );
                    $db->insert('dbc_locations', $state);
                    $stateID = $db->getLastId();
                    $states[$cityData['state']] = $stateID->id;
                }
            }else{
                $get_city_sql = 'select * from dbc_locations where name = '.$db->q($cityData['city']).' and type = "city"';
                $cityRec = $db->getRecord($get_city_sql);
                if(!$cityRec){
                    $city = array(
                        'name' => $db->q($cityData['city']),
                         'type' => $db->q('city'),
                         'status' => 1,
                        'parent_country' => $countryID,
                        'parent' => $states[$cityData['state']],
                    );
                    $db->insert('dbc_locations', $city);
                    $cityID = $db->getLastId();
                }
            }
        }
        return true;
    }
    
    function add_city($city,$state,$country){
        $countryID = $this->add_country($country);
        $stateID = $this->add_state($state,$countryID);
        
        $db = new Database();
        $cityRec = array(
            'name' => $db->q(ucfirst($city)),
            'type' => $db->q('city'),
            'status' => 1,
            'parent_country' => $countryID,
            'parent' => $stateID,
        );
        $db->insert('dbc_locations', $cityRec);
        $cityID = $db->getLastId();
        return array(
            'city' => $cityID,
            'state' => $stateID,
            'country' => $countryID
        );
    }
    
    function add_country($country){
        $db = new Database();
        $sql = 'select * from dbc_locations where name = '.$db->q($country).' and type = "country"';
        $country_data = $db->getRecord($sql);
        if($country_data){
            $countryID = $country_data->id;
        }else{
            $country = array(
               'name' => $db->q(ucfirst($country)),
               'type' => $db->q('country'),
                'status' => 1
            );
            $db->insert('dbc_locations', $country);
            $countryID = $db->getLastId();
        }
        return $countryID;
    }
    
    function add_state($state,$countryID){
        if(empty($state)){
            return 0;
        }
        $db = new Database();
        $sql = 'select * from dbc_locations where name = '.$db->q($state).' and parent_country = '.$countryID.' and type = "state"';
        $stateData = $db->getRecord($sql);
        if($stateData){
            $stateID = $stateData->id;
        }else{
                $stateRec = array(
                    'name' => $db->q(ucfirst($state)),
                     'type' => $db->q('state'),
                     'status' => 1,
                    'parent_country' => $countryID,
                    'parent' => $countryID
                );
                $db->insert('dbc_locations', $stateRec);
                $stateID = $db->getLastId();
            return $stateID;
        }
    }
    
    function add_listing() {
        $this->page = $this->fetch_page_data($this->fbPageId);
//        echo "<pre>";
//        print_r($this->page);
        $check = $this->check_listing_exists();
        if($check) {
            return false;
        }
        $listing_id = $this->create_listing();
        $this->listingID = $listing_id;
        $this->addListingMeta();
        return $listing_id;
    }
    
    function check_listing_exists(){
        $db = new Database();
        $sql = 'select * from dbc_posts where unique_id = '.$db->q($this->page->facebook->id);
        $check = $db->getRecord($sql);
        if($check){
            return TRUE;
        }
        return false;
    }
    
    function addListingMeta(){
        $db = new Database();
        $postMetaFBProfile = array(
            '`post_id`' => $this->listingID,
            '`key`' => $db->q('facebook_profile'),
            '`value`' => $db->q($this->page->facebook->link),
            '`status`' => 1
        );
        $db->insert('dbc_post_meta', $postMetaFBProfile);
        
        $postMetaBusinessLogo = array(
            '`post_id`' =>$this->listingID,
            '`key`' => $db->q('business_logo'),
            '`value`' => $db->q('http://graph.facebook.com/'.$this->page->facebook->id.'/picture?type=large'),
            '`status`' => 1
        );
        $db->insert('dbc_post_meta', $postMetaBusinessLogo);
        
    }
    
    function create_listing(){
        $db = new Database;
        $page = $this->page;
        $facebook = $page->facebook;
        $basic = $page->basic;
        $title = array(
            'en' => $facebook->name
        );
        $description = array(
            'en' => $this->generateDescription($facebook)
        );
        
        $cityID = $stateID = $countryID = 0;
        
        if(isset($basic->location->city)){
            $city = $this->getCityByName($basic->location->city);
        }
        if(!$city){
            
            $LocationIds = $this->add_city($basic->location->city, $basic->location->state, $basic->location->country);
            $cityID = $LocationIds['city'];
            $stateID = $LocationIds['state'];
            $countryID = $LocationIds['country'];
//            $state = $this->getStateByName($basic->location->state);
//            
//            if(!$state){
//                $country = $this->getCountryByName($basic->location->country);
//                if($country){
//                    $countryID = $country->id;
//                }
//            }else{
//                $stateID = $state->id;
//                $countryID = $state->parent;
//            }
            
        }else{
            $cityID = $city->id;
            $stateID = $city->parent;
            $countryID = $city->parent_country;
        }
        
        $categoryID = 0;
        if(!empty($facebook->category)){
            $category = $this->findListingCategory($facebook->category);
//            if($category){
//                $categoryID = $category->id;
//            }
        }
        
        $tags = array();
        if(isset($facebook->category_list)){
            foreach ($facebook->category_list as $tag){
                $tags[] = $tag->name;
            }
        }
        
        $gallery = $this->getGalleryPhotos();
        
        $listing = array(
            'unique_id' => $facebook->id,
            'title' => $db->q(json_encode($title)),
            'description' => $db->q(json_encode($description)),
            'tags' => $db->q(implode(',', $tags)),
            'category' => $db->q($category),
            'address' => $db->q($basic->location->street),
            'phone_no' => $db->q($basic->phone),
            'website' => $db->q($basic->website),
            'founded' => '',
            'email' => $db->q($basic->email),
            'country' => $countryID,
            'state' => $stateID,
            'city' => $cityID,
            'zip_code' => $db->q($basic->location->zip),
            'latitude' => $db->q($basic->location->latitude),
            'longitude' => $db->q($basic->location->longitude),
            'featured_img' => $db->q('http://graph.facebook.com/'.$facebook->id.'/picture?type=large'),
            'gallery' => $db->q(json_encode($gallery)),
            'opening_hour' => $db->q(json_encode($facebook->hours)),
            'create_time' => time(),
            'search_meta' => $db->q($facebook->name),
            'status' => 1,
        );
//        echo "<pre>";
//        print_r($listing);
        $db->insert('dbc_posts', $listing);
        return $db->getLastId();
    }
    
    function generateDescription($data){
        $description = '';
        if(isset($data->about)){
            $description .= '<p>'.$data->about.'</p>';
        }
        
        if(isset($data->description)){
            $description .= '<p>'.$data->description.'</p>';
        }
        return $description;
    }
    
    function findListingCategory($category){
        $db = new Database();
        $sql = 'select * from dbc_categories where title = '.$db->q($category);
        $catData = $db->getRecord($sql);
        if(!$catData){
            $catRec = array(
                'title' => $db->q($category),
                'create_time' => time(),
                'status' => 1, 
            );
            $db->insert('dbc_categories', $catRec);
            return $db->getLastId();
        }
        return $catData->id;
    }
    
    function getCategoryByName($category){
        
    }
    
    function getGalleryPhotos(){
        $gallery = array();
        if($this->page->photos){
            $photos = $this->page->photos;
            foreach($photos as $photo){
                $gallery[] = 'http://graph.facebook.com/'.$photo->id.'/picture?type=normal';
            }
        }
        return $gallery;
    }
    
    function getCountryByName($name){
        $db = new Database();
        $mappings = array(
            'United States' => 'USA'
        );
        if(isset($mappings[$name])){
            $name = $mappings[$name];
        }
        
        $sql = 'select * from dbc_locations where name = '.$db->q($name);
        $data = $db->getRecord($sql);
        return $data;
    }
    
    function getStateByName($name){
        
        include_once __DIR__.'/data/USAStates.php';
        if(isset($states[$name])){
            $name = $states[$name];
        }
        
        $db = new Database();
        $sql = 'select * from dbc_locations where name = '.$db->q($name);
        $data = $db->getRecord($sql);
        return $data;
    }
    
    function getCityByName($name){
        $db = new Database();
        $sql = 'select * from dbc_locations where name = '.$db->q($name);
        $data = $db->getRecord($sql);
        return $data;
    }
    
    private function fetch_page_data($fb_id)
    {
        $page = self::download_page_data($fb_id);
        return $this->parse_page_data($page);
    }


    private static function download_page_data($fb_id)
    {

        $fb_name_identifier = $fb_id;

        $facebook_basic_url = "https://graph.facebook.com/". $fb_name_identifier ."?access_token=". FB_TOKEN."&fields=id,username,name,link,picture,likes,talking_about_count,cover,website,phone,location,company_overview,general_info,personal_info,mission,personal_interests,about,description,bio,genre,founded,band_members,members,general_manager,hometown,current_location,band_interests,hours,category,category_list";

//        $facebook_email_url = "http://otodirectory.test/migration/fb_email.php?page_id=" . $fb_name_identifier;

        $posts_url = "http://builder.otonomic.com/resource/fb_id:". $fb_name_identifier ."/media:facebook/resources:post";

        $testimonials_url = "http://wp.otonomic.com/migration/helpers/FacebookReviews.php?url=".urlencode("https://m.facebook.com/page/reviews.php?id=". $fb_name_identifier);
        
        //$albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields(id,name,link,cover_photo,type,description,count,photos.fields(id,picture,source,images,caption,modified)";
        $albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields=id,name,link,cover_photo,type,description,count,photos.fields(id,picture,images,source,caption,link,width,height,created_time,updated_time,name)";

        $videos_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/videos?access_token=". USER_TOKEN;

        $notes_url = "https://graph.facebook.com/v1.0/". $fb_name_identifier ."/notes?access_token=". FB_TOKEN;
        
        $latest_photos_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/photos?type=uploaded&access_token=". FB_TOKEN."&fields=images,id,created_time,name,link,album,source";

        $respond = Curl::multiRequest(array($facebook_basic_url, $latest_photos_url,$testimonials_url));
        
//        $reviews = "https://graph.facebook.com/v3.0/". $fb_name_identifier ."/ratings?access_token=". FB_TOKEN;
//        echo $reviews;
//        echo $testimonials_url;
        if(empty($respond[0])) {
            $respond[0] = file_get_contents($facebook_basic_url);
        }

        foreach($respond as $key=>$value):
            $respond[$key] = json_decode($value);
        endforeach;


        $page = new stdClass();
        $page->facebook = $respond[0];

        $page->facebook->email = $respond[1]->result;

//        $page->posts = $respond[1]->data->post;
        $page->testimonials = $respond[2]->facebook_reviews;
//        $page->albums = isset($respond[2]->data) ? $respond[2]->data : null;
//        $page->videos = isset($respond[4]->data) ? $respond[4]->data : null;
//        $page->notes = isset($respond[5]->data) ? $respond[5]->data : [];

//        $page->posts = self::parse_posts($page);
//        $page->albums = self::parse_albums($page);
        
        $page->photos = isset($respond[1]->data) ? $respond[1]->data : null;
        
        if( isset($page->facebook->error)) {
            self::email_admin_about_facebook_graph_error();
        }
        return $page;
    }
    
    protected function parse_page_data($page) {
        $page->basic = new StdClass;
//        $slug = isset($page->facebook->username) ? $page->facebook->username : null;
//
//        if( !$slug ) {
//            $slug = sanitize_title($page->facebook->name);
//            $page->facebook->username = $page->facebook->id;
//        }
//        if( !$slug ) {
//            $slug = 'site'. $page->facebook->id;
//        }
//        $page->basic->slug = $slug;

        if(isset($page->facebook->hours)) {
            $page->facebook->hours = $this->parse_facebook_hours($page->facebook->hours);
        }

        $address = array();
        $location = null;
        $email = '';
        if(isset($page->facebook)) {
            if (isset($page->facebook->location)) {
                $location = $page->facebook->location;
                $address[] = $location->street;
                $address[] = $location->city;
                $address[] = $location->state;
                $address[] = $location->zip;
                $address[] = $location->country;
            }
            if (isset($page->facebook->email)) {
                $email = $page->facebook->email;
            }
        }
        if( !$address && isset($page->facebook->current_location)) {
            $address = (array)$page->facebook->current_location;
        }
        $address = implode(", ", $address);
        $page->basic->address   = $address;

        $page->basic->location = $location;
        $page->basic->phone = $page->facebook->phone;
        $page->basic->email = $email;
        $page->basic->website = $page->facebook->website;

        $page->basic->facebook  = $page->facebook->link;
        $page->basic->twitter   = '';
        $page->basic->linkedin  = '';
        $page->basic->pinterest = '';
        $page->basic->instagram = '';
        $page->basic->youtube   = '';

        $page->basic->category =  isset($page->facebook->category) ? $page->facebook->category : '';
        $page->basic->category_list =  isset($page->facebook->category_list) ? json_encode($page->facebook->category_list) : '';

        // TODO: Add suggested domain code from CakePHP
        $page->basic->suggested_domain = $page->basic->slug . '.com';

        return $page;
    }
    
    function parse_facebook_hours($hours){
        $hours_prepare = array();
        
        $weekday_names = array(
            'mon'=>'monday',
            'tue'=>'tuesday',
            'wed'=>'wednesday',
            'thu'=>'thursday',
            'fri'=>'friday',
            'sat'=>'saturday',
            'sun'=>'sunday');
        foreach ($hours as $day => $time){
            $start_time = $end_time = null;
            $weekday_initial = substr($day,0,3);
            $weekday = $weekday_names[$weekday_initial];

            if(substr($day,-4)=="open") {
                $result .= $time . " - ";
                $start_time = $time;
                $hours_prepare[$weekday]['start_time'] = $start_time;

            } else {
                $result .= $time;
                $end_time = $time;

                $hours_prepare[$weekday]['close_time'] = $end_time;
            }
        }
        
        $opening_hours = array();
        $i = 0;
        // [{"day":"monday","closed":0,"start_time":"09:00 AM","close_time":"09:00 AM"}]
        foreach($weekday_names as $weekday){
            $opening_hours[$i]['day'] = $weekday;
            if(isset($hours_prepare[$weekday])){
                $opening_hours[$i]['closed'] = 0;
                $opening_hours[$i]['start_time'] = $hours_prepare[$weekday]['start_time'];
                $opening_hours[$i]['close_time'] = $hours_prepare[$weekday]['close_time'];
            }else{
                $opening_hours[$i]['closed'] = 1;
                $opening_hours[$i]['start_time'] = '';
                $opening_hours[$i]['close_time'] = '';
                
            }
            $i++;
        }
        return $opening_hours;
    }
    
}


?>
