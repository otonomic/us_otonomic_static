<?php

ini_set("auto_detect_line_endings", "1");
ignore_user_abort(true);
set_time_limit(0);

$base_dir = __DIR__.'/';

include_once $base_dir . '../../wp-load.php';

include_once $base_dir . '../config/fb.php';
include_once $base_dir . '../helpers/Curl.php';
include_once $base_dir . '../helpers/Page2site.php';
include_once $base_dir . '../helpers/FB_email.php';

include_once $base_dir . '../DirectoryMigrate.php';

$import_file = $_FILES['import_csv'];

$import_records = intval($_POST['import_records']);
$import_start_from = intval($_POST['import_records_from']);
$fb_column = intval($_POST['fb_column']);

if(empty($import_records))
	$import_records = null;
if(empty($import_start_from))
	$import_start_from = 0;

$allowed_mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
$respond = array();
$respond['status'] = "Error";
$respond['message'] = "Something went wrong";
if(is_uploaded_file( $_FILES['import_csv']['tmp_name'] ))
{
	if(in_array($_FILES['import_csv']['type'],$allowed_mimes))
	{
		if (($handle = fopen($_FILES['import_csv']['tmp_name'], "r")) !== FALSE) {
			$row = 0;
			$fb_ids = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$row++;
				if($row == 1)
				{
					/* Ignore header row */
					continue;
				}
				$fb_ids[] = $data[ $fb_column ];
			}
			fclose($handle);
			/* check if we need to import from start */
			$fb_ids = array_slice($fb_ids, $import_start_from, $import_records);
			/* Now import the data  */
			foreach($fb_ids as $facebook_id) {
				if(!check_listing_exists($facebook_id)) {
					$wp = new DirectoryMigrate($facebook_id);
					$processed_ids[] = $wp->add_listing();
				}
			}
			$respond['status'] = "success";
			$respond['message'] = "Successfully imported successfully";
			$respond['processed_pages'] = $processed_ids;
		}
	}
}
function check_listing_exists($facebook_id){
	global $wpdb;
	$sql = "select count(*) count from wp_postmeta where meta_key = 'facebook_id' and meta_value ='{$facebook_id}'";
	$data = $wpdb->get_results($sql);
	if($data[0]->count > 0){
		return true;
	} else{
		return false;
	}
}
echo json_encode($respond);