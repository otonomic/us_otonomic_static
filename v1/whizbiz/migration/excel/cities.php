<?php 
$GLOBALS['CHECK_BROWSER'] = false;
$GLOBALS['IGNORE_SITE_MAINTENANCE'] = true;

error_reporting(E_ALL);
ini_set('display_errors','1');

require_once ('/excel/PHPExcel.php');
ini_set('memory_limit', '256M');
set_time_limit(0);

    /** PHPExcel_IOFactory */
    require_once ('excel/PHPExcel/IOFactory.php');
    
    $file = "zip_codes_states.csv";
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $worksheets = $objPHPExcel->getSheetNames();
    
    $sheetData = $objPHPExcel->getSheetByName($worksheets[0])->toArray(null,false,false,false);
//    echo "<pre>";
//    print_r($sheetData);
    
    
    
    // Code to add cities from excel
    $cities = array();
    foreach($sheetData as $row){
        $cities[] = array(
            'city' => $row[3],
            'state' => $row[4],
            'country' => 'United States',
            'latitude' => $row[1],
            'longitude' => $row[2]
        );
    }
//    echo "<pre>";
//    print_r($cities);
//    echo "<hr> cities count - ";
//    echo count($cities);
//    die();
    $cities = array_slice($cities, 40000,10000);
    require_once ("../../wp-load.php");
    require_once ("../DirectoryMigrate.php");
    $migration = new DirectoryMigrate();
    
    $result = $migration->add_cities($cities,"code");
    echo $result;
    
    
?>