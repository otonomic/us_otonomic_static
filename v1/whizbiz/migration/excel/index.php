<?php 
$GLOBALS['CHECK_BROWSER'] = false;
$GLOBALS['IGNORE_SITE_MAINTENANCE'] = true;

error_reporting(E_ALL);
ini_set('display_errors','1');

require_once ('/excel/PHPExcel.php');
ini_set('memory_limit', '256M');
set_time_limit(0);

    /** PHPExcel_IOFactory */
    require_once ('excel/PHPExcel/IOFactory.php');
    
    $file = "pages.xls";
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $worksheets = $objPHPExcel->getSheetNames();
    
    $sheetData = $objPHPExcel->getSheetByName($worksheets[0])->toArray(null,false,false,false);
//    echo "<pre>";
//    print_r($sheetData[0]);
    
    
    
    // Code to add cities from excel
//    $fb_ids = array();
//    $cities = array();
//    foreach($sheetData as $row){
//        $cities[$row[4]] = array(
//            'state' => $row[25],
//            'country' => $row[8]
//        );
//    }
//    
//    require_once ("../../wp-load.php");
//    require_once ("../WPMigration.php");
//    $migration = new WPMigration();
//    
//    $result = $migration->add_cities($cities);
//    echo $result;
    
    //code to add listing from excel sheet
    require_once ("../../wp-load.php");
    include_once '../config/fb.php';
    include_once '../helpers/Curl.php';
    include_once '../helpers/Page2site.php';
    require_once ("../WPMigration.php");
    $fb_ids = array();
    foreach($sheetData as $row){
        $fb_ids[] = $row[17];
    }
//    echo "<pre>";
//    print_r($fb_ids);
    $first_ids = array_slice($fb_ids, 450,5);
    print_r($first_ids);
//    print_r($first_ids);
    $crnt_time = microtime(TRUE);
    foreach ($first_ids as $fbid) {
        $wp = new WPMigration($fbid);
        $check = $wp->check_listing_exists();
        if($check){
            echo "<hr>listing already exists for this page";
            continue;
        }
        $listing_id = $wp->create_listing();
        $wp->create_listing_options($listing_id);
        $wp->add_listing_category($listing_id);
        echo "<hr>".$listing_id;
    }
    $time_taken = microtime(TRUE) - $crnt_time;
    echo "Time taken to insert 100 records - ".$time_taken;
    
    
?>