<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ignore_user_abort(true);
set_time_limit(0);

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');

if(!isset($_GET['facebook_ids'])){
	echo "set facebook_ids via GET, separated by commas";
	exit;
}

try {

    $facebook_ids = rawurldecode($_GET['facebook_ids']);
    $respond = [];
    $base_dir = __DIR__.'/';

    include_once $base_dir . 'config/fb.php';
    include_once $base_dir . 'helpers/Curl.php';
    include_once $base_dir . 'helpers/Database.php';
//    include_once $base_dir . 'helpers/Page2site.php';
//    include_once $base_dir . 'helpers/FB_email.php';

    include_once $base_dir . 'WhizBizMigrate.php';

    $facebook_ids = explode(',', $facebook_ids);

    $processed_ids = [];
    foreach($facebook_ids as $facebook_id) {
        $wp = new WhizBizMigrate($facebook_id);
        $processed_ids[] = $wp->add_listing();
    }

    $respond['status'] = "success";
    $respond['message'] = "Successfully imported successfully";
    $respond['processed_pages'] = $processed_ids;

} catch(Exception $e){
    $error = json_decode($e->getMessage());

    $respond['redirect'] =  $error->redirect;
    $respond['site_url'] = $error->site_url;
    $respond['status'] = "fail";
    $respond['message'] = $error->error;
}

echo json_encode($respond);


function trim_closest_word($string, $max_length, $append_ellipsis = true) {
    $result = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $max_length));
    if(strlen($string)>$max_length) { $result .= '&hellip;'; }
    return $result;
}
