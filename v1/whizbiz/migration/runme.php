<html>
<head></head>
<body>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

<textarea id="input" rows="20" cols="20"></textarea>
<button id="go">Go</button>

<div>Results:</div>
<div id="results"></div>
<?php 
    $url = 'http://'.$_SERVER['SERVER_NAME'].'/whizbiz/migration/index.php';
?>
<script>
    var list;

    $('#go').click(function() {
        list = $('#input').val().split('\n');

        for(var i=0; i<list.length; i++) {
            if(list[i].length > 0){
                setTimeoutAddListing(i);
            }
        }
    });

    function addListing(page_id) {
        if(page_id === "") { return; }

        url = "<?= $url; ?>/?facebook_ids="+page_id;
        jQuery.getJSON(url, function(data) {
            if(data.processed_pages[0] == false){
                $('#results').append(page_id + "\t listing exists <br/>");
            }else{
                $('#results').append(page_id + "\t" + data.status + "<br/>");
            }
        });
        // alert(url);
    }

    function setTimeoutAddListing(i) {
        return setTimeout(function() {
            addListing(list[i]);
        }, 100*i);
    }
</script>
</body>
</html>