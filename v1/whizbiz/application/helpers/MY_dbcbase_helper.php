<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_featured_photo_by_id($img='')
{
        $url = parse_url($img);
        if(isset($url['scheme']) && ($url['scheme'] = 'http' || $url['scheme'] = 'https')){
            return $img;
        }
        
        if($img=='')
        return base_url('assets/admin/img/preview.jpg');
        else
        return base_url('uploads/thumbs/'.$img);
}

function get_photo_base_url($img = '',$path=''){
    $url = parse_url($img);
    if(isset($url['scheme']) && ($url['scheme'] = 'http' || $url['scheme'] = 'https')){
        return $img;
    }else{
        return $path.'/'.$img;
    }
}

function get_category_title_by_id($id='')
{
        if($id==0)
                return '';
        $CI = get_instance();
        $CI->load->database();
        $query = $CI->db->get_where('categories',array('id'=>$id));
        if($query->num_rows()>0)
        {
                $row = $query->row();
                return lang_key($row->title);
        }
        else
                return '';
}