<?php
include_once '../../../../wp-load.php';
include_once '../classes/Bluesnap.php';
$bluesnap = new Bluesnap();
$form_data = $_POST;

$name = explode(" ", $form_data['fullName'], 2);
$facebook_basic_data = get_option('otonomic_site_data_basic');
$price = Configure::read("Plans.".$form_data['plan'].".cost.".$form_data['recur']);
if(is_array($price)){
    $price = $price['year'];
}

$form_data['first_name'] = $name[0];
$form_data['last_name'] = $name[1];
$form_data['address'] = $facebook_basic_data->location->street;
$form_data['city'] = $facebook_basic_data->location->city;
$form_data['state'] = $facebook_basic_data->location->state;
$form_data['zip'] = $facebook_basic_data->location->zip;
$form_data['contract_id'] = Configure::read('Plans.'.$form_data['plan'].'.PlimusContractID.'.$form_data['recur']);
$form_data['amount'] = $price;

//echo "<pre>";
//print_r($form_data);
do_action('otonomic_payment_processing',$form_data);

$response = $bluesnap->create_shopper_place_order($form_data);
if($response['http_code'] != 200){
    do_action('otonomic_payment_processing_error',$response);
    header('location:'.$_SERVER['HTTP_REFERER'].'?type=error');
}else{
    do_action('otonomic_payment_processing_success',$response);
    header('location:'.$_SERVER['HTTP_REFERER'].'?type=success');
}

//echo "<pre>";
//print_r($response);
?>
