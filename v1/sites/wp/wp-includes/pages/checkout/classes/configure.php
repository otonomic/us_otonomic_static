<?php
class Configure {
    static $values = [];

    static function init() {
        self::write('Plimus.action_url', "https://www.plimus.com/jsp/buynow.jsp");
        self::write('Plans.Basic.PlimusContractID.monthly', '2154154');
        self::write('Plans.Basic.PlimusContractID.yearly', '2154158');
        self::write('Plans.Professional.PlimusContractID.monthly', '2154170');
        self::write('Plans.Professional.PlimusContractID.yearly', '2154164');
        self::write('Plans.Premium.PlimusContractID.monthly', '2154178');
        self::write('Plans.Premium.PlimusContractID.yearly', '2154172');

//        self::write('Plimus.action_url', "https://www.plimus.com/jsp/buynow.jsp");
//        self::write('Plans.Basic.PlimusContractID.monthly', '2327127');
//        self::write('Plans.Basic.PlimusContractID.yearly', '2327121');
//        self::write('Plans.Business.PlimusContractID.monthly', '2327131');
//        self::write('Plans.Business.PlimusContractID.yearly', '2327123');
//        self::write('Plans.VIP.PlimusContractID.monthly', '2327133');
//        self::write('Plans.VIP.PlimusContractID.yearly', '2327125');

        self::write('Plans.Basic.cost.monthly', '6');
        self::write('Plans.Basic.cost.yearly', ['year'=>'48', 'month'=>'4']);
        self::write('Plans.Basic.cost.2years', ['year'=>'72', 'month'=>'3']);

        self::write('Plans.Professional.cost.monthly', '12');
        self::write('Plans.Professional.cost.yearly', ['year'=>'96', 'month'=>'8']);
        self::write('Plans.Professional.cost.2years', ['year'=>'144', 'month'=>'6']);

        self::write('Plans.Premium.cost.monthly', '24');
        self::write('Plans.Premium.cost.yearly', ['year'=>'192', 'month'=>'16']);
        self::write('Plans.Premium.cost.2years', ['year'=>'288', 'month'=>'12']);
    }

    public static function read($key) {
        if( isset(self::$values[$key])) {
            return self::$values[$key];
        }

        return null;
    }

    public static function write($key, $value) {
        return self::$values[$key] = $value;
    }
}

Configure::init();
?>
