<?php
class Bluesnap{
    private $username = 'API_14169900038441947884279';
    private $password = 'Testing12';
    public $store_id = '14195';
    public $currency = 'USD';
    public $base_url = 'https://sandbox.plimus.com/services/2/';

    public function __construct() {
    }
    
    public function save_shopper_details($data){
        global $wpdb;
        $insert_rec = array(
            'user_id' => get_current_user_id(),
            'blog_id' => get_current_blog_id(),
            'shopper_id' => (integer)$data->shopper->{'shopper-info'}->{'shopper-id'},
            'order_id' => (integer)$data->order->{'order-id'},
            'sku_id'=> (integer)$data->order->cart->{'cart-item'}->sku->{'sku-id'},
            'invoice_id' => (integer)$data->order->{'post-sale-info'}->invoices->invoice->{'invoice-id'}
        );
         $wpdb->insert( 'wp_payments', $insert_rec );
    }
    
    public function create_shopper_place_order($details){
        $url = $this->base_url.'batch/order-placement';
        
        $user_data = array(
            'first_name' => $details['first_name'],
            'last_name' => $details['last_name'],
            'address' => $details['address'],
            'city' => $details['city'],
            'zip' => $details['zip'],
            'state' => $details['state'],
            'country' => $details['country']
        );
        $user_xml = $this->get_xml_userinfo($user_data);
        $web_info = $this->get_xml_webinfo();
        $phone_xml = $this->get_xml_phone($details['phone']);
        $email_xml = $this->get_xml_email($details['email']);
        /**
         * Assemble the XML string with variables instantiated above
       */     
        $xmlToSend = '       
        <batch-order xmlns="http://ws.plimus.com">
            <shopper>
                '.$web_info.'
                <shopper-info>
                    <shopper-contact-info>
                        '.$user_xml.$phone_xml.$email_xml.'
                    </shopper-contact-info>
                    <payment-info>     
                        <credit-cards-info>
                            <credit-card-info>
                                <billing-contact-info>
                                    '.$user_xml.'
                                </billing-contact-info>
                                <credit-card>              
                                    <encrypted-card-number>'.$details['encryptedCreditCard'].'</encrypted-card-number>
                                    <card-type>'.$details['card_type'].'</card-type>
                                    <expiration-month>'.$details['cc-exp-date-month'].'</expiration-month>
                                    <expiration-year>'.$details['cc-exp-date-year'].'</expiration-year>
                                    <encrypted-security-code>'.$details['encryptedCvv'].'</encrypted-security-code>
                                </credit-card>
                            </credit-card-info>
                        </credit-cards-info>
                    </payment-info>
                    <store-id>'.$this->store_id.'</store-id>
                    <shopper-currency>'.$this->currency.'</shopper-currency>       
                </shopper-info>
            </shopper>
            <order>
                <ordering-shopper>
                    '.$web_info.'
                </ordering-shopper>
                <cart>
                    <cart-item>
                        <sku>
                            <sku-id>'.$details['contract_id'].'</sku-id>          
                            <soft-descriptor>descTest</soft-descriptor>
                        </sku>
                        <quantity>1</quantity>
                    </cart-item>
                </cart>
                <expected-total-price>
                    <amount>'.$details['amount'].'</amount>
                    <currency>'.$this->currency.'</currency>
                </expected-total-price>
            </order>
        </batch-order>';
        $data = $this->curl_request($url,$xmlToSend);
        if($data['http_code'] == 200){
            $this->save_shopper_details($data['response']);
            $this->change_user_details($details);
        }
        return $data;
    }
    
    public function change_user_details($data){
        update_option('site_role', $data['plan']);
    }
    
    public function get_contract($contract_id){
        $url = $this->base_url.'catalog/skus/'.$contract_id;
        $data = $this->curl_request($url);
        return $data;
    }
    
    public function get_shopper($shopper_id){
        $url = $this->base_url.'shoppers/'.$shopper_id;
        $data = $this->curl_request($url);
        return $data;
    }
    
    public function get_order($order_id){
        $url = $this->base_url.'orders/'.$order_id;
        $data = $this->curl_request($url);
        return $data;
    }
    
    public function get_subscription($subscription_id,$fulldescription=false){
        $url = $this->base_url.'subscriptions/'.$subscription_id;
        if($fulldescription){
            $url .= "?fulldescription=true";
        }
        $data = $this->curl_request($url);
        return $data;
    }
    
    public function get_product($product_id){
        $url = $this->base_url.'catalog/products/'.$product_id;
        $data = $this->curl_request($url);
        return $data;
    }
    
    public function get_xml_webinfo(){
        $web_info = '<web-info>
                        <ip>'.$_SERVER['REMOTE_ADDR'].'</ip>        
                        <remote-host>bzq-219-121-253.static.bezeqint.net.reinventhosting.com</remote-host>
                        <user-agent>'.$_SERVER['HTTP_USER_AGENT'].'</user-agent>
                        <accept-language>en-us</accept-language>
                    </web-info>';
        return $web_info;
    }
    
    public function get_xml_userinfo($user_data){
        $user_info = '<first-name>'.$user_data['first_name'].'</first-name>
                        <last-name>'.$user_data['last_name'].'</last-name>
                        <address1>'.$user_data['address'].'</address1>
                        <city>'.$user_data['city'].'</city>
                        <state>'.$user_data['state'].'</state>
                        <zip>'.$user_data['zip'].'</zip>
                        <country>'.$user_data['country'].'</country>';
        
        if(isset($user_data['phone'])){
            $user_info .= $this->get_xml_phone($user_data['phone']);
        }
        if(isset($user_data['email'])){
            $user_info .= $this->get_xml_email($user_data['email']);
        }
        return $user_info;
    }
    
    public function get_xml_phone($phone){
        return '<phone>'.$phone.'</phone>';
    }
    
    public function get_xml_email($email){
        return '<email>'.$email.'</email>';
    }

    public function curl_request($url,$xml=false){
        $credentials = $this->username . ":" . $this->password;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        if($xml){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        }
        
        // The following switches are needed only when running in development-mode on localhost
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);  // follow redirects recursively 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    
        /**
         * Execute Curl call and display XML response
        */
        $result = curl_exec($ch);
        
        $request_info =  curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        $response_data = simplexml_load_string($result);
        $response = array(
            'http_code' => $request_info,
            'response' => $response_data
        );
        return $response;
    }
}
?>
