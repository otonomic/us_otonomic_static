<?php include_once '../../../wp-load.php'; 
$plans = array('Basic','Professional','Premium');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="In under 20 seconds , we will turn your Facebook page into a professional website">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Otonomic Checkout</title>

        <title>Otonomic Pricing Page</title>
        <link rel="shortcut icon" href="/favicon.ico">
        <meta property="og:title" content="Even a small business deserves a big spotlight." />
        <meta property="og:site_name" content="otonomic"/>
        <meta property="og:description" content="In under 20 seconds , we will turn your Facebook page into a professional website"/>
        <meta property="og:url" content="http://otonomic.com"/>
        <meta property="og:image" content="http://otonomic.com/images/section-home-bg.png"/>

        <!-- Custom styles for this template -->
        <link href="css/styles.css" rel="stylesheet">

      <style>
          #dropdown-language {display:none;}
      </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-37736198-1', 'auto');
          ga('send', 'pageview');
      </script>

      <script text="text/javascript" src="/wp-content/plugins/otonomic-analytics/js/main.js"></script>
      <script text="text/javascript" src="/wp-content/plugins/otonomic-analytics/js/otonomic_platform_analytics_events.js"></script>

      <!-- Facebook Conversion Code for Checkout View -->
      <script>(function() {
              var _fbq = window._fbq || (window._fbq = []);
              if (!_fbq.loaded) {
                  var fbds = document.createElement('script');
                  fbds.async = true;
                  fbds.src = '//connect.facebook.net/en_US/fbds.js';
                  var s = document.getElementsByTagName('script')[0];
                  s.parentNode.insertBefore(fbds, s);
                  _fbq.loaded = true;
              }
          })();
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6018983585430', {'value':'0.00','currency':'USD'}]);
      </script>
      <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018983585430&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
      <!-- END Facebook Conversion Code for Checkout View -->

  </head>

  <body>

    <nav class="navbar navbar-inverse">
      
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="otonomic.com"><img class="logo-img" src="images/otonomic-logo.png"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <!-- <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul> -->
          <form class="navbar-form navbar-right">
            <div id="dropdown-language" class="form-group">
              <label>Langauge</label>
              <select class="form-control">
                <option>English</option>
                <option>Spanish</option>
                <option>French</option>
                <option>Hebrew</option>
                <option>Russian</option>
              </select>
            </div>

            <div id="dropdown-plan" class="form-group">
              <label>Plan</label>
              <select name="plan_select" class="form-control">
                  <?php 
                  foreach($plans as $plan): 
                      if(!isset($_GET['plan']) && $plan == "Professional"){
                          $selected = 'selected';
                      }
                      elseif(isset($_GET['plan']) && $plan == ucfirst($_GET['plan'])){
                            $selected = 'selected';
                      }else{
                            $selected = '';
                      }
                    ?>
                        <option <?= $selected; ?> value="<?= $plan ?>"><?= $plan ?></option>
                  <?php endforeach; ?>
              </select>
            </div>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <h2>Upgrade to a premium plan:</h2>
      <div class="row">
        <div class="col-md-8">
          <div id="form-container">
            <form id="form" method="post" action="includes/payment_process.php" class="form-horizontal">
            <h4>Billing information</h4>
            <?php if(isset($_GET['type']) && $_GET['type'] == 'error'): ?>
            <div style="color: red">Error in processing payment. Please try again will valid data.</div><br/>
            <?php elseif(isset($_GET['type']) && $_GET['type'] == 'success'): ?>
            <div style="color: green">Payment has been done successfully.</div><br/>
            <?php endif; ?>
              <div class="form-group has-feedback">
                <label for="fullName" class="col-sm-3 control-label">Full name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="fullName" id="fullName">
                  <span class="glyphicon form-control-feedback" id="fullName1"></span>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                  <input type="email" class="form-control" name="email" id="email">
                  <span class="glyphicon form-control-feedback" id="email1"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="country" class="col-sm-3 control-label">Country</label>
                <div class="col-sm-6">
                    <?php 
                        include_once 'includes/countries.php';
                        $basic_data = get_option('otonomic_site_data_basic');
                        $user_country = $basic_data->location->country;
                        if(!$user_country){
                            $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$_SERVER['REMOTE_ADDR']));
                            if($ip_data && $ip_data->geoplugin_countryCode != null){
                                $user_country = $ip_data->geoplugin_countryCode;
                            }else{
                                $user_country = '';
                            }
                        }
                    ?>
                  <select id="country" name="country" class="form-control">
                      <?php foreach ($countries as $country_code => $country):
                          
                                if($country_code == $user_country || $country == $user_country){
                                    $selected = 'selected';
                                }else{
                                    $selected = '';
                                }
                        ?>
                    <option <?= $selected ?> value="<?= $country_code ?>" data-selectable="" class="option"><?= $country; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              
              <div class="form-group has-feedback">
                <label for="phone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="phone" value="<?= get_option('phone') ?>" id="phone">
                  <span class="glyphicon form-control-feedback" id="phone1"></span>
                </div>
              </div>

              <h4>Payment information</h4>
              <div class="form-group">
                <label for="payment-method" class="col-sm-3 control-label">Payment method</label>
                <div id="payment-method" class="col-sm-9">
                  <label class="radio-inline">
                    <input type="radio" name="credit-card" id="credit-card" value="Credit card"><img class="cc-logos" src="images/credit_cards.svg" alt="Credit Card" checked>
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="paypal" id="paypal" value="Paypal"><img class="paypal-logo" src="images/PayPal_logo.svg" alt="PayPal">
                  </label>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="cc-number" class="col-sm-3 control-label">Card number</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="ccNumber" id="ccNumber" data-bluesnap="encryptedCreditCard">
                  <span class="glyphicon form-control-feedback" id="ccNumber1"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="cc-type" class="col-sm-3 control-label">Card type</label>
                <div class="col-sm-6">
                    <select class="form-control" id="cc-type" name="card_type">
                        <option>VISA</option>
                        <option>MASTERCARD</option>
                        <option>AMEX</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label for="cc-exp-date" class="col-sm-3 control-label">Expiration date</label>
                <div class="col-md-3 col-lg-2">
                  <!-- <label class="control-label">Month</label> -->
                  <select class="form-control" name="cc-exp-date-month" id="cc-exp-date-month"> 
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                  </select>
                </div>
                <div class="col-md-3 col-lg-2">
                  <!-- <label class="control-label">Year</label> -->
                  <select class="form-control" name="cc-exp-date-year" id="cc-exp-date-year"> 
                    <option value="2014">2014</option>
                    <option selected="selected" value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="security-code" class="col-sm-3 control-label">Security code</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="secutityCode" id="secutityCode" data-bluesnap="encryptedCvv">
                  <span class="glyphicon form-control-feedback" id="secutityCode1"></span>
                </div>
                <div>
                  <img class="cc-cvv" src="images/cvv.png" alt="CVV">
                  <button id="cc-sc"class="btn btn-link" data-toggle="popover" title="Credit security Code">What is this?</button>
                </div>
              </div>
              <div>
                  <input type="hidden" name="plan" value="Business" >
                  <input type="hidden" id="recurring_option" name="recur" value="yearly" >
                  <input type="hidden" id="currency_option" name="currency" value="USD" >
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                  <button type="submit" class="btn btn-lg btn-success">Pay now</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
        <div class="col-md-4">
            <?php 
            if(isset($_GET['plan'])){
                $selected_plan = ucfirst($_GET['plan']);
            }else{
                $selected_plan = $plans[1];   
            }
            $hidden_class = "";
                foreach ($plans as $plan):
                    $plan_html_id = 'oto_'.$plan.'_plan';
                    if($plan != $selected_plan){
                        $hidden_class = "hidden";
                    }else{
                        $hidden_class = "";
                    }
                    $monthly_cost = Configure::read('Plans.'.$plan.'.cost.monthly');
                    $yearly_cost = Configure::read('Plans.'.$plan.'.cost.yearly');
                    $biyearly_cost = Configure::read('Plans.'.$plan.'.cost.2years');
            ?>
          <div id="<?= $plan_html_id; ?>" class="<?= $hidden_class; ?> panel price-panel-container">
            <div class="row">
              <div class="col-xs-8">
                <h4>Choose your price</h4>
              </div>
<!--              <div class="col-xs-4">
                <div class="form-group currency-select-container">
                    <select name="currency_select" class="currency-select form-control">
                      <option value="USD">USD</option>
                    <option value="ILS">Shekels</option>
                  </select>
                </div>
              </div>-->
            </div>
            <div class="radio active">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan1_<?= $plan ;?>" value="monthly">
                $<?= $monthly_cost; ?> / Month<br/>
                <input type="hidden" class="paymentPlan_monthly_<?= $plan ;?>" value="<?= $monthly_cost; ?>" />
                <small>billed monthly</small>
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan2_<?= $plan ;?>" value="yearly" >
                $<?= $yearly_cost['month']; ?> / Month <span class="save-text">save 33%</span><br/>
                <input type="hidden" class="paymentPlan_yearly_<?= $plan ;?>" value="<?= $yearly_cost['year']; ?>" />
                <small>billed annaully</small>
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan3_<?= $plan ;?>" value="2years">
                $<?= $biyearly_cost['month']; ?> / Month <span class="save-text">save 50%</span><br/>
                <input type="hidden" class="paymentPlan_2years_<?= $plan ;?>" value="<?= $biyearly_cost['year']; ?>" />
                <small>billed every 2 years</small>
              </label>
            </div>
            <hr>
            <h4>Order summary</h4>
            <table class="table">
              <tr>
                <td><?= $plan; ?> Plan</td>
                <td id="oto_gross_payment_<?= $plan; ?>" class="text-right">$144</td>
              </tr>
              <tr>
                <td>Your Savings</td>
                <td id="oto_savings_<?= $plan; ?>" class="text-right green-text">$-48</td>
              </tr>
              <tr>
                <td><h4>Next Payment</h4></td>
                <td id="oto_actual_payment_<?= $plan; ?>" class="text-right"><h4>$96</h4></td>
              </tr>
            </table>
          </div>
            <?php endforeach; ?>
        </div>
      </div>
      <div class="row footer">
        <div class="col-xs-12 text-center">
          <img src="images/footer-icons.png" alt="Our security">
          <p>Secure e-Commerce services are provide by <a href="http://home.bluesnap.com/" target="_blank">BlueSnap</a>, on online reseller for <a href="http://www.otonomic.com/" target="_blank">Otonomic</a></p>
        </div>
      </div>
    </div><!-- /.container -->
    
    <script src="https://gateway.bluesnap.com/js/cse/v1.0.1/bluesnap.js"></script>
    <script>BlueSnap.publicKey = "10001$87e2291adb72bc06e4e6eb529468bf6278a913cd27e2f9682a680e4195eeb475d3ea0823c14060787fa5244606ebb21ff9f581bd3f87488f97ce4704b3f19b37827fa3f3b29e58eb7737d4cdf4bb826347c5c9d596e7dd0fef37e40d4bbc2723eba6fb0952ff4f1f6390f37e67f165d40d75d60204c3c2c41de0fd79c56593104530d0999dde4328bd706453d3d9d34c140af32c9a75b759719b5f8e8f1a267e51005ab53805f78388d55d69a2b163e02fef806429e42d9560254a167a3584ef812567126039f58a9dd6cff6e363d5a2aececdb02ee6aa0e0d8fc40197ac334cc1b2d6b7a851482296e60bd1e49bd09440a7cc50a03e81b954a52e1652df9467";  
    BlueSnap.setTargetFormId("form");
    </script>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/jquery-validate.bootstrap-tooltip.min.js"></script>

    <script type="text/javascript">
      $(function () {
        $('#cc-sc[data-toggle="popover"]').popover({
          trigger: 'hover',
          placement: 'top',
          html: true,
          content: '\
          <p>A three or four digit code on your credit card, separate from the 16 digit card number. The location varies slightly depending on your type of card:</p>\
          <div class="row">\
            <div class="col-sm-6"><img src="images/visa-cvv.png" alt="CCV"><p>On the reverse side of your card, you should see either the entire 16-digit credit card number or just the last four digits followed by a special 3-digit code. This 3-digit code is your Card Security Code.</p></div>\
            <div class="col-sm-6"><img src="images/american-x-cvv.png" alt="CCV"><p>Look for the 4-digit code printed on the front of your card just above and to the right of your main credit card number. This 4-digit code is your Card Security Code.</p></div>\
          </div>\
          '
        });

        $("input[name='paymentPlan']").change(function() {
            var paymentPlan = $(this).val();
            var subs_plan = $("[name='plan_select']").val();
            $("input[name='paymentPlan']").parents('.radio').removeClass('active');
            $(this).parents('.radio').addClass('active');
            $("#recurring_option").val(paymentPlan);
            
            // Code to change order status according to plan and recurring option selected
            $('#oto_actual_payment_'+subs_plan).html("<h4>$"+$(".paymentPlan_"+paymentPlan+"_"+subs_plan).val()+"</h4>");
            if(paymentPlan == 'monthly'){
                $("#oto_gross_payment_"+subs_plan).html("$"+$(".paymentPlan_"+paymentPlan+"_"+subs_plan).val());
                $("#oto_savings_"+subs_plan).html("$0");
                
            }else if(paymentPlan == 'yearly'){
                var monthly_amount = $(".paymentPlan_monthly_"+subs_plan).val();
                var yearly_gross_amount = monthly_amount*12;
                var savings = yearly_gross_amount - $(".paymentPlan_"+paymentPlan+"_"+subs_plan).val();
                $("#oto_gross_payment_"+subs_plan).html("$"+yearly_gross_amount);
                $("#oto_savings_"+subs_plan).html("$-"+savings);
            }else if(paymentPlan == '2years'){
                var total_payment = $(".paymentPlan_"+paymentPlan+"_"+subs_plan).val();
                $("#oto_gross_payment_"+subs_plan).html("$"+total_payment*2);
                $("#oto_savings_"+subs_plan).html("$-"+total_payment);
            }
            
            
        });
        
        $("select[name='currency_select']").change(function() {
            $("#currency_option").val($(this).val());
        });
        
        $("select[name='plan_select']").change(function() {
            $('div.price-panel-container').addClass('hidden');
            $('#oto_'+$(this).val()+'_plan').removeClass('hidden');
            
            $('#paymentPlan2_'+$(this).val()).click();
        });

        // Validation
        $("#form").validate({
          rules: {
            fullName: {
              required: true
            },
            email:  {
              email:true, 
              required: true
            },
            phone:  { 
              required: true 
            },
            ccNumber: {
              required: true,
              digits:true
            },
            secutityCode: {
              required: true,
              digits:true
            }

          },
            messages: {
            fullName: "type your First Name and Last Name"
          },
          tooltip_options: {
            fullName: {
                        //trigger:'focus',
                        //placement:'right',
                        //html: true
                      },
          },
          highlight: function(element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
              var id_attr = "#" + $( element ).attr("id") + "1";
              $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
              var id_attr = "#" + $( element ).attr("id") + "1";
              $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
          },

          // submitHandler
          submitHandler: function(form) {
            $(form).ajaxSubmit();
          }
        });
      });
    </script>
  </body>
</html>
