<?php
function get_checkout_link_plimus($options) {
    extract( $options);

    switch ($billing_period) {
	    case "2Y":
		    switch ($item_name) {
			    case "Professional":
				    $contractId = Configure::read('Plans.Professional.PlimusContractID.2years');
				    $plan_cost = Configure::read('Plans.Professional.cost.2years');
				    break;

			    case "Basic":
				    $contractId = Configure::read('Plans.Basic.PlimusContractID.2years');
				    $plan_cost = Configure::read('Plans.Basic.cost.2years');
				    break;

			    case "Premium":
			    default:
				    $contractId = Configure::read('Plans.Premium.PlimusContractID.2years');
				    $plan_cost = Configure::read('Plans.Premium.cost.2years');
				    break;
		    }
		break;

        case "Y":
            switch ($item_name) {
                case "Professional":
                    $contractId = Configure::read('Plans.Professional.PlimusContractID.yearly');
                    $plan_cost = Configure::read('Plans.Professional.cost.yearly');
                    break;

                case "Basic":
                    $contractId = Configure::read('Plans.Basic.PlimusContractID.yearly');
                    $plan_cost = Configure::read('Plans.Basic.cost.yearly');
                    break;

                case "Premium":
                default:
                    $contractId = Configure::read('Plans.Premium.PlimusContractID.yearly');
                    $plan_cost = Configure::read('Plans.Premium.cost.yearly');
                    break;
            }
            break;

        case "M":
        default:
            switch ($item_name) {
                case "Professional":
                    $contractId = Configure::read('Plans.Professional.PlimusContractID.monthly');
                    $plan_cost = Configure::read('Plans.Professional.cost.monthly');
                    break;

                case "Basic":
                    $contractId = Configure::read('Plans.Basic.PlimusContractID.monthly');
                    $plan_cost = Configure::read('Plans.Basic.cost.monthly');
                    break;

                case "Premium":
                default:
                    $contractId = Configure::read('Plans.Premium.PlimusContractID.monthly');
                    $plan_cost = Configure::read('Plans.Premium.cost.monthly');
                    break;
            }
            break;

    }

    $params = array(
        /*
    //		'title'			=> OtonomicUser::get('gender')=='male' ? 'Mr' : 'Mrs',
    //		'companyName'	=> $settings['name'],

        'email' 		=> OtonomicUser::get('email'),
        'firstName' 	=> OtonomicUser::get('first_name'),
        'lastName' 		=> OtonomicUser::get('last_name'),
    //		'address1'		=> OtonomicSite::get('address'),
    //		'city'			=> OtonomicSite::get('city'),
    //		'country'		=> OtonomicSite::get('country'),
        'workPhone'		=> OtonomicSite::get('phone'),
        'contractId'	=> $contractId,
        'overrideRecurringPrice' => $plan_cost,
    //        'overridePrice' => $plan_cost,
//         'custom1'		=> Page2site::encrypt(OtonomicSite::get('id')."--".OtonomicUser::get('id')."--".OtonomicSite::get('slug'))
        */

        'email' 		=> $user['email'],
        'firstName' 	=> $user['first_name'],
        'lastName' 		=> $user['last_name'],
        'workPhone'		=> $user['phone'],
        'contractId'	=> $contractId,
        'overrideRecurringPrice' => $plan_cost,

    );

    $link = Configure::read('Plimus.action_url') . '?' . http_build_query($params);
    return str_replace('href="#"', 'href="'.$link.'"', $button_html);
}
?>