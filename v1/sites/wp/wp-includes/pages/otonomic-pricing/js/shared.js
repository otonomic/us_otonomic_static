var reference_time;

function ajaxTrack(category,label){
	data = {category: category, label: label};
	makeAjaxTrackCall(data);
}

function p2sTrack(category,event,label,value){
	var data = {category : category, event : event, label : label, value : value};
	makeAjaxTrackCall(data);
}

function makeAjaxTrackCall(data, callback){
    if(typeof WEBROOT == 'undefined'){
        WEBROOT = '/';
    }
    var url = WEBROOT + 'sites/track_click/';
    if(typeof site_id != "undefined" && site_id) { url += site_id  + '/';};
    
	jQuery.ajax({
    	url: url,
    	data:data,
        type: "POST",
        success: function(data, textStatus, jqXHR){
        	if(callback != undefined){
        		callback(data);
        	}
        }
	});
}

function trackEvent(category, event, label, value) {
    if(typeof(value) == 'undefined') { value = null; }
    console.log('Track event in GA - '+category+' >>> '+event+' >>> '+label+' >>> '+value);

	if(typeof(_gaq) != 'undefined') {
        category = "" + jQuery.camelCase(category);
        event = "" + jQuery.camelCase(event);
        label = "" + label;
        if(value!=null) { value = parseInt(value); }
		_gaq.push(['_trackEvent', category, event, label, value]);
	}

    if(typeof(_kmq) != 'undefined') {
        _kmq.push(['record', category+'.'+event, {'label':label, 'value':value}]);
    }
}

function trackPageView(url) {
    if(typeof(_gaq) != 'undefined') {
        _gaq.push(['_trackPageview', '/virtual_pageviews/' + url]);
    }
}

function preview(text, max_length) {
	if(typeof(text) != 'undefined' && text.length > max_length) {
		text = text.substr(0,max_length) + '...';
	}
	return text;
}

function urlencode(str) {
	return escape(str).replace(/\+/g,'%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
}

function sql2jsDate(sqlDate) {
	return sqlDate.replace(/^(....).(..).(.{11}).*$/, "$1/$2/$3");
}

function show_modal_whats_next() {
    $("#hidden_button_modal_whats_next").trigger('click');
    reference_time = new Date().getTime();
}

String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.substr(1);
};

function wrapShortcodeInRedactor(code) {
    return '<div class="p2s_widget"><img class="p2s_widget_editor_replacement" src="/img/editor/filler.gif" /><span class="p2s_widget_editor_shortcode">' + code + '</span></div>';
}

function add_template_page() {
	var $this = jQuery(this);
	var field = $this.data('associated-field');
	$input_el = jQuery('#'+field);
	var template_id = $this.data('associated-template');
	var field = $this.data('database-field');

	if($this.data('error_selector') == undefined){
		$errSel = jQuery("#addPageMessages");
	}else{
		//$errSel = jQuery("#" + $this.data('error_selector'));
		$errSel = $this.closest(".bootstro_tb").find("#" + $this.data('error_selector'));
	}
	$errSel.find("label").hide();

	var val = $input_el.val();
	if(val == '') {
		$input_el.focus();
		return false;
	}

	jQuery.ajax({
		type: "POST",
		url:  WEBROOT +"custom_pages/addTemplatePage/" + site_id,
		data: { template_id: template_id, field: field,  value: val },
		success: function(data, textStatus, jqXHR) {
			var obj = jQuery.parseJSON(data);
			$form = $errSel.closest("form.modal_form");

			if(obj.status == 'success') {
				$errSel.find("label.alert-success").html(obj.message).show().delay(5000).slideUp();
				$form.find("button[type='submit']").removeAttr("disabled");
				$form.find("ul.pager > li.previous").removeClass("disabled");

			} else {
				$errSel.find("label.alert-error").html(obj.message).show();
				$form.find("button[type='submit']").attr("disabled", "disabled");
				$form.find("ul.pager > li.previous").addClass("disabled");
				$t.focus();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			$errSel.find("label.alert-error").html('An error occured').show();
			$form = $errSel.closest("form.modal_form");
			$form.find("button[type='submit']").attr("disabled", "disabled");
			$form.find("ul.pager > li.previous").addClass("disabled");
			$t.focus();

			console.debug(textStatus + ": " + errorThrown);
		}
	});

	return false;
}


jQuery(document).ready(function($) {
    //$('a[rel=tooltip]').tooltip();

    $('#flashMessage').delay(3000).fadeOut(1000);
    $('#flashMessage').click(function() { $(this).fadeOut(1000); })

    //$('.add_template_page').live('click',add_template_page);  //live function depereceted from jQuery 1.8 and higher
    $(document).on('click', '.add_template_page', add_template_page);

    function redirectToNewSite(url) {
        window.location.href = url;
    }

    if (typeof($.sortable) != "undefined") {
        jQuery( ".sortable" ).sortable();
    }

    if (typeof($.tipsy) != "undefined") {
        $('.etip img, a.etip, span.etip, .etipsy').tipsy({
            gravity : 'e',
            live : true
        });
        $('.wtip img, a.wtip, span.wtip, .wtipsy').tipsy({
            gravity : 'w',
            live : true
        });
        $('.ntip img, a.ntip, span.ntip, .ntipsy').tipsy({
            gravity : 'n',
            live : true
        });
        $('.tip img, a.tip, span.tip, .tipsy').tipsy({
            gravity : 's',
            live : true
        });

        $('#zenbox_tab').tipsy({
            gravity : 'e',
            live : true
        });
    }

    // Google Analytics Event Tracking - Click Events
    $(document).on('mousedown','.track_event', function(e) {
        default_category = default_label = '';
        default_event = 'Click';
        default_value = null;
        default_log_to_db = 1;

        var $this       = $(this);

        if($this.attr("data-tag-id")) {
            tags = get_tags($this.attr("data-tag-id"));
            category = tags[0] || default_category;
            event = tags[1]    || default_event;
            label = tags[2]    || default_label;
            value = tags[3]    || default_value;
            ajax_track = tags[4]    || default_log_to_db;

        } else {
            var category    = $this.attr("data-ga-category") || default_category;
            var event       = $this.attr("data-ga-event")    || default_event;
            var label       = $this.attr("data-ga-label")    || default_label;
            var value       = $this.attr("data-ga-value")    || default_value;

            var ajax_track  = $this.attr("data-ajax-track") || default_log_to_db;
        }

        trackEvent(category, event, label, value);

        if(ajax_track == 1){
            p2sTrack(category, event, label, value);
        }
    });

    // Google Analytics Event Tracking - Click Events
    $('.ga_track_click_event').on('mousedown', function(e) {
        var $this = $(this);
        var category = $this.attr("data-ga-category") || '';
        var event    = "Click";
        var label = $this.attr("data-ga-label") || '';
        var value = $this.attr("data-ga-value") || null;

        var ajax_track = $this.attr("data-ajax-track") || 1;

        trackEvent(category, event, label, value);
        if(ajax_track == 1){
            ajaxTrack(category,event,label,value);
        }
    });

    // Google Analytics Event Tracking - Click Events
    $('.upgrade_link').on('mousedown', function(e) {
        var $this = $(this);
        var category = 'Upgrade';
        var event = $this.attr("data-ga-event") || '';
        var label = $this.attr("data-ga-label") || document.URL;

        if(!event) {
            var str = $this.attr('href').split('reason:')[1];
            event = str.substr(0,str.indexOf('/'));
            if(!event) { event = str;}
        }

        ajaxTrack(category, event, label);
        trackEvent(category, event, label);
    });
});

function bindModalPromoteSite(){
	var stepSlider = $('.steps_holder').bxSlider({
		pager:false,
		infiniteLoop:false,
		controls:false
	});

	$('.promote_your_site .next').unbind('click').bind('click',function (e) {
		e.preventDefault();
		$('#steps_box .top_head .actived').removeClass('actived');
		$('#steps_box .top_head .step:eq(1)').addClass('actived');
		stepSlider.goToSlide(1);
	});

	$('.steps_content .prev').unbind('click').bind('click',function (e) {
		e.preventDefault();
		$('#steps_box .top_head .actived').removeClass('actived');
		$('#steps_box .top_head .step:eq(0)').addClass('actived');
		stepSlider.goToSlide(0);
	});

	$('.get_domain .step_content').css({display:'block'});

	$('.steps_content .step_accordion > .title input').unbind('change').bind('change',function (e) {
		e.preventDefault();
		var parent = $(this).parents('.step_accordion');

		if ( !parent.hasClass('actived') ) {
			parent.addClass('actived');
			parent.find('.step_content').slideDown(200);
			parent.siblings().find('.step_content').slideUp(200,function () {
				parent.siblings().removeClass('actived');
			});
		}
	});
}


function calculateRectangularImageShift($el, avg_ratio) {
    var W = 220;
    var H = 200;
    var ratio = intToFloat(H)/intToFloat(W);

    img_W = $el.width();
    img_H = $el.height();
    console.log(img_W);
    console.log(img_H);
    img_ratio = intToFloat(img_H) / intToFloat(img_W);

    new_W = img_ratio > ratio     ? W : img_H/img_ratio;
    new_H = img_ratio > avg_ratio ? new_W * img_ratio : H;

    shift_W = -(new_W-W)/2;
    shift_H = -(new_H-H)/2;

    console.log(new_W);
    console.log(new_H);

    jQuery($el).width(new_W).height(new_H).css('margin-left',shift_W).css('margin-right',shift_H);
}

function get_tags(tag_id) {
    switch(tag_id) {
        case "upgrade_engagement_albums_limit_small_upgrade_btn":
            return ["Upgrade engagement", "Upgrade Click", "Portfolio"];
            break;

        case "upgrade_engagement_albums_limit_small_close_btn":
            return ["Upgrade engagement", "Close", "Portfolio"];
            break;

        case "social_share_box_invite_friends":
            return ["Social engagement", "Click", "Invite"];
            break;
        case "social_share_box_share_button":
            return ["Social engagement", "Click", "Share"];
            break;
        case "social_share_box_share_twitter":
            return ["Social engagement", "Click", "Twitter"];
            break;
        case "social_share_box_share_email":
            return ["Social engagement", "Click", "Mail"];
            break;
        case "social_share_box_share_google_plus":
            return ["Social engagement", "Click", "Google"];
            break;
    }

    return [];
}