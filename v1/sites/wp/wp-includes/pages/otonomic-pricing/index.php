<?php
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
//define( 'SHORTINIT', TRUE );
//require_once('../../../wp-config.otonomic.com.php');
require_once('../../../wp-load.php');

define('URL_MARKETING_SITE', 'http://otonomic.com');
define('WHITE_LABEL_EMAIL_SUPPORT', 'support@otonomic.com');

$user = [
    'address' => '',
    'email' => '',
    'phone' => '',
    'name' => '',
    'first_name' => '',
    'last_name' => ''
];


$subdomain = isset($_GET['subdomain']) ? $_GET['subdomain'] : explode('.', $_SERVER['HTTP_HOST'])[0];
$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASSWORD);
$query = 'SELECT * FROM otonomic_sites WHERE slug = '.$subdomain;

foreach($db->query($query) as $row) {
    $user = [
        'address' => $row['address'],
        'email' => $row['email'],
        'phone' => $row['phone'],
        'name' => $row['name'],
        'first_name' => $row['name'],
        'last_name' => ''
    ];
    break;
}

require_once('../../../migration/helpers/Page2site.php');
require_once('gateways/plimus.php');
// require_once('../../../wp-content/mu-plugins/otonomic-first-session/includes/classes/entities/OtonomicSite.php');
// require_once('../../../wp-content/mu-plugins/otonomic-first-session/includes/classes/entities/OtonomicUser.php');

$payment_method = 'plimus';
$reason = isset($_GET['reason']) ? $_GET['reason'] : "";

try {
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Otonomic Pricing Page</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <meta property="og:title" content="Even a small business deserves a big spotlight." />
    <meta property="og:site_name" content="otonomic"/>
    <meta property="og:description" content="In under 20 seconds , we will turn your Facebook page into a professional website"/>
    <meta property="og:url" content="http://otonomic.com"/>
    <meta property="og:image" content="http://otonomic.com/images/section-home-bg.png"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom style -->
    <link href="css/pricingPage.css" rel="stylesheet">
    <!-- Google+ -->
    <link href="https://plus.google.com/112126439055007134666" rel="publisher">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	  <script>
		  var settings = {localhost:<?php echo LOCALHOST; ?>, p2s_local:<?php echo P2S_LOCAL; ?>, user_id:<?php echo get_current_user_id(); ?>, site_id:<?php echo get_current_blog_id(); ?> };
	  </script>

      <!-- Facebook Conversion Code for Pricing View -->
      <script>(function() {
              var _fbq = window._fbq || (window._fbq = []);
              if (!_fbq.loaded) {
                  var fbds = document.createElement('script');
                  fbds.async = true;
                  fbds.src = '//connect.facebook.net/en_US/fbds.js';
                  var s = document.getElementsByTagName('script')[0];
                  s.parentNode.insertBefore(fbds, s);
                  _fbq.loaded = true;
              }
          })();
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6018983658230', {'value':'0.00','currency':'USD'}]);
      </script>
      <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018983658230&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
      <!-- END Facebook Conversion Code for Pricing View -->


  </head>
  <body>
<div id="pricing_page" class="tb3">
    <div class="container-fluid">
     <!-- top navbar -->
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="navbar-header">
                <button type="button" class="navbar-toggle track_event track_hover hidden" id="menu-toggle" data-toggle="offcanvas" data-target=".sidebar-nav" data-ga-category="Marketing Website"  data-ga-event="Menu Click" data-ga-event-hover="Menu Hover" data-ajax-track="1">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <div class="upper-bar">
                  <div class="center-logo-holder">
                      <div class="center-logo center-block">
                        <img src="images/otonomic-logo.png" class="logo-img" alt="otonomic logo" width="140px;">
                      </div>
                    </div>
                      <div class="back-btn-holder">
                        <a href="javsscript:history.go(-1);" id="back-btn" class="btn btn-ttc-gray back-btn">
                            <img src="images/back-icon.png" alt="Back"><span class="hidden-xs">Back to Your Website</span>
                        </a>
                      </div>
                    </div>
                    <div class="lower-bar">
                    <a class="navbar-brand hidden-xs" href="#"><img src="images/otonomic-logo.png" alt="otonomic logo"></a>
                        <div class="btn-holder">
                            <?php
                            $price = Configure::read('Plans.Professional.cost.yearly')['year'];
                            echo get_checkout_link_plimus(array(
                                'user' => $user,
                                'billing_period' => 'Y',
                                'item_name' => "Professional",
                                'cost' => $price,
                                'button_html' => '<a href="#"
                                            class="button important track_event pay_link"
                                            data-ga-category="Pricing"
                                            data-ga-label="Trial Plan - Premium - Yearly - Reason:'.$reason.'"
                                            data-ga-event="Pay"
                                            data-ga-value="'.$price.'"
                                            data-ajax-track="1"
                                            style="display: inline-block;"
                                       >

                                            <button class="btn btn-ttc">
                                                <img src="images/btn-badge.png">
                                                Upgrade to <b>Professional</b> plan
                                            </button>
                                       </a>
                                  '
                            ));?>
                        </div>
                        <div class="text-holder">$<?= Configure::read('Plans.Professional.cost.yearly')['month']?> Per month, Billed annually. <span><br/></span>You can cancel any time.</div>
                    </div>
          </div>
      </div>
    </div>

    <div class="container-fluid">
      <section class="pricing"><!-- Section pricing -->
        <div class="row section-pricing">
            <div class="col-xs-12 text-center">
            <h1 class="title">Upgrade today to boost your site!</h1>
            <p class="sub-title">Get your own domain, sell or get more business online, and much more.</p>
            </div>

            <div class="col-xs-12 text-center">
                <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-payment-plan btn-2years" data-ga-label="2-years">
                        <input type="radio" name="payment-plan" id="2yearly" > 2 Years
                      </label>
                      <label class="btn btn-payment-plan btn-yearly" data-ga-label="yearly">
                        <input type="radio" name="payment-plan" id="yearly" > Yearly
                      </label>
                      <label class="btn btn-payment-plan btn-monthly active" data-ga-label="monthly">
                        <input type="radio" name="payment-plan" id="monthly"> Monthly
                      </label>
                </div>
            </div>













            <div class="col-xs-12 text-center pricing_plan_container plan_2yrs" id="pricing-table-2years">
            <div class="row pricing-table center-block">
            <div class="col-xs-4 vip-table">
                <div class="table-header vip-table-header">
                    <h1>Premium
                        <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Premium.cost.2years')['month']);?>
                        <sup>$</sup>
                        <strong><?= $price['round'];?></strong>
                        <sub><?= $price['fraction'];?></sub>
                    </h1>
                    <p>Per month, Billed once every 2 years</p>
                    <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                </div>
                <div class="table-content vip-table-content">
                    <ul>
                        <li data-index="0" class="have">
                            Custom Web Address
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                        </li>
                        <li class="have" data-index="1">
                            2 Emails
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                        </li>
                        <li class="have" data-index="2">
                            Sell unlimited products
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with unlimited products or services. Otonomic will not charge any transaction fees."></span>
                        </li>
                        <li class="have" data-index="3">
                            Online Booking
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Manage all your appointments from one place, automatically synced with your calendar. Your clients will be able  to book & change appointments online, upon your availability, and will receive automatic SMS and email reminders."></span>
                        </li>
                        <li class="have" data-index="4">
                            Add Unlimited pages
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with  unlimited new pages."></span>
                        </li>
                        <li class="have" data-index="5">
                            Phone, Chat & Email Support
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via phone, online chat and/or email."></span>
                        </li>
                        <li class="have" data-index="6">
                            3 Free Apps
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 3 free apps from our app store. Apps add features to your site and help you grow your business."></span>
                        </li>
                        <li class="have" data-index="7">
                            Advanced Search Engine Boost
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Personalized service from  a digital expert who will work on your website's so it's found on search engines (Google, Bing, Yahoo)."></span>
                        </li>
                        <li class="have" data-index="8">
                            20GB
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                        </li>
                        <li class="have" data-index="9">
                            Unlimited
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                        </li>
                        <li class="have" data-index="10">
                            Custom Design
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Our graphic design pros will work with you on making your website one of a kind."></span>
                        </li>
                    </ul>

                    <?php
                    $price = Configure::read('Plans.Premium.cost.2years')['year'];
                    echo get_checkout_link_plimus(array(
                        'user' => $user,
                        'billing_period' => '2Y',
                        'item_name' => "Premium",
                        'cost' => $price,
                        'button_html' => '<a href="#"
                                        class="button important track_event pay_link"
                                        data-ga-category="Pricing"
                                        data-ga-label="Trial Plan - Premium - 2 years - Reason:'.$reason.'"
                                        data-ga-event="Pay"
                                        data-ga-value="'.$price.'"
                                        data-ajax-track="1"
                                        style="display: inline-block;"
                                        >
                                            <button class="btn btn-ttc">Upgrade to <b>Premium</b> Plan</button>
                                        </a>
                                    '
                    ));?>
                </div>
            </div>
            <div class="col-xs-5 business-table">
                <div class="business-table-dude">
                    <img class="" src="images/megaphone-dude.png">
                    <p class="business-table-dude-text">50% Off!</p>
                </div>
                <div class="table-header business-table-header">
                    <h1>Professional
                        <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Professional.cost.2years')['month']);?>
                        <sup>$</sup>
                        <strong><?= $price['round'];?></strong>
                        <sub><?= $price['fraction'];?></sub>
                    </h1>
                    <p>Per month, Billed once every 2 years</p>
                    <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                    <img class="business-table-badge" src="images/business-table-badge.png">
                </div>
                <div class="table-content business-table-content">
                    <ul>
                        <li data-index="0" class="have">
                            Custom Web Address
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                        </li>
                        <li class="have" data-index="1">
                            2 Emails
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                        </li>
                        <li data-index="2" class="have">
                            Sell up to 10 products
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 10 products or services. Otonomic will not charge any transaction fees."></span>
                        </li>
                        <li data-index="3" class="have">
                            Booking Request
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your clients will be able to send requests for appointments directly from your site. You'll then contact them to confirm or cancel their request."></span>
                        </li>
                        <li data-index="4" class="have">
                            Add 8 pages
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 8 new pages."></span>
                        </li>
                        <li data-index="5" class="have">
                            Chat & Email Support
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via online chat and email"></span>
                        </li>
                        <li data-index="6" class="have">
                            1 Free App
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 1 free app from our app store. Apps add features to your site and help you grow your business."></span>
                        </li>
                        <li data-index="7" class="have">
                            Limited Search Engine Boost
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc)."></span>
                        </li>
                        <li data-index="8" class="have">
                            3GB
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                        </li>
                        <li data-index="9" class="have">
                            5GB
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                        </li>
                        <li data-index="10" class="have-not">
                            Custom Design
                        </li>
                    </ul>

                    <?php
                    $price = Configure::read('Plans.Professional.cost.2years')['year'];
                    echo get_checkout_link_plimus(array(
                        'user' => $user,
                        'billing_period' => '2Y',
                        'item_name' => "Professional",
                        'cost' => $price,
                        'button_html' => '<a href="#"
                                                class="button important track_event pay_link"
                                                data-ga-category="Pricing"
                                                data-ga-label="Trial Plan - Premium - 2 Years - Reason:'.$reason.'"
                                                data-ga-event="Pay"
                                                data-ga-value="'.$price.'"
                                                data-ajax-track="1"
                                                style="display: inline-block;"
                                              >
                                              <button class="btn btn-ttc"><img src="images/btn-badge.png">Upgrade to <b>Professional</b> Plan</button>
                                          </a>
                                      '
                    ));?>
                </div>
            </div>
            <div class="col-xs-3 free-table">
                <div class="table-header free-table-header">
                    <h1>Basic
                        <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Basic.cost.2years')['month']);?>
                        <sup>$</sup>
                        <strong><?= $price['round'];?></strong>
                        <sub><?= $price['fraction'];?></sub>
                    </h1>
                    <p>Per month, Billed once every 2 years</p>
                    <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                </div>
                <div class="table-content free-table-content">
                    <ul>
                        <li data-index="0" class="have">
                            Otonomic.com Web Address
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Otonomic.com subdomain (www.yoursite.otonomic.com)"></span>
                        </li>
                        <li data-index="1" class="have-not">Emails</li>
                        <li data-index="2" class="have">
                            Sell up to 3 products
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 3 products or services. Otonomic will not charge any transaction fees."></span>
                        </li>
                        <li data-index="3" class="have-not">Booking</li>
                        <li data-index="4" class="have">
                            Add 2 pages
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 2 new pages."></span>
                        </li>
                        <li data-index="5" class="have">
                            Email Support
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via email."></span>
                        </li>
                        <li data-index="6" class="have-not">Free Apps</li>
                        <li data-index="7" class="have">
                            Limited Search Engine Boost
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc.)"></span>
                        </li>
                        <li data-index="8" class="have">
                            500MB
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                        </li>
                        <li data-index="9" class="have">
                            1GB
                            <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site. "></span>
                        </li>
                        <li data-index="10" class="have-not">Custom Design</li>
                    </ul>
                    <?php
                    $price = Configure::read('Plans.Premium.cost.monthly');
                    echo get_checkout_link_plimus(array(
                        'user' => $user,
                        'billing_period' => '2Y',
                        'item_name' => "Basic",
                        'cost' => $price,
                        'button_html' => '<a href="#"
                                                  class="button important track_event pay_link"
                                                  data-ga-category="Pricing"
                                                  data-ga-label="Trial Plan - Basic - 2 Years - Reason:'.$reason.'"
                                                  data-ga-event="Pay"
                                                  data-ga-value="'.$price.'"
                                                  data-ajax-track="1"
                                                  style="display: inline-block;"
                                                  >
                                                      <button class="btn btn-ttc">Upgrade to <b>Basic</b> Plan</button>
                                                  </a>
                                              '
                    ));?>
                </div>
            </div>
            </div>
            </div>






























            <div class="col-xs-12 text-center pricing_plan_container plan_yr" id="pricing-table-yearly">
                    <div class="row pricing-table center-block">
                        <div class="col-xs-4 vip-table">
                            <div class="table-header vip-table-header">
                                <h1>Premium
                                    <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Premium.cost.yearly')['month']);?>
                                    <sup>$</sup>
                                    <strong><?= $price['round'];?></strong>
                                    <sub><?= $price['fraction'];?></sub>
                                </h1>
                                <p>Per month, Billed annually</p>
                                <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                            </div>
                            <div class="table-content vip-table-content">
                                <ul>
                                    <li data-index="0" class="have">
                                      Custom Web Address
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                                    </li>
                                    <li class="have" data-index="1">
                                      2 Emails
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                                    </li>
                                    <li class="have" data-index="2">
                                      Sell unlimited products
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with unlimited products or services. Otonomic will not charge any transaction fees."></span>
                                    </li>
                                    <li class="have" data-index="3">
                                      Online Booking
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Manage all your appointments from one place, automatically synced with your calendar. Your clients will be able  to book & change appointments online, upon your availability, and will receive automatic SMS and email reminders."></span>
                                    </li>
                                    <li class="have" data-index="4">
                                      Add Unlimited pages
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with  unlimited new pages."></span>
                                    </li>
                                    <li class="have" data-index="5">
                                      Phone, Chat & Email Support
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via phone, online chat and/or email."></span>
                                    </li>
                                    <li class="have" data-index="6">
                                      3 Free Apps
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 3 free apps from our app store. Apps add features to your site and help you grow your business."></span>
                                    </li>
                                    <li class="have" data-index="7">
                                      Advanced Search Engine Boost
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Personalized service from  a digital expert who will work on your website's so it's found on search engines (Google, Bing, Yahoo)."></span>
                                    </li>
                                    <li class="have" data-index="8">
                                      20GB
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                                    </li>
                                    <li class="have" data-index="9">
                                      Unlimited
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                                    </li>
                                    <li class="have" data-index="10">
                                      Custom Design
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Our graphic design pros will work with you on making your website one of a kind."></span>
                                    </li>
                                </ul>

                                <?php
                                $price = Configure::read('Plans.Premium.cost.yearly')['year'];
                                echo get_checkout_link_plimus(array(
                                    'user' => $user,
                                    'billing_period' => 'Y',
                                    'item_name' => "Premium",
                                    'cost' => $price,
                                    'button_html' => '<a href="#"
                                        class="button important track_event pay_link"
                                        data-ga-category="Pricing"
                                        data-ga-label="Trial Plan - Premium - Yearly - Reason:'.$reason.'"
                                        data-ga-event="Pay"
                                        data-ga-value="'.$price.'"
                                        data-ajax-track="1"
                                        style="display: inline-block;"
                                        >
                                            <button class="btn btn-ttc">Upgrade to <b>Premium</b> Plan</button>
                                        </a>
                                    '
                                ));?>
                              </div>
                        </div>
                        <div class="col-xs-5 business-table">
                                <div class="business-table-dude">
                                    <img class="" src="images/megaphone-dude.png">
                                    <p class="business-table-dude-text">33% Off!</p>
                                </div>
                                <div class="table-header business-table-header">
                                    <h1>Professional
                                        <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Professional.cost.yearly')['month']);?>
                                        <sup>$</sup>
                                        <strong><?= $price['round'];?></strong>
                                        <sub><?= $price['fraction'];?></sub>
                                    </h1>
                                    <p>Per month, Billed annually</p>
                                    <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                                    <img class="business-table-badge" src="images/business-table-badge.png">
                              </div>
                              <div class="table-content business-table-content">
                                    <ul>
                                        <li data-index="0" class="have">
                                          Custom Web Address
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                                        </li>
                                        <li class="have" data-index="1">
                                          2 Emails
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                                        </li>
                                        <li data-index="2" class="have">
                                          Sell up to 10 products
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 10 products or services. Otonomic will not charge any transaction fees."></span>
                                        </li>
                                        <li data-index="3" class="have">
                                          Booking Request
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your clients will be able to send requests for appointments directly from your site. You'll then contact them to confirm or cancel their request."></span>
                                        </li>
                                        <li data-index="4" class="have">
                                          Add 8 pages
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 8 new pages."></span>
                                        </li>
                                        <li data-index="5" class="have">
                                          Chat & Email Support
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via online chat and email"></span>
                                        </li>
                                        <li data-index="6" class="have">
                                          1 Free App
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 1 free app from our app store. Apps add features to your site and help you grow your business."></span>
                                        </li>
                                        <li data-index="7" class="have">
                                          Limited Search Engine Boost
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc)."></span>
                                        </li>
                                        <li data-index="8" class="have">
                                          3GB
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                                        </li>
                                        <li data-index="9" class="have">
                                          5GB
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                                        </li>
                                        <li data-index="10" class="have-not">
                                          Custom Design
                                        </li>
                                    </ul>

                                  <?php
                                  $price = Configure::read('Plans.Professional.cost.yearly')['year'];
                                  echo get_checkout_link_plimus(array(
                                      'user' => $user,
                                      'billing_period' => 'Y',
                                      'item_name' => "Professional",
                                      'cost' => $price,
                                      'button_html' => '<a href="#"
                                                class="button important track_event pay_link"
                                                data-ga-category="Pricing"
                                                data-ga-label="Trial Plan - Premium - Yearly - Reason:'.$reason.'"
                                                data-ga-event="Pay"
                                                data-ga-value="'.$price.'"
                                                data-ajax-track="1"
                                                style="display: inline-block;"
                                              >
                                              <button class="btn btn-ttc"><img src="images/btn-badge.png">Upgrade to <b>Professional</b> Plan</button>
                                          </a>
                                      '
                                      ));?>
                              </div>
                        </div>
                        <div class="col-xs-3 free-table">
                              <div class="table-header free-table-header">
                                    <h1>Basic 
                                      <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Basic.cost.yearly')['month']);?>
                                      <sup>$</sup>
                                      <strong><?= $price['round'];?></strong>
                                      <sub><?= $price['fraction'];?></sub>
                                    </h1>
                                    <p>Per month, Billed annually</p>
                                    <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                              </div>
                              <div class="table-content free-table-content">
                                    <ul>
                                        <li data-index="0" class="have">
                                          Otonomic.com Web Address
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Otonomic.com subdomain (www.yoursite.otonomic.com)"></span>
                                        </li>
                                        <li data-index="1" class="have-not">Emails</li>
                                        <li data-index="2" class="have">
                                          Sell up to 3 products
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 3 products or services. Otonomic will not charge any transaction fees."></span>
                                        </li>
                                        <li data-index="3" class="have-not">Booking</li>
                                        <li data-index="4" class="have">
                                          Add 2 pages
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 2 new pages."></span>
                                        </li>
                                        <li data-index="5" class="have">
                                          Email Support
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via email."></span>
                                        </li>
                                        <li data-index="6" class="have-not">Free Apps</li>
                                        <li data-index="7" class="have">
                                          Limited Search Engine Boost
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc.)"></span>
                                        </li>
                                        <li data-index="8" class="have">
                                          500MB
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                                        </li>
                                        <li data-index="9" class="have">
                                          1GB
                                          <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site. "></span>
                                        </li>
                                        <li data-index="10" class="have-not">Custom Design</li>
                                    </ul>
                                    <?php
                                      $price = Configure::read('Plans.Premium.cost.monthly');
                                      echo get_checkout_link_plimus(array(
                                          'user' => $user,
                                          'billing_period' => 'Y',
                                          'item_name' => "Basic",
                                          'cost' => $price,
                                          'button_html' => '<a href="#"
                                                  class="button important track_event pay_link"
                                                  data-ga-category="Pricing"
                                                  data-ga-label="Trial Plan - Basic - Yearly - Reason:'.$reason.'"
                                                  data-ga-event="Pay"
                                                  data-ga-value="'.$price.'"
                                                  data-ajax-track="1"
                                                  style="display: inline-block;"
                                                  >
                                                      <button class="btn btn-ttc">Upgrade to <b>Basic</b> Plan</button>
                                                  </a>
                                              '
                                      ));?>
                              </div>
                        </div>
                    </div>
            </div>





            <div class="col-xs-12 text-center pricing_plan_container plan_mo" id="pricing-table-monthly">
                <div class="row pricing-table center-block">
                    <div class="col-xs-4 vip-table">
                        <div class="table-header vip-table-header">
                            <h1>Premium
                                <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Premium.cost.monthly'));?>
                                <sup>$</sup>
                                <strong><?= $price['round'];?></strong>
                                <sub><?= $price['fraction'];?></sub>
                            </h1>
                            <p>Per month, Billed monthly</p>
                            <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                        </div>
                        <div class="table-content vip-table-content">
                            <ul>
                                <li data-index="0" class="have">
                                      Custom Web Address
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                                    </li>
                                    <li class="have" data-index="1">
                                      2 Emails
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                                    </li>
                                    <li class="have" data-index="2">
                                      Sell unlimited products
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with unlimited products or services. Otonomic will not charge any transaction fees."></span>
                                    </li>
                                    <li class="have" data-index="3">
                                      Online Booking
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Manage all your appointments from one place, automatically synced with your calendar. Your clients will be able  to book & change appointments online, upon your availability, and will receive automatic SMS and email reminders."></span>
                                    </li>
                                    <li class="have" data-index="4">
                                      Add Unlimited pages
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with  unlimited new pages."></span>
                                    </li>
                                    <li class="have" data-index="5">
                                      Phone, Chat & Email Support
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via phone, online chat and/or email."></span>
                                    </li>
                                    <li class="have" data-index="6">
                                      3 Free Apps
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 3 free apps from our app store. Apps add features to your site and help you grow your business."></span>
                                    </li>
                                    <li class="have" data-index="7">
                                      Advanced Search Engine Boost
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Personalized service from  a digital expert who will work on your website's so it's found on search engines (Google, Bing, Yahoo)."></span>
                                    </li>
                                    <li class="have" data-index="8">
                                      20GB
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                                    </li>
                                    <li class="have" data-index="9">
                                      Unlimited
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                                    </li>
                                    <li class="have" data-index="10">
                                      Custom Design
                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Our graphic design pros will work with you on making your website one of a kind."></span>
                                    </li>
                            </ul>

                            <?php
                            $price = Configure::read('Plans.Premium.cost.monthly');
                            echo get_checkout_link_plimus(array(
                                'user' => $user,
                                'billing_period' => 'Y',
                                'item_name' => "Premium",
                                'cost' => $price,
                                'button_html' => '<a href="#"
                                        class="button important track_event pay_link"
                                        data-ga-category="Pricing"
                                        data-ga-label="Trial Plan - Premium - Monthly - Reason:'.$reason.'"
                                        data-ga-event="Pay"
                                        data-ga-value="'.$price.'"
                                        data-ajax-track="1"
                                        style="display: inline-block;"
                                        >
                                            <button class="btn btn-ttc">Upgrade to <b>Premium</b> Plan</button>
                                        </a>
                                    '
                            ));?>
                        </div>
                    </div>
                    <div class="col-xs-5 business-table">
                        <div class="business-table-dude">
                        </div>
                        <div class="table-header business-table-header">
                            <h1>Professional
                                <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Professional.cost.monthly'));?>
                                <sup>$</sup>
                                <strong><?= $price['round'];?></strong>
                                <sub><?= $price['fraction'];?></sub>
                            </h1>
                            <p>Per month, Billed monthly</p>
                            <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                            <img class="business-table-badge" src="images/business-table-badge.png">
                        </div>
                        <div class="table-content business-table-content">
                            <ul>
                              <li data-index="0" class="have">
                                Custom Web Address
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your domain is your web address (e.g. www.yourdomain.com). Only for annual subscriptions."></span>
                              </li>
                              <li class="have" data-index="1">
                                2 Emails
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Includes 2 email addresses associated with your custom domain name (e.g. info@yourdomain.com). Only for annual subscriptions."></span>
                              </li>
                              <li data-index="2" class="have">
                                Sell up to 10 products
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 10 products or services. Otonomic will not charge any transaction fees."></span>
                              </li>
                              <li data-index="3" class="have">
                                Booking Request
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your clients will be able to send requests for appointments directly from your site. You'll then contact them to confirm or cancel their request."></span>
                              </li>
                              <li data-index="4" class="have">
                                Add 8 pages
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 8 new pages."></span>
                              </li>
                              <li data-index="5" class="have">
                                Chat & Email Support
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via online chat and email"></span>
                              </li>
                              <li data-index="6" class="have">
                                1 Free App
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Plan includes 1 free app from our app store. Apps add features to your site and help you grow your business."></span>
                              </li>
                              <li data-index="7" class="have">
                                Limited Search Engine Boost
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc)."></span>
                              </li>
                              <li data-index="8" class="have">
                                3GB
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                              </li>
                              <li data-index="9" class="have">
                                5GB
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site."></span>
                              </li>
                              <li data-index="10" class="have-not">
                                Custom Design
                              </li>
                            </ul>

                            <?php
                            $price = Configure::read('Plans.Professional.cost.monthly');
                            echo get_checkout_link_plimus(array(
                                'user' => $user,
                                'billing_period' => 'Y',
                                'item_name' => "Professional",
                                'cost' => $price,
                                'button_html' => '<a href="#"
                                                class="button important track_event pay_link"
                                                data-ga-category="Pricing"
                                                data-ga-label="Trial Plan - Premium - Monthly - Reason:'.$reason.'"
                                                data-ga-event="Pay"
                                                data-ga-value="'.$price.'"
                                                data-ajax-track="1"
                                                style="display: inline-block;"
                                              >
                                              <button class="btn btn-ttc"><img src="images/btn-badge.png">Upgrade to <b>Professional</b> Plan</button>
                                          </a>
                                      '
                            ));?>
                        </div>
                    </div>
                    <div class="col-xs-3 free-table">
                        <div class="table-header free-table-header">
                            <h1>Basic 
                              <?php $price = Page2site::getRoundPriceAndFraction(Configure::read('Plans.Basic.cost.monthly'));?>
                              <sup>$</sup>
                              <strong><?= $price['round'];?></strong>
                              <sub><?= $price['fraction'];?></sub>
                            </h1>
                            <p>Per month, Billed monthly</p>
                            <span class="glyphicon glyphicon-chevron-down visible-xs"></span>
                        </div>
                        <div class="table-content free-table-content">
                            <ul>
                              <li data-index="0" class="have">
                                Otonomic.com Web Address
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Otonomic.com subdomain (www.yoursite.otonomic.com)"></span>
                              </li>
                              <li data-index="1" class="have-not">Emails</li>
                              <li data-index="2" class="have">
                                Sell up to 3 products
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Online store with up to 3 products or services. Otonomic will not charge any transaction fees."></span>
                              </li>
                              <li data-index="3" class="have-not">Booking</li>
                              <li data-index="4" class="have">
                                Add 2 pages
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Customize your website with up to 2 new pages."></span>
                              </li>
                              <li data-index="5" class="have">
                                Email Support
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Contact customer support via email."></span>
                              </li>
                              <li data-index="6" class="have-not">Free Apps</li>
                              <li data-index="7" class="have">
                                Limited Search Engine Boost
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Your website will be optimized to be found on search engines (Google, Yahoo, Bing, etc.)"></span>
                              </li>
                              <li data-index="8" class="have">
                                500MB
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Storage provided for your website content: images, videos and audio files."></span>
                              </li>
                              <li data-index="9" class="have">
                                1GB
                                <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Bandwidth provided for your website. The more media you add to your website, the more bandwidth it will require for faster navigation of your site. "></span>
                              </li>
                              <li data-index="10" class="have-not">Custom Design</li>
                          </ul>
                            <?php
                            $price = Configure::read('Plans.Premium.cost.monthly');
                            echo get_checkout_link_plimus(array(
                                'user' => $user,
                                'billing_period' => 'Y',
                                'item_name' => "Basic",
                                'cost' => $price,
                                'button_html' => '<a href="#"
                                        class="button important track_event pay_link"
                                        data-ga-category="Pricing"
                                        data-ga-label="Trial Plan - Basic - Monthly - Reason:'.$reason.'"
                                        data-ga-event="Pay"
                                        data-ga-value="'.$price.'"
                                        data-ajax-track="1"
                                        style="display: inline-block;"
                                        >
                                            <button class="btn btn-ttc">Upgrade to <b>Basic</b> Plan</button>
                                        </a>
                                    '
                            ));?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 text-center plans-include">
                <h3>All Premium plans include</h3>
                <p>
                    <span><img src="images/v.png"> Free Web hosting</span>
                    <span><img src="images/v.png"> Analytics</span>
                    <span><img src="images/v.png"> Email Address</span>
                    <span><img src="images/v.png"> Sync with social media</span>
                </p>
                <img src="images/creditCards_logos.png" alt="Credit Cards logos">
                <p class="payments"><img src="images/lock.png" class="lock-icon"> PayPal, Visa, MasterCard secure payments</p>
                <div class="fb-facepile" data-href="https://www.facebook.com/otonomic" data-width="270" data-max-rows="1" data-colorscheme="light" data-size="large" data-show-count="true"></div>
            </div>
        </div>
      </section>

      <section class="counter">
        <div class="row section-counter">
            <div class="col-xs-12 text-center counter-inner">
                <img src="images/counter-logo.png">
                <p><span class="counter-digits">30,218</span> Sites created, and counting...</p>
            </div>
        </div>
      </section>

      <section class="testimonials">
        <div class="row section-testimonials">
            <div class="col-xs-12 testimonials-inner">
                <h1 class="title text-center">Small business owners love us!</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12  col-sm-4 testimonial">
                            <img src="images/testimonial1.png">
                            <h3>Steve Bross</h3>
                            <a href="http://thehawkmobile.com/" target="_blank">thehawkmobile.com</a>
                            <p>Since I do a lot of daily updates, I like the fact that the page updates on it's own. Otonomic have been very helpful in working with me to get the site just the way I wanted it. Glad I found them!</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 testimonial">
                            <img src="images/testimonial2.png">
                            <h3>Shawna Todd</h3>
                            <a href="http://boulderacupuncture.me/" target="_blank">boulderacupuncture.me</a>
                            <p>I had been wanting a website for my small business for too long. I never found the time or energy to get it done. I'm so grateful, I wish I had done it sooner.</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 testimonial">
                            <img src="images/testimonial3.png">
                            <h3>Brian O'Callaghan</h3>
                            <a href="http://www.dublinacupunctureclinic.com/" target="_blank">dublinacupunctureclinic.com</a>
                            <p>Otonomic offered me what I need at the time I needed it, a well designed, informative website that I update effortlessly wherever I travel.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>

      <section class="likes">
        <div class="row section-likes">
          <div class="hidden-xs col-xs-12 text-center">
            <p class="text-center"><img src="images/thumbUp.png"></p>
            <h3 class="text-center">If you like us help spread the word. Thanks.</h3>
            <div class="likes-div" id="likes-div">
              <div class="like-div" title="Facebook">
                <div class="fb-like" data-href="http://www.facebook.com/otonomic" data-width="86" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
              </div>
              <div class="like-div" title="Twitter">
                <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://otonomic.com/" data-via="Otonomic.com" data-lang="en" width="90">Tweet</a>
              </div>
              <div class="like-div" title="Google Plus">
                <div class="g-plusone" data-size="medium" data-href="http://otonomic.com/"></div>
              </div>
              <div class="like-div" title="Pinterest">
                <a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fotonomic.com%2F&media=http%3A%2F%2Fotonomic.com%2Fimages%2Fsection-home-bg.png&description=Even%20a%20small%20business%20deserves%20a%20big%20spotlight." data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
              </div>
              <div class="like-div" title="LinkedIn">
                <script type="IN/Share" data-url="http://otonomic.com/" data-counter="right"></script>
              </div>
            </div>
          </div>

          <div class="visible-xs col-xs-12 text-center">
            <div class="btn-group  dropup">
              <button class="btn-social dropdown-toggle" type="button" data-toggle="dropdown">
                <img src="images/thumbUp.png" width="26">  Like/Share us
              </button>
              <ul class="dropdown-menu">
                <li><script type="IN/Share" data-url="http://otonomic.com/" data-counter="right"></script></li>
                <li>
                  <a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fotonomic.com%2F&media=http%3A%2F%2Fotonomic.com%2Fimages%2Fsection-home-bg.png&description=Even%20a%20small%20business%20deserves%20a%20big%20spotlight." data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
                </li>
                <li>
                  <a href="https://twitter.com/share" class="twitter-share-button" data-url="https://twitter.com/otonomic/" data-via="Otonomic.com" data-lang="en">Tweet</a>
                </li>
                <li>
                  <div class="g-plusone" data-size="medium" data-href="http://otonomic.com/"></div>
                </li>
                <li>
                  <div class="fb-like" data-href="http://www.facebook.com/otonomic" data-width="86" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section class="faq">
            <div class="row section-faq">
              <div class="col-xs-12 faq-inner">
                <h1 class="title text-center">Questions people usually ask</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h3 class="question">Does Otonomic own the content I put on my site?</h3>
                            <p class="answer">Our service pulls the content from your Facebook page, which is yours and yours alone. You gave us permission to host it and display it on our service. This permission exists only for as long as you continue to use the service or remain an Account Holder.</p>
                            <h3 class="question">How can I cancel if I don’t like it?</h3>
                            <p class="answer">You may delete your site at any time from the settings menu. Cancellation will take effect immediately, you will then have no access to the website and the information will be deleted. We recommend you contact support with any issues - perhaps we can help! If you have comments about our service, please share them with us, so we can improve.</p>
                            <h3 class="question">Do we take Amex? Discovery? Diners?<br /> or just Visa and MC?</h3>
                            <p class="answer">We accept all major credit cards, as well as PayPal.</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <h3 class="question">Is Otonomic secured?</h3>
                            <p class="answer">Your site management is accessed with your personal Facebook account, and will only show when you are logged in to Facebook. We recommend you keep your Facebook credentials secure and personal at all times. If there is any security issue or unauthorized use of your Facebook account, you should notify support immediately.</p>
                            <h3 class="question">Can I use my existing domain name with an Otonomic site?</h3>
                            <p class="answer">Absolutely. If you upgraded to any of our packages, send support your domain details and we will place your site on your address.</p>
                            <h3 class="question">What if I pick the wrong plan?</h3>
                            <p class="answer">If you wish to upgrade from your current plan, you may do so at any time by selecting one of the plans above. For any other plan changes, please contact support, and you will get a reply within 1 business day.</p>
                        </div>
                    </div>
                </div>
                <div class="text-center figure">
                    <img src="images/figure1.png">
                    <p>Didn’t find your answer?</p>
                    <div class="btn-holder">
                        <a href="mailto:<?= WHITE_LABEL_EMAIL_SUPPORT?>">
                            <button class="btn btn-ttc">
                                <img src="images/email-icon.png"/>Contact Us
                            </button>
                        </a>
                    </div>
                </div>
              </div>
          </div>
        </section>
      <section class="start-trial">
            <div class="row section-start-trial">
              <div class="col-xs-12 start-trial-inner text-center">
                <h1 class="title">Let us help you grow. Join our Premium Plan Now!</h1>

                <?php
                $price = Configure::read('Plans.Professional.cost.yearly')['year'];
                echo get_checkout_link_plimus(array(
                    'user' => $user,
                    'billing_period' => 'Y',
                    'item_name' => "Professional",
                    'cost' => $price,
                    'button_html' => '<a href="#"
                                            class="button important track_event pay_link"
                                            data-ga-category="Pricing"
                                            data-ga-label="Trial Plan - Premium - Yearly - Reason:'.$reason.'"
                                            data-ga-event="Pay"
                                            data-ga-value="'.$price.'"
                                            data-ajax-track="1"
                                            style="display: inline-block;"
                                       >

                                            <button class="btn btn-ttc">
                                                <img src="images/btn-badge.png">
                                                Upgrade to <b>Professional</b> plan
                                            </button>
                                       </a>
                                  '
                ));?>

                <p class="sub-title">$<?= $price = Configure::read('Plans.Professional.cost.yearly')['month'];?> Per month, Billed annually. You can cancel any time.</p>
              </div>
          </div>
        </section>
        <section class="footer">
            <div class="row section-footer">
              <div class="col-xs-12  text-center">
                <ul>
                    <li><a href="<?URL_MARKETING_SITE?>/terms">Terms of Use</a></li>
                    <li>▪</li>
                    <li><a href="<?URL_MARKETING_SITE?>/privacy">Privay Notice</a></li>
                    <li>▪</li>
                    <li><a href="<?URL_MARKETING_SITE?>">2014 ©<img src="images/footer-logo.png"></a></li>
                </ul>
              </div>
          </div>
        </section>
    </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    
    <!-- Social  -->

    <!-- // Facebook -->
    <div id="fb-root"></div>
    <script type="text/javascript">
        window.fbAsyncInit = function() {
            FB.init({ appId: "373931652687761",status: true,cookie: true,xfbml: true});

            window.fbAsyncInit.fbLoaded.resolve();
            //checkConnectedWithFacebook();
        };

        window.fbAsyncInit.fbLoaded = jQuery.Deferred();

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js&appId=373931652687761";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- // Google Plus -->
    <script type="text/javascript">
      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>

    <!-- // Twitter -->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    </script>

    <!-- // Pintrest -->
    <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>

    <!-- // Linkedin -->
    <script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
    <!-- /Social  -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-37736198-1', 'auto');
        ga('send', 'pageview');
    </script>


    <script text="text/javascript" src="/wp-content/plugins/otonomic-analytics/js/main.js"></script>
    <script text="text/javascript" src="/wp-content/plugins/otonomic-analytics/js/otonomic_platform_analytics_events.js"></script>


    <script type="text/javascript">
    var reason = '<?= $reason?>';
    var active_plan = '<?= Configure::read('Plan.time'); ?>';

    function show_yearly_plans() {
        $(".pricing_plan_container").hide();
        $(".plan_yr").show();
        $(".btn-payment-plan").removeClass('active');
        $(".btn-payment-plan.btn-yearly").addClass('active');
    }

    function show_2years_plans() {
        $(".pricing_plan_container").hide();
        $(".plan_2yrs").show();
        $(".btn-payment-plan").removeClass('active');
        $(".btn-payment-plan.btn-2years").addClass('active');
    }

    function show_monthly_plans() {
        $(".pricing_plan_container").hide();
        $(".plan_mo").show();
        $(".btn-payment-plan").removeClass('active');
        $(".btn-payment-plan.btn-monthly").addClass('active');
    }

    jQuery(document).ready(function($) {
        // Track event of pricing view immediately
        $(document).otonomicTrackEvent({
            category: 'Pricing',
            action: 'View',
            label: reason,
            bind_tracking: false
        });


        $('#back-btn').on('click', function() {
            window.history.back();
        });

        // toggle table hover
        $('.pricing-table .table-content ul li').hover(function(){
            var index = $(this).attr('data-index');
            $('.pricing-table .table-content ul li[data-index='+index+']').addClass('active');
        },
        function(){
            var index = $(this).attr('data-index');
            $('.pricing-table .table-content ul li[data-index='+index+']').removeClass('active');
        });

        if(active_plan == 'Y'){
            show_yearly_plans();
        } else {
            show_monthly_plans();
        }

        show_yearly_plans();

        $(".btn-payment-plan").bind('click', function(e){
            var $this = $(this);
            /*
            $(".pricing-filter li").removeClass('active');
            $this.addClass('active');
            */
            if($this.hasClass('btn-monthly')){
                show_monthly_plans();
            } else {
                if($this.hasClass('btn-yearly')){
                    show_yearly_plans();
                } else {
                    show_2years_plans();
                }
            }
        });

        $('.glyphicon-question-sign').tooltip({
          delay : { 'show': 500, 'hide': 100},
          container : '.container-fluid',
          viewport:'table-content',
          placement: function(){
            if ( $(window).width() < 768 ){
              return 'left';
            }
          }
        });

        // show
        //$('.glyphicon-question-sign').tooltip('show');

        // Scroll Snaping
          function navBarToggle(dir){
            if( dir =='down'){
              $(".navbar .navbar-header .upper-bar").animate({top: "100"}, 300,"swing");
              $(".navbar .navbar-header .lower-bar").animate({top: "10"}, 300,"swing");
            }
            else if(dir =='up'){
              $(".navbar .navbar-header .upper-bar").animate({top: "0"}, 300,"swing");
              $(".navbar .navbar-header .lower-bar").animate({top: "-100"}, 300,"swing");
            }
          }
          // if scrolled to top change header
          $(window).on('scroll', function() {
            var scrollTop = $(this).scrollTop();
            var toggleFlag = $('.navbar').hasClass('active');
            //console.log(scrollTop);
            //console.log(toggleFlag);
            if ( (scrollTop <= 550) && (toggleFlag) ) {
                $('.navbar').removeClass('active');
                navBarToggle('up');
            }
            else if((scrollTop > 550) && (!toggleFlag)){
                $('.navbar').addClass('active');
                navBarToggle('down');
            }
          });
          
            $('.table-header').click(function(){
              if ( $(window).width() < 768 ){
                $(this).parent().find('.table-content').slideToggle();
                $(this).find('.glyphicon').toggleClass('glyphicon-chevron-down');
                $(this).find('.glyphicon').toggleClass('glyphicon-chevron-up');
              }
            });
    });
    </script>

    <script src="js/shared.js" defer></script>

    <script>
    jQuery(document).ready(function($) {
	    $('.pay_link').bind('click', function (e){
		    ga('set', 'metric9', '1');
		    jQuery(document).otonomicTrackPageView({
			    as: 'pageview',
			    category: "Pricing",
			    action: "Click upgrade",
			    label: ''
		    });
	    });
        /*$('.pay_link').otonomicTrackEvent({
            as: 'pageview',
            category: "Pricing Page",
            action: "Click upgrade",
            label: ''
        });*/
    });
    ga('set', 'metric8', '1');
    jQuery(document).otonomicTrackPageView({
	    category: "Pricing",
	    action: "View",
	    label: reason,
	    bind_tracking: false,
	    as: "pageview"
    });

    </script>
  </body>
</html>

<?php
} catch(Exception $e) {
    var_dump($e);
}