
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Pricing page</title>

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse">
      
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="otonomic.com"><img class="logo-img" src="images/otonomic-logo.png"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <!-- <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul> -->
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <label>Langauge</label>
              <select class="form-control">
                <option>English</option>
                <option>Spanish</option>
                <option>French</option>
                <option>Hebrew</option>
                <option>Russian</option>
              </select>
            </div>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <h2>Upgrade to: Professional Plan</h2>
      <div class="row">
        <div class="col-md-8">
          <div id="form-container">
            <form id="form"class="form-horizontal">
            <h4>Billing information</h4>
              <div class="form-group has-feedback">
                <label for="fullName" class="col-sm-3 control-label">Full name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="fullName" id="fullName">
                  <span class="glyphicon form-control-feedback" id="fullName1"></span>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                  <input type="email" class="form-control" name="email" id="email">
                  <span class="glyphicon form-control-feedback" id="email1"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="country" class="col-sm-3 control-label">Country</label>
                <div class="col-sm-6">
                  <select id="country" class="form-control">
                    <option data-value="AF" data-selectable="" class="option">Afghanistan</option>
                    <option data-value="AX" data-selectable="" class="option">Åland Islands</option>
                    <option data-value="AL" data-selectable="" class="option">Albania</option>
                    <option data-value="DZ" data-selectable="" class="option">Algeria</option>
                    <option data-value="AS" data-selectable="" class="option">American Samoa</option>
                    <option data-value="AD" data-selectable="" class="option">Andorra</option>
                    <option data-value="AO" data-selectable="" class="option">Angola</option>
                    <option data-value="AI" data-selectable="" class="option">Anguilla</option>
                    <option data-value="AQ" data-selectable="" class="option">Antarctica</option>
                    <option data-value="AG" data-selectable="" class="option">Antigua and Barbuda</option>
                    <option data-value="AR" data-selectable="" class="option">Argentina</option>
                    <option data-value="AM" data-selectable="" class="option">Armenia</option>
                    <option data-value="AW" data-selectable="" class="option">Aruba</option>
                    <option data-value="AU" data-selectable="" class="option">Australia</option>
                    <option data-value="AT" data-selectable="" class="option">Austria</option>
                    <option data-value="AZ" data-selectable="" class="option">Azerbaijan</option>
                    <option data-value="BS" data-selectable="" class="option">Bahamas</option>
                    <option data-value="BH" data-selectable="" class="option">Bahrain</option>
                    <option data-value="BD" data-selectable="" class="option">Bangladesh</option>
                    <option data-value="BB" data-selectable="" class="option">Barbados</option>
                    <option data-value="BY" data-selectable="" class="option">Belarus</option>
                    <option data-value="BE" data-selectable="" class="option">Belgium</option>
                    <option data-value="BZ" data-selectable="" class="option">Belize</option>
                    <option data-value="BJ" data-selectable="" class="option">Benin</option>
                    <option data-value="BM" data-selectable="" class="option">Bermuda</option>
                    <option data-value="BT" data-selectable="" class="option">Bhutan</option>
                    <option data-value="BO" data-selectable="" class="option">Bolivia, Plurinational State of</option>
                    <option data-value="BA" data-selectable="" class="option">Bosnia and Herzegovina</option>
                    <option data-value="BW" data-selectable="" class="option">Botswana</option>
                    <option data-value="BV" data-selectable="" class="option">Bouvet Island</option>
                    <option data-value="BR" data-selectable="" class="option">Brazil</option>
                    <option data-value="IO" data-selectable="" class="option">British Indian Ocean Territory</option>
                    <option data-value="BN" data-selectable="" class="option">Brunei Darussalam</option>
                    <option data-value="BG" data-selectable="" class="option">Bulgaria</option>
                    <option data-value="BF" data-selectable="" class="option">Burkina Faso</option>
                    <option data-value="BI" data-selectable="" class="option">Burundi</option>
                    <option data-value="KH" data-selectable="" class="option">Cambodia</option>
                    <option data-value="CM" data-selectable="" class="option">Cameroon</option>
                    <option data-value="CA" data-selectable="" class="option">Canada</option>
                    <option data-value="CV" data-selectable="" class="option">Cape Verde</option>
                    <option data-value="KY" data-selectable="" class="option">Cayman Islands</option>
                    <option data-value="CF" data-selectable="" class="option">Central African Republic</option>
                    <option data-value="TD" data-selectable="" class="option">Chad</option>
                    <option data-value="CL" data-selectable="" class="option">Chile</option>
                    <option data-value="CN" data-selectable="" class="option">China</option>
                    <option data-value="CX" data-selectable="" class="option">Christmas Island</option>
                    <option data-value="CC" data-selectable="" class="option">Cocos (Keeling) Islands</option>
                    <option data-value="CO" data-selectable="" class="option">Colombia</option>
                    <option data-value="KM" data-selectable="" class="option">Comoros</option>
                    <option data-value="CG" data-selectable="" class="option">Congo</option>
                    <option data-value="CD" data-selectable="" class="option">Congo, the Democratic Republic of the</option>
                    <option data-value="CK" data-selectable="" class="option">Cook Islands</option>
                    <option data-value="CR" data-selectable="" class="option">Costa Rica</option>
                    <option data-value="CI" data-selectable="" class="option">Côte d'Ivoire</option>
                    <option data-value="HR" data-selectable="" class="option">Croatia</option>
                    <option data-value="CU" data-selectable="" class="option">Cuba</option>
                    <option data-value="CY" data-selectable="" class="option">Cyprus</option>
                    <option data-value="CZ" data-selectable="" class="option">Czech Republic</option>
                    <option data-value="DK" data-selectable="" class="option">Denmark</option>
                    <option data-value="DJ" data-selectable="" class="option">Djibouti</option>
                    <option data-value="DM" data-selectable="" class="option">Dominica</option>
                    <option data-value="DO" data-selectable="" class="option">Dominican Republic</option>
                    <option data-value="EC" data-selectable="" class="option">Ecuador</option>
                    <option data-value="EG" data-selectable="" class="option">Egypt</option>
                    <option data-value="SV" data-selectable="" class="option">El Salvador</option>
                    <option data-value="GQ" data-selectable="" class="option">Equatorial Guinea</option>
                    <option data-value="ER" data-selectable="" class="option">Eritrea</option>
                    <option data-value="EE" data-selectable="" class="option">Estonia</option>
                    <option data-value="ET" data-selectable="" class="option">Ethiopia</option>
                    <option data-value="FK" data-selectable="" class="option">Falkland Islands (Malvinas)</option>
                    <option data-value="FO" data-selectable="" class="option">Faroe Islands</option>
                    <option data-value="FJ" data-selectable="" class="option">Fiji</option>
                    <option data-value="FI" data-selectable="" class="option">Finland</option>
                    <option data-value="FR" data-selectable="" class="option">France</option>
                    <option data-value="GF" data-selectable="" class="option">French Guiana</option>
                    <option data-value="PF" data-selectable="" class="option">French Polynesia</option>
                    <option data-value="TF" data-selectable="" class="option">French Southern Territories</option>
                    <option data-value="GA" data-selectable="" class="option">Gabon</option>
                    <option data-value="GM" data-selectable="" class="option">Gambia</option>
                    <option data-value="GE" data-selectable="" class="option">Georgia</option>
                    <option data-value="DE" data-selectable="" class="option">Germany</option>
                    <option data-value="GH" data-selectable="" class="option">Ghana</option>
                    <option data-value="GI" data-selectable="" class="option">Gibraltar</option>
                    <option data-value="GR" data-selectable="" class="option">Greece</option>
                    <option data-value="GL" data-selectable="" class="option">Greenland</option>
                    <option data-value="GD" data-selectable="" class="option">Grenada</option>
                    <option data-value="GP" data-selectable="" class="option">Guadeloupe</option>
                    <option data-value="GU" data-selectable="" class="option">Guam</option>
                    <option data-value="GT" data-selectable="" class="option">Guatemala</option>
                    <option data-value="GG" data-selectable="" class="option">Guernsey</option>
                    <option data-value="GN" data-selectable="" class="option">Guinea</option>
                    <option data-value="GW" data-selectable="" class="option">Guinea-Bissau</option>
                    <option data-value="GY" data-selectable="" class="option">Guyana</option>
                    <option data-value="HT" data-selectable="" class="option">Haiti</option>
                    <option data-value="HM" data-selectable="" class="option">Heard Island and McDonald Islands</option>
                    <option data-value="VA" data-selectable="" class="option">Holy See (Vatican City State)</option>
                    <option data-value="HN" data-selectable="" class="option">Honduras</option>
                    <option data-value="HK" data-selectable="" class="option">Hong Kong</option>
                    <option data-value="HU" data-selectable="" class="option">Hungary</option>
                    <option data-value="IS" data-selectable="" class="option">Iceland</option>
                    <option data-value="IN" data-selectable="" class="option">India</option>
                    <option data-value="ID" data-selectable="" class="option">Indonesia</option>
                    <option data-value="IR" data-selectable="" class="option">Iran, Islamic Republic of</option>
                    <option data-value="IQ" data-selectable="" class="option">Iraq</option>
                    <option data-value="IE" data-selectable="" class="option">Ireland</option>
                    <option data-value="IM" data-selectable="" class="option">Isle of Man</option>
                    <option data-value="IL" data-selectable="" class="option">Israel</option>
                    <option data-value="IT" data-selectable="" class="option">Italy</option>
                    <option data-value="JM" data-selectable="" class="option">Jamaica</option>
                    <option data-value="JP" data-selectable="" class="option">Japan</option>
                    <option data-value="JE" data-selectable="" class="option">Jersey</option>
                    <option data-value="JO" data-selectable="" class="option">Jordan</option>
                    <option data-value="KZ" data-selectable="" class="option">Kazakhstan</option>
                    <option data-value="KE" data-selectable="" class="option">Kenya</option>
                    <option data-value="KI" data-selectable="" class="option">Kiribati</option>
                    <option data-value="KP" data-selectable="" class="option">Korea, Democratic People's Republic of</option>
                    <option data-value="KR" data-selectable="" class="option">Korea, Republic of</option>
                    <option data-value="KW" data-selectable="" class="option">Kuwait</option>
                    <option data-value="KG" data-selectable="" class="option">Kyrgyzstan</option>
                    <option data-value="LA" data-selectable="" class="option">Lao People's Democratic Republic</option>
                    <option data-value="LV" data-selectable="" class="option">Latvia</option>
                    <option data-value="LB" data-selectable="" class="option">Lebanon</option>
                    <option data-value="LS" data-selectable="" class="option">Lesotho</option>
                    <option data-value="LR" data-selectable="" class="option">Liberia</option>
                    <option data-value="LY" data-selectable="" class="option">Libyan Arab Jamahiriya</option>
                    <option data-value="LI" data-selectable="" class="option">Liechtenstein</option>
                    <option data-value="LT" data-selectable="" class="option">Lithuania</option>
                    <option data-value="LU" data-selectable="" class="option">Luxembourg</option>
                    <option data-value="MO" data-selectable="" class="option">Macao</option>
                    <option data-value="MK" data-selectable="" class="option">Macedonia, the former Yugoslav Republic of</option>
                    <option data-value="MG" data-selectable="" class="option">Madagascar</option>
                    <option data-value="MW" data-selectable="" class="option">Malawi</option>
                    <option data-value="MY" data-selectable="" class="option">Malaysia</option>
                    <option data-value="MV" data-selectable="" class="option">Maldives</option>
                    <option data-value="ML" data-selectable="" class="option">Mali</option>
                    <option data-value="MT" data-selectable="" class="option">Malta</option>
                    <option data-value="MH" data-selectable="" class="option">Marshall Islands</option>
                    <option data-value="MQ" data-selectable="" class="option">Martinique</option>
                    <option data-value="MR" data-selectable="" class="option">Mauritania</option>
                    <option data-value="MU" data-selectable="" class="option">Mauritius</option>
                    <option data-value="YT" data-selectable="" class="option">Mayotte</option>
                    <option data-value="MX" data-selectable="" class="option">Mexico</option>
                    <option data-value="FM" data-selectable="" class="option">Micronesia, Federated States of</option>
                    <option data-value="MD" data-selectable="" class="option">Moldova, Republic of</option>
                    <option data-value="MC" data-selectable="" class="option">Monaco</option>
                    <option data-value="MN" data-selectable="" class="option">Mongolia</option>
                    <option data-value="ME" data-selectable="" class="option">Montenegro</option>
                    <option data-value="MS" data-selectable="" class="option">Montserrat</option>
                    <option data-value="MA" data-selectable="" class="option">Morocco</option>
                    <option data-value="MZ" data-selectable="" class="option">Mozambique</option>
                    <option data-value="MM" data-selectable="" class="option">Myanmar</option>
                    <option data-value="NA" data-selectable="" class="option">Namibia</option>
                    <option data-value="NR" data-selectable="" class="option">Nauru</option>
                    <option data-value="NP" data-selectable="" class="option">Nepal</option>
                    <option data-value="NL" data-selectable="" class="option">Netherlands</option>
                    <option data-value="AN" data-selectable="" class="option">Netherlands Antilles</option>
                    <option data-value="NC" data-selectable="" class="option">New Caledonia</option>
                    <option data-value="NZ" data-selectable="" class="option">New Zealand</option>
                    <option data-value="NI" data-selectable="" class="option">Nicaragua</option>
                    <option data-value="NE" data-selectable="" class="option">Niger</option>
                    <option data-value="NG" data-selectable="" class="option">Nigeria</option>
                    <option data-value="NU" data-selectable="" class="option">Niue</option>
                    <option data-value="NF" data-selectable="" class="option">Norfolk Island</option>
                    <option data-value="MP" data-selectable="" class="option">Northern Mariana Islands</option>
                    <option data-value="NO" data-selectable="" class="option">Norway</option>
                    <option data-value="OM" data-selectable="" class="option">Oman</option>
                    <option data-value="PK" data-selectable="" class="option">Pakistan</option>
                    <option data-value="PW" data-selectable="" class="option">Palau</option>
                    <option data-value="PS" data-selectable="" class="option">Palestinian Territory, Occupied</option>
                    <option data-value="PA" data-selectable="" class="option">Panama</option>
                    <option data-value="PG" data-selectable="" class="option">Papua New Guinea</option>
                    <option data-value="PY" data-selectable="" class="option">Paraguay</option>
                    <option data-value="PE" data-selectable="" class="option">Peru</option>
                    <option data-value="PH" data-selectable="" class="option">Philippines</option>
                    <option data-value="PN" data-selectable="" class="option">Pitcairn</option>
                    <option data-value="PL" data-selectable="" class="option">Poland</option>
                    <option data-value="PT" data-selectable="" class="option">Portugal</option>
                    <option data-value="PR" data-selectable="" class="option">Puerto Rico</option>
                    <option data-value="QA" data-selectable="" class="option">Qatar</option>
                    <option data-value="RE" data-selectable="" class="option">Réunion</option>
                    <option data-value="RO" data-selectable="" class="option">Romania</option>
                    <option data-value="RU" data-selectable="" class="option">Russian Federation</option>
                    <option data-value="RW" data-selectable="" class="option">Rwanda</option>
                    <option data-value="BL" data-selectable="" class="option">Saint Barthélemy</option>
                    <option data-value="SH" data-selectable="" class="option">Saint Helena, Ascension and Tristan da Cunha</option>
                    <option data-value="KN" data-selectable="" class="option">Saint Kitts and Nevis</option>
                    <option data-value="LC" data-selectable="" class="option">Saint Lucia</option>
                    <option data-value="MF" data-selectable="" class="option">Saint Martin (French part)</option>
                    <option data-value="PM" data-selectable="" class="option">Saint Pierre and Miquelon</option>
                    <option data-value="VC" data-selectable="" class="option">Saint Vincent and the Grenadines</option>
                    <option data-value="WS" data-selectable="" class="option">Samoa</option>
                    <option data-value="SM" data-selectable="" class="option">San Marino</option>
                    <option data-value="ST" data-selectable="" class="option">Sao Tome and Principe</option>
                    <option data-value="SA" data-selectable="" class="option">Saudi Arabia</option>
                    <option data-value="SN" data-selectable="" class="option">Senegal</option>
                    <option data-value="RS" data-selectable="" class="option">Serbia</option>
                    <option data-value="SC" data-selectable="" class="option">Seychelles</option>
                    <option data-value="SL" data-selectable="" class="option">Sierra Leone</option>
                    <option data-value="SG" data-selectable="" class="option">Singapore</option>
                    <option data-value="SK" data-selectable="" class="option">Slovakia</option>
                    <option data-value="SI" data-selectable="" class="option">Slovenia</option>
                    <option data-value="SB" data-selectable="" class="option">Solomon Islands</option>
                    <option data-value="SO" data-selectable="" class="option">Somalia</option>
                    <option data-value="ZA" data-selectable="" class="option">South Africa</option>
                    <option data-value="GS" data-selectable="" class="option">South Georgia and the South Sandwich Islands</option>
                    <option data-value="ES" data-selectable="" class="option">Spain</option>
                    <option data-value="LK" data-selectable="" class="option">Sri Lanka</option>
                    <option data-value="SD" data-selectable="" class="option">Sudan</option>
                    <option data-value="SR" data-selectable="" class="option">Suriname</option>
                    <option data-value="SJ" data-selectable="" class="option">Svalbard and Jan Mayen</option>
                    <option data-value="SZ" data-selectable="" class="option">Swaziland</option>
                    <option data-value="SE" data-selectable="" class="option">Sweden</option>
                    <option data-value="CH" data-selectable="" class="option">Switzerland</option>
                    <option data-value="SY" data-selectable="" class="option">Syrian Arab Republic</option>
                    <option data-value="TW" data-selectable="" class="option">Taiwan, Province of China</option>
                    <option data-value="TJ" data-selectable="" class="option">Tajikistan</option>
                    <option data-value="TZ" data-selectable="" class="option">Tanzania, United Republic of</option>
                    <option data-value="TH" data-selectable="" class="option">Thailand</option>
                    <option data-value="TL" data-selectable="" class="option">Timor-Leste</option>
                    <option data-value="TG" data-selectable="" class="option">Togo</option>
                    <option data-value="TK" data-selectable="" class="option">Tokelau</option>
                    <option data-value="TO" data-selectable="" class="option">Tonga</option>
                    <option data-value="TT" data-selectable="" class="option">Trinidad and Tobago</option>
                    <option data-value="TN" data-selectable="" class="option">Tunisia</option>
                    <option data-value="TR" data-selectable="" class="option">Turkey</option>
                    <option data-value="TM" data-selectable="" class="option">Turkmenistan</option>
                    <option data-value="TC" data-selectable="" class="option">Turks and Caicos Islands</option>
                    <option data-value="TV" data-selectable="" class="option">Tuvalu</option>
                    <option data-value="UG" data-selectable="" class="option">Uganda</option>
                    <option data-value="UA" data-selectable="" class="option">Ukraine</option>
                    <option data-value="AE" data-selectable="" class="option">United Arab Emirates</option>
                    <option data-value="GB" data-selectable="" class="option">United Kingdom</option>
                    <option data-value="US" data-selectable="" class="option">United States</option>
                    <option data-value="UM" data-selectable="" class="option">United States Minor Outlying Islands</option>
                    <option data-value="UY" data-selectable="" class="option">Uruguay</option>
                    <option data-value="UZ" data-selectable="" class="option">Uzbekistan</option>
                    <option data-value="VU" data-selectable="" class="option">Vanuatu</option>
                    <option data-value="VE" data-selectable="" class="option">Venezuela, Bolivarian Republic of</option>
                    <option data-value="VN" data-selectable="" class="option">Viet Nam</option>
                    <option data-value="VG" data-selectable="" class="option">Virgin Islands, British</option>
                    <option data-value="VI" data-selectable="" class="option">Virgin Islands, U.S.</option>
                    <option data-value="WF" data-selectable="" class="option">Wallis and Futuna</option>
                    <option data-value="EH" data-selectable="" class="option">Western Sahara</option>
                    <option data-value="YE" data-selectable="" class="option">Yemen</option>
                    <option data-value="ZM" data-selectable="" class="option">Zambia</option>
                    <option data-value="ZW" data-selectable="" class="option">Zimbabwe</option>
                  </select>
                </div>
              </div>
              
              <div class="form-group has-feedback">
                <label for="phone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="phone" id="phone">
                  <span class="glyphicon form-control-feedback" id="phone1"></span>
                </div>
              </div>

              <h4>Payment information</h4>
              <div class="form-group">
                <label for="payment-method" class="col-sm-3 control-label">Payment method</label>
                <div id="payment-method" class="col-sm-9">
                  <label class="radio-inline">
                    <input type="radio" name="credit-card" id="credit-card" value="Credit card"><img class="cc-logos" src="images/credit_cards.svg" alt="Credit Card" checked>
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="paypal" id="paypal" value="Paypal"><img class="paypal-logo" src="images/PayPal_logo.svg" alt="PayPal">
                  </label>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="cc-number" class="col-sm-3 control-label">Card number</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="ccNumber" id="ccNumber">
                  <span class="glyphicon form-control-feedback" id="ccNumber1"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="cc-exp-date" class="col-sm-3 control-label">Expiration date</label>
                <div class="col-md-3 col-lg-2">
                  <!-- <label class="control-label">Month</label> -->
                  <select class="form-control" name="cc-exp-date-month" id="cc-exp-date-month"> 
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                  </select>
                </div>
                <div class="col-md-3 col-lg-2">
                  <!-- <label class="control-label">Year</label> -->
                  <select class="form-control" name="cc-exp-date-year" id="cc-exp-date-year"> 
                    <option value="2014">2014</option>
                    <option selected="selected" value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label for="security-code" class="col-sm-3 control-label">Security code</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="secutityCode" id="secutityCode">
                  <span class="glyphicon form-control-feedback" id="secutityCode1"></span>
                </div>
                <div>
                  <img class="cc-cvv" src="images/cvv.png" alt="CVV">
                  <button id="cc-sc"class="btn btn-link" data-toggle="popover" title="Credit security Code">What is this?</button>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                  <button type="submit" class="btn btn-lg btn-success">Pay now</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="panel price-panel-container">
            <div class="row">
              <div class="col-xs-8">
                <h4>Choose your price</h4>
              </div>
              <div class="col-xs-4">
                <div class="form-group currency-select-container">
                  <select class="currency-select form-control">
                    <option>USD</option>
                    <option>Shekels</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="radio active">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan1" value="option1" checked>
                $12 / Month<br/>
                <small>billed monthly</small>
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan2" value="option2">
                $8 / Month <span class="save-text">save 33%</span><br/>
                <small>billed annaully</small>
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="paymentPlan" id="paymentPlan3" value="option3">
                $6 / Month <span class="save-text">save 50%</span><br/>
                <small>billed every 2 years</small>
              </label>
            </div>
            <hr>
            <h4>Order summary</h4>
            <table class="table">
              <tr>
                <td>Professional Plan</td>
                <td class="text-right">$144</td>
              </tr>
              <tr>
                <td>Your Savings</td>
                <td class="text-right green-text">$-48</td>
              </tr>
              <tr>
                <td><h4>Total payment</h4></td>
                <td class="text-right"><h4>$96</h4></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row footer">
        <div class="col-xs-12 text-center">
          <img src="images/footer-icons.png" alt="Our security">
          <p>Secure e-Commerce services are provide by <a href="http://home.bluesnap.com/" target="_blank">BlueSnap</a>, on online reseller for <a href="http://www.otonomic.com/" target="_blank">Otonomic</a></p>
        </div>
      </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/jquery-validate.bootstrap-tooltip.min.js"></script>

    <script type="text/javascript">
      $(function () {
        $('#cc-sc[data-toggle="popover"]').popover({
          trigger: 'hover',
          placement: 'top',
          html: true,
          content: '\
          <p>A three or four digit code on your credit card, separate from the 16 digit card number. The location varies slightly depending on your type of card:</p>\
          <div class="row">\
            <div class="col-sm-6"><img src="images/visa-cvv.png" alt="CCV"><p>On the reverse side of your card, you should see either the entire 16-digit credit card number or just the last four digits followed by a special 3-digit code. This 3-digit code is your Card Security Code.</p></div>\
            <div class="col-sm-6"><img src="images/american-x-cvv.png" alt="CCV"><p>Look for the 4-digit code printed on the front of your card just above and to the right of your main credit card number. This 4-digit code is your Card Security Code.</p></div>\
          </div>\
          '
        });

        $("input[name='paymentPlan']").change(function() {
            console.log("changed");
            $("input[name='paymentPlan']").parents('.radio').removeClass('active');
            $(this).parents('.radio').addClass('active');
        });

        // Validation
        $("#form").validate({
          rules: {
            fullName: {
              required: true 
            },
            email:  {
              email:true, 
              required: true
            },
            phone:  { 
              required: true 
            },
            ccNumber: {
              required: true 
            },
            secutityCode: {
              required: true
            }

          },
            messages: {
            fullName: "type your First Name and Last Name"
          },
          tooltip_options: {
            fullName: {
                        //trigger:'focus',
                        //placement:'right',
                        //html: true
                      },
          },
          highlight: function(element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
              var id_attr = "#" + $( element ).attr("id") + "1";
              $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
              var id_attr = "#" + $( element ).attr("id") + "1";
              $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
          },

          // submitHandler
          submitHandler: function(form) {
            $(form).ajaxSubmit();
          }
        });
      });
    </script>
  </body>
</html>
