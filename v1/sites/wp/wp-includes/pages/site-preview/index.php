<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Otonomic - Mobile Preview</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <meta property="og:title" content="Even a small business deserves a big spotlight." />
    <meta property="og:site_name" content="otonomic"/>
    <meta property="og:description" content="In under 20 seconds , we will turn your Facebook page into a professional website"/>
    <meta property="og:url" content="http://otonomic.com"/>
    <meta property="og:image" content="http://otonomic.com/images/section-home-bg.png"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom style -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Google+ -->
    <link href="https://plus.google.com/112126439055007134666" rel="publisher">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="mobile_preview">
      <div class="container-fluid">
       <!-- top navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                  <button type="button" class="navbar-toggle track_event track_hover hidden" id="menu-toggle" data-toggle="offcanvas" data-target=".sidebar-nav" data-ga-category="Marketing Website"  data-ga-event="Menu Click" data-ga-event-hover="Menu Hover" data-ajax-track="1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <div class="upper-bar">
                    <div class="logo-holder">
                          <img src="images/otonomic-logo.png" class="logo-img" alt="otonomic logo" width="140px;">
                    </div>
                    <div class="view-toggler center-block text-center">
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-device-type  btn-smart-phone">
                          <input type="radio" name="device-type" id="smart-phone" > <div class="ico-holder"><img src="images/smart-phone-ico.png" alt="smart-phone"></div>
                        </label>
                        <label class="btn btn-device-type active btn-tablet">
                          <input type="radio" name="device-type" id="tablet"> <div class="ico-holder"><img src="images/tablet-ico.png" alt="tablet"></div>
                        </label>
                        <label class="btn btn-device-type btn-desktop">
                          <input type="radio" name="device-type" id="desktop"> <div class="ico-holder"><img src="images/desktop-ico.png" alt="tablet"></div>
                        </label>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="device-holder">
          <div class="iframe-holder tablet"><iframe id="site_iframe" src="/sites/home/?t=elegantblue"></iframe></div>
          <div class="bg-holder"><img src="images/tablet.png"></div>
        </div>
      </div>
    </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function($) {
        $('.btn-smart-phone').click(function(){
            $('.device-holder').animate({opacity: "0"}, 250,"swing",function(){
              $('.iframe-holder').removeClass('tablet');
              $('.iframe-holder').removeClass('desktop');
              $('.iframe-holder').addClass('smart-phone');
              $('.device-holder .bg-holder').removeClass('tablet');
              $('.bg-holder img').attr('src','images/smart-phone.png');
              $('.device-holder').animate({opacity: "1"}, 250,"swing");
            });
        });
        $('.btn-tablet').click(function(){
          $('.device-holder').animate({opacity: "0"}, 250,"swing",function(){
            $('.iframe-holder').removeClass('smart-phone');
            $('.iframe-holder').removeClass('desktop');
            $('.iframe-holder').addClass('tablet');
            $('.device-holder .bg-holder').addClass('tablet');
            $('.bg-holder img').attr('src','images/tablet.png');
            $('.device-holder').animate({opacity: "1"}, 250,"swing");
          });
        });
        $('.btn-desktop').click(function(){
          $('.device-holder').animate({opacity: "0"}, 250,"swing",function(){
            $('.iframe-holder').removeClass('smart-phone');
            $('.iframe-holder').removeClass('tablet');
            $('.iframe-holder').addClass('desktop');
            $('.device-holder .bg-holder').addClass('desktop');
            $('.bg-holder img').attr('src','');
            $('.device-holder').animate({opacity: "1"}, 250,"swing");
          });
        });
        
    });
  </script>
  </body>
</html>