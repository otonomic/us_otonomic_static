<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', dirname(__FILE__).'/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager

define('WP_ALLOW_REPAIR', false);
define('AUTOSAVE_INTERVAL', 500);
define('WP_POST_REVISIONS', false);

define('WP_AUTO_UPDATE_CORE', false);
define( 'DISALLOW_FILE_MODS', false);
define( 'DISABLE_WP_CRON', true );
define( 'AUTOMATIC_UPDATER_DISABLED', true );

define('APP_ROOT', dirname(__DIR__));

define('APP_ENV', getenv('APPLICATION_ENV'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FB_USER_ACCESS_TOKEN', 'CAAUlWzuUGiYBAO7BZA8ek15RMSy8LNdD38wWlO9ssi6fPjdUFl3dSdxiB6eddwtZBWEEied2cPV8UmYH9zTqr1x2hhJCtRx60w0ZA4tdHziZAZCBzEUEFSFsMLXDnZCTZAQW3RETBAbtto9sIhiDjvwnIxeVSrbYY9ICTfOvLlxniqjnNWDK3tuxwTRBcUzQWvIxA3ZCeVBQgkYcYUE7V08p');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'otosalt');
define('SECURE_AUTH_KEY',  'otosalt');
define('LOGGED_IN_KEY',    'otosalt');
define('NONCE_KEY',        'otosalt');
define('AUTH_SALT',        'otosalt');
define('SECURE_AUTH_SALT', 'otosalt');
define('LOGGED_IN_SALT',   'otosalt');
define('NONCE_SALT',       'otosalt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'en_US');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);

define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('ALLOW_ALL_EXTERNAL_SITES', true);

define('INTERCOM_APP_ID', 'y23y9s35');
define('INTERCOM_API_KEY', 'ef3dbf3f1a0e2001e79a048ce99af8ad710b2be8');

if(!defined(FB_TOKEN)) {
    define('FB_TOKEN', "389314351133865|O4FgcprDMY0k6rxRUO-KOkWuVoU");
}


define('OTONOMIC_GET_OPTIONS_FROM_SERIALIZED_KEY', false);

define('OTONOMIC_VERSION', '1.00.004');


// IAM user for CloudFront and S3 only
define( 'AWS_ACCESS_KEY_ID', 'AKIAIPI52IDKIFN5ANBA' );
define( 'AWS_SECRET_ACCESS_KEY', 'XKkQaCK3LJI6OGG3vK+fCbf9P3DJxbQu8ANM/Vmo' );

// Added by W3 Total Cache
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

$whitelist = array(
    '127.0.0.1',
    '::1',
    '192.168.33.1'
);

if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
    $localhost = true;
    define( 'LOCALHOST', 1);
} else {
    $localhost = false;
    define( 'LOCALHOST', 0);
}

$filename = dirname(__FILE__) . '/wp-config.'.$_SERVER['HTTP_HOST'].'.php';
if (file_exists($filename)) {
    require ($filename);
}
else
{
	$http_host = $_SERVER['HTTP_HOST'];
	$http_host = explode('.', $http_host);
	if(count($http_host)>2)
	{
		/* we just need last 2 values */
		//$http_host = $http_host[count($http_host)-2].'.'.$http_host[count($http_host)-1];
		$http_host = array_slice($http_host, count($http_host)-2);
	}
	$http_host = implode('.', $http_host);
	$filename = dirname(__FILE__) . '/wp-config.'.$http_host.'.php';
	if (file_exists($filename)) {
		require ($filename);
	}
	else
	{
		$filename = dirname(__FILE__) . '/wp-config.otonomic.com.php';
		require($filename);
	}
}


/*else {
    $filename = dirname(__FILE__) . '/wp-config.local.php';
    if (file_exists($filename)) { require ($filename); }
}*/

// Basic Wordpress Minify Plugin - allow in admin
define('BWP_MINIFY_ALLOWED_IN_ADMIN', false);

// TODO: Copy file to this location
include_once(ABSPATH . 'white_label/otonomic.original.php');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
if(!defined( 'SKIP_WP_LOAD')) {
    require_once(ABSPATH . 'wp-settings.php');
}

define( 'DISALLOW_FILE_EDIT', true );

if(!defined('PROMOTED_PRICING_PLAN_LINK'))
	define( 'PROMOTED_PRICING_PLAN_LINK', 'https://www.plimus.com/jsp/buynow.jsp?contractId=2327123');


define( 'SUNRISE', 'on' );

if (!function_exists('write_log')) {
    function write_log( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}
