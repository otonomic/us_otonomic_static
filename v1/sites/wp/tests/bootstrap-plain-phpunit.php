<?php

require_once __DIR__ . '/wordpress-develop/tests/phpunit/includes/bootstrap.php';

// return;





/*

define('OT_APP_ROOT', realpath(__DIR__ . '/../'));

// From main wp-config.php
define('APP_ROOT', OT_APP_ROOT);
define('APP_ENV', getenv('APPLICATION_ENV'));
define('ANALYTICS_ID','UA-37736198-1');
define('OTONOMIC_GET_OPTIONS_FROM_SERIALIZED_KEY', false);
$localhost = true; // ...right?

if ($localhost) {
    define('MAIN_DOMAIN', 'wp.test');
    define('SUBDOMAIN_INSTALL', false);
    define('DOMAIN_CURRENT_SITE', MAIN_DOMAIN);
    define('DOMAIN_FOR_SITE_CREATION', MAIN_DOMAIN);
    define('DOMAIN_FOR_THE_SYSTEM', MAIN_DOMAIN);

} else {
    define('SUBDOMAIN_INSTALL', true);

    define('MAIN_DOMAIN', 'otonomic.com');
    define('DOMAIN_CURRENT_SITE', MAIN_DOMAIN);
    define('DOMAIN_FOR_SITE_CREATION', 'wp.'.MAIN_DOMAIN);
    define('DOMAIN_FOR_THE_SYSTEM', 'wp.'.MAIN_DOMAIN);
}
if (!isset($_SERVER['REMOTE_ADDR'])) $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

// NgGallery
define('NGGALLERY_ABSPATH', OT_APP_ROOT . '/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ngglegacy/');

*/
// Utility functions

function array2obj($arr)
{
    return (object) $arr;
}

function obj2array($obj)
{
    if(is_object($obj)) $obj = (array) $obj;
    if(is_array($obj)) {
        $new = array();
        foreach($obj as $key => $val) {
            $new[$key] = obj2array($val);
        }
    }
    else $new = $obj;
    return $new;
}

function true_var_export($v, $array2obj_func_name = 'array2obj')
{
    return str_replace('stdClass::__set_state', $array2obj_func_name, var_export($v, true));
}

function true_var_export_to_file($v, $file_name)
{
    $fh = fopen($file_name, 'w+');
    fwrite($fh, true_var_export($v));
    fclose($fh);
}

function tprint($v)
{
    echo "\n\n------------------------\n";
    echo true_var_export($v);
    echo "\n------------------------\n\n";
}
