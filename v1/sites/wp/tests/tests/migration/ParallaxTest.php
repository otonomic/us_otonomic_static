<?php

class ParallaxTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        // include_once '../bootstrap-plain-phpunit.php';

        $base_dir = OT_APP_ROOT . '/migration/';

        include_once $base_dir . 'config/fb.php';

        //include_once $base_dir . 'utils.php';

        include_once $base_dir . 'helpers/Curl.php';

        include_once $base_dir . 'helpers/Page2site.php';
        include_once $base_dir . 'helpers/Section.php';
        include_once $base_dir . 'helpers/DB.php';

        include_once $base_dir . 'WPMigration.php';

        include_once $base_dir . "plugins/core/Factory.php";

        include_once $base_dir . "core/ThemeAbstract.php";
        include_once $base_dir . "core/ThemifyAbstract.php";
        include_once $base_dir . "core/interfaces/ThemeInterface.php";
        include_once $base_dir . "core/interfaces/ThemePluginSupportInterface.php";

        include_once $base_dir . "themes/parallax/Parallax.php";


        include_once OT_APP_ROOT . '/tests/testing-data/fb-data.php';

    }

    public function test_get_gallery_shortcode_main_slider(){
        $main_slider_gallery_ids = [1,2,3];

        $Parallax = new Parallax(1,1);
        $result = $Parallax->get_gallery_shortcode_main_slider($main_slider_gallery_ids);

        $expected = "[gallery ids=\"".implode(',', $main_slider_gallery_ids)."\"]";

        $this->assertEquals($result, $expected);
    }

    function test_get_images_for_slider() {
        global $fb_page_data, $fb_parsed_page_data;
        $fb_page_data_arr = obj2array($fb_page_data);
        $albums = $fb_page_data_arr['albums'];

        $Parallax = new Parallax(1,1);
        $images_for_slider = $Parallax->get_images_for_slider( $albums);

        print_r($images_for_slider);
    }
}
