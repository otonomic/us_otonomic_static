<?php
//class WPMigrationTest extends PHPUnit_Framework_TestCase
class WPMigrationTest extends WP_UnitTestCase
{
    public $fb_id;
    public $theme;
    public $wpm;

    public function setUp()
    {
        require_once APP_ROOT . '/migration/config/fb.php';
        require_once APP_ROOT . '/migration/utils.php';
        require_once APP_ROOT . '/migration/helpers/Curl.php';
        require_once APP_ROOT . '/migration/helpers/Page2site.php';
        require_once APP_ROOT . '/migration/helpers/Section.php';
        require_once APP_ROOT . '/migration/helpers/DB.php';
        require_once APP_ROOT . '/migration/WPMigration.php';

        require_once APP_ROOT . '/tests/testing-data/fb-data.php';

    }


    public function test_download_and_parse_page_data()
    {
        $this->markTestIncomplete(); return;

        $this->fb_id = '310783022385273';
        $this->theme = 'parallax';

        $this->wpm = new WPMigration($this->fb_id, $this->theme);

        global $fb_page_data, $fb_parsed_page_data;

        $page = self::invokeMethod('WPMigration', 'download_page_data', [$this->fb_id]);

        // DEBUG
        // tprint($page); // use the output of this to update $fb_page_data

        $fb_page_data_arr = obj2array($fb_page_data);
        $page_arr = obj2array($page);

        // true_var_export_to_file($fb_page_data_arr, 'fb_page_data_arr'); // DEBUG
        // true_var_export_to_file($page_arr, 'page_arr'); // DEBUG

        // TODO: figure out why these are not equal on comparison and remove this
        // (hint: figure out what data is time variable in them)
        unset($fb_page_data_arr['posts']);
        unset($page_arr['posts']);

        $this->assertEquals($fb_page_data_arr, $page_arr);

        $parsed_page_data = self::invokeMethod('WPMigration', 'parse_page_data', [$page]);

        // DEBUG
        // tprint($parsed_page_data); // use the output of this to update $fb_parsed_page_data

        // TODO: figure out why these are not equal and uncomment the following
        // (hint: figure out what data is time variable in them)
        /*
        $fb_parsed_page_data_arr = obj2array($fb_parsed_page_data);
        $parsed_page_data_arr = obj2array($parsed_page_data);
        $this->assertEquals($fb_parsed_page_data_arr, $parsed_page_data_arr);
        */
    }


    public function test_create_user()
    {
        $this->markTestIncomplete(); return;

        $this->fb_id = '310783022385273';
        $this->theme = 'parallax';

        $this->wpm = new WPMigration($this->fb_id, $this->theme);

        $user_data = array(
            'email'     => $this->wpm->slug ."@facebook.com",
            'username'  => $this->wpm->slug
        );
        $user_id = $this->invokeMethod($this->wpm, 'create_user', [$user_data]);
        $user = get_user_by('id', $user_id);
        if (!$user) $this->fail('Unable to create user');
        $created_user_data = (array) $user->data;
        $expected_user_data = [
            'user_login'	=> $user_data['username'],
            'user_nicename' => $user_data['username'],
            'user_email'	=> $user_data['email'],
            // 'nickname'		=> $data['username'],
            'display_name'	=> $user_data['username'],
        ];
        foreach ($expected_user_data as $k => $v) {
            if (!isset($created_user_data[$k])) {
                $this->fail("Created user with missing user data ('{$k}' should be '{$v}', but it's empty)");
            } elseif($created_user_data[$k] != $v) {
                $this->fail("Created user with wrong user data ('{$k}' should be '{$v}', but it's '{$created_user_data[$k]}'')");
            }
        }
        if (empty($user->allcaps['free'])) $this->fail("Created user doesn't have the 'free' role");
    }

    // TODO: get this working
    /*
    public function test_create_blog()
    {
        $blog_id = $this->invokeMethod($this->wpm, 'create_blog');
        if (!$blog_id) $this->fail('Unable to create blog');
        $blog_details = get_blog_details($blog_id);
        tprint($blog_details);
    }
    */


    public static function invokeMethod($object, $methodName, array $params = [])
    {
        $reflection = new \ReflectionClass(is_string($object) ? $object : get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(is_object($object) ? $object : null, $params);
    }
}
