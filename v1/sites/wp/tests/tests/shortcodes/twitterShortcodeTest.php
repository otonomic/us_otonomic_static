<?php

class twitterShortcodeTest extends WP_UnitTestCase {

    public function setUp()
    {
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/otonomic-first-session.php";
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/public/includes/shortcodes/twitter.php";
    }

    function testTwitterBasic() {
        $result = wp_otonomic_twitter_handler([]);
        $expected = '[twitter username="" show_count="3" show_follow="1"]';

        $this->assertEquals($result, $expected);
    }

    function testTwitterChangeUsername(){
//        wp_otonomic_set_settings('social_media_twitter', 'otonomic2');
//        $username = wp_otonomic_get_settings('social_media_twitter');

        $username = 'otonomic2';
        $result = wp_otonomic_twitter_handler(['username' => $username]);
        $expected = '[twitter username="'. $username .'" show_count="3" show_follow="1"]';

        $this->assertEquals($result, $expected);
    }
}
 