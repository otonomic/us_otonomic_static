<?php

class fblikeShortcodeTest extends WP_UnitTestCase {

    public function setUp()
    {
        require_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/otonomic-first-session.php";
        require_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/includes/plugin-functions.php";
        require_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/public/includes/shortcodes/fb_like.php";
    }

    function testFBLikeBasic() {
        $result = otonomic_fb_like_handler(['link' => 'http://facebook.com/otonomic']);
        $expected = '<iframe style="border: none; overflow: hidden; width: 100%; height: 100%;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2Fotonomic&amp;width=100%&amp;height=100%&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color&amp;header=true&amp;appId=319378544859599" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>';

        $this->assertEquals($result ,$expected);
    }

    function testFBLikeChangeLink(){
        $link = 'http://facebook.com/otonomic2';
        // wp_otonomic_set_settings('facebook_link', $new_link);
        // $link = wp_otonomic_get_settings('facebook_link');

        $result = otonomic_fb_like_handler(['link' => $link]);
        $expected = '<iframe style="border: none; overflow: hidden; width: 100%; height: 100%;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2Fotonomic2&amp;width=100%&amp;height=100%&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color&amp;header=true&amp;appId=319378544859599" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>';

        $this->assertEquals($result, $expected);
    }
}
