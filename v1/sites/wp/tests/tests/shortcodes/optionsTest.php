<?php

class optionsTest extends WP_UnitTestCase {

    public function setUp()
    {
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/otonomic-first-session.php";
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/public/includes/shortcodes/options.php";
    }

    function testOptionHandler() {
        wp_otonomic_set_settings('contact_address', 'Israel');

        $maphtml = otonomic_options_handler(["before"=>"<b>","after"=>"</b>", "option"=> 'contact_address']);
        $expected = '<b>Israel</b>';

        $this->assertEquals($maphtml,$expected);
    }

    function testSocialHandler() {

       // wp_otonomic_set_settings('contact_address', 'Israel');

        $social = wp_otonomic_social_handler([]);
        $expected =<<<E
[col grid='2-1 first']<h3>Facebook</h3><iframe style="border: none; overflow: hidden; width: 310px; height: 310px;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2Fotonomic2&amp;width=310px&amp;height=310px&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color&amp;header=true&amp;appId=319378544859599" height="310px" width="310px" frameborder="0" scrolling="no"></iframe>[/col][col grid='2-1 last'][otonomic_twitter show_count="3" show_follow=true before="<h3>Twitter</h3>"][/col]
E;


        $this->assertEquals($social,$expected);

    }

    function testSocialHandlerTwitter() {

        // wp_otonomic_set_settings('contact_address', 'Israel');

        $social = wp_otonomic_social_handler(['network'=>'twitter']);
        $expected =<<<E
[col grid='2-1 first'][otonomic_twitter show_count="3" show_follow=true before="<h3>Twitter</h3>"][/col]
E;

        $this->assertEquals($social,$expected);

    }



    function testSocialHandlerMulti() {
        // wp_otonomic_set_settings('contact_address', 'Israel');

        $social = wp_otonomic_social_handler(['network'=>'twitter,youtube']);
        $expected =<<<E
[col grid='2-1 first'][otonomic_twitter show_count="3" show_follow=true before="<h3>Twitter</h3>"][/col][col grid='2-1 last'][/col]
E;

        $this->assertEquals($social,$expected);

    }
}
 