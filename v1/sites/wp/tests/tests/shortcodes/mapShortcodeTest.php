<?php

class mapShortcodeTest extends WP_UnitTestCase {

    public function setUp()
    {
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/otonomic-first-session.php";
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/public/includes/shortcodes/map.php";
    }

    function testMapBasic() {
        $maphtml = otonomic_map_handler(['address' => 'beer-sheva']);
        $expected = '[map address="beer-sheva" width="100%" height="600px"]';

        $this->assertEquals($maphtml,$expected);

    }

    function testMapChangeAddress(){
        $address = 'beer-sheva, Israel';
        // wp_otonomic_set_settings('contact_address', $new_address);
        // $address = wp_otonomic_get_settings('contact_address');

        $maphtml = otonomic_map_handler(['address' => $address]);
        $expected = '[map address="'.  $address .'" width="100%" height="600px"]';

        $this->assertEquals($maphtml, $expected);

    }
}
 