<?php

class WPMigrationTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        // include_once '../bootstrap-plain-phpunit.php';
        include_once OT_APP_ROOT . "/migration/WPMigration.php";
    }

    public function test_parse_facebook_hours(){
        $input = json_decode('{
            "mon_1_open": "11:30",
            "mon_1_close": "15:00",
            "tue_1_open": "11:30",
            "tue_1_close": "15:00",
            "wed_1_open": "11:30",
            "wed_1_close": "15:00",
            "thu_1_open": "11:30",
            "thu_1_close": "15:00",
            "fri_1_open": "11:30",
            "fri_1_close": "15:00",
            "sat_1_open": "11:30",
            "sat_1_close": "15:00",
            "sun_1_open": "11:30",
            "sun_1_close": "15:00",
            "mon_2_open": "18:00",
            "mon_2_close": "00:00",
            "tue_2_open": "18:00",
            "tue_2_close": "00:00",
            "wed_2_open": "18:00",
            "wed_2_close": "00:00",
            "thu_2_open": "18:00",
            "thu_2_close": "00:00",
            "fri_2_open": "18:00",
            "fri_2_close": "00:00",
            "sat_2_open": "18:00",
            "sat_2_close": "00:00",
            "sun_2_open": "18:00",
            "sun_2_close": "00:00"
            }');

        $expected = 'Mon: 11:30-15:00
Tue: 11:30-15:00
Wed: 11:30-15:00
Thu: 11:30-15:00
Fri: 11:30-15:00
Sat: 11:30-15:00
Sun: 11:30-15:00
Mon: 18:00-00:00
Tue: 18:00-00:00
Wed: 18:00-00:00
Thu: 18:00-00:00
Fri: 18:00-00:00
Sat: 18:00-00:00
Sun: 18:00-00:00';
        $result = $this->invokePrivateOrProtectedMethod( $class = 'WPMigration', $method = 'parse_facebook_hours', $args = [$input]);

        // $this->assertEquals($result, $expected);
    }

    function invokePrivateOrProtectedMethod($class, $method, $args) {
        $reflection_class = new ReflectionClass('WPMigration');
        $method = $reflection_class->getMethod( $method);
        $method->setAccessible(true);

        if(is_string($class)) {
            $result = $method->invokeArgs(null, $args);
        } else {
            $result = $method->invokeArgs($class, $args);
        }
        return $result;
    }
}
 