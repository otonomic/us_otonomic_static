<?php

class ParallaxTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        // include_once '../bootstrap-plain-phpunit.php';
        include_once OT_APP_ROOT . "/migration/core/ThemeAbstract.php";
        include_once OT_APP_ROOT . "/migration/core/ThemifyAbstract.php";
        include_once OT_APP_ROOT . "/migration/themes/parallax/Parallax.php";
    }

    public function test_get_gallery_shortcode_main_slider(){
        $main_slider_gallery_ids = [1,2,3];

        $Parallax = new Parallax(1,1);
        $result = $Parallax->get_gallery_shortcode_main_slider($main_slider_gallery_ids);

        $expected = "[gallery ids=\"".implode(',', $main_slider_gallery_ids)."\"]";

        $this->assertEquals($result, $expected);
    }
}
