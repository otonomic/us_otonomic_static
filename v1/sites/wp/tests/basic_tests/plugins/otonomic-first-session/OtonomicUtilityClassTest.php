<?php

class OtonomicUtilityClassTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        // include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/";
        // include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/includes/classes/site/utility/otonomic_utility.class.php";
    }

    function test_sanity() {
        $this->assertTrue(true);
    }

    function invokePrivateOrProtectedMethod($class, $method, $args) {
        $reflection_class = new ReflectionClass('WPMigration');
        $method = $reflection_class->getMethod( $method);
        $method->setAccessible(true);

        if(is_string($class)) {
            $result = $method->invokeArgs(null, $args);
        } else {
            $result = $method->invokeArgs($class, $args);
        }
        return $result;
    }
}
 