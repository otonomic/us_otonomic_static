<?php

class OtonomicFunctionsClassTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        include_once OT_APP_ROOT . "/wp-content/mu-plugins/otonomic-first-session/includes/classes/site/utility/otonomic_functions.class.php";
    }

    public function test_filter_array_by_attribute(){
        $array = json_decode('[
            {"name" : "mobile", "data" : "mobile data"},
            {"name" : "cover photos", "data" : "cover photos data"},
            {"name" : "album1", "data" : "album1 data"}
        ]');

        $result = otonomic_functions::filter_array_by_attribute( $array, $att = 'name', $value = 'cover photos');
        $expected = json_decode('{"name" : "cover photos", "data" : "cover photos data"}');
        $this->assertEquals($result, $expected);
    }



    function invokePrivateOrProtectedMethod($class, $method, $args) {
        $reflection_class = new ReflectionClass('WPMigration');
        $method = $reflection_class->getMethod( $method);
        $method->setAccessible(true);

        if(is_string($class)) {
            $result = $method->invokeArgs(null, $args);
        } else {
            $result = $method->invokeArgs($class, $args);
        }
        return $result;
    }
}
 