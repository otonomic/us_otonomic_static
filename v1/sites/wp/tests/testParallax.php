<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Omri
 * Date: 21/06/14
 * Time: 01:24
 * To change this template use File | Settings | File Templates.
 */

$migration_root = __DIR__ . '/../';
include_once $migration_root . '../wp-load.php';

include_once $migration_root . 'config/fb.php';

include_once $migration_root . 'helpers/Curl.php';
include_once $migration_root . 'helpers/Section.php';
include_once $migration_root . 'helpers/DB.php';

include_once $migration_root . 'WPMigration.php';

include_once $migration_root . "plugins/core/Factory.php";

include_once $migration_root . "core/ThemeAbstract.php";
include_once $migration_root . "core/ThemifyAbstract.php";
include_once $migration_root . "core/interfaces/ThemeInterface.php";
include_once $migration_root . "core/interfaces/ThemePluginSupportInterface.php";

$blog_id = 170;
switch_to_blog( $blog_id );

$domain = DOMAIN_CURRENT_SITE;


include_once $migration_root . "themes/parallax/Parallax.php";


$theme = new Parallax($blog_id, $page=null);

$post_id = $theme->add_section_youtube();
echo "Added post #{$post_id} to blog #".get_current_blog_id();