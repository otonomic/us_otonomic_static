<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('APP_ROOT', dirname(__DIR__));
define('OT_APP_ROOT', APP_ROOT);

define('APP_ENV', getenv('APPLICATION_ENV'));

define('ANALYTICS_ID','UA-37736198-1');
define('OTONOMIC_GET_OPTIONS_FROM_SERIALIZED_KEY', false);

define('AUTOSAVE_INTERVAL', 500);
define('WP_POST_REVISIONS', false);
define('WP_AUTO_UPDATE_CORE', false);

// IAM user for CloudFront and S3 only
define( 'AWS_ACCESS_KEY_ID', 'AKIAIPI52IDKIFN5ANBA' );
define( 'AWS_SECRET_ACCESS_KEY', 'XKkQaCK3LJI6OGG3vK+fCbf9P3DJxbQu8ANM/Vmo' );

 // Added by W3 Total Cache
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

$localhost = true;
define( 'LOCALHOST', 1);

define('WP_DEBUG', true);
define('SCRIPT_DEBUG', false);
define('WP_DEBUG_LOG', true);

if (defined('DOING_AJAX') && DOING_AJAX) {
    define('WP_DEBUG_DISPLAY', isset($_GET['debug']) ? $_GET['debug'] : false);

} else {
    error_reporting(E_ALL); ini_set('display_errors', 1);
    define('WP_DEBUG_DISPLAY', isset($_GET['debug']) ? $_GET['debug'] : true);
}

define('DB_NAME', 'wp_testing');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');

// define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'wp.test');
define('DOMAIN_FOR_SITE_CREATION', 'wp.test');
define('DOMAIN_FOR_THE_SYSTEM', 'wp.test');

define('PIWIK_URL', 'a.otonomic.com/');
define('PIWIK_SITE_ID', 1);


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'otosalt');
define('SECURE_AUTH_KEY',  'otosalt');
define('LOGGED_IN_KEY',    'otosalt');
define('NONCE_KEY',        'otosalt');
define('AUTH_SALT',        'otosalt');
define('SECURE_AUTH_SALT', 'otosalt');
define('LOGGED_IN_SALT',   'otosalt');
define('NONCE_SALT',       'otosalt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wptests_';   // Only numbers, letters, and underscores please!

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'en_US');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define( 'WP_ALLOW_MULTISITE', true );

// define('MULTISITE', true);

define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('ALLOW_ALL_EXTERNAL_SITES', true);

// TODO: Copy file to this location
include_once(ABSPATH . 'white_label/otonomic.original.php');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
if(!defined( 'SKIP_WP_LOAD')) {
//    require_once(ABSPATH . 'wp-settings.php');
}

define( 'DISALLOW_FILE_EDIT', true );

define( 'PROMOTED_PRICING_PLAN_LINK', 'https://www.plimus.com/jsp/buynow.jsp?contractId=2327123');



define( 'WP_TESTS_DOMAIN', 'wp.test' );
define( 'WP_TESTS_EMAIL', 'admin@wp.test' );
define( 'WP_TESTS_TITLE', 'Test Blog' );

define( 'WP_PHP_BINARY', 'php' );
