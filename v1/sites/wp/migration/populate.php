<?php
ignore_user_abort(true);
set_time_limit(0);
$base_dir = __DIR__.'/';
include_once $base_dir . '../wp-load.php';

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');

include_once $base_dir . 'config/fb.php';
include_once $base_dir . 'helpers/Curl.php';
include_once $base_dir . 'helpers/Page2site.php';
include_once $base_dir . 'helpers/Utility.class.php';
include_once $base_dir . 'WPMigration.php';


$blogs = false;
if(isset($_GET['site_id']))
{
	$blog = new stdClass();
	$blog->blog_id = $_GET['site_id'];

	$blogs = array($blog);
}
else
{
	$blogs = Utility::get_all_blogs();
}
$action = $_GET['action'];
global $wpdb;
switch($action)
{
	case 'populate_sites_table':
		$wp = new WPMigration($facebook_id, $theme);
		$wp->save_all_sites_to_otonomic_sites_table();
	break;
	case 'populate_seo_data':
		if(count($blogs)>0)
		{
			foreach ( $blogs as $blog )
			{
				$blog_id = $blog->blog_id;
				switch_to_blog( $blog_id );
				$wpdb->set_blog_id($blog_id);
				Utility::setup_site_seo_data();
			}
		}
	break;
	case 'populate_post_author':
		if(count($blogs)>0)
		{
			foreach ( $blogs as $blog )
			{
				$blog_id = $blog->blog_id;
				switch_to_blog( $blog_id );
				$wpdb->set_blog_id($blog_id);
				Utility::populate_post_author();
			}
		}
		break;
	case 'submit_to_search_engines':
		if(count($blogs)>0)
		{
			foreach ( $blogs as $blog )
			{
				$blog_id = $blog->blog_id;
				switch_to_blog( $blog_id );
				$wpdb->set_blog_id($blog_id);
				Utility::submit_to_search_engines();
			}
		}
		break;
}
restore_current_blog();
exit;