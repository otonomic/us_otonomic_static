<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

include_once '../wp-load.php';

$address = htmlspecialchars($_REQUEST['address']);
$email = htmlspecialchars($_REQUEST['email']);
$phone = htmlspecialchars($_REQUEST['phone']);

$blog_id = $_REQUEST['blog_id'];
if(!isset($blog_id) || (int) $blog_id == 0){
    echo "set site_id via GET"; exit;
}

switch_to_blog($blog_id);

$theme = $_REQUEST['theme'];

if(!isset($theme))
    $theme = 'parallax';


switch ($theme){

    case 'simfo':
        save_simfo_contact();

    case 'parallax':
    default:
        save_parallax_contact();
}

function get_contact($post_name){
    global $wpdb, $blog_id;

    $result = $wpdb->get_results( 'SELECT ID,post_content FROM wp_'. $blog_id .'_posts WHERE post_name = "'.$post_name.'" AND post_type="page"');

    if(empty($result)){
        return false;
    }
    return $result[0];
}

function save_contact($post_id, $contact){
    global $wpdb, $blog_id;

    $contact_post = array(
        'ID'           => $post_id,
        'post_content' => $contact
    );
    $result = wp_update_post($contact_post, true );
}

function save_parallax_contact(){
    global $wpdb, $address, $blog_id;

    $post_name = 'contact';
    //get the content of the contact page
    $data = get_contact($post_name);

    $post_id = $data->ID;
    $content = $data->post_content;

    echo $content;
    //update the address information using regular expression
    $pattern = "/\&q=(.*)\"/";
    $content = preg_replace($pattern, '&q='.$address.'"', $content);

    echo $content;

    save_contact($post_id, $content);
}

/**
 * TODO :Fix this method for simfo template
 */
function save_simfo_contact(){
    global $wpdb, $blog_id, $address, $phone;

    $content = get_contact('contact');
}

