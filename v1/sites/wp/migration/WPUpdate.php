<?php


class WPUpdate extends ThemifyAbstract{

    const FB_TOKEN = "389314351133865|O4FgcprDMY0k6rxRUO-KOkWuVoU";

    protected $blog_id;
    protected $slug;

    protected $since;

    public function __construct($blog_id){

        $this->blog_id = $blog_id;

        switch_to_blog($this->blog_id);

        $this->setSlug();

    }

    public function update(){

        $this->since = $this->getLastUpdateTimestamp();

        $this->updatePosts();
        $this->updateTestimonials();
        $this->updateAlbums();
        $this->updatePhotos();
        $this->updateCoverAlbum();
        $this->updateVideos();

        $this->setLastUpdateTimestamp();
    }

    private function setSlug(){
        $blog = get_blog_details( $this->blog_id );

        $fb_id = get_option( 'facebook_id' );
        $this->slug = $fb_id;
//        return;
//        if (strpos($blog->domain,'wp.test') !== false) {
//            $this->slug = trim($blog->path, '/');
//        } else {
//            $domain = explode(".", $blog->domain);
//            $this->slug = $domain[0];
//        }

    }

    private function getLastUpdateTimestamp(){
        return get_option( 'last_update' );
    }

    private function setLastUpdateTimestamp(){
        update_option( 'last_update', time() );
    }

    private function updatePosts(){

       $url = 'https://graph.facebook.com/v2.0/'. $this->slug .'/posts/?fields=likes.limit(1),comments.limit(1),message,picture,full_picture,source&since='. $this->since .'&access_token='. self::FB_TOKEN;
       $new_posts = json_decode(file_get_contents($url));

        if(count($new_posts->data) == 0)
            return;

       foreach($new_posts->data as $key=>$post){
           $this->add_post($post);
       }

        echo count($new_posts->data) ." posts added<br>";

    }

    private function updateVideos(){

        $videos_url = "https://graph.facebook.com/v2.0/". $this->slug ."/videos/?since=". $this->since ."&access_token=". USER_TOKEN;
        $new_videos = json_decode(file_get_contents($videos_url));


        if(count($new_videos->data) == 0)
            return;

        //if we dont have videos album, lets create it
        $video_album = get_posts(array('name' => 'videos', 'post_type' => 'portfolio'));
        $video_album_id = $video_album[0]->ID;

        if(count($video_album) == 0){
            $this->add_videos_album($new_videos->data);
            echo count($new_videos->data) ." videos added<br>";
            return;
        }

        $added = 0;
        $new_video_ids = [];
        foreach($new_videos->data as $key=>$video){


            if(strtotime($video->created_time) > $this->since){
                $new_video_ids[] = $this->add_video($video, $video_album_id);
                $added++;
            }
        }


        $this->add_to_gallery_shortcode($video_album_id,$new_video_ids );

        echo $added ." videos added<br>";

    }


    private function updateAlbums(){

        $albums_url = "https://graph.facebook.com/v2.0/". $this->slug ."/albums?since=". $this->since ."&access_token=". self::FB_TOKEN ."&fields=id,name,link,cover_photo,from,type,description,count,photos.fields(id,picture,source,images)";

        $new_albums = json_decode(file_get_contents($albums_url));

        if(count($new_albums->data) == 0)
            return;

        foreach($new_albums->data as $key => $album){
            $this->add_album($album);
        }

        echo count($new_albums->data) ." albums added<br>";

    }

    private function updatePhotos(){
       $current_albums = get_posts( ['post_type'=>'portfolio'] );

        foreach($current_albums as $key => $album){
            $this->updateExisitingAlbum($album);
        }

    }

    private function updateExisitingAlbum($album){

        if($album->post_name == 'videos')
            return;

        $album->guid = $str_numbers_only = preg_replace("/[^\d]/", "", $album->guid);

        $url = 'https://graph.facebook.com/v2.0/'. $album->guid .'/photos?fields=id,picture,source,images&since='. $this->since .'&access_token='. self::FB_TOKEN;
        $new_photos = json_decode(file_get_contents($url));


        if(count($new_photos->data) == 0)
            return;


        $cover_album_id = get_option( 'cover_photos_album_facebook_id' );
        $new_photo_ids = [];
        foreach($new_photos->data as $key => $photo){

            if($cover_album_id == $album->ID)
                continue;

            $new_photo_ids[] = $this->add_image($photo, $album->ID);
        }

        $this->add_to_gallery_shortcode($album->ID, $new_photo_ids );


        echo count($new_photos->data) ." photos added to album ". $album->name ."<br>";

    }


    private function updateCoverAlbum(){


        $cover_album_id = get_option( 'cover_photos_album_post_id' );
        $cover_album_fb_id = get_option( 'cover_photos_album_facebook_id' );

        $url = 'https://graph.facebook.com/v2.0/'. $cover_album_fb_id .'/?fields=photos.fields(id,picture,source,images)&since='. $this->since .'&access_token='. self::FB_TOKEN;

        $new_photos = json_decode(file_get_contents($url));

        // update slider_images in wp_options ar array of ID's
       // $slider_images = get_option('slider_images');

        $home_section_id = $this->get_page_id(['name'=>'home']);

        $background_gallery = get_post_meta($home_section_id, 'background_gallery', true );


        $current_ids = explode("\"", $background_gallery);
        $slider_images = explode(",",$current_ids[1]);

        if(count($slider_images) == $new_photos->photos->data)
            return;

        if(count($new_photos->photos->data) == 0)
            return;

        foreach($new_photos->photos->data as $key=>$picture):

            // if cover photo && is big

//            if(strtotime($picture->created_time) < $this->since)
//                continue;

            $big_found = false;

            $picture->source = $picture->images[0]->source;

            if((int) $picture->images[0]->width > 900){
                //big picture!
                $big_found = true;
            }

            // if big == 1 use the big array, else use regular array

            $image_id = $this->add_image($picture, $cover_album_id);

            $image_ids[] = $image_id;

            if($big_found){
                array_unshift($slider_images , $image_id);
                echo "found new picture for the slider <br>";
            }

        endforeach;


        if(count($slider_images) > 0){
           update_option('slider_images', $slider_images);
           $slider_images_stringify = implode(",", $slider_images);
           update_post_meta($home_section_id, 'background_gallery','[gallery ids="'. $slider_images_stringify .'"]');
        }

        echo count($new_photos->data) ." photos added to the cover album <br>";


    }

    private function updateTestimonials(){
        $fb_id = get_option( 'facebook_id' );
        $testimonials_url = "http://builder.otonomic.com/resource/fb_id:". $fb_id ."/media:db/resources:review?DEBUG=3";

        $testimonials = json_decode(file_get_contents($testimonials_url));

        $testimonials_added = 0;
        foreach($testimonials->data as $key=>$testimonial){

            if(strtotime($testimonial->date) > $this->since){
                $this->add_team($testimonial);
                $this->add_testimonial($testimonial);
                $testimonials_added++;
            }
        }

        if($testimonials_added > 0)
            echo $testimonials_added ." testimonaials added <br>";

    }




    private function add_to_gallery_shortcode($parent_post_id, $array_of_ids){

        $gallery_short_code = get_post_meta( $parent_post_id, 'gallery_shortcode', true );
        $comma_seperated_current_images = explode("\"", $gallery_short_code);
        $current_images = explode(",", $comma_seperated_current_images[1]);

        $new_album_image_list = array_merge($current_images, $array_of_ids);
        $comma_seperated_new_images = implode(',', $new_album_image_list);

        $new_shortcode = '[gallery ids="'. $comma_seperated_new_images .'" link="file"]';
        update_post_meta($parent_post_id, 'gallery_shortcode', $new_shortcode);

    }



}