<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/16/14
 * Time: 4:12 PM
 */

interface ThemeInterface {

    public function install();

    public function init();
    public function save_content();
    public function save_theme_data();
    public function finalize();

    public function setup_modules();

	public function add_module_about();
	public function add_module_services();
	public function add_module_portfolio();
	public function add_module_testimonials();
	public function add_module_blog();


	public function add_module_contact();
	// public function add_module_plugin_booking();
	// public function add_module_store();
}