<?php
class OtonomicMigrationLoader
{
	private static $__loader;
	private $base_dir;
	private $exts;
	private $invalid_chars;

	private function __construct ( $base_dir )
	{
		$this->exts = array ('.class.php');
		$this->invalid_chars = array(
			'.', '\\', '/', ':', '*', '?', '"', '<', '>', "'", '|'
		);
		$this->base_dir = $base_dir;
		spl_autoload_register ( array ( $this, 'auto_load_config' ) );
	}
	public static function init ( $base_dir )
	{
		if ( self::$__loader == NULL )
		{
			self::$__loader = new self( $base_dir );
		}
		return self::$__loader;
	}
	public function auto_load_config( $class )
	{
		$load_path = $this->base_dir.'/config/';
		$this->auto_load( $class, $load_path );
	}
	private function auto_load($class, $load_path)
	{

		$class = str_replace($this->invalid_chars, '', $class);

		spl_autoload_extensions("'" . implode ( ',', $this->exts ) . "'");
		set_include_path(get_include_path() . PATH_SEPARATOR . $load_path);

		foreach ($this->exts as $extention)
		{
			if( is_readable ( $path = $load_path . $class . $extention) )
			{
				require_once $path ;
				return true;
			}
		}
		self::recursive_auto_load( $class, $load_path, $this->exts);
	}
	private static function recursive_auto_load( $class, $path, $exts )
	{
		foreach ($exts as $extention)
		{
			if( is_readable ( $file = $path . '/' . $class.$extention))
			{
				require_once $file;
				return true;
			}
		}
		if( is_dir ( $path))
		{
			if ( ($handle = opendir ( $path)) !== FALSE )
			{
				while (( $resource = readdir($handle) ) !== FALSE)
				{
					if( ($resource == '..') OR ($resource == '.'))
					{
						continue ;
					}
					if( is_dir ( $dir = $path . '/' . $resource))
					{
						self::recursive_auto_load($class, $dir, $exts);
					}
				}
				closedir( $handle);
			}
		}
	}
}