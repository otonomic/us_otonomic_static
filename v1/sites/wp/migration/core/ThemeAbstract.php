<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/16/14
 * Time: 4:12 PM
 */

abstract class ThemeAbstract {
    var $modules = [
        'slider',
        'about',
        'blog',
        'portfolio',
        'testimonials',
        'store',
        'social',
        'contact',
    ];


    protected $blog_id;
    protected $page;

    protected $theme;
	protected $fb_category_object;

    protected $plugins_list;

	private $fb_imported_images = array();

    public function __construct($blog_id, $page, $fb_category_object){
        if(!defined('SECTION_CATEGORY_HOME')) {
            define('SECTION_CATEGORY_HOME', 24);
        }

        $this->blog_id = $blog_id;
        $this->page = $page;
	    $this->fb_category_object = $fb_category_object;

        // TODO: Log the user in

         // $this->plugins_list = $this->get_plugins_list();
        //$this->plugins_list = ['Booking', 'ContactForm7', 'WooCommerce'];
    }

	function filter_modules()
	{
		if(!is_null($this->fb_category_object))
		{
			foreach($this->modules as $key=>$module)
			{
				if(!$this->fb_category_object->get_module_status( $module ))
				{
					unset($this->modules[$key]);
				}
			}
		}
		$this->modules = array_values($this->modules);
	}

    public function install()
    {
	    $this->filter_modules();
        $this->init();
        $this->save_site_data();
        $this->save_content();

	    $this->setup_store();

        $this->setup_modules();

        $this->setup_plugins();

        $this->save_theme_data();
        $this->finalize();

        return $this->blog_id;
    }
	public function setup_store()
	{
		$OtonomicStore = OtonomicStore::get_instance();
		$OtonomicStore->show_store_pages(false);
		$OtonomicStore->hide_store_pages();
	}

    public function switch_to_theme() {
        include_once "../themes/{$this->theme}/sql/first_data.php";
        $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        $this->setup_modules();
        $this->setup_plugins();
        $this->save_theme_data();
    }

    public function switch_from_theme() {
        /* Hide current home page */
        $frontpage_id = get_option('page_on_front');
        /* Make this page as draft */
        $post = array(
            'ID'=>$frontpage_id,
            'post_status'  => 'draft',
        );
        wp_update_post( $post );
        /* Fetch about page content */
        $page_obj = get_page_by_title( 'About', OBJECT, 'section' );

        $pages = array('about'=>$page_obj->post_content);

        return $pages;

    }

    function export_testimonials()
    {
        /* We will need to read and return the testimonials */

    }

    public function init(){
        // global $wpdb;
        $domain = DOMAIN_CURRENT_SITE;
        $blog_id = $this->blog_id;

        // TODO: Export updated first_data file with minimized database entries
        // NOTE: This first_data file contains template specific data
        include_once dirname(__FILE__) . "/../themes/{$this->theme}/sql/first_data.php";
        $this->set_site_settings();
        // $this->save_taxonomies();
    }

    public function save_taxonomies() {
        // Add term (table wp_terms) "Home" (would be category for sections)
        // term_id, name, slug, term_group
        // 24	Home	home	0

        // Add term "Testimonials"
        // 33	Testimonials	testimonials	0

        // Add term taxonomy (to table terms_taxonomy)
        // 25	24	section-category		0	7
    }

    function set_main_site_slider() {
        $images_for_slider = $this->get_images_for_slider( $this->page->albums);

        if(is_array( $images_for_slider)) {
            $images_for_slider = $images_for_slider[0];
        }

        add_option('background_image', $images_for_slider->source);

        return $images_for_slider;
    }

    public function finalize(){
        $this->setup_SEO();
	    $this->add_site_score();
    }
	public function add_site_score(){

		//slider images
		$slider_images = $this->get_images_for_slider( $this->page->albums);
		$slider_images_count = count($slider_images);

		//photos
		$photos_count = 0;
		foreach($this->page->albums as $album)
		{
			$album_photos = $album->photos->data;
			if(!isset($album_photos)) { continue; }

			foreach((array)$album_photos as $picture)
			{
				$photos_count++;
			}
		}


		//testimonials
		$testimonials = $this->page->testimonials;
		$testimonials_count = 0;
		if($testimonials && count($testimonials) > 0) {
			foreach($testimonials as $testimonial) {
				if (!$testimonial->message) {
					continue;
				}
				$testimonials_count++;
			}
		}

		//about num
		$available_fields = array(
			'description',
			'company_overview',
			'hours',
			'general_info',
			'personal_info',
			'mission',
			'personal_interests',
			'description_html',
			'bio',
			'genre',
			'band_interests'
		);
		$about_length = 0;
		if($this->page->facebook->about){
			$about_length = strlen($this->page->facebook->about);
		}

		foreach($available_fields as $field) {
			if(isset($this->page->facebook->{$field})) {
				$data = $this->page->facebook->{$field};

				if($field == "hours"){
					$data = $data['str'];
				}

				if(is_array($data)) {
					$data = implode(", ", $data);
				};

				$about_length += strlen($data);
			}
		}


		$site_score_components = array(
			'num_slider_images' => $slider_images_count,
			'num_photos' => $photos_count,
			'num_testimonials' => $testimonials_count,
			'num_chars_about' => $about_length
		);
		update_option( 'otonomic_site_score_components', $site_score_components);

		if($slider_images_count >= 1 && $photos_count >= 4 && $testimonials_count >= 3 &&  $about_length >= 400){
			update_option('otonomic_pending_quality_site', 'yes');
		}
	}

    function get_plugins_list() {
        if(!empty($this->plugins_list)) {
            return $this->plugins_list;
        }

        $reflector = new ReflectionClass(get_class($this));
        $path = dirname($reflector->getFileName());
        $this->plugins_list = include_once $path.'/config/plugins.php';
        return $this->plugins_list;
    }

    function setup_modules() {
        foreach($this->modules as $module) {
            $func_name = 'add_module_'.$module;
            try {
                if(method_exists($this, $func_name)) {
                    $this->$func_name();
                }
            }
            catch(Exception $e)
            {
                /* Silently ignore the error */
                write_log('Error adding a module in file '.__FILE__);
                write_log($e->getMessage());
            }
        }
    }

    protected function update_options()
    {
        try {
            update_option( 'template', $this->theme );
            update_option( 'stylesheet', $this->theme );
            update_option( 'current_theme', $this->theme );

	        //switch_theme($this->theme,$this->theme);

            update_option( 'last_update', time() );
            update_option( 'facebook_id', $this->page->facebook->id );

            update_option( 'page_comments', 0);

        } catch(Exception $e) {
            /* Silently ignore the error */
        }

    }





    function get_slider_options() {
        return [
            'min_width'                  => 850,
            // 'min_width'                  => 1000,
//            'min_w2h_ratio'              => 1.3,
            'max_num_main_slider_images' => 5,
            'min_num_main_slider_images' => 1,
            'width1' => 800,
            'width2' => 1000,
        ];
    }

    protected function get_page_id_by_name($page_name) {
        return $this->get_page_id(array('name' => $page_name));
    }

    protected function get_page_id(array $options){
        $options['post_type'] = 'page';
        $wpquery = new WP_Query($options);
        if(isset($wpquery->post->ID)) {
            return $wpquery->post->ID;
        } else {
            return false;
        }
    }

    function set_site_settings() {
        update_option( 'slider_options', $this->get_slider_options());
        update_option( 'json_api_controllers', 'core,posts,ototest,store,widgets,adminpage,products,settings,testimonials,menu,menuitems');
        update_option( 'site_role', 'free');
        update_option( 'wp_otonomic_blog_connected', 'yes');
        return true;
    }

    function get_gallery_shortcode_main_slider($main_slider_gallery_ids) {
        return "[gallery ids=\"".implode(',', $main_slider_gallery_ids)."\"]";
    }

    function get_images_for_slider( $albums = []) {
        $slider_images_objects = [];
        $result = [];

        // TODO: Write this code
        $slider_options = $this->get_slider_options();

        include_once ABSPATH . "/wp-content/mu-plugins/otonomic-first-session/includes/classes/site/utility/otonomic_functions.class.php";
        $album = otonomic_functions::filter_array_by_attribute( $albums, 'name', 'Cover Photos');

        if( !empty($album->photos->data)) {
            foreach( (array)$album->photos->data as $key => $image) {
                $image->width = $image->images[0]->width;
                $image->height = $image->images[0]->height;
                $image->source = $image->images[0]->source;
                // if first image
                if($key == 0){
                    if( $image->width > $slider_options['width1']){
                        $slider_images_objects[] = $image;
                    }

                } else {
                    if( $image->width > $slider_options['width2']) {
                        $slider_images_objects[] = $image;
                    }
                }
            }
        }

        if( empty($slider_images_objects)) {
            $maximum_time_passed = 60 * 60 * 24 * 31 * 6; // Max half year passed
            $temp = otonomic_functions::get_max_width_image( $albums, $maximum_time_passed);

            if( empty($temp)) {
                $maximum_time_passed = 3600 * 24 * 365 * 10; // Max 10 years - life time
                $temp = otonomic_functions::get_max_width_image( $albums, $maximum_time_passed);
            }
            $slider_images_objects[] = $temp;
        }
        // not simply check if count($result) == 0, so there is no big images, you can take it from some collection

        $slider_images_objects = array_slice( $slider_images_objects, 0, $slider_options['max_num_main_slider_images']);

        return $slider_images_objects;
    }

    function save_images_locally( $images, $post_id = null) {
        foreach( $images as $image) {
            $result[] = $this->add_image( $image, $post_id, $copy_locally = true);
        }

        return $result;
    }

    function save_images_remotely( $images, $post_id = null) {
        foreach( $images as $image) {
            $result[] = $this->add_image( $image, $post_id, $copy_locally = false);
        }

        return $result;
    }

    protected function add_slider_images_post() {
        $my_album = array(
        'post_title'     => 'slider images',
        'post_content'   => '',
        'post_status'    => 'publish',
        'comment_status' => 'closed',
        'ping_status' 	 => 'closed',
//        'post_type' 	 => '',
        'guid'           => null,
//        'tax_input'       => array('portfolio-category' => ($portfolio_category = 28))
        );

        //return $post_id = wp_insert_post( $my_album );
        return $post_id = otonomic_insert_post($my_album);
    }

    protected function add_image_replicate($fbpost, $post_id)
    {
        if(!$fbpost || !$post_id) { return false; }

        $otonomic_url = otonomic_url::get_instance();

        if(isset($fbpost->images[0])) {
            $fbpost->source = $fbpost->images[0]->source;
            $fbpost->width = $fbpost->images[0]->width;
            $fbpost->height = $fbpost->images[0]->height;
        } else {
            $fbpost->width = null;
            $fbpost->height = null;
        }
        if(empty($fbpost->source)) {
            return false;
        }

        $fbpost->picture        = $otonomic_url->ot_get_source_url($fbpost->picture);
        $fbpost->full_picture   = $otonomic_url->ot_get_source_url($fbpost->full_picture);
        $fbpost->source         = $otonomic_url->ot_get_source_url($fbpost->source);

        $img_content = isset($fbpost->name) ? $fbpost->name : '';

        if(isset($fbpost->user_picture)){
            $thumb = $fbpost->user_picture;
            $full_image = $fbpost->user_picture;

        } else {
            $thumb = ($fbpost->full_picture) ? $fbpost->full_picture : $fbpost->picture;
            $full_image = $fbpost->source;
        }

        //$path_parts = pathinfo($full_image);

        $fb_image = wp_remote_get( $full_image , array('timeout'=>30));
        $fb_image_type = wp_remote_retrieve_header( $fb_image, 'content-type' );

        $fb_image_attachment = wp_upload_bits( basename( $full_image ), '', wp_remote_retrieve_body( $fb_image ) );

        $post_info = array(
            'post_name'      => $fbpost->id,
            'post_content'   => $img_content,
            'post_excerpt'   => $img_content,
            'post_parent'    => $post_id,
            'post_status'    => 'inherit',
            'post_type' 	 => 'attachment',
            'post_mime_type' => $fb_image_type,
        );
        $filename = $fb_image_attachment['file'];

        $image_id = wp_insert_attachment( $post_info, $filename, $post_id );

        if( !function_exists( 'wp_generate_attachment_data' ) )
        {
            require_once(ABSPATH . "wp-admin/includes/image.php");
        }

        $attachment_data = wp_generate_attachment_metadata( $image_id, $filename );
        wp_update_attachment_metadata( $image_id,  $attachment_data );

        $post_meta = [];

        if(isset($fbpost->images)) {
            $img_atts = [
                'num_comments' => $fbpost->comments->count,
                'width' => $fbpost->width,
                'height' => $fbpost->height
            ];

            $post_meta['attributes'] = $img_atts;
            //add_post_meta( $image_id, 'attributes', $img_atts );
            otonomic_insert_postmeta($image_id, $post_meta);
        }

        return $image_id;
    }

    protected function add_image_replicate_cover($fbpost){
        if(!$fbpost) { return false; }

        $otonomic_url = otonomic_url::get_instance();

        if(isset($fbpost->images[0])) {
            $fbpost->source = $fbpost->images[0]->source;
            $fbpost->width = $fbpost->images[0]->width;
            $fbpost->height = $fbpost->images[0]->height;
        } else {
            $fbpost->width = null;
            $fbpost->height = null;
        }
        if(empty($fbpost->source)) {
            return false;
        }

        $fbpost->picture        = $otonomic_url->ot_get_source_url($fbpost->picture);
        $fbpost->full_picture   = $otonomic_url->ot_get_source_url($fbpost->full_picture);
        $fbpost->source         = $otonomic_url->ot_get_source_url($fbpost->source);

        $img_content = isset($fbpost->name) ? $fbpost->name : '';
        
        $full_image = $fbpost->source;

        //$path_parts = pathinfo($full_image);

        $fb_image = wp_remote_get( $full_image , array('timeout'=>300));
        $fb_image_type = wp_remote_retrieve_header( $fb_image, 'content-type' );
        
        add_filter('upload_dir', array(&$this,'cover_wallpage_dir_filter'));


        $set_image_path = basename( $full_image );
        $fb_image_attachment = wp_upload_bits( $set_image_path, '', wp_remote_retrieve_body( $fb_image ) );
        remove_filter('upload_dir' , array(&$this,'cover_wallpage_dir_filter'));
        $post_info = array(
            'post_name'      => $fbpost->id,
            'post_content'   => $img_content,
            'post_excerpt'   => $img_content,
            'post_parent'    => 0,
            'post_status'    => 'inherit',
            'post_type' 	 => 'attachment',
            'post_mime_type' => $fb_image_type,
        );
        $filename = $fb_image_attachment['file'];

        $image_id = wp_insert_attachment( $post_info, $filename, 0 );

        if( !function_exists( 'wp_generate_attachment_data' ) )
        {
            require_once(ABSPATH . "wp-admin/includes/image.php");
        }

        $attachment_data = wp_generate_attachment_metadata( $image_id, $filename );
        wp_update_attachment_metadata( $image_id,  $attachment_data );

        $post_meta = [];

        if(isset($fbpost->images)) {
            $img_atts = [
                'num_comments' => $fbpost->comments->count,
                'width' => $fbpost->width,
                'height' => $fbpost->height
            ];

            $post_meta['attributes'] = $img_atts;
            //add_post_meta( $image_id, 'attributes', $img_atts );
            otonomic_insert_postmeta($image_id, $post_meta);
        }
        
        $image_path_response = get_attachment_icon_src($image_id);
        $response['path'] = $image_path_response[0];
        $response['id'] = $image_id;
        
        return $response;
    }
    
    function cover_wallpage_dir_filter( $param ){
        $mydir = '/cover';

        $param['path'] = $param['path'] . $mydir;
        $param['url'] = $param['url'] . $mydir;

        error_log("path={$param['path']}");  
        error_log("url={$param['url']}");
        error_log("subdir={$param['subdir']}");
        error_log("basedir={$param['basedir']}");
        error_log("baseurl={$param['baseurl']}");
        error_log("error={$param['error']}"); 

        return $param;
    }

    protected function add_image($fbpost, $post_id, $download=false)
    {
        //if($download)
	    if(false)
        {
            return $this->add_image_replicate($fbpost, $post_id);
        }
        else
        {
            return $this->add_image_remote($fbpost, $post_id);
        }
    }


    protected function add_image_remote($fbpost, $post_id)
    {
        if(!$fbpost) {
	        return false;
        }

        if(empty($fbpost->source)) {
            return false;
        }

	    if(!$post_id){
		    $post_id = 0;
	    }

        $otonomic_url = otonomic_url::get_instance();

        $img_content = isset($fbpost->name) ? $fbpost->name : '';

        $fbpost->picture        = $otonomic_url->ot_get_source_url($fbpost->picture);
        $fbpost->source         = $otonomic_url->ot_get_source_url($fbpost->source);

	    /* Better way!! */

	    /*if(isset($fbpost->object_id)){
            $fbpost->thumb = 'http://graph.facebook.com/'.$fbpost->object_id.'/picture?type=thumbnail';
		    $fbpost->picture = 'http://graph.facebook.com/'.$fbpost->object_id.'/picture?type=normal';
		    $fbpost->source = 'http://graph.facebook.com/'.$fbpost->object_id.'/picture?type=normal';
	    }
	    else if(isset($fbpost->id)){*/
	    $fb_post_id = false;
	    if(isset($fbpost->object_id)){
		    $fb_post_id = $fbpost->object_id;
	    }
	    else if(isset($fbpost->id)){
		    $fb_post_id = $fbpost->id;
	    }
        if($fb_post_id) {
	        if(isset($this->fb_imported_images[$fb_post_id]))
		        return $this->fb_imported_images[$fb_post_id];

            $fbpost->thumb = 'http://graph.facebook.com/'.$fb_post_id.'/picture?type=thumbnail';
		    $fbpost->picture = 'http://graph.facebook.com/'.$fb_post_id.'/picture?type=normal';
		    $fbpost->source = 'http://graph.facebook.com/'.$fb_post_id.'/picture?type=normal';

	    }

        $thumb = $fbpost->thumb;
        $medium_image = $fbpost->source;
        // $full_image = isset($fbpost->images[0]) ? $fbpost->images[0]->source : $medium_image;
        $full_image = $medium_image;

        $my_post = array(
            'post_name'      => $fbpost->id,
            'post_content'   => $img_content,
            'post_excerpt'   => $img_content,
            'post_parent'    => $post_id,
            'post_status'    => 'inherit',
            'post_type' 	 => 'attachment',
            'post_mime_type' => 'image/jpeg',
            'guid'           => $fbpost->picture,
//            'filter'        => true
        );
        
        // $image_id = wp_insert_post( $my_post );
	    $image_id = otonomic_insert_post($my_post);
	    if($fb_post_id) {
		    $this->fb_imported_images[$fb_post_id]=$image_id;
	    }

        // insert meta for the photos
        $imagemeta = array(
            'file' => $full_image,
            'sizes' => array
            (
                'thumbnail' => array
                (
                    'file' => $thumb,
                    'mime-type' => 'image/jpeg'
                ),
                'medium' => array
                (
                    'file'=> $medium_image,
                    'mime-type' => 'image/jpeg'
                ),
                'large' => array
                (
                    'file' => $full_image,
                    'mime-type' => 'image/jpeg'
                )
            ));

        $post_meta = [
            '_wp_attached_file' => $full_image,
            '_wp_attachment_metadata' => $imagemeta,
            'external_link' => $fbpost->link
        ];

        $img_atts = [];
        if(isset($fbpost->width))           { $img_atts['width'] = $fbpost->width; }
        if(isset($fbpost->height))          { $img_atts['height'] = $fbpost->height; }
        if(isset($fbpost->comments->count)) { $img_atts['num_comments'] = $fbpost->comments->count; }
        $post_meta['attributes'] = $img_atts;

        otonomic_insert_postmeta( $image_id, $post_meta);

        return $image_id;
    }




    protected function save_notes(){
        $posts = $this->page->notes;

        if(!is_array($posts) || count($posts) == 0)
            return;

        $this->wpdb_disable_autocommit();

	    $notes_term = term_exists( 'note', 'post_tag' );
	    if ($notes_term === null)
	    {
		    /* Create the new term */
		    wp_insert_term(
			    'note', // the term
			    'post_tag', // the taxonomy
			    array(
				    'description'=> 'Note',
				    'slug' => 'note'
			    )
		    );
	    }

        foreach($posts as $post){
            try
            {
                $this->add_note($post);
            }
            catch(Exception $e)
            {
                /* Silently ignore the error */
            }
        }

        $this->wpdb_enable_autocommit();
    }

    protected function save_posts(){
        $posts = $this->page->posts;

        if(!is_array($posts) || count($posts) == 0)
            return;

        $this->wpdb_disable_autocommit();

        foreach($posts as $post){
            try
            {
                $this->add_post($post);
            }
            catch(Exception $e)
            {
                /* Silently ignore the error */
            }
        }

        $this->wpdb_enable_autocommit();
    }

    protected function save_albums(){
        if(!is_array($this->page->albums) || count($this->page->albums) == 0)
            return;

        $this->wpdb_disable_autocommit();

        foreach($this->page->albums as $key=>$album)
        {
            try
            {
                if(strtolower($album->name) == 'cover photos')
                {
                    // if its cover
                    $album_post_id = $this->add_album($album);

                    add_option( 'cover_photos_album_facebook_id', $album->id );
                    add_option( 'cover_photos_album_post_id', $album_post_id );
                }
                else
                {
                    $this->add_album($album);
                }
            }
            catch(Exception $e)
            {
                /* Silently ignore the error */
            }
        }

        $this->wpdb_enable_autocommit();
    }



    protected function add_note($fbpost, $download_image=false) {
        $post_title = strlen($fbpost->message)>0 ? $fbpost->message : $fbpost->message;
        $fbpost->message = make_clickable($fbpost->message);

        $my_post = array(
            'post_title'    => trim_closest_word(strip_tags($post_title), 50, $append_ellipsis = true),
            'post_content'  => $fbpost->message,
            'post_status'   => 'publish',
            'post_type' 	=> 'post',
//            'filter'        => true
        );

        if($fbpost->created_time){
            $post_date = $this->parse_date($fbpost->created_time);
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        }

        $post_id = otonomic_insert_post( $my_post );

        // TODO: Frank: Add taxonomy - note

	    $notes_term = term_exists( 'note', 'post_tag' );
	    $tags[] =  intval($notes_term['term_id']);

	    $term_taxonomy_ids = wp_set_object_terms( $post_id , $tags, 'post_tag', false );

//        $setting_cats = wp_set_post_categories($post_id,1,FALSE);

    }




    public function save_site_data()
    {
        $this->wpdb_disable_autocommit();

	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();
		/* Lets save the website data */
		/* Save facebook id */
        $facebook_id = $this->page->facebook->id;
	    $Otonomic_First_Session->set_settings('facebook_id', $facebook_id);

		/* Save facebook link */
	    $Otonomic_First_Session->set_settings('facebook_link', $this->page->basic->facebook);

	    $Otonomic_First_Session->set_settings('logo_image', $this->get_facebook_profile_picture($this->page->facebook->id, 400));

		/* Save socual media details */
	    $Otonomic_First_Session->set_settings('facebook', $this->page->facebook->username);
        // TODO: Write code that finds the user's twitter, instagram and pinterest account, based on code already written by Omri
        /*
		$Otonomic_First_Session->set_settings('social_media_twitter', $this->page->twitter);
		$Otonomic_First_Session->set_settings('social_media_instagram', $this->page->instagram);
		$Otonomic_First_Session->set_settings('social_media_pinterest', $this->page->pinterest);
        */

		/* Save address */
            $ot_address = trim($this->page->basic->address,', ');
	    $Otonomic_First_Session->set_settings('address', $ot_address);
	    $Otonomic_First_Session->set_settings('location', $this->page->basic->location);
	    $Otonomic_First_Session->set_settings('phone', $this->page->basic->phone);
	    $Otonomic_First_Session->set_settings('website', $this->page->basic->website);

        $scraper = new FbEmailScrape(); $email = json_decode($scraper->getEmail($facebook_id));
        if(!empty($email->result)) {
            $Otonomic_First_Session->set_settings('email', html_entity_decode($email->result));
        }

        $Otonomic_First_Session->set_settings('about', $this->page->facebook->about);
        if($this->page->facebook->hours) {
            $opening_hours_string = $this->page->facebook->hours['str'];
            $opening_hours_array = $this->page->facebook->hours['array'];
	        $Otonomic_First_Session->set_settings('opening_hours', $opening_hours_string);
            $Otonomic_First_Session->set_settings('opening_hours_array', $opening_hours_array);
        } else {
            $opening_hours_string = $this->page->facebook->hours['str'];
            $opening_hours_array = [
                'mon' => ['start' => '12:00', 'end' => '12:00'],
                'tue' => ['start' => '12:00', 'end' => '12:00'],
                'wed' => ['start' => '12:00', 'end' => '12:00'],
                'thu' => ['start' => '12:00', 'end' => '12:00'],
                'fri' => ['start' => '12:00', 'end' => '12:00'],
                'sat' => ['start' => '12:00', 'end' => '12:00'],
                'sun' => ['start' => '12:00', 'end' => '12:00'],
            ];

            $opening_hours_string = "Mon 12:00-12:00
Tue 12:00-12:00
Wed 12:00-12:00
Thu 12:00-12:00
Fri 12:00-12:00
Sat 12:00-12:00
Sun 12:00-12:00";
            $Otonomic_First_Session->set_settings('opening_hours', $opening_hours_string);
            $Otonomic_First_Session->set_settings('opening_hours_array', $opening_hours_array);

        }

        $Otonomic_First_Session->set_settings('site_creation_ip', $this->get_client_ip());
        $Otonomic_First_Session->set_settings('category', $this->page->basic->category);
        $Otonomic_First_Session->set_settings('category_list', $this->page->basic->category_list);
        $Otonomic_First_Session->set_settings('slug', $this->page->basic->slug);

        // Add domain name
	    $Otonomic_First_Session->set_settings( 'user_domain', $this->page->basic->suggested_domain);

        // TODO: In the future add all basic site data
        $Otonomic_First_Session->set_settings( 'otonomic_site_data_facebook', $this->page->facebook);
        $Otonomic_First_Session->set_settings( 'otonomic_site_data_basic',    $this->page->basic);
        
        $this->set_social_profiles($this->page->facebook->username);
        
        $this->wpdb_enable_autocommit();
    }


    function get_client_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }



    public function setup_plugins()
	{
        foreach($this->get_plugins_list() as $key=>$plugin_name)
        {
	        try
	        {
	            $plugin = \Plugins\Core\Factory::make($plugin_name);
	            $plugin->setBlogID($this->blog_id);
	            $plugin->setData($this->page);
	            $plugin->install();

	            $function_name = 'add_module_plugin_'.$plugin_name;
	            if(method_exists($this, $function_name))
	            {
		            $this->$function_name();
	            }
	        }
	        catch(Exception $e)
	        {
		        /* Silently ignore the error */
	        }
        }
    }


    protected function set_album_id_cover_photos()
    {
        if(empty($this->page->albums)) { return false; }

	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();

        foreach($this->page->albums as $album){
            if(strtolower($album->name) == 'cover photos'){
	            $Otonomic_First_Session->set_settings( 'cover_photos_album_facebook_id', $album->id );
                return $album;
            }
        }
        return false;
    }


    function get_facebook_profile_picture($facebook_id, $width = 400, $height = null) {
        if(!$height) { $height = $width; }
        return 'http://graph.facebook.com/'.$facebook_id.'/picture?width='.$width.'&height='.$height;
    }




    function wpdb_disable_autocommit() {
        global $wpdb;
        $wpdb->query( 'SET autocommit = 0;' );
    }

    function wpdb_enable_autocommit() {
        global $wpdb;
        $wpdb->query( 'COMMIT;' );
        $wpdb->query( 'SET autocommit = 1;' );
    }

    protected function set_widgets(){
        $reflector = new ReflectionClass(get_class($this));
        $path = dirname($reflector->getFileName());

        /*$widgets_list = include_once $path.'/config/widgets.php';

        foreach($widgets_list as $widget_location => $widget_name) {
            $widget = \Widgets\Core\Factory::make($widget_name);
            $widget->setBlogID($this->blog_id);
            $widget->setData($this->page);

            // TODO: Check how to add the widget to sidebar\footer location
            $widget->install($widget_location);
        }*/
	    $widgets_list = include_once $path.'/config/widgets.php';

	    if(count($widgets_list)>0)
	    {
		    foreach($widgets_list as $option_name => $option_value)
		    {
			    update_option($option_name, $option_value);
		    }
	    }
    }

    function get_content_about( $logo_bottom=true ) {
        $available_fields = array(
            'description',
            'company_overview',
            'hours',
            'general_info',
            'personal_info',
            'mission',
            'products',
            'personal_interests',
            'description_html',
            'bio',
            'genre',
            'band_members',
            'members',
            'general_manager',
            'band_interests',
            'hometown',
            'current_location',
            'founded',
        );

        $logo_image = get_option( 'logo_image');
        $business_name = esc_attr(get_bloginfo('name'));
        $content = "<div class='page-section-about about'><div class='business-name' style='font-size:2.0em;'>".$business_name." </div>".$this->page->facebook->about."</div>";

        foreach($available_fields as $field) {
            if(isset($this->page->facebook->{$field})) {
                $data = $this->page->facebook->{$field};

                if($field == "hours"){
                    $data = $data['str'];
                }

                if(is_array($data)) { $data = implode(", ", $data); };

                $field_title = ucwords(str_replace('_', ' ', $field));

                if($field == "hours"){
                    $data = nl2br( $data);
                }

                $content .= <<<E
    <div class='page-section-about {$field}'>
        <div class='page-section-title'><b>{$field_title}</b></div>
        <div class='page-section-content'>{$data}</div>
    </div>
E;
            }
        }

        // Wrap content in div and add image to the end.
        $content = <<<E
        <div class="otonomic-core-style about-section">
            <div class="row">
                <div class="col-xs-12">
                    <div class="media">
                      <div class="media-left media-about-img">
                        [otonomic_image src='logo_image']
                      </div>
                      <div class="media-body">
                        {$content}
                      </div>
                    </div>
                </div>
            </div>
        </div>
E;

        $content = make_clickable(  $content );
        return $content;
    }

	function get_content_services() {

		$content = '<div class="loops-wrapper shortcode highlight grid4 highlight-multiple clearfix type-multiple fly-in">';

		$services_content = [
			[
				'title' => 'Weight Loss',
				'icon' => 'fa-gamepad',
				'color' => '#F26522',
				'content' => 'This is the most comprehensive weight loss program ever unveiled. You will receive full step-by-step support in all aspects of the program covering
diet/nutrition, exercise, stress reduction, emotional eating, monthly progress reports, and permanent weight loss & lifestyle management. This program is by far the best of the best when it comes to weight loss. It’s also why we guarantee your results.',
			],
			[
				'title' => 'Strength &amp; Conditioning',
				'icon' => 'fa-film',
				'color' => '#82B53C',
				'content' => 'Whether you are young or old, your body is your vehicle through life and you must keep it strong! Together we will strengthen your joints, connective tissue, muscles, and your cardiovascular system to get you into peak condition and increase your quality of life.',
			],
			[
				'title' => 'Injury Prevention & Rehab',
				'icon' => 'fa-shopping-cart',
				'color' => '#00AEEF',
				'content' => 'After we review your overhead squat analysis and functional movement screen we will develop a program to add flexibility and strength in the areas needed to move your body back into balance. You’ll also get a great workout while doing it!',
			],
			[
				'title' => 'Sports Specific Training',
				'icon' => 'fa-gears',
				'color' => '#00AAA9',
				'content' => 'Our team is composed of  certified strength and conditioning coaches with the knowledge and know-how to design professional-level sports conditioning or other types of programs to meet your unique goals. We are also the only facility in Boston to have Keiser Strength equipment. If you’re not using Keiser equipment, you’re not training for true power!',
			],
		];

		foreach ($services_content as $sc) {
			$content .= <<<E
				<article itemtype='http://schema.org/Article' class='post highlight type-highlight status-publish has-post-thumbnail hentry post highlight-post col4-1 animated'>
					<figure class='post-image '>
						<div class='chart' data-percent='84' data-color='{$sc['color']}'>
							<div class='fa-background' style='background-color:{$sc['color']};'></div>
							<i class='fa {$sc['icon']}' style='color: #fff'></i>
							<canvas width='175' height='175'></canvas>
						</div><!-- /.chart -->
					</figure>
					<div class='post-content'>
						<h4 class='post-title entry-title' itemprop='name'>{$sc['title']}</h4>
						<div class='entry-content' itemprop='articleBody'>
							<p>{$sc['content']}</p>
							<div id='themify_builder_content-168' data-postid='168' class='themify_builder_content themify_builder_content-168 themify_builder themify_builder_front ui-sortable'>
							</div><!-- /themify_builder_content -->
						</div><!-- /.entry-content -->
						<span class='edit-button'>[<a class='post-edit-link' href='http://wp.test/wp-admin/post.php?post=168&amp;action=edit'>Edit</a>]</span></div>
				</article><!-- / .post -->
E;
		}

		$content .= "</div>";

		return $content;
	}

    function get_content_contact() {
        return $content = <<<E
[otonomic_contact]
E;
    }

    function get_content_testimonials() {
        return $content = <<<E
[otonomic_testimonials]
E;
    }

    function get_content_social() {
        return $content = <<<E
[otonomic_dpSocialTimeline before ="<h3 style="width: 100%;" >Social Timeline</h3>"]
[otonomic_social network="facebook"]
E;
    }

    protected function parse_date($date) {
        if($date){
            $date_timestamp = strtotime($date);
            return date('Y-m-d H:i:s', $date_timestamp);

        } else {
            return date("Y-m-d H:i:s", 0);
        }
    }

    function set_social_profiles($user){
        $base_url = "http://otonomic.com/sandbox/developers/omri/social_accounts_finder.php";
        $url = $base_url."?q=".$user;
        $json_data = file_get_contents($url);
        $data = json_decode($json_data,true);
        if(is_array($data) && count($data['results']) > 0){
            $user_profiles = array(
                'facebook',
                'twitter',
                'youtube',
                'vimeo',
                'linkedin',
                'yelp',
                'instagram',
                'pinterest',
                'flickr'
            );
            $option_name = 'social_media_';
            foreach ($data['results'] as $profile_key => $username) {
                foreach($user_profiles as $profile){
                    if(strpos($profile_key, $profile)){
                        update_option($option_name.$profile, $profile_key.$username);
                        break;
                    }
                }
            }
        }
    }


    function set_menu($menu_location='', $secondary_location = "") {
        if(!$menu_location) {
            $menu_location = $this->menu_location['primary'];
        }

	    $store_menu_id = 0;


        $menu_name = 'Otonomic Menu';

        if(!is_array($menu_location)) {
		    $menu_location = array($menu_location);
	    }

        // Does the menu exist already?
        $menu = wp_get_nav_menu_object( $menu_name );

        // If it doesn't exist, let's create it.
        if( !$menu) {
            $menu_id = wp_create_nav_menu($menu_name);
        } else {
            $menu_id = wp_update_nav_menu_item($menu->term_id);
        }

	    $menu_items = $this->get_menu_items();

        foreach ($menu_items as $menu_item) {
	        $db_menu_id = wp_update_nav_menu_item($menu_id, 0, $menu_item);
	        if($menu_item['menu-item-title'] == 'Store')
	        {
		        $store_menu_id = $db_menu_id;
	        }
        }
	    if($store_menu_id>0)
	    {
		    $OtonomicStore = OtonomicStore::get_instance();
		    $OtonomicStore->add_store_pages_to_menu( $store_menu_id, $menu_name);
	    }

        $locations = get_theme_mod('nav_menu_locations');
	    foreach($menu_location as $location) {
		    $locations[$location] = $menu_id;
	    }

        if(!empty($secondary_location)){
            $locations[$secondary_location] = $menu_id;
        }

	    $nav_options = get_option('nav_menu_options');
	    $nav_options['auto_add'][] = $menu_id;
	    update_option('nav_menu_options', $nav_options);

        set_theme_mod('nav_menu_locations', $locations);
        return $menu_id;
    }

	function get_menu_items()
	{
		$menu_items = array();

		$menu_items[] = array(
			'menu-item-title' => 'Home',
			'menu-item-url' => home_url( '/' ),
			/*'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',*/
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'About',
			'menu-item-object-id' => get_page_by_path('about')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Photos',
			'menu-item-object-id' => get_page_by_path('portfolio')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Updates',
			'menu-item-object-id' => get_page_by_path('blog')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Testimonials',
			'menu-item-object-id' => get_page_by_path('testimonials')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		/* check if we need to add store */
		if(in_array('store',$this->modules))
		{
			/* Lets create store menu */
			$menu_items[] = array(
				'menu-item-title' => 'Store',
				'menu-item-object-id' => get_page_by_path('shop')->ID,
				'menu-item-object' => 'page',
				'menu-item-type' => 'post_type',
				'menu-item-status' => 'publish'
			);
		}
		$menu_items[] = array(
			'menu-item-title' => 'Social',
			'menu-item-object-id' => get_page_by_path('social')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Contact',
			'menu-item-object-id' => get_page_by_path('contact')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);

		return $menu_items;
	}


    function set_page_settings($page, $data) {
        $page = get_page_by_path( $page, OBJECT, ['page','section']);

        foreach($data as $key => $value) {
            add_post_meta($page->ID, $key, $value);
        }

        return $page->ID;
    }



    function get_placeholder_testimonials() {
        return json_decode( json_encode(
        [
            [
                'rank' => '5',
                'user_name' => 'Jane Doe',
                'user_picture' => get_site_url().'/wp-content/mu-plugins/otonomic-first-session/assets/img/girl-profile2.png',
                'user_link' => '#',
                'text' => 'Here you can add testimonials from your clients',
            ]
        ]));
    }
}



function otonomic_insert_post($post) {
    $table = 'wp_'.get_current_blog_id().'_posts';
    global $wpdb;

	/* create unique post slug */
	$post_name = $post['post_name'];
	if(empty($post_name))
	{
		$post_name = sanitize_title_with_dashes( $post['post_title'] );
	}
	if(empty($post_name))
	{
		$post_name = 'temp';
	}
	$temp_post_name = $post_name;
	$i=0;
	do
	{
		if($i>0)
		{
			$temp_post_name = $post_name.'-'.$i;
		}
		$sql = "select * from {$table} where post_name = '{$temp_post_name}'";
		$data = $wpdb->get_results($sql);
		$i++;
	}while(count($data)>0);

	$post['post_name'] = $temp_post_name;

	//$post_name = $temp_post_name;

	if(!isset($post['post_author']))
		$post['post_author'] = get_current_user_id();

	if($post['post_author']<=0)
		$post['post_author'] = 1;

    if( $wpdb->insert( $table, $post))
    {
	    $post_id = $wpdb->insert_id;


        return $post_id;
    } else {
        return false;
    }
}

function otonomic_insert_postmeta( $post_id, $post_meta) {
    $sql_arr  =[];
	$default_meta = array(
		'_yoast_wpseo_meta-robots-noindex'  =>  2,
		'_yoast_wpseo_sitemap-include'  =>  'always'
	);
	$post_meta = array_merge($post_meta, $default_meta);

    foreach($post_meta as $key=>$value) {
        if($value === false) { $value = 0; }
        if(is_array($value)) {
            $value = serialize($value);
        }
        $sql_arr[] = "( {$post_id}, '{$key}', '{$value}' )";
    }

    $table = 'wp_'.get_current_blog_id().'_postmeta';
    $sql = "INSERT INTO {$table}
        (`post_id`, `meta_key`, `meta_value`)
        VALUES ".implode(', ', $sql_arr). ";";

    global $wpdb;
    return $wpdb->query( $sql);
}
