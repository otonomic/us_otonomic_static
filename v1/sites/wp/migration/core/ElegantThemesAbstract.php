<?php
include_once ABSPATH . "migration/core/ThemeAbstract.php";

abstract class ElegantThemesAbstract extends ThemeAbstract {
	public $featured_post_setting_meta_name;

    public $menu_location = [
        'primary' => 'primary-menu',
        'footer' => ''
    ];


    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);

        if(empty($this->theme))
            $this->theme = strtolower(get_called_class());

        $this->featured_post_setting_meta_name = '_et_'.strtolower($this->theme).'_settings';
    }

    public function save_content(){
        kses_remove_filters();

        $this->save_posts();
	    $this->save_notes();
        // NOTE: save_testimonials() is specific to the template - should be part of the template change code
        $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        $this->save_services();
        $this->save_albums();
        $this->save_videos();
        kses_init_filters();
    }

    function set_full_width_template($pageId){
        add_post_meta($pageId, '_wp_page_template', 'page-full.php');
    }

    function get_full_width_settings_array() {
        return [
            'et_ptemplate_settings' => [
                'et_fullwidthpage' => 1,
            ],
            '_wp_page_template' => 'page-full.php'
        ];
    }

    public function setup_SEO()
    {
        $OtonomicSEO  = OtonomicSEO::get_instance();

        $home_page_id = $this->get_homepage_id();
        $OtonomicSEO->add_seo_tags( $home_page_id );
	    $OtonomicSEO->add_home_seo_tags( );

        $shop_page_id = $this->get_page_id_by_name('Shop');
        $OtonomicSEO->add_seo_tags( $shop_page_id );

        $cart_page_id = $this->get_page_id_by_name('Cart');
        $OtonomicSEO->add_seo_tags( $cart_page_id );

        $checkout_page_id = $this->get_page_id_by_name('Checkout');
        $OtonomicSEO->add_seo_tags( $checkout_page_id );

        $my_account_page_id = $this->get_page_id_by_name('My Account');
        $OtonomicSEO->add_seo_tags( $my_account_page_id );

    }

    function update_homepage() {
        // TODO: Write code that adds a page that would be the homepage
    }

    function add_module_slider()
    {
        $this->set_main_site_slider();
    }

    function add_module_about()
    {
        $this->add_section_about();
    }

    function add_module_services()
    {
        $this->add_section_services();
    }

    function add_module_store() {
        $this->add_section_store();
    }

    function add_module_blog()
    {
//        $this->add_section_blog();
        // $this->add_page_blog();
    }

    function add_module_portfolio()
    {
        $this->add_section_portfolio();
        // $this->add_page_portfolio();
    }

    function add_module_testimonials()
    {
        $this->add_section_testimonials();
        // $this->add_page_testimonials();
    }

    function add_module_social()
    {
        $this->add_section_social();
    }

    function add_module_contact()
    {
        $this->add_section_contact();
    }







    protected function add_post($fbpost, $download_image=false){
        // We do not insert posts of type status
        if($fbpost->type == 'Status') { return; }
		$post_title = strlen($fbpost->message)>0?$fbpost->message:$fbpost->name;
        $fbpost->message = make_clickable($fbpost->message);
        $my_post = array(
            'post_title'    => trim_closest_word($post_title, 50, $append_ellipsis = true),
//             'post_name'   => wp_unique_post_slug( sanitize_title( trim_closest_word($fbpost->message, 50, false) ) ),
            'post_name'   => sanitize_title( trim_closest_word($fbpost->message, 50, false) ),
            'post_content'  => $fbpost->message,
            'post_status'   => 'publish',
            'post_type' 	=> 'post',
//            'filter'        => true
        );

        if($fbpost->created_time){
            $post_date = $this->parse_date($fbpost->created_time);
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        }

        $post_id = otonomic_insert_post( $my_post );

        // TODO: Add taxonomy - video/link/
        // TODO: Available categories - video, link, image, review
        $term_taxonomy_ids = wp_set_object_terms( $post_id , array(2,3), 'category', false );
//        $setting_cats = wp_set_post_categories($post_id,1,FALSE);
        // adding the image to the post
        $image_id = $this->add_image($fbpost, $post_id, $download_image);

        // add meta tags for this post
        $post_meta = [
            '_thumbnail_id'         => $image_id,
            'post_image'            => $fbpost->picture,
            '_post_image_attach_id' => $image_id
        ];

        $result = otonomic_insert_postmeta($post_id, $post_meta);
    }

    protected function add_testimonial($testimonial, $categories = array(33)){
        
        if(empty($testimonial->rank)) {
            $testimonial->rank = '';
        }

        if( empty($testimonial->message) || trim($testimonial->message) == '') {
            $testimonial->message = trim($testimonial->text);
            if (!$testimonial->message) {
                return;
            }
        }

        $testimonial->message = make_clickable($testimonial->message);
        $post_date = $this->parse_date($testimonial->date);

        if($testimonial->rank >= 4 || (!empty($testimonial->sentiment) && ($testimonial->sentiment)>70)) {
            $status = 'publish';
        } else {
            $status = 'draft';
        }

        $my_post = array(
            'post_title'    => $testimonial->user_name,
            'post_content'  => $testimonial->text,
            'post_status'   => $status,
            'post_type' 	  => 'testimonial',
        );

        if($testimonial->source == 'facebook_reviews'){
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        } else {
            $current_date = $this->parse_date(false);
            $my_post['post_date'] = $current_date;
            $my_post['post_date_gmt'] = $current_date;
            $my_post['post_modified'] = $current_date;
            $my_post['post_modified_gmt'] = $current_date;
        }

        // $post_id = wp_insert_post( $my_post, true );
        $post_id = otonomic_insert_post( $my_post);
//        if(!get_option('feather_quote_one') || get_option('feather_quote_one') == ""){
//            if($testimonial->text != ""){
//                update_option('feather_quote_one', $testimonial->text);
//            }
//        }
        // TODO: Add category + terms
	    $facebook_term = term_exists( 'facebook-testimonial', 'testimonial-category' );
	    if ($facebook_term === null)
	    {
		    /* Create the new term */
		    wp_insert_term(
			    'facebook-testimonial', // the term
			    'testimonial-category', // the taxonomy
			    array(
				    'description'=> 'Facebook testimonial',
				    'slug' => 'facebook-testimonial'
			    )
		    );

	    }
	    $user_term = term_exists( 'user-testimonial', 'testimonial-category' );
	    if ($user_term === null)
	    {
		    /* Create the new term */
		    wp_insert_term(
			    'user-testimonial', // the term
			    'testimonial-category', // the taxonomy
			    array(
				    'description'=> 'User testimonial',
				    'slug' => 'user-testimonial'
			    )
		    );

	    }
	    $facebook_term = term_exists( 'facebook-testimonial', 'testimonial-category' );

	    $categories[] =  intval($facebook_term['term_id']);

        $term_taxonomy_ids = wp_set_object_terms( $post_id , $categories, 'testimonial-category', false );

        if(isset($testimonial->from) && strpos($testimonial->from->picture, 'width=') === false){
            $testimonial->from->picture = $testimonial->from->picture . "?width=300&height=300";
        }
        // adding the image to the post

        $testimonial_image_post = [
            'id' => $testimonial->user_social_id,
            'picture' => $testimonial->user_picture,
            'source' => $testimonial->user_picture
        ];
        $testimonial_image_post = (object)$testimonial_image_post;

        $image_id = $this->add_image($testimonial_image_post, $post_id);
        // add meta tags for this post

        $post_meta = [
            'reviewer_rating'       => $testimonial->rank,
            '_thumbnail_id'         => $image_id,
            'post_image'            => $testimonial->user_picture,
            '_post_image_attach_id' => $image_id,
            'reviewer_profile' => $testimonial->user_link
        ];

        otonomic_insert_postmeta($post_id, $post_meta);
    }


    protected function add_album($fbalbum, $download_image=false, $album=0) {
        if(!isset($fbalbum->photos->data)) { return; }

        $album_photos = $fbalbum->photos->data;

        if(empty($album_photos)) { return; }

        $my_album = array(
            'post_title'     => $fbalbum->name,
            'post_content'   => $fbalbum->description,
            'post_status'    => 'publish',
            'comment_status' => 'closed',
            'ping_status' 	 => 'closed',
            'post_type' 	 => 'portfolio',
            'guid'           => $fbalbum->id,
//            'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
        );

        $post_date = $this->parse_date($fbalbum->created_time);
        $my_album['post_date'] = $post_date;
        $my_album['post_date_gmt'] = $post_date;
        $my_album['post_modified'] = $post_date;
        $my_album['post_modified_gmt'] = $post_date;

        //$post_id = wp_insert_post( $my_album );
        $post_id = otonomic_insert_post($my_album);
//        $this->clone_album_posts($my_album);
//        wp_set_post_categories($post_id, 9);
        // TODO: Change category from number
        // TODO: Add terms+category
        // tax_input in the input to wp_insert_post() should work instead
        // $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

        // adding the image to the post
        $image_ids = array();

        foreach((array)$album_photos as $key=>$picture)
        {
	        try {
	            $image_id = $this->add_image($picture, $post_id, $download_image);
	            $image_ids[] = $image_id;

            } catch(Exception $e) {
	        }
        }

        // add meta tags for this post
        $album_cover_photo_localid = $image_ids[0];
        $album_cover_photo_url = $album_photos[0]->source;
        $gallery_shortcode = '[gallery ids="'. implode(",", $image_ids) .'" link="file"]';

        $post_meta = [
            '_thumbnail_id'         => $album_cover_photo_localid,
            '_post_image_attach_id' => $album_cover_photo_localid,
            'post_image'            => $album_cover_photo_url,
            'gallery_shortcode'     => $gallery_shortcode
        ];

        otonomic_insert_postmeta($post_id, $post_meta);

        $this->clone_album_posts($my_album,$image_ids);

        return $post_id;
    }
    
    protected function clone_album_posts($album_post, $images){
        if(strtolower($album_post['post_title']) == 'cover photos'){
            $album_post['post_type'] = 'post';
            $album_post['post_title'] = get_bloginfo('name');
	        $album_post['post_name'] = sanitize_title_with_dashes( get_bloginfo('name') );

            // $slider_images = $this->get_images_for_slider();

            foreach($images as $image){
                $postID = otonomic_insert_post($album_post);
                wp_set_post_categories($postID, 11, FALSE);
                $post_meta = [
                    '_thumbnail_id'         => $image,
                    '_post_image_attach_id' => $image
                ];
                otonomic_insert_postmeta($postID, $post_meta);

                $ft_post_meta = array(
                    "et_is_featured" => 1,
                    "et_fs_variation" => 5,
                    "et_fs_video" => "",
                    "et_fs_video_embed" => "",
                    "et_fs_title" => "",
                    "et_fs_description" => "",
                    "et_fs_link" => ""
                );
                add_post_meta($postID, $this->featured_post_setting_meta_name, $ft_post_meta);
            }
        }
    }

    protected function add_team($testimonial, $categories = array(33), $post_type='team'){
        $message = $testimonial->message;
        $from = $testimonial->from->name;
        $from_picture = $testimonial->from->picture;

        if((!$message || trim($message)) == ''){
            if((!$testimonial->text || trim($testimonial->text)) == ''){
                return;
            }
        }

        if(!$message){
            $message = $testimonial->text;
        }
        if(!$from){
            $from = $testimonial->user_name;
        }

        if(!$from_picture || strpos($from_picture, 'http://') === false){
            $from_picture = $testimonial->user_picture;
        }

        $my_post = array(
            'post_title'    => $from,
            'post_content'  => $message,
            'post_status'   => 'publish',
            'post_type' 	  => $post_type
        );

        $post_date = $this->parse_date($testimonial->date);

        if($testimonial->source == 'facebook_reviews'){
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        } else {
            $my_post['post_date'] = $this->parse_date(false);
            $my_post['post_date_gmt'] = $this->parse_date(false);
            $my_post['post_modified'] = $this->parse_date(false);
            $my_post['post_modified_gmt'] = $this->parse_date(false);
        }

        //$post_id = wp_insert_post( $my_post );
        $post_id = otonomic_insert_post($my_post);

        $term_taxonomy_ids = wp_set_object_terms( $post_id , $categories, 'testimonial-category', false );


        if(strpos($from_picture, 'width=') === false){
            $from_picture = $from_picture . "?width=300&height=300";
        }
        // adding the image to the post
        $image_id = $this->add_image($testimonial, $post_id);

        $post_meta = [
            '_thumbnail_id'         => $image_id,
            'post_image'            => $from_picture,
            '_post_image_attach_id' => $image_id
        ];

        otonomic_insert_postmeta($post_id, $post_meta);
    }


	protected function add_videos_album($videos) {
		if(empty($videos)) { return; }

		$my_album = array(
			'post_title'     => 'videos',
			'post_content'   => '',
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status' 	 => 'closed',
			'post_type' 	 => 'portfolio',
			'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
		);

        $video_album_id = otonomic_insert_post($my_album);

		// TODO: Change category from number
		// TODO: Add terms+category
		// tax_input in the input to wp_insert_post() should work instead
		// $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

		// adding the image to the post
		$video_ids = array();

		foreach($videos as $key=>$video) {
			try
			{
                $video_id = $this->add_video($video, $video_album_id);
                $video_ids[] = $video_id;
            }
			catch(Exception $e)
			{
				/* Silently ignore the error */
			}
		}

		// add meta tags for this post
		$album_cover_photo_localid = $video_ids[0];
		$album_cover_photo_url = $video_ids[0]->format[1]->picture;
		$gallery_shortcode = '[gallery ids="'. implode(",", $video_ids) .'" link="file"]';

        $post_meta = [
            '_thumbnail_id'         => $album_cover_photo_localid,
            '_post_image_attach_id' => $album_cover_photo_localid,
            'post_image'            => $album_cover_photo_url,
            'gallery_shortcode'     => $gallery_shortcode
        ];

        otonomic_insert_postmeta($video_album_id, $post_meta);

		return $video_album_id;
	}

	protected function add_video($video, $video_album_id)
	{
		$otonomic_url = otonomic_url::get_instance();
		$main_image = $otonomic_url->ot_get_source_url($video->format[1]->picture);

		$my_post = array(
			'post_name'     => $video->description,
			'post_content'   => $video->embed_html,
			'post_parent'    => $video_album_id,
			'post_status'    => 'inherit',
			'post_type' 	 => 'attachment',
			'post_mime_type' => 'image/jpeg',
			'guid'           => $video->source,
		);

		//$video_id = wp_insert_post( $my_post );
        $video_id = otonomic_insert_post($my_post);

        $post_meta = [
            'thumb'             => $main_image,
            '_wp_attached_file' => $main_image,
            'external_link'     => $main_image
        ];

        otonomic_insert_postmeta($video_id, $post_meta);

		return $video_id;
	}

    protected function save_videos() {
        if(!is_array($this->page->videos) || count($this->page->videos) == 0)
            return;

	    try {
	        $this->wpdb_disable_autocommit();
            $this->add_videos_album($this->page->videos);
            $this->wpdb_enable_autocommit();

        } catch(Exception $e) {
	    }
    }

    protected function save_testimonials( $testimonial_categories, $team_categories, $team_post_type='team' ){
        $testimonials = $this->page->testimonials;

        if(!$testimonials) {
            $testimonials = $this->get_placeholder_testimonials();
        }

        if($testimonials && count($testimonials) > 0) {
            foreach($testimonials as $key=>$testimonial) {
	            try {
                    $this->add_testimonial($testimonial, $testimonial_categories);
                    // $this->add_team($testimonial, $team_categories, $team_post_type);

                } catch(Exception $e) {
	            }
            }

        } else {
            $post_query = new WP_Query( array( 'post_type' => 'section', 'name' => 'team'));
            Section::hide($this->blog_id, $post_query->post->ID);
        }
    }


    protected function save_services(){
        $records = $this->page->services;
        if($records && count($records) > 0) {
            foreach($records as $key=>$record) {
                try {
                    $this->add_service($records);

                } catch(Exception $e) {
                }
            }

        } else {
            $post_query = new WP_Query( array( 'post_type' => 'section', 'name' => 'services'));
            Section::hide($this->blog_id, $post_query->post->ID);
        }
    }


    protected function set_text_widgets_footer(){
        $text_widget = array(
            array(
                'title' => 'About',
                'text' => '[otonomic_option option="blogdescription"]',
                'filter' => ""
            ),
            array(
                'title' => 'Social',
                'text' => '[otonomic_social]',
                'filter' => ""
            ),
            array(
                'title' => 'Contact',
                'text' => '[otonomic_contact]',
                'filter' => ""
            ),
            array(
                'title' =>'Opening Hours',
                'text' => '[otonomic_option option="opening_hours" before="" after=""]',
                'filter' => ""
            ),
            array(
                'title' => 'Testimonial',
                'text' => '[otonomic_testimonials limit="1"]',
                'filter' => '',
            )
        );
        $text_widget['_multiwidget'] = 1;
        update_option('widget_text', $text_widget);
    }

    protected function set_text_widgets_sidebar(){
        $text_widget = array(
            array(
                'title' => 'About',
                'text' => '[otonomic_option option="blogdescription"]',
                'filter' => ""
            ),
            array(
                'title' =>'Opening Hours',
                'text' => '[otonomic_option option="opening_hours" before="" after=""]',
                'filter' => ""
            )
        );
        $text_widget['_multiwidget'] = 1;

        $set_widgets = array(
            'wp_inactive_widgets' => array('archives-2','meta-2','search-2','categories-2','recent-posts-2'),
            'sidebar-1' => $text_widget,
            'array_version' => 3
        );
        update_option('sidebars_widgets',$set_widgets);
    }

    public function add_section_blog() {
        return $page_id = $this->insert_section_blog($content = "");
    }

    public function add_section_portfolio()
    {
        return $page_id = $this->insert_section_portfolio($content = "");
    }




    function add_section_store() {
        global $wpdb;

        $content = '[recent_products per_page="12" columns="3"][woocommerce_cart]';

        $data_post = array(
            'post_title'   => 'Store',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 70,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        //$wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");

        $OtonomicStore = OtonomicStore::get_instance();
        $OtonomicStore->add_sample_product();
        $OtonomicStore->show_store_pages(false);

        $OtonomicStore->add_store_pages_to_menu( $parent = '2343');

        return $page_id;
    }








    public function add_section_contact() {
        $data_post = array(
            'post_title'   => 'Contact',
            'post_content' => $this->get_content_contact(),
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 75,
        );
        $data_postmeta = array(
        );

        $page_id = ot_insert_post( $data_post , $data_postmeta);
        add_post_meta($page_id, '_wp_page_template', 'page-full.php');

        return $page_id;
    }


    public function add_section_testimonials() {
        $data_post = array(
            'post_title'   => 'Testimonials',
            'post_content' => $this->get_content_testimonials(),
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 60,
        );
        $data_postmeta = array();
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        add_post_meta($page_id, '_wp_page_template', 'page-full.php');

        return $page_id;
    }


    public function add_section_social() {
        $data_post = array(
            'post_title'   => 'Get Social',
            'post_content' => $this->get_content_social(),
            'post_name'    => 'social',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 50,
        );
        $data_postmeta = array();
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        return $page_id;
    }



    function add_section_slider() {
    }



    public function add_section_about() {
        $content = $this->get_content_about();

        // Save content
        $data_post = array(
            'post_title'   => 'About',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 20
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        return $page_id;
    }



    public function add_section_services() {
        $content = $this->get_content_services();

        // Save content
        $data_post = array(
            'post_title'   => 'Services',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 15,
        );
        $data_postmeta = array();
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        return $page_id;
    }

    function insert_section_portfolio($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
                'post_title'   => 'Photos',
                'post_content' => $content,
                'post_name'    => 'portfolio',
                'post_status'  => 'publish',
                'post_type'    => 'page',
                'menu_order'   => 20,
                'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
            ) + $data_post;

        $data_postmeta = array(
            ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);
        add_post_meta($page_id, '_wp_page_template', 'page-template-portfolio.php');
        $et_ptemplate_settings = array(
            'et_fullwidthpage' => 1,
            'et_ptemplate_gallerycats' => array(3),
            'et_ptemplate_gallery_perpage' => 12,
            'et_ptemplate_showtitle' => 0,
            'et_ptemplate_showdesc' => 1,
            'et_ptemplate_detect_portrait' => 1,
            'et_ptemplate_imagesize' => 3
        );
        add_post_meta($page_id, 'et_ptemplate_settings', $et_ptemplate_settings);

        return $page_id;
    }


    function insert_section_blog($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
                'post_title'   => 'Updates',
                'post_content' => $content,
                'post_name'    => 'blog',
                'post_status'  => 'publish',
                'post_type'    => 'page',
                'menu_order'   => 40,
            ) + $data_post;

        $data_postmeta = array(
                '_wp_page_template' => 'page-blog.php'
            ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);
//        add_post_meta($page_id, '_wp_page_template', 'page-blog.php');
        $et_ptemplate_settings = array(
            'et_fullwidthpage' => 1,
            'et_ptemplate_blogstyle' => 0,
            'et_ptemplate_showthumb' => 0,
            'et_ptemplate_gallerycats' => array(2),
            'et_ptemplate_blog_perpage' => 6
        );
        add_post_meta($page_id, 'et_ptemplate_settings', $et_ptemplate_settings);

        return $page_id;
    }




    protected function get_homepage_id() {
        return $this->get_page_id(array('name'=>'home'));
    }

    public function set_elegantthemes_options()
    {
        try {
            include_once('config/elegantthemes_options.php');
            $data = get_elegantthemes_options();
            if ( get_option( 'elegantthemes_data' ) !== false )
            {
                update_option( 'elegantthemes_data' , $data );
            }
            else
            {
                add_option( 'elegantthemes_data', $data, null, 'yes' );
            }

        } catch(Exception $e) {
        }

        switch_theme($this->theme,$this->theme);
    }
}
