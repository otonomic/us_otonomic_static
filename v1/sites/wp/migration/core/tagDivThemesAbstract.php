<?php
include_once ABSPATH . "migration/core/BlankThemeAbstract.php";

abstract class tagDivThemesAbstract extends BlankThemeAbstract {
	public $featured_post_setting_meta_name;

    public $menu_location = [
        'primary' => 'primary-menu',
        'footer' => ''
    ];

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);

        if(empty($this->theme))
            $this->theme = strtolower(get_called_class());

        $this->featured_post_setting_meta_name = '_et_'.strtolower($this->theme).'_settings';
    }

    public function save_content(){
        kses_remove_filters();
        //$this->save_posts();
	    //$this->save_notes();
        // NOTE: save_testimonials() is specific to the template - should be part of the template change code
        //$this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        //$this->save_services();
        //$this->save_albums();
        //$this->save_videos();
        kses_init_filters();
    }

    public function setup_SEO()
    {
        $OtonomicSEO  = OtonomicSEO::get_instance();

        $home_page_id = $this->get_homepage_id();
        $OtonomicSEO->add_seo_tags( $home_page_id );
	    $OtonomicSEO->add_home_seo_tags( );

        $shop_page_id = $this->get_page_id_by_name('Shop');
        $OtonomicSEO->add_seo_tags( $shop_page_id );

        $cart_page_id = $this->get_page_id_by_name('Cart');
        $OtonomicSEO->add_seo_tags( $cart_page_id );

        $checkout_page_id = $this->get_page_id_by_name('Checkout');
        $OtonomicSEO->add_seo_tags( $checkout_page_id );

        $my_account_page_id = $this->get_page_id_by_name('My Account');
        $OtonomicSEO->add_seo_tags( $my_account_page_id );

    }

    function update_homepage() {
        // TODO: Write code that adds a page that would be the homepage
    }

    function add_module_slider()
    {
        $this->set_main_site_slider();
    }

    function add_module_about()
    {
        $this->add_section_about();
    }

    function add_module_services()
    {
        $this->add_section_services();
    }

    function add_module_store() {
        $this->add_section_store();
    }

    function add_module_blog()
    {
//        $this->add_section_blog();
        // $this->add_page_blog();
    }

    function add_module_portfolio()
    {
        $this->add_section_portfolio();
        // $this->add_page_portfolio();
    }

    function add_module_testimonials()
    {
        $this->add_section_testimonials();
        // $this->add_page_testimonials();
    }

    function add_module_social()
    {
        $this->add_section_social();
    }

    function add_module_contact()
    {
        $this->add_section_contact();
    }

    function add_section_store() {
        global $wpdb;

        $content = '[recent_products per_page="12" columns="3"][woocommerce_cart]';

        $data_post = array(
            'post_title'   => 'Store',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 70,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        //$wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");

        $OtonomicStore = OtonomicStore::get_instance();
        $OtonomicStore->add_sample_product();
        $OtonomicStore->show_store_pages(false);

        $OtonomicStore->add_store_pages_to_menu( $parent = '2343');

        return $page_id;
    }


    function add_section_slider() {
    }



    public function add_section_about() {
        $content = $this->get_content_about();

        // Save content
        $data_post = array(
            'post_title'   => 'About',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 20
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        return $page_id;
    }

    protected function get_homepage_id() {
        return $this->get_page_id(array('name'=>'home'));
    }
}
