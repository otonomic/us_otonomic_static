<?php
include_once ABSPATH . "migration/core/ThemeAbstract.php";

/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/17/14
 * Time: 1:15 PM
 */

abstract class ThemifyAbstract extends ThemeAbstract {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = strtolower(__CLASS__);
    }

    protected function add_post($fbpost, $download_image=false){
        // We do not insert posts of type status
        if($fbpost->type == 'Status') { return; }

        $fbpost->message = make_clickable($fbpost->message);
        $my_post = array(
            'post_title'    => trim_closest_word($fbpost->message, 50, $append_ellipsis = true),
            'post_content'  => $fbpost->message,
            'post_status'   => 'publish',
            'post_type' 	=> 'post',
//            'filter'        => true
        );

        if($fbpost->created_time){
            $post_date = $this->parse_date($fbpost->created_time);
            // $post_date = date("Y-m-d H:i:s", strtotime($fbpost->created_time));

            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        }

        // $post_id = wp_insert_post( $my_post );
        $post_id = otonomic_insert_post( $my_post );

        // TODO: Add taxonomy - video/link/
        // TODO: Available categories - video, link, image, review
        $term_taxonomy_ids = wp_set_object_terms( $post_id , array(2,3), 'category', false );

        // adding the image to the post
        $image_id = $this->add_image($fbpost, $post_id, $download_image);

        /*
        // add meta tags for this post
        add_post_meta( $post_id, '_thumbnail_id', $image_id );
        add_post_meta( $post_id, 'post_image', $fbpost->picture );
        add_post_meta( $post_id, '_post_image_attach_id', $image_id );
        */

        // add meta tags for this post
        $post_meta = [
            '_thumbnail_id'         => $image_id,
            'post_image'            => $fbpost->picture,
            '_post_image_attach_id' => $image_id
        ];

        $result = otonomic_insert_postmeta($post_id, $post_meta);
    }

    protected function add_testimonial($testimonial, $categories = array(33)){

        if((!$testimonial->message || trim($testimonial->message)) == ''){
            if((!$testimonial->text || trim($testimonial->text)) == ''){
                return;
            }

            $testimonial->message = $testimonial->text;
        }

        $testimonial->message = make_clickable($testimonial->message);
        $post_date = $this->parse_date($testimonial->date);

        $my_post = array(
            'post_title'    => $testimonial->user_name,
            'post_content'  => $testimonial->text,
            'post_status'   => 'publish',
            'post_type' 	  => 'testimonial',
            //post_date
//            'filter'        => true
        );

        if($testimonial->source == 'facebook_reviews'){
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        } else {
            $current_date = $this->parse_date(false);
            $my_post['post_date'] = $current_date;
            $my_post['post_date_gmt'] = $current_date;
            $my_post['post_modified'] = $current_date;
            $my_post['post_modified_gmt'] = $current_date;
        }

        // $post_id = wp_insert_post( $my_post, true );
        $post_id = otonomic_insert_post( $my_post);

        // TODO: Add category + terms
	    $facebook_term = term_exists( 'facebook-testimonial', 'testimonial-category' );
	    if ($facebook_term === null)
	    {
		    /* Create the new term */
		    wp_insert_term(
			    'facebook-testimonial', // the term
			    'testimonial-category', // the taxonomy
			    array(
				    'description'=> 'Facebook testimonial',
				    'slug' => 'facebook-testimonial'
			    )
		    );

	    }
	    $user_term = term_exists( 'user-testimonial', 'testimonial-category' );
	    if ($user_term === null)
	    {
		    /* Create the new term */
		    wp_insert_term(
			    'user-testimonial', // the term
			    'testimonial-category', // the taxonomy
			    array(
				    'description'=> 'User testimonial',
				    'slug' => 'user-testimonial'
			    )
		    );

	    }
	    $facebook_term = term_exists( 'facebook-testimonial', 'testimonial-category' );

	    $categories[] =  intval($facebook_term['term_id']);

        $term_taxonomy_ids = wp_set_object_terms( $post_id , $categories, 'testimonial-category', false );

        if(isset($testimonial->from) && strpos($testimonial->from->picture, 'width=') === false){
            $testimonial->from->picture = $testimonial->from->picture . "?width=300&height=300";
        }
        // adding the image to the post
	    $testimonial_image_post = [
		    'id' => $testimonial->user_name,
		    'picture' => $testimonial->user_picture,
		    'source' => $testimonial->user_picture
	    ];
	    $testimonial_image_post = (object)$testimonial_image_post;
	    
        $image_id = $this->add_image($testimonial_image_post, $post_id);
        // add meta tags for this post

        $post_meta = [
            'reviewer_rating'       => $testimonial->rank,
            '_thumbnail_id'         => $image_id,
            'post_image'            => $testimonial->from->picture,
            '_post_image_attach_id' => $image_id
        ];

        otonomic_insert_postmeta($post_id, $post_meta);
    }


    protected function add_album($fbalbum, $download_image=false, $album=0) {
        $album_photos = $fbalbum->photos->data;

        if(empty($album_photos)) { return; }

        $my_album = array(
            'post_title'     => $fbalbum->name,
            'post_content'   => $fbalbum->description,
            'post_status'    => 'publish',
            'comment_status' => 'closed',
            'ping_status' 	 => 'closed',
            'post_type' 	 => 'portfolio',
            'guid'           => $fbalbum->id,
            'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
        );

        $post_date = $this->parse_date($fbalbum->created_time);
        $my_album['post_date'] = $post_date;
        $my_album['post_date_gmt'] = $post_date;
        $my_album['post_modified'] = $post_date;
        $my_album['post_modified_gmt'] = $post_date;

        //$post_id = wp_insert_post( $my_album );
        $post_id = otonomic_insert_post($my_album);

        // TODO: Change category from number
        // TODO: Add terms+category
        // tax_input in the input to wp_insert_post() should work instead
        // $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

        // adding the image to the post
        $image_ids = array();

        foreach((array)$album_photos as $key=>$picture)
        {
	        try
	        {
	            $image_id = $this->add_image($picture, $post_id, $download_image);
	            $image_ids[] = $image_id;
            }
	        catch(Exception $e)
	        {
		        /* Silently ignore the error */
	        }
        }

        // add meta tags for this post
        $album_cover_photo_localid = $image_ids[0];
        $album_cover_photo_url = $album_photos[0]->source;
        $gallery_shortcode = '[gallery ids="'. implode(",", $image_ids) .'" link="file"]';

        /*
        add_post_meta( $post_id, '_thumbnail_id', $album_cover_photo_localid );
        add_post_meta( $post_id, '_post_image_attach_id', $album_cover_photo_localid );
        add_post_meta( $post_id, 'post_image', $album_cover_photo_url );
        // Add a shortcode of a gallery of all relevant images for the parent post
        add_post_meta( $post_id, 'gallery_shortcode', $gallery_shortcode );
        */

        $post_meta = [
            '_thumbnail_id'         => $album_cover_photo_localid,
            '_post_image_attach_id' => $album_cover_photo_localid,
            'post_image'            => $album_cover_photo_url,
            'gallery_shortcode'     => $gallery_shortcode
        ];

        otonomic_insert_postmeta($post_id, $post_meta);

        return $post_id;
    }

    protected function add_team($testimonial, $categories = array(33), $post_type='team'){
        $message = $testimonial->message;
        $from = $testimonial->from->name;
        $from_picture = $testimonial->from->picture;

        if((!$message || trim($message)) == ''){
            if((!$testimonial->text || trim($testimonial->text)) == ''){
                return;
            }
        }

        if(!$message){
            $message = $testimonial->text;
        }
        if(!$from){
            $from = $testimonial->user_name;
        }

        if(!$from_picture || strpos($from_picture, 'http://') === false){
            $from_picture = $testimonial->user_picture;
        }

        $my_post = array(
            'post_title'    => $from,
            'post_content'  => $message,
            'post_status'   => 'publish',
            'post_type' 	  => $post_type
        );

        $post_date = $this->parse_date($testimonial->date);

        if($testimonial->source == 'facebook_reviews'){
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        } else {
            $my_post['post_date'] = $this->parse_date(false);
            $my_post['post_date_gmt'] = $this->parse_date(false);
            $my_post['post_modified'] = $this->parse_date(false);
            $my_post['post_modified_gmt'] = $this->parse_date(false);
        }

        //$post_id = wp_insert_post( $my_post );
        $post_id = otonomic_insert_post($my_post);

        $term_taxonomy_ids = wp_set_object_terms( $post_id , $categories, 'testimonial-category', false );


        if(strpos($from_picture, 'width=') === false){
            $from_picture = $from_picture . "?width=300&height=300";
        }
        // adding the image to the post
        $image_id = $this->add_image($testimonial, $post_id);

        /*
        // add meta tags for this post
        add_post_meta( $post_id, '_thumbnail_id', $image_id );
        add_post_meta( $post_id, 'post_image', $from_picture );
        add_post_meta( $post_id, '_post_image_attach_id', $image_id );
        */

        $post_meta = [
            '_thumbnail_id'         => $image_id,
            'post_image'            => $from_picture,
            '_post_image_attach_id' => $image_id
        ];

        otonomic_insert_postmeta($post_id, $post_meta);
    }


	protected function add_videos_album($videos) {
		if(empty($videos)) { return; }

		$my_album = array(
			'post_title'     => 'videos',
			'post_content'   => '',
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status' 	 => 'closed',
			'post_type' 	 => 'portfolio',
			'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
		);

		//$video_album_id = wp_insert_post( $my_album );
        $video_album_id = otonomic_insert_post($my_album);

		// TODO: Change category from number
		// TODO: Add terms+category
		// tax_input in the input to wp_insert_post() should work instead
		// $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

		// adding the image to the post
		$video_ids = array();

		foreach($videos as $key=>$video) {
			try
			{
                $video_id = $this->add_video($video, $video_album_id);
                $video_ids[] = $video_id;
            }
			catch(Exception $e)
			{
				/* Silently ignore the error */
			}
		}

		// add meta tags for this post
		$album_cover_photo_localid = $video_ids[0];
		$album_cover_photo_url = $video_ids[0]->format[1]->picture;
		$gallery_shortcode = '[gallery ids="'. implode(",", $video_ids) .'" link="file"]';

        /*
		add_post_meta( $video_album_id, '_thumbnail_id', $album_cover_photo_localid );
		add_post_meta( $video_album_id, '_post_image_attach_id', $album_cover_photo_localid );
		add_post_meta( $video_album_id, 'post_image', $album_cover_photo_url );
		// Add a shortcode of a gallery of all relevant images for the parent post
		add_post_meta( $video_album_id, 'gallery_shortcode', $gallery_shortcode );
        */

        $post_meta = [
            '_thumbnail_id'         => $album_cover_photo_localid,
            '_post_image_attach_id' => $album_cover_photo_localid,
            'post_image'            => $album_cover_photo_url,
            'gallery_shortcode'     => $gallery_shortcode
        ];

        otonomic_insert_postmeta($video_album_id, $post_meta);

		return $video_album_id;
	}

	protected function add_video($video, $video_album_id)
	{
		$otonomic_url = otonomic_url::get_instance();
		$main_image = $otonomic_url->ot_get_source_url($video->format[1]->picture);

		$my_post = array(
			'post_name'     => $video->description,
			'post_content'   => $video->embed_html,
			'post_parent'    => $video_album_id,
			'post_status'    => 'inherit',
			'post_type' 	 => 'attachment',
			'post_mime_type' => 'image/jpeg',
			'guid'           => $video->source,
		);

		//$video_id = wp_insert_post( $my_post );
        $video_id = otonomic_insert_post($my_post);

        /*
		add_post_meta( $video_id, 'thumb', $main_image );
		add_post_meta( $video_id, '_wp_attached_file', $main_image );
		add_post_meta( $video_id, 'external_link', $main_image );
        */

        $post_meta = [
            'thumb'             => $main_image,
            '_wp_attached_file' => $main_image,
            'external_link'     => $main_image
        ];

        otonomic_insert_postmeta($video_id, $post_meta);

		return $video_id;
	}

    protected function save_videos()
    {
        if(!is_array($this->page->videos) || count($this->page->videos) == 0)
            return;

	    try
	    {
	        $this->wpdb_disable_autocommit();
            $this->add_videos_album($this->page->videos);
            $this->wpdb_enable_autocommit();
        }
	    catch(Exception $e)
	    {
		    /* Silently ignore the error */
	    }
    }

    protected function save_testimonials( $testimonial_categories, $team_categories, $team_post_type='team' ){
        $testimonials = $this->page->testimonials;
        if($testimonials && count($testimonials) > 0)
        {
            foreach($testimonials as $key=>$testimonial)
            {
	            try
	            {
                    $this->add_testimonial($testimonial, $testimonial_categories);
                    // $this->add_team($testimonial, $team_categories, $team_post_type);
                }
	            catch(Exception $e)
	            {
		            /* Silently ignore the error */
	            }

            }

        }
        else
        {
            $post_query = new WP_Query( array( 'post_type' => 'section', 'name' => 'team'));
            Section::hide($this->blog_id, $post_query->post->ID);
        }
    }

    protected function update_options() {
        update_option( 'page_on_front', $this->get_homepage_id() );
        update_option( 'show_on_front', 'page' );

        parent::update_options();
    }

    protected function get_homepage_id() {
        return $this->get_page_id(array('name'=>'home'));
    }

    protected function get_page_id_by_name($page_name) {
        return $this->get_page_id(array('name' => $page_name));
    }

    protected function get_page_id(array $options){
        $options['post_type'] = 'page';
        $wpquery = new WP_Query($options);
        return $wpquery->post->ID;
    }


	protected function themify_builder_get_row($row_order=0)
	{
		$row = array();
		$row['row_order'] = $row_order;
		$row['cols'] = array();
		$row['styling'] = array();
		return $row;
	}
	protected function themify_builder_get_module( $mod_name, $mod_settings=array() )
	{
		/* This is our base data :) thanks to demo */
		$_themify_builder_settings = 's:6575:"a:7:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:1:{i:0;a:2:{s:8:"mod_name";s:6:"slider";s:12:"mod_settings";a:17:{s:21:"layout_display_slider";s:6:"slider";s:20:"blog_category_slider";s:7:"|single";s:22:"slider_category_slider";s:15:"slider|multiple";s:25:"portfolio_category_slider";s:7:"|single";s:21:"posts_per_page_slider";s:1:"3";s:14:"display_slider";s:7:"content";s:13:"layout_slider";s:14:"slider-default";s:12:"img_w_slider";s:3:"978";s:12:"img_h_slider";s:3:"550";s:18:"visible_opt_slider";s:1:"1";s:22:"auto_scroll_opt_slider";s:1:"4";s:17:"scroll_opt_slider";s:1:"1";s:16:"speed_opt_slider";s:6:"normal";s:13:"effect_slider";s:6:"scroll";s:11:"wrap_slider";s:3:"yes";s:15:"show_nav_slider";s:3:"yes";s:17:"show_arrow_slider";s:2:"no";}}}}}}i:1;a:2:{s:9:"row_order";s:1:"1";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:55:"<h2 style=\'text-align: center;\'>Welcome to Themify</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:512:"<p style=\'text-align: center;\'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a est lorem. Mauris lacinia mattis sem in aliquet. In quis nibh imperdiet, facilisis nulla vel, pharetra nunc. In pharetra, lorem at aliquam suscipit, erat orci placerat tortor, accumsan convallis elit purus a nisi.</p><p style=\'text-align: center;\'>[button style=\'large rounded blue\' link=\'http://themify.me/themes\']Learn More[/button] [button style=\'large rounded green\' link=\'http://themify.me\']Sign Up[/button]</p>";}}}}}}i:2;a:2:{s:9:"row_order";s:1:"2";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:45:"<h2 style=\'text-align: center;\'>Services</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"highlight";s:12:"mod_settings";a:9:{s:16:"layout_highlight";s:5:"grid3";s:18:"category_highlight";s:17:"services|multiple";s:23:"post_per_page_highlight";s:1:"6";s:15:"order_highlight";s:4:"desc";s:17:"orderby_highlight";s:4:"date";s:17:"display_highlight";s:7:"content";s:19:"img_width_highlight";s:2:"68";s:20:"img_height_highlight";s:2:"68";s:23:"hide_page_nav_highlight";s:3:"yes";}}}}}}i:3;a:2:{s:9:"row_order";s:1:"3";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:46:"<h2 style=\'text-align: center;\'>Portfolio</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"portfolio";s:12:"mod_settings";a:11:{s:16:"layout_portfolio";s:5:"grid4";s:18:"category_portfolio";s:8:"0|single";s:23:"post_per_page_portfolio";s:1:"4";s:15:"order_portfolio";s:4:"desc";s:17:"orderby_portfolio";s:4:"date";s:17:"display_portfolio";s:4:"none";s:19:"img_width_portfolio";s:3:"225";s:20:"img_height_portfolio";s:3:"140";s:24:"hide_post_date_portfolio";s:3:"yes";s:24:"hide_post_meta_portfolio";s:3:"yes";s:23:"hide_page_nav_portfolio";s:3:"yes";}}}}}}i:4;a:2:{s:9:"row_order";s:1:"4";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:49:"<h2 style=\'text-align: center;\'>Testimonials</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:11:"testimonial";s:12:"mod_settings";a:9:{s:18:"layout_testimonial";s:5:"grid2";s:20:"category_testimonial";s:21:"testimonials|multiple";s:25:"post_per_page_testimonial";s:1:"4";s:17:"order_testimonial";s:4:"desc";s:19:"orderby_testimonial";s:4:"date";s:19:"display_testimonial";s:7:"content";s:21:"img_width_testimonial";s:2:"80";s:22:"img_height_testimonial";s:2:"80";s:25:"hide_page_nav_testimonial";s:3:"yes";}}}}}}i:5;a:2:{s:9:"row_order";s:1:"5";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:13:"last col-full";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:41:"<h2 style=\'text-align: center;\'>Blog</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:4:"post";s:12:"mod_settings";a:9:{s:11:"layout_post";s:11:"grid2-thumb";s:13:"category_post";s:15:"images|multiple";s:18:"post_per_page_post";s:1:"4";s:10:"order_post";s:4:"desc";s:12:"orderby_post";s:4:"date";s:12:"display_post";s:7:"excerpt";s:14:"img_width_post";s:3:"100";s:15:"img_height_post";s:3:"100";s:18:"hide_page_nav_post";s:3:"yes";}}}}}}i:6;a:2:{s:9:"row_order";s:1:"6";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:13:"last col-full";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:45:"<h2 style=\'text-align: center;\'>Our Team</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:11:"testimonial";s:12:"mod_settings";a:9:{s:18:"layout_testimonial";s:5:"grid4";s:20:"category_testimonial";s:13:"team|multiple";s:25:"post_per_page_testimonial";s:1:"4";s:17:"order_testimonial";s:4:"desc";s:19:"orderby_testimonial";s:4:"date";s:19:"display_testimonial";s:4:"none";s:21:"img_width_testimonial";s:2:"80";s:22:"img_height_testimonial";s:2:"80";s:25:"hide_page_nav_testimonial";s:3:"yes";}}}}}}}";';

		$_themify_builder_settings = stripslashes($_themify_builder_settings);
		$_themify_builder_settings = unserialize(unserialize($_themify_builder_settings));


		if(!is_array($_themify_builder_settings))
		{
			$_themify_builder_settings = array();
		}
		if(count($_themify_builder_settings)>0)
		{
			foreach($_themify_builder_settings as $builder_row_id => $builder_row)
			{
				$mod_found_row = false;
				foreach($builder_row['cols'] as $builder_col_id => $builder_col)
				{
					foreach($builder_col['modules'] as $builder_module_id => $builder_module)
					{
						$mod_found = $this->is_themify_builder_module_found($builder_module, $mod_name);
						if($mod_found)
						{
							$mod_found_row = true;
							/* We got the module lets update it */
							if(count($mod_settings)>0)
							{
								foreach($mod_settings as $setting_key=>$setting_value)
								{
									$builder_module['mod_settings'][$setting_key] = $setting_value;
								}
							}
							return $builder_module;
						}
					}
				}
			}

		}

	}
	protected function is_themify_builder_module_found($builder_module, $mod_name, $mod_condition=array())
	{
		$mod_found = false;
		if($builder_module['mod_name'] == $mod_name)
		{
			$mod_found = true;
			if(count($mod_condition)>0)
			{
				/* Need to check for settings */
				foreach($mod_condition as $condition_key=>$cont_value)
				{
					if( $builder_module['mod_settings'][$condition_key] != $cont_value)
					{
						$mod_found = false;
					}
				}
			}

		}
		return $mod_found;
	}

	protected function update_themify_builder_module_settings($post_id, $mod_name, $mod_settings, $mod_condition=array(), $row_settings=array())
	{
		$_themify_builder_settings = get_post_meta($post_id, '_themify_builder_settings', true);

		$_themify_builder_settings = maybe_unserialize($_themify_builder_settings);

		if(!is_array($_themify_builder_settings))
		{
			$_themify_builder_settings = array();
		}
		if(count($_themify_builder_settings)>0)
		{
			foreach($_themify_builder_settings as $builder_row_id => $builder_row)
			{
				$mod_found_row = false;
				foreach($builder_row['cols'] as $builder_col_id => $builder_col)
				{
					foreach($builder_col['modules'] as $builder_module_id => $builder_module)
					{
						$mod_found = $this->is_themify_builder_module_found($builder_module, $mod_name, $mod_condition);
						if($mod_found)
						{
							$mod_found_row = true;
							/* We got the module lets update it */
							if(count($mod_settings)>0)
							{
								foreach($mod_settings as $setting_key=>$setting_value)
								{
									$builder_module['mod_settings'][$setting_key] = $setting_value;
								}
							}
							$_themify_builder_settings[$builder_row_id]['cols'][$builder_col_id]['modules'][$builder_module_id] = $builder_module;
						}
					}
				}
				/* lets see if we need to update this row_settings */
				if($mod_found_row)
				{
					/* May be!! */
					foreach($row_settings as $setting_key=>$setting_value)
					{
						$_themify_builder_settings[$builder_row_id]['styling'][$setting_key] = $setting_value;
					}
				}
			}
		}
		update_post_meta($post_id,'_themify_builder_settings', $_themify_builder_settings);

	}

	function get_builder_data($data)
	{
		if (is_object($data))
		{
			$data = get_object_vars($data);
		}
		if (is_array($data))
		{
			return array_map(array($this,'get_builder_data'), $data);
		}
		else
		{
			return $data;
		}
	}
}
