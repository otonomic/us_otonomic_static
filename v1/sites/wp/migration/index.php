<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ignore_user_abort(true);
set_time_limit(0);

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');

if(!isset($_GET['facebook_id'])){
	echo "set facebook_id via GET";
	exit;
}

try {

    $facebook_id = rawurldecode($_GET['facebook_id']);
    $theme = isset($_GET['theme']) ? $_GET['theme'] : null;


    $respond = [];

    $base_dir = __DIR__.'/';

    include_once $base_dir . '../wp-load.php';

    include_once $base_dir . 'config/fb.php';
	include_once $base_dir . 'core/loader/OtonomicMigrationLoader.class.php';

	$OtonomicMigrationLoader = OtonomicMigrationLoader::init( $base_dir );

    //include_once $base_dir . 'utils.php';

    include_once $base_dir . 'helpers/Curl.php';

    include_once $base_dir . 'helpers/Page2site.php';
    include_once $base_dir . 'helpers/Section.php';
    include_once $base_dir . 'helpers/DB.php';
    include_once $base_dir . 'helpers/FB_email.php';

    include_once $base_dir . 'WPMigration.php';

    include_once $base_dir . "plugins/core/Factory.php";

    include_once $base_dir . "core/interfaces/ThemeInterface.php";
    include_once $base_dir . "core/interfaces/ThemePluginSupportInterface.php";

    // latest version of WooCommerce doesn't include this in all needed cases, so we do it for it
    // include_once WP_PLUGIN_DIR . '/woocommerce/includes/admin/wc-admin-functions.php';

	/*$page_subcategory_list = 'HairSalon';
	if(class_exists( $page_subcategory_list ))
	{
		$fb_category_object = new $page_subcategory_list();
	}
	print_r($fb_category_object); die();*/

    $domain = DOMAIN_CURRENT_SITE;

    /* Remove switch_theme action */
    $OtonomicTheme = OtonomicTheme::get_instance();
    $OtonomicTheme->remove_action('theme_switched');
	$OtonomicTheme->remove_action('regenerate_thumbnails');

    $wp = new WPMigration($facebook_id, $theme);
    $blog_id = $wp->create_site();

    $url = $wp->get_site_address();
    $user_id = $wp->get_user_id();

    //$token = md5($blog_id.sha1(SALT));
	$token = $wp->get_token();

    $respond['redirect'] = $url."/wp-admin/admin-ajax.php?action=wp_otonomic_migrate&cmd=login&user_id={$user_id}&token={$token}";
    $respond['site_url'] = $url;
    $respond['user_id'] = $user_id;
    $respond['status'] = "success";
    $respond['blog_id'] = $blog_id;
    $respond['message'] = "";
    $respond['token'] = $token;

} catch(Exception $e){
    $error = json_decode($e->getMessage());

    $respond['redirect'] =  $error->redirect;
    $respond['site_url'] = $error->site_url;
    $respond['status'] = "fail";
    $respond['message'] = $error->error;
}

if( isset($_GET['callback'])) {
    header('Content-Type: text/javascript');
    echo $_GET['callback'] . '(' . json_encode($respond) . ');';

} elseif (!empty($_GET['redirect'])) {
    header('Location: http://'.$respond['site_url']);

} else {
    echo json_encode($respond);
}


function trim_closest_word($string, $max_length, $append_ellipsis = true) {
    $result = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $max_length));
    if(strlen($string)>$max_length) { $result .= '&hellip;'; }
    return $result;
}
