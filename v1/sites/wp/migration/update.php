<?php
include_once '../wp-load.php';

include_once 'helpers/Curl.php';
include_once 'config/fb.php';

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');

include_once 'core/ThemeAbstract.php';
include_once 'core/ThemifyAbstract.php';
include_once "WPUpdate.php";

$blod_id = (int) $_GET['update_blog_id'];

if($blod_id == 0){
    echo "sed blog_id via GET";
    exit;
}

function trim_closest_word($string, $max_length, $append_ellipsis = true) {
    $result = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $max_length));
    if(strlen($string)>$max_length) { $result .= '&hellip;'; }
    return $result;
}

$wp = new WPUpdate($blod_id);
$wp->update();

echo "ok";