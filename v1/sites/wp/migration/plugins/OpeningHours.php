<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class OpeningHours extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'wp-opening-hours';

    public function install(){
        if (! $this->isPluginActive()) {
            return false;
        }

        global $wpdb;

		$blog_id = $this->blog_id;

		add_option( 'wp_opening_hours','{"monday":{"times":[["00","15","20","35"]]}}','yes' );
		add_option( 'widget_widget_op_overview','a:2:{i:2;a:3:{s:5:"title";s:0:"";s:14:"caption-closed";s:0:"";s:9:"highlight";s:7:"nothing";}s:12:"_multiwidget";i:1;}','yes' );
		update_option('sidebars_widgets','a:9:{s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:19:"wp_inactive_widgets";a:0:{}s:12:"sidebar-main";a:0:{}s:13:"social-widget";a:0:{}s:15:"footer-widget-1";a:0:{}s:15:"footer-widget-2";a:0:{}s:15:"footer-widget-3";a:0:{}s:22:"widgets_for_shortcodes";a:1:{i:0;s:20:"widget_op_overview-2";}s:13:"array_version";i:3;}','yes');
	}
}

