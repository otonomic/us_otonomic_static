<?php
namespace Plugins\Core;

interface PluginInterface {
	
	public function setBlogID($blog_id);
	public function setData($data);
	public function install();
}