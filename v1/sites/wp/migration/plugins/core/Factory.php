<?php
namespace Plugins\Core;
use Plugins;


class Factory {
	

	public static function make($plugin_name){


		try {

			$plugin_name = ucfirst($plugin_name);

			require_once __dir__ ."/../".$plugin_name .".php";

			$class_name = 'Plugins\\'. $plugin_name;

			if(class_exists($class_name)){

				return new $class_name;
				

			} else {
				throw new \Exception("plugin not found");
			}


		} catch (\Exception $e){
			echo $e->getMessage();
		}

	}

}