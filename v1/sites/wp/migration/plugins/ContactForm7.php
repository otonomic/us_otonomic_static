<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class ContactForm7 extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'contact-form-7';

    public function install(){
        if (! $this->isPluginActive()) {
            return false;
        }

    }
}

