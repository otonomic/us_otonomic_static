<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class Handheld extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'handheld';

    public function install(){
//        if (! $this->isPluginActive()) {
//            return false;
//        }

        global $wpdb;
                $blog_id = $this->blog_id;
                $activated_plugins = get_option('active_plugins');
                $activated_plugins[] = $this->plugin_name."/".$this->plugin_name.".php";
                update_option('active_plugins', $activated_plugins);
                
                $et_mobile_handheld_options = unserialize('a:21:{s:10:"main_theme";s:8:"HandHeld";s:11:"child_theme";s:14:"handheld-child";s:4:"logo";s:0:"";s:18:"webpage_icon_small";s:0:"";s:16:"webpage_icon_big";s:0:"";s:12:"splash_image";s:0:"";s:13:"activate_ipad";i:0;s:15:"home_intro_page";s:3:"175";s:20:"home_blog_categories";a:1:{i:0;s:1:"2";}s:23:"home_project_categories";a:1:{i:0;s:1:"3";}s:19:"home_blog_posts_num";s:1:"3";s:23:"home_blog_posts_add_num";s:1:"3";s:23:"home_projects_posts_num";s:1:"6";s:27:"home_projects_posts_add_num";s:1:"6";s:15:"home_front_page";s:1:"0";s:8:"bg_color";s:0:"";s:20:"home_welcome_section";i:0;s:24:"home_recentposts_section";i:1;s:23:"home_recentwork_section";i:1;s:20:"single_post_comments";i:0;s:20:"single_page_comments";i:0;}');
                $et_mobile_plugin_options = unserialize('a:2:{s:10:"main_theme";s:8:"HandHeld";s:11:"child_theme";s:14:"handheld-child";}');
                
		update_option('et_mobile_handheld_options',$et_mobile_handheld_options );
                update_option('et_mobile_plugin_options', $et_mobile_plugin_options);
                $this->add_background_image();
        }
        
        public function add_background_image(){
            $site_fb_data = get_option('otonomic_site_data_facebook');
            if(isset($site_fb_data->cover)){
                update_option('handheld_homepage_bgimg', $site_fb_data->cover->source);
            }
        }
}

