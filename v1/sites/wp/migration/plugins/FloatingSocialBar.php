<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class FloatingSocialBar extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'floating-social-bar';

    public function install(){
        if (! $this->isPluginActive()) {
            return false;
        }

    }
}

