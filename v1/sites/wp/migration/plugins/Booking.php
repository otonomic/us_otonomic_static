<?php
namespace Plugins;

include_once "core/PluginInterface.php";
include_once "core/PluginInstallerClass.php";

class Booking extends Core\PluginInstallerClass implements Core\PluginInterface {
	
    protected $plugin_name = 'birchschedule';

	public function install()
	{
        if (! $this->isPluginActive()) {
            return false;
        }

		global $wpdb;
		$Otonomic_First_Session = \Otonomic_First_Session::get_instance();

		$blog_id = $this->blog_id;

		// set default data for booking

		//location
		$my_post = array(
		  'post_title'    => 'Main office',
		  'post_status'   => 'publish',
		  'post_type' 	  => 'birs_location'
		);
		$location_id = wp_insert_post( $my_post );

		// staff
		$my_post = array(
		  'post_title'    => 'Staff',
		  'post_status'   => 'publish',
		  'post_type' 	  => 'birs_staff'
		);
		$staff_id = wp_insert_post( $my_post );

		//services
		$my_post = array(
		  'post_title'    => 'Default',
		  'post_status'   => 'publish',
		  'post_type' 	  => 'birs_service'
		);
		$service_id = wp_insert_post( $my_post );

        $phone = $Otonomic_First_Session->get_settings('phone');
        $location = $Otonomic_First_Session->get_settings('location');
        if( $location) {
            $street     = $location->street;
            $city       = $location->city;
            $state      = $location->state;
            $country    = $location->country;
            $zip        = $location->zip;

        } else {
            $street = $Otonomic_First_Session->get_settings('address');
            $city = $state = $country = $zip = "";
        }

$query =<<<E
INSERT INTO `wp_{$blog_id}_postmeta` (`post_id`, `meta_key`, `meta_value`) VALUES
({$service_id}, '_edit_last', '1'),
({$service_id}, '_birs_service_length', ''),
({$service_id}, '_birs_service_padding', ''),
({$service_id}, '_birs_service_price', ''),
({$service_id}, '_birs_service_length_type', 'minutes'),
({$service_id}, '_edit_lock', '1400582876:1'),
({$service_id}, '_birs_service_length_type', 'minutes'),
({$service_id}, '_birs_service_padding_type', 'before'),
({$service_id}, '_birs_service_price_type', 'fixed'),
({$service_id}, '_birs_assigned_staff', 's:22:"a:1:{i:{$staff_id};s:2:"on";}";'),
({$staff_id}, '_birs_staff_schedule', 's:212:"a:1:{i:{$location_id};a:1:{s:9:"schedules";a:1:{s:13:"537b33710e0f1";a:3:{s:13:"minutes_start";s:3:"540";s:11:"minutes_end";s:4:"1020";s:5:"weeks";a:5:{i:1;s:2:"on";i:2;s:2:"on";i:3;s:2:"on";i:4;s:2:"on";i:5;s:2:"on";}}}}}";'),
({$staff_id}, '_edit_last', '1'),
({$staff_id}, '_edit_lock', '1400583044:1'),
({$staff_id}, '_birs_assigned_services', 's:22:"a:1:{i:{$service_id};s:2:"on";}";'),
({$location_id}, '_edit_last', '1'),
({$location_id}, '_edit_lock', '1400578002:1'),
({$location_id}, '_birs_location_phone', '{$phone}'),
({$location_id}, '_birs_location_address1', '{$street}'),
({$location_id}, '_birs_location_address2', ''),
({$location_id}, '_birs_location_city', '{$city}'),
({$location_id}, '_birs_location_state', '{$state}'),
({$location_id}, '_birs_location_country', '{$country}'),
({$location_id}, '_birs_location_zip', '{$zip}')
;
E;

		$wpdb->get_results($query);
		/* settings array */
		$birch_settings = array
		(
			'version' => 1,
		    'currency' => 'USD',
		    'default_calendar_view' => 'month'
		);
		add_option( 'birchschedule_options',$birch_settings,'yes');
		add_option( 'birs_db_version_appointment','1.2','yes' );
		add_option( 'birs_staff_schedule_version','1.1','yes' );
	}
}