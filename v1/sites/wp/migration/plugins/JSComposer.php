<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class JSComposer extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'js_composer';

    public function install(){
        if (! $this->isPluginActive()) {
            return false;
        }

    }
}

