<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class WooCommerce extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'woocommerce';

    public function install(){
        include_once WP_PLUGIN_DIR . '/woocommerce/includes/admin/wc-admin-functions.php';

        if (! $this->isPluginActive()) {
            return false;
        }

        /*
        $blog_id = $this->blog_id;

		$OtonomicStore = \OtonomicStore::get_instance();
	    $OtonomicStore->add_sample_product();
		$OtonomicStore->show_store_pages();
        */
    }
}