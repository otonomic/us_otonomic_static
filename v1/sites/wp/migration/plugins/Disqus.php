<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class Disqus extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'disqus-comment-system';

    public function install(){
        $activated_plugins = get_option('active_plugins');
        $activated_plugins[] = $this->plugin_name."/disqus.php";
        update_option('active_plugins', $activated_plugins);

        update_option('disqus_active', '1');
        update_option('disqus_version', '2.84');
        update_option('disqus_forum_url', 'otonomic-test');
        update_option('disqus_cc_fix', '1');
        update_option('disqus_api_key', 'oSCcUfyJTYe6Ty6ji1mKVcWpnz0UVLlVicJjJVkSqdmXGTI7EkPZ3PIBqTAWtGzQ');
        update_option('disqus_user_api_key', 'uzQujoUrqQ7g52f6QzJAZoJ4HiGD7CM6nuZmqGXSY2jDTXixhMjmQkHNdaA8TjKD');
        update_option('disqus_replace', 'all');
    }
}

