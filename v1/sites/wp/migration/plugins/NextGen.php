<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class NextGen extends Core\PluginInstallerClass implements Core\PluginInterface {

    protected $plugin_name = 'nextgen-gallery';

    public function install()
	{
        $max_num_images_nextgen_gallery = 30;

        if (! $this->isPluginActive()) {
            return false;
        }

		$OtonomicNextgen = \OtonomicNextgen::get_instance();

		$gallery_id = $OtonomicNextgen->create_gallery( 'facebook' );

        $image_urls = $this->get_images_for_gallery($this->data->albums);
		if(!is_null($image_urls))
		{
            $image_urls = array_slice( $image_urls, 0, $max_num_images_nextgen_gallery);
			$OtonomicNextgen->add_to_gallery($gallery_id, $image_urls);
		}
	}


	public function install_old()
	{
		if(!is_array($this->page->albums) || count($this->page->albums) == 0)
			return;

        $gallery_id = null;

		/* Lets create the NextGen 'facebook' album */
		$OtonomicNextgen = \OtonomicNextgen::get_instance();
		$nextgen_album = $OtonomicNextgen->create_album('facebook');

		foreach($this->page->albums as $key=>$album)
		{
			try
			{
                $gallery_id = $this->add_album($album, $nextgen_album);
			}
			catch(Exception $e)
			{
				/* Silently ignore the error */
			}
		}

        return $gallery_id;
	}


	public function add_album( $fbalbum, $album = null )
	{
		$album_photos = $fbalbum->photos->data;

		// Create a NextGEN gallery
		$OtonomicNextgen = \OtonomicNextgen::get_instance();
		$gallery_id = $OtonomicNextgen->create_gallery( $fbalbum->name );

		// adding the image to the post
		$image_urls = array();

		foreach($album_photos as $picture) {
			$image_urls[] = ($picture->full_picture) ? $picture->full_picture : $picture->source;
        }

		$OtonomicNextgen->add_to_gallery($gallery_id, $image_urls);

		if(!empty($album))
		{
			$OtonomicNextgen->add_to_album($album, $gallery_id);
		}

        return $gallery_id;
	}


    function get_images_for_gallery($albums) {
        if(!is_array($albums) || count($albums) == 0) {
            return;
        }

        $image_urls = array();

        foreach($albums as $album)
        {
            try
            {
                $album_photos = $album->photos->data;
                foreach((array)$album_photos as $picture)
                {
                    $image_urls[] = ($picture->full_picture) ? $picture->full_picture : $picture->source;
                }
            }
            catch(Exception $e)
            {
                /* Silently ignore the error */
            }
        }

        return $image_urls;
    }
}