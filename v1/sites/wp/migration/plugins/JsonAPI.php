<?php
namespace Plugins;

include_once "core/PluginInterface.php";

class JsonAPI implements Core\PluginInterface {

    private $blog_id;

    public function setBlogID($blog_id){
        $this->blog_id = $blog_id;
    }

    public function setData($data){
        return;
    }

    public function install(){

        $allowed_controllers = 'core,store,widgets,adminpage,settings,menu,menuitems,products,testimonials';

        add_option( 'json_api_base','api');
        add_option( 'json_api_controllers', $allowed_controllers);
    }
}

