<?php
/**
 * Misc. helper functions
 */

function ot_insert_post($data_post, $data_postmeta = []) {

    kses_remove_filters();
	$post_id = wp_insert_post($data_post);
	if($data_postmeta) {
		foreach($data_postmeta as $key => $value) {
			add_post_meta($post_id, $key, $value);
		}
	}
    kses_init_filters();

	return $post_id;
}

function ot_get_source_url($url) {
    $url = urldecode($url);

    if(strrpos($url, 'http') > 0) {
        $url = preg_replace('/http.*http/','http', urldecode($url));
    }
    $url = preg_replace('/(p130x130|s130x130|p128x128)\//','', $url);

    if (strpos($url,'/v/') !== false) {
        $url = str_replace("/v/", "/", $url);
        $url = strstr($url, '?', true);
    }

    if (strpos($url,'&') !== false) {
        $url = explode('&', $url);
        $url = $url[0];
    }

    return $url;
}
