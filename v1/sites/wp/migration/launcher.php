<?php
if( !empty($_GET['path'])) {
    $launch_path = $_GET['path'];
}
if( !empty($_GET['class'])) {
    $launch_class = $_GET['class'];
}
if( !empty($_GET['method'])) {
    $launch_method = $_GET['method'];
}

if( !$launch_path || !$launch_method) {
    wp_die('Incorrect request');
}

$base_dir = __DIR__.'/';

include_once $base_dir . '../wp-load.php';

include_once $base_dir . 'config/fb.php';

include_once $base_dir . 'helpers/Curl.php';
include_once $base_dir . 'helpers/Section.php';
include_once $base_dir . 'helpers/DB.php';

include_once $base_dir . 'WPMigration.php';

include_once $base_dir . "plugins/core/Factory.php";

include_once $base_dir . "core/ThemeAbstract.php";
include_once $base_dir . "core/ThemifyAbstract.php";
include_once $base_dir . "core/interfaces/ThemeInterface.php";
include_once $base_dir . "core/interfaces/ThemePluginSupportInterface.php";

if( file_exists($launch_path)) {
    include_once $launch_path;
}
if( class_exists($launch_class)) {
    $c = new $launch_class();
    if( method_exists($c, $launch_method)) {
        $c->$launch_method();
    }

} else {
    if( function_exists( $launch_method)) {
        $launch_method();
    }
}
