<?php
class Generic extends FBCategoryConfig
{
	public function __construct()
	{
		parent::__construct();

		$this->module_status = [
			'store' => false,
			'booking' => true,
		];
		$this->template = 'dreamtheme';

		$splash_images = array();
		$services = array();

		/*
		*** adding splash image ***
		$splash_image_1 = new stdClass();
		$splash_image_1->picture = $splash_image_1->source = 'image_path';

		$splash_images[] = $splash_image_1;

		*** adding services ***
		$service_1 = array(
			'title'=>'Some title',
			'content'=>'Some content',
			'image'=>'Image URL'
		);
		$services[] = $service_1;
		*/
		$this->splash_images = $splash_images;
		$this->services = $services;
	}
}