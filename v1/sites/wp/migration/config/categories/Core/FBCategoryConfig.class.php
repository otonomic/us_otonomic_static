<?php
class FBCategoryConfig
{
	protected $module_status;
	protected $template;
	protected $splash_images = array();
	protected $services = array();

	public function __construct()
	{

	}

	function get_module_status($module_name)
	{
		if(isset($this->module_status[$module_name]))
			return $this->module_status[$module_name];

		return true;
	}

	function get_template()
	{
		return $this->template;
	}
	function get_splash_images()
	{
		return $this->splash_images;
	}

	function get_services()
	{
		return $this->services;
	}
}