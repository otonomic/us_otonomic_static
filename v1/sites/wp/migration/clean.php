<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');
set_time_limit ( 0);

if(!isset($_GET['site_id'])){
    echo "set me site_id via GET";
    exit;
}

if($_SERVER['HTTP_HOST']=="wp.otonomic.com" || $_SERVER['HTTP_HOST']=="wp.verisites.com") {
    define(SUBDOMAIN_INSTALL, true);
} else {
    define(SUBDOMAIN_INSTALL, false);
}

require_once '../wp-load.php';
require_once '../wp-admin/includes/ms.php';
require_once './helpers/DB.php';

$deleteSiteClass = new OtonomicDeleteSite;
$deleteSiteClass->run();

class OtonomicDeleteSite {
    var $site_id_start, $site_id_end;

    function __construct() {

    }

    function run() {
        $this->get_sites_to_delete();
        $this->delete_sites();
    }

    function get_sites_to_delete() {
        $site_id = $_GET['site_id'];
        $site_id_temp = explode('...', $site_id);

        if(count($site_id_temp)===2) {
            $this->site_id_start = $site_id_temp[0];
            $this->site_id_end = $site_id_temp[1];

        } else {
            $this->site_id_start = $this->site_id_end = $site_id_temp[0];
        }

        if($this->get_logged_in_user_type($this->site_id_start) != 'superadmin')
        {
            $this->site_id_end = $this->site_id_start;
        }
    }

    function get_logged_in_user_type($site_id) {
        $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        return $Otonomic_First_Session->get_user_type(null, $site_id);
    }

    function authorize_user_to_delete_site($site_id) {
        $user_type = $this->get_logged_in_user_type($site_id);

        if($user_type == 'visitor')
        {
            wp_die('You need to login to perform this action');
        }

        if($user_type != 'superadmin')
        {
            if( !current_user_can_for_blog( $site_id, 'manage_options')) {
                wp_die('Unauthorized request');
            }
        }

        return true;
    }

    function drop_tables($site_id) {
        $db = DB::getConnection();

        $clean_wp_blogs = 'DELETE FROM wp_blogs WHERE blog_id='. $site_id;
        //echo $clean_wp_blogs .'<br>';
        $db->exec($clean_wp_blogs);

        /*
        //get blog admin user_id
        $select_sql = 'SELECT user_id FROM wp_usermeta WHERE meta_key="primary_blog" AND meta_value=' . $site_id;
        echo $select_sql . '<br>';
        $result = $db->query($select_sql);
        foreach($result as $row) {
            $user_id = $row['user_id'];
        }

        if(!empty($user_id)){
            $delete_user_sql = 'DELETE FROM wp_users WHERE ID=' . $user_id;
            echo $delete_user_sql .'<br>';
            $db->exec($delete_user_sql);
            $delete_usermeta_sql = 'DELETE FROM wp_usermeta WHERE user_id='.$user_id;
            echo $delete_usermeta_sql .'<br>';
            $db->exec($delete_usermeta_sql);
        }
        */

        $tables = array('commentmeta',
            'comments',
            'links',
            'options',
            'postmeta',
            'posts',
            'terms',
            'term_relationships',
            'term_taxonomy',
            'popover',
            'popover_ip_cache',
            'woocommerce_order_items',
            'woocommerce_downloadable_product_permissions',
            'woocommerce_attribute_taxonomies',
            'woocommerce_tax_rate_locations',
            'woocommerce_tax_rates',
            'woocommerce_order_itemmeta',
            'woocommerce_termmeta',
            'ngg_album',
            'ngg_gallery',
            'ngg_pictures'
        );

        foreach($tables as $table){
            $drop_query = 'DROP TABLE wp_' . $site_id .'_'.$table;
            //echo $drop_query .'<br>';
            $db->exec($drop_query);
        }
    }

    function delete_from_usermeta($site_id) {
        $db = DB::getConnection();

        $clean_wp_blogs = 'DELETE FROM wp_usermeta WHERE meta_key LIKE "wp_'.$site_id.'%"';
        //echo $clean_wp_blogs .'<br>';
        $db->exec($clean_wp_blogs);
    }

    function delete_from_otonomic_sites($site_id) {
        $db = DB::getConnection();

        $clean_wp_blogs = 'DELETE FROM otonomic_sites WHERE site_id = '.$site_id;
        //echo $clean_wp_blogs .'<br>';
        $db->exec($clean_wp_blogs);
    }
	function delete_from_otonomic_crons($site_id) {
		$db = DB::getConnection();

		$clean_crons = 'DELETE FROM otonomic_cron WHERE cron_site_id='. $site_id;
		$db->exec($clean_crons);

	}

    function get_site_id_from_slug($site_id) {
        if(!SUBDOMAIN_INSTALL) {
            $domain = DOMAIN_CURRENT_SITE;
            $path = '/'.trim($site_id,'/').'/';
            $site_id = get_blog_id_from_url($domain, $path);

        } else {
            $domain = trim($site_id.'.'.DOMAIN_CURRENT_SITE,'/');
            $domain = str_replace('wp.', '', $domain);
            $site_id = get_blog_id_from_url($domain);
        }

        if($site_id == 0) {
            // die("Can't find site. Please correct your request");
            return null;
        }

        return $site_id;
    }


    function delete_sites() {
        for( $site_id = $this->site_id_start; $site_id <= $this->site_id_end; $site_id++) {
            if(!is_numeric($site_id)){
                $site_id = $this->get_site_id_from_slug($site_id);
            }

            if(!is_numeric($site_id)){
                continue;
            }

            $this->authorize_user_to_delete_site($site_id);

            wpmu_delete_blog($site_id, $drop = true);
            $this->drop_tables($site_id);
            $this->delete_from_usermeta($site_id);
            $this->delete_from_otonomic_sites($site_id);
	        $this->delete_from_otonomic_crons($site_id);
        }

        if(isset($_GET['redirect']))
        {
            header('Location: http://otonomic.com/?msg=site-deleted');
        } else {
            echo 'Finished cleaning site #'.$site_id.'<br/>';
        }
        die();
    }
}