<?php
$base_dir = __DIR__.'/';

include_once $base_dir . '../wp-load.php';

include_once $base_dir . 'config/fb.php';

include_once $base_dir . 'helpers/Curl.php';
include_once $base_dir . 'helpers/Section.php';
include_once $base_dir . 'helpers/DB.php';

include_once $base_dir . 'WPMigration.php';

include_once $base_dir . "plugins/core/Factory.php";

include_once $base_dir . "core/ThemeAbstract.php";
include_once $base_dir . "core/ThemifyAbstract.php";
include_once $base_dir . "core/interfaces/ThemeInterface.php";
include_once $base_dir . "core/interfaces/ThemePluginSupportInterface.php";

$domain = DOMAIN_CURRENT_SITE;


$facebook_id = $_GET['facebook_id'];
$theme = 'parallax';
$wp = new WPMigration($facebook_id, $theme);
echo json_encode($wp->page); die;
