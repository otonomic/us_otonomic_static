<?php
require_once "../wp-load.php";

$user_id = $_GET['user_id'];
$site_url = $_GET['site_url'];

if (strpos($site_url,'http') === false) {
    $site_url = "http://". $site_url;
}

wp_clear_auth_cookie();
wp_set_auth_cookie  ( $user_id , true);

header('location: '. $site_url);

