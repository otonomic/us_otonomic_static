<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/16/14
 * Time: 4:55 PM
 */

class Section {

    public static function hide($blog_id, $section_id){
        global $wpdb;

        if((int) $section_id == 0)
            return;

        $wpdb->get_results( 'UPDATE wp_'. $blog_id .'_term_relationships SET term_taxonomy_id = 0 where object_id ='. $section_id );
    }

    public static function show($blog_id, $section_id){
        global $wpdb;

        if((int) $section_id == 0)
            return;

        $wpdb->get_results( 'UPDATE wp_'. $blog_id .'_term_relationships SET term_taxonomy_id = 25 where object_id ='. $section_id );
    }

}

