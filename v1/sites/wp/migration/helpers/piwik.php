<?php
class Piwik{

    var $piwik_url = 'http://progang.com/piwik';
    var $piwik_token = '4d1ff0386c1933bcb68ad517a6573d1e';

    private function makeCall($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    public function add_website($name, $url){
        $data['module'] = 'API';
        $data['method'] = 'SitesManager.addSite';
        $data['siteName'] = $name;
        $data['urls'] = urlencode($url);
        $data['format'] = 'PHP';
        $data['token_auth'] = $this->piwik_token;

        // we call the REST API and request the 100 first keywords for the last month for the idsite=7
        //$url = $this->piwik_url .'?'. http_build_query($data);

        $url = $this->piwik_url .'?';
        foreach($data as $key => $value)
            $url .= $key.'='.$value .'&';

        $url = rtrim($url,'&');

        echo $url;
        //$fetched = file_get_contents($url);
        $fetched = $this->makeCall($url);
        print_r($fetched);
        $content = unserialize($fetched);

        // case error
        if (!$content) {
            print("Error, content fetched = " . $fetched);
        }

        print_r($content);
    }

    public function get_keywords($site_id){
        $data['module'] = 'API';
        $data['method'] = 'Referrers.getKeywords';
        $data['idSite'] = $site_id;
        $data['period'] = 'month';
        $data['date'] = 'yesterday';
        $data['format'] = 'PHP';
        $data['filter_limit'] = 20;
        $data['token_auth'] = $this->piwik_token;

        // we call the REST API and request the 100 first keywords for the last month for the idsite=7
        $url = $this->piwik_url .'?'. http_build_query($data);

        echo $url;
        $fetched = file_get_contents($url);
        $content = unserialize($fetched);

        // case error
        if (!$content) {
            print("Error, content fetched = " . $fetched);
        }

        print("<h1>Keywords for the last month</h1>");
        foreach ($content as $row) {
            $keyword = htmlspecialchars(html_entity_decode(urldecode($row['label']), ENT_QUOTES), ENT_QUOTES);
            $hits = $row['nb_visits'];

            print("<b>$keyword</b> ($hits hits)<br>");
        }
    }
}