<?php
class Utility
{
	public static function submit_to_search_engines( $blog_id=null )
	{
		if(!empty($blog_id))
			switch_to_blog( $blog_id );

		$sitemapurl = urlencode( home_url( 'sitemap_index.xml' ) );

		wp_remote_get( 'http://www.google.com/webmasters/tools/ping?sitemap=' . $sitemapurl );
		wp_remote_get( 'http://www.bing.com/ping?sitemap=' . $sitemapurl );
		wp_remote_get( 'http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=3usdTDLV34HbjQpIBuzMM1UkECFl5KDN7fogidABihmHBfqaebDuZk1vpLDR64I-&url=' . $sitemapurl );
		wp_remote_get( 'http://submissions.ask.com/ping?sitemap=' . $sitemapurl );

		if(!empty($blog_id))
			restore_current_blog();
	}
	public static function populate_post_author( $blog_id=null )
	{
		global $wpdb;
		$sql = "update {$wpdb->posts} set post_author=1 where post_author = 0";
		$wpdb->query($sql);
	}
	public static function setup_site_seo_data( $blog_id=null )
	{
		if(!empty($blog_id))
			switch_to_blog( $blog_id );

		$OtonomicSEO  = OtonomicSEO::get_instance();

		$pages = self::get_all_pages();
		$OtonomicSEO->add_home_seo_tags();
		foreach($pages as $page)
		{
			$OtonomicSEO->add_seo_tags( $page->ID );
		}
		/*$home_page_id = self::get_page_id_by_name('Home');
		$OtonomicSEO->add_seo_tags( $home_page_id );
		$OtonomicSEO->add_home_seo_tags( );

		$shop_page_id = self::get_page_id_by_name('Shop');
		$OtonomicSEO->add_seo_tags( $shop_page_id );

		$cart_page_id = self::get_page_id_by_name('Cart');
		$OtonomicSEO->add_seo_tags( $cart_page_id );

		$checkout_page_id = self::get_page_id_by_name('Checkout');
		$OtonomicSEO->add_seo_tags( $checkout_page_id );

		$my_account_page_id = self::get_page_id_by_name('My Account');
		$OtonomicSEO->add_seo_tags( $my_account_page_id );*/

		if(!empty($blog_id))
			restore_current_blog();
	}
	public static function get_all_pages()
	{
		global $wpdb;
		$sql = "SELECT ID FROM wp_".get_current_blog_id()."_posts
			WHERE post_type='page'";
		$pages = $wpdb->get_results( $sql );

		return $pages;
	}
	public static function get_page_id_by_name($page_name)
	{
		$options = array('name' => $page_name);
		$options['post_type'] = 'page';
		$wpquery = new WP_Query($options);
		return $wpquery->post->ID;
	}
	public static function get_all_blogs()
	{
		global $wpdb;
		$sql = "SELECT * FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";
		$blogs = $wpdb->get_results( $sql );

		return $blogs;
	}
}