<?php
function get_themify_builder_module_slider() {
    $module =
        array (
            'mod_name' => 'slider',
            'mod_settings' =>
            array (
                'layout_display_slider' => 'blog',
                'blog_category_slider' => 'top-stories|multiple',
                'slider_category_slider' => '|single',
                'portfolio_category_slider' => '|single',
                'posts_per_page_slider' => '3',
                'display_slider' => 'content',
                'hide_post_title_slider' => 'yes',
                'layout_slider' => 'slider-overlay',
                'img_w_slider' => '672',
                'img_h_slider' => '350',
                'visible_opt_slider' => '1',
                'auto_scroll_opt_slider' => '7',
                'scroll_opt_slider' => '1',
                'speed_opt_slider' => 'normal',
                'effect_slider' => 'fade',
                'wrap_slider' => 'yes',
                'show_nav_slider' => 'yes',
                'show_arrow_slider' => 'yes',
            ),
        );

    return $module;
}
?>