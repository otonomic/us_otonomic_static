<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/16/14
 * Time: 4:55 PM
 */

class ThemifyBuilder {

    public static function get_module($module_name){
        if( file_exists('themify_builder_modules/'.$module_name.'.php')) {
            include_once ( 'themify_builder_modules/'.$module_name.'.php');
        }
        $function_name = 'get_themify_builder_module_'.$module_name;
        if( function_exists($function_name)) {
            return $function_name();
        }
        return null;
    }

    public static function set_module_settings($module, $settings) {
        return array_merge_recursive( $module, $settings);
    }

    public static function get_row( $row_order, $modules) {
        $row = Self::get_module('row');
        $row_settings = [
            'row_order' => $row_order,
            'cols' =>
                array (
                    0 => $modules
                )
        ];
        return Self::set_module_settings($row, $row_settings);
    }
}

