<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 6/17/14
 * Time: 11:14 AM
 */

class DB {


    public static function getConnection(){


        $config['host'] = DB_HOST;
        $config['database'] = DB_NAME;
        $config['login'] = DB_USER;
        $config['password'] = DB_PASSWORD;

        $dbh = new PDO('mysql:host='. $config['host'] .';dbname='.  $config['database'] .'',  $config['login'],  $config['password']);
        $dbh->exec("SET CHARACTER SET utf8");
        return $dbh;

    }

}