<?php

if(!isset($_GET['site_id'])){
    echo "set me site_id via GET";
    exit;
}

if(!isset($_GET['theme'])){
    echo "set theme via GET";
    exit;
}



$base_dir = __DIR__.'/';

include_once $base_dir . '../wp-load.php';

include_once $base_dir . 'config/fb.php';

include_once $base_dir . 'utils.php';

include_once $base_dir . 'helpers/Curl.php';
include_once $base_dir . 'helpers/Section.php';
include_once $base_dir . 'helpers/DB.php';

include_once $base_dir . 'WPMigration.php';

include_once $base_dir . "plugins/core/Factory.php";

include_once $base_dir . "core/ThemeAbstract.php";
include_once $base_dir . "core/ThemifyAbstract.php";
include_once $base_dir . "core/interfaces/ThemeInterface.php";
include_once $base_dir . "core/interfaces/ThemePluginSupportInterface.php";

$Otonomic_First_Session = Otonomic_First_Session::get_instance();

$domain = DOMAIN_FOR_THE_SYSTEM;

$site_id = $_GET['site_id'];

if(!is_numeric($site_id))
{
    $site_id = get_blog_id_from_url($site_id);
}
else
{
	$site_id = intval( $site_id );
}

$theme = $_GET['theme'];

/* Lets try to switch the blog */
switch_to_blog( $site_id );

if(isset($_GET['switched']))
{
    $current_theme = get_option( 'theme_switched' );
}
else
{
    $current_theme = strtolower( wp_get_theme() );
}

$facebook_id = $Otonomic_First_Session->get_settings('facebook_id');

$wp = new WPMigration($facebook_id, $theme);

include_once "themes/". strtolower($current_theme) ."/". ucfirst($current_theme) .'.php';
$current_theme_class = ucfirst($current_theme);
$current_theme_object = new $current_theme_class($site_id, $wp->page);
$content = $current_theme_object->switch_from_theme();

/* Remove switch_theme action */
$OtonomicTheme = OtonomicTheme::get_instance();
$OtonomicTheme->remove_action('theme_switched');

/* Now lets switch to new theme */

include_once "themes/". strtolower($theme) ."/". ucfirst($theme) .'.php';
$theme_class = ucfirst($theme);
$theme_object = new $theme_class($site_id, $wp->page);
$theme_object->switch_to_theme( $content );

echo 'SUCCESS';