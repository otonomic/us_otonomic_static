<?php
class WPMigration {
	private $blog_id;
	private $user_id = 0;
    public $slug;

	public $page;

    private $theme;

	private $fb_category_object;

	public function __construct($identifier = '', $theme = '') {
        if(!$identifier) { return; }

        $this->page = $this->fetch_page_data($identifier);

        $this->fb_category_object = null;

        /* Read page Sub-Category */
        $page_subcategory_list = json_decode($this->page->basic->category_list);

        if(count($page_subcategory_list)>0) {
            /* Clean Sub-Category Name */
            foreach($page_subcategory_list as $page_subcategory) {
                $page_subcategory = strtolower(str_replace(' ', '-', $page_subcategory->name));
                $page_subcategory = preg_replace('/[^A-Za-z0-9\-]/', '-', $page_subcategory);
                /* Get Class Name */
                $page_subcategory_array = explode('-', $page_subcategory);
                $page_subcategory_array = array_map('ucfirst', $page_subcategory_array);
                $page_subcategory = implode($page_subcategory_array);

                if (class_exists($page_subcategory)) {
                    $this->fb_category_object = new $page_subcategory();
                    break;
                }
            }
        }

		if(is_null($this->fb_category_object)) {
            $this->fb_category_object = new Generic();
        }
        $this->theme = $this->fb_category_object->get_template();

        if($theme) {
			$this->theme = $theme;
		}

        // $this->slug = Page2site::slugify2( $this->page->basic->slug);
        // if( !$this->slug) { $this->slug = 's'.$this->page->basic->id; }
        $this->slug = 's'.$this->page->facebook->id;
    }

	function ping_search_engines()
	{
		$sitemapurl = urlencode( home_url( 'sitemap_index.xml' ) );

        if(!WP_DEBUG && !LOCALHOST) {
            wp_remote_get( 'http://www.google.com/webmasters/tools/ping?sitemap=' . $sitemapurl );
            wp_remote_get( 'http://www.bing.com/ping?sitemap=' . $sitemapurl );
        }
	}

	function schedule_crons()
	{
		$OtonomicCrons = OtonomicCrons::get_instance();
		$Otonomic_First_Session = Otonomic_First_Session::get_instance();

		$date_after_12_days = date('Y-m-d H:i:s', strtotime("+12 days"));

		$OtonomicCrons->AddCron('populate-suggested-domain', 'SITE_CREATION', array($Otonomic_First_Session, "save_suggested_domains"), OtonomicCrons::ONCE);
		$OtonomicCrons->AddCron('12-days-since-site-created', 'SITE_CREATION', array('otonomic_transactional_email', 'email_12_days_since_site_created'), OtonomicCrons::ONCE,null,$date_after_12_days);

	}

	function regenerate_thumbs()
	{
		$OtonomicTheme = OtonomicTheme::get_instance();
		$OtonomicTheme->regenerate_thumbnails();
	}

    function create_site() {
        wp_defer_term_counting( true );
        wp_defer_comment_counting( true );
        $this->create_blog_and_user();

        $blog_id = $this->installTheme();

        $this->save_site_to_otonomic_sites_table();
        $this->ping_search_engines();
	    $this->schedule_crons();
	    //$this->regenerate_thumbs();

        // TODO: Write function and uncomment this line
        // $this->save_site_to_otonomic_sites_table();

        wp_defer_term_counting( false );
        wp_defer_comment_counting( false );

        return $blog_id;
    }

    function create_blog_and_user() {

        $user = wp_get_current_user();
	    $intercom_email = '';
	    $user_email = '';
        if($user->ID == 0){
	        $user_email = $this->page->basic->email;
	        if(empty($user_email))
	        {
		        $scraper = new FbEmailScrape();
		        $email = json_decode($scraper->getEmail($this->page->facebook->id));
		        if(!empty($email->result)) {
			        $user_email = html_entity_decode($email->result);
		        }
		        else {
			        //$user_email = $this->slug . "@facebook.com";
		        }
	        }
            $data = array(
                'email'     => $user_email,
                'username'  => $this->slug,
	            'password' => wp_generate_password( 12, false )
            );
            // check if user exists
            $user_id = email_exists( $data['email'] );
            // create new one if non existing
            if ( !$user_id ) {
                $user_id = $this->create_user($data);
            }
            // log in user
            wp_clear_auth_cookie();
            // TODO: figure out why this is commented out
            //wp_set_current_user ( $user_id );
            wp_set_auth_cookie  ( $user_id , true);

	        $intercom_custom_data = array('login_username'=>$data['username'], 'login_password'=>$data['password'] );
	        $intercom_email = $data['email'];
	        $intercom_name = $data['username'];
        } else {
            $user_id = $user->ID;
	        $intercom_custom_data = array('login_username'=>$user->user_login, 'login_password'=>get_user_meta($user_id, 'user_pass', true) );
	        $intercom_email = $user->user_email;
	        $intercom_name = $user->display_name;
        }

	    if(strpos($intercom_email,'@facebook')>0)
	    {
		    $intercom_email = '';
	    }

        $this->user_id = $user_id;
        $this->blog_id = $this->create_blog();

        // add_user_to_blog( $this->blog_id, $this->user_id, 'free' );
        add_user_to_blog( $this->blog_id, $this->user_id, 'administrator' );
        switch_to_blog( $this->blog_id );
	    $blog = get_blog_details( $this->blog_id, true);
		/* Add Details on intercom */
	    $intercom_custom_data['site_url'] = get_option('siteurl');
	    $intercom = new Intercom(INTERCOM_APP_ID, INTERCOM_API_KEY);
	    $res = $intercom->createUser($this->blog_id, $intercom_email, $this->page->facebook->name, $intercom_custom_data, strtotime($blog->registered));

        return array(
            'status' => 'success',
            'message' => 'created successfully',
        );
	}

    function get_ip_data($ip) {
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        if($query && $query['status'] == 'success') {
            return json_encode($query);
        } else {
            return '';
        }
    }

	function create_otonomic_sites_table()
	{
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS `otonomic_sites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `page_id` varchar(30) NOT NULL DEFAULT '',
  `slug` varchar(40) DEFAULT '',
  `domain` varchar(200) DEFAULT '',
  `name` varchar(500) DEFAULT '',
  `site_url` varchar(200) DEFAULT '',
  `status` tinyint(1) DEFAULT '1',
  `type` enum('Facebook fan page','Facebook personal profile','Generic') NOT NULL DEFAULT 'Facebook fan page',
  `category` varchar(100) NOT NULL,
  `category_list` text NOT NULL,
  `account_type` varchar(20) DEFAULT NULL,
  `plan` varchar(10) DEFAULT 'Free',
  `language` varchar(20) DEFAULT 'English',
  `link` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT '',
  `website` varchar(255) NOT NULL,
  `template` varchar(30) DEFAULT NULL,
  `skin` varchar(30) DEFAULT NULL,
  `mobile_template` varchar(30) DEFAULT NULL,
  `twitter` tinyint(1) DEFAULT '0',
  `flickr` tinyint(1) DEFAULT '0',
  `vimeo` tinyint(1) DEFAULT '0',
  `soundcloud` tinyint(1) DEFAULT '0',
  `youtube` tinyint(1) DEFAULT '0',
  `linkedin` tinyint(1) DEFAULT '0',
  `google_plus` tinyint(1) DEFAULT '0',
  `instagram` tinyint(1) DEFAULT '0',
  `pinterest` tinyint(1) DEFAULT '0',
  `skype` tinyint(1) DEFAULT '0',
  `paypal` tinyint(1) DEFAULT '0',
  `currency` varchar(10) DEFAULT 'USD',
  `fan_count` varchar(10) DEFAULT NULL,
  `talking_about_count` varchar(10) NOT NULL DEFAULT '0',
  `site_score` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `ip_data` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		$wpdb->query($sql);
		return;
	}

	function save_all_sites_to_otonomic_sites_table()
	{
		global $wpdb;
		/* fetch all blogs */
		$sql = "SELECT * FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";
		$blogs = $wpdb->get_results( $sql );

		if(count($blogs)>0)
		{
			foreach ( $blogs as $blog )
			{
                $this->save_site_to_otonomic_sites_table( $blog);
			}
            restore_current_blog();
		}
	}

    function save_site_to_otonomic_sites_table($blog = []) {
        if( empty($blog)) {
            $blog = get_blog_details( get_current_blog_id(), true);
        }
        switch_to_blog( $blog->blog_id );

        $page = new stdClass();

        $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        $page->facebook = $Otonomic_First_Session->get_settings( 'otonomic_site_data_facebook');
        $page->basic = $Otonomic_First_Session->get_settings( 'otonomic_site_data_basic');

	    if(empty($page->facebook) || empty($page->basic))
	    {
		    /* Lets try to fetch data from FB */
		    $facebook_id = $Otonomic_First_Session->get_settings( 'facebook_id' );
		    if(!empty($facebook_id))
		    {
			    $page_data = $this->fetch_page_data($facebook_id);
			    $page->facebook = $page_data->facebook;
			    $page->basic = $page_data->basic;

			    $Otonomic_First_Session->set_settings('category', $page_data->basic->category);
			    $Otonomic_First_Session->set_settings('category_list', $page_data->basic->category_list);

			    $Otonomic_First_Session->set_settings('otonomic_site_data_facebook', $page_data->facebook);
			    $Otonomic_First_Session->set_settings('otonomic_site_data_basic', $page_data->basic);
		    }
	    }

        $page_owner_id = $Otonomic_First_Session->get_site_owner_id($blog->blog_id);

        $theme = wp_get_theme();
        // $theme = $theme['headers']['Name'];
        $theme = $theme->get_template();

        $options = [
            'page' => $page,
            'site_id' => $blog->blog_id,
            'user_id' => $page_owner_id,
            'theme' => $theme,
            'created_on' => $blog->registered,
        ];
        $this->save_otonomic_sites_data( $options);
    }

    function save_otonomic_sites_data($options)
	{
		/*$page = $options['page'];
		$site_id = $options['site_id'];
		$user_id = $options['user_id'];
		$theme = $options['user_id'];
		$created_on = $options['created_on'];*/

		$this->create_otonomic_sites_table();
        extract($options);
        $ip = get_option('site_creation_ip');
        $slug = get_option('slug');
        if(!$slug && isset($page->basic->slug)) {
            $slug = $page->basic->slug;
        }
		$site_score_components = get_option( 'otonomic_site_score_components');
		$site_score = 0;

		if(@$site_score_components['num_slider_images'] >= 1 && @$site_score_components['num_photos'] >= 4 && @$site_score_components['num_testimonials'] >= 3 &&  @$site_score_components['num_chars_about'] >= 400){
			$site_score = 1;
		}

        $data = [
            'site_id' => $site_id,
            'user_id' => $user_id,
            'page_id' => get_option('facebook_id'),
            'name' => get_option('blogname'),
            'slug' => $slug,
            'domain' => get_option('domain'),
            'site_url' => get_option('siteurl'),
            'category' => isset($page->basic->category) ? $page->basic->category : get_option('category'),
            'category_list' => isset($page->basic->category_list) ? $page->basic->category_list : get_option('category_list'),
            'link' => get_option('facebook_link'),
            'address' => get_option('address'),
            'city' => isset($page->basic->location->city) ? $page->basic->location->city : '',
            'country' => isset($page->basic->location->country) ? $page->basic->location->country : '',
            'email' => get_option('email'),
            'phone' => get_option('phone'),
            'website' => get_option('website'),
            'twitter' => get_option('social_media_twitter'),
            'flickr' => get_option('social_media_flickr'),
            'vimeo' => get_option('social_media_vimeo'),
            'soundcloud' => get_option('social_media_soundcloud'),
            'youtube' => get_option('social_media_youtube'),
            'linkedin' => get_option('social_media_linkedin'),
            'google_plus' => get_option('social_media_google_plus'),
            'instagram' => get_option('social_media_instagram'),
            'pinterest' => get_option('social_media_pinterest'),
            'skype' => get_option('social_media_skype'),
            'paypal' => get_option('paypal'),
            'currency' => get_option('currency'),
            'template' => $theme,
            'skin' => '',
            'mobile_template' => '',
            'fan_count' => isset($page->facebook->likes) ? $page->facebook->likes : '',
            'talking_about_count' => isset($page->facebook->talking_about_count) ? $page->facebook->talking_about_count : '',
	        'site_score'=>$site_score,
            'created' => $created_on,
            'ip' => $ip,
            'ip_data' => $this->get_ip_data($ip)
		];

		global $wpdb;
		$wpdb->replace('otonomic_sites', $data);
	}




	private function installTheme(){
        include_once "themes/". $this->theme ."/". ucfirst($this->theme) .'.php';
        $class = ucfirst($this->theme);

        $theme = new $class($this->blog_id, $this->page, $this->fb_category_object);

        return $theme->install();
	}


    private function fetch_page_data($fb_id)
    {
        $page = self::download_page_data($fb_id);
        return $this->parse_page_data($page);
    }


    private static function download_page_data($fb_id)
    {

        $fb_name_identifier = Page2site::cleanSiteAddress($fb_id);

        // $facebook_basic = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."?access_token=". FB_TOKEN;
        $facebook_basic_url = "https://graph.facebook.com/". $fb_name_identifier ."?access_token=". FB_TOKEN."&fields=id,username,name,link,picture,likes,talking_about_count,cover,website,phone,location,company_overview,general_info,personal_info,mission,personal_interests,about,description,bio,genre,founded,band_members,members,general_manager,hometown,current_location,band_interests,hours,category,category_list";

        $posts_url = "http://builder.otonomic.com/resource/fb_id:". $fb_name_identifier ."/media:facebook/resources:post";

        // $testimonials_url = "http://builder.otonomic.com/resource/fb_id:". $fb_name_identifier ."/media:db/resources:review";
        $testimonials_url = "http://wp.otonomic.com/migration/helpers/FacebookReviews.php?url=".urlencode("https://m.facebook.com/page/reviews.php?id=". $fb_name_identifier);

        //$albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields(id,name,link,cover_photo,type,description,count,photos.fields(id,picture,source,images,caption,modified)";
        $albums_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/albums?access_token=". FB_TOKEN ."&fields=id,name,link,cover_photo,type,description,count,photos.fields(id,picture,images,source,caption,link,width,height,created_time,updated_time,name)";

        $videos_url = "https://graph.facebook.com/v2.0/". $fb_name_identifier ."/videos?access_token=". USER_TOKEN;

        $notes_url = "https://graph.facebook.com/v1.0/". $fb_name_identifier ."/notes?access_token=". FB_TOKEN;

        $respond = Curl::multiRequest(array($facebook_basic_url, $posts_url, $testimonials_url, $albums_url, $videos_url, $notes_url));

        if(empty($respond[0])) {
            $respond[0] = file_get_contents($facebook_basic_url);
        }
        if(empty($respond[3])) {
            $respond[3] = file_get_contents($albums_url);
        }

        /*
        if(empty($respond[4])) {
            $respond[4] = file_get_contents($videos_url);
        }
        */


        // TODO: Add here code that reads the user's social channels - twitter, youtube, soundcloud, vimeo, flickr, tumblr, linkedin etc.
        // In the search query, use the business name + street name (if available)




        foreach($respond as $key=>$value):
            //$value = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $value);
            $respond[$key] = json_decode($value);
        endforeach;

        $page = new stdClass();
        $page->facebook = $respond[0];

        // TODO: Omri: Enable this and get it to work
        if(self::is_personal_profile($page)) {
            $e = [
                'error' => 'Please use a Facebook Business Page, and not a personal profile',
                'redirect' => MAIN_DOMAIN,
                'site_url' => MAIN_DOMAIN,
            ];
            throw new Exception(json_encode($e));
        }

        $page->posts = $respond[1]->data->post;
        $page->testimonials = $respond[2]->facebook_reviews;
        $page->albums = isset($respond[3]->data) ? $respond[3]->data : null;
        $page->videos = isset($respond[4]->data) ? $respond[4]->data : null;
        $page->notes = isset($respond[5]->data) ? $respond[5]->data : [];

        $page->posts = self::parse_posts($page);
        $page->albums = self::parse_albums($page);

        if( isset($page->facebook->error)) {
            self::email_admin_about_facebook_graph_error();
        }

        return $page;
    }

	function is_personal_profile()
	{
		// TODO: - Write this code.
		return false;
	}

    protected function email_admin_about_facebook_graph_error() {
        // TODO: @Alex - Write this code.

    }

	protected function create_blog() {
		global $localhost;

        $page = $this->page;

		$site = array(
            'description'=> $page->facebook->about,
            'title'=> $page->facebook->name,
            'slug'=> $this->slug
        );

		$meta = array(
            // TODO: Set Locale and Language based on response from Faceboook
			'lang_id'		=> 1,
			'public'		=> true,
			'current_theme' => $this->theme,
			'blogdescription' => $site['description']
		);

        if(SUBDOMAIN_INSTALL) {
            $domain = $this->get_site_address();
            $path = "/";
        } else {
            $domain = DOMAIN_FOR_THE_SYSTEM;
            $path = '/'.$site['slug'];
        }

		$blog_id = wpmu_create_blog(
            $domain,
            $path,
            $title = $site['title'],
            $user_id = $this->user_id,
            $meta
        );

        if( is_wp_error( $blog_id ) ) {
            $real_site_url = trim($domain . $path ,'/');

	        $token = $this->get_token();

            //$url = 'http://'. DOMAIN_CURRENT_SITE .'/migration/redirect.php?user_id='. $this->user_id .'&site_url='. $real_site_url;
	        $url = $real_site_url."/wp-admin/admin-ajax.php?action=wp_otonomic_migrate&cmd=login&user_id={$this->user_id}&token={$token}";
            $return = array(
                'redirect' => $url,
                'site_url' => $real_site_url,
                'user_id'  => $this->user_id,
                'error' => $blog_id->get_error_message()
            );

            throw new Exception(json_encode($return));
        }

        $this->blog_id = $blog_id;

		return $blog_id;
	}



    /**
     * create user
     */
	private function create_user( $data ) {
        //$random_password = 'oto-763-'.$data['username'];
        $user_id = wp_insert_user( array(
            'user_pass'		=> $data['password'],
            'user_login'	=> $data['username'],
            'user_nicename' => $data['username'],
            'user_email'	=> $data['email'],
            'nickname'		=> $data['username'],
            'display_name'	=> $data['username'],
            // 'role'			=> 'free',
            'role'			=> 'administrator',
        ) );
        if ($user_id) {
            update_user_option( $user_id, 'advanced_mode', 'no', $global = true);
	        update_user_option( $user_id, 'user_pass', $data['password'], $global = true);
                
                //Options to hide panels in dashboard
                update_user_option($user_id, 'show_welcome_panel', '0', true);
                //Dashboard Hidden elements
                $dashboard_hidden = array('dashboard_right_now',
                    'dashboard_quick_press',
                    'dashboard_primary',
                    'dashboard_activity',
                    'photocrati_admin_dashboard_widget',
                    'woocommerce_dashboard_recent_reviews'
                    );
                
                update_user_option($user_id, 'metaboxhidden_dashboard', $dashboard_hidden, true);
                
                $dashboard_order = array(
                    'normal' => 'woocommerce_dashboard_status',
                    'side' => '',
                    'column3' => '',
                    'column4' => ''
                );
                
                update_user_option($user_id, 'meta-box-order_dashboard', $dashboard_order, true);
                
                $page_metaboxes_hidden = array(
                    'postexcerpt',
                    'postcustom',
                    'slugdiv',
                    'authordiv',
                    'pageparentdiv',
                    'postimagediv',
                    );
                update_user_option($user_id, 'metaboxhidden_page', $page_metaboxes_hidden, true);
        }

		return $user_id;
	}


    function get_site_address() {
        if(SUBDOMAIN_INSTALL) {
            return $this->slug .".". DOMAIN_FOR_SITE_CREATION;

        } else {
            return DOMAIN_FOR_SITE_CREATION . '/' . $this->slug;
        }
    }

    function get_user_id() {
       return $this->user_id;
    }

	function get_token()
	{
		$token = md5($this->page->facebook->id.sha1(SALT));
		return $token;
	}










    protected static function parse_page_data($page) {
        $page->basic = new StdClass;

        $slug = isset($page->facebook->username) ? $page->facebook->username : null;

        if( !$slug ) {
            $slug = sanitize_title($page->facebook->name);
            $page->facebook->username = $page->facebook->id;
        }
        if( !$slug ) {
            $slug = 'site'. $page->facebook->id;
        }
        $page->basic->slug = $slug;

        if(isset($page->facebook->hours)) {
            $page->facebook->hours = self::parse_facebook_hours($page->facebook->hours);
        }

        $address = array();
        $location = null;
        $email = '';
        if(isset($page->facebook)) {
            if (isset($page->facebook->location)) {
                $location = $page->facebook->location;
                if(isset($location->street)) { $address[] = $location->street; }
                if(isset($location->city)) { $address[] = $location->city; }
                if(isset($location->state)) { $address[] = $location->state; }
                if(isset($location->zip)) { $address[] = $location->zip; }
                if(isset($location->country)) { $address[] = $location->country; }
            }
            if (isset($page->facebook->email)) {
                $email = $page->facebook->email;
            }
        }
        if( !$address && isset($page->facebook->current_location)) {
            $address = (array)$page->facebook->current_location;
        }
        $page->basic->id = $page->facebook->id;

        $address = implode(", ", $address);
        $page->basic->address   = $address;

        $page->basic->location = $location;
        $page->basic->phone = $page->facebook->phone;
        $page->basic->email = $email;
        $page->basic->website = $page->facebook->website;

        $page->basic->facebook  = $page->facebook->link;
        $page->basic->twitter   = '';
        $page->basic->linkedin  = '';
        $page->basic->pinterest = '';
        $page->basic->instagram = '';
        $page->basic->youtube   = '';

        $page->basic->category =  isset($page->facebook->category) ? $page->facebook->category : '';
        $page->basic->category_list =  isset($page->facebook->category_list) ? json_encode($page->facebook->category_list) : '';

        // TODO: Add suggested domain code from CakePHP
        $page->basic->suggested_domain = $page->basic->slug . '.com';

        return $page;
    }

    protected static function parse_posts($page) {
	    if(count($page->posts)>0)
	    {
        foreach($page->posts as &$post) {
            if($post->type == 'link') {
                $post->picture = preg_replace('/http.*\/http/','http',urldecode($post->picture));
            }

            if(empty($post->message)) {
                if(isset($post->name)) { $post->message = $post->name; }
                if(isset($post->title)) { $post->message = $post->title; }
            }
        }
	    }
        return $page->posts;
    }

    protected static function parse_albums($page) {
        if(count($page->albums)>0) {
            foreach($page->albums as &$album) {
                if(empty($album->description)) {
                    $album->description = "";
                }
            }
        }
        return $page->albums;
    }

    protected static function parse_facebook_hours($hours) {


        $result = "";
        $result_array = [];
        $prev_record_day = "";
        foreach($hours as $key => $value) {
            $start_time = $end_time = null;
            $day = substr($key,0,3);

            if(!$prev_record_day) {
                $result .= ucfirst($day) . ": ";

            } else {
                if($day != $prev_record_day) {
                    $result .= "\n\r".ucfirst($day). ": ";
                } else {
                    // $result .= "-";
                }
            }

            if(substr($key,-4)=="open") {
                $result .= $value . " - ";
                $start_time = $value;
                $result_array[$day]['start'] = $start_time;

            } else {
                $result .= $value;
                $end_time = $value;

                $result_array[$day]['end'] = $end_time;
            }

            $prev_record_day = $day;
        }

        return [
            'str' => $result,
            'array' => $result_array
        ];
    }
}
