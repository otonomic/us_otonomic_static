<?php
include_once '../wp-load.php';
include_once 'helpers/piwik.php';

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');

$bid = (int) $_GET['blog_id'];

if($bid == 0){
    echo "send blog_id via GET";
    exit;
}

switch_to_blog($bid);

run_activate_plugin('wp-piwik/wp-piwik.php');

$piwik = new Piwik();
$piwik->add_website('google','http://google.progang.com/');

$piwik->get_keywords(1);

exit;


//update_piwik_settings();

function run_activate_plugin( $plugin ) {
    $current = get_option( 'active_plugins' );
    $plugin = plugin_basename( trim( $plugin ) );

    if ( !in_array( $plugin, $current ) ) {
        $current[] = $plugin;
        sort( $current );
        do_action( 'activate_plugin', trim( $plugin ) );
        update_option( 'active_plugins', $current );
        do_action( 'activate_' . trim( $plugin ) );
        do_action( 'activated_plugin', trim( $plugin) );
    }

    return null;
}

function update_piwik_settings(){

    $piwik_url = 'http://localhost/piwik';
    $piwik_token = '4d1ff0386c1933bcb68ad517a6573d1e';

    //update_option('wp-piwik_global-settings', '')

    $data = get_option('wp-piwik_global-settings');

    $data['piwik_url'] = $piwik_url;
    $data['piwik_token'] = $piwik_token;

    update_option('wp-piwik_global-settings', $data);

    $data = get_option('wp-piwik_global-settings');
    debug($data);
}

function debug($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

echo "<br>ok";