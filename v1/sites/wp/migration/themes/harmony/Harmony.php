<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Harmony extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'harmony';
    }
    
    function add_module_plugin_booking()
    {
        global $wpdb;
        $post_content = '[bpscheduler_booking_form]';

        $my_post = array(
            'post_title'    => 'Book an Appointment',
            'post_name'     => 'booking',
            'post_content'  => $post_content,
            'post_status'   => 'publish',
            'post_type' 	  => 'section',
            'menu_order'    => 65,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );

        $post_id = wp_insert_post( $my_post );

        $wpdb->query( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_id ."',25)");

        add_post_meta( $post_id, 'background_image', '/wp-content/plugins/otonomic-templates/img/booking.jpg');
        add_post_meta( $post_id, 'background_repeat', 'fullcover' );


        // add to menu
        $args = array(
            'post_type' => 'nav_menu_item',
            'post_title' => 'Booking',
            'name' => 'booking',
        );

        $post_type_query = new \WP_Query( $args );

        update_post_meta($post_type_query->post->ID, '_menu_item_url', '#booking');
        update_post_meta($post_type_query->post->ID, '_menu_item_type', 'custom');
        update_post_meta($post_type_query->post->ID, '_menu_item_menu_item_parent', '0');
        update_post_meta($post_type_query->post->ID, '_menu_item_object_id', $post_type_query->post->ID);
        update_post_meta($post_type_query->post->ID, '_menu_item_object', 'custom');

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_type_query->post->ID ."',43)");
    }


    public function save_theme_data(){
//        $this->set_text_widgets_sidebar();
        $this->set_text_widgets_footer();
        $this->update_options();
        $this->add_posts_galleries();
        $this->set_menu();
    }

    protected function set_text_widgets_footer(){
        parent::set_text_widgets_footer();
        $widget_location = array(
            'wp_inactive_widgets' => array(),
            'sidebar-1' => array('text-2','text-3'),
            'sidebar-2' => array('text-0'),
            'sidebar-3' => array('text-4'),
            'sidebar-4' => array('text-3'),
            'array_version' => 3
        );
        update_option('sidebars_widgets',$widget_location);
    }

    protected function set_text_widgets_sidebar(){
        parent::set_text_widgets_sidebar();
    }
    
    protected function add_posts_galleries(){
        if(!is_array($this->page->albums) || count($this->page->albums) == 0)
            return;

        $this->wpdb_disable_autocommit();

        foreach($this->page->albums as $key=>$album)
        {
	        try
	        {
                    
                    $album_photos = $album->photos->data;

                    if(empty($album_photos)) { return; }

                    $my_album = array(
                        'post_title'     => $album->name,
                        'post_name'   => wp_unique_post_slug( sanitize_title( trim_closest_word($album->name, 50, false) ) ),
                        'post_content'   => $album->description,
                        'post_status'    => 'publish',
                        'comment_status' => 'closed',
                        'ping_status' 	 => 'closed',
                        'post_type' 	 => 'gallery',
                        'guid'           => $album->id
                    );

                    $post_date = $this->parse_date($album->created_time);
                    $my_album['post_date'] = $post_date;
                    $my_album['post_date_gmt'] = $post_date;
                    $my_album['post_modified'] = $post_date;
                    $my_album['post_modified_gmt'] = $post_date;
                    
                    $post_id = otonomic_insert_post($my_album);
                    
                    $image_ids = array();

                    foreach((array)$album_photos as $key=>$picture)
                    {
                            try
                            {
                                $image_id = $this->add_image($picture, $post_id, $download_image);
                                $image_ids[] = $image_id;
                        }
                            catch(Exception $e)
                            {
                                    /* Silently ignore the error */
                            }
                    }
                    $album_time = strtotime($album->created_time);
                    add_post_meta($post_id, '_et_used_images', $image_ids);
                    add_post_meta($post_id, '_et_gallery_date', $album_time);
                    add_post_meta($post_id, '_thumbnail_id', $image_ids[0]);
                    
                    if(strtolower($album->name) == 'cover photos' && count($image_ids) > 0)
	            {
                       $header_bg_img = wp_get_attachment_image_src($image_ids[0],'full');
                       $this->set_header_image($header_bg_img[0]);
                       $random_media_bg = array_rand($image_ids);
                       $media_bg_img = wp_get_attachment_image_src($image_ids[$random_media_bg],'full');
                       $this->set_media_gallery_bg($media_bg_img[0]);
                    }
                    
            }
	        catch(Exception $e)
	        {
		        /* Silently ignore the error */
	        }
        }

        $this->wpdb_enable_autocommit();
    }
    
    protected function set_header_image($url){
        $theme_options = get_option('et_harmony');
        $theme_options['harmony_header_bg_image'] = $url;
        update_option('et_harmony', $theme_options);
    }
    
    protected function set_media_gallery_bg($url){
        $theme_options = get_option('et_harmony');
        $theme_options['harmony_media_bg_image'] = $url;
        update_option('et_harmony', $theme_options);
    }


    protected function update_options()
    {
        try {
            parent::update_options();
            
            update_option('stylesheet', 'harmony-child');
            update_option('current_theme', 'harmony child');
            
            $custom_css = "div.image a img {
                            height: 150px;
                            }
                            div.et_pt_portfolio_image img {
                                height: 280px;
                            }
                            div.et_pt_thumb img{
                                height: 100%;
                            }
                            ";
            
            $theme_options = get_option('et_harmony');
            $theme_options['harmony_custom_css'] = $custom_css;
            update_option('et_harmony', $theme_options);

        } catch(Exception $e) {
            /* Silently ignore the error */
        }
    }
}
