<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 7),
(5, 5, 'portfolio-category', '', 0, 0),
(6, 6, 'testimonial-category', '', 0, 0),
(7, 7, 'testimonial-category', '', 0, 0),
(8, 8, 'highlight-category', '', 0, 0),
(9, 1, 'portfolio-category', '', 0, 0),
(10, 9, 'category', '', 0, 0),
(11, 10, 'post_tag', '', 0, 0),
(12,11,'category','',0,0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Blog', 'blog', 0),
(3, 'Image', 'image', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Main Portfolio', 'main-portfolio', 0),
(6, 'Testimonials', 'testimonials', 0),
(7, 'Team','team', 0),
(8, 'Services','services', 0),
(9, 'Facebook Post','facebook-post', 0),
(10, 'Facebook','facebook', 0),
(11,'home slider','home-slider',0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0);
E;






if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US');
E;






if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('woocommerce_permalinks', 'false', 'yes'),
('et_harmony','a:94:{s:12:"harmony_logo";s:0:"";s:15:"harmony_favicon";s:0:"";s:23:"harmony_header_bg_image";s:0:"";s:22:"harmony_songs_bg_image";s:0:"";s:22:"harmony_media_bg_image";s:0:"";s:18:"harmony_blog_style";s:5:"false";s:18:"harmony_grab_image";s:5:"false";s:25:"harmony_show_twitter_icon";s:2:"on";s:21:"harmony_show_rss_icon";s:2:"on";s:26:"harmony_show_facebook_icon";s:2:"on";s:28:"harmony_show_soundcloud_icon";s:2:"on";s:19:"harmony_twitter_url";s:0:"";s:15:"harmony_rss_url";s:0:"";s:20:"harmony_facebook_url";s:0:"";s:22:"harmony_soundcloud_url";s:0:"";s:28:"harmony_shop_pages_fullwidth";s:2:"on";s:31:"harmony_product_pages_fullwidth";s:5:"false";s:20:"harmony_catnum_posts";i:6;s:24:"harmony_archivenum_posts";i:5;s:23:"harmony_searchnum_posts";i:5;s:20:"harmony_tagnum_posts";i:5;s:19:"harmony_date_format";s:6:"M j, Y";s:19:"harmony_use_excerpt";s:5:"false";s:29:"harmony_responsive_shortcodes";s:2:"on";s:36:"harmony_gf_enable_all_character_sets";s:5:"false";s:18:"harmony_custom_css";s:0:"";s:28:"harmony_display_news_section";s:2:"on";s:29:"harmony_display_shows_section";s:2:"on";s:29:"harmony_display_audio_section";s:2:"on";s:28:"harmony_display_shop_section";s:2:"on";s:31:"harmony_display_gallery_section";s:2:"on";s:22:"harmony_more_news_link";s:0:"";s:22:"harmony_homepage_posts";i:3;s:26:"harmony_home_events_number";i:3;s:22:"harmony_audio_postsnum";i:4;s:34:"harmony_homepage_products_per_page";i:8;s:27:"harmony_home_gallery_number";i:8;s:25:"harmony_recent_news_title";s:0:"";s:28:"harmony_upcoming_shows_title";s:0:"";s:28:"harmony_featured_songs_title";s:0:"";s:26:"harmony_shop_section_title";s:0:"";s:35:"harmony_media_gallery_section_title";s:0:"";s:24:"harmony_enable_dropdowns";s:2:"on";s:17:"harmony_home_link";s:2:"on";s:18:"harmony_sort_pages";s:10:"post_title";s:18:"harmony_order_page";s:3:"asc";s:25:"harmony_tiers_shown_pages";i:3;s:35:"harmony_enable_dropdowns_categories";s:2:"on";s:24:"harmony_categories_empty";s:2:"on";s:30:"harmony_tiers_shown_categories";i:3;s:16:"harmony_sort_cat";s:4:"name";s:17:"harmony_order_cat";s:3:"asc";s:23:"harmony_disable_toptier";s:5:"false";s:17:"harmony_postinfo2";a:2:{i:0;s:6:"author";i:1;s:4:"date";}s:18:"harmony_thumbnails";s:2:"on";s:25:"harmony_show_postcomments";s:2:"on";s:23:"harmony_page_thumbnails";s:5:"false";s:26:"harmony_show_pagescomments";s:5:"false";s:17:"harmony_postinfo1";a:2:{i:0;s:6:"author";i:1;s:4:"date";}s:24:"harmony_thumbnails_index";s:2:"on";s:22:"harmony_seo_home_title";s:5:"false";s:28:"harmony_seo_home_description";s:5:"false";s:25:"harmony_seo_home_keywords";s:5:"false";s:26:"harmony_seo_home_canonical";s:5:"false";s:26:"harmony_seo_home_titletext";s:0:"";s:32:"harmony_seo_home_descriptiontext";s:0:"";s:29:"harmony_seo_home_keywordstext";s:0:"";s:21:"harmony_seo_home_type";s:27:"BlogName | Blog description";s:25:"harmony_seo_home_separate";s:3:" | ";s:24:"harmony_seo_single_title";s:5:"false";s:30:"harmony_seo_single_description";s:5:"false";s:27:"harmony_seo_single_keywords";s:5:"false";s:28:"harmony_seo_single_canonical";s:5:"false";s:30:"harmony_seo_single_field_title";s:9:"seo_title";s:36:"harmony_seo_single_field_description";s:15:"seo_description";s:33:"harmony_seo_single_field_keywords";s:12:"seo_keywords";s:23:"harmony_seo_single_type";s:21:"Post title | BlogName";s:27:"harmony_seo_single_separate";s:3:" | ";s:27:"harmony_seo_index_canonical";s:5:"false";s:29:"harmony_seo_index_description";s:5:"false";s:22:"harmony_seo_index_type";s:24:"Category name | BlogName";s:26:"harmony_seo_index_separate";s:3:" | ";s:31:"harmony_integrate_header_enable";s:2:"on";s:29:"harmony_integrate_body_enable";s:2:"on";s:34:"harmony_integrate_singletop_enable";s:2:"on";s:37:"harmony_integrate_singlebottom_enable";s:2:"on";s:24:"harmony_integration_head";s:0:"";s:24:"harmony_integration_body";s:0:"";s:30:"harmony_integration_single_top";s:0:"";s:33:"harmony_integration_single_bottom";s:0:"";s:18:"harmony_468_enable";s:5:"false";s:17:"harmony_468_image";s:0:"";s:15:"harmony_468_url";s:0:"";s:19:"harmony_468_adsense";s:0:"";}','yes');
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
