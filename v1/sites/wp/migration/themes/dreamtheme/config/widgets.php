<?php
$config = array(
    'widget_text' => array(
	    array(
		    'title' => 'See also',
		    'text' => '[pagelist]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Contact us',
		    'text' => '[otonomic_contact_details]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Our Address',
		    'text' => '[otonomic_map]',
		    'filter' => ''
	    ),
	    array(
		    'title' =>'Opening hours',
		    'text' => '[otonomic_option option="opening_hours" ]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Book Appointment',
		    'text' => '[dt_sc_button variation="black" type="without-icon" link="/reservation/" size="medium" target="_blank"]Reserve now![/dt_sc_button] ',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Get a Discount',
		    'text' => '<img src="http://placehold.it/260x152&amp;text=Image">',
		    'filter' => ""
	    ),
	    '_multiwidget'=>1
    ),
	'sidebars_widgets'=> array(
		'footer-sidebar-1'=>array('text-0'),
		'footer-sidebar-2' => array('text-1'),
		'footer-sidebar-3' => array('text-2'),
		'footer-sidebar-4' => array('text-3'),
		'display-everywhere-sidebar' => array('text-4', 'text-5'),
		'array_version' => 3
	),
);
return $config;
