<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 7),
(3, 3, 'portfolio-category', '', 0, 0),
(4, 4, 'testimonial-category', '', 0, 0),
(5, 5, 'testimonial-category', '', 0, 0),
(6, 6, 'highlight-category', '', 0, 0),
(7, 1, 'portfolio-category', '', 0, 0),
(8, 7, 'category', '', 0, 0),
(9, 8, 'post_tag', '', 0, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, 'Main Portfolio', 'main-portfolio', 0),
(4, 'Testimonials', 'testimonials', 0),
(5, 'Team','team', 0),
(6, 'Services','services', 0),
(7, 'Facebook Post','facebook-post', 0),
(8, 'Facebook','facebook', 0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0),
('2333', '1', '2014-05-08 12:32:20', '2014-05-08 12:32:20', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit \"Send\"]</p>', 'Contact form 1', '', 'publish', 'open', 'open', '', 'contact-form-1', '', '', '2014-05-08 12:32:20', '2014-05-08 12:32:20', '', '0', 'http://wp.test//?post_type=wpcf7_contact_form&p=2333', '0', 'wpcf7_contact_form', '', '0');
E;


if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';



$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
(18, '_menu_item_type', 'post_type'),
(18, '_menu_item_menu_item_parent', '0'),
(18, '_menu_item_object_id', '4'),
(18, '_menu_item_object', 'page'),
(18, '_menu_item_target', ''),
(18, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(18, '_menu_item_xfn', ''),
(18, '_menu_item_url', ''),
(19, '_menu_item_type', 'post_type'),
(19, '_menu_item_menu_item_parent', '0'),
(19, '_menu_item_object_id', '16'),
(19, '_menu_item_object', 'page'),
(19, '_menu_item_target', ''),
(19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(19, '_menu_item_xfn', ''),
(19, '_menu_item_url', ''),
(20, '_menu_item_type', 'post_type'),
(20, '_menu_item_menu_item_parent', '0'),
(20, '_menu_item_object_id', '14'),
(20, '_menu_item_object', 'page'),
(20, '_menu_item_target', ''),
(20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(20, '_menu_item_xfn', ''),
(20, '_menu_item_url', ''),
(21, '_menu_item_type', 'post_type'),
(21, '_menu_item_menu_item_parent', '0'),
(21, '_menu_item_object_id', '12'),
(21, '_menu_item_object', 'page'),
(21, '_menu_item_target', ''),
(21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(21, '_menu_item_xfn', ''),
(21, '_menu_item_url', ''),
(22, '_menu_item_type', 'post_type'),
(22, '_menu_item_menu_item_parent', '0'),
(22, '_menu_item_object_id', '10'),
(22, '_menu_item_object', 'page'),
(22, '_menu_item_target', ''),
(22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(22, '_menu_item_xfn', ''),
(22, '_menu_item_url', ''),
(23, '_menu_item_type', 'post_type'),
(23, '_menu_item_menu_item_parent', '0'),
(23, '_menu_item_object_id', '8'),
(23, '_menu_item_object', 'page'),
(23, '_menu_item_target', ''),
(23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(23, '_menu_item_xfn', ''),
(23, '_menu_item_url', ''),
(24, '_menu_item_type', 'post_type'),
(24, '_menu_item_menu_item_parent', '0'),
(24, '_menu_item_object_id', '6'),
(24, '_menu_item_object', 'page'),
(24, '_menu_item_target', ''),
(24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(24, '_menu_item_xfn', ''),
(24, '_menu_item_url', ''),

('5640', '2333', '_form', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit \"Send\"]</p>'),

('5641', '2333', '_mail', 'a:7:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:26:"[your-name] <[your-email]>";s:4:"body";s:86:"From: [your-name] <[your-email]>
Subject: [your-subject]

Message Body:
[your-message]";s:9:"recipient";s:0:"";s:18:"additional_headers";s:0:"";s:11:"attachments";s:0:"";s:8:"use_html";b:0;}'),

('5642', '2333', '_mail_2', 'a:8:{s:6:\"active\",b:0,s:7:\"subject\",s:14:\"[your-subject]\",s:6:\"sender\",s:26:\"[your-name] <[your-email]>\",s:4:\"body\",s:105:\"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\",s:9:\"recipient\",s:12:\"[your-email]\",s:18:\"additional_headers\",s:0:\"\",s:11:\"attachments\",s:0:\"\",s:8:\"use_html\",i:0,}'),

('5643', '2333', '_messages', 'a:21:{s:12:"mail_sent_ok";s:43:"Your message was sent successfully. Thanks.";s:12:"mail_sent_ng";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:16:"validation_error";s:74:"Validation errors occurred. Please confirm the fields and submit it again.";s:4:"spam";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:12:"accept_terms";s:35:"Please accept the terms to proceed.";s:16:"invalid_required";s:31:"Please fill the required field.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:14:"invalid_number";s:28:"Number format seems invalid.";s:16:"number_too_small";s:25:"This number is too small.";s:16:"number_too_large";s:25:"This number is too large.";s:13:"invalid_email";s:28:"Email address seems invalid.";s:11:"invalid_url";s:18:"URL seems invalid.";s:11:"invalid_tel";s:31:"Telephone number seems invalid.";s:23:"quiz_answer_not_correct";s:27:"Your answer is not correct.";s:12:"invalid_date";s:26:"Date format seems invalid.";s:14:"date_too_early";s:23:"This date is too early.";s:13:"date_too_late";s:22:"This date is too late.";s:13:"upload_failed";s:22:"Failed to upload file.";s:24:"upload_file_type_invalid";s:30:"This file type is not allowed.";s:21:"upload_file_too_large";s:23:"This file is too large.";s:23:"upload_failed_php_error";s:38:"Failed to upload file. Error occurred.";}'),

('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US'),

E;



if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin


('woocommerce_permalinks', 'false', 'yes');
E;



$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
