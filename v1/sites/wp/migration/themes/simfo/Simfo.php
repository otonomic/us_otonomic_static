<?php
include_once ABSPATH . "migration/core/ThemifyAbstract.php";

class Simfo extends ThemifyAbstract implements ThemeInterface, ThemePluginSupportInterface
{
	private $homepage_id;
    public function __construct($blog_id, $page, $fb_category_object)
	{
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = strtolower(__CLASS__);
    }

    public function install(){
        $this->init();
        $this->save_site_data();
		
        $this->save_content();
        $this->save_theme_data();
        $this->install_plugins();
        $this->finalize();

        return $this->blog_id;
    }
	
	public function install_plugins()
	{
		$this->setup_plugins();
	}
	
	
    public function init()
    {
        global $wpdb;
        $domain = DOMAIN_CURRENT_SITE;
        $blog_id = $this->blog_id;

        // TODO: Export updated first_data file with minimized database entries
        include_once "sql/first_data.php";
        $this->set_site_settings(); // May be we dont need this //
        // $this->save_taxonomies();
    }



    public function save_taxonomies() {
        // Add term (table wp_terms) "Home" (would be category for sections)
        // term_id, name, slug, term_group
        // 24	Home	home	0

        // Add term "Testimonials"
        // 33	Testimonials	testimonials	0

        // Add term taxonomy (to table terms_taxonomy)
        // 25	24	section-category		0	7
    }





	public function get_homepage_id()
	{
		return $this->homepage_id;
	}

    public function save_content(){
        $this->save_posts();
        $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        $this->save_albums();
		
	    $this->setup_modules();


        $slider_images = $this->get_images_for_slider();
        add_option('slider_images', $slider_images);


//        $this->get_main_images( $cover_photos_album_facebook_id);


    }

    function setup_modules()
    {
	    /* Create home page */
	    $this->add_page_home();
	    /* Create about us page */
	    $this->add_module_about();
	    /* Create service page */
	    //$this->add_module_services(); // Not needed so lets skip this section //
	    /* Create portfolio page */
	    $this->add_module_portfolio();
	    /* Create testimonials page */
	    $this->add_module_testimonials();
	    /* Create Blog page */
	    $this->add_module_blog();
	    /* Create Contact us page page */
	    $this->add_module_contact();
	    /* Create Booking page page */
	    $this->add_module_plugin_booking();
	    /* Create store page */
	    $this->add_module_store();
    }
	function add_module_about()
	{
		$this->add_page_about();
	}
	function add_module_services()
	{
		//$this->add_page_service(); // Not needed so lets skip this section //
	}
	function add_module_portfolio()
	{
		$this->add_page_portfolio();
	}
	function add_module_testimonials()
	{
		$this->add_page_testimonials();
    }
	function add_module_blog()
	{
		$this->add_page_blog();
	}
	function add_module_contact()
	{
		$this->add_page_contact();
	}
	function add_module_plugin_booking()
	{
		$this->add_page_booking();
	}
	function add_module_store()
	{
		$this->add_page_store();
	}

	
	function switch_to_theme($page_data)
	{
		
		/* Create home page */
		$this->add_page_home();
		/* Create about us page */
		$this->add_page_about_from_content( $page_data['about'] );
		/* Create portfolio page */
		$this->add_page_portfolio();
		/* Create testimonials page */
		$this->add_page_testimonials();
		/* Create Blog page */
		$this->add_page_blog();
		/* Create Contact us page */
		$this->add_page_contact();
		/* Create Booking page */
		$this->add_page_booking();
		
		$this->add_page_store();
		
		$this->save_theme_data();
		$this->install_plugins();
        $this->finalize();
		
	}

	function add_page_store()
	{
		//wp_otonomic_add_woocommerce_pages();
		$OtonomicStore = OtonomicStore::get_instance();
		$OtonomicStore->add_sample_product();
		$OtonomicStore->show_store_pages(false);
		$OtonomicStore->hide_store_pages();
	}

    function add_page_home() {
        $row = array();
        $row[] = $this->get_section_slider();
        //$row[] = $this->get_section_about();
		// $row[] = $this->get_section_services();
        $row[] = $this->get_section_portfolio();
        $row[] = $this->get_section_blog();
        $row[] = $this->get_section_testimonials();
        //$row[] = $this->get_section_store();





		$builder_data = serialize( $this->get_builder_data( $row ));
		
		/* Now save page content */
		
		$data_post = array(
            'post_title'   => 'Home',
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 10,
			'comment_status'=>'closed',
        );
		$data_postmeta = array(
            '_themify_builder_settings'	=> $builder_data,
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'sidebar-none',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
        );
		
		/* Now lets create the page */
        return $this->homepage_id = ot_insert_post( $data_post , $data_postmeta);
    }

    protected function add_page_about_from_content($content)
	{
		// Save content
        $data_post = array(
            'post_title'   => 'About',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 20,
			'comment_status'=>'closed',
        );
        $data_postmeta = array(
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'default',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

    protected function add_page_about() {
        $content = $this->get_content_about();
		return $page_id = $this->add_page_about_from_content($content);
    }

	protected function add_servicepage() 
	{
		// May be required to implement in later date //
		//(8, '_themify_builder_settings', 'a:1:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:4:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:74:"<h1 style=''text-align: center;''>We build easy to use WordPress themes</h1>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:5:"solid";s:16:"stroke_w_divider";s:1:"1";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"highlight";s:12:"mod_settings";a:9:{s:16:"layout_highlight";s:5:"grid3";s:18:"category_highlight";s:17:"services|multiple";s:23:"post_per_page_highlight";s:1:"6";s:15:"order_highlight";s:4:"desc";s:17:"orderby_highlight";s:4:"date";s:17:"display_highlight";s:7:"content";s:19:"img_width_highlight";s:2:"80";s:20:"img_height_highlight";s:2:"80";s:23:"hide_page_nav_highlight";s:3:"yes";}}i:3;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:5:"solid";s:16:"stroke_w_divider";s:1:"0";s:13:"color_divider";s:11:"transparent";s:18:"top_margin_divider";s:2:"50";s:21:"bottom_margin_divider";s:2:"50";}}}}}}}'),

		$data_post = array(
            'post_title'   => 'Services',
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 35,
			'comment_status'=>'closed',
        );
        $data_postmeta = array(
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'default',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

    protected function add_page_portfolio()
	{
		$row = array();
		/* First row */
		$row[0] = new stdClass;
		$row[0]->row_order = 0;
		$row[0]->cols[0] = new stdClass;
		$row[0]->cols[0]->grid_class = 'col-full first';
		$row[0]->cols[0]->modules[0]= array(
			'mod_name' => 'portfolio',
			'mod_settings' => array(
				'layout_portfolio' => 'grid3',
				'category_portfolio' => '0|multiple',
				'order_portfolio' => 'desc',
				'orderby_portfolio' => 'date',
				'display_portfolio' => 'content',
				'hide_page_nav_portfolio' => 'yes',
				'font_family' => 'default',
			)
		);
		
		$builder_data = $this->get_builder_data( $row );
		$builder_data = serialize($builder_data);
		
		$data_post = array(
            'post_title'   => 'Portfolio',
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 30,
			'comment_status'=>'closed',
        );
		
        $data_postmeta = array(
			'_themify_builder_settings'	=> $builder_data,
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'sidebar-none',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

    protected function add_page_testimonials()
	{
		$row = array();
		/* First row */
		$row[0] = new stdClass;
		$row[0]->row_order = 0;
		$row[0]->cols[0] = new stdClass;
		$row[0]->cols[0]->grid_class = 'col-full first last';
		$row[0]->cols[0]->modules[0]= array(
			'mod_name' => 'testimonial',
			'mod_settings' => array(
				'layout_testimonial' => 'grid2',
				'category_testimonial' => '|single',
				'order_testimonial' => 'desc',
				'orderby_testimonial' => 'date',
				'display_testimonial' => 'content',
				'hide_page_nav_testimonial' => 'yes',
				'font_family' => 'default',
			)
		);
		
		$builder_data = $this->get_builder_data( $row );
		$builder_data = serialize($builder_data);
		
		$data_post = array(
            'post_title'   => 'Testimonials',
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 50,
			'comment_status'=>'closed',
        );
		
        $data_postmeta = array(
			'_themify_builder_settings'	=> $builder_data,
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'default',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

    protected function add_page_blog()
	{
		$data_post = array(
            'post_title'   => 'Blog',
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 40,
			'comment_status'=>'closed',
        );
		
        $data_postmeta = array(
            'layout'        => 'list-large-image',
            'content_width'  => 'default_width',
            'page_layout' => 'sidebar1',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			'query_category'=>'0',
			'posts_per_page'=>5
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

    protected function add_page_contact()
	{
		$content = <<<E
            [otonomic_map  width='100%' height='450px' before='' after='' ]

            [col grid="3-2 first"]
                [contact-form-7 id="2333" title="Contact"]
            [/col]

            [col grid="3-1 last"]
            [otonomic_option option="address" before="<h3>Address</h3>" after=""]
            [otonomic_option option="phone" before="<h3>Phone</h3>" after=""]
            [otonomic_option option="email" before="<h3>Email</h3>" after=""]
            [otonomic_option option="opening_hours" before="<h3>Opening Hours</h3>" after=""]
            [/col]
E;
		$data_post = array(
            'post_title'   => 'Contact Us',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 80,
			'comment_status'=>'closed',
        );
		
        $data_postmeta = array(
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'default',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}
	
	function add_page_booking()
	{
		$content = <<<E
            [bpscheduler_booking_form]
E;
		$data_post = array(
            'post_title'   => 'Book an Appointment',
			'post_name'     => 'booking',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
			'comment_status'=>'closed',
			'menu_order'    => 70,
        );
		
        $data_postmeta = array(
            'layout'        => 'list-post',
            'content_width'  => 'default_width',
            'page_layout' => 'default',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
        );
        return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}
	
		

    public function save_theme_data(){
        $this->set_menu();
        // TODO: Omri: Understand how set_widgets work
        //$this->set_widgets();
        $this->set_themify_options();
        $this->update_options();
    }


    public function finalize(){

    }



    private function set_menu(){
        /*
         * TODO: Fix this function. It should be run as a callback function whenever:
         *    new post added/deleted
         *    new testimonial added/deleted
         *    youtube/twitter/flickr/social account updated
         */

/*
        global $wpdb;

        $posts = $this->page->posts;
        $testimonials = $this->page->testimonials;

        $categories = array('home','about','booking','portfolio','blog','team','social','youtube','contact');

        $q = 'UPDATE wp_'. $this->blog_id .'_posts SET post_status = "draft" where post_type="nav_menu_item"';
        $wpdb->query( $q );

        foreach($categories as $key=>$value){
            $post_status = 'publish';

            if($value == 'blog' && count($posts) == 0){
                $post_status = 'draft';
            }

            if($value == 'team' && count($testimonials) == 0){
                $post_status = 'draft';
            }

            $q = 'UPDATE wp_'. $this->blog_id .'_posts SET menu_order = '. $key .', post_status = "'. $post_status .'" where post_name ="'. $value .'" AND post_type="nav_menu_item"';
            $wpdb->query( $q );
        }
*/
    }


    protected function set_main_slider_images(array $image_ids){
        if(count($image_ids) == 0) { return; }


        if(is_array($image_ids)) { $image_ids = implode(',', $image_ids); }
        update_post_meta($this->get_homepage_id(), 'background_gallery', '[gallery ids="'. $image_ids .'"]');
    }



    private function set_widgets(){
        /*
        $reflector = new ReflectionClass(get_class($this));
        $path = dirname($reflector->getFileName());

        $widgets_list = include_once $path.'/config/widgets.php';

        foreach($widgets_list as $widget_location => $widget_name) {
            $widget = \Widgets\Core\Factory::make($widget_name);
            $widget->setBlogID($this->blog_id);
            $widget->setData($this->page);

            // TODO: Check how to add the widget to sidebar\footer location
            $widget->install($widget_location);
        }
        */
    }


    protected function update_options(){
        // TODO: Comment global here?
        global $wpdb;

        update_option( 'page_on_front', $this->get_homepage_id() );
        update_option( 'show_on_front', 'page' );

        update_option( 'template', $this->theme );
        update_option( 'stylesheet', $this->theme );
        update_option( 'current_theme', $this->theme );

        update_option( 'last_update', time() );
        update_option( 'facebook_id', $this->page->facebook->id );
    }


    public function set_themify_options(){
        include_once('config/themify_options.php');
        $data = get_themify_options();
        if ( get_option( 'themify_data' ) !== false ) {
            update_option( 'themify_data' , $data );
        } else {
            add_option( 'themify_data', $data, null, 'yes' );
        }

        switch_theme($this->theme,$this->theme);
    }





	
	
	protected function remove_search_box(){
        //'setting-exclude_search_form'
        $themify_data = get_option('themify_data');
        $themify_data['setting-exclude_search_form'] = 'on' ;

        update_option( 'themify_data' , $themify_data);
    }


	protected function add_sidebar_widgets(){
        $sidebar_widgets = array (
            'sidebar-1' => array ('search-2', 'recent-posts-2', 'recent-comments-2', 'archives-2', 'categories-2', 'meta-2'),
            'wp_inactive_widgets' =>  array (  ),
            'sidebar-main' => array ( 'text-2', 'themify-feature-posts-2', 'themify-twitter-2'),
            'social-widget' =>  array ('themify-social-links-2'),
            'footer-widget-1' =>  array (  ),
            'footer-widget-2' =>  array (  ),
            'footer-widget-3' =>  array (  ),
            'array_version' => 3,
        );
        update_option('sidebars_widgets', $sidebar_widgets);

        $themify_widget = array (2 =>  array ( 'title' => '','show_link_name' => NULL,'open_new_window' => NULL,'icon_size' => 'icon-medium','orientation' => 'horizontal',),'_multiwidget' => 1,);
        update_option('widget_themify-social-links', $themify_widget);

        $fburl = ($this->page->facebook->link);
        $widget_text = array (  2 =>
            array (
                'title' => 'Facebook',
                'text' => '[wp_otonomic_fb_like width="100%" height="290px"]',
                'filter' => false,
            ),
            '_multiwidget' => 1,
        );
        update_option('widget_text', $widget_text);

        $featured_posts = array (
            2 =>
                array (
                    'title' => 'Recent Posts',
                    'category' => '0',
                    'show_count' => '5',
                    'show_date' => NULL,
                    'show_thumb' => NULL,
                    'display' => 'none',
                    'hide_title' => NULL,
                    'thumb_width' => '50',
                    'thumb_height' => '50',
                    'excerpt_length' => '55',
                    'orderby' => 'date',
                    'order' => 'DESC',
                ),
            '_multiwidget' => 1,
        );
        update_option('widget_themify-feature-posts', $featured_posts);

        $twitter_widget = array (
            2 =>
                array (
                    'title' => 'Latest Tweets',
                    'username' => $this->page->twitter,
                    'show_count' => '5',
                    'hide_timestamp' => NULL,
                    'show_follow' => NULL,
                    'follow_text' => '→ Follow me',
                    'include_retweets' => NULL,
                    'exclude_replies' => NULL,
                ),
            '_multiwidget' => 1,
        );

        update_option('widget_themify-twitter', $twitter_widget);

        //echo 'sidebar widgets updated <br>';
    }

	
	

    protected function update_main_nav_location($menu_id, $theme_key = 'theme_mods_simfo'){
        $simfo_menu = get_option($theme_key);

        if(empty($simfo_menu)){
            add_option($theme_key, array('nav_menu_locations' => array('main-nav' => $menu_id, 'footer-nav' => 0)));
        } else {
            $simfo_menu['nav_menu_locations']['main-nav'] = $menu_id;
            update_option($theme_key, $simfo_menu);
        }

        //echo 'main navigatino updated <br>';
    }

    protected function set_social(){

        $options = get_blog_option($this->blog_id,'themify_data');

        if(!is_array($options)){
            $options = unserialize($options);

            if(!is_array($options)){
                //echo 'themify_data not found';
                return false;
            }
        }

        //clear default social account links
        for($ii=0; $ii<5; $ii++){
            $key = 'setting-link_link_themify-link-'.$ii;
            $options[$key] = '';
        }

        if(empty($options)){
            //echo 'themify data empty or not retrieved';
            return false;
        }
        $i = 0;
        if(!empty($this->page->facebook)){
            $options = $this->set_social_option($options, $i, 'Facebook', $this->page->facebook, 'facebook.png');
            $i++;
        }
        if(!empty($this->page->twitter)){
            $options = $this->set_social_option($options, $i, 'Twitter', 'http://twitter.com/' . $this->page->twitter, 'twitter.png');
            $i++;
        }
        if(!empty($this->page->instagram)){
            $options = $this->set_social_option($options, $i, 'Instagram', 'http://instagram.com/' . $this->page->instagram, 'instagram.png');
            $i++;
        }

        if(!empty($this->page->pinterest)){
            $options = $this->set_social_option($options, $i, 'Pinterest', 'http://pinterest.com/' . $this->page->pinterest, 'pinterest.png');
            $i++;
        }

        $result = update_blog_option($this->blog_id, 'themify_data', $options);

        //echo 'social links updated <br>';
    }

    protected function set_social_option($options, $index, $title_value, $url_value, $img_value){

        $img_value = 'http://themify.me/demo/themes/parallax/wp-content/themes/parallax/images/social/'.$img_value;

        $icon = 'setting-link_type_themify-link-' . $index;
        $title = 'setting-link_title_themify-link-' . $index;
        $url = 'setting-link_link_themify-link-' . $index;
        $img = 'setting-link_img_themify-link-' . $index;

        $options[$icon] = 'image-icon';
        $options[$title] = $title_value;
        $options[$url] = $url_value;
        $options[$img] = $img_value;

        return $options;
    }





    function get_section_services() {
        $row = new stdClass;
        $row->row_order = 1;
        $row->styling = array('custom_css_row' => 'themify_builder_row services');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';
        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<h2 style="text-align: center;">Services</h2>',
            )
        );
        $row->cols[0]->modules[1]= array(
            'mod_name' => 'divider',
            'mod_settings' => array(
                'style_divider' => 'double',
                'stroke_w_divider' => '4',
                'color_divider' => 'eeeeee',
                'top_margin_divider' => '10',
                'bottom_margin_divider' => '30',
            )
        );
        $row->cols[0]->modules[2]= array(
            'mod_name' => 'highlight',
            'mod_settings' => array(
                'layout_highlight' => 'grid3',
                'category_highlight' => 'services|multiple',
                'order_highlight' => 'desc',
                'orderby_highlight' => 'date',
                'display_highlight' => 'content',
                'img_width_highlight' => '68',
                'img_height_highlight' => '68',
                'hide_page_nav_highlight' => 'yes',
            )
        );
        return $row;
    }

    function get_section_portfolio() {
        $row = new stdClass;
        $row->row_order = 2;
        $row->styling = array('custom_css_row' => 'themify_builder_row portfolio');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';
        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<h2 style="text-align: center;">Portfolio</h2>',
            )
        );
        $row->cols[0]->modules[1]= array(
            'mod_name' => 'divider',
            'mod_settings' => array(
                'style_divider' => 'double',
                'stroke_w_divider' => '4',
                'color_divider' => 'eeeeee',
                'top_margin_divider' => '10',
                'bottom_margin_divider' => '30',
            )
        );
        $row->cols[0]->modules[2]= array(
            'mod_name' => 'portfolio',
            'mod_settings' => array(
                'layout_portfolio' => 'grid4',
                'category_portfolio' => '0|multiple',
                'post_per_page_portfolio' => '4',
                'order_portfolio' => 'desc',
                'orderby_portfolio' => 'date',
                'display_portfolio' => 'none',
                'img_width_portfolio' => '240',
                'img_height_portfolio' => '140',
                'hide_post_date_portfolio' => 'yes',
                'hide_post_meta_portfolio' => 'yes',
                'hide_page_nav_portfolio' => 'yes',
            )
        );

        return $row;
    }

    function get_section_testimonials() {
        $row = new stdClass;
        $row->row_order = 3;
        $row->styling = array('custom_css_row' => 'themify_builder_row testimonials');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';

        $row->styling = array(
            'row_width' => '',
            'animation_effect' => '',
            'background_image' => '',
            'background_color' => '',
            'background_repeat' => '',
            'font_family' => 'default',
            'font_color' => '',
            'font_size' => '',
            'font_size_unit' => '',
            'line_height' => '',
            'line_height_unit' => '',
            'link_color' => '',
            'text_decoration' => '',
            'padding_top' => '',
            'padding_right' => '',
            'padding_bottom' => '',
            'padding_left' => '',
            'margin_top' => '',
            'margin_right' => '',
            'margin_bottom' => '',
            'margin_left' => '',
            'border_top_color' => '',
            'border_top_width' => '',
            'border_top_style' => '',
            'border_right_color' => '',
            'border_right_width' => '',
            'border_right_style' => '',
            'border_bottom_color' => '',
            'border_bottom_width' => '',
            'border_bottom_style' => '',
            'border_left_color' => '',
            'border_left_width' => '',
            'border_left_style' => '',
            'custom_css_row' => 'themify-builder-row-testimonials show'
        );

        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<h2 style="text-align: center;">Testimonials</h2>',
            )
        );
        $row->cols[0]->modules[1]= array(
            'mod_name' => 'divider',
            'mod_settings' => array(
                'style_divider' => 'double',
                'stroke_w_divider' => '4',
                'color_divider' => 'eeeeee',
                'top_margin_divider' => '10',
                'bottom_margin_divider' => '30',
            )
        );
        $row->cols[0]->modules[2]= array(
            'mod_name' => 'testimonial',
            'mod_settings' => array(
                'layout_testimonial' => 'grid2',
                'category_testimonial' => '|single',
                'post_per_page_testimonial' => '4',
                'order_testimonial' => 'desc',
                'orderby_testimonial' => 'date',
                'display_testimonial' => 'content',
                'img_width_testimonial' => '80',
                'img_height_testimonial' => '80',
                'hide_page_nav_testimonial' => 'yes',
            )
        );

        return $row;
    }

    function get_section_slider() {
        $row = new stdClass;
        $row->row_order = 0;
        $row->styling = array('custom_css_row' => 'themify_builder_row slider');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';
        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<p>[otonomic_slider data=""]</p>',
            )
        );

        return $row;
    }

    function get_section_blog() {
        $row = new stdClass;
        $row->row_order = 4;
        $row->styling = array('custom_css_row' => 'themify_builder_row blog');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';

        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<h2 style="text-align: center;">Blog</h2>',
            )
        );
        $row->cols[0]->modules[1]= array(
            'mod_name' => 'divider',
            'mod_settings' => array(
                'style_divider' => 'double',
                'stroke_w_divider' => '4',
                'color_divider' => 'eeeeee',
                'top_margin_divider' => '10',
                'bottom_margin_divider' => '30',
            )
        );
        $row->cols[0]->modules[2]= array(
            'mod_name' => 'post',
            'mod_settings' => array(
                'layout_post' => 'grid2-thumb',
                'category_post' => '0|multiple',
                'post_per_page_post' => '4',
                'order_post' => 'desc',
                'orderby_post' => 'date',
                'display_post' => 'excerpt',
                'img_width_post' => '100',
                'img_height_post' => '100',
                'hide_page_nav_post' => 'yes',
                'font_family' => 'default',
            )
        );

        return $row;
    }

    function get_section_team() {
        $row = new stdClass;
        $row->row_order = 5;
        $row->styling = array('custom_css_row' => 'themify_builder_row team');

        $row->cols[0] = new stdClass;
        $row->cols[0]->grid_class = 'col-full first last';

        $row->cols[0]->modules[0]= array(
            'mod_name' => 'text',
            'mod_settings' => array(
                'content_text' => '<h2 style="text-align: center;">Our Team</h2>',
            )
        );
        $row->cols[0]->modules[1]= array(
            'mod_name' => 'divider',
            'mod_settings' => array(
                'style_divider' => 'double',
                'stroke_w_divider' => '4',
                'color_divider' => 'eeeeee',
                'top_margin_divider' => '10',
                'bottom_margin_divider' => '30',
            )
        );
        $row->cols[0]->modules[2]= array(
            'mod_name' => 'testimonial',
            'mod_settings' => array(
                'layout_testimonial' => 'grid4',
                'category_testimonial' => 'team|multiple',
                'post_per_page_testimonial' => '4',
                'order_testimonial' => 'desc',
                'orderby_testimonial' => 'date',
                'display_testimonial' => 'none',
                'img_width_testimonial' => '80',
                'img_height_testimonial' => '80',
                'hide_page_nav_testimonial' => 'yes',
            )
        );

        return $row;
    }
}
