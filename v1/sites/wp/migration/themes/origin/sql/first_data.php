<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 7),
(5, 5, 'portfolio-category', '', 0, 0),
(6, 6, 'testimonial-category', '', 0, 0),
(7, 7, 'testimonial-category', '', 0, 0),
(8, 8, 'highlight-category', '', 0, 0),
(9, 1, 'portfolio-category', '', 0, 0),
(10, 9, 'category', '', 0, 0),
(11, 10, 'post_tag', '', 0, 0),
(12,11,'category','',0,0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Blog', 'blog', 0),
(3, 'Image', 'image', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Main Portfolio', 'main-portfolio', 0),
(6, 'Testimonials', 'testimonials', 0),
(7, 'Team','team', 0),
(8, 'Services','services', 0),
(9, 'Facebook Post','facebook-post', 0),
(10, 'Facebook','facebook', 0),
(11,'home slider','home-slider',0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0);
E;






if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US');
E;






if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
('woocommerce_permalinks', 'false', 'yes'),
('et_origin', 'a:85:{s:11:"origin_logo";s:0:"";s:14:"origin_favicon";s:0:"";s:17:"origin_grab_image";s:5:"false";s:19:"origin_catnum_posts";i:6;s:23:"origin_archivenum_posts";i:5;s:22:"origin_searchnum_posts";i:5;s:19:"origin_tagnum_posts";i:5;s:18:"origin_date_format";s:6:"M j, Y";s:18:"origin_use_excerpt";s:5:"false";s:28:"origin_responsive_shortcodes";s:2:"on";s:35:"origin_gf_enable_all_character_sets";s:5:"false";s:17:"origin_custom_css";s:0:"";s:21:"origin_homepage_posts";i:16;s:23:"origin_enable_dropdowns";s:2:"on";s:16:"origin_home_link";s:2:"on";s:17:"origin_sort_pages";s:10:"post_title";s:17:"origin_order_page";s:3:"asc";s:24:"origin_tiers_shown_pages";i:3;s:34:"origin_enable_dropdowns_categories";s:2:"on";s:23:"origin_categories_empty";s:2:"on";s:29:"origin_tiers_shown_categories";i:3;s:15:"origin_sort_cat";s:4:"name";s:16:"origin_order_cat";s:3:"asc";s:22:"origin_disable_toptier";s:5:"false";s:16:"origin_postinfo2";a:4:{i:0;s:6:"author";i:1;s:4:"date";i:2;s:10:"categories";i:3;s:8:"comments";}s:17:"origin_thumbnails";s:2:"on";s:24:"origin_show_postcomments";s:2:"on";s:22:"origin_page_thumbnails";s:2:"on";s:25:"origin_show_pagescomments";s:5:"false";s:16:"origin_postinfo1";a:2:{i:0;s:6:"author";i:1;s:4:"date";}s:20:"origin_custom_colors";s:5:"false";s:16:"origin_child_css";s:5:"false";s:19:"origin_child_cssurl";s:0:"";s:21:"origin_color_mainfont";s:0:"";s:21:"origin_color_mainlink";s:0:"";s:21:"origin_color_pagelink";s:0:"";s:28:"origin_color_pagelink_active";s:0:"";s:21:"origin_color_headings";s:0:"";s:26:"origin_color_sidebar_links";s:6:"452745";s:18:"origin_footer_text";s:0:"";s:24:"origin_color_footerlinks";s:0:"";s:21:"origin_seo_home_title";s:5:"false";s:27:"origin_seo_home_description";s:5:"false";s:24:"origin_seo_home_keywords";s:5:"false";s:25:"origin_seo_home_canonical";s:5:"false";s:25:"origin_seo_home_titletext";s:0:"";s:31:"origin_seo_home_descriptiontext";s:0:"";s:28:"origin_seo_home_keywordstext";s:0:"";s:20:"origin_seo_home_type";s:27:"BlogName | Blog description";s:24:"origin_seo_home_separate";s:3:" | ";s:23:"origin_seo_single_title";s:5:"false";s:29:"origin_seo_single_description";s:5:"false";s:26:"origin_seo_single_keywords";s:5:"false";s:27:"origin_seo_single_canonical";s:5:"false";s:29:"origin_seo_single_field_title";s:9:"seo_title";s:35:"origin_seo_single_field_description";s:15:"seo_description";s:32:"origin_seo_single_field_keywords";s:12:"seo_keywords";s:22:"origin_seo_single_type";s:21:"Post title | BlogName";s:26:"origin_seo_single_separate";s:3:" | ";s:26:"origin_seo_index_canonical";s:5:"false";s:28:"origin_seo_index_description";s:5:"false";s:21:"origin_seo_index_type";s:24:"Category name | BlogName";s:25:"origin_seo_index_separate";s:3:" | ";s:30:"origin_integrate_header_enable";s:2:"on";s:28:"origin_integrate_body_enable";s:2:"on";s:33:"origin_integrate_singletop_enable";s:2:"on";s:36:"origin_integrate_singlebottom_enable";s:2:"on";s:23:"origin_integration_head";s:0:"";s:23:"origin_integration_body";s:0:"";s:29:"origin_integration_single_top";s:0:"";s:32:"origin_integration_single_bottom";s:0:"";s:17:"origin_468_enable";s:5:"false";s:16:"origin_468_image";s:0:"";s:14:"origin_468_url";s:0:"";s:18:"origin_468_adsense";s:0:"";s:16:"sidebar_bg_color";s:7:"#6ab3b2";s:21:"sidebar_borders_color";s:7:"#5EA5A4";s:22:"sidebar_active_link_bg";s:7:"#ffffff";s:24:"sidebar_dropdown_link_bg";s:7:"#f8f8f8";s:18:"sidebar_text_color";s:7:"#ffffff";s:19:"sidebar_links_color";s:7:"#ffffff";s:12:"heading_font";s:4:"none";s:9:"body_font";s:4:"none";s:13:"color_schemes";s:4:"gray";s:21:"origin_exlcats_recent";a:3:{i:0;i:2;i:1;i:3;i:2;i:1;}}','yes');
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
