<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Origin extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'origin';
    }

    public function install(){
        parent::install();

        /* Load the child theme now */
        if($this->theme == 'origin') {
			update_option('stylesheet', 'originchild');
            update_option('current_theme', 'Originchild');
        }

        return $this->blog_id;
    }

    /*
    protected function save_posts(){
        $posts = $this->page->posts;

        if(!is_array($posts) || count($posts) == 0)
            return;

        $this->wpdb_disable_autocommit();

        foreach($posts as $post){
            try
            {
                $this->add_post($post);
            }
            catch(Exception $e)
            {
            }
        }

        $this->wpdb_enable_autocommit();
    }


    protected function add_post($fbpost, $download_image=false){
        // We do not insert posts of type status
        if($fbpost->type == 'Status') { return; }

        $fbpost->message = make_clickable($fbpost->message);
        $my_post = array(
            'post_title'    => trim_closest_word($fbpost->message, 50, $append_ellipsis = true),
//             'post_name'   => wp_unique_post_slug( sanitize_title( trim_closest_word($fbpost->message, 50, false) ) ),
            'post_name'   => sanitize_title( trim_closest_word($fbpost->message, 50, false) ),
            'post_content'  => $fbpost->message,
            'post_status'   => 'publish',
            'post_type' 	=> 'post',
//            'filter'        => true
        );

        if($fbpost->created_time){
            $post_date = $this->parse_date($fbpost->created_time);
            $my_post['post_date'] = $post_date;
            $my_post['post_date_gmt'] = $post_date;
            $my_post['post_modified'] = $post_date;
            $my_post['post_modified_gmt'] = $post_date;
        }

        $post_id = otonomic_insert_post( $my_post );

        // TODO: Add taxonomy - video/link/
        // TODO: Available categories - video, link, image, review
        $term_taxonomy_ids = wp_set_object_terms( $post_id , array(2,3), 'category', false );
//        $setting_cats = wp_set_post_categories($post_id,1,FALSE);
        // adding the image to the post
        $image_id = $this->add_image($fbpost, $post_id, $download_image);

        $feature_image = $fbpost->picture;
        $this->set_featured_image($post_id, $feature_image);
        // add meta tags for this post
        $post_meta = [
            '_thumbnail_id'         => $image_id,
            'post_image'            => $fbpost->picture,
            '_post_image_attach_id' => $image_id,
        ];


        $result = otonomic_insert_postmeta($post_id, $post_meta);
    }
*/

    function set_featured_image($post_id, $image_path){
        $m_value = array('thumbnail' => $image_path);
        update_post_meta($post_id, '_et_origin_settings', $m_value);
    }

    function get_featured_image_settings_array($image_path) {
        return ['_et_origin_settings' =>
            ['thumbnail' => $image_path]
        ];
    }

    function set_pages_settings() {
        $featured_image = $this->set_main_site_slider();

        $data = [
            'about' => $this->get_featured_image_settings_array($featured_image),

            'contact' => $this->get_featured_image_settings_array($featured_image),

            'social' => $this->get_featured_image_settings_array($featured_image),

            'portfolio' => [
                '_wp_page_template' => 'page-gallery.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 1,
                    'et_ptemplate_gallerycats' => array(3),
                    'et_ptemplate_gallery_perpage' => 12,
                    'et_ptemplate_showtitle' => 0,
                    'et_ptemplate_showdesc' => 0,
                    'et_ptemplate_detect_portrait' => 1,
                    'et_ptemplate_imagesize' => 2
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ],

            'blog' => [
                '_wp_page_template' => 'page-blog.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 0,
                    'et_ptemplate_blogstyle' => 0,
                    'et_ptemplate_showthumb' => 0,
                    'et_ptemplate_gallerycats' => array(2),
                    'et_ptemplate_blog_perpage' => 6
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ]
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    public function save_theme_data(){
        $this->set_text_widgets_sidebar();

        //$this->set_text_widgets_footer();

        $this->update_options();

        $this->set_menu();
    }

    protected function set_text_widgets_sidebar(){
        //parent::set_text_widgets_sidebar();

        $text_widget = array(
            array(
                'title' => 'Contact',
                'text' => '<div id="origin_contact_sidebar" >[otonomic_contact]</div>',
                'filter' => ""
            ),
        );
        $text_widget['_multiwidget'] = 1;

        update_option('widget_text', $text_widget);

        $sidebar1 = array('text-0');

        $set_widgets = array(
            'wp_inactive_widgets' => array('archives-2','meta-2','search-2','categories-2','recent-posts-2'),
            'sidebar-1' => $sidebar1,
            //'sidebar-2' => array('text-4','text-2','text-1'),
            'array_version' => 3
        );
        update_option('sidebars_widgets',$set_widgets);
    }
}
