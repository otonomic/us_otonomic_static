<?php
$config = array(
    'widget_text' => array(
        array(
            'title' => 'Contact',
            'text' => '[otonomic_contact]',
            'filter' => ""
        ),
        '_multiwidget'=>1
    ),
    'sidebars_widgets'=> array(
        'sidebar-1'=>array('text-0'),
        'array_version' => 3
    ),
);
return $config;

