<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Mycuisine extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
	    $this->modules = array(
		    'slider',
		    'about',
		    'blog',
		    'portfolio',
		    'testimonials',
		    //'store',
		    'social',
		    'contact',
	    );
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'mycuisine';
    }

	function insert_section_portfolio($content = '', $data_post = [], $data_postmeta = [])
	{
		$content = <<<E
[otonomic_dt_gallery limit="64" read_more_enabled="off" title_enabled="off"]
E;
		$data_post = array(
				'post_title'   => 'Photos',
				'post_content' => $content,
				'post_name'    => 'portfolio',
				'post_status'  => 'publish',
				'post_type'    => 'page',
				'menu_order'   => 20,
				'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
			) + $data_post;

		$data_postmeta = array(
			) + $data_postmeta;

		$page_id = ot_insert_post( $data_post , $data_postmeta);
		add_post_meta($page_id, '_wp_page_template', 'page-full.php');
		$et_ptemplate_settings = array(
			'et_fullwidthpage' => 1,
			'et_ptemplate_gallerycats' => array(3),
			'et_ptemplate_gallery_perpage' => 12,
			'et_ptemplate_showtitle' => 0,
			'et_ptemplate_showdesc' => 1,
			'et_ptemplate_detect_portrait' => 1,
			'et_ptemplate_imagesize' => 3
		);
		add_post_meta($page_id, 'et_ptemplate_settings', $et_ptemplate_settings);

		return $page_id;
	}


    function set_pages_settings() {
        $data = [
            'updates' => [
                'et_webly_settings' => [
                    'et_is_featured' => 0
                ],
            ],
            'portfolio' => [
                'et_webly_settings' => [
                    'et_is_featured' => 0
                ],
            ],
        ];
        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    public function save_theme_data(){
        $this->update_options();

        $this->set_pages_settings();

        // $this->set_widgets();
        $this->set_text_widgets_footer();

        $this->set_quote_area([
            'h1' => $this->page->facebook->about,
            'h2' => ''
        ]);

        $this->set_address_options([
            'address' => $this->page->basic->address,
            'email' => $this->page->basic->email,
            'phone' => $this->page->basic->phone
        ]);

        $this->set_homepage_content();

        $this->set_menu( array('primary-menu', 'footer-menu') );
    }

    /*
    function set_post_content($post_id, $data = []) {
        $default_data = [
            'title' => '',
            'subtitle' => '',
            'featured_image' => null,
            'category' => null
        ];

        $data = $default_data + $data;

        // Update fields
    }
*/

    public function set_homepage_content() {
    }

    function set_content_area_1() {
        // Add page
        // Assign page to content area 1
        // Set "Use Content Area 1 Page" to "Enabled"
    }

    /*protected function clone_album_posts($album_post, $images){
        if(strtolower($album_post['post_title']) == 'cover photos'){
            $album_post['post_type'] = 'post';
            $album_post['post_title'] = get_bloginfo('name');
	        $album_post['post_name'] = sanitize_title_with_dashes( get_bloginfo('name') );
            // $slider_images = $this->get_images_for_slider();

            foreach($images as $image){
                $postID = otonomic_insert_post($album_post);
                wp_set_post_categories($postID, 11, FALSE);
                $post_meta = [
                    '_thumbnail_id'         => $image,
                    '_post_image_attach_id' => $image
                ];
                otonomic_insert_postmeta($postID, $post_meta);

                $ft_post_meta = array(
                    "et_is_featured" => 1,
                    "et_fs_variation" => 5,
                    "et_fs_video" => "",
                    "et_fs_video_embed" => "",
                    "et_fs_title" => "",
                    "et_fs_description" => "",
                    "et_fs_link" => ""
                );
                add_post_meta($postID, '_et_mycuisine_settings', $ft_post_meta);
            }
        }
    }*/

    public function set_address_options($data = []){
        if(isset($data['address'])) {
            update_option('mycuisine_address', $data['address']);
        }
        if(isset($data['email'])) {
            update_option('mycuisine_email', $data['email']);
        }
        if(isset($data['phone'])) {
            update_option('mycuisine_telephone', $data['phone']);
        }
    }


    public function set_quote_area($data) {
        if(isset($data['h1'])) {
            update_option('mycuisine_quote_line1', $data['h1']);
        }
        if(isset($data['h2'])) {
            update_option('mycuisine_quote_line2', $data['h2']);
        }
    }

    function update_options() {
	    parent::update_options();
        try {
            update_option( 'page_on_front', $this->get_homepage_id() );
            update_option( 'show_on_front', 'posts' );

            //update_option( 'template', $this->theme );
            update_option('current_theme', 'Mycuisinechild');
            update_option('stylesheet', 'mycuisinechild');

	        //switch_theme('mycuisinechild', 'mycuisinechild');

            $custom_css = "h1.category-title {
                            text-align:center
                            }
                            div.et_pt_portfolio_image img {
                                height: 280px;
                            }                            
                            ";
            update_option('mycuisine_custom_css', $custom_css);
            
            $contact_page = get_page_by_title('Contact');
            $contact_url = get_permalink($contact_page->ID);
            update_option('mycuisine_menu_page_url', $contact_url);

		} catch(Exception $e) {
        }
    }
}
