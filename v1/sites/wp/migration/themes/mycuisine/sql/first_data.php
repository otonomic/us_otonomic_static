<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 7),
(5, 5, 'portfolio-category', '', 0, 0),
(6, 6, 'testimonial-category', '', 0, 0),
(7, 7, 'testimonial-category', '', 0, 0),
(8, 8, 'highlight-category', '', 0, 0),
(9, 1, 'portfolio-category', '', 0, 0),
(10, 9, 'category', '', 0, 0),
(11, 10, 'post_tag', '', 0, 0),
(12,11,'category','',0,0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Blog', 'blog', 0),
(3, 'Image', 'image', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Main Portfolio', 'main-portfolio', 0),
(6, 'Testimonials', 'testimonials', 0),
(7, 'Team','team', 0),
(8, 'Services','services', 0),
(9, 'Facebook Post','facebook-post', 0),
(10, 'Facebook','facebook', 0),
(11,'home slider','home-slider',0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0);
E;






if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US');
E;






if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
('woocommerce_permalinks', 'false', 'yes'),
('mycuisine_color_scheme', 'Default', 'yes'),
('mycuisine_menu_numposts', '3', 'yes'),
('mycuisine_currency_sign', '$', 'yes'),
('mycuisine_catnum_posts', '6', 'yes'),
('mycuisine_archivenum_posts', '5', 'yes'),
('mycuisine_searchnum_posts', '5', 'yes'),
('mycuisine_tagnum_posts', '5', 'yes'),
('mycuisine_date_format', 'M j, Y', 'yes'),
('mycuisine_comment_date_format', 'n-j-Y', 'yes'),
('mycuisine_cufon', 'on', 'yes'),
('mycuisine_display_blurbs', 'on', 'yes'),
('mycuisine_quote', 'on', 'yes'),
('mycuisine_numposts_recommendations', '3', 'yes'),
('mycuisine_address', '', 'yes'),
('mycuisine_mon_fri', '', 'yes'),
('mycuisine_sat_sun', '', 'yes'),
('mycuisine_quote_line1', '', 'yes'),
('mycuisine_quote_line2', '', 'yes'),
('mycuisine_menu_page_url', '#', 'yes'),
('mycuisine_homepage_posts', '7', 'yes'),
('mycuisine_featured', 'on', 'yes'),
('mycuisine_duplicate', 'on', 'yes'),
('mycuisine_feat_cat', '11', 'yes'),
('mycuisine_featured_num', '3', 'yes'),
('mycuisine_slider_autospeed', '7000', 'yes'),
('mycuisine_enable_dropdowns', 'on', 'yes'),
('mycuisine_home_link', 'on', 'yes'),
('mycuisine_sort_pages', 'post_title', 'yes'),
('mycuisine_order_page', 'asc', 'yes'),
('mycuisine_tiers_shown_pages', '3', 'yes'),
('mycuisine_enable_dropdowns_categories', 'on', 'yes'),
('mycuisine_categories_empty', 'on', 'yes'),
('mycuisine_thumbnails', 'on', 'yes'),
('mycuisine_integrate_header_enable', 'on', 'yes'),
('mycuisine_integrate_body_enable', 'on', 'yes'),
('mycuisine_integrate_singletop_enable', 'on', 'yes'),
('mycuisine_integrate_singlebottom_enable', 'on', 'yes'),
('mycuisine_use_pages', 'false', 'yes'),
('mycuisine_logo', '', 'yes'),
('mycuisine_favicon', '', 'yes'),
('mycuisine_grab_image', 'false', 'yes'),
('mycuisine_use_excerpt', 'false', 'yes'),
('mycuisine_custom_css', '', 'yes'),
('mycuisine_use_area1', 'false', 'yes'),
('mycuisine_use_area2', 'false', 'yes'),
('mycuisine_use_area3', 'false', 'yes'),
('mycuisine_email', '', 'yes'),
('mycuisine_telephone', '', 'yes'),
('mycuisine_fax', '', 'yes'),
('mycuisine_slider_auto', 'false', 'yes'),
('mycuisine_page_thumbnails', 'false', 'yes'),
('nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
('mycuisine_blog_style', 'false', 'yes'),
('mycuisine_home_page_1', '369', 'yes'),
('mycuisine_home_page_2', '369', 'yes'),
('mycuisine_home_page_3', '369', 'yes'),
('mycuisine_tiers_shown_categories', '3', 'yes'),
('mycuisine_sort_cat', 'name', 'yes'),
('mycuisine_order_cat', 'asc', 'yes'),
('mycuisine_disable_toptier', 'false', 'yes'),
('mycuisine_postinfo2', 'a:4:{i:0;s:6:"author";i:1;s:4:"date";i:2;s:10:"categories";i:3;s:8:"comments";}', 'yes'),
('mycuisine_show_postcomments', 'on', 'yes'),
('mycuisine_show_pagescomments', 'false', 'yes'),
('mycuisine_postinfo1', 'a:4:{i:0;s:6:"author";i:1;s:4:"date";i:2;s:10:"categories";i:3;s:8:"comments";}', 'yes'),
('mycuisine_thumbnails_index', 'on', 'yes'),
('mycuisine_custom_colors', 'false', 'yes'),
('mycuisine_child_css', 'false', 'yes'),
('mycuisine_child_cssurl', '', 'yes'),
('mycuisine_color_mainfont', '', 'yes'),
('mycuisine_color_mainlink', '', 'yes'),
('mycuisine_color_pagelink', '', 'yes'),
('mycuisine_color_pagelink_active', '', 'yes'),
('mycuisine_color_headings', '', 'yes'),
('mycuisine_color_sidebar_links', '', 'yes'),
('mycuisine_footer_text', '', 'yes');
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
