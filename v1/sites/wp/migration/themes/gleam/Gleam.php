<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Gleam extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {
    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'gleam';
    }
	
	public function install(){
        parent::install();

        /* Load the child theme now */
        if($this->theme == 'gleam') {
            update_option('current_theme', 'Gleamchild');
            update_option('stylesheet', 'gleamchild');
        }

        return $this->blog_id;
    }

    function add_module_slider()
    {
        $image = $this->set_main_site_slider();
        add_option('gleam_homepage_bg', $image->source);

    }

    function set_pages_settings() {
        $data = [
            'about' => $this->get_full_width_settings_array(),

            'contact' => $this->get_full_width_settings_array(),

            'social' => $this->get_full_width_settings_array(),

            'portfolio' => [
                '_wp_page_template' => 'page-template-portfolio.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 1,
                    'et_ptemplate_gallerycats' => array(3),
                    'et_ptemplate_gallery_perpage' => 12,
                    'et_ptemplate_showtitle' => 0,
                    'et_ptemplate_showdesc' => 0,
                    'et_ptemplate_detect_portrait' => 1,
                    'et_ptemplate_imagesize' => 2
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ],

            'blog' => [
                '_wp_page_template' => 'page-blog.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 0,
                    'et_ptemplate_blogstyle' => 0,
                    'et_ptemplate_showthumb' => 0,
                    'et_ptemplate_gallerycats' => array(2),
                    'et_ptemplate_blog_perpage' => 6
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ]
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }



    public function save_theme_data(){
        $this->update_options();

        $this->set_pages_settings();

        $this->set_menu(array('primary-menu', 'footer-menu'));
    }

    protected function update_options()
    {
        try {
            parent::update_options();
            //update_option('simplepress_feat_cat', '3');

            update_option( 'gleam_logo_text', $this->page->facebook->name );

        } catch(Exception $e) {
        }
    }
}
