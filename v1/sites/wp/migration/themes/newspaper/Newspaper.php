<?php
include_once ABSPATH . "migration/core/tagDivThemesAbstract.php";

class Newspaper extends tagDivThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

	protected $page_ids;

    public function __construct($blog_id, $page, $fb_category_object = 'Generic'){
	    $this->theme = 'newspaper';
	    $this->modules = array(
		    'about',
		    'home',
		    'news',
		    'videos',
		    'games',
		    'buzz',
		    //'services',
		    // 'reservation',
		    //'store',
			'shop'
		    // 'blog',
	    );
		$this->page_ids = array();
        parent::__construct($blog_id, $page, $fb_category_object);
    }


	/* add_module_ functions */
	function add_module_home()
	{
		$this->page_ids['home'] = $this->add_section_home();
	}
	function add_module_services()
	{
		$this->page_ids['service'] = $this->add_section_services();
	}
	function add_module_shop()
	{
		$this->page_ids['shop'] = $this->add_section_shop();
	}

	function add_module_videos()
	{
		$this->page_ids['cideos'] = $this->add_section_videos();
	}
	function add_module_news()
	{
		$this->page_ids['news'] = $this->add_section_news();
	}
	function add_module_games()
	{
		$this->page_ids['games'] = $this->add_section_games();
	}
	function add_module_buzz()
	{
		$this->page_ids['buzz'] = $this->add_section_buzz();
	}

	function add_section_shop()
	{
		$data_post = array(
			'post_title'   => 'Shop',
			'post_content' => $this->get_content_shop(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 70,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}

	/* add_section_ functions */
	public function add_section_buzz()
	{
		$data_post = array(
			'post_title'   => 'Buzz',
			'post_content' => $this->get_content_buzz(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 40,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}

        public function add_default_products(){
            $wpotonomic_product = wpotonomic_product::get_instance();
            $WPOtonomicProduct = new WPOtonomicProduct();

            for($i = 1; $i <= 4 ; $i++){
                $WPOtonomicProduct = new WPOtonomicProduct();
                $WPOtonomicProduct->type = 'simple';
                $WPOtonomicProduct->id = '0';
                $WPOtonomicProduct->sku = 'sku-00'.$i;
                $WPOtonomicProduct->title = 'Product '.$i;
                $WPOtonomicProduct->description = 'This is your sample product. Edit it and add more like it to your store!';
                $WPOtonomicProduct->price = '10';
                $WPOtonomicProduct->category = 0;
                $wpotonomic_product->save($WPOtonomicProduct);
            }
        }

	public function add_default_staff( $services_id )
	{
		$user = wp_get_current_user();
		$data_post = array(
			'post_title'   => $user->display_name,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'dt_staffs',
		);
		$data_postmeta = array(
			'_services'=>$services_id,
			'_timer'=>array (
				'monday_start' => '08:00',
				'monday_end' => '17:00',
				'tuesday_start' => '08:00',
				'tuesday_end' => '17:00',
				'wednesday_start' => '08:00',
				'wednesday_end' => '17:00',
				'thursday_start' => '08:00',
				'thursday_end' => '17:00',
				'friday_start' => '08:00',
				'friday_end' => '18:00',
				'saturday_start' => '',
				'saturday_end' => '00:00',
				'sunday_start' => '',
				'sunday_end' => '00:00',
			)
		);
		ot_insert_post( $data_post , $data_postmeta);
	}
	public function add_default_services()
	{
		$services_id = array();
		$services = array(
                    array(
                        'title' => 'Hair Cutting',
                        'image'=> content_url( '/themes/newspaperchild/images/service/services-1.png'),
                        'content'=>
                            <<<E
                                    <ul class="with-bullets">
                                        <li>Women’s Haircut<span class="pull-right"></span></li>
                                        <li>Women’s Haircut & Style<span class="pull-right"></span></li>
                                        <li>Women’s Haircut, Wash & Style<span class="pull-right"></span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Men’s Haircut<span class="pull-right"></span></li>
                                        <li class="hidden">Child Haircut<span class="pull-right"></span></li>
                                    </ul>
E

                    ),
                    array(
                        'title' => 'Hair Coloring',
                        'image'=> content_url( '/themes/newspaperchild/images/service/services-2.png'),
                        'content' => <<<E
                                    <ul class="with-bullets">
                                        <li>Single Process<span class="pull-right"> </span></li>
                                        <li>Double Process<span class="pull-right"> </span></li>
                                        <li>Full Highlights<span class="pull-right"> </span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Partial Highlights<span class="pull-right"></span></li>
                                        <li class="hidden">Corrective Color<span class="pull-right"></span></li>
                                    </ul>
E
                    ),
                    array(
                        'title' => 'Hair Styling',
                        'image'=> content_url( '/themes/newspaperchild/images/service/services-3.png'),
                        'content' => <<<E
                                    <ul class="with-bullets">
                                        <li>Shampoo & Style<span class="pull-right"></span></li>
                                        <li>Blow Dry & Style<span class="pull-right"></span></li>
                                        <li>Flat or Curling Iron<span class="pull-right"></span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Partial Updo<span class="pull-right"></span></li>
                                        <li class="hidden">Updo<span class="pull-right"></span></li>
                                    </ul>
E
                    )
		);

		$services = $this->fb_category_object->get_services();

		foreach($services as $service)
		{
			$data_post = array(
				'post_title'   => $service['title'],
				'post_content' => $service['content'],
				'post_status'  => 'publish',
				'post_type'    => 'service',
			);
			$data_postmeta = array(
				'_info'=>array (
					'price' => '30',
					'duration' => '1800',
				)
			);
			$post_id = ot_insert_post( $data_post , $data_postmeta);
			$services_id[] = $post_id;

            $dummy_image = new stdClass();
            $dummy_image->source = $service['image'];
            $dummy_image->picture = $dummy_image->source;
            $attachment_id = $this->add_image_remote($dummy_image, $post_id);
            set_post_thumbnail( $post_id, $attachment_id );
		}
		return $services_id;
	}

	public function add_section_news()
	{
		$data_post = array(
			'post_title'   => 'News',
			'post_content' => $this->get_content_news(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 30,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}
	public function add_section_games() {
		$data_post = array(
			'post_title'   => 'Games',
			'post_content' => $this->get_content_games(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 60,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}
	function add_section_blog()
	{
		$content = '';
		$data_post = array(
				'post_title'   => 'News',
				'post_content' => $content,
				'post_name'    => 'news',
				'post_status'  => 'publish',
				'post_type'    => 'page',
				'menu_order'   => 70,
			);
		$data_postmeta = array();
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_videos()
	{
		$content = $this->get_content_videos();

		// Save content
		$data_post = array(
			'post_title'   => 'Videos',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 50
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_reservation()
	{
		$content = $this->get_content_reservation();

		// Save content
		$data_post = array(
			'post_title'   => 'Make an Appointment',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 40
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_services()
	{
		$content = $this->get_content_services();

		// Save content
		$data_post = array(
			'post_title'   => 'Services',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 30
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_home()
	{
		$content = $this->get_content_home();

		// Save content
		$data_post = array(
			'post_title'   => 'Home',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 10
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	/* Content functions */
	function get_content_buzz()
	{
		$content = '';
		$content .= <<<E
[otonomic_dpSocialTimeline]
E;
		return $content;
	}
	function get_content_news()
	{
		$content = '';
		$content .= <<<E
		[vc_row]
			[vc_column width="2/3"]
				[td_block2 limit="6" custom_title="News"]
			[/vc_column]
			[vc_column width="1/3"]
				[vc_widget_sidebar sidebar_id="td-default"]
			[/vc_column]
		[/vc_row]
		[vc_row]
			[vc_column width="1/1"]
				[facebook_comments]
			[/vc_column]
		[/vc_row]
E;
		return $content;
	}
	function get_content_shop()
	{
		$business_name = get_bloginfo('name');

		return $content = <<<E
		<script charset="utf-8" type="text/javascript">
amzn_assoc_ad_type = "responsive_search_widget";
amzn_assoc_tracking_id = "widgetsamazon-20";
amzn_assoc_link_id = "CEUBIJ7HQ7USBAWM";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "US";
amzn_assoc_placement = "";
amzn_assoc_search_type = "search_widget";
amzn_assoc_width = "700";
amzn_assoc_height = "610";
amzn_assoc_default_search_category = "";
amzn_assoc_default_search_key = "{$business_name}";
amzn_assoc_theme = "light";
amzn_assoc_bg_color = "FFFFFF";
</script>
<script src="//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1&MarketPlace=US"></script>
		[vc_row]
			[vc_column width="1/1"]
				[facebook_comments]
			[/vc_column]
		[/vc_row]
E;
	}
	function get_content_games() {
		return $content = <<<E
		<script src="//cdn.playbuzz.com/widget/feed.js" type="text/javascript"></script>
<div class="pb_feed" data-key="Default" data-tags="pop" data-social="true" data-recommend="true" data-margin-top="" data-height="" data-embed-by="18f2e5d3-4e9e-4764-8ffa-349b771fb136"></div>
		[vc_row]
			[vc_column width="1/1"]
				[facebook_comments]
			[/vc_column]
		[/vc_row]
E;
	}
	function get_content_videos()
	{
		$business_name = get_bloginfo('name');
		$content = '';
		$content .= <<<E
		<style>
body.Newspaper .tubepress_container{float:none}
body.Newspaper .tubepress_container .tubepress_normal_embedded_wrapper{width:100% !important}body.Newspaper .tubepress_container .ellipsis,body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta_title a{display:block;display:-webkit-box;max-height:2.2em;margin:0 auto;font-size:13px;line-height:1.1;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis}
body.Newspaper .tubepress_container .tubepress_thumb{height:auto;font-family:'Roboto Condensed',sans-serif}
body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta{line-height:1.1}body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta_title a{display:-webkit-box !important;display:-moz-box !important;display:-ms-box !important;display:-o-box !important;display:box !important;color:#222222;margin-top:4px;margin-bottom:1px}
</style>
[tubepress mode="tag" tagValue="{$business_name}" playerLocation="normal" resultsPerPage="20"]
		[vc_row]
			[vc_column width="1/1"]
				[facebook_comments]
			[/vc_column]
		[/vc_row]
E;
		return $content;
	}

	function get_content_reservation()
	{
		$content = '';
		$content .= <<<E

E;
		return $content;
	}

	function get_content_services()
	{
		$content = <<<E
[otonomic_dt_services]
E;
		return $content;
	}

	function get_slider_images()
	{
		$slider_images = array();
		$images = array('slide-2.jpg', 'slide-1.jpg');
		foreach($images as $image)
		{
			$dummy_image = new stdClass();
			$dummy_image->source = content_url('/themes/newspaperchild/images/slider/' . $image);
			$dummy_image->picture = $dummy_image->source;

			$slider_images[] = $dummy_image;
		}
		return $slider_images;
	}

    function get_slider_options() {
        return [
            'min_width'                  => 100,
            // 'min_width'                  => 1000,
//            'min_w2h_ratio'              => 1.3,
            'max_num_main_slider_images' => 5,
            'min_num_main_slider_images' => 1,
            'width1' => 600,
            'width2' => 600,
        ];
    }

    function get_slider_shortcode()
	{
		$slider_images = $this->get_merged_slider_images();
        
		if(count($slider_images)>0)
		{
			$slide_count=0;
                        $slides = array();
			foreach($slider_images as $slider_image)
			{
				$attachment_id = $this->add_image_replicate_cover($slider_image);
				$slides[] = $attachment_id['path'];
                                $slide_count++;
			}
		}
                
		return '[otonomic_greyout_slider urls="'.implode(',', $slides).'"]';
	}
	function get_merged_slider_images()
	{
		$slider_images_facebook = $this->get_images_for_slider( $this->page->albums);
		// $slider_images_stock = $this->get_slider_images();
		//$slider_images_stock = [];
		$slider_images_stock = $this->fb_category_object->get_splash_images();

		$slider_images = array_merge($slider_images_facebook, $slider_images_stock);

		return $slider_images;
	}

	function get_content_about()
	{
		$content = parent::get_content_about();
		$content .= '<div id="about-team-wrapper" class="clearfix">[otonomic_team /]</div>[facebook_comments]';
		return $content;
	}

	function get_content_home()
	{
		//$slider = $this->get_slider_shortcode();
		$business_name = get_bloginfo('name');

		$slider_images = $this->get_merged_slider_images();
		$splash_image = '';

		if(count($slider_images)>0)
		{
			$attachment_id = $this->add_image_replicate_cover($slider_images[0]);
			$splash_image = $attachment_id['path'];
		}

		$content = <<<E

		[vc_row]
			[vc_column width="1/1" css=".vc_custom_1422972689932{padding-top: 200px !important;padding-bottom: 200px !important;background-image: url({$splash_image}) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"]
				[vc_column_text]
					<h1 style="text-align: center;"><span style="font-size: 40px; color: #ffffff;"><strong>{$this->page->facebook->about}</strong></span></h1>
				[/vc_column_text]
			[/vc_column]
		[/vc_row]
		[vc_row]
			[vc_column width="1/1"]
				[td_text_with_title custom_title="Latest photos"]
					[otonomic_fb_gallery link="lightbox" width="250" height="250" limit="4"]
				[/td_text_with_title]
			[/vc_column]
		[/vc_row]
		[vc_row]
			[vc_column width="3/4"]
				[td_text_with_title custom_title="About"]
					<strong>{$business_name}</strong> [otonomic_option option="blogdescription"]
				[/td_text_with_title]
				[vc_single_image image="318" border_color="grey" img_link_target="_self" css=".vc_custom_1422981687769{padding-bottom: 40px !important;}"]
				[td_text_with_title custom_title="Videos"]
					<div>[tubepress mode="tag" tagValue="{$business_name}" playerLocation="normal" resultsPerPage="6"]</div>
				[/td_text_with_title]
				[td_text_with_title custom_title="Quizzes"]
					<script src="//cdn.playbuzz.com/widget/widget.js" type="text/javascript"></script>
					<div class="pb_recommendations" data-key="Default" data-provider="playbuzz" data-view="large_images" data-links="" data-num-items="6" data-tags="pop" data-nostyle="false" data-embed-by="18f2e5d3-4e9e-4764-8ffa-349b771fb136"></div>
				[/td_text_with_title]
				[vc_single_image image="329" border_color="grey" img_link_target="_self" css=".vc_custom_1422981763043{padding-bottom: 40px !important;}"]
				[td_block6 limit="5" custom_title="News"]
			[/vc_column]
			[vc_column width="1/4"]
				[vc_widget_sidebar sidebar_id="td-default"]
			[/vc_column]
		[/vc_row]
E;
		return $content;
	}

    function set_pages_settings() {
        $data = [
	        'home'          => $this->get_home_settings_array(),
            'about'         => $this->get_right_sidebar_settings_array(),
            'news'			=> $this->get_news_settings_array(),
            'buzz'			=> $this->get_container_width_no_sidebar_settings_array(),
            'videos'       => $this->get_right_sidebar_settings_array(),
            'games'       => $this->get_right_sidebar_settings_array(),
	        'shop'      => $this->get_right_sidebar_settings_array(),
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }
	function get_news_settings_array()
	{
		$slider_images = $this->get_merged_slider_images();
		$splash_image = '';

		if(count($slider_images)>0)
		{
			$attachment_id = $this->add_image_replicate_cover($slider_images[0]);
			$splash_image = $attachment_id['path'];
		}
		$settings = [
			'_wp_page_template' => 'page-homepage-bg.php',
			'slide_template' => '',
			'td_homepage_loop_slide'=> [
				'td_slide_background' => $splash_image,
			],
			'vc_teaser'=> [
				'data' => '[{"name":"title","link":"post"},{"name":"image","image":"featured","link":"none"},{"name":"text","mode":"excerpt"}]',
				'bgcolor' => '',
			],
		];
		return $settings;
	}
	function get_right_sidebar_settings_array()
	{
		$settings = [
			'_wp_page_template' => 'default',
			'slide_template' => 'default',
			'vc_teaser'=> [
				'data' => '[{"name":"title","link":"post"},{"name":"image","image":"featured","link":"none"},{"name":"text","mode":"excerpt"}]',
				'bgcolor' => '',
			],
		];
		return $settings;
	}
	function get_home_settings_array()
	{
		$slider_images = $this->get_merged_slider_images();
		$splash_image = '';

		if(count($slider_images)>0)
		{
			$attachment_id = $this->add_image_replicate_cover($slider_images[0]);
			$splash_image = $attachment_id['path'];
		}

		$settings = [
			'_wp_page_template' => 'page-homepage-blank.php',
			'slide_template' => 'default',
			'vc_teaser'=>array (
				'data' => '[{"name":"title","link":"none"},{"name":"image","image":"featured","link":"none"},{"name":"text","mode":"excerpt"}]',
				'bgcolor' => '',
			),
			'_wpb_shortcodes_custom_css'=>'.vc_custom_1422972689932{padding-top: 200px !important;padding-bottom: 200px !important;background-image: url('.$splash_image.') !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1422981687769{padding-bottom: 40px !important;}.vc_custom_1422981763043{padding-bottom: 40px !important;}',
			'td_homepage_loop_slide'=>array (
				'td_slide_background' => $splash_image,
				'td_slide_limit' => '0',
			),
			'_wpb_post_custom_css'=>"body.Newspaper .tubepress_container{float:none;}
 body.Newspaper .tubepress_container .tubepress_normal_embedded_wrapper{width:100% !important}body.Newspaper .tubepress_container .ellipsis,body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta_title a{display:block;display:-webkit-box;max-height:2.2em;margin:0 auto;font-size:13px;line-height:1.1;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis}
 body.Newspaper .tubepress_container .tubepress_thumb{height:auto;font-family:'Roboto Condensed',sans-serif}
 body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta{line-height:1.1}
 body.Newspaper .tubepress_container .tubepress_thumb .tubepress_meta_title a{display:-webkit-box !important;display:-moz-box !important;display:-ms-box !important;display:-o-box !important;display:box !important;color:#222222;margin-top:4px;margin-bottom:1px}
 body.Newspaper .tubepress_container .pagination{float:none;}"
		];
		return $settings;
	}


	function get_full_width_settings_array() {
		$settings = [
			'_wp_page_template' => 'tpl-fullwidth.php',
			'_tpl_default_settings' => [
				'layout' => 'content-full-width',
			],
			'_dt_post_settings'=>[
				'layout' => 'content-full-width',
			]
		];

		return $settings;
	}
	function get_container_width_no_sidebar_settings_array() {
		$settings = [
			'_wp_page_template' => 'default',
			'td_page' => [
				'td_sidebar_position' => 'no_sidebar',
			],
		];
		return $settings;
	}


	/* Menu items */
	function get_menu_items()
	{
		$menu_items = array();

		$menu_items[] = array(
			'menu-item-title' => 'Home',
			'menu-item-url' => home_url( '/' ),
			/*'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',*/
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'News',
			'menu-item-object-id' => get_page_by_path('news')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Buzz',
			'menu-item-object-id' => get_page_by_path('buzz')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Games',
			'menu-item-object-id' => get_page_by_path('games')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Videos',
			'menu-item-object-id' => get_page_by_path('videos')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Shop',
			'menu-item-object-id' => $this->page_ids['shop'],
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'About',
			'menu-item-object-id' => get_page_by_path('about')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);



		/*$menu_items[] = array(
			'menu-item-title' => 'Services',
			'menu-item-object-id' => get_page_by_path('services')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/
		/*$menu_items[] = array(
			'menu-item-title' => 'Reservation',
			'menu-item-object-id' => get_page_by_path('reservation')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/
		/* check if we need to add store */
		//if(in_array('store',$this->modules))
		{
			/* Lets create store menu */
			$menu_items[] = array(
				'menu-item-title' => 'Store',
				'menu-item-object-id' => get_page_by_path('shop')->ID,
				'menu-item-object' => 'page',
				'menu-item-type' => 'post_type',
				'menu-item-status' => 'publish'
			);
		}


		/*$menu_items[] = array(
			'menu-item-title' => 'News',
			'menu-item-object-id' => get_page_by_path('news')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/


		return $menu_items;
	}

	protected function add_album($fbalbum, $download_image=false, $album=0) {	
            if(!isset($fbalbum->photos->data)) { return; }

		$album_photos = $fbalbum->photos->data;

		if(empty($album_photos)) { return; }

		$my_album = array(
			'post_title'     => $fbalbum->name,
			'post_content'   => $fbalbum->description,
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status' 	 => 'closed',
			'post_type' 	 => 'dt_galleries',
			'guid'           => $fbalbum->id,
//            'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
		);

		$post_date = $this->parse_date($fbalbum->created_time);
		$my_album['post_date'] = $post_date;
		$my_album['post_date_gmt'] = $post_date;
		$my_album['post_modified'] = $post_date;
		$my_album['post_modified_gmt'] = $post_date;

		//$post_id = wp_insert_post( $my_album );
		$post_id = otonomic_insert_post($my_album);
//        $this->clone_album_posts($my_album);
//        wp_set_post_categories($post_id, 9);
		// TODO: Change category from number
		// TODO: Add terms+category
		// tax_input in the input to wp_insert_post() should work instead
		// $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

		// adding the image to the post
		$image_ids = array();
		$items_thumbnail = array();

		$otonomic_url = otonomic_url::get_instance();

		foreach((array)$album_photos as $key=>$picture)
		{
			try {
				$image_id = $this->add_image($picture, $post_id, $download_image);
				$image_ids[] = $image_id;
				$items_thumbnail[] = $otonomic_url->ot_get_source_url($picture->picture);

			} catch(Exception $e) {
			}
		}
		// add meta tags for this videos
		$post_meta = [
			'_dt_post_settings'         => array (
				'layout' => 'content-full-width',
			),
			'_tpl_default_settings' => array (
				'layout' => 'content-full-width',
			),
			'_videos_settings'=>array (
				'layout' => 'content-full-width',
				'items' =>$items_thumbnail,
				'items_thumbnail' => $items_thumbnail,
				'items_name' => $image_ids,
			)
		];

		otonomic_insert_postmeta($post_id, $post_meta);

		//$this->clone_album_posts($my_album,$image_ids);

		return $post_id;
	}

    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work

	    //$this->add_homepage();

	    $this->set_theme_options();
        $this->set_widgets();
		$this->set_pages_settings();
        $this->update_options();
        
        $this->set_menu(array('header-menu', 'footer-menu'));
        // $this->add_default_products();
    }



    protected function update_options()
    {
        try {
	        parent::update_options();
            // TODO: Comment global here?
            global $wpdb;

            update_option( 'page_on_front', $this->get_homepage_id() );
            update_option( 'show_on_front', 'page' );

            update_option( 'template', $this->theme );
            update_option( 'stylesheet', 'newspaperchild' );
            update_option( 'current_theme', 'newspaperchild' );
            /*update_option('Newspaper_feat_cat', '11');
            update_option('Newspaper_use_pages', 'false');*/

            update_option( 'last_update', time() );
            update_option( 'facebook_id', $this->page->facebook->id );

        } catch(Exception $e) {
        }
    }

	protected function set_theme_options()
	{
		try {
			include_once('config/theme_options.php');
			$data = get_theme_options();
			if ( get_option( 'td_008' ) !== false )
			{
				update_option( 'td_008' , $data );
			}
			else
			{
				add_option( 'td_008', $data, null, 'yes' );
			}

		} catch(Exception $e) {
		}
		//switch_theme($this->theme,$this->theme);
	}
}
