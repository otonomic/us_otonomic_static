<?php
$business_name = get_bloginfo('name');
$config = array(
    'widget_text' => array(
	    array(
			'title' => 'Contact us',
			'text' => '[otonomic_contact_details]',
			'filter' => '',
	    ),
	    array(
			'title' => 'Instagram',
			'text' => '[instagram-feed]',
			'filter' => false,
	    ),
	    array(
		    'title' => 'Merchandise',
			'text' => '<script type=\'text/javascript\'>
 var amzn_wdgt={widget:\'MyFavorites\'};
 amzn_wdgt.tag=\'widgetsamazon-20\';
 amzn_wdgt.columns=\'1\';
 amzn_wdgt.rows=\'3\';
 amzn_wdgt.title=\''.$business_name.'\';
 amzn_wdgt.width=\'325\';
 amzn_wdgt.ASIN=\'B00ERIUO02,B006XBSAP2,B0017ZB8M6,B00EH49FRE,B003O86CJS,B00ENL4HJW\';
 amzn_wdgt.showImage=\'True\';
 amzn_wdgt.showPrice=\'True\';
 amzn_wdgt.showRating=\'True\';
 amzn_wdgt.design=\'2\';
 amzn_wdgt.colorTheme=\'Blue\';
 amzn_wdgt.headerTextColor=\'#FFFFFF\';
 amzn_wdgt.marketPlace=\'US\';
 </script>
 <script type=\'text/javascript\' src=\'http://wms-na.amazon-adsystem.com/20070822/US//js/AmazonWidgets.js\'>
 </script>',
			'filter' => false,
	    ),
	    array(
			'title' => $business_name,
			'text' => '[otonomic_option option="blogdescription"]',
			'filter' => false,
	    ),
	    '_multiwidget'=>1
    ),
	'widget_fetch_tweets_widget_by_id' => array (
		array (
			'title' => 'Latest Tweets',
			'selected_ids' =>
				array (
					0 => '327',
				),
			'count' => '20',
			'twitter_media' => '1',
			'external_media' => '1',
			'template' => './wp-content/plugins/fetch-tweets/template/single',
			'avatar_size' => '48',
			'width' => '100',
			'width_unit' => '%',
			'height' => '400',
			'height_unit' => 'px',
		),
		'_multiwidget' => 1,
	),
	'widget_songkick-concerts-widget' => array(
		array (
			'title' => 'Upcoming Shows',
			'songkick_id_type' => 'artist',
			'songkick_id' => '1134363',
			'attendance' => 'all',
			'gigography' => false,
			'number_of_events' => 0,
			'hide_if_empty' => true,
			'date_color' => '',
			'logo' => 'songkick-logo.png',
			'show_pagination' => false,
		),
		'_multiwidget' => 1,
	),
	'widget_pages' => array(
		array (
			'title' => 'Pages',
			'sortby' => 'menu_order',
			'exclude' => '',
		),
		'_multiwidget' => 1,
	),
	'sidebars_widgets'=> array(
		'td-default' => array ('fetch_tweets_widget_by_id-0','text-2','text-1','songkick-concerts-widget-0'),
		'td-top-right' => array('text-0'),
		'td-footer-1' => array('pages-0'),
		'td-footer-2' => array('text-3'),
		'td-footer-3' => array(),
		'array_version' => 3
	),
);
return $config;
