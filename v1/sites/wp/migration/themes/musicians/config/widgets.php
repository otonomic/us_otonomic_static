<?php
$config = array(
    'widget_text' => array(
	    array(
		    'title' => 'See also',
		    'text' => '[pagelist]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Social',
		    'text' => '[otonomic_contact_details]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Show some love',
		    'text' => '<iframe src="//www.facebook.com/plugins/likebox.php?href='.urlencode($this->page->basic->facebook).'&amp;width=250&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:290px;" allowTransparency="true"></iframe>',
		    'filter' => ''
	    ),
            array(
                'title' => 'tweets',
                'text' => '<a class="twitter-timeline"
                            href="https://twitter.com/CMFoogo001"
                            data-widget-id="552437709923696641"
                            data-screen-name="CMFoogo001"
                            data-show-replies="false"
                            data-tweet-limit="5">
                          Tweets by @twitterapi
                          </a>',
                'filter' => ''
            ),
	    array(
		    'title' =>'Opening hours',
		    'text' => '[otonomic_option option="opening_hours" ]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Book Appointment',
		    'text' => '[dt_sc_button variation="black" type="without-icon" link="/reservation/" size="medium" target="_blank"]Reserve now![/dt_sc_button] ',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Get a Discount',
		    'text' => '<img src="http://placehold.it/260x152&amp;text=Image">',
		    'filter' => ""
	    ),
	    '_multiwidget'=>1
    ),
	'sidebars_widgets'=> array(
		'footer-sidebar-1'=>array('text-0'),
		'footer-sidebar-2' => array('text-1'),
		'footer-sidebar-3' => array('text-2'),
		'footer-sidebar-4' => array('text-4'),
		'display-everywhere-sidebar' => array('text-4', 'text-5'),
		'array_version' => 3
	),
);
return $config;
