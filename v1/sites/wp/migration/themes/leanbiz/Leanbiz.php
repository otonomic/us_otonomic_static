<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Leanbiz extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'leanbiz';
    }

    function set_pages_settings() {
        $data = [
            'about' => $this->get_full_width_settings_array(),
//            'social' => $this->get_full_width_settings_array(),
            'updates' => [
                '_et_leanbiz_settings' => [
                    'et_is_featured' => 0
                ],
            ],
            'portfolio' => [
                '_et_leanbiz_settings' => [
                    'et_is_featured' => 0
                ],
            ],
        ];
        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work
        // $this->set_widgets();
        $this->set_pages_settings();
        $this->set_text_widgets_sidebar();
        $this->set_text_widgets_footer();
        $this->update_options();

        $this->set_menu();
    }




    
    protected function set_text_widgets_footer(){
        $text_widget = array(
            array(
                'title' => 'About',
                'text' => '[otonomic_option option="blogdescription"]',
                'filter' => ""
            ),
            array(
                'title' => 'Recent Posts',
                'text' => '',
                'filter' => ""
            ),
            array(
                'title' => 'Contact Details',
                'text' => '[otonomic_contact_details tag_type1="h6"]',
                'filter' => ""
            ),
            array(
                'title' =>'Contact',
                'text' => '<div id="leanbiz_contact_sidebar" >[otonomic_contact]</div>',
                'filter' => ""
            ),
            array(
                'title' => 'Testimonial',
                'text' => '[otonomic_testimonials limit="1"]',
                'filter' => '',
            )
        );
        $text_widget['_multiwidget'] = 1;

        update_option('widget_text', $text_widget);

        $recent_posts_data = array(
            array(
                'title' => '',
                'number' => 5,
                'show_date' => ''
            ),
            '_multiwidget' => 1
        );
        update_option('widget_recent-posts', $recent_posts_data);
        
        $sidebar1 = array('text-2','text-3');
        
        $set_widgets = array(
            'wp_inactive_widgets' => array('archives-2','meta-2','search-2','categories-2','recent-posts-2'),
            'sidebar-1' => $sidebar1,
            'footer-area' => array('text-0','recent-posts-0','text-4','text-2'),
            'array_version' => 3
        );
        update_option('sidebars_widgets',$set_widgets);
    }

    protected function update_options()
    {
        try
        {
            // TODO: Comment global here?
            global $wpdb;

            update_option( 'page_on_front', $this->get_homepage_id() );
            update_option( 'show_on_front', 'page' );

            update_option( 'template', $this->theme );
            update_option( 'stylesheet', 'leanbizchild' );
            update_option( 'current_theme', 'Leanbizchild' );
            /*update_option('leanbiz_feat_cat', '11');
            update_option('leanbiz_use_pages', 'false');*/

            update_option( 'last_update', time() );
            update_option( 'facebook_id', $this->page->facebook->id );
        } catch(Exception $e) {
        }
    }
}
