<?php
$config = array(
    'widget_text' => array(
	    array(
		    'title' => 'About',
		    'text' => '[otonomic_option option="blogdescription"]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Contact Details',
		    'text' => '[otonomic_contact_details]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Services',
		    'text' => '[otonomic_services]',
		    'filter' => ''
	    ),
	    array(
		    'title' =>'',
		    'text' => '[otonomic_fb_like before="" after="" width="230"]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Contact',
		    'text' => '[otonomic_contact]',
		    'filter' => ""
	    ),
	    '_multiwidget'=>1
    ),
	'sidebars_widgets'=> array(
		'sidebar-1'=>array('text-3', 'text-4'),
		'sidebar-2' => array('text-2','text-1','text-0'),
		'array_version' => 3
	),
);
return $config;
