<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Feather extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
	    $this->theme = 'feather';
        parent::__construct($blog_id, $page, $fb_category_object);
	    $this->featured_post_setting_meta_name = 'et_feather_settings';
    }

    function get_first_testimonial() {
        $testimonials = $this->page->testimonials;

        $content = "";
        $testimonial_quote = "";

        foreach($testimonials as $key=>$testimonial){
            if($testimonial->text != ""){
                if($testimonial_quote == "") {
                    $testimonial_quote = $testimonial->text;
                    $testimonial_user_image = $testimonial->user_picture;
                }
            }
        }

        if(strlen($testimonial_quote) > 150){
            $last_space = strrpos(substr($testimonial_quote, 0, 150), ' ');
            $trimmed_testimonial = substr($testimonial_quote, 0, $last_space);
            $read_more = '... <a href="/testimonials">read more</a>';
            $image = '<img src="'.$testimonial_user_image.'" alt="" class="testimonial-photo" width="40" height="40">';
            $testimonial_quote = $image.$trimmed_testimonial.$read_more;
        }

        return $testimonial_quote;
    }

    public function set_quote_area() {
        update_option('feather_quote_one', $this->get_first_testimonial());
    }

    function set_pages_settings() {
        $data = [
            'about' => $this->get_full_width_settings_array(),

            'contact' => $this->get_full_width_settings_array(),

            'social' => $this->get_full_width_settings_array(),

            'testimonials' => $this->get_full_width_settings_array(),

            'portfolio' => [
                '_wp_page_template' => 'page-gallery.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 1,
                    'et_ptemplate_gallery_perpage' => 12
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ],

            'blog' => [
                '_wp_page_template' => 'page-blog.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 1,
                    'et_ptemplate_blogstyle' => 0,
                    'et_ptemplate_showthumb' => 0,
                    'et_ptemplate_blogcats' => array('2'),
                    'et_ptemplate_blog_perpage' => 6
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ]
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work
        // $this->set_widgets();
        // $this->set_themify_options();
	    $this->set_widgets();
	    // $this->set_text_widgets_sidebar();
        // $this->set_text_widgets_footer();
        $this->update_options();

        $this->set_pages_settings();
        $this->set_quote_area();

        $this->set_menu(array('primary-menu', 'footer-menu'));
    }

    protected function update_options()
    {

        try {
            parent::update_options();

	        update_option( 'page_on_front', $this->get_homepage_id() );
	        update_option( 'show_on_front', 'page' );

	        update_option( 'template', $this->theme );
	        update_option( 'stylesheet', 'featherchild');
	        update_option( 'current_theme', 'Featherchild' );

	        // random number so that 4th section displays blank
	        update_option('feather_home_page_4', 22334);

            //update_option('feather_feat_cat', 'home slider');
            //update_option('feather_use_pages','false');

            // high number so that 4th section displays blank
            update_option('feather_home_page_4', 22334);

            update_option('feather_quote', 'on');
            update_option('feather_display_blurbs', false);

        } catch(Exception $e) {
        }
    }
}
