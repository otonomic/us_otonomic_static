<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 7),
(5, 5, 'portfolio-category', '', 0, 0),
(6, 6, 'testimonial-category', '', 0, 0),
(7, 7, 'testimonial-category', '', 0, 0),
(8, 8, 'highlight-category', '', 0, 0),
(9, 1, 'portfolio-category', '', 0, 0),
(10, 9, 'category', '', 0, 0),
(11, 10, 'post_tag', '', 0, 0),
(12,11,'category','',0,0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Blog', 'blog', 0),
(3, 'Image', 'image', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Main Portfolio', 'main-portfolio', 0),
(6, 'Testimonials', 'testimonials', 0),
(7, 'Team','team', 0),
(8, 'Services','services', 0),
(9, 'Facebook Post','facebook-post', 0),
(10, 'Facebook','facebook', 0),
(11,'home slider','home-slider',0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0);
E;






if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US');
E;






if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('feather_bgtexture_url', 'Default', 'yes'),
('feather_header_font', 'Kreon', 'yes'),
('feather_body_font', 'Droid Sans', 'yes'),
('feather_logo', '', 'yes'),
('feather_quote_one','','yes'),
('feather_favicon', '', 'yes'),
('feather_bgcolor', '', 'yes'),
('feather_bgimage', '', 'yes'),
('feather_catnum_posts', '6', 'yes'),
('feather_archivenum_posts', '5', 'yes'),
('feather_searchnum_posts', '5', 'yes'),
('feather_tagnum_posts', '5', 'yes'),
('feather_date_format', 'M j, Y', 'yes'),
('feather_new_thumb_method', 'on', 'yes'),
('feather_show_control_panel', 'false', 'yes'),
('feather_display_media', 'on', 'yes'),
('feather_header_font_color', '', 'yes'),
('feather_body_font_color', '', 'yes'),
('feather_blog_style', 'false', 'yes'),
('feather_grab_image', 'false', 'yes'),
('feather_rss_url', '', 'yes'),
('feather_use_excerpt', 'false', 'yes'),
('feather_display_recentblog_section', 'on', 'yes'),
('feather_display_recentwork_section', 'on', 'yes'),
('feather_home_recentblog_section', 'Blog', 'yes'),
('feather_homepage_posts', '7', 'yes'),
('feather_featured', 'on', 'yes'),
('feather_duplicate', 'on', 'yes'),
('feather_slider_type', 'cycle', 'yes'),
('feather_featured_num', '3', 'yes'),
('feather_slider_autospeed', '7000', 'yes'),
('feather_slider_auto', 'on', 'yes'),
('feather_disable_toptier', 'false', 'yes'),
('feather_page_thumbnails', 'false', 'yes'),
('feather_show_pagescomments', 'false', 'yes'),
('feather_custom_colors', 'false', 'yes'),
('feather_child_css', 'false', 'yes'),
('feather_child_cssurl', '', 'yes'),
('feather_footer_text', '', 'yes'),
('feather_color_footerlinks', '', 'yes'),
('feather_integration_head', '', 'yes'),
('feather_integration_body', '', 'yes'),
('feather_integration_single_top', '', 'yes'),
('feather_integration_single_bottom', '', 'yes'),
('feather_468_enable', 'false', 'yes'),
('feather_468_image', '', 'yes'),
('feather_468_url', '', 'yes'),
('feather_468_adsense', '', 'yes'),
('category_children', 'a:0:{}', 'yes'),
('feather_integrate_header_enable', 'on', 'yes'),
('feather_integrate_body_enable', 'on', 'yes'),
('feather_integrate_singletop_enable', 'on', 'yes'),
('feather_integrate_singlebottom_enable', 'on', 'yes'),
('feather_posts_media', '10', 'yes'),
('feather_enable_dropdowns', 'on', 'yes'),
('feather_home_link', 'on', 'yes'),
('feather_sort_pages', 'post_title', 'yes'),
('feather_order_page', 'asc', 'yes'),
('feather_tiers_shown_pages', '3', 'yes'),
('feather_enable_dropdowns_categories', 'on', 'yes'),
('feather_categories_empty', 'on', 'yes'),
('feather_tiers_shown_categories', '3', 'yes'),
('feather_sort_cat', 'name', 'yes'),
('feather_order_cat', 'asc', 'yes'),
('feather_postinfo2', 'a:1:{i:0;s:4:"date";}', 'yes'),
('feather_thumbnails', 'on', 'yes'),
('feather_show_postcomments', 'false', 'yes'),
('feather_postinfo1', 'a:1:{i:0;s:4:"date"', 'yes'),
('feather_thumbnails_index', 'on', 'yes'),
('feather_color_mainfont', '', 'yes'),
('feather_color_mainlink', '', 'yes'),
('feather_color_pagelink', '', 'yes'),
('feather_color_pagelink_active', '', 'yes'),
('feather_color_headings', '', 'yes'),
('feather_color_sidebar_links', '', 'yes'),
('feather_homeblog_desc', '', 'yes'),
('feather_homework_desc', '', 'yes'),
('feather_display_blurbs', 'false', 'yes'),
('feather_quote', 'on', 'yes'),
('feather_feat_cat', 'home slider', 'yes'),
('feather_use_pages', 'false', 'yes'),
('theme_mods_feather', 'a:1:{s:18:"nav_menu_locations";a:2:{s:12:"primary-menu";i:4;s:11:"footer-menu";i:4;}}', 'yes'),
('nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:1:{i:0;i:4;}}', 'yes'),
('su_option_prefix', 'su_', 'yes'),
('woocommerce_permalinks', 'false', 'yes');
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
