<?php
$config = array(
    'widget_text' => array(
        array(
		    'title' => 'See also',
		    'text' => '[pagelist]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Contact us',
		    'text' => '[otonomic_contact_details]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Our Address',
		    'text' => '[otonomic_map]',
		    'filter' => ''
	    ),
	    array(
		    'title' =>'Opening hours',
		    'text' => '[otonomic_option option="opening_hours" ]',
		    'filter' => ""
	    ),
	    array(
		    'title' => 'Get a Discount',
		    'text' => '<img src="http://placehold.it/260x152&amp;text=Image">',
		    'filter' => ""
	    ),
	    '_multiwidget'=>1
    ),
    
    'widget_otonomic_header' => array(
        array(
            'blog_name' => '',
            'address' => '',
            'phone' => '',
            'email' => ''
        ),
    '_multiwidget' => 1
    ),
    
    'widget_otonomic_contact' => array (
        array (
          'title' => 'Contact Us',
          'address' => get_option('address'),
          'phone' => get_option('phone'),
          'email' => get_option('admin_email'),
          'facebook' => get_option('facebook'),
          'twitter' => get_option('twitter'),
          'instagram' => get_option('instagram'),
          'youtube' => get_option('youtube'),
          'linkedin' => get_option('linkedin'),
          'template' => 'column1'
        ),
        '_multiwidget' => 1,
   ),
    
    'widget_otonomic_opening_hours' => array (
        array (
          'title' => 'Opening Hours',
          'template' => 'vertical',
          'source' => 'facebook',
        ),
        '_multiwidget' => 1,
    ),
    
    'widget_otonomic_sitemap' => array (
        array (
          'title' => 'See Also',
        ),
        '_multiwidget' => 1,
    ),
    
   'widget_otonomic_google_map' => array (
        array (
          'title' => 'Our Address',
        ),
        '_multiwidget' => 1,
    ),
    
	'sidebars_widgets'=> array(
		'footer-sidebar-1'=>array('otonomic_sitemap-0'),
		'footer-sidebar-2' => array('otonomic_contact-0'),
		'footer-sidebar-3' => array('otonomic_google_map-0'),
		'footer-sidebar-4' => array('otonomic_opening_hours-0'),
		'display-everywhere-sidebar' => array(),
                'header-2'=> array('otonomic_header-0'),
		'array_version' => 3
	),
);
return $config;
