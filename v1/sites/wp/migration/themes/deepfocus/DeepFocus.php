<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Deepfocus extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'deepfocus';
    }

    public function install(){
        parent::install();

        /* Load the child theme now */
        if($this->theme == 'deepfocus') {
            update_option('current_theme', 'Deepfocuschild');
            update_option('stylesheet', 'deepfocuschild');
        }

        return $this->blog_id;
    }

    function set_pages_settings() {
        $data = [
            'about' => $this->get_full_width_settings_array(),

            'contact' => $this->get_full_width_settings_array(),

            'social' => $this->get_full_width_settings_array(),

            'portfolio' => [
                '_wp_page_template' => 'page-gallery.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 1,
                    'et_ptemplate_gallerycats' => array(3),
                    'et_ptemplate_gallery_perpage' => 12,
                    'et_ptemplate_showtitle' => 0,
                    'et_ptemplate_showdesc' => 0,
                    'et_ptemplate_detect_portrait' => 1,
                    'et_ptemplate_imagesize' => 2
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ],

            'blog' => [
                '_wp_page_template' => 'page-blog.php',
                'et_ptemplate_settings' => [
                    'et_fullwidthpage' => 0,
                    'et_ptemplate_blogstyle' => 0,
                    'et_ptemplate_showthumb' => 0,
                    'et_ptemplate_gallerycats' => array(2),
                    'et_ptemplate_blog_perpage' => 6
                ],
                'et_feather_settings' => [
                    'et_is_featured' => 0
                ]
            ]
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    public function save_theme_data(){
        $this->update_options();

        $this->set_pages_settings();

        // $this->set_widgets();
        $this->set_text_widgets_footer();

        $this->set_quote_area([
            'h1' => $this->page->facebook->about,
            'h2' => ''
        ]);

        $this->set_menu();
    }




    public function set_quote_area($data) {
        if(isset($data['h1'])) {
            update_option('deepfocus_quote_one', $data['h1']);
        }
        if(isset($data['h2'])) {
            update_option('deepfocus_quote_two', $data['h2']);
        }
    }

    protected function update_options()
    {
        try {
            parent::update_options();

            update_option('deepfocus_feat_cat', '11');
            update_option('deepfocus_home_page_1', 'about');
            update_option('deepfocus_home_page_2', '');
            update_option('deepfocus_portfolio_cat', '5');
            update_option('posts_per_page', '5');

        } catch(Exception $e) {
        }
    }
}
