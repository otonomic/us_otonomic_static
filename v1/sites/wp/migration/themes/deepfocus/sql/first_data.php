<?php
$link = $domain .'/'. $page->username;
set_time_limit(0);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 7),
(5, 5, 'portfolio-category', '', 0, 0),
(6, 6, 'testimonial-category', '', 0, 0),
(7, 7, 'testimonial-category', '', 0, 0),
(8, 8, 'highlight-category', '', 0, 0),
(9, 1, 'portfolio-category', '', 0, 0),
(10, 9, 'category', '', 0, 0),
(11, 10, 'post_tag', '', 0, 0),
(12,11,'category','',0,0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Blog', 'blog', 0),
(3, 'Image', 'image', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Main Portfolio', 'main-portfolio', 0),
(6, 'Testimonials', 'testimonials', 0),
(7, 'Team','team', 0),
(8, 'Services','services', 0),
(9, 'Facebook Post','facebook-post', 0),
(10, 'Facebook','facebook', 0),
(11,'home slider','home-slider',0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0);
E;






if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
('5644', '2333', '_additional_settings', ''),
('5645', '2333', '_locale', 'en_US');
E;






if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
('woocommerce_permalinks', 'false', 'yes'),
('deepfocus_color_scheme', 'Default', 'yes'),
('deepfocus_blog_cat', '3', 'yes'),
('deepfocus_catnum_posts', '12', 'yes'),
('deepfocus_archivenum_posts', '12', 'yes'),
('deepfocus_searchnum_posts', '12', 'yes'),
('deepfocus_tagnum_posts', '12', 'yes'),
('deepfocus_date_format', 'M j, Y', 'yes'),
('deepfocus_cufon', 'on', 'yes'),
('deepfocus_quote', 'on', 'yes'),
('deepfocus_fromblog_number', '9', 'yes'),
('deepfocus_portfolio_number', '8', 'yes'),
('deepfocus_homepage_posts', '7', 'yes'),
('deepfocus_featured', 'on', 'yes'),
('deepfocus_featured_num', '5', 'yes'),
('deepfocus_slider_auto', 'on', 'yes'),
('deepfocus_slider_autospeed', '3500', 'yes'),
('deepfocus_slider_effect', 'fade', 'yes'),
('deepfocus_enable_dropdowns', 'on', 'yes'),
('deepfocus_home_link', 'on', 'yes'),
('deepfocus_sort_pages', 'post_title', 'yes'),
('deepfocus_order_page', 'asc', 'yes'),
('deepfocus_tiers_shown_pages', '3', 'yes'),
('deepfocus_menucats', 'a:1:{i:0;i:1;}', 'yes'),
('deepfocus_enable_dropdowns_categories', 'on', 'yes'),
('deepfocus_categories_empty', 'on', 'yes'),
('deepfocus_tiers_shown_categories', '3', 'yes'),
('deepfocus_sort_cat', 'name', 'yes'),
('deepfocus_order_cat', 'asc', 'yes'),
('deepfocus_postinfo2', 'a:4:{i:0;s:6:"author";i:1;s:4:"date";i:2;s:10:"categories";i:3;s:8:"comments";}', 'yes'),
('deepfocus_blog_thumbnails', 'on', 'yes'),
('deepfocus_show_postcomments', 'on', 'yes'),
('deepfocus_gallery_thumbnails', 'on', 'yes'),
('deepfocus_postinfo1', 'a:4:{i:0;s:6:"author";i:1;s:4:"date";i:2;s:10:"categories";i:3;s:8:"comments";}', 'yes'),
('deepfocus_thumbnails_index', 'on', 'yes'),
('deepfocus_seo_home_type', 'BlogName | Blog description', 'yes'),
('deepfocus_seo_home_separate', ' | ', 'yes'),
('deepfocus_seo_single_field_title', 'seo_title', 'yes'),
('deepfocus_seo_single_field_description', 'seo_description', 'yes'),
('deepfocus_seo_single_field_keywords', 'seo_keywords', 'yes'),
('deepfocus_seo_single_type', 'Post title | BlogName', 'yes'),
('deepfocus_seo_single_separate', ' | ', 'yes'),
('deepfocus_seo_index_type', 'Category name | BlogName', 'yes'),
('deepfocus_seo_index_separate', ' | ', 'yes'),
('deepfocus_integrate_header_enable', 'on', 'yes'),
('deepfocus_integrate_body_enable', 'on', 'yes'),
('deepfocus_integrate_singletop_enable', 'on', 'yes'),
('deepfocus_integrate_singlebottom_enable', 'on', 'yes'),
('deepfocus_use_pages', 'false', 'yes'),
('deepfocus_logo', '', 'yes'),
('deepfocus_favicon', '', 'yes'),
('deepfocus_blog_style', 'false', 'yes'),
('deepfocus_grab_image', 'false', 'yes'),
('deepfocus_use_excerpt', 'false', 'yes'),
('deepfocus_responsive_layout', 'on', 'yes'),
('deepfocus_responsive_shortcodes', 'on', 'yes'),
('deepfocus_gf_enable_all_character_sets', 'false', 'yes'),
('deepfocus_custom_css', '', 'yes'),
('deepfocus_home_page_3', '676', 'yes'),
('deepfocus_duplicate', 'false', 'yes'),
('deepfocus_swap_navbar', 'false', 'yes'),
('deepfocus_disable_toptier', 'false', 'yes'),
('deepfocus_page_thumbnails', 'on', 'yes'),
('deepfocus_show_pagescomments', 'false', 'yes'),
('deepfocus_custom_colors', 'false', 'yes'),
('deepfocus_child_css', 'false', 'yes'),
('deepfocus_child_cssurl', '', 'yes'),
('deepfocus_color_bgcolor', '', 'yes'),
('deepfocus_color_mainfont', '', 'yes'),
('deepfocus_color_mainlink', '', 'yes'),
('deepfocus_color_pagelink', '', 'yes'),
('deepfocus_color_catslink', '', 'yes'),
('deepfocus_color_sidebar_titles', '', 'yes'),
('deepfocus_color_footer_titles', '', 'yes'),
('deepfocus_color_footer_links', '', 'yes'),
('deepfocus_seo_home_title', 'false', 'yes'),
('deepfocus_seo_home_description', 'false', 'yes'),
('deepfocus_seo_home_keywords', 'false', 'yes'),
('deepfocus_seo_home_canonical', 'false', 'yes'),
('deepfocus_seo_home_titletext', '', 'yes'),
('deepfocus_seo_home_descriptiontext', '', 'yes'),
('deepfocus_seo_home_keywordstext', '', 'yes'),
('deepfocus_seo_single_title', 'false', 'yes'),
('deepfocus_seo_single_description', 'false', 'yes'),
('deepfocus_seo_single_keywords', 'false', 'yes'),
('deepfocus_seo_single_canonical', 'false', 'yes'),
('deepfocus_seo_index_canonical', 'false', 'yes'),
('deepfocus_seo_index_description', 'false', 'yes'),
('deepfocus_integration_head', '', 'yes'),
('deepfocus_integration_body', '', 'yes'),
('deepfocus_integration_single_top', '', 'yes'),
('deepfocus_integration_single_bottom', '', 'yes'),
('deepfocus_468_enable', 'false', 'yes'),
('deepfocus_468_image', '', 'yes'),
('deepfocus_468_url', '', 'yes'),
('deepfocus_468_adsense', '', 'yes'),
('visit_count', '1', 'yes'),
('nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes');
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);
