<?php
function get_theme_options() {
	return
		array (
			'general' =>
				array (
					'logo' => 'true',
					'logo-url' => '',
					'retina-logo-url' => '',
					'footer-logo-url' => '',
					'enable-favicon' => 'true',
					'favicon-url' => '',
					'\'apple-favicon\'' => '',
					'\'apple-retina-favicon\'' => '',
					'\'apple-ipad-favicon\'' => '',
					'\'apple-ipad-retina-favicon\'' => '',
					'enable-sticky-nav' => 'false',
					'disable-page-comment' => 'true',
					'disable-custom-scroll' => 'on',
                                        'disable-picker' => 'true',
					'h2-phoneno' => '',
					'h2-emailid' => '',
					'google-font-subset' => '',
					'mailchimp-key' => '',
					'show-sociables' => 'on',
					'show-footer' => 'on',
					'footer-columns' => '4',
					'copyright-text' => '&copy; 2014.',
				),
			'social' =>
				array (
					'social-1' =>
						array (
							'icon' => 'flickr.png',
							'link' => '#',
						),
					'social-2' =>
						array (
							'icon' => 'google.png',
							'link' => '#',
						),
					'social-3' =>
						array (
							'icon' => 'technorati.png',
							'link' => '#',
						),
				),
			'appearance' =>
				array (
					'header_type' => 'header1',
					'header-bg-color' => '#',
					'enable-top-bar' => 'true',
					'topbar-bg-color' => '#',
					'topbar-primary-color' => '#',
					'topbar-border-color' => '#',
					'topbar-text-color' => '#',
					'topbar-secondary-color' => '#',
					'disable-menu-settings' => 'true',
					'menu-standard-font' => '',
					'menu-standard-font-style' => '',
					'menu-font' => '',
					'menu-font-size' => '',
					'menu-primary-color' => '#',
					'menu-bg-color' => '#',
					'menu-secondary-color' => '#',
					'smenu-bg-color' => '#',
					'menu-bg' => '',
					'menu-bg-repeat' => '',
					'menu-bg-position' => '',
					'disable-boddy-settings' => 'true',
					'body-standard-font' => '',
					'body-standard-font-style' => '',
					'body-font' => '',
					'body-font-color' => '#',
					'body-font-size' => '',
					'body-primary-color' => '#',
					'body-secondary-color' => '#',
					'footer-title-standard-font' => '',
					'footer-title-standard-font-style' => '',
					'footer-title-font' => '',
					'footer-title-font-color' => '#',
					'footer-font-size' => '',
					'footer-content-standard-font' => '',
					'footer-content-standard-font-style' => '',
					'footer-content-font' => '',
					'footer-content-font-color' => '#',
					'footer-content-font-size' => '',
					'footer-primary-color' => '#',
					'footer-secondary-color' => '#',
					'footer-bg-color' => '#',
					'copyright-bg-color' => '#',
					'disable-typography-settings' => 'true',
					'H1-standard-font' => '',
					'H1-standard-font-style' => '',
					'H1-font' => '',
					'H1-font-color' => '#',
					'H1-size' => '',
					'H2-standard-font' => '',
					'H2-standard-font-style' => '',
					'H2-font' => '',
					'H2-font-color' => '#',
					'H2-size' => '',
					'H3-standard-font' => '',
					'H3-standard-font-style' => '',
					'H3-font' => '',
					'H3-font-color' => '#',
					'H3-size' => '',
					'H4-standard-font' => '',
					'H4-standard-font-style' => '',
					'H4-font' => '',
					'H4-font-color' => '#',
					'H4-size' => '',
					'H5-standard-font' => '',
					'H5-standard-font-style' => '',
					'H5-font' => '',
					'H5-font-color' => '#',
					'H5-size' => '',
					'H6-standard-font' => '',
					'H6-standard-font-style' => '',
					'H6-font' => '',
					'H6-font-color' => '#',
					'H6-size' => '',
					'layout' => 'wide',
					'bg-type' => 'bg-patterns',
					'boxed-layout-pattern' => '',
					'boxed-layout-pattern-repeat' => '',
					'boxed-layout-pattern-color' => '#',
					'boxed-layout-pattern-opacity' => '',
					'boxed-layout-bg' => '',
					'boxed-layout-bg-repeat' => '',
					'boxed-layout-bg-position' => '',
					'boxed-layout-bg-color' => '#',
					'boxed-layout-bg-opacity' => '',
					'skin' => 'chocolate',
				),
			'integration' =>
				array (
					'header-code' => '',
					'body-code' => '',
					'custom-css' => '',
					'single-post-top-code' => '',
					'single-post-bottom-code' => '',
					'post-googleplus-layout' => 'small',
					'post-googleplus-lang' => 'en_GB',
					'post-fb_like-layout' => 'box_count',
					'post-fb_like-color-scheme' => 'light',
					'post-digg-layout' => 'DiggWide',
					'post-stumbleupon-layout' => '5',
					'post-linkedin-layout' => '2',
					'post-pintrest-layout' => 'none',
					'post-delicious-text' => '',
					'post-twitter-username' => '',
					'post-twitter-layout' => 'vertical',
					'post-twitter-lang' => '',
					'single-page-top-code' => '',
					'single-page-bottom-code' => '',
					'page-googleplus-layout' => 'small',
					'page-googleplus-lang' => 'en_GB',
					'page-fb_like-layout' => 'box_count',
					'page-fb_like-color-scheme' => 'light',
					'page-digg-layout' => 'DiggWide',
					'page-stumbleupon-layout' => '5',
					'page-linkedin-layout' => '2',
					'page-pintrest-layout' => 'none',
					'page-delicious-text' => '',
					'page-twitter-username' => '',
					'page-twitter-layout' => 'none',
					'page-twitter-lang' => '',
				),
			'specialty' =>
				array (
					'author-archives-layout' => 'content-full-width',
					'author-archives-post-layout' => 'one-half-column',
					'category-archives-layout' => 'content-full-width',
					'category-archives-post-layout' => 'one-half-column',
					'tag-archives-layout' => 'content-full-width',
					'tag-archives-post-layout' => 'one-half-column',
					'gallery-archives-layout' => 'content-full-width',
					'gallery-archives-post-layout' => 'one-third-column',
					'search-layout' => 'content-full-width',
					'search-post-layout' => 'one-half-column',
					'404-layout' => 'content-full-width',
                    '404-message' => '<h2> Oops! </h2>
<h3> The page you\'re looking for cannot be found. </h3>
<p><a href="/">Click here</a> to return to the home page or <a href="support@otonomic.com">contact us</a>.</p>',
					'message-font' => '',
					'message-font-color' => '#',
					'message-font-size' => '',
				),
			'mobile' =>
				array (
					'is-theme-responsive' => 'true',
				),
			'advance' =>
				array (
					'buddhapanel-logo-url' => '',
				),
			'company' =>
				array (
					'dt_company_monday_start' => '08:00',
					'dt_company_monday_end' => '08:45',
					'dt_company_tuesday_start' => '08:00',
					'dt_company_tuesday_end' => '08:45',
					'dt_company_wednesday_start' => '08:00',
					'dt_company_wednesday_end' => '08:45',
					'dt_company_thursday_start' => '08:00',
					'dt_company_thursday_end' => '08:45',
					'dt_company_friday_start' => '08:00',
					'dt_company_friday_end' => '09:45',
					'dt_company_saturday_start' => '',
					'dt_company_saturday_end' => '00:00',
					'dt_company_sunday_start' => '',
					'dt_company_sunday_end' => '00:00',
					'currency' => 'USD',
					'enable-pay-at-arrival'=>'true',
					'api-username' => '',
					'api-password' => '',
					'api-signature' => '',
					'notification_sender_name' => 'otonomic',
					'notification_sender_email' => 'support@otonomic.com',
					'appointment_notification_to_staff_subject' => 'Hi [STAFF_NAME] , New booking information ( Booking id: [APPOINTMENT_ID] )',
					'appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
<p> Your new Booking id : [APPOINTMENT_ID] </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p>Client Name: [CLIENT_NAME]</p>
<p>Client Phone: [CLIENT_PHONE]</p>
<p>Client Email: [CLIENT_EMAIL]</p>
<p>[APPOINTMENT_BODY]</p>
',
					'modified_appointment_notification_to_staff_subject' => 'Hi [STAFF_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Modified',
					'modified_appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
<p> Your Booking id : [APPOINTMENT_ID]  was modified </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p>Client Name: [CLIENT_NAME]</p>
<p>Client Phone: [CLIENT_PHONE]</p>
<p>Client Email: [CLIENT_EMAIL]</p>
<p>[APPOINTMENT_BODY]</p>
',
					'deleted_appointment_notification_to_staff_subject' => 'Hi [STAFF_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Deleted / Declined',
					'deleted_appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
<p> Booking id : [APPOINTMENT_ID]  was Deleted / Declined </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p>Client Name: [CLIENT_NAME]</p>
<p>Client Phone: [CLIENT_PHONE]</p>
<p>Client Email: [CLIENT_EMAIL]</p>
<p>[APPOINTMENT_BODY]</p>
',
					'agenda_to_staff_subject' => 'Hi [STAFF_NAME] , Your Agenda for [TOMORROW]',
					'agenda_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
<p>Your agenda for tomorrow is </p>
<p>[TOMORROW_AGENDA]</p>
',
					'appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , New booking information ( Booking id: [APPOINTMENT_ID] )',
					'appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
<p> Your new Booking id : [APPOINTMENT_ID] </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p> Amount to pay : [AMOUNT]</p>
<p>[APPOINTMENT_BODY]</p>
</p>
<p>Thank you for choosing our company.</p>
',
					'modified_appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Modified',
					'modified_appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
<p> Your Booking id : [APPOINTMENT_ID]  was modified </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p> Amount to pay : [AMOUNT]</p>
<p>[APPOINTMENT_BODY]</p>
</p>
<p>Thank you for choosing our company.</p>
',
					'deleted_appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Deleted / Declined',
					'deleted_appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
<p> Your Booking id : [APPOINTMENT_ID]  was Deleted / Declined </p>
<p> Service: [SERVICE]</p>
<p> Date &#038; Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
<p>[APPOINTMENT_BODY]</p>
',
					'appointment_success' => '[dt_sc_titled_box type="success-box"]Success. You got a appointment to experience our excellent service. [/dt_sc_titled_box]',
					'appointment_error' => '[dt_sc_titled_box type="error-box"] Oops! You have cancelled the payment process :([/dt_sc_titled_box]',
				),
		);
}