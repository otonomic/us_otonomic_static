<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Dreamspa extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
	    $this->theme = 'dreamspa';
	    $this->modules = array(
		    'about',
		    'home',
		    'testimonials',
		    'gallery',
		    'contact',
		    'buzz',
		    //'services',
		    'reservation',
		    //'store',
		    'blog',
	    );
        parent::__construct($blog_id, $page, $fb_category_object);

    }


	/* add_module_ functions */
	function add_module_home()
	{
		$this->add_section_home();
	}
	function add_module_services()
	{
		$this->add_section_services();
	}
	function add_module_reservation()
	{
		$this->add_section_reservation();
		$services_id = $this->add_default_services();
		$this->add_default_staff( $services_id );
	}
	function add_module_gallery()
	{
		$this->add_section_gallery();
	}
	function add_module_testimonials()
	{
		$this->add_section_testimonials();
	}
	function add_module_buzz()
	{
		$this->add_section_buzz();
	}

	/* add_section_ functions */
	public function add_section_buzz()
	{
		$data_post = array(
			'post_title'   => 'Buzz',
			'post_content' => $this->get_content_buzz(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 40,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}
        public function add_default_products(){
            $wpotonomic_product = wpotonomic_product::get_instance();
            $WPOtonomicProduct = new WPOtonomicProduct();
            for($i = 1; $i <= 4 ; $i++){
                $WPOtonomicProduct = new WPOtonomicProduct();
                $WPOtonomicProduct->type = 'simple';
                $WPOtonomicProduct->id = '0';
                $WPOtonomicProduct->sku = 'sku-00'.$i;
                $WPOtonomicProduct->title = 'Product '.$i;
                $WPOtonomicProduct->description = 'This is your sample product. Edit it and add more like it to your store!';
                $WPOtonomicProduct->price = '10';
                $WPOtonomicProduct->category = 0;
                $wpotonomic_product->save($WPOtonomicProduct);
            }
        }
	public function add_default_staff( $services_id )
	{
		$user = wp_get_current_user();
		$data_post = array(
			'post_title'   => $user->display_name,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'dt_staffs',
		);
		$data_postmeta = array(
			'_services'=>$services_id,
			'_timer'=>array (
				'monday_start' => '08:00',
				'monday_end' => '17:00',
				'tuesday_start' => '08:00',
				'tuesday_end' => '17:00',
				'wednesday_start' => '08:00',
				'wednesday_end' => '17:00',
				'thursday_start' => '08:00',
				'thursday_end' => '17:00',
				'friday_start' => '08:00',
				'friday_end' => '18:00',
				'saturday_start' => '',
				'saturday_end' => '00:00',
				'sunday_start' => '',
				'sunday_end' => '00:00',
			)
		);
		ot_insert_post( $data_post , $data_postmeta);
	}
	public function add_default_services()
	{
		$services_id = array();
		$services = array(
                    array(
                        'title' => 'Hair Cutting',
                        'image'=> content_url( '/themes/dreamthemechild/images/service/services-1.png'),
                        'content'=>
                            <<<E
                                    <ul class="with-bullets">
                                        <li>Women’s Haircut<span class="pull-right"></span></li>
                                        <li>Women’s Haircut & Style<span class="pull-right"></span></li>
                                        <li>Women’s Haircut, Wash & Style<span class="pull-right"></span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Men’s Haircut<span class="pull-right"></span></li>
                                        <li class="hidden">Child Haircut<span class="pull-right"></span></li>
                                    </ul>
E
                    ),
                    array(
                        'title' => 'Hair Coloring',
                        'image'=> content_url( '/themes/dreamthemechild/images/service/services-2.png'),
                        'content' => <<<E
                                    <ul class="with-bullets">
                                        <li>Single Process<span class="pull-right"> </span></li>
                                        <li>Double Process<span class="pull-right"> </span></li>
                                        <li>Full Highlights<span class="pull-right"> </span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Partial Highlights<span class="pull-right"></span></li>
                                        <li class="hidden">Corrective Color<span class="pull-right"></span></li>
                                    </ul>
E
                    ),
                    array(
                        'title' => 'Hair Styling',
                        'image'=> content_url( '/themes/dreamthemechild/images/service/services-3.png'),
                        'content' => <<<E
                                    <ul class="with-bullets">
                                        <li>Shampoo & Style<span class="pull-right"></span></li>
                                        <li>Blow Dry & Style<span class="pull-right"></span></li>
                                        <li>Flat or Curling Iron<span class="pull-right"></span></li>
                                        <li class="js-more more">More <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></li>
                                        <li class="hidden">Partial Updo<span class="pull-right"></span></li>
                                        <li class="hidden">Updo<span class="pull-right"></span></li>
                                    </ul>
E
                    )
		);

		$services = $this->fb_category_object->get_services();

		foreach($services as $service)
		{
			$data_post = array(
				'post_title'   => $service['title'],
				'post_content' => $service['content'],
				'post_status'  => 'publish',
				'post_type'    => 'service',
			);
			$data_postmeta = array(
				'_info'=>array (
					'price' => '30',
					'duration' => '1800',
				)
			);
			$post_id = ot_insert_post( $data_post , $data_postmeta);
			$services_id[] = $post_id;
            $dummy_image = new stdClass();
            $dummy_image->source =  $service['image'];
            $dummy_image->picture = $dummy_image->source;
            $attachment_id = $this->add_image_remote($dummy_image, $post_id);
            set_post_thumbnail( $post_id, $attachment_id );
		}
		return $services_id;
	}

	public function add_section_testimonials()
	{
		$data_post = array(
			'post_title'   => 'Testimonials',
			'post_content' => $this->get_content_testimonials(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 30,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}
	public function add_section_contact() {
		$data_post = array(
			'post_title'   => 'Contact',
			'post_content' => $this->get_content_contact(),
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 60,
		);
		$data_postmeta = array(
		);

		$page_id = ot_insert_post( $data_post , $data_postmeta);

		return $page_id;
	}
	function add_section_blog()
	{
		$content = '';
		$data_post = array(
				'post_title'   => 'News',
				'post_content' => $content,
				'post_name'    => 'news',
				'post_status'  => 'publish',
				'post_type'    => 'page',
				'menu_order'   => 70,
			);
		$data_postmeta = array();
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_gallery()
	{
		$content = $this->get_content_gallery();

		// Save content
		$data_post = array(
			'post_title'   => 'Gallery',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 50
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_reservation()
	{
		$content = $this->get_content_reservation();

		// Save content
		$data_post = array(
			'post_title'   => 'Make an Appointment',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 40
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_services()
	{
		$content = $this->get_content_services();

		// Save content
		$data_post = array(
			'post_title'   => 'Services',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 30
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	function add_section_home()
	{
		$content = $this->get_content_home();

		// Save content
		$data_post = array(
			'post_title'   => 'Home',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 10
		);
		$data_postmeta = array(
		);
		$page_id = ot_insert_post( $data_post , $data_postmeta);
		return $page_id;
	}
	/* Content functions */
	function get_content_buzz()
	{
		$content = '';
		$content .= <<<E
[otonomic_dpSocialTimeline]
E;
		return $content;
	}
	function get_content_testimonials()
	{
		$content = '';
		$content .= <<<E
[otonomic_dt_testimonials disable_heading='yes']
E;
		return $content;
	}
	function get_content_contact() {
		return $content = <<<E
		[otonomic_contact_page]
E;
	}
	function get_content_gallery()
	{
		$content = '';
		$content .= <<<E
[otonomic_dt_gallery limit="64" read_more_enabled="off" title_enabled="off"]
E;
		return $content;
	}

	function get_content_reservation()
	{
		$content = '';
		$content .= <<<E

E;
		return $content;
	}

	function get_content_services()
	{
		$content = <<<E
[otonomic_dt_services]
E;
		return $content;
	}

	function get_slider_images()
	{
		$slider_images = array();
		$images = array('slide-2.jpg', 'slide-1.jpg');
		foreach($images as $image)
		{
			$dummy_image = new stdClass();
			$dummy_image->source = content_url('/themes/dreamspachild/images/slider/' . $image);
			$dummy_image->picture = $dummy_image->source;

			$slider_images[] = $dummy_image;
		}
		return $slider_images;
	}

    function get_slider_options() {
        return [
            'min_width'                  => 1300,
            // 'min_width'                  => 1000,
//            'min_w2h_ratio'              => 1.3,
            'max_num_main_slider_images' => 5,
            'min_num_main_slider_images' => 1,
            'width1' => 1200,
            'width2' => 1300,
        ];
    }

    function get_slider_shortcode()
	{
		$slider_images_facebook = $this->get_images_for_slider( $this->page->albums);

		//$slider_images_stock = $this->get_slider_images();
		$slider_images_stock = $this->fb_category_object->get_splash_images();

        $slider_images = array_merge($slider_images_facebook, $slider_images_stock);

		/* this theme uses layer slider so lets add slides to it */
		$slider_data = array (
			'properties' =>
				array (
					'post_taxonomy' => '0',
					'post_orderby' => 'date',
					'post_order' => 'DESC',
					'post_offset' => '-1',
					'title' => 'Homepage',
					'width' => '1600',
					'height' => '769',
					'responsive' => 'on',
					'maxwidth' => '',
					'responsiveunder' => '0',
					'sublayercontainer' => '0',
					'autostart' => 'on',
					'pauseonhover' => 'on',
					'firstlayer' => '1',
					'animatefirstlayer' => 'on',
					'keybnav' => 'on',
					'touchnav' => 'on',
					'loops' => '0',
					'forceloopnum' => 'on',
					'skin' => 'v5',
					'backgroundcolor' => '',
					'backgroundimage' => '',
					'sliderstyle' => 'margin-bottom: 0px;',
					'navprevnext' => 'on',
					'navstartstop' => 'on',
					'navbuttons' => 'on',
					'hoverprevnext' => 'on',
					'circletimer' => 'on',
					'thumb_nav' => 'hover',
					'thumb_container_width' => '60%',
					'thumb_width' => '100',
					'thumb_height' => '60',
					'thumb_active_opacity' => '35',
					'thumb_inactive_opacity' => '100',
					'autoplayvideos' => 'on',
					'autopauseslideshow' => 'auto',
					'youtubepreview' => 'maxresdefault.jpg',
					'imgpreload' => 'on',
					'lazyload' => 'on',
					'yourlogoId' => '',
					'yourlogo' => '',
					'yourlogostyle' => 'left: -10px; top: -10px;',
					'yourlogolink' => '',
					'yourlogotarget' => '_self',
					'cbinit' => 'function(element) {

}',
					'cbstart' => 'function(data) {

}',
					'cbstop' => 'function(data) {

}',
					'cbpause' => 'function(data) {

}',
					'cbanimstart' => 'function(data) {

}',
					'cbanimstop' => 'function(data) {

}',
					'cbprev' => 'function(data) {

}',
					'cbnext' => 'function(data) {

}',
				),
			'layers' => array (),
		);
		if(count($slider_images)>0)
		{
			$slide_count=0;
			foreach($slider_images as $slider_image)
			{       
                                if(!$slider_image){
                                    continue;
                                }
				$attachment_id = $this->add_image_remote($slider_image, 0);
				$slide_count++;
				$slider_data['layers'][]= array (
					'properties' =>
						array (
							'post_offset' => '-1',
							'3d_transitions' => '',
							'2d_transitions' => '5',
							'custom_3d_transitions' => '',
							'custom_2d_transitions' => '',
							'backgroundId' => $attachment_id,
							'background' => $slider_image->source,
							'thumbnailId' => $attachment_id,
							'thumbnail' => $slider_image->source,
							'slidedelay' => '4000',
							'timeshift' => '0',
							'layer_link' => '',
							'layer_link_target' => '_self',
							'id' => 'otonomic_slide'.$slide_count.'_homepage',
							'deeplink' => '',
						),
				);

			}
		}
		$slider_data_json = json_encode($slider_data);
		/* Now insert data in table */
		global $wpdb;
		/* Layer slider tables dosen't exists for new sites, issue in plugin itself! */
		if(!function_exists('layerslider_create_db_table')) {
			/* Load LayerSlider */
			require_once(ABSPATH . '/wp-content/plugins/LayerSlider/layerslider.php');
			require_once(ABSPATH . '/wp-content/plugins/LayerSlider/wp/activation.php');
		}
		layerslider_create_db_table();

		//$table = "wp_{$this->blog_id}_layerslider";
		$table_name = $wpdb->prefix . "layerslider";
		$now = time();
		$wpdb->insert(
			$table_name,
			array(
				'name'=>'Homepage',
				'data'=>$slider_data_json,
				'date_c'=>$now,
				'date_m'=>$now,
				'flag_hidden'=>0,
				'flag_deleted'=>0
			)
		);
		//
		$slider_id = $wpdb->insert_id;
		return '[layerslider id="'.$slider_id.'"]';
	}

	function get_content_about()
	{
		$content = parent::get_content_about();
		$content .= '<div id="about-team-wrapper" class="clearfix">[otonomic_team /]</div>';
		return $content;
	}

	function get_content_home()
	{
		$slider = $this->get_slider_shortcode();
		$about_page_obj = get_page_by_path('about');
		$about_page_url = get_permalink($about_page_obj->ID);

		$content = '';

//        if(!empty($this->page->facebook->about)){
            $about = <<<E
[otonomic_titles title="About" read_more_url="{$about_page_url}"][otonomic_option option="blogdescription"][/otonomic_titles]
E;
//        }

		$content .= <<<E
		<div id="home-slider">
                {$slider}
		</div>
		<div class="container hidden-xs">
			<div id="home-about">
                {$about}
			</div>
			<div id="home-services">
				[otonomic_dreamspa_services limit="6" type="carousel"]
			</div>
			<div id="home-testimonial">
				[otonomic_dt_testimonials sort_by="rating" limit="2" columns="2" word_length="30"]
			</div>
			<div id="home-gallery">
				[otonomic_dt_gallery limit="4"]
			</div>
		</div>

E;
		return $content;
	}

    function set_pages_settings() {
        $data = [
	        'home'          => $this->get_full_width_settings_array(),
            'about'         => $this->get_container_width_no_sidebar_settings_array() + $this->add_subtitle_settings(''),
            'testimonials' => $this->get_container_width_no_sidebar_settings_array(),
            'buzz' => $this->get_container_width_no_sidebar_settings_array(),
            'gallery'       => $this->get_container_width_no_sidebar_settings_array() + $this->add_subtitle_settings(''),
            //'contact'       => $this->get_container_width_no_sidebar_settings_array() + $this->add_subtitle_settings('[otonomic_contact_details type="one_line"]'),
            'contact'       => $this->get_full_width_settings_array() + $this->add_subtitle_settings('[otonomic_contact_details type="one_line"]'),
	        'services'      => $this->get_container_width_no_sidebar_settings_array() + $this->add_subtitle_settings(''),
	        'news'          => $this->get_blog_settings_array() + $this->add_subtitle_settings(''),
	        'make-an-appointment'   => $this->get_full_width_settings_array()
        ];

        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

	function get_gallery_settings_reservation()
	{
		$settings = [
			'_wp_page_template' => 'tpl-fullwidth.php',
			'_tpl_default_settings' => [
				'layout' => 'content-full-width',
			],
			'_dt_post_settings'=>[
				'layout' => 'content-full-width',
			]
		];
		if(!empty($subtitle))
		{
			$settings = $this->add_subtitle_settings($settings, $subtitle);
		}

		return $settings;
	}
	function get_gallery_settings_array($subtitle='')
	{
		$settings = [
			'_wp_page_template' => 'tpl-gallery.php',
			'_tpl_default_settings' => [
				'layout' => 'content-full-width',
				'gallery-post-layout' => 'one-third-column',
				'gallery-post-per-page' => '-1',
				'gallery-categories' =>
					array (
						0 => '',
					),
			],
			'_dt_post_settings'=>[
				'layout' => 'content-full-width',
			],
		];
		if(!empty($subtitle))
		{
			$settings = $this->add_subtitle_settings($settings, $subtitle);
		}
		return $settings;
	}
	function get_blog_settings_array($subtitle='')
	{
		$settings = [
			'_wp_page_template' => 'tpl-blog.php',
			'_tpl_default_settings' => [
				'layout' => 'with-right-sidebar',
				'blog-post-layout' => 'one-half-column',
				'blog-post-per-page' => '-1',
				'blog-post-excerpt-length' => '45',
				'blog-post-exclude-categories' =>
					array (
						0 => '',
					),
				'disable-date-info' => 'disable-date-info',
				'disable-author-info' => 'disable-author-info',
				'disable-comment-info' => 'disable-comment-info',
				'disable-category-info' => 'disable-category-info',
				'disable-tag-info' => 'disable-tag-info',
			],
			'_dt_post_settings'=>[
				'layout' => 'with-right-sidebar',
			]
		];
		if(!empty($subtitle))
		{
			$settings = $this->add_subtitle_settings($settings, $subtitle);
		}

		return $settings;
	}
	function get_full_width_settings_array() {
		$settings = [
			'_wp_page_template' => 'tpl-fullwidth.php',
			'_tpl_default_settings' => [
				'layout' => 'content-full-width',
			],
			'_dt_post_settings'=>[
				'layout' => 'content-full-width',
			]
		];

		return $settings;
	}
	function get_container_width_no_sidebar_settings_array($subtitle='') {
		$settings = [
			'_wp_page_template' => 'default',
			'_tpl_default_settings' => [
				'layout' => 'content-full-width',
			],
			'_dt_post_settings'=>[
				'layout' => 'content-full-width',
			]
		];
		if(!empty($subtitle))
		{
			$settings = $this->add_subtitle_settings($settings, $subtitle);
		}

		return $settings;
	}
	function get_right_sidebar_settings_array($subtitle='') {
		$settings = [
			'_wp_page_template' => 'default',
			'_tpl_default_settings' => [
				'layout' => 'with-right-sidebar',
			],
			'_dt_post_settings'=>[
				'layout' => 'with-right-sidebar',
			]
		];
		if(!empty($subtitle))
		{
			$settings = $this->add_subtitle_settings($settings, $subtitle);
		}

		return $settings;
	}
	function add_subtitle_settings($subtitle)
	{
        $settings = [];
		$settings['_dt_post_settings']['sub-title'] = $subtitle;
		$settings['_tpl_default_settings']['sub-title'] = $subtitle;

		return $settings;
	}

	/* Menu items */
	function get_menu_items()
	{
		$menu_items = array();

		$menu_items[] = array(
			'menu-item-title' => 'Home',
			'menu-item-url' => home_url( '/' ),
			/*'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',*/
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'About',
			'menu-item-object-id' => get_page_by_path('about')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Testimonials',
			'menu-item-object-id' => get_page_by_path('testimonials')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Buzz',
			'menu-item-object-id' => get_page_by_path('buzz')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Gallery',
			'menu-item-object-id' => get_page_by_path('gallery')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		$menu_items[] = array(
			'menu-item-title' => 'Contact',
			'menu-item-object-id' => get_page_by_path('contact')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);
		/*$menu_items[] = array(
			'menu-item-title' => 'Services',
			'menu-item-object-id' => get_page_by_path('services')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/
		/*$menu_items[] = array(
			'menu-item-title' => 'Reservation',
			'menu-item-object-id' => get_page_by_path('reservation')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/
		/* check if we need to add store */
		if(in_array('store',$this->modules))
		{
			/* Lets create store menu */
			$menu_items[] = array(
				'menu-item-title' => 'Store',
				'menu-item-object-id' => get_page_by_path('shop')->ID,
				'menu-item-object' => 'page',
				'menu-item-type' => 'post_type',
				'menu-item-status' => 'publish'
			);
		}


		/*$menu_items[] = array(
			'menu-item-title' => 'News',
			'menu-item-object-id' => get_page_by_path('news')->ID,
			'menu-item-object' => 'page',
			'menu-item-type' => 'post_type',
			'menu-item-status' => 'publish'
		);*/


		return $menu_items;
	}

	protected function add_album($fbalbum, $download_image=false, $album=0) {	
            if(!isset($fbalbum->photos->data)) { return; }

		$album_photos = $fbalbum->photos->data;

		if(empty($album_photos)) { return; }

		$my_album = array(
			'post_title'     => $fbalbum->name,
			'post_content'   => $fbalbum->description,
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status' 	 => 'closed',
			'post_type' 	 => 'dt_galleries',
			'guid'           => $fbalbum->id,
//            'tax_input'       => array('portfolio-category' => ($portfolio_category = 28)),
//            'filter'        => true
		);

		$post_date = $this->parse_date($fbalbum->created_time);
		$my_album['post_date'] = $post_date;
		$my_album['post_date_gmt'] = $post_date;
		$my_album['post_modified'] = $post_date;
		$my_album['post_modified_gmt'] = $post_date;

		//$post_id = wp_insert_post( $my_album );
		$post_id = otonomic_insert_post($my_album);
//        $this->clone_album_posts($my_album);
//        wp_set_post_categories($post_id, 9);
		// TODO: Change category from number
		// TODO: Add terms+category
		// tax_input in the input to wp_insert_post() should work instead
		// $term_taxonomy_ids = wp_set_object_terms( $post_id , 28 , 'portfolio-category', false );

		// adding the image to the post
		$image_ids = array();
		$items_thumbnail = array();

		$otonomic_url = otonomic_url::get_instance();

		foreach((array)$album_photos as $key=>$picture)
		{
			try {
				$image_id = $this->add_image($picture, $post_id, $download_image);
				$image_ids[] = $image_id;
				$items_thumbnail[] = $otonomic_url->ot_get_source_url($picture->picture);

			} catch(Exception $e) {
			}
		}
		// add meta tags for this gallery
		$post_meta = [
			'_dt_post_settings'         => array (
				'layout' => 'content-full-width',
			),
			'_tpl_default_settings' => array (
				'layout' => 'content-full-width',
			),
			'_gallery_settings'=>array (
				'layout' => 'content-full-width',
				'items' =>$items_thumbnail,
				'items_thumbnail' => $items_thumbnail,
				'items_name' => $image_ids,
			)
		];

		otonomic_insert_postmeta($post_id, $post_meta);

		//$this->clone_album_posts($my_album,$image_ids);

		return $post_id;
	}

    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work

	    //$this->add_homepage();

	    $this->set_theme_options();
        $this->set_widgets();
		$this->set_pages_settings();
        $this->update_options();

        $this->set_menu(array('header_menu'));
    }



    protected function update_options()
    {
        try {
	        parent::update_options();
            // TODO: Comment global here?
            global $wpdb;

            update_option( 'page_on_front', $this->get_homepage_id() );
            update_option( 'show_on_front', 'page' );

            update_option( 'template', $this->theme );
            update_option( 'stylesheet', 'dreamspachild' );
            update_option( 'current_theme', 'dreamspachild' );
            /*update_option('Dreamspa_feat_cat', '11');
            update_option('Dreamspa_use_pages', 'false');*/

            update_option( 'last_update', time() );
            update_option( 'facebook_id', $this->page->facebook->id );

        } catch(Exception $e) {
        }
    }

	protected function set_theme_options()
	{
		try {
			include_once('config/theme_options.php');
			$data = get_theme_options();
			if ( get_option( 'mytheme' ) !== false )
			{
				update_option( 'mytheme' , $data );
			}
			else
			{
				add_option( 'mytheme', $data, null, 'yes' );
			}

		} catch(Exception $e) {
		}
		//switch_theme($this->theme,$this->theme);
	}
}
