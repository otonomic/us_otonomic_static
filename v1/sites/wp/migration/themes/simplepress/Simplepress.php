<?php
include_once ABSPATH . "migration/core/ElegantThemesAbstract.php";

class Simplepress extends ElegantThemesAbstract implements ThemeInterface, ThemePluginSupportInterface {
    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        //$this->theme = strtolower(__CLASS__);
        $this->theme = 'simplepress';
    }

    public function setup_SEO()
    {
        $OtonomicSEO  = OtonomicSEO::get_instance();

        $home_page_id = $this->get_homepage_id();
        $OtonomicSEO->add_seo_tags( $home_page_id );
	    $OtonomicSEO->add_home_seo_tags( );

        $shop_page_id = $this->get_page_id_by_name('Shop');
        $OtonomicSEO->add_seo_tags( $shop_page_id );

        $cart_page_id = $this->get_page_id_by_name('Cart');
        $OtonomicSEO->add_seo_tags( $cart_page_id );

        $checkout_page_id = $this->get_page_id_by_name('Checkout');
        $OtonomicSEO->add_seo_tags( $checkout_page_id );

        $my_account_page_id = $this->get_page_id_by_name('My Account');
        $OtonomicSEO->add_seo_tags( $my_account_page_id );

    }

    public function save_content(){
        kses_remove_filters();
        $this->save_posts();
        // NOTE: save_testimonials() is specific to the template - should be part of the template change code
        $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        $this->save_albums();
        $this->save_videos();
        kses_init_filters();
    }

    function update_homepage() {
        // TODO: Write code that adds a page that would be the homepage
    }

    function add_module_slider()
    {
        $this->set_main_site_slider();
    }

    function add_module_about()
    {
        $this->add_section_about();
    }

    function add_module_services()
    {
        $this->add_section_services();
    }

    function add_module_store() {
        $this->add_section_store();
    }

    function add_module_blog()
    {
        $this->add_section_blog();
        // $this->add_page_blog();
    }

    function add_module_portfolio()
    {
        $this->add_section_portfolio();
        // $this->add_page_portfolio();
    }

    function add_module_testimonials()
    {
        $this->add_section_testimonials();
        // $this->add_page_testimonials();
    }

    function add_module_social()
    {
        $this->add_section_social();
    }

    function add_module_contact()
    {
        $this->add_section_contact();
    }






    function add_module_plugin_booking()
    {
        global $wpdb;
        $post_content = '[bpscheduler_booking_form]';

        $my_post = array(
            'post_title'    => 'Book an Appointment',
            'post_name'     => 'booking',
            'post_content'  => $post_content,
            'post_status'   => 'publish',
            'post_type' 	  => 'section',
            'menu_order'    => 65,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );

        $post_id = wp_insert_post( $my_post );

        $wpdb->query( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_id ."',25)");

        add_post_meta( $post_id, 'background_image', '/wp-content/plugins/otonomic-templates/img/booking.jpg');
        add_post_meta( $post_id, 'background_repeat', 'fullcover' );


        // add to menu
        $args = array(
            'post_type' => 'nav_menu_item',
            'post_title' => 'Booking',
            'name' => 'booking',
        );

        $post_type_query = new \WP_Query( $args );

        update_post_meta($post_type_query->post->ID, '_menu_item_url', '#booking');
        update_post_meta($post_type_query->post->ID, '_menu_item_type', 'custom');
        update_post_meta($post_type_query->post->ID, '_menu_item_menu_item_parent', '0');
        update_post_meta($post_type_query->post->ID, '_menu_item_object_id', $post_type_query->post->ID);
        update_post_meta($post_type_query->post->ID, '_menu_item_object', 'custom');

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_type_query->post->ID ."',43)");
    }



    function add_section_store() {
        global $wpdb;

        $content = '[recent_products per_page="12" columns="3"][woocommerce_cart]';

        $data_post = array(
            'post_title'   => 'Store',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 70,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");

        $OtonomicStore = OtonomicStore::get_instance();
        $OtonomicStore->add_sample_product();
        $OtonomicStore->show_store_pages(false);

        $OtonomicStore->add_store_pages_to_menu( $parent = '2343');

        return $page_id;
    }



    public function add_section_contact() {
        global $wpdb;

        $data_post = array(
            'post_title'   => 'Contact',
            'post_content' => $this->get_content_contact(),
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 75,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        
        add_post_meta($page_id, '_wp_page_template', 'page-full.php');
        
        return $page_id;
    }


    public function add_section_testimonials() {
        global $wpdb;
        
        $data_post = array(
            'post_title'   => 'Testimonials',
            'post_content' => $this->get_content_testimonials(),
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 60,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }


    public function add_section_social() {
        $data_post = array(
            'post_title'   => 'Get Social',
            'post_content' => $this->get_content_social(),
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 50,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        
        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }



    function add_section_slider() {
    }



    public function add_section_about() {
        $content = $this->get_content_about();

        // Save content
        $data_post = array(
            'post_title'   => 'About',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 10
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        
        global  $wpdb;
        $wpdb->query( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }



    public function add_section_services() {
        $content = $this->get_content_services();

        // Save content
        $data_post = array(
            'post_title'   => 'Services',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'page',
            'menu_order'   => 15,
        );
        $data_postmeta = array(
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);
        
        return $page_id;
    }


    public function add_module_plugin_NextGen()
    {
        // Search for 'Photo Albums' section
        global $wpdb;
        $content = <<<E
            [ngg_images gallery_ids="1" display_type="photocrati-nextgen_pro_masonry" images="20" width="230" images_per_page="20" disable_pagination="yes"]
E;

        $page_obj = get_page_by_title( 'Photos', OBJECT, 'section' );
        if($page_obj)
        {
            $post = array(
                'ID'          => $page_obj->ID,
                'post_content' => $content,
            );
            wp_update_post( $post );

        } else {
            return $page_id = $this->insert_section_portfolio($content);
        }
    }


    function insert_section_portfolio($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
                'post_title'   => 'Photos',
                'post_content' => $content,
                'post_status'  => 'publish',
                'post_type'    => 'page',
                'menu_order'   => 20,
                'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
            ) + $data_post;

        $data_postmeta = array(
            ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);
        add_post_meta($page_id, '_wp_page_template', 'page-gallery.php');
        $et_ptemplate_settings = array(
                                'et_fullwidthpage' => 1,
                                'et_ptemplate_gallerycats' => array(3),
                                'et_ptemplate_gallery_perpage' => 12
                                );
        add_post_meta($page_id, 'et_ptemplate_settings', $et_ptemplate_settings);

        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }


    function insert_section_blog($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
                'post_title'   => 'Updates',
                'post_content' => $content,
                'post_status'  => 'publish',
                'post_type'    => 'page',
                'menu_order'   => 40,
            ) + $data_post;

        $data_postmeta = array(
                '_wp_page_template' => 'page-blog.php'
            ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);
//        add_post_meta($page_id, '_wp_page_template', 'page-blog.php');
        $et_ptemplate_settings = array(
                                'et_fullwidthpage' => 0,
                                'et_ptemplate_blogstyle' => 0,
                                'et_ptemplate_showthumb' => 0,
                                'et_ptemplate_gallerycats' => array(2),
                                'et_ptemplate_blog_perpage' => 6
                                );
        add_post_meta($page_id, 'et_ptemplate_settings', $et_ptemplate_settings);
        
        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }







    function add_page_testimonials() {
    }

    function add_page_portfolio() {
    }

    function add_page_blog() {
    }







    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work
        // $this->set_widgets();
        // $this->set_themify_options();
        $this->set_text_widgets_sidebar();
        $this->set_text_widgets_footer();
        $this->update_options();
        $this->set_quote_area();
        $this->set_menu();
    }



    
    public function set_quote_area(){
        update_option('simplepress_quote_one',$this->page->facebook->about);
        update_option('simplepress_quote_two', '');
    }

    protected function set_text_widgets_footer(){
        parent::set_text_widgets_footer();
    }

    protected function set_text_widgets_sidebar(){
        parent::set_text_widgets_sidebar();
    }

    protected function update_options()
    {
        try
        {
            parent::update_options();
            update_option('simplepress_feat_cat', '11');
            update_option('stylesheet', 'SimplePress-child');
            update_option('current_theme', 'SimplePress Child');
            update_option('simplepress_use_pages', 'false');
            update_option('simplepress_quote', 'false');
        }
        catch(Exception $e)
        {
            /* Silently ignore the error */
        }
    }





    public function set_elegantthemes_options()
    {
        try
        {
            include_once('config/elegantthemes_options.php');
            $data = get_elegantthemes_options();
            if ( get_option( 'elegantthemes_data' ) !== false )
            {
                update_option( 'elegantthemes_data' , $data );
            }
            else
            {
                add_option( 'elegantthemes_data', $data, null, 'yes' );
            }
        }
        catch(Exception $e)
        {
            /* Silently ignore the error */
        }
        switch_theme($this->theme,$this->theme);
    }
}
