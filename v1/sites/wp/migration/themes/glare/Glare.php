<?php
include_once ABSPATH . "migration/core/CssIgnitorAbstract.php";

class Glare extends CssIgnitorAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = 'glare';
    }

    protected function add_homepage() {
        if(!is_array($this->page->albums) || count($this->page->albums) == 0)
            return;

        $this->wpdb_disable_autocommit();
        
        foreach($this->page->albums as $key=>$album)
        {
	        try
	        {
                    // if its cover
                    if(strtolower($album->name) == 'cover photos')
	            {
                        $post_type = 'page';
                        $post_title = 'Home';
                    }
                    else{
                        $post_title = $album->name;
                        $post_type = 'galleries';
                    }
                        
//                    $album_post_id = $this->add_album($album);
                    
                    $album_photos = $album->photos->data;
                    if(strtolower($album->name) != 'cover photos')
	            {
                        if(empty($album_photos)) { continue; }
                    }

                    $my_album = array(
                        'post_title'     => $post_title,
                        'post_name'   => sanitize_title( trim_closest_word($post_title, 50, false) ),
                        'post_content'   => $album->description,
                        'post_status'    => 'publish',
                        'comment_status' => 'closed',
                        'ping_status' 	 => 'closed',
                        'post_type' 	 => $post_type,
                        'guid'           => $album->id
                    );

                    $post_date = $this->parse_date($album->created_time);
                    $my_album['post_date'] = $post_date;
                    $my_album['post_date_gmt'] = $post_date;
                    $my_album['post_modified'] = $post_date;
                    $my_album['post_modified_gmt'] = $post_date;
                    
                    $post_id = otonomic_insert_post($my_album);
                    
                    if(strtolower($album->name) == 'cover photos')
	            {
                        add_post_meta($post_id, '_wp_page_template', 'template-home-slider.php');

                        update_option('show_on_front','page');
                        update_option('page_on_front', $post_id);
                    }
                    else{
                        add_post_meta($post_id, 'ci_cpt_gallery_homepage', 'enabled');
                        add_post_meta($post_id, 'ci_cpt_gallery_internal_slider', 'disabled');
                    }
                    
                    $image_ids = array();

                    foreach((array)$album_photos as $key=>$picture)
                    {
                            try
                            {
                                $image_id = $this->add_image($picture, $post_id, false);
                                $image_ids[] = $image_id;
                        }
                            catch(Exception $e)
                            {
                                    /* Silently ignore the error */
                            }
                    }

                    // add meta tags for this post
                    $album_cover_photo_localid = $image_ids[0];
                    $album_cover_photo_url = $album_photos[0]->source;
                    $gallery_shortcode = '[gallery ids="'. implode(",", $image_ids) .'" link="file"]';

                    $post_meta = [
                        '_thumbnail_id'         => $album_cover_photo_localid,
                        '_post_image_attach_id' => $album_cover_photo_localid,
                        'post_image'            => $album_cover_photo_url,
                        'gallery_shortcode'     => $gallery_shortcode
                    ];

                    otonomic_insert_postmeta($post_id, $post_meta);
                    
                    if(strtolower($album->name) == 'cover photos')
	            {
                        $theme_options = get_option('ci_glare_theme_options');
                        $theme_options['default_header_bg'] = wp_get_attachment_url( $image_ids[0] );
                        update_option('ci_glare_theme_options', $theme_options);
                    }
            }
	        catch(Exception $e)
	        {
		        /* Silently ignore the error */
	        }
        }

        $this->wpdb_enable_autocommit();
    }

    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work
        // $this->set_widgets();
        // $this->set_themify_options();
        $this->add_homepage();

        $this->set_text_widgets_footer();

        $this->set_widget_locations();

        $this->set_pages_settings();

        $this->update_options();

        $this->set_menu();
    }


    function set_pages_settings() {
        $data = [
            'about' => $this->get_full_width_settings_array(),

            'contact' => $this->get_full_width_settings_array(),

            'social' => $this->get_full_width_settings_array(),

            'testimonials' => $this->get_full_width_settings_array(),

            'portfolio' => [
                '_wp_page_template' => 'page-gallery.php',
            ],

            'blog' => [
                '_wp_page_template' => 'page-blog.php',
            ]
        ];
        foreach($data as $page_name => $settings) {
            $this->set_page_settings($page_name, $settings);
        }
    }

    protected function set_widget_locations(){
        $widget_location = array(
            'wp_inactive_widgets' => array(),
            'blog-sidebar' => array('text-2','text-3'),
            'pages-sidebar' => array('text-2','text-3'),
            'footer-widgets' => array('text-0','text-4','text-3'),
//            'contact-widgets' => array('text-0','text-4','text-2'),
            'array_version' => 3
        );
        update_option('sidebars_widgets',$widget_location);
    }



    protected function update_options()
    {
	    parent::update_options();
        try {
//            update_option( 'page_on_front', $this->get_homepage_id() );
//            update_option( 'show_on_front', 'page' );

            update_option( 'template', $this->theme );
            update_option( 'stylesheet', 'glarechild' );
            update_option( 'current_theme', 'Glarechild' );

            update_option( 'last_update', time() );
            update_option( 'facebook_id', $this->page->facebook->id );


            
            //$theme_options = get_option('ci_glare_theme_options');
//            #page-header {
//                    background-size: 2200px 400px !important;
//                }
            //update_option('ci_glare_theme_options', $theme_options);

        } catch(Exception $e) {
        }
    }
}
