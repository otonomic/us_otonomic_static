<?php
if(empty($blog_id)) {
    $blog_id = $_GET['blog_id'];
}
if(!$blog_id) {
    die('Missing blog id');
}
// $link = $domain .'/'. $page->username;

$site_url = get_site_url();

set_time_limit(300);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E

INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
 ('1', '1', 'category', '', '0', '0'),
 ('2', '2', 'category', '', '0', '9'),
 ('3', '3', 'category', '', '0', '9'),
 ('4', '4', 'category', '', '0', '0'),
 ('7', '7', 'category', '', '0', '0'),
 ('8', '8', 'category', '', '0', '3'),
 ('18', '18', 'slider-category', '', '0', '3'),
 ('25', '24', 'section-category', '', '0', '8'),
 ('32', '31', 'slider-category', '', '0', '4'),
 ('33', '32', 'testimonial-category', '', '0', '4'),
 ('34', '33', 'testimonial-category', '', '0', '4'),
 ('43', '41', 'nav_menu', '', '0', '9'),
 ('47', '42', 'nav_menu', '', '0', '0'),
 ('48', '43', 'product_type', '', '0', '0'),
 ('49', '44', 'product_type', '', '0', '0'),
 ('50', '45', 'product_type', '', '0', '0'),
 ('51', '46', 'product_type', '', '0', '0'),
 ('52', '47', 'shop_order_status', '', '0', '0'),
 ('53', '48', 'shop_order_status', '', '0', '0'),
 ('54', '49', 'shop_order_status', '', '0', '0'),
 ('55', '50', 'shop_order_status', '', '0', '0'),
 ('56', '51', 'shop_order_status', '', '0', '0'),
 ('57', '52', 'shop_order_status', '', '0', '0'),
 ('58', '53', 'shop_order_status', '', '0', '0'),
 ('59', '1', 'section-category', '', '0', '0');
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
 ('1', 'Uncategorized', 'uncategorized', '0'),
 ('2', 'Blog', 'blog', '0'),
 ('3', 'Images', 'images', '0'),
 ('4', 'News', 'news', '0'),
 ('7', 'Updates', 'updates', '0'),
 ('8', 'Video', 'video', '0'),
 ('18', 'Corporate', 'corporate', '0'),
 ('24', 'Home', 'home', '0'),
 ('31', 'Slider', 'slider', '0'),
 ('32', 'Team', 'team', '0'),
 ('33', 'Testimonials', 'testimonials', '0'),
 ('41', 'Home Menu', 'home-menu', '0'),
 ('42', 'Otonomic Main Menu', 'otonomic-main-menu', '0'),
 ('43', 'simple', 'simple', '0'),
 ('44', 'grouped', 'grouped', '0'),
 ('45', 'variable', 'variable', '0'),
 ('46', 'external', 'external', '0'),
 ('47', 'pending', 'pending', '0'),
 ('48', 'failed', 'failed', '0'),
 ('49', 'on-hold', 'on-hold', '0'),
 ('50', 'processing', 'processing', '0'),
 ('51', 'completed', 'completed', '0'),
 ('52', 'refunded', 'refunded', '0'),
 ('53', 'cancelled', 'cancelled', '0');
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES

 ('2204', '1', '2014-02-14 20:30:46', '2014-02-14 20:30:46', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2014-05-12 13:37:24', '2014-05-12 13:37:24', '', '0', 'http://themify.me/demo/themes/wp-content/uploads/image-placeholder.jpg', '0', 'page', '', '0'),

 ('2333', '1', '2014-05-08 12:32:20', '2014-05-08 12:32:20', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit \"Send\"]</p>\n[your-subject]\n[your-name] <[your-email]>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\nroman.raslin@gmail.com\n\n\n0\n\n[your-subject]\n[your-name] <[your-email]>\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\n[your-email]\n\n\n0\nYour message was sent successfully. Thanks.\nFailed to send your message. Please try later or contact the administrator by another method.\nValidation errors occurred. Please confirm the fields and submit it again.\nFailed to send your message. Please try later or contact the administrator by another method.\nPlease accept the terms to proceed.\nPlease fill the required field.', 'Contact form 1', '', 'publish', 'open', 'open', '', 'contact-form-1', '', '', '2014-05-08 12:32:20', '2014-05-08 12:32:20', '', '0', 'http://wp.test//?post_type=wpcf7_contact_form&p=2333', '0', 'wpcf7_contact_form', '', '0'),
-- ('2335', '1', '2014-06-20 15:07:00', '2014-06-20 15:07:00', '[bpscheduler_booking_form]', 'Booking', '', 'publish', 'closed', 'closed', '', 'booking', '', '', '2014-06-20 18:04:12', '2014-06-20 18:04:12', '', '0', 'http://wp.test/site491474764215798/blog/section/booking/', '0', 'section', '', '0'),

 ('2283', '1', '2014-05-08 12:06:20', '2014-05-08 12:06:20', '', 'Home', '', 'publish', 'open', 'open', '', 'home', '', '', '2014-06-20 20:24:36', '2014-06-20 20:24:36', '', '0', 'http://wp.test//blog/2014/05/08/2283/', '1', 'nav_menu_item', '', '0'),
 ('2341', '1', '2014-06-20 20:24:36', '2014-06-20 20:24:36', '', 'About', '', 'publish', 'open', 'open', '', 'about', '', '', '2014-06-20 20:24:36', '2014-06-20 20:24:36', '', '0', 'http://wp.test/site491474764215798/?p=2341', '2', 'nav_menu_item', '', '0'),
 ('2225', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Gallery', '', 'publish', 'open', 'open', '', 'gallery', '', '', '2014-06-20 20:24:36', '2014-06-20 20:24:36', '', '0', 'http://wp.test//blog/2014/05/08/portfolio/', '3', 'nav_menu_item', '', '0'),
 ('2239', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Updates', '', 'publish', 'open', 'open', '', 'updates', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test//blog/2014/05/08/blog/', '4', 'nav_menu_item', '', '0'),
 ('2226', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Social', '', 'publish', 'open', 'open', '', 'social', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test//blog/2014/05/08/social/', '5', 'nav_menu_item', '', '0'),
 ('2228', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Testimonials', '', 'publish', 'open', 'open', '', 'testimonials', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test//blog/2014/05/08/team/', '6', 'nav_menu_item', '', '0'),
 ('1000', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Booking', '', 'publish', 'open', 'open', '', 'booking', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test/booking/', '7', 'nav_menu_item', '', '0'),
 ('2343', '1', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', 'Store', '', 'publish', 'open', 'open', '', 'store', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test/site491474764215798/?p=2343', '8', 'nav_menu_item', '', '0'),
 ('2227', '1', '2014-05-08 12:06:14', '2014-05-08 12:06:14', '', 'Contact', '', 'publish', 'open', 'open', '', 'contact', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test//blog/2014/05/08/contact/', '9', 'nav_menu_item', '', '0');
 ('2342', '1', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', 'YouTube', '', 'publish', 'open', 'open', '', 'youtube', '', '', '2014-06-20 20:24:37', '2014-06-20 20:24:37', '', '0', 'http://wp.test/site491474764215798/?p=2342', '10', 'nav_menu_item', '', '0');

E;



if($blog_id == 1)
	$tablename = 'wp_term_relationships';
else
	$tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
 ('0', '2', '0'),
 ('0', '3', '0'),
 ('2206', '25', '0'),
 ('2208', '25', '0'),
 ('2209', '25', '0'),
 ('2210', '25', '0'),
 ('2335', '25', '0'),
 ('2340', '25', '0'),
 ('2344', '25', '0'),
 ('2345', '25', '0'),
 ('2224', '43', '0'),
 ('2225', '43', '0'),
 ('2226', '43', '0'),
 ('2227', '43', '0'),
 ('2228', '43', '0'),
 ('2229', '43', '0'),
 ('2230', '43', '0'),
 ('2231', '43', '0'),
 ('2239', '43', '0'),
 ('2283', '43', '0'),
 ('2293', '43', '0'),
 ('2294', '43', '0'),
 ('2295', '43', '0'),
 ('2296', '43', '0'),
 ('2297', '43', '0'),
 ('2298', '43', '0'),
 ('2299', '43', '0'),
 ('2300', '43', '0'),
 ('2301', '43', '0'),
 ('2302', '43', '0'),
 ('2303', '43', '0'),
 ('2304', '43', '0'),
 ('2305', '43', '0'),
 ('2341', '43', '0'),
 ('2342', '43', '0'),
 ('2343', '43', '0'),
 ('2348', '2', '0'),
 ('2348', '3', '0'),
 ('2350', '2', '0'),
 ('2350', '3', '0'),
 ('2352', '2', '0'),
 ('2352', '3', '0'),
 ('2355', '2', '0'),
 ('2355', '3', '0'),
 ('2357', '2', '0'),
 ('2357', '3', '0'),
 ('2359', '2', '0'),
 ('2359', '3', '0'),
 ('2361', '2', '0'),
 ('2361', '3', '0'),
 ('2363', '2', '0'),
 ('2363', '3', '0'),
 ('2366', '2', '0'),
 ('2366', '3', '0'),
 ('2955', '59', '0'),
 ('2956', '59', '0'),
 ('2957', '59', '0'),
 ('2958', '59', '0'),
 ('2959', '59', '0'),
 ('2960', '59', '0'),
 ('2961', '59', '0'),
 ('2962', '59', '0'),
 ('2963', '59', '0'),
 ('2964', '59', '0'),
 ('2965', '59', '0'),
 ('2346', '59', '0'),
 ('2347', '59', '0'),
 ('2211', '0', '0'),
 ('2264', '0', '0'),
 ('2268', '0', '0')
 ;
E;

if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
 ('1298', '2225', '_menu_item_type', 'custom'),
 ('1299', '2225', '_menu_item_menu_item_parent', '0'),
 ('1300', '2225', '_menu_item_object_id', '2225'),
 ('1301', '2225', '_menu_item_object', 'custom'),
 ('1302', '2225', '_menu_item_target', ''),
 ('1303', '2225', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('1304', '2225', '_menu_item_xfn', ''),
 ('1305', '2225', '_menu_item_url', '{$site_url}#gallery'),
 
 ('1306', '2226', '_menu_item_type', 'custom'),
 ('1307', '2226', '_menu_item_menu_item_parent', '0'),
 ('1308', '2226', '_menu_item_object_id', '2226'),
 ('1309', '2226', '_menu_item_object', 'custom'),
 ('1310', '2226', '_menu_item_target', ''),
 ('1311', '2226', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('1312', '2226', '_menu_item_xfn', ''),
 ('1313', '2226', '_menu_item_url', '{$site_url}#get-social'),
 
 ('1314', '2227', '_menu_item_type', 'custom'),
 ('1315', '2227', '_menu_item_menu_item_parent', '0'),
 ('1316', '2227', '_menu_item_object_id', '2227'),
 ('1317', '2227', '_menu_item_object', 'custom'),
 ('1318', '2227', '_menu_item_target', ''),
 ('1319', '2227', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('1320', '2227', '_menu_item_xfn', ''),
 ('1321', '2227', '_menu_item_url', '{$site_url}#contact'),
 
 ('1322', '2228', '_menu_item_type', 'custom'),
 ('1323', '2228', '_menu_item_menu_item_parent', '0'),
 ('1324', '2228', '_menu_item_object_id', '2228'),
 ('1325', '2228', '_menu_item_object', 'custom'),
 ('1326', '2228', '_menu_item_target', ''),
 ('1327', '2228', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('1328', '2228', '_menu_item_xfn', ''),
 ('1329', '2228', '_menu_item_url', '{$site_url}#testimonials'),
 
 ('1410', '2239', '_menu_item_type', 'custom'),
 ('1411', '2239', '_menu_item_menu_item_parent', '0'),
 ('1412', '2239', '_menu_item_object_id', '2239'),
 ('1413', '2239', '_menu_item_object', 'custom'),
 ('1414', '2239', '_menu_item_target', ''),
 ('1415', '2239', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('1416', '2239', '_menu_item_xfn', ''),
 ('1417', '2239', '_menu_item_url', '{$site_url}#updates'),
 
 ('2481', '2204', 'content_width', 'default_width'),
 ('2482', '2204', 'background_wrap', 'yes'),
 ('2483', '2204', 'background_speed', '500'),
 ('2484', '2204', 'background_type', 'slider'),
 ('2485', '2204', 'background_auto', 'yes'),
 ('2487', '2204', 'background_autotimeout', '5'),
 ('2489', '2204', 'layout', 'list-post'),
 ('2490', '2204', 'background_repeat', 'default'),
 ('2491', '2204', 'background_gallery', null),
 ('2492', '2204', 'page_layout', 'sidebar-none'),
 ('2493', '2204', 'hide_page_title', 'yes'),
 ('2494', '2204', 'custom_menu', 'home-menu'),
 ('2495', '2204', 'order', 'desc'),
 ('2496', '2204', 'orderby', 'content'),
 ('2497', '2204', 'display_content', 'content'),
 ('2498', '2204', 'feature_size_page', 'blank'),
 ('2506', '2204', 'section_query_category', 'home'),
 ('2507', '2204', 'section_order', 'asc'),
 ('2508', '2204', 'section_orderby', 'menu_order'),
 ('2509', '2204', 'section_feature_size_page', 'blank'),
 ('2512', '2204', 'section_parallax_effect', 'effect2'),
 ('2513', '2204', 'portfolio_order', 'desc'),
 ('2514', '2204', 'portfolio_orderby', 'content'),
 ('2515', '2204', 'portfolio_layout', 'list-post'),
 ('2516', '2204', 'portfolio_display_content', 'content'),
 ('2517', '2204', 'portfolio_feature_size_page', 'blank'),
 ('4512', '2206', 'background_image', 'http://themify.me/demo/themes/parallax/files/2013/05/team.jpg'),
 ('4513', '2206', '_background_image_attach_id', '38'),
 ('4514', '2206', 'background_repeat', 'fullcover'),
 ('4517', '2206', 'font_color', '000000'),
 ('4518', '2206', 'link_color', '000000'),
 ('4519', '2206', 'subtitle', 'Hear what people think about us!'),
 ('4524', '2206', 'background_color', '609fc4'),
 ('4525', '2206', '_dp_original', '13'),
 ('4555', '2208', 'background_image', 'http://themify.me/demo/themes/parallax/files/2013/05/contact.jpg'),
 ('4556', '2208', '_background_image_attach_id', '37'),
 ('4557', '2208', 'background_repeat', 'fullcover'),
 ('4560', '2208', 'font_color', '000000'),
 ('4561', '2208', 'link_color', '000000'),
 ('4562', '2208', 'subtitle', 'Drop by and say hi...'),
 ('4567', '2208', 'background_color', 'DCE8E8'),
 ('4574', '2209', 'background_image', 'http://themify.me/demo/themes/parallax/files/2013/05/nightsky.jpg'),
 ('4575', '2209', 'background_repeat', 'fullcover'),
 ('4578', '2209', 'font_color', 'ffffff'),
 ('4579', '2209', 'link_color', 'ffffdb'),
 ('4580', '2209', 'background_color', ''),
 ('4581', '2209', 'subtitle', 'Connect with us on social media'),
 ('4588', '2209', '_dp_original', '23'),
 ('4593', '2209', '_background_image_attach_id', '339'),
 ('4596', '2210', 'background_image', ''),
 ('4597', '2210', 'background_repeat', 'repeat'),
 ('4600', '2210', 'font_color', 'ffffff'),
 ('4601', '2210', 'link_color', 'ffffff'),
 ('4602', '2210', 'subtitle', 'Some of our latest photos'),
 ('4606', '2210', 'background_color', '000000'),
 ('4607', '2210', '_dp_original', '29'),

 ('5238', '2283', '_menu_item_type', 'custom'),
 ('5239', '2283', '_menu_item_menu_item_parent', '0'),
 ('5240', '2283', '_menu_item_object_id', '2283'),
 ('5241', '2283', '_menu_item_object', 'custom'),
 ('5242', '2283', '_menu_item_target', ''),
 ('5243', '2283', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('5244', '2283', '_menu_item_xfn', ''),
 ('5245', '2283', '_menu_item_url', '{$site_url}#'),
 
 ('5640', '2333', '_form', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit \"Send\"]</p>'),
 
 ('5641', '2333', '_mail', 'a:7:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:26:"[your-name] <[your-email]>";s:4:"body";s:86:"From: [your-name] <[your-email]>
Subject: [your-subject]

Message Body:
[your-message]";s:9:"recipient";s:0:"";s:18:"additional_headers";s:0:"";s:11:"attachments";s:0:"";s:8:"use_html";b:0;}'),

 ('5642', '2333', '_mail_2', 'a:8:{s:6:\"active\",b:0,s:7:\"subject\",s:14:\"[your-subject]\",s:6:\"sender\",s:26:\"[your-name] <[your-email]>\",s:4:\"body\",s:105:\"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\",s:9:\"recipient\",s:12:\"[your-email]\",s:18:\"additional_headers\",s:0:\"\",s:11:\"attachments\",s:0:\"\",s:8:\"use_html\",i:0,}'),

 ('5643', '2333', '_messages', 'a:21:{s:12:"mail_sent_ok";s:43:"Your message was sent successfully. Thanks.";s:12:"mail_sent_ng";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:16:"validation_error";s:74:"Validation errors occurred. Please confirm the fields and submit it again.";s:4:"spam";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:12:"accept_terms";s:35:"Please accept the terms to proceed.";s:16:"invalid_required";s:31:"Please fill the required field.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:14:"invalid_number";s:28:"Number format seems invalid.";s:16:"number_too_small";s:25:"This number is too small.";s:16:"number_too_large";s:25:"This number is too large.";s:13:"invalid_email";s:28:"Email address seems invalid.";s:11:"invalid_url";s:18:"URL seems invalid.";s:11:"invalid_tel";s:31:"Telephone number seems invalid.";s:23:"quiz_answer_not_correct";s:27:"Your answer is not correct.";s:12:"invalid_date";s:26:"Date format seems invalid.";s:14:"date_too_early";s:23:"This date is too early.";s:13:"date_too_late";s:22:"This date is too late.";s:13:"upload_failed";s:22:"Failed to upload file.";s:24:"upload_file_type_invalid";s:30:"This file type is not allowed.";s:21:"upload_file_too_large";s:23:"This file is too large.";s:23:"upload_failed_php_error";s:38:"Failed to upload file. Error occurred.";}'),
 
 ('5644', '2333', '_additional_settings', ''),
 ('5645', '2333', '_locale', 'en_US'),
 
 ('7248', '2335', 'background_image', 'http://webneel.com/wallpaper/sites/default/files/images/04-2013/indian-beach-wallpaper.jpg'),
 ('7249', '2335', 'background_repeat', 'fullcover'),
 ('7304', '2335', 'subtitle', 'Make a reservation'),
 
 ('7337', '2341', '_menu_item_type', 'custom'),
 ('7338', '2341', '_menu_item_menu_item_parent', '0'),
 ('7339', '2341', '_menu_item_object_id', '2341'),
 ('7340', '2341', '_menu_item_object', 'custom'),
 ('7341', '2341', '_menu_item_target', ''),
 ('7342', '2341', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('7343', '2341', '_menu_item_xfn', ''),
 ('7344', '2341', '_menu_item_url', '{$site_url}#about'),
 
 ('7346', '2342', '_menu_item_type', 'custom'),
 ('7347', '2342', '_menu_item_menu_item_parent', '0'),
 ('7348', '2342', '_menu_item_object_id', '2342'),
 ('7349', '2342', '_menu_item_object', 'custom'),
 ('7350', '2342', '_menu_item_target', ''),
 ('7351', '2342', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('7352', '2342', '_menu_item_xfn', ''),
 ('7353', '2342', '_menu_item_url', '{$site_url}#youtube'),
 
 ('7355', '2343', '_menu_item_type', 'custom'),
 ('7356', '2343', '_menu_item_menu_item_parent', '0'),
 ('7357', '2343', '_menu_item_object_id', '2343'),
 ('7358', '2343', '_menu_item_object', 'custom'),
 ('7359', '2343', '_menu_item_target', ''),
 ('7360', '2343', '_menu_item_classes', 'a:1:{i:0,s:0:"",}'),
 ('7361', '2343', '_menu_item_xfn', ''),
 ('7362', '2343', '_menu_item_url', '{$site_url}#store');

E;




if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
('tracked_site_created', 'no', 'yes'),
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin


('woocommerce_permalinks', 'false', 'yes');
E;

$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
// Make sure not to perform truncate on wp_options table!

$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
$db->exec($wp_posts);
$db->exec($wp_postmeta);

