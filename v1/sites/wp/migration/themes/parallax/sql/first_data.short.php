<?php
$link = $domain .'/'. $page->username;
set_time_limit(300);

if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E
INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 7),
(3, 3, 'portfolio-category', '', 0, 0),
(4, 4, 'testimonial-category', '', 0, 0),
(5, 5, 'testimonial-category', '', 0, 0),
(6, 6, 'highlight-category', '', 0, 0),
(7, 1, 'portfolio-category', '', 0, 0),
(8, 7, 'category', '', 0, 0),
(9, 8, 'post_tag', '', 0, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, 'Main Portfolio', 'main-portfolio', 0),
(4, 'Testimonials', 'testimonials', 0),
(5, 'Team','team', 0),
(6, 'Services','services', 0),
(7, 'Facebook Post','facebook-post', 0),
(8, 'Facebook','facebook', 0);
E;


if($blog_id == 1)
    $tablename = 'wp_term_relationships';
else
    $tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0);
E;

if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(18, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '18', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '19', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=19', 7, 'nav_menu_item', '', 0),
(20, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '20', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=20', 6, 'nav_menu_item', '', 0),
(21, 1, '2014-05-23 10:36:32', '2014-05-23 10:36:32', ' ', '', '', 'publish', 'open', 'open', '', '21', '', '', '2014-05-23 10:36:32', '2014-05-23 10:36:32', '', 0, 'http://{$link}/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '22', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=22', 4, 'nav_menu_item', '', 0),
(23, 1, '2014-05-23 10:36:31', '2014-05-23 10:36:31', ' ', '', '', 'publish', 'open', 'open', '', '23', '', '', '2014-05-23 10:36:31', '2014-05-23 10:36:31', '', 0, 'http://{$link}/?p=23', 3, 'nav_menu_item', '', 0),
(24, 1, '2014-05-23 10:36:30', '2014-05-23 10:36:30', ' ', '', '', 'publish', 'open', 'open', '', '24', '', '', '2014-05-23 10:36:30', '2014-05-23 10:36:30', '', 0, 'http://{$link}/?p=24', 2, 'nav_menu_item', '', 0),
(4, 1, '2014-05-23 10:33:31', '2014-05-23 10:33:31', '', 'Home', '', 'publish', 'close', 'close', '', 'home', '', '', '2014-05-23 11:14:24', '2014-05-23 11:14:24', '', 0, 'http://{$link}/?page_id=4', 0, 'page', '', 0),
(6, 1, '2014-05-23 10:33:53', '2014-05-23 10:33:53', '', 'About', '', 'publish', 'close', 'close', '', 'about', '', '', '2014-05-23 10:33:53', '2014-05-23 10:33:53', '', 0, 'http://{$link}/?page_id=6', 0, 'page', '', 0),
(8, 1, '2014-05-23 10:34:05', '2014-05-23 10:34:05', '', 'Services', '', 'publish', 'close', 'close', '', 'services', '', '', '2014-05-23 10:34:05', '2014-05-23 10:34:05', '', 0, 'http://{$link}/?page_id=8', 0, 'page', '', 0),
(10, 1, '2014-05-23 10:34:22', '2014-05-23 10:34:22', '', 'Portfolio', '', 'publish', 'close', 'close', '', 'portfolio', '', '', '2014-05-23 10:34:22', '2014-05-23 10:34:22', '', 0, 'http://{$link}/?page_id=10', 0, 'page', '', 0),
(12, 1, '2014-05-23 10:34:41', '2014-05-23 10:34:41', '', 'Testimonials', '', 'publish', 'close', 'close', '', 'testimonials', '', '', '2014-05-23 10:34:41', '2014-05-23 10:34:41', '', 0, 'http://{$link}/?page_id=12', 0, 'page', '', 0),
(14, 1, '2014-05-23 10:34:56', '2014-05-23 10:34:56', '', 'Blog', '', 'publish', 'close', 'close', '', 'blog', '', '', '2014-05-23 10:34:56', '2014-05-23 10:34:56', '', 0, 'http://{$link}/?page_id=14', 0, 'page', '', 0),
(16, 1, '2014-05-23 10:35:12', '2014-05-23 10:35:12', '[col grid="3-2 first"]\r\n[map address="Yonge St. and Eglinton Ave, Toronto, Ontario, Canada" width=100% height=600px]\r\n[/col]\r\n\r\n[col grid="3-1"]\r\n<h3>Address</h3>\r\n123 Street Name,\r\nCity, Province\r\n23446\r\n<h3>Phone</h3>\r\n236-298-2828\r\n<h3>Hours</h3>\r\nMon - Fri : 11:00am - 10:00pm\r\nSat : 11:00am - 2:00pm\r\nSun : 12:00am - 11:00pm\r\n[/col]', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2014-05-28 09:36:09', '2014-05-28 09:36:09', '', 0, 'http://{$link}/?page_id=16', 0, 'page', '', 0);
E;


if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
INSERT INTO `{$tablename}` (`post_id`, `meta_key`, `meta_value`) VALUES
(4, '_edit_last', '1'),
(4, '_edit_lock', '1400845991:1'),
(4, 'layout', 'list-post'),
(4, 'content_width', 'default_width'),
(4, 'page_layout', 'sidebar-none'),
(4, 'hide_page_title', 'yes'),
(4, 'order', 'desc'),
(4, 'orderby', 'content'),
(4, 'section_categories', 'default'),
(4, 'allow_sorting', 'default'),
(4, 'display_content', 'content'),
(4, 'feature_size_page', 'blank'),
(4, 'hide_title', 'default'),
(4, 'unlink_title', 'default'),
(4, 'hide_date', 'default'),
(4, 'hide_meta', 'default'),
(4, 'hide_image', 'default'),
(4, 'unlink_image', 'default'),
(4, 'hide_navigation', 'default'),
(4, 'builder_switch_frontend', '0'),
(6, '_edit_last', '1'),
(6, '_edit_lock', '1400841092:1'),
(6, 'layout', 'list-post'),
(6, 'content_width', 'default_width'),
(6, 'page_layout', 'default'),
(6, 'hide_page_title', 'yes'),
(6, 'order', 'desc'),
(6, 'orderby', 'content'),
(6, 'section_categories', 'default'),
(6, 'allow_sorting', 'default'),
(6, 'display_content', 'content'),
(6, 'feature_size_page', 'blank'),
(6, 'hide_title', 'default'),
(6, 'unlink_title', 'default'),
(6, 'hide_date', 'default'),
(6, 'hide_meta', 'default'),
(6, 'hide_image', 'default'),
(6, 'unlink_image', 'default'),
(6, 'hide_navigation', 'default'),
(6, 'builder_switch_frontend', '0'),
(8, '_edit_last', '1'),
(8, 'layout', 'list-post'),
(8, 'content_width', 'default_width'),
(8, 'page_layout', 'default'),
(8, 'hide_page_title', 'yes'),
(8, 'order', 'desc'),
(8, 'orderby', 'content'),
(8, 'section_categories', 'default'),
(8, 'allow_sorting', 'default'),
(8, 'display_content', 'content'),
(8, 'feature_size_page', 'blank'),
(8, 'hide_title', 'default'),
(8, 'unlink_title', 'default'),
(8, 'hide_date', 'default'),
(8, 'hide_meta', 'default'),
(8, 'hide_image', 'default'),
(8, 'unlink_image', 'default'),
(8, 'hide_navigation', 'default'),
(8, 'builder_switch_frontend', '0'),
(8, '_edit_lock', '1400841106:1'),
(10, '_edit_last', '1'),
(10, '_edit_lock', '1401259874:1'),
(10, 'layout', 'grid3'),
(10, 'content_width', 'default_width'),
(10, 'page_layout', 'sidebar-none'),
(10, 'hide_page_title', 'yes'),
(10, 'order', 'desc'),
(10, 'orderby', 'content'),
(10, 'section_categories', 'default'),
(10, 'allow_sorting', 'default'),
(10, 'display_content', 'none'),
(10, 'feature_size_page', 'blank'),
(10, 'hide_title', 'default'),
(10, 'unlink_title', 'default'),
(10, 'hide_date', 'yes'),
(10, 'hide_meta', 'yes'),
(10, 'hide_image', 'default'),
(10, 'unlink_image', 'default'),
(10, 'hide_navigation', 'default'),
(10, 'builder_switch_frontend', '0'),
(12, '_edit_last', '1'),
(12, '_edit_lock', '1400841140:1'),
(12, 'layout', 'list-post'),
(12, 'content_width', 'default_width'),
(12, 'page_layout', 'default'),
(12, 'hide_page_title', 'yes'),
(12, 'order', 'desc'),
(12, 'orderby', 'content'),
(12, 'section_categories', 'default'),
(12, 'allow_sorting', 'default'),
(12, 'display_content', 'content'),
(12, 'feature_size_page', 'blank'),
(12, 'hide_title', 'default'),
(12, 'unlink_title', 'default'),
(12, 'hide_date', 'default'),
(12, 'hide_meta', 'default'),
(12, 'hide_image', 'default'),
(12, 'unlink_image', 'default'),
(12, 'hide_navigation', 'default'),
(12, 'builder_switch_frontend', '0'),
(14, '_edit_last', '1'),
(14, '_edit_lock', '1401260686:1'),
(14, 'layout', 'list-large-image'),
(14, 'content_width', 'default_width'),
(14, 'page_layout', 'sidebar1'),
(14, 'hide_page_title', 'yes'),
(14, 'order', 'desc'),
(14, 'orderby', 'content'),
(14, 'section_categories', 'default'),
(14, 'allow_sorting', 'default'),
(14, 'display_content', 'excerpt'),
(14, 'feature_size_page', 'blank'),
(14, 'hide_title', 'default'),
(14, 'unlink_title', 'default'),
(14, 'hide_date', 'default'),
(14, 'hide_meta', 'default'),
(14, 'hide_image', 'default'),
(14, 'unlink_image', 'default'),
(14, 'hide_navigation', 'default'),
(14, 'builder_switch_frontend', '0'),
(16, '_edit_last', '1'),
(16, '_edit_lock', '1400841171:1'),
(16, 'layout', 'list-post'),
(16, 'content_width', 'default_width'),
(16, 'page_layout', 'default'),
(16, 'hide_page_title', 'yes'),
(16, 'order', 'desc'),
(16, 'orderby', 'content'),
(16, 'section_categories', 'default'),
(16, 'allow_sorting', 'default'),
(16, 'display_content', 'content'),
(16, 'feature_size_page', 'blank'),
(16, 'hide_title', 'default'),
(16, 'unlink_title', 'default'),
(16, 'hide_date', 'default'),
(16, 'hide_meta', 'default'),
(16, 'hide_image', 'default'),
(16, 'unlink_image', 'default'),
(16, 'hide_navigation', 'default'),
(16, 'builder_switch_frontend', '0'),
(18, '_menu_item_type', 'post_type'),
(18, '_menu_item_menu_item_parent', '0'),
(18, '_menu_item_object_id', '4'),
(18, '_menu_item_object', 'page'),
(18, '_menu_item_target', ''),
(18, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(18, '_menu_item_xfn', ''),
(18, '_menu_item_url', ''),
(19, '_menu_item_type', 'post_type'),
(19, '_menu_item_menu_item_parent', '0'),
(19, '_menu_item_object_id', '16'),
(19, '_menu_item_object', 'page'),
(19, '_menu_item_target', ''),
(19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(19, '_menu_item_xfn', ''),
(19, '_menu_item_url', ''),
(20, '_menu_item_type', 'post_type'),
(20, '_menu_item_menu_item_parent', '0'),
(20, '_menu_item_object_id', '14'),
(20, '_menu_item_object', 'page'),
(20, '_menu_item_target', ''),
(20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(20, '_menu_item_xfn', ''),
(20, '_menu_item_url', ''),
(21, '_menu_item_type', 'post_type'),
(21, '_menu_item_menu_item_parent', '0'),
(21, '_menu_item_object_id', '12'),
(21, '_menu_item_object', 'page'),
(21, '_menu_item_target', ''),
(21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(21, '_menu_item_xfn', ''),
(21, '_menu_item_url', ''),
(22, '_menu_item_type', 'post_type'),
(22, '_menu_item_menu_item_parent', '0'),
(22, '_menu_item_object_id', '10'),
(22, '_menu_item_object', 'page'),
(22, '_menu_item_target', ''),
(22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(22, '_menu_item_xfn', ''),
(22, '_menu_item_url', ''),
(23, '_menu_item_type', 'post_type'),
(23, '_menu_item_menu_item_parent', '0'),
(23, '_menu_item_object_id', '8'),
(23, '_menu_item_object', 'page'),
(23, '_menu_item_target', ''),
(23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(23, '_menu_item_xfn', ''),
(23, '_menu_item_url', ''),
(24, '_menu_item_type', 'post_type'),
(24, '_menu_item_menu_item_parent', '0'),
(24, '_menu_item_object_id', '6'),
(24, '_menu_item_object', 'page'),
(24, '_menu_item_target', ''),
(24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(24, '_menu_item_xfn', ''),
(24, '_menu_item_url', ''),
(4, '_themify_builder_settings', 'a:6:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:1:{i:0;a:2:{s:8:"mod_name";s:6:"slider";s:12:"mod_settings";a:17:{s:21:"layout_display_slider";s:6:"slider";s:20:"blog_category_slider";s:7:"|single";s:22:"slider_category_slider";s:7:"|single";s:25:"portfolio_category_slider";s:7:"|single";s:21:"posts_per_page_slider";s:1:"3";s:14:"display_slider";s:7:"content";s:13:"layout_slider";s:14:"slider-default";s:12:"img_w_slider";s:4:"1064";s:12:"img_h_slider";s:3:"550";s:18:"visible_opt_slider";s:1:"1";s:22:"auto_scroll_opt_slider";s:1:"4";s:17:"scroll_opt_slider";s:1:"1";s:16:"speed_opt_slider";s:6:"normal";s:13:"effect_slider";s:6:"scroll";s:11:"wrap_slider";s:3:"yes";s:15:"show_nav_slider";s:3:"yes";s:17:"show_arrow_slider";s:2:"no";}}}}}}i:1;a:2:{s:9:"row_order";s:1:"1";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:45:"<h2 style=''text-align: center;''>Services</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"highlight";s:12:"mod_settings";a:8:{s:16:"layout_highlight";s:5:"grid3";s:18:"category_highlight";s:17:"services|multiple";s:15:"order_highlight";s:4:"desc";s:17:"orderby_highlight";s:4:"date";s:17:"display_highlight";s:7:"content";s:19:"img_width_highlight";s:2:"68";s:20:"img_height_highlight";s:2:"68";s:23:"hide_page_nav_highlight";s:3:"yes";}}}}}}i:2;a:2:{s:9:"row_order";s:1:"2";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:46:"<h2 style=''text-align: center;''>Portfolio</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"portfolio";s:12:"mod_settings";a:11:{s:16:"layout_portfolio";s:5:"grid4";s:18:"category_portfolio";s:10:"0|multiple";s:23:"post_per_page_portfolio";s:1:"4";s:15:"order_portfolio";s:4:"desc";s:17:"orderby_portfolio";s:4:"date";s:17:"display_portfolio";s:4:"none";s:19:"img_width_portfolio";s:3:"240";s:20:"img_height_portfolio";s:3:"140";s:24:"hide_post_date_portfolio";s:3:"yes";s:24:"hide_post_meta_portfolio";s:3:"yes";s:23:"hide_page_nav_portfolio";s:3:"yes";}}}}}}i:3;a:2:{s:9:"row_order";s:1:"3";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:49:"<h2 style=''text-align: center;''>Testimonials</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:11:"testimonial";s:12:"mod_settings";a:9:{s:18:"layout_testimonial";s:5:"grid2";s:20:"category_testimonial";s:21:"testimonials|multiple";s:25:"post_per_page_testimonial";s:1:"4";s:17:"order_testimonial";s:4:"desc";s:19:"orderby_testimonial";s:4:"date";s:19:"display_testimonial";s:7:"content";s:21:"img_width_testimonial";s:2:"80";s:22:"img_height_testimonial";s:2:"80";s:25:"hide_page_nav_testimonial";s:3:"yes";}}}}}}i:4;a:2:{s:9:"row_order";s:1:"4";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:13:"last col-full";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:41:"<h2 style=''text-align: center;''>Blog</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:4:"post";s:12:"mod_settings";a:10:{s:11:"layout_post";s:11:"grid2-thumb";s:13:"category_post";s:10:"0|multiple";s:18:"post_per_page_post";s:1:"4";s:10:"order_post";s:4:"desc";s:12:"orderby_post";s:4:"date";s:12:"display_post";s:7:"excerpt";s:14:"img_width_post";s:3:"100";s:15:"img_height_post";s:3:"100";s:18:"hide_page_nav_post";s:3:"yes";s:11:"font_family";s:7:"default";}}}}}}i:5;a:2:{s:9:"row_order";s:1:"5";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:13:"last col-full";s:7:"modules";a:3:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:45:"<h2 style=''text-align: center;''>Our Team</h2>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:6:"double";s:16:"stroke_w_divider";s:1:"4";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:11:"testimonial";s:12:"mod_settings";a:9:{s:18:"layout_testimonial";s:5:"grid4";s:20:"category_testimonial";s:13:"team|multiple";s:25:"post_per_page_testimonial";s:1:"4";s:17:"order_testimonial";s:4:"desc";s:19:"orderby_testimonial";s:4:"date";s:19:"display_testimonial";s:4:"none";s:21:"img_width_testimonial";s:2:"80";s:22:"img_height_testimonial";s:2:"80";s:25:"hide_page_nav_testimonial";s:3:"yes";}}}}}}}'),
(8, '_themify_builder_settings', 'a:1:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:4:{i:0;a:2:{s:8:"mod_name";s:4:"text";s:12:"mod_settings";a:1:{s:12:"content_text";s:74:"<h1 style=''text-align: center;''>We build easy to use WordPress themes</h1>";}}i:1;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:5:"solid";s:16:"stroke_w_divider";s:1:"1";s:13:"color_divider";s:6:"eeeeee";s:18:"top_margin_divider";s:2:"10";s:21:"bottom_margin_divider";s:2:"30";}}i:2;a:2:{s:8:"mod_name";s:9:"highlight";s:12:"mod_settings";a:9:{s:16:"layout_highlight";s:5:"grid3";s:18:"category_highlight";s:17:"services|multiple";s:23:"post_per_page_highlight";s:1:"6";s:15:"order_highlight";s:4:"desc";s:17:"orderby_highlight";s:4:"date";s:17:"display_highlight";s:7:"content";s:19:"img_width_highlight";s:2:"80";s:20:"img_height_highlight";s:2:"80";s:23:"hide_page_nav_highlight";s:3:"yes";}}i:3;a:2:{s:8:"mod_name";s:7:"divider";s:12:"mod_settings";a:5:{s:13:"style_divider";s:5:"solid";s:16:"stroke_w_divider";s:1:"0";s:13:"color_divider";s:11:"transparent";s:18:"top_margin_divider";s:2:"50";s:21:"bottom_margin_divider";s:2:"50";}}}}}}}'),
(10, '_themify_builder_settings', 'a:1:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:14:"col-full first";s:7:"modules";a:1:{i:0;a:2:{s:8:"mod_name";s:9:"portfolio";s:12:"mod_settings";a:7:{s:16:"layout_portfolio";s:5:"grid3";s:18:"category_portfolio";s:23:"main-portfolio|multiple";s:15:"order_portfolio";s:4:"desc";s:17:"orderby_portfolio";s:4:"date";s:17:"display_portfolio";s:7:"content";s:23:"hide_page_nav_portfolio";s:3:"yes";s:11:"font_family";s:7:"default";}}}}}}}'),
(12, '_themify_builder_settings', 'a:1:{i:0;a:2:{s:9:"row_order";s:1:"0";s:4:"cols";a:1:{i:0;a:2:{s:10:"grid_class";s:19:"col-full first last";s:7:"modules";a:1:{i:0;a:2:{s:8:"mod_name";s:11:"testimonial";s:12:"mod_settings";a:7:{s:18:"layout_testimonial";s:5:"grid2";s:20:"category_testimonial";s:21:"testimonials|multiple";s:17:"order_testimonial";s:4:"desc";s:19:"orderby_testimonial";s:4:"date";s:19:"display_testimonial";s:7:"content";s:25:"hide_page_nav_testimonial";s:3:"yes";s:11:"font_family";s:7:"default";}}}}}}}'),
(14, 'query_category', '0'),
(14, 'posts_per_page', '5');
E;



















if($blog_id == 1)
    $tablename = 'wp_options';
else
    $tablename = 'wp_'. $blog_id .'_options';

$wp_options =<<<E
INSERT INTO `{$tablename}` (`option_name`, `option_value`, `autoload`) VALUES
('widget_pages', 'false', 'yes'),
('widget_calendar', 'false', 'yes'),
('widget_tag_cloud', 'false', 'yes'),
('widget_nav_menu', 'false', 'yes'),
('widget_themify-list-pages', 'false', 'yes'),
('widget_themify-list-categories', 'false', 'yes'),
('widget_themify-recent-comments', 'false', 'yes'),
('widget_themify-social-links', 'false', 'yes'),
('widget_themify-twitter', 'false', 'yes'),
('widget_themify-flickr', 'false', 'yes'),
('widget_themify-most-commented', 'false', 'yes'),
('widget_ngg-images', 'false', 'yes'),
('widget_ngg-mrssw', 'false', 'yes'),
('widget_slideshow', 'false', 'yes'),
('widget_synved_social_share', 'false', 'yes'),
('widget_synved_social_follow', 'false', 'yes'),
('widget_woocommerce_widget_cart', 'false', 'yes'),
('widget_woocommerce_products', 'false', 'yes'),
('widget_woocommerce_layered_nav', 'false', 'yes'),
('widget_woocommerce_layered_nav_filters', 'false', 'yes'),
('widget_woocommerce_price_filter', 'false', 'yes'),
('widget_woocommerce_product_categories', 'false', 'yes'),
('widget_woocommerce_product_search', 'false', 'yes'),
('widget_woocommerce_product_tag_cloud', 'false', 'yes'),
('widget_woocommerce_recent_reviews', 'false', 'yes'),
('widget_woocommerce_recently_viewed_products', 'false', 'yes'),
('widget_woocommerce_top_rated_products', 'false', 'yes'),
('widget_wptt_twittertweets', 'false', 'yes'),
('su_option_prefix', 'su_', 'yes'),
-- uninstall_plugins
-- WPLANG
-- ngg_init_check
-- site_role
-- woocommerce_enable_coupons
-- _wc_needs_pages
-- plan
-- photocrati_pro_recently_activated
-- woocommerce_lock_down_admin


('woocommerce_permalinks', 'false', 'yes');
E;












$db = DB::getConnection();

$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_options');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');
$db->exec($wp_posts);
$db->exec($wp_term_taxonomy);
$db->exec($wp_terms);
$db->exec($wp_postmeta);
$db->exec($wp_term_relationships);
$db->exec($wp_options);
