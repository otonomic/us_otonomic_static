<?php
include_once ABSPATH . "migration/core/ThemifyAbstract.php";

class Parallax extends ThemifyAbstract implements ThemeInterface, ThemePluginSupportInterface {

    public function __construct($blog_id, $page, $fb_category_object){
        parent::__construct($blog_id, $page, $fb_category_object);
        //$this->theme = strtolower(__CLASS__);
	    $this->theme = 'parallax';
    }

	public function install(){
        parent::install();

		/* Load the child theme now */
		if($this->theme == 'parallax') {
            update_option('current_theme', 'Parallaxchild');
            update_option('stylesheet', 'parallaxchild');
        }

		return $this->blog_id;
	}

	public function setup_SEO()
	{
		$OtonomicSEO  = OtonomicSEO::get_instance();

		$home_page_id = $this->get_homepage_id();
		$OtonomicSEO->add_seo_tags( $home_page_id );
		$OtonomicSEO->add_home_seo_tags( );

		$shop_page_id = $this->get_page_id_by_name('Shop');
		$OtonomicSEO->add_seo_tags( $shop_page_id );

		$cart_page_id = $this->get_page_id_by_name('Cart');
		$OtonomicSEO->add_seo_tags( $cart_page_id );

		$checkout_page_id = $this->get_page_id_by_name('Checkout');
		$OtonomicSEO->add_seo_tags( $checkout_page_id );

		$my_account_page_id = $this->get_page_id_by_name('My Account');
		$OtonomicSEO->add_seo_tags( $my_account_page_id );

	}

    public function save_content(){
        kses_remove_filters();
        $this->save_posts();
	    $this->save_notes();
        // NOTE: save_testimonials() is specific to the template - should be part of the template change code
        $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
        $this->save_albums();
        $this->save_videos();
        // $cover_photos_album_facebook_id = $this->set_album_id_cover_photos();

        // $this->get_main_images( $cover_photos_album_facebook_id);
        kses_init_filters();
    }

    function update_homepage() {
        // TODO: Write code that adds a page that would be the homepage
    }

    function add_module_slider()
    {
        $this->set_main_site_slider();
        // $this->add_section_slider();
    }

    function add_module_about()
    {
        $this->add_section_about();
    }

    function add_module_services()
    {
        $this->add_section_services();
    }

    function add_module_store() {
         $this->add_section_store();
    }

    function add_module_blog()
    {
        $this->add_section_blog();
        // $this->add_page_blog();
    }

    function add_module_portfolio()
    {
        $this->add_section_portfolio();
        // $this->add_page_portfolio();
    }

    function add_module_testimonials()
    {
        $this->add_section_testimonials();
        // $this->add_page_testimonials();
    }

    function add_module_social()
    {
        $this->add_section_social();
    }

    function add_module_contact()
    {
        $this->add_section_contact();
    }






    function add_module_plugin_booking()
    {
        global $wpdb;
        $post_content = '[bpscheduler_booking_form]';

        $my_post = array(
            'post_title'    => 'Book an Appointment',
            'post_name'     => 'booking',
            'post_content'  => $post_content,
            'post_status'   => 'publish',
            'post_type' 	  => 'section',
            'menu_order'    => 65,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );

        $post_id = wp_insert_post( $my_post );

        $wpdb->query( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_id ."',25)");

        add_post_meta( $post_id, 'background_image', 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/booking.jpg');
        add_post_meta( $post_id, 'background_repeat', 'fullcover' );
	    //add_post_meta( $post_id, 'background_color', 'ffffff' );



        // add to menu
        $args = array(
            'post_type' => 'nav_menu_item',
            'post_title' => 'Booking',
            'name' => 'booking',
        );

        $post_type_query = new \WP_Query( $args );

        update_post_meta($post_type_query->post->ID, '_menu_item_url', '#booking');
        update_post_meta($post_type_query->post->ID, '_menu_item_type', 'custom');
		update_post_meta($post_type_query->post->ID, '_menu_item_menu_item_parent', '0');
		update_post_meta($post_type_query->post->ID, '_menu_item_object_id', $post_type_query->post->ID);
		update_post_meta($post_type_query->post->ID, '_menu_item_object', 'custom');

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $post_type_query->post->ID ."',43)");
    }



    function add_section_store() {
        global $wpdb;

        //wp_otonomic_add_woocommerce_sample_product();
        //wp_otonomic_add_woocommerce_pages();

        $content = '[recent_products per_page="12" columns="3"][woocommerce_cart]';

        $data_post = array(
            'post_title'   => 'Store',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 70,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );
        $data_postmeta = array(
            'subtitle'          => '',
            'font_color'        => '',
            'background_image'  => 'http://subtlepatterns.com/patterns/subtle_white_feathers.png',
            'background_repeat' => '',
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");


        $OtonomicStore = OtonomicStore::get_instance();
        $OtonomicStore->add_sample_product();
        $OtonomicStore->show_store_pages(false);

        $OtonomicStore->add_store_pages_to_menu( $parent = '2343');


        return $page_id;
    }






    public function add_section_contact() {
        global $wpdb;

	    $content = <<<E
            [otonomic_map  width='100%' height='450px' before='' after='' ]

            [col grid="3-2 first"]
                [otonomic_contact_form]
            [/col]

            [col grid="3-1 last"]
            [otonomic_option option="address" before="<h3>Address</h3>" after=""]
            [otonomic_option option="phone" before="<h3>Phone</h3>" after=""]
            [otonomic_option option="email" before="<h3>Email</h3>" after=""]
            [otonomic_option option="opening_hours" before="<h3>Opening Hours</h3>" after=""]
            [/col]
E;

        $data_post = array(
            'post_title'   => 'Contact',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 75,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );
        $data_postmeta = array(
            'subtitle'          => 'Drop by and say hi...',
            'font_color'        => '',
            'background_image'  => '',
            'background_color'  => 'cae4ed',
            'background_repeat' => '',
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }


    public function add_section_testimonials() {
        global $wpdb;
        // $content = '[team type="testimonial" style="grid4" limit="4" image_w="150" image_h="150" display="none" unlink_image="yes" unlink_title="yes"]';
        $content = '[themify_list_posts post_type="testimonial" limit="4" image="yes" style="grid4 team-post" display="content"]';

        $data_post = array(
            'post_title'   => 'Testimonials',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 60,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );
        /*$data_postmeta = array(
            'subtitle'          => 'Hear what people say about us',
            'font_color'        => '',
            'background_image'  => 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/pattern1.jpg',
            'background_color'  => 'e8eef0',
            'background_repeat' => 'repeat',
        );*/
	    $data_postmeta = array(
		    'subtitle'          => 'Hear what people say about us',
		    'font_color'        => '',
//            'background_image'  => 'http://subtlepatterns.com/patterns/subtle_white_feathers.png',
		    'background_color'  => '5DB3D2',
		    'background_repeat' => '',
	    );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }



    public function add_section_social() {
        $content = <<<E
[otonomic_dpSocialTimeline before ="<h3 style="width: 100%;" >Social Timeline</h3>"]
[otonomic_social network="facebook"]
E;

        $data_post = array(
            'post_title'   => 'Get Social',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 50,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );
        /*$data_postmeta = array(
            'subtitle'          => 'Connect with us on social media',
            'font_color'        => '',
            //'background_image'  => 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/social.jpg',
            'background_image'  => 'http://subtlepatterns.com/patterns/subtle_white_feathers.png',
            'background_repeat' => 'no-repeat',
            'background_color'  =>  'cddfdf',
        );*/
	    $data_postmeta = array(
		    'subtitle'          => 'Connect with us on social media',
		    'font_color'        => '',
		    'background_image'  => 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/social.jpg',
		    'background_repeat' => '',
	    );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }



    function add_section_slider() {
    }



    public function add_section_about() {
        $content = $this->get_content_about();

        // Save content
        $data_post = array(
            'post_title'   => 'About',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 10,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME )
        );
        $data_postmeta = array(
            'subtitle'          => '',
            'font_color'        => '',
            'background_image'  => '',
            'background_color'  => 'e9f5f5',
            'background_repeat' => '',
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        global  $wpdb;
	    $wpdb->query( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }



    public function add_section_services() {
		$content = $this->get_content_services();

        // Save content
        $data_post = array(
            'post_title'   => 'Services',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 15,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        );
        $data_postmeta = array(
            'subtitle'          => '',
            'font_color'        => '',
            'background_image'  => '',
            'background_color'  => 'e9f5f5',
            'background_repeat' => '',
        );
        $page_id = ot_insert_post( $data_post , $data_postmeta);

        return $page_id;
    }


	public function add_module_plugin_NextGen()
	{
		// Search for 'Photo Albums' section
		global $wpdb;
		$content = <<<E
            [ngg_images gallery_ids="1" display_type="photocrati-nextgen_pro_masonry" images="20" width="230" images_per_page="20" disable_pagination="yes"]
E;

		$page_obj = get_page_by_title( 'Gallery', OBJECT, 'section' );
		if($page_obj)
		{
			$post = array(
				'ID'          => $page_obj->ID,
				'post_content' => $content,
			);
			wp_update_post( $post );

		} else {
            return $page_id = $this->insert_section_portfolio($content);
        }
	}


    public function add_section_portfolio()
    {
        global $wpdb;
        $content = <<<E
            [portfolio limit="6" style="grid3" title="yes" post_meta="no" post_date="no"]
E;

        return $page_id = $this->insert_section_portfolio($content);
    }


    function insert_section_portfolio($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
            'post_title'   => 'Gallery',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 20,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        ) + $data_post;

        $data_postmeta = array(
            'subtitle'          => 'Some of our latest photos',
            'font_color'        => '',
            'background_image'  => 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/portfolio.jpg',
            'background_repeat' => '',
        ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);

        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }


    public function add_section_blog() {
        $facebook_link = get_option('facebook_link');
        $content = <<<E
            [list_posts limit="6" image="yes" post_date="yes" style="grid3" display="excerpt" category="blog"]
            <p style="text-align: center;">[button style="large rounded" link="{$facebook_link}" ]Read More[/button]</p>
E;

        return $page_id = $this->insert_section_blog($content);
    }


    function insert_section_blog($content = '', $data_post = [], $data_postmeta = []) {
        $data_post = array(
            'post_title'   => 'Updates',
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_type'    => 'section',
            'menu_order'   => 40,
            'tax_input' => array( 'section-category' => SECTION_CATEGORY_HOME)
        ) + $data_post;

        $data_postmeta = array(
            'subtitle'          => '',
            'font_color'        => '',
            'background_image'  => 'wp-content/mu-plugins/otonomic-first-session/assets/images/backgrounds/pattern1.jpg',
            'background_repeat' => 'repeat',
		    'background_color'  =>  'f6f3f0',
        ) + $data_postmeta;

        $page_id = ot_insert_post( $data_post , $data_postmeta);

        global $wpdb;
        $wpdb->get_results( "insert into wp_". $this->blog_id ."_term_relationships (object_id,term_taxonomy_id) VALUES ('". $page_id ."',25)");
        return $page_id;
    }







    function add_page_testimonials() {
    }

    function add_page_portfolio() {
    }

    function add_page_blog() {
    }







    public function save_theme_data(){
        // TODO: Omri: Understand how set_widgets work
        // $this->set_widgets();
        $this->set_themify_options();
        $this->update_options();

        $this->set_menu();
    }



    function set_menu(){
        /*
         * TODO: Fix this function. It should be run as a callback function whenever:
         *    new post added/deleted
         *    new testimonial added/deleted
         *    youtube/twitter/flickr/social account updated
         */
		global $wpdb;

	    try
	    {
		$menu_exists = wp_get_nav_menu_object( 'Home menu' );
		// If it doesn't exist, let's create it.
		if( !$menu_exists)
		{
			wp_create_nav_menu('Home menu');
		}

		$menu_data = wp_get_nav_menu_object( 'Home menu' );

		$locations = get_theme_mod('nav_menu_locations');
		$locations['main-nav'] = $menu_data->term_id;
		set_theme_mod( 'nav_menu_locations', $locations );

        /*
        $categories = array('home','about','booking','portfolio','blog','testimonials','store','social','contact');

		$posts = $this->page->posts;
		$testimonials = $this->page->testimonials;

	    $q = 'UPDATE wp_'. $this->blog_id .'_posts SET post_status = "draft" where post_type="nav_menu_item"';
	    $wpdb->query( $q );

		foreach($categories as $key=>$value){
			$post_status = 'publish';

			if($value == 'blog' && count($posts) == 0){
					$post_status = 'draft';
			}

			if($value == 'team' && count($testimonials) == 0){
					$post_status = 'draft';
			}

	        $q = 'UPDATE wp_'. $this->blog_id .'_posts SET menu_order = '. $key .', post_status = "'. $post_status .'" where post_name ="'. $value .'" AND post_type="nav_menu_item"';
			$wpdb->query( $q );
		}
        */

		$sql = "select * from wp_{$this->blog_id}_posts where  post_type='nav_menu_item'";
		$nav_items = $wpdb->get_results($sql);
		foreach ($nav_items as $nav_item)
		{
			wp_set_object_terms($nav_item->ID, 'home-menu', 'nav_menu');
			//wp_update_nav_menu_item($menu_data->term_id, $item->ID );
		}
	}
	    catch(Exception $e)
	    {
		    /* Silently ignore the error */
	    }
	}


    protected function set_main_slider_images(array $image_ids){
//        if(count($image_ids) == 0) { return; }
//
//        if(is_array($image_ids)) { $image_ids = implode(',', $image_ids); }
//        update_post_meta($this->get_homepage_id(), 'background_gallery', '[gallery ids="'. $image_ids .'"]');
    }



	public function set_themify_options()
	{
		try
		{
            include_once('config/themify_options.php');
            $data = get_themify_options();
            if ( get_option( 'themify_data' ) !== false )
            {
                update_option( 'themify_data' , $data );
            }
            else
            {
                add_option( 'themify_data', $data, null, 'yes' );
            }
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
        switch_theme($this->theme,$this->theme);
	}
}
