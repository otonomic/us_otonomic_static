<?php
if(empty($blog_id)) {
	$blog_id = $_GET['blog_id'];
}
if(!$blog_id) {
	die('Missing blog id');
}

$link = $domain .'/'. $page->username;
set_time_limit(0);

$site_url = get_site_url();

/* Terms */
if($blog_id == 1)
	$tablename = 'wp_terms';
else
	$tablename = 'wp_'. $blog_id .'_terms';

$wp_terms =<<<E
INSERT INTO `{$tablename}` (`term_id`, `name`, `slug`, `term_group`) VALUES
	(1, 'Uncategorized', 'uncategorized', 0),
	(2, 'Blog', 'blog', 0),
	(3, 'Images', 'images', 0),
	(4, 'News', 'news', 0),
	(7, 'Updates', 'updates', 0),
	(8, 'Video', 'video', 0),
	(18, 'Corporate', 'corporate', 0),
	(24, 'Home', 'home', 0),
	(31, 'Slider', 'slider', 0),
	(32, 'Team', 'team', 0),
	(33, 'Testimonials', 'testimonials', 0),
	(41, 'Main Menu', 'main-menu', 0),
	(42, 'Otonomic Main Menu', 'otonomic-main-menu', 0),
	(43, 'simple', 'simple', 0),
	(44, 'grouped', 'grouped', 0),
	(45, 'variable', 'variable', 0),
	(46, 'external', 'external', 0),
	(47, 'pending', 'pending', 0),
	(48, 'failed', 'failed', 0),
	(49, 'on-hold', 'on-hold', 0),
	(50, 'processing', 'processing', 0),
	(51, 'completed', 'completed', 0),
	(52, 'refunded', 'refunded', 0),
	(53, 'cancelled', 'cancelled', 0),
	(54, 'Top Navigation', 'top-navigation', 0);
E;

/* Term Taxonomy */
if($blog_id == 1)
	$tablename = 'wp_term_taxonomy';
else
	$tablename = 'wp_'. $blog_id .'_term_taxonomy';

$wp_term_taxonomy =<<<E

INSERT INTO `{$tablename}` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
	(1, 1, 'category', '', 0, 0),
	(2, 2, 'category', '', 0, 15),
	(3, 3, 'category', '', 0, 15),
	(4, 4, 'category', '', 0, 0),
	(7, 7, 'category', '', 0, 0),
	(8, 8, 'category', '', 0, 3),
	(18, 18, 'slider-category', '', 0, 3),
	(25, 24, 'section-category', '', 0, 8),
	(32, 31, 'slider-category', '', 0, 4),
	(33, 32, 'testimonial-category', '', 0, 4),
	(34, 33, 'testimonial-category', '', 0, 18),
	(43, 41, 'nav_menu', '', 0, 8),
	(47, 42, 'nav_menu', '', 0, 0),
	(48, 43, 'product_type', '', 0, 0),
	(49, 44, 'product_type', '', 0, 0),
	(50, 45, 'product_type', '', 0, 0),
	(51, 46, 'product_type', '', 0, 0),
	(52, 47, 'shop_order_status', '', 0, 0),
	(53, 48, 'shop_order_status', '', 0, 0),
	(54, 49, 'shop_order_status', '', 0, 0),
	(55, 50, 'shop_order_status', '', 0, 0),
	(56, 51, 'shop_order_status', '', 0, 0),
	(57, 52, 'shop_order_status', '', 0, 0),
	(58, 53, 'shop_order_status', '', 0, 0),
	(59, 1, 'section-category', '', 0, 0),
	(60, 54, 'nav_menu', '', 0, 2);
E;

/* Term Relationships */
if($blog_id == 1)
	$tablename = 'wp_term_relationships';
else
	$tablename = 'wp_'. $blog_id .'_term_relationships';

$wp_term_relationships =<<<E
INSERT INTO `{$tablename}` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
	(0, 2, 0),
	(0, 3, 0),
	(0, 43, 0),
	(2206, 25, 0),
	(2208, 25, 0),
	(2209, 25, 0),
	(2210, 25, 0),
	(2211, 0, 0),
	(2224, 43, 0),
	(2225, 43, 0),
	(2228, 43, 0),
	(2229, 43, 0),
	(2230, 43, 0),
	(2231, 43, 0),
	(2239, 43, 0),
	(2264, 0, 0),
	(2268, 0, 0),
	(2283, 43, 0),
	(2293, 43, 0),
	(2294, 43, 0),
	(2295, 43, 0),
	(2296, 43, 0),
	(2297, 43, 0),
	(2298, 43, 0),
	(2299, 43, 0),
	(2300, 43, 0),
	(2301, 43, 0),
	(2302, 43, 0),
	(2303, 43, 0),
	(2304, 43, 0),
	(2305, 43, 0),
	(2335, 25, 0),
	(2340, 25, 0),
	(2342, 43, 0),
	(2343, 43, 0),
	(2344, 2, 0),
	(2344, 3, 0),
	(2344, 25, 0),
	(2345, 25, 0),
	(2346, 2, 0),
	(2346, 3, 0),
	(2346, 59, 0),
	(2347, 59, 0),
	(2348, 2, 0),
	(2348, 3, 0),
	(2350, 2, 0),
	(2350, 3, 0),
	(2352, 2, 0),
	(2352, 3, 0),
	(2354, 2, 0),
	(2354, 3, 0),
	(2355, 2, 0),
	(2355, 3, 0),
	(2356, 2, 0),
	(2356, 3, 0),
	(2357, 2, 0),
	(2357, 3, 0),
	(2358, 2, 0),
	(2358, 3, 0),
	(2359, 2, 0),
	(2359, 3, 0),
	(2360, 2, 0),
	(2360, 3, 0),
	(2361, 2, 0),
	(2361, 3, 0),
	(2362, 2, 0),
	(2362, 3, 0),
	(2363, 2, 0),
	(2363, 3, 0),
	(2364, 2, 0),
	(2364, 3, 0),
	(2366, 2, 0),
	(2366, 3, 0),
	(2368, 2, 0),
	(2368, 3, 0),
	(2370, 2, 0),
	(2370, 3, 0),
	(2372, 2, 0),
	(2372, 3, 0),
	(2374, 34, 0),
	(2376, 34, 0),
	(2378, 34, 0),
	(2380, 34, 0),
	(2382, 34, 0),
	(2384, 34, 0),
	(2386, 34, 0),
	(2388, 34, 0),
	(2390, 34, 0),
	(2392, 34, 0),
	(2394, 34, 0),
	(2396, 34, 0),
	(2398, 34, 0),
	(2400, 34, 0),
	(2402, 34, 0),
	(2404, 34, 0),
	(2406, 34, 0),
	(2408, 34, 0),
	(2410, 34, 0),
	(2412, 34, 0),
	(2414, 34, 0),
	(2416, 34, 0),
	(2418, 34, 0),
	(2420, 34, 0),
	(2422, 34, 0),
	(2424, 34, 0),
	(2426, 34, 0),
	(2428, 34, 0),
	(2430, 34, 0),
	(2432, 34, 0),
	(2434, 34, 0),
	(2436, 34, 0),
	(2438, 34, 0),
	(2440, 34, 0),
	(2442, 34, 0),
	(2444, 34, 0),
	(2854, 25, 0),
	(2855, 25, 0),
	(2856, 25, 0),
	(2857, 25, 0),
	(2858, 25, 0),
	(2859, 25, 0),
	(2866, 25, 0),
	(2867, 43, 0),
	(2868, 43, 0),
	(2869, 43, 0),
	(2873, 60, 0),
	(2874, 60, 0),
	(2879, 43, 0),
	(2955, 59, 0),
	(2956, 59, 0),
	(2957, 59, 0),
	(2958, 59, 0),
	(2959, 59, 0),
	(2960, 59, 0),
	(2961, 59, 0),
	(2962, 59, 0),
	(2963, 59, 0),
	(2964, 59, 0),
	(2965, 59, 0)
 ;
E;

/* Posts */
if($blog_id == 1)
	$tablename = 'wp_posts';
else
	$tablename = 'wp_'. $blog_id .'_posts';

$wp_posts =<<<E
	INSERT INTO `{$tablename}` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
	(2333, 1, '2014-05-08 12:32:20', '2014-05-08 12:32:20', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit "Send"]</p>\n[your-subject]\n[your-name] <[your-email]>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\nroman.raslin@gmail.com\n\n\n0\n\n[your-subject]\n[your-name] <[your-email]>\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)\n[your-email]\n\n\n0\nYour message was sent successfully. Thanks.\nFailed to send your message. Please try later or contact the administrator by another method.\nValidation errors occurred. Please confirm the fields and submit it again.\nFailed to send your message. Please try later or contact the administrator by another method.\nPlease accept the terms to proceed.\nPlease fill the required field.', 'Contact form 1', '', 'publish', 'open', 'open', '', 'contact-form-1', '', '', '2014-05-08 12:32:20', '2014-05-08 12:32:20', '', 0, '', 0, 'wpcf7_contact_form', '', 0),
;
E;

if($blog_id == 1)
	$tablename = 'wp_postmeta';
else
	$tablename = 'wp_'. $blog_id .'_postmeta';

$wp_postmeta =<<<E
	INSERT INTO `{$tablename}` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
	(5640, 2333, '_form', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit "Send"]</p>'),
	(5641, 2333, '_mail', 'a:7:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:26:"[your-name] <[your-email]>";s:4:"body";s:86:"From: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]";s:9:"recipient";s:0:"";s:18:"additional_headers";s:0:"";s:11:"attachments";s:0:"";s:8:"use_html";b:0;}'),
	(5642, 2333, '_mail_2', 'a:8:{s:6:"active",b:0,s:7:"subject",s:14:"[your-subject]",s:6:"sender",s:26:"[your-name] <[your-email]>",s:4:"body",s:105:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Romans test (http://wp.test/)",s:9:"recipient",s:12:"[your-email]",s:18:"additional_headers",s:0:"",s:11:"attachments",s:0:"",s:8:"use_html",i:0,}'),
	(5643, 2333, '_messages', 'a:21:{s:12:"mail_sent_ok";s:43:"Your message was sent successfully. Thanks.";s:12:"mail_sent_ng";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:16:"validation_error";s:74:"Validation errors occurred. Please confirm the fields and submit it again.";s:4:"spam";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:12:"accept_terms";s:35:"Please accept the terms to proceed.";s:16:"invalid_required";s:31:"Please fill the required field.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:14:"invalid_number";s:28:"Number format seems invalid.";s:16:"number_too_small";s:25:"This number is too small.";s:16:"number_too_large";s:25:"This number is too large.";s:13:"invalid_email";s:28:"Email address seems invalid.";s:11:"invalid_url";s:18:"URL seems invalid.";s:11:"invalid_tel";s:31:"Telephone number seems invalid.";s:23:"quiz_answer_not_correct";s:27:"Your answer is not correct.";s:12:"invalid_date";s:26:"Date format seems invalid.";s:14:"date_too_early";s:23:"This date is too early.";s:13:"date_too_late";s:22:"This date is too late.";s:13:"upload_failed";s:22:"Failed to upload file.";s:24:"upload_file_type_invalid";s:30:"This file type is not allowed.";s:21:"upload_file_too_large";s:23:"This file is too large.";s:23:"upload_failed_php_error";s:38:"Failed to upload file. Error occurred.";}'),
	(5644, 2333, '_additional_settings', ''),
	(5645, 2333, '_locale', 'en_US'),
;
E;


$db = DB::getConnection();


$db->exec('TRUNCATE wp_'. $blog_id .'_term_taxonomy');
$db->exec('TRUNCATE wp_'. $blog_id .'_terms');
$db->exec('TRUNCATE wp_'. $blog_id .'_postmeta');
$db->exec('TRUNCATE wp_'. $blog_id .'_posts');
$db->exec('TRUNCATE wp_'. $blog_id .'_term_relationships');
$db->exec('TRUNCATE wp_'. $blog_id .'_comments');

$db->exec($wp_terms);
$db->exec($wp_term_taxonomy);
$db->exec($wp_term_relationships);
$db->exec($wp_posts);
$db->exec($wp_postmeta);