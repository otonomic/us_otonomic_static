<?php
include_once ABSPATH . "migration/core/ThemifyAbstract.php";

class Magazine extends ThemifyAbstract implements ThemeInterface, ThemePluginSupportInterface
{
    private $homepage_id;

    public function __construct($blog_id, $page, $fb_category_object)
    {
        parent::__construct($blog_id, $page, $fb_category_object);
        $this->theme = strtolower(__CLASS__);
    }

    public function install()
    {
        $this->init();
        $this->save_site_data();

        $this->save_content();
        $this->save_theme_data();
        $this->install_plugins();
        $this->finalize();

        return $this->blog_id;
    }
	function switch_to_theme($page_data)
	{
		/* Create home page */
		$this->add_page_home();
		/* Create about us page */
		$this->add_aboutpage_from_content( $page_data['about'] );
		/* Create service page */
		//$this->add_page_service(); // Not needed so lets skip this section //
		/* Create portfolio page */
		$this->add_page_portfolio();
		/* Create testimonials page */
		$this->add_page_testimonials();
		/* Create Blog page */
		$this->add_page_blog();
		/* Create Contact us page page */
		$this->add_page_contact();

		$this->add_page_booking();
		$this->add_page_store();

		$this->save_theme_data();
		$this->install_plugins();
		$this->finalize();

	}
    public function install_plugins()
    {
        $this->setup_plugins();
    }

    public function init()
    {
        global $wpdb;
        $domain = DOMAIN_CURRENT_SITE;
        $blog_id = $this->blog_id;

        // NOTE: This first_data file contains template specific data
        include_once "sql/first_data.php";
        $this->set_site_settings(); // May be we dont need this //
        // $this->save_taxonomies();
    }

    public function get_homepage_id()
    {
        return $this->homepage_id;
    }

    public function save_content()
    {
	    $this->save_posts();
	    $this->save_testimonials( $testimonial_categories = array(33), $team_categories = array(33), $team_post_type='team' );
	    $this->save_albums();

	    $this->setup_modules();

	    $slider_images = $this->get_images_for_slider();
	    add_option('slider_images', $slider_images);


		//$this->get_main_images( $cover_photos_album_facebook_id);


    }

	function setup_modules()
	{
		/* Create home page */
		$this->add_page_home();
		/* Create about us page */
		$this->add_module_about();
		/* Create service page */
		//$this->add_module_services(); // Not needed so lets skip this section //
		/* Create portfolio page */
		$this->add_module_portfolio();
		/* Create testimonials page */
		$this->add_module_testimonials();
		/* Create Blog page */
		$this->add_module_blog();
		/* Create Contact us page page */
		$this->add_module_contact();
		/* Create Booking page page */
		$this->add_module_plugin_booking();
		/* Create Store page page */
		$this->add_module_store();
	}
	function add_module_about()
	{
		try
		{
			$this->add_page_about();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_services()
	{
		try
		{
			//$this->add_page_service(); // Not needed so lets skip this section //
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_portfolio()
	{
		try
		{
			$this->add_page_portfolio();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_testimonials()
	{
		try
		{
			$this->add_page_testimonials();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_blog()
	{
		try
		{
			$this->add_page_blog();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_contact()
	{
		try
		{
			$this->add_page_contact();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_plugin_booking()
	{
		try
		{
			$this->add_page_booking();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}
	function add_module_store()
	{
		try
		{
			$this->add_page_store();
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
	}

    public function save_theme_data()
    {
	    $this->set_themify_options();
	    $this->update_options();
    }

	private function set_widgets(){
		$reflector = new ReflectionClass(get_class($this));
		$path = dirname($reflector->getFileName());

		$about_content = $this->page->facebook->about;

		$widgets_list = include_once $path.'/config/widgets.php';

		if(count($widgets_list)>0)
		{
			foreach($widgets_list as $option_name => $option_value)
			{
				update_option($option_name, $option_value);
			}
		}

	}

    public function finalize()
    {
	    $this->set_menu();
	    $this->set_widgets();
    }

	private function set_themify_options()
	{
		try
		{
			include_once('config/themify_options.php');
			$data = get_themify_options();
			if ( get_option( 'themify_data' ) !== false ) {
				update_option( 'themify_data' , $data );
			} else {
				add_option( 'themify_data', $data, null, 'yes' );
			}
		}
		catch(Exception $e)
		{
			/* Silently ignore the error */
		}
		switch_theme($this->theme,$this->theme);
	}

	protected function update_options()
	{
		global $wpdb;

		update_option( 'page_on_front', $this->get_homepage_id() );
		update_option( 'show_on_front', 'page' );

		update_option( 'template', $this->theme );
		update_option( 'stylesheet', $this->theme );
		update_option( 'current_theme', $this->theme );

		update_option( 'last_update', time() );
		update_option( 'facebook_id', $this->page->facebook->id );
	}

	private function set_menu()
	{
		global $wpdb;

		/* Home menu */
		$menu_exists = wp_get_nav_menu_object( 'Main menu' );
		if( !$menu_exists)
		{
			wp_create_nav_menu('Main menu');
		}
		$main_menu_data = wp_get_nav_menu_object( 'Main menu' );
		$locations = get_theme_mod('nav_menu_locations');
		$locations['main-nav'] = $main_menu_data->term_id;
		$locations['footer-nav'] = $main_menu_data->term_id;
		set_theme_mod( 'nav_menu_locations', $locations );

		/* Main menu items */
		$home_nav_items = array('Home'=>'Home', 'Portfolio'=>'Portfolio', 'Book an Appointment'=>'Booking', 'Blog'=>'Blog', 'Testimonials'=>'Testimonials', 'Shop'=>'Shop');
		$store_nav_items = array('Cart' => 'Cart', 'My Account' => 'My Account', 'Checkout' => 'Checkout');

		if(count($home_nav_items)>0)
		{
			$menu_pos = 0;
			foreach($home_nav_items as $page_name => $nav_title)
			{
				$menu_pos++;
				/* Fetch the selected page via its name */
				$page_obj = get_page_by_title( $page_name );
				if($page_obj)
				{
					if($page_obj->post_status == 'publish')
					{
						$menu_db_id = null;
						$nav_item_obj = get_page_by_title( $nav_title, OBJECT, 'nav_menu_item' );
						if($nav_item_obj)
						{
							$menu_db_id = $nav_item_obj->ID;
						}
						/* Lets Add this menu */
						$permalink = get_permalink( $page_obj->ID );
						wp_update_nav_menu_item($main_menu_data->term_id, $menu_db_id, array(
								'menu-item-title' =>  $nav_title,
								'menu-item-classes' => '',
								'menu-item-url' => $permalink,
								'menu-item-parent-id' => 0,
								'menu-item-position'=> $menu_pos,
								'menu-item-status' => 'publish')
						);
					}
				}
			}
		}
		if(count($store_nav_items)>0)
		{
			$store_page_obj = get_page_by_title( 'Shop', null, 'nav_menu_item' );
			$menu_pos = 0;
			foreach($store_nav_items as $page_name => $nav_title)
			{
				$menu_pos++;
				/* Fetch the selected page via its name */
				$page_obj = get_page_by_title( $page_name );
				if($page_obj)
				{
					if($page_obj->post_status == 'publish')
					{
						$menu_db_id = null;
						$nav_item_obj = get_page_by_title( $nav_title, OBJECT, 'nav_menu_item' );
						if($nav_item_obj)
						{
							$menu_db_id = $nav_item_obj->ID;
						}
						/* Lets Add this menu */
						$permalink = get_permalink( $page_obj->ID );
						wp_update_nav_menu_item($main_menu_data->term_id, $menu_db_id, array(
								'menu-item-title' =>  $nav_title,
								'menu-item-classes' => '',
								'menu-item-url' => $permalink,
								'menu-item-parent-id' => intval($store_page_obj->ID),
								'menu-item-position'=> $menu_pos,
								'menu-item-status' => 'publish')
						);
					}
				}
			}
		}


		/* Top menu */
		$menu_exists = wp_get_nav_menu_object( 'Top Navigation' );
		if( !$menu_exists)
		{
			wp_create_nav_menu('Top Navigation');
		}
		$top_menu_data = wp_get_nav_menu_object( 'Top Navigation' );
		$locations = get_theme_mod('nav_menu_locations');
		$locations['top-nav'] = $top_menu_data->term_id;
		set_theme_mod( 'nav_menu_locations', $locations );

		/* Top menu items */
		$store_nav_items = array('About'=>'About', 'Contact Us'=>'Contact');
		if(count($home_nav_items)>0)
		{
			$menu_pos = 0;
			foreach($store_nav_items as $page_name => $nav_title)
			{
				$menu_pos++;
				/* Fetch the selected page via its name */
				$page_obj = get_page_by_title( $page_name );
				if($page_obj)
				{
					if($page_obj->post_status == 'publish')
					{
						$menu_db_id = null;
						$nav_item_obj = get_page_by_title( $nav_title, OBJECT, 'nav_menu_item' );
						if($nav_item_obj)
						{
							$menu_db_id = $nav_item_obj->ID;
						}
						/* Lets Add this menu */
						$permalink = get_permalink( $page_obj->ID );
						wp_update_nav_menu_item($top_menu_data->term_id, $menu_db_id, array(
								'menu-item-title' =>  $nav_title,
								'menu-item-classes' => '',
								'menu-item-url' => $permalink,
								'menu-item-parent-id' => 0,
								'menu-item-position'=> $menu_pos,
								'menu-item-status' => 'publish')
						);
					}
				}
			}
		}

	}



	private function add_page_home()
	{
		$row = array();
		/* First row */
		$row[0] = new stdClass;
		$row[0]->row_order = 0;
		$row[0]->cols[0] = new stdClass;
		/* Top slider */
		$row[0]->cols[0]->grid_class = 'col-full first last';
		$row[0]->cols[0]->modules[0]= array(
			'mod_name' => 'text',
			'mod_settings' => array(
				'content_text' => '<p>[otonomic_slider data=""]</p>',
				'font_family' => 'default',
			)
		);
		/* 2 column section starts */
		$row[1] = new stdClass;
		$row[2]->row_order = 1;
		$row[1]->cols[0] = new stdClass;
		$row[1]->cols[0]->grid_class = 'col4-3 first';
		/* portfolio */
		$row[1]->cols[0]->modules[0]= array(
			'mod_name' => 'portfolio',
			'mod_settings' => array(
				'layout_portfolio' => 'grid3',
				'category_portfolio' => '0|multiple',
				'post_per_page_portfolio' => '3',
				'order_portfolio' => 'desc',
				'orderby_portfolio' => 'date',
				'display_portfolio' => 'none',
				'img_width_portfolio' => '155',
				'img_height_portfolio' => '105',
				'hide_post_title_portfolio' => 'no',
				'hide_post_date_portfolio' => 'yes',
				'hide_post_meta_portfolio' => 'yes',
				'hide_page_nav_portfolio' => 'yes',
				'font_family' => 'default',
				'css_post' => 'portfolio-page-rc',
			)
		);
		/* posts heading */
		$row[1]->cols[0]->modules[1]= array(
			'mod_name' => 'text',
			'mod_settings' => array(
				'mod_title_text' => 'Posts',
				'font_family' => 'default',
			)
		);
		/* posts */
		$row[1]->cols[0]->modules[2]= array(
			'mod_name' => 'post',
			'mod_settings' => array(
				'layout_post' => 'grid2',
				'category_post' => '|single',
				'post_per_page_post' => '2',
				'order_post' => 'desc',
				'orderby_post' => 'date',
				'display_post' => 'excerpt',
				'img_width_post' => '245',
				'img_height_post' => '156',
				'hide_post_date_post' => 'yes',
				'hide_post_meta_post' => 'yes',
				'hide_page_nav_post' => 'yes',
				'font_family' => 'default',
				'css_post' => 'home-large-posts',
			)
		);
		/* posts */
		$row[1]->cols[0]->modules[3]= array(
			'mod_name' => 'post',
			'mod_settings' => array(
				'layout_post' => 'grid2-thumb',
				'category_post' => '0|multiple',
				'post_per_page_post' => '6',
				'offset_post' => '2',
				'order_post' => 'desc',
				'orderby_post' => 'date',
				'display_post' => 'none',
				'img_width_post' => '65',
				'img_height_post' => '65',
				'hide_post_date_post' => 'yes',
				'hide_post_meta_post' => 'yes',
				'hide_page_nav_post' => 'yes',
				'font_family' => 'default',
				'css_post' => 'home-small-posts',
			)
		);
		/* videos */
		$row[1]->cols[0]->modules[4]= array(
			'mod_name' => 'post',
			'mod_settings' => array(
				'mod_title_post' => 'Video',
				'layout_post' => 'grid2',
				'category_post' => 'video|multiple',
				'post_per_page_post' => '2',
				'order_post' => 'desc',
				'orderby_post' => 'date',
				'display_post' => 'none',
				'img_width_post' => '245',
				'img_height_post' => '156',
				'hide_post_date_post' => 'yes',
				'hide_post_meta_post' => 'yes',
				'hide_page_nav_post' => 'yes',
				'font_family' => 'default',
			)
		);
		/* second column starts */
		$row[1]->cols[1] = new stdClass;
		$row[1]->cols[1]->grid_class = 'col4-1 last';
		/* Facebook */
		$row[1]->cols[1]->modules[0]= array(
			'mod_name' => 'text',
			'mod_settings' => array(
				'mod_title_text' => 'Facebook',
				'content_text' => '<p>[otonomic_fb_like width="226" height="320" /]</p>',
				'font_family' => 'default',
				'add_css_text' => 'widget',
			)
		);
		/* Twitter */
		$row[1]->cols[1]->modules[1]= array(
			'mod_name' => 'text',
			'mod_settings' => array(
				'mod_title_text' => 'Latest Tweets',
				'content_text' => '<p>[otonomic_twitter username="twitter" /]</p>',
				'font_family' => 'default',
				'add_css_text' => 'widget',
			)
		);
		/* Reviews */
		$row[1]->cols[1]->modules[2]= array(
			'mod_name' => 'text',
			'mod_settings' => array(
				'mod_title_text' => 'Recent Reviews',
				'content_text' => '<p>[otonomic_recent_reviews count=5 /]</p>',
				'font_family' => 'default',
				'add_css_text' => 'widget',
			)
		);




		$builder_data = $this->get_builder_data( $row );
		$builder_data = serialize($builder_data);

		/* Now save page content */

		$data_post = array(
			'post_title'   => 'Home',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 10,
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
		);
		$data_postmeta = array(
			'_themify_builder_settings'	=> $builder_data,
			'layout'        => 'list-post',
			'content_width'  => 'default_width',
			'page_layout' => 'sidebar-none',
			'hide_page_title'=>'yes',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
		);

		/* Now lets create the page */
		return $this->homepage_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_about()
	{
		$content = $this->get_content_about( );
		return $page_id = $this->add_aboutpage_from_content($content);
	}
	protected function add_aboutpage_from_content($content)
	{
		// Save content
		$data_post = array(
			'post_title'   => 'About',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 20,
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
		);
		$data_postmeta = array(
			'layout'        => 'list-post',
			'content_width'  => 'default_width',
			'page_layout' => 'default',
			'hide_page_title'=>'default',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_portfolio()
	{
		$row = array();
		/* First row */
		$row[0] = new stdClass;
		$row[0]->row_order = 0;
		$row[0]->cols[0] = new stdClass;
		$row[0]->cols[0]->grid_class = 'col-full first last';
		$row[0]->cols[0]->modules[0]= array(
			'mod_name' => 'slider',
			'mod_settings' => array(
				'layout_display_slider' => 'portfolio',
				'blog_category_slider' => '|single',
				'slider_category_slider' => '|single',
				'portfolio_category_slider' => '|single',
				'testimonial_category_slider' => '|single',
				'posts_per_page_slider' => '5',
				'display_slider' => 'none',
				'layout_slider' => 'slider-default',
				'image_size_slider' => 'thumbnail',
				'img_w_slider' => '266',
				'img_h_slider' => '200',
				'visible_opt_slider' => '4',
				'auto_scroll_opt_slider' => '4',
				'scroll_opt_slider' => '1',
				'speed_opt_slider' => 'normal',
				'effect_slider' => 'scroll',
				'pause_on_hover_slider' => 'resume',
				'wrap_slider' => 'yes',
				'show_nav_slider' => 'yes',
				'show_arrow_slider' => 'no',
				'font_family' => 'default',
				'css_slider' => 'portfolio-page-rc',
			)
		);
		$row[0]->cols[0]->modules[1]= array(
			'mod_name' => 'divider',
			'mod_settings' => array(
				'style_divider' => 'solid',
				'color_divider' => 'eeeeee',
			)
		);
		$row[0]->cols[0]->modules[2]= array(
			'mod_name' => 'portfolio',
			'mod_settings' => array(
				'layout_portfolio' => 'grid4',
				'category_portfolio' => '0|multiple',
				'order_portfolio' => 'desc',
				'orderby_portfolio' => 'date',
				'display_portfolio' => 'none',
				'img_width_portfolio' => '240',
				'img_height_portfolio' => '121',
				'hide_post_date_portfolio' => 'yes',
				'hide_post_meta_portfolio' => 'yes',
				'hide_page_nav_portfolio' => 'yes',
				'font_family' => 'default',
				'css_portfolio' => 'portfolio-page-rc',
			)
		);

		$builder_data = $this->get_builder_data( $row );
		$builder_data = serialize($builder_data);

		$data_post = array(
			'post_title'   => 'Portfolio',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 30,
			'comment_status'=>'closed',
		);

		$data_postmeta = array(
			'_themify_builder_settings'	=> $builder_data,
			'layout'        => 'list-post',
			'content_width'  => 'default_width',
			'page_layout' => 'sidebar-none',
			'hide_page_title'=>'default',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_testimonials()
	{
		$row = array();
		/* First row */
		$row[0] = new stdClass;
		$row[0]->row_order = 0;
		$row[0]->cols[0] = new stdClass;
		$row[0]->cols[0]->grid_class = 'col-full first last';
		$row[0]->cols[0]->modules[0]= array(
			'mod_name' => 'testimonial',
			'mod_settings' => array(
				'layout_testimonial' => 'grid2',
				'category_testimonial' => 'testimonials|multiple',
				'order_testimonial' => 'desc',
				'orderby_testimonial' => 'date',
				'display_testimonial' => 'excerpt',
				'img_width_testimonial' => '150',
				'img_height_testimonial' => '150',
				'hide_page_nav_testimonial' => 'yes',
				'font_family' => 'default',
			)
		);

		$builder_data = $this->get_builder_data( $row );
		$builder_data = serialize($builder_data);

		$data_post = array(
			'post_title'   => 'Testimonials',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 50,
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
		);

		$data_postmeta = array(
			'_themify_builder_settings'	=> $builder_data,
			'layout'        => 'list-post',
			'page_layout' => 'default',
			'content_width'  => 'default_width',
			'hide_page_title'=>'default',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
			''=>'',
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_blog()
	{
		$data_post = array(
			'post_title'   => 'Blog',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'menu_order'   => 40,
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
		);

		$data_postmeta = array(
			'layout'        => 'list-post',
			'page_layout' => 'sidebar1',
			'content_width'  => 'default_width',
			'hide_page_title'=>'default',
			'query_category'=>'0',
			'order'=>'desc',
			'orderby'=>'content',
			'display_content'=>'excerpt',
			'hide_title'=>'yes',
			'image_width'=>720,
			'image_height'=>300,
			'builder_switch_frontend'=>'0',
			'posts_per_page'=>5
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_contact()
	{
		$content = <<<E
            [otonomic_map  width='100%' height='450px' before='' after='' ]

            [col grid="3-2 first"]
                [contact-form-7 id="2333" title="Contact"]
            [/col]

            [col grid="3-1 last"]
            [otonomic_option option="address" before="<h3>Address</h3>" after=""]
            [otonomic_option option="phone" before="<h3>Phone</h3>" after=""]
            [otonomic_option option="email" before="<h3>Email</h3>" after=""]
            [otonomic_option option="opening_hours" before="<h3>Opening Hours</h3>" after=""]
            [/col]
E;
		$data_post = array(
			'post_title'   => 'Contact Us',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
			'menu_order'   => 80,
		);

		$data_postmeta = array(
			'layout'        => 'list-post',
			'page_layout' => 'default',
			'content_width'  => 'default_width',
			'hide_page_title'=>'default',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_booking()
	{
		$content = <<<E
            [bpscheduler_booking_form]
E;
		$data_post = array(
			'post_title'   => 'Book an Appointment',
			'post_content' => $content,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'comment_status'=>'closed',
			'ping_status'   =>'closed',
			'menu_order'    => 70,
		);
		$data_postmeta = array(
			'layout'        => 'list-post',
			'page_layout' => 'default',
			'content_width'  => 'default_width',
			'hide_page_title'=>'default',
			'order'=>'desc',
			'orderby'=>'content',
			'builder_switch_frontend'=>'0',
		);
		return $page_id = ot_insert_post( $data_post , $data_postmeta);
	}

	private function add_page_store()
	{
		$OtonomicStore = OtonomicStore::get_instance();
		$OtonomicStore->add_sample_product();
		$OtonomicStore->show_store_pages(false);
		$OtonomicStore->hide_store_pages();
		//wp_otonomic_add_woocommerce_pages();
	}
}
