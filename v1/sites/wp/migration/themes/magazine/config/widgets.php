<?php
$sidebars_widgets = array (
	'wp_inactive_widgets' =>
		array (
		),
	'sidebar-main' =>
		array (
			0 => 'text-2',
			1 => 'text-3',
			2 => 'text-5',
		),
	'sidebar-alt' =>
		array (
		),
	'sidebar-main-2a' =>
		array (
		),
	'sidebar-main-2b' =>
		array (
		),
	'sidebar-main-3' =>
		array (
		),
	'social-widget' =>
		array (
			0 => 'themify-social-links-3',
		),
	'header-widget' =>
		array (
		),
	'before-content-widget' =>
		array (
		),
	'after-content-widget' =>
		array (
		),
	'footer-social-widget' =>
		array (
			0 => 'themify-social-links-2',
		),
	'footer-widget-1' =>
		array (
		),
	'footer-widget-2' =>
		array (
			0 => 'text-7',
		),
	'footer-widget-3' =>
		array (
			0 => 'text-4',
		),
	'array_version' => 3,
);
$widget_text = array (
	2 =>
		array (
			'title' => 'Facebook',
			'text' => '[otonomic_fb_like width="310" height="320" /]',
			'filter' => false,
		),
	3 =>
		array (
			'title' => 'Latest Tweets',
			'text' => '[otonomic_twitter username="twitter" /]',
			'filter' => false,
		),
	4 =>
		array (
			'title' => 'ABOUT',
			'text' => $about_content,
			'filter' => false,
		),
	5 =>
		array (
			'title' => 'Recent Reviews',
			'text' => '[otonomic_recent_reviews count=5 /]',
			'filter' => false,
		),
	7 =>
		array (
			'title' => 'RECENT PHOTOS',
			'text' => '[themify_list_posts limit="8" category="2" image="yes" image_w="50" image_h="50" post_meta="no" display="excerpt" post_date="no" style="grid4" ]',
			'filter' => false,
		),
	'_multiwidget' => 1,
);
$widget_themify_social_links = array (
	2 =>
		array (
			'title' => '',
			'show_link_name' => NULL,
			'open_new_window' => NULL,
			'icon_size' => 'icon-medium',
			'orientation' => 'horizontal',
		),
	3 =>
		array (
			'title' => '',
			'show_link_name' => NULL,
			'open_new_window' => NULL,
			'icon_size' => 'icon-medium',
			'orientation' => 'horizontal',
		),
	'_multiwidget' => 1,
);

return array('sidebars_widgets'=>$sidebars_widgets, 'widget_text'=>$widget_text, 'widget_themify-social-links' => $widget_themify_social_links);