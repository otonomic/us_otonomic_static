<?php
// D-kit Serbia
define('MAIN_DOMAIN', 'verisites.com');

define('WHITE_LABEL_BRAND_NAME', 'Verifone');
define('WHITE_LABEL_SEND_EMAILS', false);

define('WHITE_LABEL_URL_FACEBOOK_FANPAGE', 'https://www.facebook.com/pages/%D7%A2%D7%95%D7%A9%D7%99%D7%9D-%D7%A2%D7%A1%D7%A7%D7%99%D7%9D-%D7%A2%D7%9D-%D7%95%D7%A8%D7%99%D7%A4%D7%95%D7%9F/119274841465688');
define('WHITE_LABEL_URL_TWITTER', '');
define('WHITE_LABEL_URL_LINKEDIN', '');

define('WHITE_LABEL_EMAIL_SUPPORT', 'support@verisites.com');
define('WHITE_LABEL_EMAIL_INFO', 'support@verisites.com');

if(LOCALHOST) {
	define('WHITE_LABEL_FOLDER', 'page2site');
	define('WHITE_LABEL_RESOURCES_FOLDER', 'verisites/');
	
} else {
	define('WHITE_LABEL_FOLDER', 'verisites');
	define('WHITE_LABEL_RESOURCES_FOLDER', 'verisites/');
}

define('URL_BUILDER', 'http://builder.verisites.com/');
define('URL_MARKETING_SITE', 'http://www.verisites.com/');
define('URL_DOMAIN_RESELLER', 'http://supersite.page2site.com');

if(!LOCALHOST) {
	define('DB_HOSTNAME','localhost');
	define('DB_USERNAME','omri');
	define('DB_PASSWORD','vxhxnt12akgnrh');
	if(TEST) {
		define('DB_DATABASE','verisites');
	} else {
		define('DB_DATABASE','verisites');
	}

	define('FACEBOOK_APP_ID','601835769844429');
	define('FB_API_SECRET',"17ba1095df8629138a97884bb5517285");
	
	if((SUBDOMAIN=='www' || SUBDOMAIN=='')
			&& ONLYDOMAIN==MAIN_DOMAIN) {
		define('PAGE2SITE_FOLDER','/code/');
		define('CAKEPHP_ABSOLUTE_PATH', DOMAIN.PAGE2SITE_FOLDER);
		define('FULL_PATH', '/var/www/www.domain.com' . PAGE2SITE_FOLDER);
	
	} else {
		define('PAGE2SITE_FOLDER','/');
		define('CAKEPHP_ABSOLUTE_PATH', 'http://'.SUBDOMAIN.'.'.ONLYDOMAIN.PAGE2SITE_FOLDER);
		define('FULL_PATH', '/var/www/any.domain.com' . PAGE2SITE_FOLDER);
	}
}

define('DOMAIN','http://www.'.MAIN_DOMAIN);	
?>