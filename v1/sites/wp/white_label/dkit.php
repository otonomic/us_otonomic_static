<?php
// D-kit Serbia
define('MAIN_DOMAIN', 'zink-it.com');

define('WHITE_LABEL_BRAND_NAME', 'D-kit');
define('WHITE_LABEL_SEND_EMAILS', false);

define('WHITE_LABEL_URL_FACEBOOK_FANPAGE', 'http://www.facebook.com/Dkit.rs');
define('WHITE_LABEL_URL_TWITTER', '');
define('WHITE_LABEL_URL_LINKEDIN', '');

define('WHITE_LABEL_EMAIL_SUPPORT', 'support@dkit.rs');
define('WHITE_LABEL_EMAIL_INFO', 'support@dkit.rs');

if(LOCALHOST) {
	define('WHITE_LABEL_FOLDER', 'page2site');
	define('WHITE_LABEL_RESOURCES_FOLDER', 'dkit/');
	
} else {
	define('WHITE_LABEL_FOLDER', 'dkit');
	define('WHITE_LABEL_RESOURCES_FOLDER', 'dkit/');
}

define('URL_BUILDER', 'http://builder.dkit.rs/');
define('URL_MARKETING_SITE', 'http://www.dkit.rs/');
define('URL_DOMAIN_RESELLER', 'http://supersite.page2site.com');

if(!LOCALHOST) {
	define('DB_HOSTNAME','localhost');
	define('DB_USERNAME','omri');
	define('DB_PASSWORD','vxhxnt12akgnrh');
	if(TEST) {
		define('DB_DATABASE','zinkit_test');
	} else {
		define('DB_DATABASE','zinkit_test');
	}

	define('FACEBOOK_APP_ID','601835769844429');
	define('FB_API_SECRET',"17ba1095df8629138a97884bb5517285");
	
	if((SUBDOMAIN=='www' || SUBDOMAIN=='')
			&& ONLYDOMAIN==MAIN_DOMAIN) {
		define('PAGE2SITE_FOLDER','/code/');
		define('CAKEPHP_ABSOLUTE_PATH', DOMAIN.PAGE2SITE_FOLDER);
		define('FULL_PATH', '/var/www/www.domain.com' . PAGE2SITE_FOLDER);
	
	} else {
		define('PAGE2SITE_FOLDER','/');
		define('CAKEPHP_ABSOLUTE_PATH', 'http://'.SUBDOMAIN.'.'.ONLYDOMAIN.PAGE2SITE_FOLDER);
		define('FULL_PATH', '/var/www/any.domain.com' . PAGE2SITE_FOLDER);
	}
}

define('DOMAIN','http://www.'.MAIN_DOMAIN);	
?>