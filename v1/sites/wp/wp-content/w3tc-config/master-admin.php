<?php

return array(
	'version' => '0.9.4',
	'browsercache.configuration_sealed' => false,
	'cdn.configuration_sealed' => false,
	'common.install' => 1406411490,
	'common.visible_by_master_only' => true,
	'dbcache.configuration_sealed' => false,
	'minify.configuration_sealed' => false,
	'objectcache.configuration_sealed' => false,
	'pgcache.configuration_sealed' => false,
	'previewmode.enabled' => false,
	'varnish.configuration_sealed' => false,
	'fragmentcache.configuration_sealed' => false,
	'newrelic.configuration_sealed' => false,
	'extensions.configuration_sealed' => array(
	),
	'notes.minify_error' => true,
	'minify.error.last' => 'JSMin: Unterminated String at byte 66879: "===a.STRING(4,5).toUpperCase()?r():!1):!1',
	'minify.error.notification' => 'admin',
	'minify.error.notification.last' => 0,
	'minify.error.file' => 'dY9LEoMwDEMvRON-WPU2BkJwxsFpPlO4fVOgrNKdRk_WyC3kRByVo7lpwXP2LDicQo2Z-YA2ynzf9AMygX1lHVaVSfUSdM1_02B0qhEnOVZPooSEHVfZENCY_1C8P-HbX3AoomyGfcX-4RU8BmTGBdKkHY3rN3G0lAD1whJiM6XknwBocVFGxLBGT7E86jYPmLpSrLtR5gS3n1I2fgA.js',
	'track.maxcdn_signup' => 0,
	'track.maxcdn_authorize' => 0,
	'track.maxcdn_validation' => 0,
	'notes.maxcdn_whitelist_ip' => true,
	'notes.remove_w3tc' => false,
	'notes.hide_extensions' => array(
	),
	'evaluation.reminder' => 0,
);