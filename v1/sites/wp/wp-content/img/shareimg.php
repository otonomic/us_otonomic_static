<?php
include_once('otonomic_facebook.class.php');
$src = $_GET['src'];
if(empty($src))
{
	die('Invalid src');
}
$src = base64_decode($src);
$OtonomicFacebook = new OtonomicFacebook();
$real_src = $OtonomicFacebook->getRealUrl($src);
//$image_data = $OtonomicFacebook->read($real_src);
$filename = basename($real_src);
$ext = strtolower(substr(strrchr($filename,"."),1));

if(strpos($ext,'?')!==false)
	$ext = substr($ext,0,strpos($ext,'?'));

switch( $ext ) {
	case "gif": $ctype="image/gif"; break;
	case "png": $ctype="image/png"; break;
	case "jpeg":
	case "jpg": $ctype="image/jpg"; break;
	default:
}
header('Content-type: ' . $ctype);
$image_data = file_get_contents($real_src);
echo $image_data;