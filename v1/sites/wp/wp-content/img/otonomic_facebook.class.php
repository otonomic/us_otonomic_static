<?php
class OtonomicFacebook {

	private $useragent = 'Otonomic Facebook PHP5 (cURL)';
	private $curl = null;
	private $response_meta_info = array();
	private $header = array(
		"Accept-Encoding: gzip,deflate",
		"Accept-Charset: utf-8;q=0.7,*;q=0.7",
		"Connection: close"
	);

	public function __construct() {
		$this->curl = curl_init();
		register_shutdown_function(array($this, 'shutdown'));
	}

	public function getRealUrl($photoLink) {
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->header);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_HEADER, false);
		curl_setopt($this->curl, CURLOPT_USERAGENT, $this->useragent);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, 15);
		curl_setopt($this->curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($this->curl, CURLOPT_URL, $photoLink);

		curl_setopt($this->curl, CURLOPT_HEADERFUNCTION, array(&$this, 'readHeader'));
		$response = curl_exec($this->curl);
		if (!curl_errno($this->curl)) {
			$info = curl_getinfo($this->curl);
			//var_dump($info);
			if ($info["http_code"] == 302) {
				$headers = $this->getHeaders();
				if (isset($headers['fileUrl'])) {
					return $headers['fileUrl'];
				}
			}
		}
		return $photoLink;
	}

	private function readHeader($ch, $header) {

		//Extracting example data: filename from header field Content-Disposition
		$filename = $this->extractCustomHeader('Location: ', '\n', $header);
		if ($filename) {
			$this->response_meta_info['fileUrl'] = trim($filename);
		}
		return strlen($header);
	}

	private function extractCustomHeader($start, $end, $header) {
		$pattern = '/'. $start .'(.*?)'. $end .'/';
		if (preg_match($pattern, $header, $result)) {
			return $result[1];
		}
		else {
			return false;
		}
	}

	public function getHeaders() {
		return $this->response_meta_info;
	}

	/**
	 * Cleanup resources
	 */
	public function shutdown() {
		if($this->curl) {
			curl_close($this->curl);
		}
	}
}