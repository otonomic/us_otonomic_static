<?php
define ('ALLOW_EXTERNAL', TRUE);
define ('ALLOW_ALL_EXTERNAL_SITES', TRUE);
define ('BROWSER_CACHE_DISABLE', TRUE);

define ('MEMORY_LIMIT', '1024M');
define ('DEBUG_ON', false);
define ('DEBUG_LEVEL', 3);

$ALLOWED_SITES = array (
	'flickr.com',
	'staticflickr.com',
	'picasa.com',
	'img.youtube.com',
	'upload.wikimedia.org',
	'photobucket.com',
	'imgur.com',
	'imageshack.us',
	'tinypic.com',
	'fbcdn.net',
	'akamaihd.net',
	'youtube.com',
	'facebook.com',
	'wp.test',
	'verisites.com',
	'otonomic.com'
);