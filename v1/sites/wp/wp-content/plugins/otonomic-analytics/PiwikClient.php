<?php

    require_once $_SERVER['DOCUMENT_ROOT'] .'/piwik/libs/PiwikTracker/PiwikTracker.php';


    class PiwikClient {

        public static function event($category, $action, $label, $value = false){

            if(!defined('PIWIK_URL')) {
                trigger_error('please set PIWIK_URL in the wp_config.php', E_USER_NOTICE);
                return;
            }

            if(!defined('PIWIK_SITE_ID')) {
                trigger_error('please set PIWIK_SITE_ID in the wp_config.php', E_USER_NOTICE);
                return;
            }

            $piwik = new PiwikTracker( PIWIK_SITE_ID, PIWIK_URL);

            if( $current_user = get_current_user_id()) {
                $piwik->setUserId($current_user);
            }

            return $result = $piwik->doTrackEvent($category, $action, $label, $value);

        }
    }
?>