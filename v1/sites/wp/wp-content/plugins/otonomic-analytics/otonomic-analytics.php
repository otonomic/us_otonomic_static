<?php

/*
Plugin Name: Otonomic analytics
Plugin URI: http://otonomic.com
Description: Otonomic WordPress Admin Theme - Upload and Activate.
Author: otonomic.com
Version: 1.0
Author URI: http://otonomic.com
*/

if(is_admin()){
    // add_action('in_admin_footer', 'otonomic_analytics_footer');
    add_action('admin_print_scripts', 'otonomic_analytics_footer');
    add_action('wp_otonomic_cmd_hook','otonomic_analytics_parse_request');
    add_action('switch_theme', 'otonomic_analytics_switch_theme');
    add_action( 'admin_print_scripts', 'otonomic_analytics_scripts' );
}

add_action( 'wp_enqueue_scripts', 'otonomic_analytics_scripts' );
add_action('wp_footer', 'otonomic_analytics_frontsidefooter');
add_action('wp_head', 'otonomic_analytics_frontsideheader');


function otonomic_analytics_scripts() {
    wp_register_script( 'otonomic_analytics', plugins_url('/js/main.js', __FILE__ ), array( 'jquery' ),'1.2',true);
    wp_register_script( 'otonomic_analytics_events', plugins_url('/js/otonomic_platform_analytics_events.js', __FILE__ ), array( 'jquery', 'otonomic_analytics' ),'1.3',true);

    wp_localize_script( 'otonomic_analytics', 'settings', [
        'localhost' => LOCALHOST,
        'p2s_local' => P2S_LOCAL,
        'user_id' => get_current_user_id(),
        'site_id' => get_current_blog_id(),
    ] );

    wp_enqueue_script( 'otonomic_analytics');
    wp_enqueue_script( 'otonomic_analytics_events');
}


if(!empty($_GET['site_created'])) {
    otonomic_track_site_created();
}


function otonomic_track_site_created() {
    otonomic_analytics_track_event(
        $category   = 'Account Manage',
        $action     = 'Create Success'
//        $label      = 'Fan'
    );

    otonomic_analytics_track_goal(
    );
}

function otonomic_analytics_track_goal() {
    // TODO: Write function
}

function otonomic_analytics_track_event($category = "", $action = "", $label = "", $value = false) {
    if( TRACK_PIWIK) {
        include_once plugin_dir_path( __FILE__ ) .'PiwikClient.php';
        PiwikClient::event( $category, $action, $label, $value);
    }

    include_once plugin_dir_path( __FILE__ ) .'GAClient.php';
    GAClient::event(    $category, $action, $label, $value);

    if( TRACK_OTONOMIC) {
        include_once plugin_dir_path( __FILE__ ) .'OTClient.php';
        OTClient::event(    $category, $action, $label, $value);
    }
}

function otonomic_analytics_frontsideheader(){
    include_once plugin_dir_path( __FILE__ ) .'tracking_codes/fb.php';
}


function otonomic_analytics_frontsidefooter(){
    otonomic_analytics_footer();
}

function otonomic_analytics_footer () {
    if( TRACK_LUCKYORANGE) {
        include_once plugin_dir_path( __FILE__ ) .'tracking_codes/lucky_orange.php';
    }

    include_once plugin_dir_path( __FILE__ ) .'tracking_codes/ga.php';

    if( TRACK_PIWIK) { include_once plugin_dir_path( __FILE__ ) .'tracking_codes/piwik.php'; }

    // include_once plugin_dir_path( __FILE__ ) .'tracking_codes/events.php';

    /*
    $analytics_services = get_otonomic_constants('analytics_services');
    foreach( $analytics_services as $service) {
        include_once './tracking_codes/'.$service.'.php';
    }
    */
}




/***********************************************************
 *  Handle options
 */

//function otonomic_analytics_save_post($post_id){
//    otonomic_analytics_track_event('wp-admin',$_REQUEST['page'] , json_encode($_REQUEST));
//}

function otonomic_analytics_parse_request(){

    $wp_otonomic_cmd = $_REQUEST['wp_otonomic_cmd'];

    $original_data = [];

    if(isset($_REQUEST['original_data'])){
        $original_data = json_decode(urldecode($_REQUEST['original_data']), true);

        foreach($original_data as $key => $original_value) {
            if(isset($_POST[$key]) && $_POST[$key]!=$original_value) {
                if(is_array($_POST[$key])) { $_POST[$key] = json_encode($_POST[$key]); }
                if(is_array($original_value)) { $original_value = json_encode($original_value); }
                otonomic_analytics_track_event('Edit', $key , $original_value .' -> '. $_POST[$key]);
            }
        }
    }
    return;
}

function otonomic_analytics_switch_theme ($new_theme){
    otonomic_analytics_track_event('wp-admin','switch_theme',$new_theme);
}
