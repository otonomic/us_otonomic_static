<?php
$Otonomic_First_Session = Otonomic_First_Session::get_instance();
?>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?= ANALYTICS_ID; ?>', 'auto');

	ga('set', 'dimension1', '<?= $Otonomic_First_Session->get_user_type();?>'); // User type (visitor/owner)
	ga('set', 'dimension2', '<?= $Otonomic_First_Session->get_plan_type();?>'); // Plan type (free/monthly/yearly)
	ga('set', 'dimension3', '<?= $Otonomic_First_Session->get_site_category();?>'); // FB category
    ga('set', 'dimension4', '<?= get_site_url();?>'); // Site url

	ga('set', 'metric1', '<?= $Otonomic_First_Session->get_days_since_site_creation();?>'); // Days since site creation
	ga('set', 'metric2', '<?= ($Otonomic_First_Session->get_number_of_owner_visits()+1);?>'); // Number of visits

    <?php do_action('otonomic_google_analytics_init'); ?>

	ga('send', 'pageview');
</script>
