<script>
//    track_menu();
//    track_pageview();


    function ototrack_event(category, action, label){

        if(typeof label === "undefined") {
            label = '';
        }

        if(action == '-' || label == '-')
            return;


        ga('send', 'event',category,action, label);
        save_to_otonomic(category,action, label);
        _paq.push(['trackEvent', category, action, label ]);
    }

/*
    jQuery('#btn-skip').click(function(){
        ototrack_event('Editor','Skip', get_current_page_title());
    });

    jQuery('#btn-close').click(function(){
        ototrack_event('Editor','Close', get_current_page_title());
    });

    jQuery('#btn-save').click(function(){
        ototrack_event('Editor','Save', get_current_page_title());
    });

    jQuery('#btn-back').click(function(){
        ototrack_event('Editor','Back', get_current_page_title());
    });

    jQuery('#saveAndAnother').click(function(){
        ototrack_event('Editor','Save and Another', get_current_page_title());
    });
*/

//    jQuery( "a[title='Preview']" ).on( "click", function() {
//        alert( $( this ).text() );
//    });




//    jQuery('#wp-admin-bar-otonomic-adminbar-pricing').click(function(){
//        ototrack_event('Editor','view', get_current_page_title());
//    });
//    jQuery("#wp-admin-bar-otonomic-adminbar-link > a").click(function(){
//        alert(33);
//        ototrack_event('User Website',' Edit Click', 'topbar');
//    });

/*
    jQuery( document ).ready(function() {

        jQuery('.otonomic_edit_link > a').click(function(event){
            var selected_section = jQuery(this).closest('section')[0].id;
            ototrack_event('User Website',' Edit Click', selected_section);
        });

        jQuery('#wp-admin-bar-pro-site > a').click(function(){
            ototrack_event('User Website',' Upgrade Click', 'Top Bar');
        });


        jQuery('#wp-admin-bar-otonomic-adminbar-link > a').click(function(){
            ototrack_event('User Website',' Edit Click', 'Top Bar');
        });


        jQuery("#wp-admin-bar-otonomic-adminbar-backend-preview > a").click(function(){
            ototrack_event('Editor','Preview');
        });

    });
*/

    function track_pageview(){
        ototrack_event('Editor','View', get_current_page_title());
    }

    function save_to_otonomic(category, event, label){
        jQuery.get('http://builder.otonomic.com/sites/track_click/', { category: category, event: event , label: label, site_id: '<?= get_current_blog_id(); ?>' });
    }


    function track_menu(){
        var ids_to_track_click = [
            'toplevel_page_fs-top-level-business-profile',
            'toplevel_page_fs-top-level-contact-details',
            'toplevel_page_fs-top-level-customer-reviews',
            'toplevel_page_fs-top-level-social-media',
            'toplevel_page_fs-top-level-gallery',
            'toplevel_page_fs-top-level-store',
            'toplevel_page_fs-top-level-domain',
            'toplevel_page_fs-top-level-manage-pages',
            'toplevel_page_themes',
            'toplevel_page_opening-hours'
        ];


        for(mykey in ids_to_track_click){

            jQuery( "#"+ ids_to_track_click[mykey]).on('click',function() {
                ototrack_event('Editor','Edit', get_current_page_title());
            });
        }

    }
</script>