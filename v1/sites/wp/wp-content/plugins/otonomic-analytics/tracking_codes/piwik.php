<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
    _paq.push(["setCookieDomain", "*.<?= MAIN_DOMAIN?>"]);
    _paq.push(["setDomains", ["*.<?= MAIN_DOMAIN?>"]]);

    <?php $current_user = wp_get_current_user(); ?>
    <?php if (!empty($current_user->user_login) ) : ?>
        _paq.push(['setUserId', "<?= $current_user->ID ?>"]);
        _paq.push(['setCustomVariable', 1, "User", "<?php echo $current_user->user_login ?>","visit"]);
    <?php endif; ?>

    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u=(("https:" == document.location.protocol) ? "https" : "http") + "://<?php echo PIWIK_URL; ?>";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', <?php echo PIWIK_SITE_ID; ?>]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
        g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="http://<?php echo PIWIK_URL; ?>piwik.php?idsite=<?php echo PIWIK_SITE_ID; ?>" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
