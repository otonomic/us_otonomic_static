<script> window.intercomSettings = {
    // TODO: The current logged in user's full name
    name: "<?= OtonomicUser::get('display_name'); ?>",
    // TODO: The current logged in user's email address.
    email: "<?= OtonomicUser::get('user_email'); ?>",
    // TODO: The current logged in user's sign-up date as a Unix timestamp.
    created_at: <?= strtotime(OtonomicUser::get('user_registered')); ?>,
    app_id: "y23y9s35"
}; </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/y23y9s35';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
