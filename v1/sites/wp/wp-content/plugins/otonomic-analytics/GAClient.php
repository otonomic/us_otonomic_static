<?php
/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 7/8/14
 * Time: 10:10 AM
 */

require_once plugin_dir_path( __FILE__ ) . 'OACurl.php';
require_once plugin_dir_path( __FILE__ ) . 'UUID.php';

class GAClient {

    const url = "www.google-analytics.com/collect";
    const v = 1;

    const TYPE_EVENT = 'event';
	const TYPE_PAGEVIEW = 'pagegiew';

    public static function event($category, $action, $label, $value = false, $type = self::TYPE_EVENT){

        if(!defined('ANALYTICS_ID')) {
            trigger_error('please set ANALYTICS_ID in the wp_config.php', E_USER_NOTICE);
            return;
        }

        $uuid = UUID::generate();

        $data_to_send = array(
            'v' => self::v,
            'tid' => ANALYTICS_ID,
            'cid' => $uuid,
            't' => $type,
            'ec' => $category,
            'ea' => $action,
            'el' => $label,
        );

        if($value)
           $data_to_send['ev'] = $value;


        $curl = new OACurl();
        $curl->setOpt(CURLOPT_RETURNTRANSFER , true);
        $curl->setOpt(CURLOPT_TIMEOUT , 0.1);
        $curl->setOpt(CURLOPT_HEADER , false);
        $curl->post(self::url , $data_to_send);
    }
	public static function pageview($page){
		if(!defined('ANALYTICS_ID')) {
			trigger_error('please set ANALYTICS_ID in the wp_config.php', E_USER_NOTICE);
			return;
		}
		$uuid = UUID::generate();
		$data_to_send = array(
			'v' => self::v,
			'tid' => ANALYTICS_ID,
			'cid' => $uuid,
			't' => 'pageview',
			'dp' => $page
		);

		$curl = new OACurl();
		$curl->setOpt(CURLOPT_RETURNTRANSFER , true);
		$curl->setOpt(CURLOPT_TIMEOUT , 0.1);
		$curl->setOpt(CURLOPT_HEADER , false);
		$curl->post(self::url , $data_to_send);
	}

}