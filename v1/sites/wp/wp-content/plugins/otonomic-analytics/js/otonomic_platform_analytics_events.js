// On DOM ready
var otonomic_analytic_fb_label='';

; jQuery(document).ready(function ($) {

    function set_button_for_media_gallery_options(el, opts) {

        // TODO: find out why this gets called 2 extra (!!) times with no arguments
        // this is just an ugly hack around an unsolved bug:
        if (!opts) return;

        opts.category = document.title;
        opts.action = 'Media Library';
        opts.label = el.attr('id');
    }

    function set_admin_menu_options(el, opts) {
        // get path of (li > a) patterns (nested menu items) and set it as label
        var path = [el.text()];
        el.parents('li').each(function (i) {
            if (!i) return;
            var child_as = $(this).children('a');
            if (child_as.length) {
                path.unshift(child_as.text());
            }
        });
        var label = path.join(' / ');
        if (label.length) opts.label = label;

        // "guess" category from the top parent menu id or class, if not already set
        if (!opts.category) {
            var top_parent_menu = el.closest('ul:not(.wp-submenu)');
            var category = top_parent_menu.attr('id') || top_parent_menu.attr('class');
            if (category) opts.category = category;
        }
    }

    function share_otonomic_card(el, opts) {
        var num_shares = Cookies.get('num_shares_performed');
        Cookies.set('num_shares_performed', num_shares+1);


    }

    function set_help_screen_options(el, opts) {
        opts.category = "Top Bar";
        opts.action = "Help";
        opts.label = $('title').text();
        if( $('a', el).hasClass('screen-meta-active')) {
            opts.action = "Help Close";
        } else {
            opts.action = "Help Open";
        }

    }

    function set_share_popup_button_options(el, opts) {
        opts.category = "Share Site";
        opts.action = el.attr('data-platform');
    }

	function set_delete_site_popup_button_options(el, opts) {
		opts.category = "Delete Popup";
		opts.action = el.attr('data-action');
	}

	function set_welcome_popup_button_options(el, opts) {
		opts.category = "Welcome Popup";
		opts.action = el.attr('data-action');
	}

    function set_domain_page_search_options(el, opts) {
        opts.category = "Domain Page";
        opts.action = "Search";
        opts.label = $('#domain-search-box').val();
    }



    /**
     * Event Tracking handlers
     */

    /// - Phase 1: track specific events:

    // Edit section link (front-end)
    // TODO: track *which* section
    /*
    $('.otonomic_edit_link').otonomicTrackEvent({
        category: "Template Engagement",
        action: "Edit Hook",
		label: {attr: "data-analytic-label"}
    });

    // Locked elements (front-end)
    $("#updates .otonomic-locked").otonomicTrackEvent({
        category: "Updates",
        action: "Locked"
    });
    $("#testimonials .otonomic-locked").otonomicTrackEvent({
        category: "Recommendations",
        action: "Locked"
    });
    $("#photos .otonomic-locked").otonomicTrackEvent({
        category: "Photo Albums",
        action: "Locked"
    });
    */

    // Upgrade hooks/messages

	// upgrade button click
    /*
    $(".otonomic-upgrade-message").otonomicTrackEvent({
        category: "Upgrade Engagement",
        action: "Button Click",
		label: "{current_page_title}"
    });
    */

	// upgrade banner click backend


    /*
	$('#wpwrap').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook',
		category: "Upgrade Engagement",
		action: "Upgrade Hook",
		options_callback: function (el, options, event) {
			// don't track the upgrade button click again
			//if ($(event.target).closest('.button-holder').length) return false;
			options.label = el.parent().parent().attr('data-analytic-category');
		}
	});
	$('#wpwrap').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook .close',
		category: "Upgrade Engagement",
		action: "Upgrade Hook Close",
		options_callback: function (el, options, event) {
			// don't track the upgrade button click again
			//if ($(event.target).closest('.button-holder').length) return false;
			options.label = el.parent().parent().parent().parent().parent().attr('data-analytic-category');
		}
	});
	// Front
	$('#photos').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook',
		category: "Upgrade Engagement",
		action: "Upgrade Hook",
		label: "Photos Template"
	});

	$('#photos').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook .close',
		category: "Upgrade Engagement",
		action: "Upgrade Hook Close",
		label: "Photos Template"
	});

	$('#updates').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook',
		category: "Upgrade Engagement",
		action: "Upgrade Hook",
		label: "Updates Template"
	});
	$('#updates').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook .close',
		category: "Upgrade Engagement",
		action: "Upgrade Hook Close",
		label: "Updates Template"
	});
	$('#updates').otonomicTrackEvent({
		selector: '.otonomic-lock',
		category: "Upgrade Engagement",
		action: "Lock",
		label: "Updates Template"
	});

	// testimonial
	$('#testimonials').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook',
		category: "Upgrade Engagement",
		action: "Upgrade Hook",
		label: "Testimonials Template"
	});
	$('#testimonials').otonomicTrackEvent({
		selector: '.otonomic-upgrade-hook .close',
		category: "Upgrade Engagement",
		action: "Upgrade Hook Close",
		label: "Testimonials Template"
	});
	$('#testimonials').otonomicTrackEvent({
		selector: '.otonomic-lock',
		category: "Upgrade Engagement",
		action: "Lock",
		label: "Testimonials Template"
	});
*/




    // Main Admin Menu
    $('ul#adminmenu li > a').otonomicTrackEvent({
        category: "Admin side menu",
        options_callback: set_admin_menu_options
    });



    // Preview button (back-end)
    $("#wpadminbar a").otonomicTrackEvent({
        category: "Service Toolbar",
        action: "{text}"
    });
	$("#wp-admin-bar-otonomic-adminbar-backend-edit a").otonomicTrackEvent({
		category: "Service Toolbar",
		action: "Edit",
		label: "{text}"
	});

    // Media gallery click
    $('a.open-mediagal-popup').otonomicTrackEvent({
        options_callback: set_button_for_media_gallery_options
    });

    // AddThis Social Share click
    $('.at-share-btn span').otonomicTrackEvent({
        category: "AddThis Share",
        options_callback: function(el, opts) {
            opts.action = el.attr('title');
        }
    });

    // Pricing Page Plan Click (to get to checkout page)
    $('a.pay_link').otonomicTrackEvent({});

    // Pricing Page Plan Length (yearly/monthly)
    $('#pricing_page .btn-payment-plan').otonomicTrackEvent({
        category: "Pricing",
        action: "Change Plan"
    });

    // Pricing Page Like/Share
    $('#likes-div').otonomicTrackEvent({
        selector: '.like-div',
        category: "Pricing",
        action: "Like/Share",
        label: "{title}"
    });


    // Admin Help Tab open/close
    $('#wpbody').otonomicTrackEvent({
		selector: '#contextual-help-link',
        options_callback: set_help_screen_options
    });

    // Delete site popup
    $('#deletesite-popup-action-links a').otonomicTrackEvent({
        options_callback: set_delete_site_popup_button_options
    });
	// Close event tracking
	$('body').otonomicTrackEvent({
		selector: '.deletesite-popup #cboxClose',
		category: "Delete Popup",
		action: "Close"
	});

    // Welcome site popup
    $('#welcome-popup-action-links a').otonomicTrackEvent({
        options_callback: set_welcome_popup_button_options
    });

	$('body').otonomicTrackEvent({
		selector: '.share-popup #cboxClose',
		category: "Share",
		action: "Close"
	});
    // Close Colorbox popup
    $('body').otonomicTrackEvent({
		selector: '#cboxClose',
        category: "Popup",
        action: "Close"
    });

    // Search domain name
    $('#domain-search-btn').otonomicTrackEvent({
        options_callback: set_domain_page_search_options
    });



    // First Session Admin Screens Navigation Buttons
    $('#cboxLoadedContent').otonomicTrackEvent({
        selector: "a.add-app",
        category: "App Market",
        action: "Add",
        label: "{text}"
    });

	// First Session Admin Screens Navigation Buttons
	$('#wpbody').otonomicTrackEvent({
		selector: "#wpfooter-first-session a, #wpfooter-first-session button",
		options_callback: function(el, opts) {
			opts.category = el
                .parents('#wpbody-content')
                .find('.ot-admin-page-container .wrap.first-session')
                .attr('data-analytic-category');
		},
		action: "{text}"
		// label: "{text}"
	});

    $('#wpbody').otonomicTrackEvent({
        selector: '.ot-track-event',
        category: {attr: "data-analytics-category"},
        action: {attr: "data-analytics-action"},
        label: {attr: "data-analytics-label"},
        value: {attr: "data-analytics-value"},
    });

    /* Business Profile */
	$('#wpbody').otonomicTrackEvent({
		selector: '.business-identity .open-mediagal-popup',
		category: "Business Profile Editor",
		label: "",
		action: {attr: "data-analytics-action"}
	});

	/* Store Editor */
	$('#wpbody .online-store').otonomicTrackEvent({
		selector: '.product-remove-btn, .store-confirm-action a',
		category: "Store Editor",
		label: "",
		action: {attr: "data-analytics-action"}

	});


    $('#oto_whattodonext a').otonomicTrackEvent({
        category: "What to do next",
        action: {attr: "data-analytics-action"},
        label: ""
    });



	/*$('#wpbody .online-store').otonomicTrackEvent({
		selector: '',
		category: "Store Editor",
		label: "",
		action: {attr: "data-analytics-action"}

	});*/

	/* Domain Editor */
	$('#wpbody').otonomicTrackEvent({
		selector: '.domain-search-btn',
		category: "Domain Editor",
		label: "",
		action: 'Search'

	});
	/* Domain Upgrade */
	$('#wpbody').otonomicTrackEvent({
		selector: '#btn-save.btn-oto-orange',
		category: "Domain Editor",
		options_callback: function(el, opts) {
			var category = el.parents('#wpbody-content').find('.ot-admin-page-container .wrap.first-session').attr('data-analytic-category');
			//var category = el.parent('p').parent('#wpfooter-first-session').parent('#wpbody-content').find('.ot-admin-page-container .wrap.first-session').attr('data-analytic-category');
			if(category == 'Domain Editor')
			{
				search_box = $("#domain-search-box").val();
				if(search_box.length>0)
				{
					/* May be suggestion */
					if($('.error-box').hasClass('ng-hide'))
					{
						opts.action = 'Search';
					}
					else
					{
						opts.action = 'Suggestion';
					}
				}
				else
				{
					opts.action = 'Default';
				}
			}
			else
			{
				return false;
			}
		}

	});

	/* Social Media Editor */
	$('#wpbody .social-media').otonomicTrackEvent({
		selector: 'ul.nav li a',
		category: "Social Media Editor",
		label: {attr:"data-service"},
		action: 'Channel Click'
	});

	/* Recommendations Editor */
	$('#wpbody .customer-reviews').otonomicTrackEvent({
		selector: '.review',
		category: "Recommendations Editor",
		label: "",
		options_callback: function(el, opts) {
			opts.action = el.hasClass('status-hidden')?'Hide':'Show';
		}

	});


	/* Welcome popup */
	/*$('#welcome-overlay').otonomicTrackEvent({
		iframe: true,
		selector: ".btn, .not-now-link, .otonomic-track",
		category: "Welcome Popup",
		action: {attr: "data-action"},
		label: ""

	});*/
	/* Front end image click */
	$('#photos img').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Image Click",
		label: ''
	});
	/* Bottom banner */
	$('#bottom_bar').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Bottom Banner",
		label: ''
	});
	/* Blog posts */
	$('#updates .post').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Blog Post Click",
		label: ''
	});
	/* Read more for blog */
	$('#updates .shortcode.button').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Read More Blog",
		label: ''
	});
	/* Submit booking */
	$('#birs_book_appointment').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Submit Booking",
		label: ''
	});
	/* Send contact */
	$('.wpcf7-submit').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Send Contact",
		label: ''
	});
	$('.back-top').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Back to top",
		label: ''
	});
	$('body').otonomicTrackEvent({
		selector: '#uvTab',
		category: "Template Engagement",
		action: "Feedback and Support",
		label: ''
	});
	$('a.request-appointment').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Make Appointment"
	});
	$('.share-channels .rrssb-buttons a').otonomicTrackEvent({
		category: "Template Engagement",
		//action: "Heart Facebook"
		options_callback: function(el, opts) {
			opts.action = "Heart "+$.trim(el.text());
		}
	});
	$('li.menu-item a').otonomicTrackEvent({
		category: "Template Engagement",
		action: "Menu Click",
		label: '{text}'
	});
	$('body').otonomicTrackEvent({
		selector: '.appmarket-popup #cboxClose',
		category: "App Store",
		action: "Close"
	});

	/* Facebook connect events */
	$('body').otonomicTrackEvent({
		selector: '.facebook_connect',
		category: "Facebook Connect",
		action: "Start",
		options_callback: function(el, opts) {
			var label = el.attr('data-analytics-label');
			if(label == undefined) {
				label = otonomic_analytic_fb_label;
			}
			else
			{
				otonomic_analytic_fb_label = label;
			}
			opts.label = label;
		}
	});
	/* Dashboard events */
	$('body').otonomicTrackEvent({
		selector: '#welcome-dashboard .analytic-event',
		category: "Dashboard",
		options_callback: function(el, opts) {
			opts.label = el.attr('data-analytics-label');
			opts.action = el.attr('data-analytics-action');
		}
	});
	$('body').otonomicTrackEvent({
		selector: '#welcome-dashboard .carousel-control',
		category: "Dashboard",
		options_callback: function(el, opts) {
			opts.action = el.attr('data-analytics-action');
			var parent_div = el.parent('div');
			var current_item = jQuery('.carousel-inner .item.active h4', parent_div).text();
			opts.label = current_item;
		}
	});
	$('body').otonomicTrackEvent({
		selector: '#wp-admin-bar-otonomic-adminbar-dashboard a',
		category: "Dashboard",
		action: "View",
		label: "Source Toolbar"
	});

    // Newspaper theme social share icons - inside a single post
    $('.td-social-sharing-buttons.td-social-facebook').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Facebook',
        label: '{current_page_title}'
    });
    $('.td-social-sharing-buttons.td-social-twitter').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Twitter',
        label: '{current_page_title}'
    });
    $('.td-social-sharing-buttons.td-social-google').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Google',
        label: '{current_page_title}'
    });
    $('.td-social-sharing-buttons.td-social-pinterest').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Pinterest',
        label: '{current_page_title}'
    });

    // Newspaper theme social share icons - inside the Cards index page
    $('.meta-social-share .fb-share').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Facebook',
        options_callback: function(el, opts) {
            var parent = el.parents('.card-wrapper');
            opts.label = parent.attr('data-type') + ' >> ' + parent.attr('data-title');
        }
    });

    $('.td-social-sharing-buttons.td-social-twitter').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        action: 'Twitter',
        label: '{current_page_title}'
    });

    // Newspaper theme social share icons - inside the Cards index page
    $('.ot-social-share').otonomicTrackEvent({
        // selector: '.share',
        category: 'Share',
        label: '{title}'
    });



    /*
    // Need to propagate an event of "articles-loaded" on javascript and bind to it so we can attach ourselves to clicks on the whatsapp shares
    $('body').otonomicTrackEvent({
        selector: '.otonomic-share-button',
        category: 'Share',
        options_callback: function(el, opts) {
            var parent = el.parents('.card-wrapper');
            opts.label = parent.attr('data-type') + ' >> ' + parent.attr('data-title');
        }
    });
*/


    /* Generic click events */
    /*$('body').otonomicTrackEvent({
        selector: 'a',
        category: '{current_page_title}',
        action: "Click",
        options_callback: function(el, opts) {
            var label = el.attr('data-analytics-label');
            if(label == undefined) {
                label = el.attr('title');
            }
            if(label == undefined) {
                label = el.attr('id');
            }
            if(label == undefined) {
                label = el.text().trim();
            }
            opts.label = label;
        }
    });*/





/*
    ga('send', 'dimension', 'User type: not logged in'); // Unavailable
    ga('send', 'dimension', 'Gender: '); // Unavailable
    ga('send', 'dimension', 'Age: '); // Unavailable

    ga('send', 'dimension', 'User Country: '); // Already obtained by GA

    ga('send', 'dimension', 'First visit date: ');
    ga('send', 'dimension', 'Last visit date: ');
    ga('send', 'dimension', 'Source: '); // Already obtained by GA

    ga('send', 'dimension', 'Fold');

    ga('send', 'dimension', 'Layout type: '); // Currently not in use

    ga('send', 'dimension', 'Proportion algorithm: '); // Proportion algorithm used for content type randomness - if testing a few different weight systems
*/


    /*
     Time since last visit -> measured in minutes, sent once for every new visit

     When user shares -> Number of shares++
     When user visits the site -> Number of visits by same user++
     When user views an Image inner page -> Number of images viewed++
     When user watches a Video -> Number of videos viewed++
     When a user views an Article inner page -> Number of articles viewed++
     When a user completes a Game -> Number of games played++
     When a user clicks on a related item -> Number of related articles viewed++
     When a user views an Ad -> Number of ad impressions++
     When a user clicks an Ad -> Number of ad clicks++
     Ad Clicks / Ad Impressions -> Ad CTR
     When a user comments on anything within the site -> Number of comments++
     */

/*
    function content_page_viewed(item) {
        var num_actions, cookie_name;
        switch(item.type) {
            case 'article':
                cookie_name = 'num_articles_read';
                break;

            case 'photo':
                cookie_name = 'num_photos_viewed';
                break;

            case 'gallery':
                cookie_name = 'num_galleries_viewed';
                break;

            case 'video':
                cookie_name = 'num_videos_viewed';
                break;

            case 'game':
                cookie_name = 'num_games_played';
                break;

            case 'ad':
                cookie_name = 'num_ads_clicked';
                break;
        }

        ga('send', 'dimension', 'Item type: '+item.type);
        ga('send', 'dimension', 'Content source: '+item.source);
        if(item.type == 'game') {
            ga('send', 'dimension', 'Game type: '+item.game_type);
        }

        num_actions = Cookies.get(cookie_name);
        Cookies.set(cookie_name, num_actions+1);

        num_item_inner_pages_viewed = Cookies.get('num_item_inner_pages_viewed');
        Cookies.set('num_item_inner_pages_viewed', num_item_inner_pages_viewed+1);

        ga('send', 'Item View', type, window.title); // Send event of view
    }

    ga('send', 'metric1', Cookies.get('num_newsfeed_items_viewed'));
    ga('send', 'metric2', Cookies.get('num_item_inner_pages_viewed'));
    ga('send', 'metric3', Cookies.get('num_articles_read'));
    ga('send', 'metric4', Cookies.get('num_photos_viewed'));
    ga('send', 'metric5', Cookies.get('num_galleries_clicked'));
    ga('send', 'metric6', Cookies.get('num_videos_viewed'));
    ga('send', 'metric7', Cookies.get('num_games_played'));
    ga('send', 'metric8', Cookies.get('num_ads_clicked'));
    ga('send', 'metric9', Cookies.get('num_shares_performed'));
    ga('send', 'metric10', Cookies.get('num_comments_written'));
*/
    var first_scroll = true;
    $(window).scroll(function() {
        if(first_scroll) {
            first_scroll = false;
            otonomicTrackEvent('Engagement', 'User scrolled', '{title}', {'nonInteraction': 1});
        }
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            otonomicTrackEvent('Engagement', 'User reached end of story', '{title}', {'nonInteraction': 1});
        }
    });



    $('iframe').contents().ready(function(){
        setTimeout(function(){
			try {
				var iframebody = $('iframe').contents().find('body');

				iframebody.otonomicTrackEvent({
					selector: ".otonomic-share-button",
					category: "Share Site",
					action: "Share",
					label: {attr: "data-platform"}
				});
			}
			catch(e){}
        }, 2500);

    });





// Put events tracked in Pop ups that use iframe here
    $(document).on('cbox_complete', function() {
        $('iframe.cboxIframe').contents().ready(function(){
            setTimeout(function(){
                var iframebody = $('iframe.cboxIframe').contents().find('body');

                iframebody.otonomicTrackEvent({
                    selector: ".otonomic-share-button",
                    category: "Share Site",
                    action: "Share",
                    label: {attr: "data-platform"}
                });

				iframebody.on('click', '.otonomic-share-button', function (e){
					ga('set', 'metric7', '1');
                    otonomicTrackPageView("user_shared");
                });


                // App Market Add App Buttons
                iframebody.otonomicTrackEvent({
                    selector: "a.add-app",
                    category: "App Market",
                    action: "Add",
                    label: "{text}"
                });
				//jQuery('.title',iframebody).click(function (){alert('Here');});
				/* App market popup */
				jQuery('#oto-apps-container a.btn',iframebody).otonomicTrackEvent({
					iframe: false,
					category: "App Store",
					action: "Add App",
					label: {attr: "data-app-name"}
				});
				jQuery('#appmarket-carousel a.btn',iframebody).otonomicTrackEvent({
					iframe: false,
					category: "App Store",
					action: "Hero",
					label: {attr: "data-app-name"}
				});


/*
                iframebody.otonomicTrackEvent({
                    selector: 'a',
                    action: "Click",
                    options_callback: function(el, opts) {
                        var label = el.attr('data-analytics-label');
                        if(label == undefined) {
                            label = el.attr('title');
                        }
                        if(label == undefined) {
                            label = el.attr('id');
                        }
                        if(label == undefined) {
                            label = el.text().trim();
                        }
                        opts.label = label;
                    }
                });
*/


				/*iframebody.otonomicTrackEvent({
					iframe: false,
					selector: "a",
					category: "App Store",
					action: "Add App",
					label: {attr: "data-app-name"}
				});*/

            }, 2500);
        });
    });

//	}, 300);
});
jQuery(window).load(function (e){
    var since_page_load = 0;
    setInterval(function(e){
        since_page_load+=10;
        otonomicTrackEvent('Engagement', 'Time on page', since_page_load, {'nonInteraction': 1});
    }, 10000);
});
//	setTimeout(function () {
