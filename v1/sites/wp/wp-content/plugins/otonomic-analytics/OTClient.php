<?php

function otonomic_analytics_getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

/**
 * Created by PhpStorm.
 * User: romanraslin
 * Date: 7/8/14
 * Time: 10:10 AM
 */

require_once plugin_dir_path( __FILE__ ) . 'OACurl.php';

class OTClient {

    public static function event($category, $action, $label, $value = false){
        if( LOCALHOST) {
            $url = "http://p2s.test/sites/track_click/";
        } else {
            $url = "http://builder.otonomic.com/sites/track_click/";
        }


//        $action = str_replace("-", " ", $action);
//        $action = str_replace("_", " ", $action);
//
//        $label = str_replace("-", " ", $label);
//        $label = str_replace("_", " ", $label);

        $data_to_send = array(
            'category' => $category,
            'event' => $action,
            'label' => $label,
            'referer' => $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI],
            'ip' => otonomic_analytics_getUserIP(),
            'site_id' => get_current_blog_id(),
            'user_id' => get_current_user_id()
        );

        if($value)
            $data_to_send['value'] = $value;


        $curl = new OACurl();
        $curl->setOpt(CURLOPT_RETURNTRANSFER , true);
        $curl->setOpt(CURLOPT_TIMEOUT , 0.1);
        $curl->setOpt(CURLOPT_HEADER , true);
        $curl->post($url , $data_to_send);
    }


}