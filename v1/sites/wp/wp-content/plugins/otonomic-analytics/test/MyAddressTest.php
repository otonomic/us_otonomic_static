<?php


class MyAddressTest extends WP_UnitTestCase {

    function testMapChangeAddress()
    {
	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();

        $new_address = 'beer-sheva, Israel';
	    $Otonomic_First_Session->set_settings('contact_address', $new_address);
        $address = $Otonomic_First_Session->get_settings('contact_address');

        $maphtml = otonomic_map_handler(['address' => $address]);
        $expected = '[map address="'.  $new_address .'" width="100%" height="600px"]';

        $this->assertEquals($maphtml, $expected);

    }
}
