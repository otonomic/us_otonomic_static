=== Cool Tag Cloud ===
Contributors: flector
Donate link: http://goo.gl/CcxWYg
Tags: categories, category, cloud, tag, tag cloud, tags, taxonomy, widget, sidebar, cool, wp_tag_cloud, responsive, image tag, tagcloud, tags list, tagging, admin, image, images
Requires at least: 3.3
Tested up to: 4.0
Stable tag: 1.01

A simple, yet very beautiful tag cloud.

== Description ==

The plugin renders a tag cloud using a professionally designed tag image as a background.

The plugin's tag cloud is completely responsive and is correctly rendered in all browsers.

If you liked my plugin, please `rate` it.

Special thanks to [Orman Clark](http://www.premiumpixels.com/freebies/tagtastic-tag-cloud-psd/) for the tag image and to [Dimox](http://dimox.name/beautiful-tags-markup/) for the CSS code.


== Installation ==

1. Upload `cool-tag-cloud` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the `Plugins` menu in WordPress.
3. Add the plugin's widget on the `Appearance\Widgets` page.
4. That's all.


== Frequently Asked Questions ==

= Why is the font size limited to 17px? =

Because when a larger font size is set, the tag text does not fit into the tag image.

= Does the plugin support localization? =

Yes, please send your localization files (.mo и .po) to rlector@gmail.com.

== Screenshots ==

1. Configuring the plugin's widget.
2. Font-family: Arial, Font weight: Bold, Font size: 11px to 16px.
3. Font-family: Rockwell, Font weight: Bold, Font size: 11px (smallest and largest), Image align: Right.
4. Font-family: Rockwell, Font weight: Bold, Font size: 14px (smallest and largest).
5. Font-family: Arial, Font weight: Normal, Font size: 10px  (smallest and largest), Dark Theme.
6. Font-family: Rockwell, Font weight: Bold, Font size: 12px (smallest and largest), Text transform: Uppercase.
7. Font-family: "Open Sans", Font weight: Bold, Font size: 10px to 17px.

== Changelog ==

= 1.01 =
* add option "Image align"
* css fixes

= 1.00 =
* first version
