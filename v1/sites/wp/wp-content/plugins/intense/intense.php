<?php

/*
Plugin Name: Intense WordPress Plugin
Plugin URI: http://codecanyon.net/user/IntenseVisions
Description: Intense WordPress Plugin - Premium plugin with many useful shortcodes, custom post types, and more
Version: 2.2.1
Author: Intense Visions, Inc.
Author URI: http://intensevisions.com
*/

define( 'INTENSE_PLUGIN_VERSION', "2.2.1" );
define( 'INTENSE_PLUGIN_FILE', __FILE__ );
define( 'INTENSE_PLUGIN_FOLDER', dirname( INTENSE_PLUGIN_FILE ) );
define( 'INTENSE_PLUGIN_URL', ( ( !empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' ) || $_SERVER['SERVER_PORT'] == 443 ? str_replace( "http://", "https://", plugins_url() . '/intense' ) : plugins_url() . '/intense' ) );
define( 'INTENSE_PRODUCT_KEY', '60B91BC4-C5F3-4BFB-B85E-F6B16F1E9C4E' );
define( 'INTENSE_ACTIVATION_SERVER', 'http://intensevisions.com' );
define( 'INTENSE_DEBUG', WP_DEBUG );

require_once 'inc/intense.php';