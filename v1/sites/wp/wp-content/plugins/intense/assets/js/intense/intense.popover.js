/*!
 * Popover Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

jQuery(function($) {
	$(document).ready(function() {
		var adjustY = -$('#wpadminbar').outerHeight();

		$('[data-intenseqtip="popover"]').each(function() {
			var titleSetting = $(this).attr('title');
			var textSetting = $(this).data('content');
			var positionSetting = $(this).data('placement');
			var atPositionSetting = $(this).data('at-placement');
			var styleClass = $(this).attr('id');

			$(this).qtip({
				content: {
					title: titleSetting,
					text: textSetting
				},
				position: {
					my: positionSetting,
					at: atPositionSetting,
					adjust: { x: 0, y: adjustY }
				},
				style: {
					classes: styleClass
				}
			});
		});
	});
});
