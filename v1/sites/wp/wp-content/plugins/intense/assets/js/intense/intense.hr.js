/*!
 * Horizontal Ruler helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

jQuery(function($) {
	'use strict';

	function setupHrTitles() {
		var horizontalTitles = $('.intense.hr').not('.vertical');
		var verticalTitles = $('.intense.hr.vertical');

		$('.hr-title', horizontalTitles).each(function() {
			var $el = $(this);
			var $hrContainer = $el.parent();
			var elementTop = -($el.outerHeight() / 2) - 4;

			$el.css('top', elementTop + 'px');

			if ($el.hasClass('hr-title-center')) {
				var elementLeft = ($hrContainer.outerWidth() / 2) - ($el.outerWidth() / 2);
				$el.css('left', elementLeft + 'px');
			} else if ($el.hasClass('hr-title-left') && $el.parent().hasClass("large")) {
				var $tempContainer = $('<div class="container"></div>');
				$el.parent().append($tempContainer);
				var elementLeftp = $tempContainer.css("margin-left");
				$el.css('left', elementLeftp);
				$tempContainer.remove();
			} else if ($el.hasClass('hr-title-right') && $el.parent().hasClass("large")) {
				var $tempContainerRight = $('<div class="container"></div>');
				$el.parent().append($tempContainerRight);
				var elementRight = $tempContainerRight.css("margin-right");
				$el.css('right', elementRight);
				$tempContainerRight.remove();
			}

			$el.show();
		});

		$('.hr-title', verticalTitles).each(function() {
			var $el = $(this);
			$el.css('position', 'relative');
			$el.show();
			var $hrContainer = $el.parent();
			var elementTop = ($hrContainer.outerHeight() / 2) - $el.outerHeight() / 2;
			var elementWidth = $el.outerWidth();

			$el.css('margin-left', (-elementWidth / 2) - 12 + 'px');
			$el.css('position', 'absolute');
			$el.css('width', elementWidth);

			if ($el.hasClass('hr-title-center')) {
				$el.css('top', elementTop + 'px');
			} else if ($el.hasClass('hr-title-top')) {
				$el.css('top', '0');
			} else if ($el.hasClass('hr-title-bottom')) {
				$el.css('top', $hrContainer.outerHeight() - $el.outerHeight() + 'px');
			} else {
				$el.css('top', elementTop + 'px');
			}

			if (!$hrContainer.parent().hasClass('vertical-hr-wrapper')) {
				$hrContainer.wrap('<div class="vertical-hr-wrapper"></div>');
				$hrContainer.parent().css('width', elementWidth + 30);
				$hrContainer.parent().css('display', 'inline-block');
				$hrContainer.parent().css('text-align', 'center');
			}
		});
	}

	$(document).ready(function() {
		setupHrTitles();

		$(window).resize(function() {
			setupHrTitles();
		});
	});
});