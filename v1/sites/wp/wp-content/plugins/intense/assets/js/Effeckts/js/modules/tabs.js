/*!
 * strength.js
 * Original author: @aaronlumsden
 * Further changes, comments: @aaronlumsden
 *
 * Rewritten for effeckts project
 *
 * Licensed under the MIT license
 */
var Tabs = {

  tabsWrapClass:  '.effeckt-tabs-wrap',
  tabsClass:      '.effeckt-tabs a.effeckt-tab',
  tabContentClass:'.effeckt-tab-content',

  isTouchDevice: intenseModernizr.touch,
  supportsOpacity: intenseModernizr.opacity,

  init: function() {

    this.initComponent();
    this.bindUIActions();

    // cludge to make sure the tab is resized if its contents get bigger/smaller
    setInterval(function() {
      Tabs.resizeTabs();
    }, 250);
  },

  initComponent: function() {
    
    //keep a reference to this (Tabs) object.
    var self = this;
    var $ = jQuery.noConflict();

    $(this.tabsWrapClass).each(function(){

      var $el             = $(this);
      var effect          = $el.data('effeckt-type');
      var tabContents     = $el.find(self.tabContentClass);
      var activeTabContent = tabContents.filter('.active');
      var tabs            = $el.find(self.tabsClass);

      if (!activeTabContent || activeTabContent.length <= 0) {
        activeTabContent = tabContents.first();
        tabs.first().addClass('active');
        tabs.first().parent().addClass('active');
      }

      tabContents.not(activeTabContent).addClass('effeckt-hide');

      activeTabContent.addClass('effeckt-show').removeClass('effeckt-hide');
      activeTabContent.parent().css('background-color', activeTabContent.css('background-color'));

      tabContents.css('visibility', 'visible');      
      tabContents.parent().height(activeTabContent.height());
      $el.removeClass('preload');

      if (!self.supportsOpacity) {
        tabContents.not(activeTabContent).hide();
      }
    });

  },

  bindUIActions: function() {

    //keep a reference to this (Tabs) object.
    var $ = jQuery.noConflict();
    var self = this,
        evt  = ( this.isTouchDevice ) ? 'touchstart' : 'click';

    $(this.tabsClass).on(evt, function(e) {
      e.preventDefault();
      self.showTab(this);
    });

  },

  resizeTabs: function() {
	 //keep a reference to this (Tabs) object.
    var self = this;
    var $ = jQuery.noConflict();
  
    $(this.tabsWrapClass).each(function() {
      var $el             = $(this);
      var tabContents     = $el.find(self.tabContentClass);
      var activeTabContent = tabContents.filter('.effeckt-show');

      if (activeTabContent.height() != tabContents.parent().height()) {
        tabContents.parent().height(activeTabContent.height());
      }
    });
  },

  showTab: function(el, scrollIntoView, scrollOffset) {
    var $ = jQuery.noConflict();
    var tab         = $(el);
    var tabsWrap    = tab.parents(this.tabsWrapClass);
    var tabs        = tabsWrap.find(this.tabsClass);
    var tabContents = tabsWrap.find(this.tabContentClass);
    var effect      = tabsWrap.data('effeckt-type');
    
    //set active to this current clicked tab
    tabs.removeClass('active');
    tabs.parent().removeClass('active');
    tab.parent().addClass('active');
    tab.addClass('active');

    var tabID = tab.attr('href');
    var tabContent = tabContents.filter(tabID);

    tabContents.filter('.effeckt-show').removeClass('effeckt-show').removeClass('active').addClass('effeckt-hide');
    tabContent.addClass('effeckt-show').removeClass('effeckt-hide');
    tabContent.parent().css('background-color', tabContent.css('background-color'));

    if (!this.supportsOpacity) {
      tabContents.hide();
      tabContent.show();
    }
  
    //add parent height, just because of position: absolute;
    tabContents.parent().height(tabContent.height());

    if (scrollIntoView) {
     if (!scrollOffset) scrollOffset = 0;
     jQuery("html, body").stop().animate({
       scrollTop: tab.offset().top + scrollOffset
      }, 1000);
     }
    }
};

jQuery(window).load(function () {
  Tabs.init();
});
