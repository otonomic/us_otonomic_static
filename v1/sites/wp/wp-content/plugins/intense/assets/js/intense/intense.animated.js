/*!
 * Animated Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

var $scrollAnimatedElements = null;

function isScrolledIntoView(el) {
	var $placeholder;

	if (jQuery(el).data('starthidden')) {
		$placeholder = jQuery(el).next('.animation-placeholder');
	} else {
		$placeholder = jQuery(el);
	}

	if ($placeholder.length > 0) {
		var docViewTop = jQuery(window).scrollTop();
		var docViewBottom = docViewTop + jQuery(window).height();
		var elTop = $placeholder.offset().top;
		var elBottom = $placeholder.offset().top + $placeholder.outerHeight();
		var scrollPercent = (parseInt(jQuery(el).data('scrollpercent'), 10) / 100);
		var height = elBottom - elTop;
		var amountVisible = docViewBottom - elTop;
		var neededAmountVisible = height * scrollPercent;

		//console.log("amount visible: " + amountVisible + " | needed: " + neededAmountVisible + " (" + scrollPercent+ ")");

		return amountVisible >= neededAmountVisible;
	} else {
		return false;
	}
}

var intenseClickAnimationTimeout = null;

function intenseClickAnimation($el) {
	var animation = $el.data('animation');
	var delay = parseInt($el.data('delay'), 10);
	var reset = parseInt($el.data('reset'), 10);

	$el.addClass(animation);

	if ($el.data('reset')) {
		$el.removeClass('animated');
	}

	clearTimeout( intenseClickAnimationTimeout );

	intenseClickAnimationTimeout = setTimeout(function() {
		$el.addClass('animated');

		if ($el.data('reset')) {
			setTimeout( function(){  
				$el.removeClass('animated');
			}, reset);
		}
	}, delay);
}

function intenseHoverAnimation($el, $triggerEl) {
	$el.not('.instantiated').mouseenter(function() {
		var animation = $triggerEl.data('animation');
		var delay = parseInt($triggerEl.data('delay'), 10);	

		var timeoutid = setTimeout(function() {
			$triggerEl.addClass('animated').addClass(animation);
		}, delay);
		
		$triggerEl.data('timeoutid', timeoutid);
	});

	$el.not('.instantiated').mouseleave(function() {
		var animation = $triggerEl.data('animation');

		if (!$triggerEl.data('endhidden')) {
			$triggerEl.removeClass('animated').removeClass(animation);
		}

		if ( $triggerEl.data('timeoutid') ) {
			window.clearTimeout($triggerEl.data('timeoutid'));
		}
	});
}

function intenseSetupAnimations() {
	var $ = jQuery.noConflict();
	var isIE9 = $("#intense-browser-check").hasClass('ie9');
	var isOldIE = $("#intense-browser-check").hasClass('oldie') && !isIE9;
	var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );

	$scrollAnimatedElements = $('.scroll-animated');

	// Show the items that start hidden so you can 
	// calculate top positions correctly later.
	// Replace the hidden item with a placeholder
	$scrollAnimatedElements.not('.instantiated').each(function(index, el) {
		var $el = $(el);

		$el.addClass('instantiated');

		if ($el.data('starthidden')) {
			$el.show();
			$('<div class="animation-placeholder" style="height: ' + $el.outerHeight() + 'px; width: ' + $el.outerWidth() + 'px;">').insertAfter($el);
			$el.hide();
		}
	});

	// show items that are already visible on page
	$scrollAnimatedElements.each(function(index, el) {
		var $el = $(el);		

		if (isScrolledIntoView(this)) {
			setTimeout(function() {
				var animation = $el.data('animation');
				var delay = parseInt($el.data('delay'), 10);

				setTimeout(function() {
					$el.next('.animation-placeholder').remove();
					$el.show().addClass('animated').addClass(animation);

					if (isIE9 && $el.data('endhidden')) {
						$el.hide();
					}
					$(window).trigger('resize'); //hack for flexslider to show resize itself
				}, delay);
			}, 700);
		}
	});

	// disable scroll animations for old versions of IE and iOS
	if (isOldIE || iOS) {
		$scrollAnimatedElements.each(function(index, el) {
			var $el = $(el);

			if ($el.data('starthidden')) {
				$('.animation-placeholder').hide();
				$el.show();
			} else {
				$el.hide();
			}
		});
	}

	$('.click-animated').not('.instantiated').each(function(index, el) {
		var $el = $(el);

		if ($el.data('starthidden')) {
			$el.show();
		}

		if ($el.data('client-id')) {
			$('#' + $el.data('client-id')).click(function() {
				intenseClickAnimation($el);
			});
		}
	});

	$('.hover-animated').not('.instantiated').each(function(index, el) {
		var $el = $(el);

		if ($el.data('starthidden')) {
			$el.show();
		}

		if ($el.data('client-id')) {
			intenseHoverAnimation( $('#' + $el.data('client-id')), $el );
		} else {
			intenseHoverAnimation( $el, $el );
		}
		
	});

	$('.click-animated').not('.instantiated').click(function() {	
		var $el = $(this);
		
		if (!$el.data('client-id')) {
			intenseClickAnimation($el);
		}
	});

	$('.click-animated').not('.instantiated').addClass('instantiated');

	if (!isOldIE && !iOS) {
		$(document).scroll(function() {

			$scrollAnimatedElements.each(function() {
				if (isScrolledIntoView(this)) {
					var $el = $(this);
					var animation = $(this).data('animation');
					var delay = parseInt($el.data('delay'), 10);

					setTimeout(function() {
						$el.next('.animation-placeholder').remove();
						$el.show().addClass('animated').addClass(animation);

						if (isIE9 && $el.data('endhidden')) {
							$el.hide();
						}
						$(window).trigger('resize'); //hack for flexslider to show resize itself
					}, delay);
				}
			});
		});
	}

	$('.hover-animated').not('.instantiated').addClass('instantiated');

	$('.delay-animated').not('.instantiated').each(function() {
		var animation = $(this).data('animation');
		var delay = parseInt($(this).data('delay'), 10);
		var $el = $(this);

		setTimeout(function() {
			$el.show().addClass('animated').addClass(animation);
		}, delay);

		$el.addClass('instantiated');
	});
}

jQuery(function($) {
	$(window).load(function() {
		intenseSetupAnimations();
	});
});
