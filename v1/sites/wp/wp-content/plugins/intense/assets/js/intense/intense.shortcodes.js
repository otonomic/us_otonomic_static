/*!
 * Shortcodes Media Helper Functions
 * Copyright (c) 2014 Intense Visions, Inc.
 */

/* global wp */
/* exported loadFileFrame, loadImageFrame, loadVideoFrame, loadVideoFrame */
/* jshint -W061 */

var frame;

function loadFileFrame($button, callback) {
	if (typeof(frame) !== 'undefined') {
		frame.close();
	}

	// Create the media frame.
	frame = wp.media.frames.file_frame = wp.media({
		title: $button.data('uploader_title'),
		button: {
			text: $button.data('uploader_button_text')
		},
		multiple: false // Set to true to allow multiple files to be selected
	});

	// When an image is selected, run a callback.
	frame.on('select', function() {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();

		if (callback) {
			callback(attachment);
		}
	});

	// Finally, open the modal
	frame.open();
}

function loadImageFrame($button, callback) {
	if (typeof(frame) !== 'undefined') {
		frame.close();
	}

	// Create the media frame.
	frame = wp.media.frames.file_frame = wp.media({
		title: $button.data('uploader_title'),
		button: {
			text: $button.data('uploader_button_text')
		},
		library: {
			type: 'image'
		},
		multiple: false // Set to true to allow multiple files to be selected
	});

	// When an image is selected, run a callback.
	frame.on('select', function() {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();

		if (callback) {
			callback(attachment);
		}
	});

	// Finally, open the modal
	frame.open();
}

function loadVideoFrame($button, callback) {
	if (typeof(frame) !== 'undefined') {
		frame.close();
	}

	// Create the media frame.
	frame = wp.media.frames.file_frame = wp.media({
		title: $button.data('uploader_title'),
		button: {
			text: $button.data('uploader_button_text')
		},
		library: {
			type: 'video'
		},
		multiple: false // Set to true to allow multiple files to be selected
	});

	// When an image is selected, run a callback.
	frame.on('select', function() {
		// We set multiple to false so only get one image from the uploader
		var attachment = frame.state().get('selection').first().toJSON();

		if (callback) {
			callback(attachment);
		}
	});

	// Finally, open the modal
	frame.open();
}

// Loads the media upload tool with multiselect and executes
// the callback after images has been selected
function loadGalleryFrame($button, callback) {
	if (typeof(frame) !== 'undefined') {
		frame.close();
	}

	// Create the media frame.
	frame = wp.media.frames.file_frame = wp.media({
		title: $button.data('uploader_title'),
		button: {
			text: $button.data('uploader_button_text')
		},
		library: {
			type: 'image'
		},
		multiple: true // Set to true to allow multiple files to be selected
	});

	// When an image is selected, run a callback.
	frame.on('select', function() {
		// We set multiple to false so only get one image from the uploader
		var selection = frame.state().get('selection');
		var attachments = [];

		selection.map(function(attachment) {
			attachment = attachment.toJSON();
			attachments.push(attachment);
		});

		if (callback) {
			callback(attachments);
		}
	});

	// Finally, open the modal
	frame.open();
}

/*!
 * Shortcodes Dialogs Helper Functions
 */

function intenseRefreshPreview() {
	var $ = jQuery.noConflict();

	if ($('#preview-content').length > 0) {
		$('#preview').click();
	}
}

function intenseRenderDialog(shortcode, preset) {
	var $ = jQuery.noConflict();

	$('#intense-shortcode-selector').css('max-width', '1000px');
	$('.intense-shortcodes').hide();
	$('.intense-shortcode-input').show().html('<center><img src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="padding: 100px;"></center>');
	$('.intense-shortcode-filter').hide();

	var active_skin = $('#intense-active-skin').val();

	$.ajax({
		type: 'POST',
		url: ajaxurl + '?action=intense_shortcode_dialog_action&shortcode=' + shortcode + '&preset=' + preset + "&skin=" + active_skin,
		beforeSend: function() {

		},
		success: function(data) {
			$('#intense-shortcode-selector').css('max-width', '1000px');
			$('.intense-shortcode-input').html(data);

			var previewXHR = null;

			$('.intense-shortcodes-back').show();
			$('#preview').click(function() {
				var shortcodeContent = eval($(this).data('function'));
				var shortcodeClass = $(this).data('shortcode-class');
				var $previewcontent = $('<div id="preview-content"></div>');
				var activeTab = 0;

				if (previewXHR) {
					previewXHR.abort();
				}

				previewXHR = $.ajax({
					type: 'POST',
					url: ajaxurl + '?action=intense_shortcode_preview_action',
					cache: false,
					data: {
						security: $('#nonce').val(),
						shortcode: shortcodeContent,
						'shortcode-class': shortcodeClass
					},
					beforeSend: function() {
						if ($('#preview-content').length === 0) {
							$('#shortcode-form .form-actions').append($previewcontent);
						} else {
							var i = 0;

							$('#preview-content > .nav.nav-tabs > li').each(function() {
								if ($(this).hasClass('active')) {
									activeTab = i;
								}

								i++;
							});
						}

						$('#preview-content').parent().find('.intense-preview-loader').remove();
						$('#preview-content').parent().prepend('<img class="intense-preview-loader" src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="float: right;" />');
					},
					success: function(data) {
						$('#preview-content').parent().find('.intense-preview-loader').remove();
						$('#preview-content').remove();
						$('#shortcode-form .form-actions').append($previewcontent);
						$previewcontent.html(data);
						// These were causing the editor not to insert shortcode. Probably 
						// re-initializing the editor and messing things up
						//$(document).trigger('ready');
						//$(window).trigger('load');
						$('#preview-content > .nav.nav-tabs > li').removeClass('active');
						$($('#preview-content > .nav.nav-tabs > li')[activeTab]).addClass("active");
						$('#preview-content > .tab-content > .tab-pane').removeClass('active');
						$($('#preview-content > .tab-content > .tab-pane')[activeTab]).addClass("active");
						previewXHR = null;
					}
				});
			});

			//intenseSetupPresets();
			intenseInitializeRepeater();
		},
		dataType: 'html'
	});
}

function intenseResetDialog() {
	var $ = jQuery.noConflict();

	$('#intense-shortcode-selector').css('max-width', '1000px');
	$('.intense-shortcode-input').fadeOut().html('');
	$('.intense-shortcodes-back').hide();
	$('.intense-shortcodes').fadeIn();
	$('.intense-shortcode-filter').show();
	$('.intense-shortcode-filter > input').val('');
	$('.intense-shortcode-filter > input').keyup();
	$('.intense.shortcode-list > li').css("opacity", 1).css("background-color", "#fff");
}

/*!
 * Shortcodes Repeater Helper Functions
 */

function intenseInitializeRepeater() {
	var $ = jQuery.noConflict();

	$('.of-color.minicolors-input', $('.repeater-item-model')).minicolors('destroy');

	$('.repeater-add').click(function() {
		var $repeaterItem = $('.repeater-item-model').clone();
		var html = $repeaterItem.html().replace(/_id/g, '_' + $('.repeater-item').length); //set up ids

		$('.of-color.minicolors-input', $('.repeater-item-model')).minicolors('destroy');

		$repeaterItem.html(html);
		$repeaterItem.removeClass('repeater-item-model').addClass('repeater-item');
		$repeaterItem.find(".repeater-number").html($('.repeater-item').length + 1);

		$('.repeater-add').before($repeaterItem);

		intenseInitShortcode();

		$repeaterItem.slideDown();

		$repeaterItem.find('[data-toggle="tab"]').each(function() {
			var originalID = $(this).attr('href');
			var newID = originalID + '_' + $('.repeater-item').length;

			$(this).attr('href', newID);
		});

		$repeaterItem.find('.tab-pane').each(function() {
			var originalID = $(this).attr('id');
			var newID = originalID + '_' + $('.repeater-item').length;

			$(this).attr('id', newID);
		});

		intenseRefreshPreview();
	});

	$(document).on('click', '.repeater-remove', function() {
		var $item = $(this).parents('.repeater-item');

		$item.fadeOut(400, function() {
			$item.remove();

			var items = $('.repeater-item');
			var count = $('.repeater-item').length;

			for (var i = 0; i < count; i++) {
				$(items[i]).find(".repeater-number").html(i + 1);
			}
		});
	});

	$(document).on('click', '.repeater-hide', function() {
		var $content = $('.repeater-content', $(this).parents('.repeater-item'));

		$content.toggle();

		if ($content.is(':visible')) {
			$(this).html('&minus;');
		} else {
			$(this).html('&plus;');
		}
	});
}

/*!
 * Shortcodes Presets Helper Functions
 */

function intenseRenderPresets(shortcode) {
	var $ = jQuery.noConflict();

	$.ajax({
		type: 'POST',
		url: ajaxurl + '?action=intense_shortcode_presets_action',
		cache: false,
		data: {
			security: $('#nonce').val(),
			shortcode: shortcode
		},
		beforeSend: function() {
			$('#preset-list-footer').append('<img class="intense-preview-loader" src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="float: right;" />');
		},
		success: function(data) {
			$('#preset-list-footer').find('.intense-preview-loader').remove();
			$('#preset-list-content').html(data);
			intenseSetupPresets();
		}
	});
}

function intenseSetupPresets() {
	var $ = jQuery.noConflict();

	$('#save').click(function() {
		var shortcodeContent = eval($(this).data('function'));
		var shortcodeClass = $(this).data('shortcode-class');
		var key = $('#key').val();
		var activeTab = 0;

		$.ajax({
			type: 'POST',
			url: ajaxurl + '?action=intense_shortcode_save_action',
			cache: false,
			data: {
				security: $('#nonce').val(),
				shortcode: shortcodeContent,
				'shortcode-class': shortcodeClass,
				key: key
			},
			beforeSend: function() {
				$('#preset-list-footer').append('<img class="intense-preview-loader" src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="float: right;" />');
			},
			success: function(data) {
				$('#preset-list-footer').find('.intense-preview-loader').remove();
				intenseRenderPresets(shortcodeClass);
			}
		});
	});

	$("#presets").click(function(e) {
		$('#preset-list').toggle();
		e.stopPropagation();
	});

	$('#preset-list').click(function(e) {
		e.stopPropagation();
	});

	$(document).click(function() {
		$('#preset-list').hide();
	});

	$(".preset-list-item").click(function() {
		var preset = $(this).parent().attr('id').replace('preset-list-item-', '');
		var shortcode = $('#shortcode-form').data('shortcode');

		intenseRenderDialog(shortcode, preset);
	});

	$(".preset-list-remove").click(function(e) {
		var preset = $(this).parent().attr('id').replace('preset-list-item-', '');
		var shortcode = $('#shortcode-form').data('shortcode');

		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: ajaxurl + '?action=intense_shortcode_remove_action',
			cache: false,
			data: {
				security: $('#nonce').val(),
				shortcode: shortcode,
				key: preset
			},
			beforeSend: function() {
				$('#preset-list-footer').append('<img class="intense-preview-loader" src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="float: right;" />');
			},
			success: function(data) {
				$('#preset-list-footer').find('.intense-preview-loader').remove();
				intenseRenderPresets(shortcode);
			}
		});
	});
}

/*!
 * Snippet Functions
 */

function intenseResetSnippetDialog() {
	var $ = jQuery.noConflict();
	var $snippetSelect = $('#snippet-selection');
}

function intenseLoadSnippets() {
	var $ = jQuery.noConflict();

	if (!$('#snippet-selection').data('initialized')) {
		$('#snippet-selection').data('initialized', true);

		$.ajax({
			type: 'POST',
			url: ajaxurl + '?action=intense_load_snippets_action',
			cache: false,
			dataType: 'json',
			data: {
				security: $('#nonce').val()
			},
			beforeSend: function() {
				$('#snippet-form .form-actions').append('<img class="intense-preview-loader" src="' + intense_vars.plugin_url + '/assets/img/ajax-loader.gif" style="float: right; margin-top: -10px;" />');
			},
			success: function(data) {
				var $snippetSelect = $('#snippet-selection');

				$snippetSelect.data('snippets', data);

				$('#snippet-form .form-actions').find('.intense-preview-loader').remove();

				$snippetSelect.children().remove();

				$snippetSelect.append(
					$("<option></option>")
					.attr("value", '')
					.text('')
					.data("content", '')
				);

				$.each(data, function(i, location) {
					$option_group = $('<optgroup label="' + location.title + '">');

					$.each(location.snippets, function(s, snippet) {
						$option_group.append(
							$("<option></option>")
							.attr("value", snippet.id)
							.text(snippet.title)
						);
					});

					$snippetSelect.append($option_group);
				});

				$snippetSelect.selectize({
					create: false,
					sortField: 'text',
					render: {
						option: function(item, escape) {
							return '<div>' + item.text + '</div>';
						},
						item: function(item, escape) {
							return '<div><i class="dashicons dashicons-intense-scissors" style="font-size: 15px;" title="' + item.text + '"></i> ' + item.text + '</div>';
						}
					},
					onChange: function(value) {
						var snippetdata = $('#snippet-selection').data('snippets');
						var content = '';

						$.each(snippetdata, function(i, location) {
							$.each(location.snippets, function(s, snippet) {
								if (snippet.id == value) {
									content = snippet.content;
									return;
								}
							});
						});

						$('#snippet-content').val(content);

						if (content === '') {
							$('#snippet-content').addClass('disabled').attr("disabled", "disabled");
						} else {
							$('#snippet-content').removeClass('disabled').attr("disabled", null);
						}
					}
				});
			}
		});
	}
}

/*!
 * Live Preview
 */

var intensePreviewTimer = null;

function intenseInitLivePreview() {
	var $ = jQuery.noConflict();

	$('#post-preview').after('<span id="intense-live-preview">Live <input type="checkbox" id="intense-live-preview-toggle" /></span>');

	$(document).on('keyup', '.wp-editor-area, #title', intenseLivePreview);
	$(document).on('change', '.wp-editor-area, #title, #intense-live-preview-toggle, #comment_status, #ping_status', intenseLivePreview);

	$.each(tinyMCE.editors, function(i, e) {
		e.onChange.add(function(ed, l) {
			intenseLivePreview();
		});
	});
}

function intenseLivePreview() {
	var $ = jQuery.noConflict();

	if ($('#intense-live-preview-toggle').prop('checked')) {
		clearTimeout(intensePreviewTimer);

		intensePreviewTimer = setTimeout(function() {
			$('#post-preview').click();
		}, 700);
	}
}

/*!
 * Initialize
 */

var intense_dialog_initialized = false;

jQuery(function($) {
	if (!intense_dialog_initialized) {
		intenseInitLivePreview();

		//get selection on mouseover so you don't lose focus when the
		//button is clicked
		$('body').on('mouseover', '.intense-shortcode', function(e) {
			mce_selection_start = 0;
			mce_selection_end = 0;

			if (typeof qtrans_save != 'undefined') {
				if ($('.mce-tinymce:visible').length > 0) {
					mce_selection_start = tinyMCE.activeEditor.selection.getRng().startOffset;
					mce_selection_end = tinyMCE.activeEditor.selection.getRng().endOffset;
				} else {
					var ctl = document.getElementById('qtrans_textarea_content');
					mce_selection_start = ctl.selectionStart;
					mce_selection_end = ctl.selectionEnd;
				}
			}
		});

		// show dialog
		$('body').on('click', '.intense-shortcode', function(e) {
			e.preventDefault();

			$(this).magnificPopup({
				type: 'inline',
				alignTop: true,
				callbacks: {
					open: function() {
						intenseResetDialog();
					},
					afterClose: function() {
						intenseResetDialog();
					}
				}
			}).magnificPopup('open');

			return false;
		});

		$('body').on('click', '.intense-snippet', function(e) {
			e.preventDefault();

			$(this).magnificPopup({
				type: 'inline',
				alignTop: true,
				callbacks: {
					beforeOpen: function() {
						intenseLoadSnippets();
					},
					open: function() {
						intenseResetSnippetDialog();
					},
					afterClose: function() {
						intenseResetSnippetDialog();
					}
				}
			}).magnificPopup('open');
		});

		$('body').on('click', '#insert-snippet', function(e) {
			e.preventDefault();

			window.wp.media.editor.insert($('#snippet-content').val());

			$('.intense-snippet').magnificPopup('close');
		});

		// filter shortcodes
		$('.intense-shortcode-filter > input').keyup(function() {
			if ($(this).val().length === 0) {
				$('.intense.shortcode-list > li').css("opacity", 1).css("background-color", "#fff");
			} else {
				var r = new RegExp($(this).val(), 'i');
				$('.intense.shortcode-list > li').css("opacity", 0.2).css("background-color", "#fff");
				$('.intense.shortcode-list > li').filter(function() {
					return $(this).text().match(r);
				}).css("color", "#000").css("opacity", 1).css("background-color", "#DCE7F1");
			}
		});

		// load shortcode specific dialog
		$('.intense.shortcode-list > li').click(function() {
			var shortcode = $(this).data('shortcode');
			intenseResetDialog();
			intenseRenderDialog(shortcode, '');
		});

		// handle back button
		$('.intense-shortcodes-back').click(intenseResetDialog);

		// insert shortcode
		$('#intense-shortcode-selector').on('click', "input[name='insert']", function(e) {
			var shortcode = eval($(this).data('function'))();
			var previewContent = $(this).data('preview-content');
			var previewContentRegex = new RegExp(previewContent, "g");
			var cleanShortcode = shortcode.replace(/<br +\/>/gi, "\n").replace(previewContent, '');

			if (typeof qtrans_save != 'undefined') {
				var currentLanguageValue = document.getElementById('qtrans_textarea_content').value;
				var currentAllLanguagesValue = document.getElementById('content').value;
				var front = currentLanguageValue.substring(0, mce_selection_start);
				var back = currentLanguageValue.substring(mce_selection_end, currentLanguageValue.length);
				var newText = front + cleanShortcode + back;

				if ($('.mce-tinymce:visible').length > 0) {
					window.wp.media.editor.insert(cleanShortcode);
				} else {
					$('#qtrans_textarea_content').val(newText);
					switchEditors.go('content', qtrans_get_active_language());
				}
			} else {
				window.wp.media.editor.insert(cleanShortcode);
			}

			$('.intense-shortcode').magnificPopup('close');

			intenseLivePreview();

			e.preventDefault();
		});

		$('#intense-active-skin').change(function() {
			if ( $('#shortcode-form').length > 0 ) {
				var shortcode = $('#shortcode-form').data('shortcode');

				intenseRenderDialog( shortcode, '' );
			}
		});

		intense_dialog_initialized = true;
	}
});