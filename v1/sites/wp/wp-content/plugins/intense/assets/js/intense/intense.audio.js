/*!
 * Audio helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

(function($) {
	'use strict';

	$(document).ready(function() {
		// Audio player
		$('.intense.audio').each(function () {
			// Prepare data
			var $this = $(this),
				id = $this.data('id'),
				selector = '#' + id,
				$player = $(selector),
				audio = $this.data('audio'),
				swf = $this.data('swf');
			// Init jPlayer
			$player.jPlayer({
				ready: function (e) {
					// Set media
					$player.jPlayer('setMedia', {
						mp3: audio
					});
					// Autoplay
					if ($this.data('autoplay') === 1) $player.jPlayer('play');
					// Loop
					if ($this.data('loop') === 1) $player.bind($.jPlayer.event.ended + '.repeat', function () {
						$player.jPlayer('play');
					});
				},
				cssSelectorAncestor: selector + '_container',
				volume: 1,
				keyEnabled: true,
				smoothPlayBar: true,
				swfPath: swf,
				supplied: 'mp3'
			});
		});
	});
})(jQuery);