/*!
 * Picstrips Plugin
 *
 * picstrips originally by Steve Claridge (http://www.moreofless.co.uk)
 * heavily modified by Intense Visions, Inc. (http://www.intensevisions.com)
 * Modifications include:
 *  - allow for browser resize
 *  - fixed bars to correctly show scaled background image at height of bar
 *  - fixed bars to correctly show sections of image taking into account hgutter
 *  - standardized javascript formatting
 *
 * Copyright (c) 2014 Intense Visions, Inc.
 * Version: 1.0 (7-JUN-2013)
 * Dual licensed under the MIT and GPL licenses.
 */
(function($) {
    $.fn.picstrips = function(options) {
        var settings = $.extend({
            'splits': 8,
            'hgutter': '10px',
            'vgutter': '60px',
            'bgcolor': '#fff'
        }, options);

        return this.each(function() {
            var that = this;

            function doStrips() {
                var $bars = initStrips(that, null);

                $(window).resize(function() {
                    $(that).show();
                    $bars = initStrips(that, $bars);
                });
            }

            function initStrips(that, $bars) {
                var h = $(that).height(),
                    w = $(that).width(),
                    hgutterWidth = parseInt(settings.hgutter, 10),
                    sw = ((w - (hgutterWidth * settings.splits)) / settings.splits), //width of a strip
                    clstyle = 'position: relative; float:left; margin-right: ' + settings.hgutter + '; background-image: url(' + that.src + '); width: ' + sw + 'px; height: ' + h + 'px; background-size: auto ' + h + 'px; ',
                    spstyle = 'position: absolute; left: 0px; width: ' + sw + 'px; height: ' + settings.vgutter + '; background-color: ' + settings.bgcolor + '; top: ',
                    laststyle = 'position: relative; float:left; background-image: url(' + that.src + '); width: ' + sw + 'px; height: ' + h + 'px; background-size: auto ' + h + 'px; ';

                if ($bars) {
                    $bars.remove();
                }

                var cnt = $('[id^=molbars_]').length + 1;
                $bars = $('<div id="molbars_' + cnt + '"></div>');

                $bars.insertAfter($(that));

                var hoffs = 0;
                for (var lp = 0; lp < settings.splits; lp++) {
                    var voffs = (lp % 2 !== 0 ? '0px' : (h - parseInt(settings.vgutter, 10)) + 'px');

                    clstyle += ' background-position: -' + hoffs + 'px 100%;';
                    laststyle += ' background-position: -' + hoffs + 'px 100%;';

                    if (lp === settings.splits - 1) {
                        $('<div style="' + laststyle + '"><span style="' + spstyle + voffs + '"></span></div>').appendTo($bars);
                    } else {
                        $('<div style="' + clstyle + '"><span style="' + spstyle + voffs + '"></span></div>').appendTo($bars);
                    }
                    
                    hoffs += sw + hgutterWidth;
                }

                $(that).hide();

                return $bars;
            }

            //make sure image has finished loading
            if (!this.complete || this.width + this.height === 0) {
                var img = new Image();
                img.src = this.src;

                $(img).load(function() {
                    doStrips();
                });
            } else {
                doStrips();
            }

        });
    };
})(jQuery);
