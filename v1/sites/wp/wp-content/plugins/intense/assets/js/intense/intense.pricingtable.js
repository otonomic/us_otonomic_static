/*!
 * Pricing Table helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

jQuery(function($) {
	$('.intense.pricing-table[data-hover-expand="true"]').hover(
		function() {
			var el = $(this);

			feature_pricing_table(el);
		},
		function() {
			var el = $(this);

			remove_feature_pricing_table(el, false);
		}
	);

	function feature_pricing_table(table) {
		if (table && table.length > 0) {
			grouping = table.data('grouping'); 

			table.css({
				'margin-top': '-5px',
				'box-shadow': '0 0 15px rgba(0,0,0,.4)',
				'z-index': '2'
			});

			table.children('.pricing-table-section').last().css({
				'padding-top': '25px',
				'padding-bottom': '25px'
			});

			if (!table.hasClass('featured')) {
				remove_feature_pricing_table($('.featured[data-grouping="' + grouping + '"]').not(table), true);
			}
		}
	}

	function remove_feature_pricing_table(table, force) {
		if (table && table.length > 0) {
			grouping = table.data('grouping');

			if (!table.hasClass('featured') || force) {
				table.css({
					'margin-top': '10px',
					'box-shadow': 'none',
					'z-index': '1'
				});

				table.children('.pricing-table-section').last().css({
					'padding-top': '10px',
					'padding-bottom': '10px'
				});
			}

			if (!table.hasClass('featured')) {
				feature_pricing_table($('.featured[data-grouping="' + grouping + '"]').not(table));
			}
		}
	}
});