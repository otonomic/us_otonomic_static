/*!
 * Content Box helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

(function($) {
    'use stict';

    $(document).ready(function() {
        // Animate content box icons
        $(".content-box[data-animation]").mouseenter(function() {
            $(this).find("i").addClass($(this).data("animation")).addClass("animated");
        }).mouseleave(function() {
            $(this).find("i").removeClass($(this).data("animation")).removeClass("animated");
        });
    });
})(jQuery);