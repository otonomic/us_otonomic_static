/*!
 * Counter Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

 /* global accounting */

jQuery(function($) {
	$(document).ready(function() {
		$('.intense.counter ').appear(function() {
			var $counter = $(this);
			var speed = $counter.data('speed');
			var start = $counter.data('start');
			var end = $counter.data('end');
			var value = start;
			var originalText = $counter.data('text');
			var decimalPlaces = $counter.data('decimal-places');
			var decimalPoint = $counter.data('decimal-point');
			var thousandsSeparator = $counter.data('thousands-separator');
			var increment = $counter.data('increment');

			increment = (start < end ? increment : -increment);

			if ( decimalPlaces > 0 ) {
				increment *= (1 / Math.pow(10, decimalPlaces));
			}

			var counterChanger = setInterval(function() {
				if ((start < end && value >= end) || (start > end && value <= end)) {
					clearInterval(counterChanger);
				}

				$counter.html(originalText.replace("{0}", accounting.formatNumber(value, decimalPlaces, thousandsSeparator, decimalPoint)));

				value += increment;
			}, speed);
		});
	});
});