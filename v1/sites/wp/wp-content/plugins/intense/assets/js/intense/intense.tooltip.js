/*!
 * Tooltip Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

jQuery(function($) {
	$(document).ready(function() {
		var adjustY = -$('#wpadminbar').outerHeight();

		$('[data-intenseqtip="tooltip"]').each(function() {
			var positionSetting = $(this).data('placement');
			var atPositionSetting = $(this).data('at-placement');
			var styleClass = $(this).attr('id');

			$(this).qtip({
				prerender: true,
				position: {
					my: positionSetting,
					at: atPositionSetting,
					adjust: {
						x: 0,
						y: adjustY
					}
				},
				style: {
					classes: styleClass
				}
			});
		});
	});
});