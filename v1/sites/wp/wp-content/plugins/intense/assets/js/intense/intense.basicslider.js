/*!
 * Basic Slider Plugin
 * Copyright (c) 2014 Intense Visions, Inc.
 * Version: 1.0.2 (21-NOV-2013)
 * Dual licensed under the MIT and GPL licenses.
 */
(function($) {
	$.basicslider = function(el, options) {
		// To avoid scope issues, use 'base' instead of 'this'
		// to reference this class from internal events and functions.
		var base = this;

		// Access to jQuery and DOM versions of element
		base.$el = $(el);
		base.el = el;

		// Add a reverse reference to the DOM object
		base.$el.data("basicslider", base);

		base.init = function() {
			base.options = $.extend({}, $.basicslider.defaultOptions, options);

			// Put your initialization code here
			base.slides = base.$el.children(); //the first level of children are the slides
			base.activeSlideNumber = 0;
			base.totalSlides = base.slides.length;
			base.slides.hide();
			$(base.slides[0]).show();
			base.startSlider();

			if (base.options.pauseOnHover) {
				base.$el.hover(base.stopSlider, base.startSlider);
			}
		};

		base.startSlider = function() {
			base.sliderInterval = setInterval(base.rotateSlides, base.options.speed);
		};

		base.stopSlider = function() {
			window.clearInterval(base.sliderInterval);
		};

		base.rotateSlides = function() {
			var startingSlide = base.activeSlideNumber;

			if (base.activeSlideNumber + 1 < base.totalSlides) {
				base.activeSlideNumber++;
			} else {
				base.activeSlideNumber = 0;
			}

			var endingSlide = base.activeSlideNumber;

			if (startingSlide !== endingSlide) {
				$(base.slides[startingSlide]).fadeOut(function() {
					$(base.slides[endingSlide]).fadeIn();
				});
			}
		};

		// Run initializer
		base.init();
	};

	$.basicslider.defaultOptions = {
		speed: 7000,
		pauseOnHover: true
	};

	$.fn.basicslider = function(options) {
		return this.each(function() {
			var slider;
			(slider = new $.basicslider(this, options));

			// HAVE YOUR PLUGIN DO STUFF HERE


			// END DOING STUFF

		});
	};

})(jQuery);