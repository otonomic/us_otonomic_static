/*!
 * Menu Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

jQuery(function($) {
	'use strict';
	
	$(function() {
		var $activeVerticalMenu;
		var enterTimeout;
		var leaveTimeout;
		var delay = 400;

		// hide/show children when the mouse is hovered
		$('.intense.menu.vertical li.menu-item-has-children').not('.current-page-item,.current-menu-ancestor').on( {
			mouseenter: function() {
				$activeVerticalMenu = $(this);

				clearTimeout(leaveTimeout);

				enterTimeout = setTimeout(function() {
					if ($activeVerticalMenu) {
						$activeVerticalMenu.children('.sub-menu').slideDown();
					}
				}, delay);
			},
			mouseleave: function() {
				clearTimeout(enterTimeout);

				leaveTimeout = setTimeout(function() {
					if ($activeVerticalMenu) {						
						$activeVerticalMenu.children('.sub-menu').slideUp();
						$activeVerticalMenu = null;
					}
				}, delay);
			}
		});	
		
		$('.intense.menu.mobile').each(function() { 
			var $parent = $(this).parent();
			var $slicknav = $(this).slicknav({
				duplicate: false,
				label: '',
				prependTo: $parent,
				allowParentLinks: true
			});

			$parent.find('.slicknav_menu').addClass('hidden-lg hidden-md');
			$parent.find('.intense.menu.horizontal').addClass('hidden-sm hidden-xs');
		});

		// show children of currently selected menu
		$('.intense.menu.vertical li.menu-item-has-children.current-menu-ancestor > ul').show();

		// if only showing parent icons, remove icons for children
		$('.intense.menu li').not('.menu-item-has-children').find('i.intense.only-parents').remove();

		// show icons for only parents enabled icons
		$('.intense.menu li.menu-item-has-children').find('i.intense.only-parents').removeClass('hide');

		// show icons for li elements that aren't only parents enabled
		$('.intense.menu li').find('i.intense').not('.only-parents').removeClass('hide');
	});
});