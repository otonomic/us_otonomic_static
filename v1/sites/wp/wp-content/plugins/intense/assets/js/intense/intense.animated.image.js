/*!
 * Animated Image Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

jQuery(function($) {
	function intenseAnimateSlideImage($el) {
		var currentPosition = $el.data('current-position');
		var direction = $el.data('direction');
		var anchor = $el.data('anchor');

		if (direction === 'slideleft' || direction === 'slideup') {
			// 1 pixel row at a time
			currentPosition -= 1;
		} else {
			currentPosition += 1;
		}

		$el.data('current-position', currentPosition);

		// move the background with backgrond-position css properties
		$el.css("backgroundPosition", (direction === 'slideleft' || direction === 'slideright') ?
			currentPosition + "px " + anchor + "%" : anchor + "% " + currentPosition + "px");
	}

	function intenseSetupAnimatedSlideImage(i, element) {
		var $el = $(element);
		var speed = $el.data('speed');

		$el.data('current-position', 0);

		setInterval(function() {
			intenseAnimateSlideImage($el);
		}, speed);
	}

	$('.animated-image.slide').each(intenseSetupAnimatedSlideImage);
});