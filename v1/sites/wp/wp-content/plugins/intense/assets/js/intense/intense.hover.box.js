/*!
 * Content Box helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global jQuery */

(function($) {
    'use stict';

    function responsiveHoverBox() {
        $('.intense-hover-box').each(function() {
            var $el = $(this);
            var boxtype = $(this).data('box-type');
            var random = $(this).data('random');
            var aspectRatio = $(this).data('aspect-ratio');
            var borderWidth = $(this).data('border-width');
            var backgroundRGB = $(this).data('background-rgb');
            var borderRGB = $(this).data('border-rgb');
            var width = $el.parent().width();
            var height = ( width / aspectRatio );
            var innerwidth = ( width - borderWidth );
            var innerheight = ( height - borderWidth );
            var halfheight = ( height / 2 ) + 2;
            var h3height = ( innerheight / 2 );
            var $box = $('li.ch-li-' + random, $el);
            var $img = $('.ch-img-' + random, $el);
            var $wrapper = $('.ch-info-wrap-' + random, $el);
            var $info = $('.ch-info-' + random, $el);
            var $grid = $('.ch-grid-' + random + ' li', $el);
            var $infoh3 = $('.ch-info-' + random + ' h3', $el);
            var $backh3 = $('.ch-info-back-' + random + ' h3', $el);
            var $itemhover = $('#ch-item-' + random + '.ch-item-' + boxtype, $el);

            if ( boxtype == 'style2' ) {
                h3height = ( innerheight / 3 );
            } else if ( boxtype == 'style3' ) {
                h3height = innerheight;
            } else if ( boxtype == 'style4' || boxtype == 'style5' || boxtype == 'style6' || boxtype == 'style7' ) {
                h3height = ( innerheight / 4 );
            }

            $box.width( width ).height( height );
            $img.css( { backgroundSize : width + 'px ' + height + 'px' } );

            if ( boxtype == 'style4' || boxtype == 'style5' || boxtype == 'style6' ) {
                $wrapper.width( innerwidth ).height( innerheight );
                $info.width( innerwidth ).height( innerheight );
                //$infoh3.css( { padding: h3height + 'px 0 0 0' } );
                //$infoh3.css( { height: h3height + 'px' } );
            } else if ( boxtype == 'style7' ) {
                $info.width( innerwidth ).height( innerheight );
                $backh3.css( { padding: h3height + 'px 0 0 0' } );
                //$backh3.css( { height: h3height + 'px' } );
            } else if ( boxtype == 'style2' ) {
                //$infoh3.css( { height: h3height + 'px' } );

                $itemhover.hover(function() {
                    $( this ).css( { boxShadow: 'inset 0 0 0 ' + halfheight + 'px ' + backgroundRGB + ', inset 0 0 0 ' + ( borderWidth / 2 ) + 'px ' + borderRGB + ', 0 1px 2px rgba(0,0,0,0.1)' } );
                }, function() {
                    $( this ).css( { boxShadow: 'inset 0 0 0 0 ' + backgroundRGB + ', inset 0 0 0 ' + ( borderWidth / 2) + 'px ' + borderRGB + ', 0 1px 2px rgba(0,0,0,0.1)' } );
                });
            } else {
                //$infoh3.css( { height: h3height + 'px' } );
            }
        });
    }

    $(document).ready(function() {
        responsiveHoverBox();
        $('.intense-hover-box').show();

        $(window).resize(function() {
            responsiveHoverBox();
        });
    });
})(jQuery);
