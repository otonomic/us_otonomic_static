/*!
 * Video helper JS
 * Copyright (c) 2014 Intense Visions, Inc.
 */

// Resize the intenseVideoContainer iframe videos

function resizeIntenseVideos() {
    'use strict';

    jQuery('.intenseVideoContainer').each(function() {
        var $element = jQuery(this);

        if ( typeof $element.find('iframe').attr('height') !== 'undefined' && typeof $element.find('iframe').attr('width') !== 'undefined' ) {
            var $scale = $element.find('iframe').attr('height') / $element.find('iframe').attr('width');
            var $width = $element.width();

            $element.css( 'height', $width * $scale );
        } else {
            $element.css( 'height', $element.find('iframe').height() );
        }
    });
}

(function($) {
    'use stict';

    $(document).ready(function() {
        resizeIntenseVideos();

        $(window).resize(function() {
            resizeIntenseVideos();
        });
    });
})(jQuery);