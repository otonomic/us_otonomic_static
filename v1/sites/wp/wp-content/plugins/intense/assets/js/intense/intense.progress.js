/*!
 * Progress Bar Initializer
 * Copyright (c) 2014 Intense Visions, Inc.
 */

jQuery(function($) {
	$(document).ready(function() {
		$('.intense.progress .progress-bar[data-progress]').appear(function() {
			var $progress = $(this);
			var percent = 0;
			var maxPercent = $(this).data('progress');
			var animateText = $(this).data('animate-text');
			var animateSpeed = !isNaN(parseInt($progress.parent().data('animate-speed'), 10)) ? parseInt($progress.parent().data('animate-speed'), 10) : 35;

			if (animateText) {
				var originalText = $progress.find("span").html();

				$progress.css("transition", "none");

				var progressChanger = setInterval(function() {

					if (percent >= maxPercent) {
						clearInterval(progressChanger);
					}

					$progress.width(percent + '%');
					$progress.find("span").html(originalText.replace("{0}", percent));

					percent += 1;
				}, animateSpeed);
			} else {
				$progress.width(maxPercent + '%');
			}

			setTimeout(function() {
				$progress.children().fadeIn();
			}, 250);
		});
	});
});