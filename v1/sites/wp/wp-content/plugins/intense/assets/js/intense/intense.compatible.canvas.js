/*!
 * Needed for charts to work in IE8
 * Copyright (c) 2014 Intense Visions, Inc.
 */
/* global Modernizr, compatibleCanvas */

intenseModernizr.load([{
	test: intenseModernizr.canvas,
	nope: [compatibleCanvas]
}]);

// Responsive canvas
jQuery(function($) {

	function respondCanvas(c, ctx, container) {
		ctx.canvas.width = $(container).width();
		ctx.canvas.height = $(container).height();
	}

	$(document).ready(function() {
		if (intenseModernizr.canvas) {
			resizeAllCanvas();
		}

		//this won't work since you have to redraw the 
		//content within the canvas and drawing is done by 
		//chartjs
		//$(window).resize(resizeAllCanvas); 
	});

	function resizeAllCanvas() {
		//Get the canvas &
		$('.responsive-canvas').each(function() {
			var c = $(this);
			var ct = c.get(0).getContext('2d');
			var container = $(this).parent();

			container.css('width', '100%');

			//Initial call 
			respondCanvas(c, ct, container);
		});
	}
});