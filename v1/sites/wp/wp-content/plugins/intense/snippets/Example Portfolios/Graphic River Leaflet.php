<?php
/*
Intense Snippet Name: Graphic River Leaflet
*/
?>

[intense_lead]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed magna nisl. Curabitur et semper diam, quis hendrerit purus. Nullam vitae mauris nec ipsum fringilla ornare. Duis euismod dolor nec vestibulum lobortis.[/intense_lead]

Fusce lacinia non massa eget lobortis. Nam hendrerit ultrices nisl vitae vulputate. Maecenas neque dolor, malesuada sed vehicula id, varius rhoncus lacus. Nullam sodales nisl neque, vel fringilla ante mattis eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut faucibus odio sit amet malesuada dictum. Nulla ac pulvinar nisl. Vivamus non bibendum diam. Nam adipiscing cursus eros. Nam quis sollicitudin tortor. Sed vestibulum sapien nibh, quis venenatis nunc faucibus ut.

<h2>Fusce a venenatis elit</h2>

Etiam condimentum augue sed ipsum blandit, pulvinar viverra justo viverra. Morbi sollicitudin tortor et tincidunt suscipit. Nulla non commodo urna. Fusce mi massa, luctus non justo ac, mollis fermentum diam. Suspendisse interdum eget magna vitae volutpat. In auctor mi lectus, nec imperdiet ipsum bibendum id. Cras lectus mauris, viverra at vehicula laoreet, tempor nec mauris. Fusce viverra sem metus, vel fringilla ipsum fermentum id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse fringilla tellus nulla, non iaculis felis convallis quis.

[intense_content_section size="fullboxed" image="8275" imagesize="medium800" imagemode="full" height="300" padding_bottom="10"]

[/intense_content_section]

Nulla bibendum lacus sagittis dui porttitor, ut cursus lectus sollicitudin. Suspendisse vel neque vitae nunc aliquam consequat nec et lacus. Nunc ac purus diam. In egestas felis blandit dignissim auctor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

[intense_row padding_top="0"]
[intense_column size="6"]
Etiam et eros nec sapien convallis placerat. Fusce placerat, turpis et malesuada malesuada, urna nunc elementum dui, ac mattis est enim ut felis. Suspendisse elementum est sit amet ipsum luctus interdum. Duis sagittis tempus velit, aliquam lobortis lectus aliquet vel. 

Integer dignissim, libero id lacinia rhoncus, sem mi laoreet libero, aliquet venenatis arcu lectus at neque. Morbi risus ligula, lacinia commodo tellus sit amet, gravida fringilla sapien. Duis iaculis auctor libero, eu facilisis ipsum commodo non.
[/intense_column]
[intense_column size="6"]
[intense_image image="8269" size="medium500" title="Graphic River" caption="1000's of graphic files from just $1" effeckt="4" effecktcolor="#000000" effecktopacity="80" shadow="8" /]
[/intense_column]
[/intense_row]

<h2>Suspendisse et justo vel nunc</h2>

Pellentesque dignissim fringilla erat a pulvinar. Maecenas nec diam quis odio porta mattis nec vel eros. Vestibulum nec ullamcorper mi. Morbi est massa, tempus aliquam metus et, aliquet hendrerit lacus. Praesent et lectus et mauris condimentum sollicitudin at non nisl. Cras tempor ligula et ornare tempor.

[intense_row padding_top="0"]
[intense_column size="6"]
[intense_image image="8274" size="medium500" title="Front" effeckt="9" effecktcolor="#000000" effecktopacity="80" shadow="11" caption="Donec et felis in dui cursus aliquet et eu ligula." /]
[/intense_column]
[intense_column size="6"]
[intense_image image="8273" size="medium500" title="Inside" effeckt="9" effecktcolor="#000000" effecktopacity="80" shadow="11" caption="Sed urna leo, tincidunt aliquet porta id, sodales eu nibh." /]
[/intense_column]
[/intense_row]

Vestibulum tempus nec lacus sit amet tincidunt. Nunc vitae mi risus. Vivamus aliquam sodales arcu et blandit. Donec eu massa pretium, laoreet justo quis, sodales odio. Vestibulum sagittis tellus nec lacus vulputate sagittis. Ut elementum viverra erat a volutpat. Maecenas volutpat, tellus et faucibus gravida, nibh nibh consectetur metus, sit amet euismod dui erat at lacus. Mauris nec faucibus augue.