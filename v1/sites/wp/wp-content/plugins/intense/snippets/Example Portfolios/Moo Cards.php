<?php
/*
Intense Snippet Name: Moo Cards
*/
?>

[intense_lead]Vestibulum vestibulum risus vulputate, vestibulum dolor vel, fringilla turpis. Donec fringilla blandit urna, et condimentum libero fringilla eu. In hac habitasse platea dictumst.[/intense_lead]

[intense_gallery size="square400" columns="2" marginpercent=".25" type="colorbox" effeckt="2" effeckt_color="#000000" effeckt_opacity="80" grouping="photos" id="8280" include="8284,8283,8282,8281,8285,8286" order="NONE" /]

Nullam ultrices sagittis euismod. Fusce accumsan sodales interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris porta est rutrum erat condimentum, eget laoreet est dapibus. Cras pulvinar risus id ante cursus commodo vel vulputate dolor. Nulla ac nunc arcu. Nam quis nulla tristique, condimentum dui non, auctor erat. Proin et ullamcorper massa. Morbi sollicitudin vulputate urna, at fringilla nulla rutrum sit amet. Nam nunc risus, ultricies quis odio eu, consequat interdum est.

[intense_row padding_top="0"]
[intense_column size="4"]
[intense_image image="8281" size="medium500" title="Front" effeckt="7" effecktcolor="#000000" effecktopacity="80" shadow="11" title="Morbi a convallis" caption="Donec ultricies hendrerit dictum. Donec auctor pharetra lobortis." /]
[/intense_column]
[intense_column size="4"]
[intense_image image="8282" size="medium500" title="Inside" effeckt="7" effecktcolor="#000000" effecktopacity="80" shadow="11" title="Duis a est a sapien" caption="Nam nunc risus, ultricies quis odio eu, consequat interdum est." /]
[/intense_column]
[intense_column size="4"]
[intense_image image="8283" size="medium500" title="Inside" effeckt="7" effecktcolor="#000000" effecktopacity="80" shadow="11" title="Ut vel tempor" caption="Quisque sollicitudin purus et elit porttitor, vel semper leo cursus." /]
[/intense_column]
[/intense_row]

Etiam vehicula lobortis tempus. Morbi dignissim, velit vitae vulputate imperdiet, felis neque fermentum magna, ullamcorper posuere metus odio non urna. In hac habitasse platea dictumst. Praesent massa quam, consequat laoreet dignissim at, auctor non justo. Praesent ultricies turpis sem, at vestibulum nulla vulputate venenatis. Morbi faucibus sodales nulla, vitae mattis felis lobortis eget. Nulla ullamcorper lobortis lobortis.
<h2>Nam convallis elit in faucibus</h2>
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla condimentum, magna blandit pretium pulvinar, ante risus ullamcorper elit, non tincidunt velit nibh sed orci. Etiam suscipit laoreet metus a vulputate. Integer mattis lacus at felis accumsan, ut gravida quam iaculis. Integer nunc risus, mattis in lectus a, blandit hendrerit libero.

[intense_content_section size="fullboxed" image="8284" imagesize="medium800" imagemode="full" height="550" padding_bottom="10"]

[/intense_content_section]

Curabitur pharetra justo nec pretium malesuada. Cras in eros dapibus, pulvinar enim in, tincidunt lacus. Integer sit amet condimentum felis, quis feugiat urna. Mauris blandit consectetur urna. Sed bibendum erat at consequat elementum. Phasellus fringilla dictum purus, vel blandit nibh euismod et. Vivamus egestas mi a nisi consequat, in tincidunt dolor convallis.

[intense_row padding_top="0"]
[intense_column size="6"]
[intense_image image="8285" size="medium500" shadow="10" title="Morbi a convallis" caption="Donec ultricies hendrerit dictum. Donec auctor pharetra lobortis." /]
[/intense_column]
[intense_column size="6"]
[intense_image image="8286" size="medium500" shadow="10" title="Duis a est a sapien" caption="Nam nunc risus, ultricies quis odio eu, consequat interdum est." /]
[/intense_column]
[/intense_row]

Aliquam erat volutpat. Vestibulum ornare libero tellus, quis iaculis eros tincidunt sit amet. Fusce porta, arcu quis tempus placerat, magna ipsum accumsan est, id dictum lectus magna blandit nibh. Ut consectetur magna mauris, sit amet mattis leo faucibus id. Cras pulvinar id risus at vulputate.