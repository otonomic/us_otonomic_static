<?php
/*
Intense Snippet Name: How to Be a Rockstar Freelancer
*/
?>

[intense_lead]Pellentesque ultricies dolor eu hendrerit ultricies. Etiam vel turpis semper, scelerisque eros nec, convallis odio. Cras bibendum fringilla convallis. Aenean ut cursus dui. Aenean luctus risus tempor est aliquet, sit amet faucibus lacus adipiscing. In egestas justo sit amet porttitor iaculis. Aliquam erat volutpat. Nam diam ligula, tincidunt sit amet turpis id, gravida sodales orci.[/intense_lead]

Morbi posuere velit dui. Curabitur ante lacus, facilisis eget cursus in, facilisis a metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut fermentum at mauris fermentum pulvinar. Nulla felis felis, consequat ac pulvinar in, commodo vel nibh. Donec mollis enim vitae nulla scelerisque, et porta sapien porttitor. Sed ut tellus vitae lectus mattis vehicula. Vivamus ornare tortor id magna dapibus fringilla. Vestibulum feugiat semper odio. Proin a nulla id tellus faucibus luctus. Suspendisse consectetur volutpat elit, sed gravida diam luctus vel. Maecenas sollicitudin blandit congue. Aenean dignissim, lorem non aliquam laoreet, felis quam luctus leo, quis malesuada tortor nulla non ante. Ut fermentum fringilla commodo. Sed diam nunc, luctus non commodo sed, mattis id mi.

[intense_row]
[intense_column size="6"]
[intense_image image="8253" size="medium500" title="Front Cover" effeckt="8" effecktcolor="#000000" effecktopacity="80" shadow="11" caption="Aliquam fermentum porta risus, non tincidunt nulla feugiat id." /]
[/intense_column]
[intense_column size="6"]
[intense_image image="8249" size="medium500" title="Back Cover" effeckt="8" effecktcolor="#000000" effecktopacity="80" shadow="11" caption="Donec mattis orci justo, at varius neque varius sit amet. Cras varius auctor consequat." /]
[/intense_column]
[/intense_row]

Donec vestibulum a leo et convallis. Ut mattis consectetur ante, non lacinia dui posuere at. Nulla at arcu rutrum, dictum turpis vitae, mattis est. Pellentesque et elit ut metus congue tristique ac id neque. Vivamus erat leo, consectetur non sollicitudin ultrices, sollicitudin non magna. Ut a leo quis velit vehicula auctor. Praesent tempus nulla volutpat, bibendum mauris et, tempor risus.

Phasellus a eros ac mi dapibus pulvinar vel id ipsum. Morbi in erat egestas, bibendum justo id, bibendum est. In dignissim felis in sapien auctor tempus. Aliquam vehicula velit vel tincidunt tristique. Sed quis mattis ligula, ac ultricies leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce sagittis et lacus et scelerisque. Nam fringilla consequat sem et suscipit. Phasellus vitae lacus eget lorem convallis bibendum vitae non tortor.

[intense_row padding_top="0"]
[intense_column size="6"]
[intense_icon_list]
[intense_icon_list_item  type="ok" size="1" color="#1a8be2"]
Etiam eget nunc magna.
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="1" color="#1a8be2"]
Donec vestibulum a leo et convallis.
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="1" color="#1a8be2"]
Nulla eu mattis neque.
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="1" color="#1a8be2"]
Praesent ultricies ipsum eget purus scelerisque bibendum.
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="1" color="#1a8be2"]
Curabitur et sollicitudin arcu.
[/intense_icon_list_item]
[/intense_icon_list]
[/intense_column]
[intense_column size="6"]
[intense_testimonies type="slider" template="quote_bubble"]
[intense_testimony author="John Doe" company="Intense Visions" image="http://1.gravatar.com/avatar/929b8350708873d9e87c9dbd164add5d?s=50&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D50&amp;r=G"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ultrices tempor tristique. Curabitur convallis fermentum ante rutrum viverra. Aliquam pharetra, risus vel mattis luctus, tortor nibh pulvinar est, ac venenatis nunc justo a nulla. Nam enim arcu, consectetur eget vestibulum.
[/intense_testimony]
[intense_testimony author="Jane Doe" company="Intense Visions" link="http://intensity.intensevisions.com" ]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vulputate metus id dolor ornare vel ullamcorper augue ultrices. Ut posuere arcu eu velit cursus id ultricies purus condimentum. Duis sagittis, sem a dictum porttitor, urna lacus rutrum lectus, quis rutrum.[/intense_testimony]
[/intense_testimonies]
[/intense_column]
[/intense_row]

Nam aliquam non tellus a porttitor. Mauris fermentum at turpis vel varius. Proin eget nunc erat. Praesent placerat lacinia leo non consectetur. Nunc nec lacinia sapien. Etiam mollis massa eget elementum vehicula. Mauris sed porttitor mauris. Proin laoreet arcu non lorem scelerisque congue. Aliquam malesuada eu lectus at ultrices. Fusce molestie viverra tempus. Suspendisse tincidunt laoreet dapibus. Aenean bibendum lorem et commodo congue. Maecenas convallis mi felis, eget interdum ipsum blandit in. Integer iaculis imperdiet vehicula. Nulla pharetra, tortor sed accumsan mollis, enim turpis gravida purus, non ullamcorper felis leo ac diam. Maecenas ut elementum nisi.

[intense_content_section size="fullboxed" image="8279" imagesize="medium800" imagemode="full" height="300" padding_bottom="10"]

[/intense_content_section]

Nam rutrum imperdiet nisi at tristique. Proin a sagittis dui, at aliquam elit. Curabitur gravida justo eget dui elementum accumsan. Aenean id quam libero. Maecenas lacinia dapibus velit. Morbi a auctor libero. Cras non lacus gravida, sodales nunc a, gravida leo. Suspendisse in massa nec odio consectetur porta id nec ante. Cras fringilla massa odio. Donec aliquam felis sem, ut vestibulum elit mollis at. Suspendisse potenti. Fusce pellentesque metus sed tristique pretium. Etiam dignissim egestas urna ac tempor. Nam sagittis venenatis leo at venenatis.

[intense_promo_box button_text="Order Now" button_link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions" button_type="primary" button_position="right" button_size="large" size="medium" background_color="muted" color="#333" shadow="8"]
[intense_lead]Mauris accumsan tellus ligula, in dignissim.[/intense_lead] Fusce ultrices eleifend gravida. Nunc lobortis nibh et fringilla semper. Aliquam vehicula libero a massa rutrum placerat. Curabitur adipiscing lorem ac elementum dignissim. Curabitur dictum malesuada euismod. Mauris sit amet iaculis massa, in elementum nunc. [/intense_promo_box]