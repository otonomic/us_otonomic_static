<?php
/*
Intense Snippet Name: Office Building Concept

*/
?>

[intense_lead]Vestibulum vestibulum risus vulputate, vestibulum dolor vel, fringilla turpis. Donec fringilla blandit urna, et condimentum libero fringilla eu. In hac habitasse platea dictumst.[/intense_lead]

[intense_gallery size="square400" columns="2" marginpercent=".25" type="colorbox" effeckt="2" effeckt_color="#000000" effeckt_opacity="80" grouping="photos" id="8300" include="8330,8331,8332,8334" order="NONE" /]

Integer fermentum porttitor diam vitae bibendum. Aliquam hendrerit fermentum nunc vel euismod. In imperdiet urna a ullamcorper rhoncus. Nullam mattis iaculis leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer lorem velit, accumsan sit amet aliquam quis, auctor eu diam. Pellentesque nec tempus diam. Integer ut massa ut lectus rhoncus bibendum.

[intense_content_section size="fullboxed" image="8329" imagesize="medium800" imagemode="full" height="300" padding_bottom="10"]

[/intense_content_section]

Cras convallis nibh in sapien convallis, et pretium diam congue. Fusce non accumsan sem. Vivamus vel leo pulvinar, scelerisque tortor a, facilisis mi.

Morbi ultricies ipsum vitae dui sagittis rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis molestie diam at nisl fermentum, ullamcorper fermentum ante mattis. Morbi quis sapien quis lacus malesuada hendrerit.

[intense_gallery size="square400" columns="2" marginpercent=".25" type="colorbox" effeckt="2" effeckt_color="#000000" effeckt_opacity="80" grouping="photos" id="8280" include="8335,8336,8337,8338" order="NONE" /]

Fusce non nibh quis magna dapibus hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam rhoncus orci quam, at faucibus ligula condimentum lobortis. Maecenas eros nisi, ultricies a accumsan vitae, dapibus eu quam. Vivamus id dictum ligula. Mauris non dui sem. Aliquam erat volutpat.