<?php
/*
Intense Snippet Name: Interior Design
*/
?>

[intense_lead]In commodo nulla quis mauris condimentum interdum. In vehicula consequat tellus ac auctor. Maecenas tincidunt, sem eget laoreet tempus, est nisi luctus erat, nec vestibulum libero metus iaculis purus. Vestibulum laoreet, metus sed ultricies tincidunt, mauris nisl vulputate ipsum, ac pulvinar velit ante ac elit.[/intense_lead]

[intense_row padding_bottom="10"]
[intense_column size="6"]
[intense_image image="8395" size="medium800" title="" /]
[/intense_column]
[intense_column size="6"]
[intense_image image="8392" size="medium800" title="" /]
[/intense_column]
[/intense_row]

Aliquam nec odio vitae tellus blandit fringilla. Aenean semper odio eget ipsum hendrerit luctus. Nunc porta sed purus sed bibendum. Aliquam porta dolor metus, quis iaculis dui commodo a. Nam sagittis commodo odio vitae tristique. Proin enim nibh, volutpat sed eros eu, adipiscing adipiscing odio. 

Duis sed purus lectus. Nulla elit turpis, blandit et elementum et, eleifend eget nulla. Suspendisse placerat, quam id commodo auctor, purus risus placerat leo, quis auctor velit nunc vel urna. Sed vestibulum elit ac sapien adipiscing condimentum.

[intense_content_section size="fullboxed" image="8396" imagesize="medium800" imagemode="full" height="300" margin_bottom="10"]

[/intense_content_section]

Vivamus non urna a odio lacinia vulputate. Nunc egestas commodo dapibus. Donec bibendum ligula ut nibh eleifend, eget ultricies odio aliquam. Praesent nibh lorem, pharetra in diam a, dapibus laoreet dui. Duis ultrices, leo id eleifend pellentesque, justo tortor tincidunt eros, a placerat leo nulla sed turpis.

[intense_row padding_top="0" padding_bottom="10"]
[intense_column size="6"]
[intense_image image="8393" size="medium800" title="" /]
[/intense_column]
[intense_column size="6"]
[intense_image image="8394" size="medium800" title="" /]
[/intense_column]
[/intense_row]

Vestibulum laoreet non ipsum ut euismod. In venenatis laoreet libero, id ullamcorper leo molestie sit amet. Mauris at faucibus tortor. Etiam vel malesuada quam.