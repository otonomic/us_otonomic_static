<?php
/*
Intense Snippet Name: Rockable Press
*/
?>

[intense_lead]Duis eleifend magna quis dictum pretium. Fusce metus ligula, pretium nec neque a, semper consectetur ipsum. Integer sed fringilla risus. Proin magna ipsum, vestibulum eu condimentum adipiscing, consectetur et felis. Curabitur varius porttitor augue, sit amet vulputate velit tincidunt quis. Ut aliquam nisi ac mi elementum, vel fringilla lectus dapibus.[/intense_lead]

Curabitur non luctus nibh. Fusce eget purus est. Maecenas rutrum quam tellus, non egestas eros iaculis at. Phasellus et euismod mi. Aliquam at magna tincidunt, ullamcorper quam vel, ullamcorper est. Curabitur eu elit congue, cursus diam venenatis, dapibus massa. Nam suscipit vulputate sollicitudin. Proin sed viverra risus. Cras eu placerat felis. Mauris molestie tempor libero at vulputate.

<h2>Nunc placerat vehicula</h2>

Nulla ultrices metus eros. Nunc suscipit massa ac nunc tempus, feugiat congue dolor laoreet. Integer convallis adipiscing turpis nec aliquam. Mauris ultricies vel lectus ac sagittis. Ut facilisis justo eget justo porttitor, in pretium lorem tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In bibendum convallis justo, nec sollicitudin est molestie sodales. Praesent ultrices ac orci eu mattis. Pellentesque aliquam magna vel ante ultricies, sit amet adipiscing mauris consequat.

[intense_content_section size="fullboxed" image="8264" imagesize="medium800" imagemode="full" height="300" padding_bottom="10"]

[/intense_content_section]

Proin dignissim tincidunt cursus. Donec consectetur nec metus cursus varius. In at mi at arcu fringilla suscipit in ac tortor. Integer elementum eget sem in volutpat. Sed nunc neque, gravida at luctus et, posuere nec eros. Quisque venenatis ligula placerat, ornare odio non, feugiat turpis. Nulla nibh nulla, aliquet sit amet lobortis et, luctus vel ante. Integer semper ullamcorper sodales.

Sed pellentesque consectetur dui, sit amet consequat lorem egestas eu. Sed id accumsan dolor. Vestibulum sapien urna, euismod sit amet nulla vel, pharetra malesuada quam. Sed rhoncus vehicula lorem tristique iaculis. Morbi congue quam elit, sed vestibulum lectus elementum et. Nullam elementum placerat ornare. Sed mattis pretium dapibus.

[intense_row padding_top="0"]
[intense_column size="4"]
[intense_animated type="fadeInUp" trigger="scroll" scroll_percentage="10"]
[intense_image image="8263" size="medium500" shadow="10" /]
[/intense_animated]
[/intense_column]
[intense_column size="4"]
[intense_animated type="fadeInUp" trigger="scroll" scroll_percentage="10"]
[intense_image image="8266" size="medium500" shadow="10" /]
[/intense_animated]
[/intense_column]
[intense_column size="4"]
[intense_animated type="fadeInUp" trigger="scroll" scroll_percentage="10"]
[intense_image image="8265" size="medium500" shadow="10" /]
[/intense_animated]
[/intense_column]
[/intense_row]

[intense_row padding_top="0"]
[intense_column size="12"]
Donec a imperdiet velit. Nulla facilisi. Nullam elementum ante dolor, sit amet placerat tellus tincidunt nec. Nulla facilisi. Nam sit amet malesuada velit. Mauris sapien dolor, porta eget semper eget, egestas et eros. 

Donec non diam scelerisque, ultrices est placerat, lacinia sapien. Nam in enim vitae purus vehicula facilisis nec eu mauris. Fusce vitae quam accumsan, vulputate augue vitae, sollicitudin sapien. Fusce sollicitudin libero vitae urna rutrum tincidunt. Aenean hendrerit suscipit bibendum.

Maecenas tempor neque vitae risus aliquam, a aliquet erat interdum. Duis est enim, hendrerit non tincidunt quis, laoreet at massa.
[/intense_column]
[/intense_row]

<h2>Etiam pharetra hendrerit dapibus</h2>

Nulla hendrerit, nunc eu gravida imperdiet, nisi dolor venenatis nisi, a fringilla libero risus sed nulla. Sed dapibus elementum aliquam. Praesent commodo aliquet turpis a dapibus. Nullam hendrerit quis massa at pretium. Donec tincidunt turpis a venenatis tempus. Quisque ac consectetur libero. Aenean eleifend nisi id sollicitudin egestas.

[intense_content_section size="fullboxed" image="8277" imagesize="medium800" imagemode="full" height="300" padding_bottom="10"]

[/intense_content_section]

Donec vehicula, velit quis pretium venenatis, nisi risus vestibulum erat, sed vehicula lectus mi porttitor erat. Nunc suscipit sagittis odio vel porta. Curabitur nec dolor sit amet diam molestie vestibulum at eget nisi. Vestibulum ornare mauris id elit hendrerit, id fermentum lectus egestas. Mauris cursus lorem ac iaculis vehicula. Nulla dui leo, aliquet a tristique sit amet, gravida et diam. Proin non lacus vitae sem gravida pretium.