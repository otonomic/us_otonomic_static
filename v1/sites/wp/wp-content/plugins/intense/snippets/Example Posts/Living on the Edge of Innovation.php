<?php
/*
Intense Snippet Name: Living on the Edge of Innovation
*/
?>

[intense_lead]Aliquam pretium quis sem et bibendum. Suspendisse non porttitor augue. Integer fermentum enim nec accumsan dapibus. Suspendisse lacinia ipsum eget neque rhoncus, ac mollis nunc bibendum. Maecenas faucibus diam turpis, sed auctor nibh laoreet sed. Nulla ornare venenatis urna eu viverra. Proin orci eros, tincidunt sit amet cursus sit amet, rutrum nec tortor. Praesent iaculis mi in mauris dignissim tempus. Morbi metus arcu, ornare ac lorem ac, placerat placerat lectus.[/intense_lead]

[intense_gallery  size="square75" marginpercent="1" type="prettyphoto" include="7083,5440,5439,5438,5443,7079,7082,5442,5441" /]

Nulla eu sodales odio. Proin dapibus quis lorem sit amet hendrerit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum mollis nunc non quam blandit dictum. Integer blandit, augue id volutpat dignissim, enim felis tempor nulla, nec condimentum dui velit id est. Curabitur arcu dolor, ultricies sit amet nisl a, fermentum mollis felis. Proin venenatis dignissim leo, at feugiat ante consequat non. Sed et nisl porttitor, egestas odio sit amet, placerat dui. Aliquam varius feugiat lorem eget ullamcorper.

Nulla fermentum fringilla leo, vitae aliquam mauris aliquet eu. <strong>Donec vel auctor dui, in adipiscing dolor. Sed quis egestas felis, eget tincidunt libero.</strong> Nullam pellentesque enim libero, venenatis laoreet elit rutrum eu. Integer convallis, odio ac congue tempor, orci turpis varius eros, aliquet ultrices libero est tristique tortor. Nulla vitae libero quis lacus fringilla interdum. Sed urna neque, gravida id fringilla non, tempor nec tellus. Praesent ut blandit justo. Suspendisse tellus nulla, convallis id ultricies in, semper ut purus. Donec egestas ante eu nulla mattis cursus.

Phasellus quis sem semper, molestie velit id, commodo est. Aenean eu tortor nec justo porttitor facilisis eget aliquet nunc. Nunc neque ante, ultrices in tortor et, vulputate placerat neque. Donec fermentum eu nulla dictum elementum. Nam ut varius massa. <strong>Mauris a lorem eu enim porttitor lacinia. Donec ut eleifend enim.</strong> Fusce lacinia neque libero, lacinia commodo nunc elementum sodales. Vestibulum sapien erat, sagittis non elit quis, euismod blandit augue. Duis pretium semper lacus, quis sollicitudin ante elementum eget. Suspendisse molestie congue euismod.

<h2>Curabitur at Erat Auctor, Pretium Libero at, Porttitor Lacus</h2>

Integer faucibus magna sagittis arcu ullamcorper sodales. Duis aliquam leo eu nisi gravida blandit. Pellentesque eget feugiat risus, in pretium lorem. In hac habitasse platea dictumst. Nulla sit amet tristique metus. Proin vestibulum bibendum nisl in iaculis. Mauris eget faucibus felis, ac ultricies nulla. Nam tellus elit, suscipit at sagittis non, ornare sit amet arcu.

[intense_slider shadow="8"]
[intense_slide]
[intense_image image="7472" size="postWide" title="Calendar" /]
[/intense_slide]
[intense_slide]
[intense_image image="7459" size="postWide" title="book" /]
[/intense_slide]
[intense_slide]
[intense_image image="7457" size="postWide" title="record" /]
[/intense_slide]
[/intense_slider]

Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sed vulputate arcu. Nam tempus fringilla faucibus. Donec ullamcorper tortor risus, nec aliquam ipsum lacinia in. Praesent interdum sem ligula, in porta tellus suscipit nec. Duis sed hendrerit nisl. Sed accumsan ante id risus auctor mollis. Sed varius urna non est varius sodales. Fusce vehicula, lectus sit amet consequat feugiat, quam velit placerat turpis, vel sagittis erat justo a justo. Nulla dolor felis, aliquet sed ultrices eget, placerat sed lacus. Etiam ullamcorper sit amet nisi in hendrerit. Sed eget diam volutpat, blandit arcu quis, ultrices metus. Ut viverra, augue at eleifend luctus, leo turpis mattis ligula, nec laoreet neque diam et mi. Ut iaculis lorem id metus ornare, ut porta nisi porta. Nunc commodo, dolor convallis condimentum tristique, mauris nunc volutpat sem, porttitor venenatis nisi felis ac quam.

[intense_image image="8105" size="postWide" title="Call Center Operator" shadow="8" /]

<h3>Praesent Volutpat</h3>
Morbi blandit turpis nisi, id dapibus lectus bibendum eu. Praesent eleifend urna mauris, id tempor lacus eleifend non. Nullam tincidunt lacus in augue condimentum, eget suscipit lorem mollis. Praesent interdum eget erat sed elementum. Donec nisi nisl, varius dapibus bibendum vitae, hendrerit eget turpis. Sed ipsum ligula, hendrerit sit amet euismod vitae, pulvinar ut felis. Mauris sagittis iaculis justo, in eleifend massa molestie nec. Vivamus eu magna vitae metus posuere euismod. Mauris luctus quam at nisl ullamcorper pharetra. Duis lobortis dictum quam, quis molestie leo aliquam sit amet. Maecenas scelerisque dui vitae metus ultricies, at imperdiet urna posuere. Vestibulum euismod suscipit urna quis porta. Nam rhoncus augue ut tellus consectetur tincidunt. Praesent non elit augue. Aliquam ornare eleifend nisi, a tincidunt purus porttitor id.

Aliquam congue molestie iaculis. Vestibulum feugiat dolor turpis, eget sodales augue convallis ut. In scelerisque tempor accumsan. Aliquam odio leo, hendrerit et facilisis id, iaculis sit amet lorem. Duis laoreet ante velit, in porta neque pretium quis. Quisque sollicitudin, magna ut tempus tristique, libero augue eleifend sem, quis hendrerit enim nisi id justo. Praesent volutpat consequat eros fringilla porta. Cras gravida leo sit amet leo rutrum vehicula. Vestibulum felis sapien, faucibus eget metus eu, ullamcorper ullamcorper sapien. Mauris eu magna libero. Duis ac ligula ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

[intense_image image="5467" size="postWide" title="Boats" shadow="8" /]

<h3>Consectetur tincidunt</h3>
Maecenas ut est diam. Maecenas lacinia molestie risus vel ultricies. Donec vestibulum, purus non consectetur pharetra, tortor libero convallis ligula, sed sodales neque risus vel diam. Aliquam ac sem nunc. Donec scelerisque in magna et pharetra. Aliquam erat volutpat. Duis ligula ante, consequat ac viverra nec, laoreet nec eros.

Vivamus vestibulum tempus consequat. Integer iaculis quam ac lacus condimentum, id convallis risus viverra. Sed eu nulla nisi. Quisque scelerisque tellus diam. Maecenas eu erat vitae est malesuada blandit. Proin eu vehicula nunc, id congue ligula. Vivamus mauris leo, sodales non aliquam quis, volutpat eget diam.