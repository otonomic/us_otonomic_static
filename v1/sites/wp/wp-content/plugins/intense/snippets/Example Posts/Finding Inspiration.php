<?php
/*
Intense Snippet Name: Finding Inspiration
*/
?>

[intense_lead]Mauris eu lectus quis nunc imperdiet laoreet eu vitae lorem. Pellentesque sagittis tristique quam in condimentum. Duis bibendum luctus egestas. Proin felis nibh, convallis eget posuere nec, varius in arcu. Cras lacinia euismod felis, sit amet malesuada nulla scelerisque sit amet. Fusce elementum ut elit vitae sodales. Morbi nec urna sed nunc pretium molestie.[/intense_lead]
<h2>Etiam adipiscing</h2>
[intense_row padding_bottom="30"]
[intense_column size="3"]
[intense_chart type="pie" width="200" height="200" color="#333" animation="1" animation_steps="60" animation_easing="linear"]
[intense_chart_data values="45,35,10,5,5" colors="#69D2E7,#FA6900,#A7DBD8,#E0E4CC,#F38630" /]
[/intense_chart]
[/intense_column]
[intense_column size="9"]
[intense_icon_list]
[intense_icon_list_item type="sign-blank" size="2" color="#69d2e7"]
45% Fusce blandit nisi vitae consequat sagittis.
[/intense_icon_list_item]
[intense_icon_list_item type="sign-blank" size="2" color="#FA6900"]
35% Proin quam massa, vehicula et iaculis imperdiet.
[/intense_icon_list_item]
[intense_icon_list_item type="sign-blank" size="2" color="#A7DBD8"]
10% Sed suscipit scelerisque ligula, in interdum mi cursus ut.
[/intense_icon_list_item]
[intense_icon_list_item type="sign-blank" size="2" color="#E0E4CC"]
5% Curabitur mollis turpis augue.
[/intense_icon_list_item]
[intense_icon_list_item type="sign-blank" size="2" color="#F38630"]
5% Vivamus malesuada tincidunt pharetra.
[/intense_icon_list_item]
[/intense_icon_list]
[/intense_column]
[/intense_row]

Etiam adipiscing nunc lectus, ac gravida nulla tincidunt ut. Suspendisse at elit eros. Ut pulvinar id leo id malesuada. Vivamus volutpat facilisis lorem id porttitor. Etiam scelerisque magna sed eros varius, nec porttitor quam ultrices. <strong>Mauris nec diam nulla.</strong> Proin vel pellentesque magna. Integer sagittis porttitor rhoncus.

Aenean pellentesque porta erat, ut condimentum eros tempor quis. Nullam at augue ut ante luctus aliquam. Sed sodales libero ipsum, id iaculis turpis venenatis a. Sed laoreet mi eu ante molestie feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris mattis, dolor sit amet hendrerit sodales, libero dolor fringilla mauris, vitae convallis dolor sem eget elit. Donec mattis nulla sed condimentum tempor. Sed sed scelerisque dolor, quis suscipit arcu. Duis ac massa lectus. <strong>Phasellus varius lorem ac massa gravida, eget condimentum nisl sollicitudin. Etiam iaculis, metus ut bibendum consectetur, ante nibh consequat dolor, vel dapibus leo lorem eget elit. Phasellus non tristique erat.</strong> Praesent suscipit mi sit amet augue faucibus tristique. Vivamus malesuada ornare massa ut hendrerit. Cras felis enim, elementum id rutrum convallis, commodo a nisl.

Vivamus vestibulum erat vitae nisi aliquam suscipit. Sed orci lectus, sagittis at fringilla vel, facilisis a neque. Nunc luctus rhoncus nisi, mollis sollicitudin nisl dapibus id. Suspendisse molestie tristique posuere. Etiam sagittis massa id est adipiscing, in scelerisque sapien feugiat. Integer at semper tellus, id lacinia tortor. Mauris lorem sapien, mollis in feugiat non, hendrerit consequat massa. Sed blandit dictum odio, quis tempus ligula porttitor ac. Donec sed varius enim. Suspendisse interdum nulla a quam cursus interdum. Integer eros est, mattis ac malesuada in, pulvinar vitae diam. Etiam et risus eget sem mattis lobortis non a lorem.
<h2>Vivamus tempor</h2>
[intense_image image="8314" size="postWide" shadow="5" title="In purus diam" caption="Proin at arcu enim. Donec ut placerat ante. Mauris eget diam facilisis, tincidunt dolor at, egestas nibh. Integer bibendum felis eu dolor pretium, vel suscipit nunc accumsan. Cras sollicitudin commodo scelerisque. Pellentesque tempor nisl ut consectetur pretium. Etiam euismod leo quis blandit sollicitudin. Phasellus elementum libero at eros facilisis pharetra. Etiam sodales arcu id nulla porta suscipit. Aliquam suscipit gravida porta. Proin aliquam sit amet tortor vel sollicitudin." effeckt="3" effecktcolor="#000000" effecktopacity="80" /]

Sed at sem eu lacus condimentum tristique. Etiam dignissim fringilla accumsan. In lacus turpis, eleifend sed laoreet quis, mollis et arcu. Nunc purus quam, hendrerit non euismod sit amet, aliquam ut magna. Quisque porta scelerisque mattis. Sed ultricies lectus dignissim pellentesque luctus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fermentum ante pulvinar neque vehicula accumsan. Etiam laoreet libero justo, vitae congue dolor consectetur ac. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec imperdiet, est quis imperdiet placerat, libero lacus fermentum dui, eget lacinia leo ipsum eget urna. Nunc ac sodales est, quis bibendum diam.

In ut nibh et dui ullamcorper gravida in at augue. Aenean quis quam vel erat consectetur faucibus. Suspendisse potenti. Aenean tincidunt ac nunc in aliquet. Vestibulum non condimentum felis. Pellentesque ac sapien dolor. Etiam in mi mi. Vestibulum rhoncus, sem quis aliquam scelerisque, massa nisi pharetra nunc, quis porttitor libero dui ut sapien.
<h3>Quisque magna neque</h3>
<h4>Ullamcorper ac ante vitae</h4>
Quisque magna neque, ullamcorper ac ante vitae, blandit mollis quam. Sed lacinia, risus a tempus tempus, leo eros varius quam, et varius quam tellus sed nisi. Praesent vel augue sit amet ipsum gravida scelerisque. Fusce lobortis ut erat id consectetur. Ut quis accumsan orci. Phasellus cursus turpis vitae neque pretium convallis. Mauris at dolor ac erat pharetra lobortis. Nunc porta est quis sapien volutpat, eu posuere dolor posuere. Duis volutpat, velit a aliquet consequat, leo nibh dictum ante, id volutpat purus dui eu dolor.
<h4>Donec eu diam turpis</h4>
Donec eu diam turpis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum et tortor sodales, molestie elit quis, blandit lacus. Etiam feugiat non augue ac ultricies. Suspendisse potenti. Maecenas auctor, enim sed consectetur ullamcorper, lorem lorem aliquet nulla, ac ultricies ligula purus eget turpis. Ut ac mi blandit, consectetur sapien a, dictum libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
<h3>Proin varius nisi sit</h3>
[intense_chart type="line" width="800" height="200" color="#333" animation="1" animation_steps="60" animation_easing="linear" labels="Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday"]
[intense_chart_data fill_color="#69D2E7" stroke_color="#A7DBD8" point_color="#FA6900" point_stroke_color="#F38630" values="50, 100, 75, 45, 30, 40,80,90" /]
[/intense_chart]

Proin varius nisi sit amet elit semper iaculis. Etiam laoreet congue commodo. Nunc dictum turpis at convallis blandit. Fusce eu pharetra nisi. Donec nisi sapien, elementum eu purus commodo, lobortis adipiscing odio. In lectus dolor, tempus non justo eget, varius congue leo. Cras non erat nec eros tempus venenatis in id tellus. Quisque rhoncus odio arcu, bibendum consequat justo gravida a.
<h2>Phasellus a mauris</h2>
[intense_image image="8313" size="postWide" title="A Long Walk" caption="Duis nisi diam, pharetra quis commodo a, auctor eu dolor." effeckt="5" effecktcolor="#000000" effecktopacity="80" shadow="5" /]

Phasellus a mauris ac quam lobortis vulputate. Donec imperdiet quam eros. Praesent a nibh sed ligula blandit pretium in et nisl. Mauris ipsum dolor, facilisis sed ornare nec, varius id sem. In egestas augue ut metus interdum feugiat. Sed ut semper lectus. In eget bibendum dui. Nulla aliquet ipsum a dui pulvinar, in consectetur nulla rhoncus. Aenean purus ligula, adipiscing id mi sed, commodo tincidunt purus. Fusce hendrerit vestibulum ligula in fringilla. Duis ante ipsum, facilisis vitae lorem eu, accumsan vehicula tellus. Etiam a euismod felis, quis ornare tellus. Morbi mauris est, ullamcorper sit amet tortor dapibus, sollicitudin accumsan augue. Sed tempus, augue non ullamcorper commodo, sem ipsum malesuada nisi, sed ultricies erat risus at eros.

Sed sed metus et lorem laoreet fringilla in vel sem. Quisque vitae turpis sed neque adipiscing egestas. Proin nunc justo, scelerisque sed arcu sit amet, auctor dignissim tellus. Vestibulum tincidunt eros augue, ac fringilla nisi euismod sed. Aliquam convallis et lectus in pulvinar. Mauris lobortis, diam at consequat dapibus, velit quam accumsan lacus, ut vulputate enim augue sit amet nisi. Curabitur varius risus sed arcu varius dapibus. Fusce quis accumsan mauris, id egestas lorem. Nunc enim eros, ullamcorper ut lobortis id, porta vitae odio. Nunc elementum lacus pulvinar tincidunt molestie. Pellentesque gravida felis orci, fermentum tempus ipsum porta a. Fusce lacus nibh, dignissim eu erat sit amet, ultrices iaculis eros. Proin quis sapien id massa commodo aliquet.