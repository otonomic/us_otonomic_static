<?php
/*
Intense Snippet Name: Unleash the Code
*/
?>

[intense_lead]Aliquam blandit varius elit sed tincidunt. Praesent a fermentum odio, eget eleifend erat. Maecenas quis neque varius, adipiscing ligula et, fermentum sem. Fusce ut tortor non est viverra sollicitudin eu nec orci. Sed eu justo mi. Sed auctor augue in dui consequat congue ac nec diam. Pellentesque vel volutpat velit.[/intense_lead]

Etiam quis tellus a justo molestie faucibus pretium et mi. Proin sed pellentesque enim, id pharetra felis. Quisque sodales enim justo, at faucibus justo interdum sit amet. Cras consequat vitae purus sit amet interdum. Nulla imperdiet metus et tincidunt feugiat. Vivamus mauris leo, elementum sit amet lectus eu, sodales tristique est. Cras porta consequat pharetra. Integer vitae dui quis ligula hendrerit euismod. Maecenas pulvinar nisi vitae enim pulvinar, at faucibus nibh varius. Pellentesque dictum et eros non tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.

Fusce leo urna, imperdiet nec venenatis in, imperdiet ut nibh. In ultrices nibh odio, sit amet lobortis nulla eleifend ac. Suspendisse eget tincidunt neque. Sed vitae sem sed augue fringilla placerat venenatis quis massa. Sed in odio id turpis congue iaculis. Donec quis tellus suscipit leo faucibus iaculis. Vestibulum et turpis in nisl ultrices egestas sit amet sed neque. Fusce sed eros nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum consequat diam vitae nulla congue pellentesque. Cras id sem elit. Phasellus vehicula mauris sit amet sapien dignissim, a sagittis lacus molestie.

Praesent neque erat, rutrum ac ipsum eget, volutpat consequat neque. Sed at tincidunt est. Nullam sodales dui sit amet dui dapibus, eget molestie dolor semper. Nullam ut libero id lectus convallis condimentum. Praesent in massa nisl. Vivamus sed est sed eros pellentesque volutpat. Praesent bibendum ante nunc, sit amet tempor neque pretium vel. Aenean et dignissim diam, a euismod mi. Cras neque nisl, malesuada id ipsum et, accumsan aliquam velit. Proin ut mi tempor, porta enim id, blandit elit. Integer dolor sapien, rutrum ut purus nec, adipiscing posuere lorem. Pellentesque in posuere orci. Donec dui nisi, porta ut lacus eget, ullamcorper condimentum ipsum.

Pellentesque non tempus sem. Suspendisse sed mattis ligula, vitae mattis justo. Etiam vitae quam a felis feugiat semper. Nullam vitae arcu sem. Morbi faucibus orci vel purus mattis tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed vulputate ullamcorper elementum. Nam ut mauris orci. Nulla porttitor purus lorem, sit amet sodales quam hendrerit nec. Curabitur rhoncus, odio ac pharetra suscipit, tellus eros congue turpis, vitae feugiat justo odio sed augue. Integer eget rutrum diam. Sed tortor odio, aliquet ut ornare et, luctus eget arcu. Donec id aliquet ligula, non dapibus erat. Sed dignissim, metus ac fringilla tempus, eros dui tempus lorem, at varius enim tortor non sem. Proin nec faucibus justo, non consequat risus. Sed volutpat pharetra auctor.

[intense_hr shadow="1" /]

<h2>Sed eu justo mi</h2>

Etiam dapibus leo eu elit semper dapibus. Curabitur varius nec libero vitae dignissim. Suspendisse nec diam nisl. Quisque sit amet urna ac enim molestie imperdiet. Mauris dapibus libero et metus aliquam faucibus. Duis magna lorem, dignissim eu tempus et, convallis ut est. Morbi pulvinar dui sed consectetur tempor. Nullam venenatis massa fermentum elit hendrerit convallis. Fusce eleifend euismod tellus eget porttitor.

Mauris porta vehicula urna sed dapibus. Donec neque urna, ullamcorper et feugiat non, pharetra semper eros. Vivamus et placerat turpis. In hac habitasse platea dictumst. Ut gravida vitae sapien vel varius. Etiam a turpis imperdiet turpis bibendum mollis. Proin a lectus vestibulum, pellentesque ipsum quis, ullamcorper urna. Fusce sagittis, mi eget dictum tristique, risus magna egestas enim, ut lacinia velit lectus posuere metus. Integer auctor est ac lorem suscipit mattis. Vestibulum a scelerisque purus, at faucibus mi. Duis ultricies mi a elementum vehicula. Praesent laoreet tempor venenatis. Morbi a hendrerit erat, vel varius orci. Aenean vulputate faucibus faucibus. Praesent lobortis, metus at rhoncus gravida, sem sapien aliquam neque, feugiat pharetra nunc velit a mi. Morbi elementum leo ut metus fermentum accumsan.

Phasellus vel malesuada libero. Curabitur faucibus orci magna, sed sodales ante laoreet et. Vestibulum eget lectus et justo bibendum feugiat at quis neque. Mauris malesuada semper mollis. Pellentesque pharetra tellus a lectus rutrum placerat. Phasellus vulputate eros a risus posuere hendrerit. Nulla ac ligula in augue vehicula bibendum ut nec mauris. Etiam eget faucibus dolor. In vitae sapien mauris. Sed in dui in felis sollicitudin rutrum quis ac elit. Etiam faucibus convallis nisl, quis ultricies tortor hendrerit in. Aenean dolor velit, venenatis nec quam ut, vehicula rutrum felis. Cras blandit sem vitae vehicula posuere. Nullam orci orci, dignissim nec placerat eu, sollicitudin et risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

[intense_promo_box button_text="Buy Now" button_link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions" button_type="primary" button_position="right" button_size="large" size="medium" background_color="" color="#333" shadow="0" border_left="5px solid #606060"]
<h1>Sed pharetra eros ipsum, sed euismod ante viverra ac.</h1>
Maecenas rhoncus urna mauris. Pellentesque id dui ligula. Sed at auctor dui, quis vehicula est. Ut dolor tortor, sollicitudin vitae dui tincidunt, suscipit venenatis nisl. Mauris bibendum, mi vitae rhoncus pulvinar, leo leo facilisis ante, eu dignissim sem mi eget sapien. Integer vitae quam laoreet, consectetur est eu, imperdiet nunc. Vestibulum aliquet mauris leo, quis molestie felis gravida ut. Praesent et tincidunt nunc.
[/intense_promo_box]

<h2>Vestibulum aliquet mauris</h2>
Mauris et velit mattis, aliquam ipsum mollis, porta quam. Maecenas sem lorem, egestas eu dapibus non, imperdiet sed nunc. Phasellus vel ullamcorper neque. Cras purus risus, ornare quis gravida vel, interdum ut ipsum. Morbi ipsum tellus, tincidunt volutpat pellentesque vel, molestie at lectus. Donec condimentum venenatis quam, eu tristique erat cursus sit amet. Etiam sit amet neque ut urna convallis convallis. Etiam viverra orci rutrum nisl consectetur elementum. Nunc aliquam eros mauris, eu rutrum velit pellentesque et. Phasellus et orci sit amet sem porttitor lobortis. Nunc posuere tincidunt justo, at pharetra ipsum cursus ut. Aliquam accumsan felis tortor, sed venenatis nibh aliquet adipiscing. Nam pharetra libero risus, at adipiscing urna rhoncus at. Curabitur blandit velit tristique dui rhoncus euismod.

Ut malesuada mauris ante, nec vulputate risus posuere quis. Nullam tincidunt tellus sed nisl varius rutrum. Aenean elementum rhoncus facilisis. Vivamus nec orci leo. Duis id metus ac libero convallis pulvinar. Aenean euismod nibh in dolor porttitor auctor. Cras porttitor, velit quis ornare dictum, est mi pretium arcu, nec mollis elit orci et sapien. Morbi euismod nibh libero, id commodo dui adipiscing vitae. Phasellus id arcu vestibulum quam sagittis rutrum at vitae lectus. Cras condimentum diam eu ornare rhoncus. Proin consectetur lacus sem. In ac ultrices odio, non ultricies leo. Aenean ante turpis, blandit eget consequat vel, consequat vitae est. Integer sodales sem facilisis, venenatis augue id, elementum tortor. Sed et blandit lorem. Nulla tempor, diam ac lacinia accumsan, dolor tellus pharetra dolor, quis condimentum erat nisi et justo.

[intense_row]
[intense_column size="6"]
[intense_animated type="bounceInLeft" trigger="scroll" scroll_percentage="90"]
[intense_image image="7469" size="medium500" title="Man" /]
[/intense_animated]
[/intense_column]
[intense_column size="6"]
[intense_animated type="bounceInRight" trigger="scroll" scroll_percentage="90"]
[intense_image image="7790" size="medium500" title="Baseball" /]
[/intense_animated]
[/intense_column]
[/intense_row]

Nunc in tellus magna. Aliquam erat volutpat. Sed fringilla venenatis urna sit amet bibendum. Fusce elementum aliquet sagittis. Sed eget eros congue, porttitor velit non, interdum nulla. Quisque posuere, nisi vel ullamcorper fermentum, ligula eros ultricies ligula, ut sagittis mi elit sodales mi. Vivamus pulvinar massa in dui varius porttitor. In hac habitasse platea dictumst. Duis lacinia felis vel feugiat scelerisque. Morbi vulputate massa at massa dapibus, in volutpat purus bibendum.

[intense_icon_list]
[intense_icon_list_item  type="ok" size="2" color="gray"]
Mauris et laoreet nisl
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="2" color="gray"]
Nunc in sem ac augue pretium auctor
[/intense_icon_list_item]
[intense_icon_list_item  type="ok" size="2" color="gray"]
Aliquam erat volutpat
[/intense_icon_list_item]
[/intense_icon_list]

Nam a auctor nisl. Pellentesque aliquet consequat pellentesque. Mauris et laoreet nisl. Ut quis orci vitae augue feugiat gravida. Fusce eu suscipit eros, quis tempor augue. Donec quis lorem id augue varius vestibulum a vitae risus. Sed ornare diam in lectus porta iaculis. Nunc in sem ac augue pretium auctor. Vivamus eget quam ligula. Suspendisse blandit congue erat et laoreet. Integer et fringilla libero. Donec eget iaculis urna. Vivamus in mauris velit.