<?php
/*
Intense Snippet Name: Beyond the Code
*/
?>

[intense_lead]Nulla auctor libero vitae augue tincidunt viverra. Cras scelerisque id felis eget blandit. Aliquam molestie consectetur fermentum. In hac habitasse platea dictumst. Nam egestas pellentesque consectetur. Cras ut diam risus. Aliquam condimentum eu odio id dictum. Nulla malesuada consequat vestibulum. In velit tellus, blandit eget enim at, varius blandit elit.[/intense_lead]

Etiam nisi ligula, varius eget magna et, tristique sagittis diam. Donec est enim, eleifend quis tempor eget, elementum sed elit. Etiam commodo lorem a arcu gravida, id varius dui mattis. Proin congue ac mauris et bibendum. Praesent nec dapibus dolor. Donec tempor consectetur mattis. Nullam facilisis a dui et ultrices. Aliquam porta ante eu enim ultricies scelerisque. Praesent tempus libero arcu, vel auctor enim sagittis ac.

Donec feugiat fringilla felis eu hendrerit. Integer rhoncus tincidunt enim, ut cursus urna porta et. Donec at ultricies quam. Nam faucibus justo urna, eget mollis urna venenatis in. Pellentesque sagittis, ligula a tincidunt elementum, eros turpis vulputate quam, eu pellentesque tortor sem at eros. Maecenas ante dolor, ornare non rutrum eget, cursus in neque. Cras sit amet lacus diam. Etiam commodo eget erat nec suscipit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis rutrum neque sed varius. Cras facilisis, leo eget convallis porta, erat ante egestas sem, eu dignissim tortor magna ut felis. Integer sit amet tincidunt risus. Maecenas suscipit massa vel risus sagittis, ac vulputate velit porttitor. Integer lacinia porttitor diam nec sagittis. Nulla faucibus dapibus libero et condimentum. Phasellus id nisi nec dui porta pellentesque.

[intense_promo_box button_type="primary" button_position="right" button_size="large" size="large" image="3856" imagemode="repeat" color="#333" shadow="0"]
[intense_row padding_top="0"]
[intense_column size="4"]
<h4>Class aptent taciti sociosqu</h4>
[intense_badge color="success"]Donec[/intense_badge] [intense_badge color="success"]Praesent[/intense_badge] [intense_badge color="success"]Mauris[/intense_badge] 
[/intense_column]
[intense_column size="4"]
<h4>Nam malesuada ultricies pretium</h4>
[intense_badge ]Vivamus[/intense_badge] [intense_badge ]Donec[/intense_badge] [intense_badge ]Vestibulum[/intense_badge]
[/intense_column]
[intense_column size="4"]
<h4>Phasellus ut pellentesque arcu</h4>
[intense_badge color="warning"]Mauris [/intense_badge] [intense_badge color="warning"]Phasellus[/intense_badge] [intense_badge color="warning"]Maecenas[/intense_badge]
[/intense_column]
[/intense_row]
[/intense_promo_box]

<h2>Class aptent taciti sociosqu ad litora</h2>
[intense_image image="8320" size="postWide" title="Rain" caption="Donec at ultricies quam. " /]

[intense_row]
[intense_column size="2"]
[intense_label color="success"]Donec[/intense_label]
[/intense_column]
[intense_column size="10"]
Maecenas congue, ligula at sollicitudin porttitor, ante mauris porta neque, ut aliquam dolor velit at ante. Ut egestas fermentum mi sit amet elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed id ipsum leo. Nam suscipit auctor sem et sagittis. Pellentesque tellus justo, tristique sit amet pulvinar tempus, condimentum ac neque. Phasellus ornare dui velit, vitae mollis justo lobortis vel. Maecenas sodales massa ut tincidunt fermentum. Donec eget massa aliquam, adipiscing leo et, ornare lacus. Nam porta facilisis augue id congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dapibus felis a tempus luctus. Mauris vel mattis augue. Cras in lacinia leo. Vivamus sit amet ligula sit amet magna adipiscing tincidunt ut eget metus.

Maecenas vel molestie lectus. Donec vitae iaculis tortor. Morbi placerat, nisl pretium egestas pharetra, urna turpis sagittis mauris, in molestie diam enim eu elit. Etiam id tortor turpis. In vulputate enim elit, et viverra libero feugiat eu. Curabitur aliquet diam lorem, non consequat sapien rhoncus ac. Sed posuere mauris quis sapien hendrerit bibendum. Quisque tincidunt laoreet est eu convallis. Maecenas id ligula mauris. Mauris ac ipsum posuere, egestas erat sed, tempus massa. Nullam pellentesque mattis dolor, condimentum fermentum nulla rhoncus in. Ut ut orci dictum, porttitor orci sit amet, iaculis lectus. Proin scelerisque mi lorem, quis interdum tellus feugiat ut.

Sed ut nisl erat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas at condimentum neque. Donec feugiat luctus aliquam. Integer tortor elit, varius at posuere eu, aliquet vel mi. Aenean id egestas dui. Pellentesque tristique condimentum scelerisque. Mauris nulla magna, tincidunt a posuere vitae, feugiat vitae sapien.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label color="success"]Praesent[/intense_label]
[/intense_column]
[intense_column size="10"]
In eget viverra nunc. Curabitur dignissim nulla nec scelerisque molestie. Aliquam erat volutpat. Morbi cursus nibh nec feugiat ultrices. Fusce adipiscing rhoncus tortor, sed fermentum risus commodo ut. Nam semper gravida mi ut cursus. Morbi dignissim convallis arcu. Morbi et lacus dignissim, sollicitudin lacus eget, mattis augue. Sed rutrum sagittis massa, non gravida lorem placerat in.

Mauris semper ac nisi volutpat bibendum. Duis quis ligula nisl. Sed eget metus justo. Aenean fermentum eget purus nec viverra. Nulla bibendum libero in ante viverra dictum. Curabitur a consectetur mauris. In tincidunt pulvinar pharetra. Etiam sit amet mauris ac leo varius lacinia a interdum arcu. Vestibulum faucibus mauris dui, id eleifend sem tincidunt vitae.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label color="success"]Mauris[/intense_label]
[/intense_column]
[intense_column size="10"]
Ut interdum suscipit lectus id aliquam. Sed luctus ultricies sem vitae viverra. Duis viverra vel dolor a congue. Nulla facilisi. Proin sit amet dui mollis, lacinia felis non, adipiscing diam. Duis felis nunc, laoreet pretium lobortis vitae, rutrum sollicitudin dolor. Praesent convallis ante augue, eget posuere elit pharetra sit amet. Sed ac nisl sit amet mi mattis consequat quis et justo. Curabitur lobortis lorem at arcu imperdiet, vitae mattis lacus gravida. Phasellus ut nibh purus. Nam viverra, tortor ac interdum viverra, arcu ipsum viverra nibh, eget ullamcorper felis tellus ut nunc. In pharetra dapibus lorem, at porttitor sapien mollis ac.

Aenean sodales, tortor vitae scelerisque sagittis, eros elit consequat nisl, ut consequat felis ligula id felis. Donec a arcu placerat magna interdum vehicula. Integer eleifend magna ipsum, quis consequat quam suscipit quis. Aliquam ac aliquam tellus, eu euismod sem. Integer id libero dignissim, ullamcorper elit varius, laoreet neque. Nam eu tempus ipsum. Donec tempor quis mi a consectetur. Vestibulum id auctor felis. Sed sed tellus turpis. Proin aliquam, urna eu dictum iaculis, neque orci dignissim nibh, ac interdum tellus odio vel arcu.

Fusce imperdiet porttitor urna aliquam consectetur. Quisque iaculis nulla metus, eu auctor tortor tempus vel. Etiam sit amet vulputate lorem. Aliquam erat volutpat. Sed cursus sem tellus, in mattis turpis dignissim id. Proin pulvinar porttitor lobortis. Maecenas quis dignissim nisi. Aliquam imperdiet fringilla arcu sed congue.
[/intense_column]
[/intense_row]

<h2>Nam malesuada ultricies pretium</h2>
[intense_image image="8321" size="postWide" title="Cactus" caption="Etiam commodo eget erat nec suscipit." /]

[intense_row]
[intense_column size="2"]
[intense_label]Vivamus[/intense_label]
[/intense_column]
[intense_column size="10"]
Maecenas congue, ligula at sollicitudin porttitor, ante mauris porta neque, ut aliquam dolor velit at ante. Ut egestas fermentum mi sit amet elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed id ipsum leo. Nam suscipit auctor sem et sagittis. Pellentesque tellus justo, tristique sit amet pulvinar tempus, condimentum ac neque. Phasellus ornare dui velit, vitae mollis justo lobortis vel. Maecenas sodales massa ut tincidunt fermentum. Donec eget massa aliquam, adipiscing leo et, ornare lacus. Nam porta facilisis augue id congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dapibus felis a tempus luctus. Mauris vel mattis augue. Cras in lacinia leo. Vivamus sit amet ligula sit amet magna adipiscing tincidunt ut eget metus.

Maecenas vel molestie lectus. Donec vitae iaculis tortor. Morbi placerat, nisl pretium egestas pharetra, urna turpis sagittis mauris, in molestie diam enim eu elit. Etiam id tortor turpis. In vulputate enim elit, et viverra libero feugiat eu. Curabitur aliquet diam lorem, non consequat sapien rhoncus ac. Sed posuere mauris quis sapien hendrerit bibendum. Quisque tincidunt laoreet est eu convallis. Maecenas id ligula mauris. Mauris ac ipsum posuere, egestas erat sed, tempus massa. Nullam pellentesque mattis dolor, condimentum fermentum nulla rhoncus in. Ut ut orci dictum, porttitor orci sit amet, iaculis lectus. Proin scelerisque mi lorem, quis interdum tellus feugiat ut.

Sed ut nisl erat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas at condimentum neque. Donec feugiat luctus aliquam. Integer tortor elit, varius at posuere eu, aliquet vel mi. Aenean id egestas dui. Pellentesque tristique condimentum scelerisque. Mauris nulla magna, tincidunt a posuere vitae, feugiat vitae sapien.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label]Donec[/intense_label]
[/intense_column]
[intense_column size="10"]
In eget viverra nunc. Curabitur dignissim nulla nec scelerisque molestie. Aliquam erat volutpat. Morbi cursus nibh nec feugiat ultrices. Fusce adipiscing rhoncus tortor, sed fermentum risus commodo ut. Nam semper gravida mi ut cursus. Morbi dignissim convallis arcu. Morbi et lacus dignissim, sollicitudin lacus eget, mattis augue. Sed rutrum sagittis massa, non gravida lorem placerat in.

Mauris semper ac nisi volutpat bibendum. Duis quis ligula nisl. Sed eget metus justo. Aenean fermentum eget purus nec viverra. Nulla bibendum libero in ante viverra dictum. Curabitur a consectetur mauris. In tincidunt pulvinar pharetra. Etiam sit amet mauris ac leo varius lacinia a interdum arcu. Vestibulum faucibus mauris dui, id eleifend sem tincidunt vitae.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label]Vestibulum[/intense_label]
[/intense_column]
[intense_column size="10"]
Ut interdum suscipit lectus id aliquam. Sed luctus ultricies sem vitae viverra. Duis viverra vel dolor a congue. Nulla facilisi. Proin sit amet dui mollis, lacinia felis non, adipiscing diam. Duis felis nunc, laoreet pretium lobortis vitae, rutrum sollicitudin dolor. Praesent convallis ante augue, eget posuere elit pharetra sit amet. Sed ac nisl sit amet mi mattis consequat quis et justo. Curabitur lobortis lorem at arcu imperdiet, vitae mattis lacus gravida. Phasellus ut nibh purus. Nam viverra, tortor ac interdum viverra, arcu ipsum viverra nibh, eget ullamcorper felis tellus ut nunc. In pharetra dapibus lorem, at porttitor sapien mollis ac.

Aenean sodales, tortor vitae scelerisque sagittis, eros elit consequat nisl, ut consequat felis ligula id felis. Donec a arcu placerat magna interdum vehicula. Integer eleifend magna ipsum, quis consequat quam suscipit quis. Aliquam ac aliquam tellus, eu euismod sem. Integer id libero dignissim, ullamcorper elit varius, laoreet neque. Nam eu tempus ipsum. Donec tempor quis mi a consectetur. Vestibulum id auctor felis. Sed sed tellus turpis. Proin aliquam, urna eu dictum iaculis, neque orci dignissim nibh, ac interdum tellus odio vel arcu.

Fusce imperdiet porttitor urna aliquam consectetur. Quisque iaculis nulla metus, eu auctor tortor tempus vel. Etiam sit amet vulputate lorem. Aliquam erat volutpat. Sed cursus sem tellus, in mattis turpis dignissim id. Proin pulvinar porttitor lobortis. Maecenas quis dignissim nisi. Aliquam imperdiet fringilla arcu sed congue.
[/intense_column]
[/intense_row]

<h2>Phasellus ut pellentesque arcu</h2>
[intense_image image="8301" size="postWide" title="New Life" caption="Integer lacinia porttitor diam nec sagittis." /]

[intense_row]
[intense_column size="2"]
[intense_label color="warning"]Mauris[/intense_label]
[/intense_column]
[intense_column size="10"]
Maecenas congue, ligula at sollicitudin porttitor, ante mauris porta neque, ut aliquam dolor velit at ante. Ut egestas fermentum mi sit amet elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed id ipsum leo. Nam suscipit auctor sem et sagittis. Pellentesque tellus justo, tristique sit amet pulvinar tempus, condimentum ac neque. Phasellus ornare dui velit, vitae mollis justo lobortis vel. Maecenas sodales massa ut tincidunt fermentum. Donec eget massa aliquam, adipiscing leo et, ornare lacus. Nam porta facilisis augue id congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dapibus felis a tempus luctus. Mauris vel mattis augue. Cras in lacinia leo. Vivamus sit amet ligula sit amet magna adipiscing tincidunt ut eget metus.

Maecenas vel molestie lectus. Donec vitae iaculis tortor. Morbi placerat, nisl pretium egestas pharetra, urna turpis sagittis mauris, in molestie diam enim eu elit. Etiam id tortor turpis. In vulputate enim elit, et viverra libero feugiat eu. Curabitur aliquet diam lorem, non consequat sapien rhoncus ac. Sed posuere mauris quis sapien hendrerit bibendum. Quisque tincidunt laoreet est eu convallis. Maecenas id ligula mauris. Mauris ac ipsum posuere, egestas erat sed, tempus massa. Nullam pellentesque mattis dolor, condimentum fermentum nulla rhoncus in. Ut ut orci dictum, porttitor orci sit amet, iaculis lectus. Proin scelerisque mi lorem, quis interdum tellus feugiat ut.

Sed ut nisl erat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas at condimentum neque. Donec feugiat luctus aliquam. Integer tortor elit, varius at posuere eu, aliquet vel mi. Aenean id egestas dui. Pellentesque tristique condimentum scelerisque. Mauris nulla magna, tincidunt a posuere vitae, feugiat vitae sapien.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label color="warning"]Phasellus[/intense_label]
[/intense_column]
[intense_column size="10"]
In eget viverra nunc. Curabitur dignissim nulla nec scelerisque molestie. Aliquam erat volutpat. Morbi cursus nibh nec feugiat ultrices. Fusce adipiscing rhoncus tortor, sed fermentum risus commodo ut. Nam semper gravida mi ut cursus. Morbi dignissim convallis arcu. Morbi et lacus dignissim, sollicitudin lacus eget, mattis augue. Sed rutrum sagittis massa, non gravida lorem placerat in.

Mauris semper ac nisi volutpat bibendum. Duis quis ligula nisl. Sed eget metus justo. Aenean fermentum eget purus nec viverra. Nulla bibendum libero in ante viverra dictum. Curabitur a consectetur mauris. In tincidunt pulvinar pharetra. Etiam sit amet mauris ac leo varius lacinia a interdum arcu. Vestibulum faucibus mauris dui, id eleifend sem tincidunt vitae.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]
[intense_row]
[intense_column size="2"]
[intense_label color="warning"]Maecenas[/intense_label]
[/intense_column]
[intense_column size="10"]
Ut interdum suscipit lectus id aliquam. Sed luctus ultricies sem vitae viverra. Duis viverra vel dolor a congue. Nulla facilisi. Proin sit amet dui mollis, lacinia felis non, adipiscing diam. Duis felis nunc, laoreet pretium lobortis vitae, rutrum sollicitudin dolor. Praesent convallis ante augue, eget posuere elit pharetra sit amet. Sed ac nisl sit amet mi mattis consequat quis et justo. Curabitur lobortis lorem at arcu imperdiet, vitae mattis lacus gravida. Phasellus ut nibh purus. Nam viverra, tortor ac interdum viverra, arcu ipsum viverra nibh, eget ullamcorper felis tellus ut nunc. In pharetra dapibus lorem, at porttitor sapien mollis ac.

Aenean sodales, tortor vitae scelerisque sagittis, eros elit consequat nisl, ut consequat felis ligula id felis. Donec a arcu placerat magna interdum vehicula. Integer eleifend magna ipsum, quis consequat quam suscipit quis. Aliquam ac aliquam tellus, eu euismod sem. Integer id libero dignissim, ullamcorper elit varius, laoreet neque. Nam eu tempus ipsum. Donec tempor quis mi a consectetur. Vestibulum id auctor felis. Sed sed tellus turpis. Proin aliquam, urna eu dictum iaculis, neque orci dignissim nibh, ac interdum tellus odio vel arcu.

Fusce imperdiet porttitor urna aliquam consectetur. Quisque iaculis nulla metus, eu auctor tortor tempus vel. Etiam sit amet vulputate lorem. Aliquam erat volutpat. Sed cursus sem tellus, in mattis turpis dignissim id. Proin pulvinar porttitor lobortis. Maecenas quis dignissim nisi. Aliquam imperdiet fringilla arcu sed congue.
[/intense_column]
[/intense_row]
[intense_hr shadow="2" /]

<h2>Etiam sit amet vulputate lorem</h2>

Nam malesuada ultricies pretium. Aliquam ut tortor sem. Praesent sed lectus molestie, facilisis odio semper, vulputate nisi. Pellentesque auctor augue id elit varius adipiscing. <strong>Sed rhoncus augue ac orci pulvinar, sed imperdiet ante iaculis</strong>. Proin nec augue et augue dictum rhoncus. Phasellus et consectetur orci. Aenean molestie tellus a felis vulputate, eu tristique mi aliquet.

Phasellus ut pellentesque arcu. Aliquam velit metus, viverra ut purus vel, dapibus ornare ligula. Praesent vulputate laoreet nisl. Proin eget nibh eu tortor consectetur tempor blandit et nulla. Vivamus feugiat tortor mattis massa ullamcorper, vitae blandit lorem molestie. In hac habitasse platea dictumst. Donec bibendum, turpis vel scelerisque blandit, lacus dui auctor leo, vitae dapibus quam odio in augue. <strong>Duis adipiscing lorem ut leo aliquam luctus.</strong>