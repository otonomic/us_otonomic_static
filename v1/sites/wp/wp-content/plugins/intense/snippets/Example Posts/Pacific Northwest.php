<?php
/*
Intense Snippet Name: Pacific Northwest
*/
?>

[intense_lead]Suspendisse potenti. Sed convallis risus sit amet tincidunt iaculis. Nullam accumsan vel massa sed dignissim. Phasellus magna metus, semper vel erat sit amet, sagittis gravida libero. Nunc pretium tempus est, quis fringilla urna posuere vitae. Donec nec magna et felis suscipit elementum eu vitae magna. Aliquam erat volutpat. Cras ultricies, erat sed dignissim dignissim, tortor orci lobortis leo, at condimentum nunc metus eget lectus.[/intense_lead]

[intense_map type="Roadmap" fit_bounds="1" zoom="6" address="Portland, OR" hue="#969696" saturation="-100" height="500px"]
[intense_map_marker latitude="42.972502" longitude="-122.113037" color="#4cbbe0" markertype="Marker" /]
[intense_map_marker latitude="47.783635" longitude="-124.101563" color="#4cbbe0" markertype="Marker" /]
[intense_map_marker latitude="42.940339" longitude="-124.541016" color="#4cbbe0" markertype="Marker" /]
[intense_map_marker latitude="46.255847" longitude="-122.036133" color="#4cbbe0" markertype="Marker" /]
[intense_map_marker latitude="44.684277" longitude="-124.101563" color="#4cbbe0" markertype="Marker" /]
[/intense_map]
<h2>Maecenas condimentum velit</h2>
Sed placerat vel orci eu facilisis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi vel sapien rutrum, faucibus felis at, gravida libero. Nam lobortis, leo at feugiat fringilla, nisi augue scelerisque nibh, eget bibendum risus augue in neque. Mauris leo libero, tincidunt eu est in, pretium pellentesque nunc. Curabitur varius dui et viverra ullamcorper. Donec pellentesque non ligula at scelerisque. Pellentesque malesuada, tellus eget semper tristique, augue arcu pulvinar urna, ac tincidunt libero massa sed justo. Vivamus nec neque vitae metus elementum interdum.

In fringilla risus velit, vel porta sapien lobortis et. Sed congue orci vitae aliquam ornare. Fusce accumsan venenatis dictum. Quisque sit amet urna vel justo laoreet accumsan et nec purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra, eros quis rutrum fringilla, lacus ante euismod elit, lacinia aliquam nibh lacus ut augue. Sed in placerat nisl. Cras vulputate hendrerit lorem, eu fringilla libero tempus sed. Curabitur vehicula, tellus ac condimentum rutrum, ligula mi fringilla massa, eget scelerisque dolor velit id leo. Curabitur dapibus est sem, id condimentum nulla scelerisque nec. Nullam a tellus nec felis ultricies consectetur.

Pellentesque id nibh gravida, ultricies libero sed, gravida arcu. Vestibulum rhoncus lorem sed lacinia malesuada. Phasellus non elit libero. Aenean in sapien in turpis pellentesque suscipit. Nam varius faucibus mi, quis mattis est accumsan vestibulum. Nulla facilisi. Aliquam odio risus, venenatis quis aliquet vitae, pharetra vel nulla. Praesent vitae risus vel est pretium pretium. Cras egestas sem quis lorem dignissim ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus.

Sed commodo ornare sapien, et egestas felis interdum non. Vivamus in dapibus tortor, fermentum blandit dui. Duis faucibus erat vel ligula aliquet, id aliquam turpis semper. Vestibulum ornare urna at sapien auctor elementum. Nunc facilisis sodales aliquet. Sed dui ipsum, blandit non consectetur in, dignissim ac dolor. Sed eget faucibus enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vitae elit ac turpis porta fringilla. Nam dictum nibh elit, sagittis consectetur urna viverra id. Fusce eu auctor sapien. Aenean aliquet, justo eu tempor bibendum, risus leo venenatis sem, eu vulputate odio urna a arcu. Nunc ac justo varius, imperdiet erat non, ultricies tortor.

[intense_image image="8199" size="large1024" effeckt="2" effecktcolor="#000000" effecktopacity="80" shadow="12" title="Along the Shore" caption="Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed massa urna, porta eu odio dignissim, viverra egestas risus. Phasellus ac malesuada odio, et tincidunt justo. Integer nec nunc a nisl fermentum elementum et et magna. Nulla vitae hendrerit augue. Morbi suscipit elit a velit ullamcorper, sit amet blandit tortor tempor. Etiam id sapien sem. Ut a nunc at elit dictum vestibulum." /]
<h2>Class aptent taciti</h2>
Donec feugiat a urna at fringilla. Quisque ullamcorper bibendum justo, vel feugiat nulla eleifend non. Duis nisi enim, aliquam et dui non, mollis posuere ligula. Quisque nibh enim, placerat id posuere eu, bibendum nec nisi. Donec vel neque a felis ultricies commodo et vel eros. Phasellus eros mauris, sollicitudin a rutrum accumsan, ultricies nec eros. Suspendisse porta ante vel iaculis pharetra. Nunc sodales ante vitae ipsum euismod posuere. Vivamus eu sapien sed nunc posuere ultricies vitae sed justo. Donec ultrices fermentum velit, non sollicitudin dui commodo volutpat. Vestibulum faucibus, est eget convallis scelerisque, orci purus pharetra tellus, ut vestibulum tellus erat sed mauris. Nunc non sapien sed tortor eleifend ullamcorper in sit amet dolor.

Nam luctus non dolor sed posuere. Etiam porttitor, mauris sit amet blandit pretium, leo ante dictum tellus, nec adipiscing diam dui sit amet tellus. Integer ut eros id nunc posuere interdum eleifend eget justo. Pellentesque sagittis fringilla massa, sed gravida dui. Praesent ut massa sollicitudin, tempus est pharetra, aliquam tellus. Proin urna libero, mattis tincidunt tortor sit amet, pellentesque bibendum elit. Sed pellentesque adipiscing libero, interdum porta tellus placerat quis. Mauris auctor diam eu dui suscipit dictum. Cras vitae aliquet nisl. Etiam malesuada risus justo.

[intense_image image="8200" size="large1024" effeckt="2" effecktcolor="#000000" effecktopacity="80" shadow="12" title="A misty morning" caption="Nulla facilisi. Nam bibendum leo massa, et porta mauris viverra id. Donec faucibus lacinia commodo. Phasellus interdum lobortis dui eget condimentum. Sed eget elementum velit. Sed a vestibulum mauris. Vestibulum aliquet, ligula pretium iaculis aliquet, massa tortor auctor odio, sit amet luctus massa est eu magna. Nam quis erat aliquet, volutpat mi in, malesuada justo." /]
<h2>Nullam eget tristique orci, nec imperdiet turpis</h2>
Nullam laoreet odio ac placerat auctor. Pellentesque sagittis nisl ac orci luctus varius. Morbi nibh magna, lobortis vel arcu vitae, dictum suscipit ligula. Nullam porta feugiat erat in venenatis. Duis cursus magna eget lacus tristique pulvinar. Phasellus accumsan tellus quis lectus bibendum posuere. Suspendisse id dapibus lorem, nec scelerisque nulla. Nunc sit amet est massa. Morbi faucibus adipiscing felis eget aliquam. Fusce ut eros a lorem laoreet tincidunt. Vivamus mollis rhoncus cursus. Phasellus eu lectus quis ligula adipiscing lacinia eu pretium eros.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed massa urna, porta eu odio dignissim, viverra egestas risus. Phasellus ac malesuada odio, et tincidunt justo. Integer nec nunc a nisl fermentum elementum et et magna. Nulla vitae hendrerit augue. Morbi suscipit elit a velit ullamcorper, sit amet blandit tortor tempor. Etiam id sapien sem. Ut a nunc at elit dictum vestibulum.

[intense_hr type="double" title_position="center" icon_type="picture" icon_position="left" icon_size="2" icon_color="#727272" /]

[intense_gallery size="postWide" type="colorbox" effeckt="4" columns="2" effeckt_color="#000000" effeckt_opacity="80" grouping="photos" include="8198,8205,8204,8203,8202,8201" shadow="1" /]

[intense_hr type="double" /]