<?php
/*
Intense Snippet Name: Namche Bazaar
*/
?>

[intense_lead]Praesent aliquet turpis sed dictum vulputate. Proin pretium diam laoreet pellentesque hendrerit. Sed fringilla dui euismod dapibus accumsan. Curabitur lorem dui, pellentesque et convallis sed, lacinia et turpis. Nam porttitor lacus erat, at bibendum orci laoreet vitae. Cras laoreet magna in eleifend sagittis. Pellentesque vestibulum ipsum ac sem fermentum convallis. Aenean non turpis eros.[/intense_lead]

Maecenas auctor scelerisque ipsum quis volutpat. Morbi iaculis bibendum nibh, sit amet iaculis urna rhoncus et. Nulla sagittis purus eget ante egestas faucibus. Donec ornare justo in mauris fermentum fermentum. Praesent ac neque nisi. Morbi faucibus pulvinar nisi et luctus. Aenean luctus sapien at enim volutpat suscipit. Proin orci dolor, rhoncus non pellentesque at, lobortis quis tellus. Nam sit amet massa vel augue tristique lacinia. Nam vel ligula sit amet enim semper imperdiet. Sed consectetur ac mi sit amet vehicula. Aliquam varius sapien ligula, consequat lobortis odio condimentum vitae. Sed eget sapien neque. Integer non mollis leo. Nullam vel urna ante.

[intense_blockquote rightalign="0"]<h2 style="margin-top: 0;padding-top: 0">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h2>[/intense_blockquote]
[intense_gallery  size="square400" columns="3" marginpercent=".25" effeckt="8" effeckt_color="#24aee5" effeckt_opacity="80" include="8197,8196,8195,8194,8193,8192" /]

Cras neque felis, tincidunt non volutpat quis, auctor a nibh. Nulla eget cursus massa. Nullam sem neque, aliquam vel tellus ut, lobortis consectetur metus. Integer justo lorem, ultricies id dapibus ac, condimentum sit amet justo. Sed id sagittis nisl. In et gravida velit. Sed ultricies non enim sed scelerisque.

Phasellus condimentum ipsum et orci ultrices, a rhoncus nisi dictum. [intense_highlight color="muted"]Vivamus erat odio, laoreet sagittis dapibus in, pellentesque id justo. Praesent feugiat tincidunt porta. Aenean sit amet ipsum in purus pulvinar pellentesque non vitae tortor.[/intense_highlight] Nulla eget lacus eu dui tempus pulvinar euismod ac dui. Praesent at enim placerat, vulputate ipsum sit amet, tristique erat. Nullam non porta erat, eget feugiat leo.

Aliquam erat volutpat. Etiam in elit at lacus cursus feugiat. Fusce consequat dui faucibus, consequat lacus et, ornare nulla. Donec a scelerisque est, quis luctus nulla. Sed faucibus ipsum vitae sapien gravida faucibus. Aenean in nisi elit. Nullam ut libero vel purus aliquet faucibus. Integer magna augue, commodo a lacinia at, tempus ut tellus. Curabitur aliquam nisi enim, sit amet scelerisque mauris placerat vel. Nulla purus lectus, congue sit amet imperdiet sed, tempus eget urna.