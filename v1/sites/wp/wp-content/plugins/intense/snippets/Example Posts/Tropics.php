<?php
/*
Intense Snippet Name: Tropics
*/
?>

[intense_lead]Donec vitae nisi at nibh suscipit elementum vitae eget nulla. Morbi nisl sem, convallis ac dignissim nec, tincidunt vel augue. Nulla at feugiat eros, at posuere ante. Fusce sagittis diam sit amet augue mattis adipiscing. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque eu lorem diam. Integer sit amet ornare nibh.[/intense_lead]

[intense_gallery columns="4" type="swipebox" effeckt="2" effeckt_color="#017c57" effeckt_opacity="80" grouping="beach" size="small240"  include="7815,5471,5472,5465,5466,8303,8304,8299,8295,8301,8100,8106,8161,8182,7813,8320" order="NONE" /]

<h2>Vivamus</h2>
[intense_row padding_top="0"]
[intense_column size="6"]
Vivamus et egestas est. In luctus sodales mi sit amet ultricies. Nam purus dignissim massa ac facilisis facilisis. Phasellus viverra purus in tellus elementum viverra. Nullam et mi quis lectus porta venenatis. Duis vel consequat orci, quis vehicula ligula. Nulla non euismod orci, in bibendum purus. Nunc ligula sem, vehicula ac diam eget, pellentesque facilisis massa. Curabitur tristique congue urna at interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sagittis sodales nisi eu vehicula. Pellentesque pellentesque augue vestibulum viverra aliquam.

Donec luctus elit est. Donec rhoncus pharetra sollicitudin. Donec malesuada libero ac orci iaculis blandit. In sit amet aliquet sem. Aliquam non lacus in felis consectetur tempor. Suspendisse nec ante non nibh mattis volutpat volutpat ut justo. Duis scelerisque elementum neque non porttitor.
[/intense_column]
[intense_column size="6"]
Nullam ac adipiscing neque. Morbi id consectetur purus. Suspendisse accumsan lacinia ornare. <strong>Phasellus nibh lorem, tincidunt sit amet tempor sit amet, semper at nibh. Morbi egestas aliquet orci, sit amet fringilla sapien adipiscing id. Duis ullamcorper odio sed aliquam rutrum. Aliquam erat volutpat. </strong>Proin luctus lectus nec libero vestibulum, pretium varius tellus semper.

Aenean sit amet dui sit amet purus venenatis laoreet. Pellentesque non pharetra mauris. Cras fermentum vitae nisi ut dignissim. Duis accumsan, quam quis dapibus interdum, sem ante aliquet nibh, non sagittis sapien turpis non ipsum. Sed dapibus nulla nec ipsum auctor, eu tristique dui suscipit. Sed mattis nisi id est dictum aliquet. Mauris sed consequat felis. Donec at libero quam.
[/intense_column]
[/intense_row]

<h2>Donec a tincidunt nisi</h2>
[intense_image image="8294" size="postWide" title="Sail Boats" caption="Curabitur condimentum iaculis mollis. Donec vel vulputate est. Fusce tellus enim, cursus eget malesuada sit amet, placerat at arcu. Aliquam at neque commodo, imperdiet sapien sed, fringilla velit. Cras gravida nisi et augue sodales fermentum. Nulla fermentum sit amet lacus eu cursus. Curabitur at imperdiet nulla." shadow="8" /]

Donec a tincidunt nisi. Fusce malesuada neque vel velit egestas, vel sollicitudin orci sagittis. Sed volutpat tempus erat, id tristique sem blandit sed. Phasellus et sollicitudin metus. Morbi dapibus gravida elit in semper. Praesent eget nulla non metus suscipit consequat. Aenean elementum convallis egestas. Aliquam auctor urna et condimentum ultricies.<strong> Nulla suscipit est sed urna tempor, sit amet viverra diam tristique.</strong> Aliquam erat volutpat. Donec aliquet, sem in eleifend lacinia, lectus orci accumsan massa, quis vehicula orci nibh eu magna. Etiam sed metus ac elit cursus tempus et eu felis. Curabitur eleifend lobortis pulvinar. Morbi vitae lacus ut orci egestas commodo a non ante. Nullam sodales blandit lorem, congue laoreet magna viverra in.

Vivamus gravida metus sit amet velit vestibulum, quis rhoncus turpis sollicitudin. Integer fringilla lacus nisl, vitae scelerisque lectus consequat dignissim. Donec eu elit ac ante mattis ultricies ut eu tellus. Integer ut placerat dolor. Nunc laoreet sem sed elit fermentum accumsan. Fusce eu lorem purus. Suspendisse et dapibus eros, lacinia tristique mauris. Proin sagittis tempor eros nec fermentum. Mauris condimentum erat id arcu elementum, id viverra ligula pretium.