<?php
/*
Intense Snippet Name: Create a Lasting Impression
*/
?>

[intense_lead]Curabitur condimentum iaculis mollis. Donec vel vulputate est. Fusce tellus enim, cursus eget malesuada sit amet, placerat at arcu. Aliquam at neque commodo, imperdiet sapien sed, fringilla velit. Cras gravida nisi et augue sodales fermentum. Nulla fermentum sit amet lacus eu cursus. Curabitur at imperdiet nulla.[/intense_lead]

Duis nec orci in leo tristique auctor. Suspendisse ultricies dui nec dictum lacinia. Pellentesque congue eleifend lacus eu tincidunt. Nullam tempus est non adipiscing porta. Donec quis suscipit urna, non ullamcorper lacus. Nam volutpat quam ut pharetra facilisis. Duis gravida est ut sem eleifend, sit amet tincidunt lacus fermentum. Nulla facilisi.

Vivamus congue mauris dui, iaculis semper orci auctor ut. Donec at sagittis odio, volutpat ornare mi. Vestibulum cursus quis lectus vel molestie. Ut varius ornare viverra. Duis sit amet porta risus. Maecenas a magna vel erat malesuada viverra. Curabitur id lacus libero. Fusce sed libero nec leo tristique venenatis ac eget leo. Phasellus augue tortor, placerat non hendrerit nec, suscipit sit amet massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam tortor justo, bibendum vel lectus id, hendrerit porttitor nulla.

Praesent felis urna, porttitor a dolor sed, suscipit porttitor risus. Integer dapibus luctus est vel molestie. Etiam id imperdiet magna, et faucibus libero. Praesent fringilla iaculis metus, sed sodales est dictum quis. Donec quis augue lacus. Donec vulputate at nisi quis condimentum. Mauris venenatis cursus nunc, imperdiet iaculis libero rutrum nec. Pellentesque sed ligula pharetra, vehicula augue at, ultrices ante. Nullam ac nulla lacinia, porta mi sit amet, molestie nibh. Vestibulum lacinia mi eget ipsum tincidunt, molestie tempus enim ullamcorper. Morbi bibendum porta ante in rutrum. Fusce ornare nisi lorem. Nullam sit amet lacus massa. Donec tristique odio et justo adipiscing condimentum.

<h2>Proin pulvinar</h2>

Ut rutrum consequat cursus. Cras lorem velit, hendrerit vel sem sit amet, viverra consectetur arcu. Pellentesque varius ante ipsum, eget dignissim quam bibendum vel. Curabitur purus urna, laoreet nec luctus eu, adipiscing eget quam. Maecenas dictum eget nulla ut aliquet. Morbi tristique massa a tortor scelerisque, auctor ultrices risus sollicitudin. Curabitur pulvinar urna quis purus dictum mollis. Nullam vel nulla ut nunc tristique ullamcorper eu scelerisque metus. Nulla luctus libero ac pellentesque condimentum. Phasellus in interdum ligula, a lacinia lectus. Sed id nibh ipsum. Curabitur interdum imperdiet leo imperdiet porta. Aenean bibendum tellus pulvinar nibh volutpat volutpat. Nunc tempus ac turpis sed placerat. Etiam non orci vel nulla commodo dignissim.

Donec mi nulla, molestie ut diam at, rutrum auctor massa. Nullam id tempor magna. Nullam pharetra enim ornare mi condimentum ultricies. Donec ut dictum erat. Nullam gravida tellus varius imperdiet lacinia. Maecenas porttitor dolor ac dui pulvinar, vitae bibendum risus adipiscing. Vivamus ultricies tortor diam, sed volutpat urna consequat sit amet.

[intense_promo_box button_type="primary" button_position="right" button_size="large" size="medium" background_color="" color="#333" shadow="1" border="1px solid #d0dce0"]
<em>Proin in lacus libero. Maecenas suscipit, leo eleifend adipiscing adipiscing, orci massa vestibulum felis, ut aliquam dolor libero faucibus felis. Aenean quam neque, aliquam id molestie ac, varius nec nulla. Aliquam volutpat nunc non leo porttitor, in iaculis orci scelerisque. Cras accumsan rhoncus elementum. Praesent vestibulum tincidunt odio in lobortis. Nulla in ipsum vehicula, auctor erat ut, sagittis est.</em>
[/intense_promo_box]

<h2>Aliquam erat volutpat</h2>
Mauris varius metus nec tortor commodo blandit. Morbi sodales ullamcorper cursus. Aenean viverra dui eget rhoncus tempor. Proin tincidunt nibh nec convallis tincidunt. Praesent viverra neque venenatis nibh venenatis porttitor. Maecenas et mollis risus. Donec eget justo blandit, iaculis nulla vitae, interdum ipsum. Aliquam ut odio commodo arcu pharetra auctor. Donec nec fermentum enim. Morbi libero justo, suscipit sed sapien in, lobortis posuere ligula. Nunc aliquet quis massa non ultricies.
[intense_row padding_bottom="30"]
[intense_column size="2"]

[/intense_column]
[intense_column size="8"]
http://www.youtube.com/watch?v=C6c-dWhCPE0
[/intense_column]
[intense_column size="2"]

[/intense_column]
[/intense_row]

<h2>Aliquam adipiscing</h2>
Curabitur laoreet nibh at sem accumsan, sit amet tempor tellus rutrum. Pellentesque lobortis gravida ornare. Quisque at urna dictum, condimentum diam quis, ornare lorem. Donec faucibus volutpat sapien, sed lacinia eros ultricies in. Proin vestibulum ligula tellus, eget fermentum lectus dapibus id. Quisque id ante sit amet velit commodo tempus. Cras vitae dictum nisl, quis dictum erat. Nunc quis ligula dolor. Proin consequat ut arcu nec ultricies. Nulla facilisi. Donec ornare auctor mi, at mollis augue faucibus at. Proin in neque sit amet ante ultricies condimentum ut et odio. Praesent bibendum dignissim ornare. Mauris viverra purus sed enim egestas, nec adipiscing ligula ullamcorper.

Fusce dictum pulvinar consectetur. Cras sit amet eros in justo fringilla scelerisque. Donec id urna sem. Phasellus non tellus quis dui pretium mollis at sed tortor. Sed iaculis convallis tortor. Donec porta augue id lectus laoreet, sit amet faucibus erat condimentum. Nulla facilisi. Suspendisse lacinia nisi eget tortor rhoncus, vitae mattis erat placerat. Aliquam elementum velit id hendrerit imperdiet. Sed feugiat consequat tincidunt. Morbi ultrices, metus sed laoreet scelerisque, quam sem pharetra mi, in sodales tortor leo at ipsum. Ut ullamcorper, purus quis gravida adipiscing, risus lectus pretium dolor, vitae rhoncus mauris metus sed tortor. Ut ut bibendum eros.
[intense_row padding_bottom="30"]
[intense_column size="2"]

[/intense_column]
[intense_column size="8"]
[intense_image image="7204" size="medium640" title="Person_right3" /]
[/intense_column]
[intense_column size="2"]

[/intense_column]
[/intense_row]

<h2>Risus lectus</h2>
Quisque blandit risus quam, sit amet suscipit velit euismod eget. Cras gravida semper sapien sed congue. Aenean id urna gravida, tristique nunc id, feugiat ante. Curabitur elit elit, posuere a sodales eu, lobortis consequat metus. Etiam elementum congue leo, et congue augue. Donec tincidunt a quam vitae tristique. Pellentesque sollicitudin nisi nec ante euismod, id tempor urna fermentum. Phasellus sed erat ut quam viverra rhoncus in et nunc. Nunc eget feugiat turpis, semper mollis libero. Phasellus vitae lectus a mi aliquet adipiscing.

Praesent at vulputate libero. Donec condimentum auctor neque, in consectetur nisi accumsan ac. Vestibulum mi urna, facilisis quis euismod eu, dapibus at turpis. Sed ultrices ipsum sit amet tortor mollis, eget scelerisque mauris tristique. Nunc sagittis sit amet mi eget molestie. In mi neque, vehicula id suscipit et, tincidunt quis orci. Fusce aliquam auctor ultrices. Sed massa diam, laoreet dapibus magna at, ornare porta neque.