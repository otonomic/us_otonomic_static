<?php
/*
Intense Snippet Name: Build the Perfect Site
*/
?>

[intense_lead]Pellentesque vitae dignissim justo. In elementum, mi eget aliquet ullamcorper, orci felis posuere neque, a imperdiet odio ligula eget nulla. Vivamus convallis nisl dolor, et condimentum felis elementum at. In cursus, felis a tempus dictum, lectus eros posuere turpis, eget faucibus sem mi ut erat. Fusce tincidunt dignissim lorem dapibus placerat. Proin vehicula volutpat nibh, vitae interdum purus varius id. Mauris egestas facilisis ipsum, quis tristique erat. Praesent faucibus, magna sit amet venenatis dapibus, libero tortor vestibulum risus, nec interdum ipsum arcu nec mi. Sed interdum libero quis ultricies sagittis. Donec lacinia fringilla ipsum.

Curabitur mi enim, fringilla et fringilla a, hendrerit vel lectus. Nunc eget metus et mi mattis accumsan. Nullam a sagittis nisi, at pharetra tortor. Proin lacinia eleifend ullamcorper. Cras pharetra, dolor sed tincidunt pellentesque, ipsum nisl imperdiet dolor, sit amet ornare nulla dolor et nibh. Sed hendrerit erat tortor, eget sollicitudin tellus aliquet facilisis. Duis fringilla feugiat viverra. Pellentesque posuere erat nec ante porttitor, ut convallis elit suscipit.[/intense_lead]

[intense_blockquote rightalign="0"]In non neque at nulla tristique eleifend. Fusce malesuada dui non magna fermentum tincidunt. Ut volutpat neque a nisi sagittis interdum. Sed placerat dapibus suscipit. Donec vel nisl lacinia, adipiscing urna eget, ultrices metus. Proin dictum justo et convallis rutrum. Duis semper turpis felis, id hendrerit libero luctus condimentum. Fusce neque mi, ornare posuere dignissim in, sollicitudin ornare nisl. Nulla sapien nisl, aliquet in nisi at, blandit lacinia neque.[/intense_blockquote]

Suspendisse lobortis mauris in fermentum rutrum. Sed et cursus leo. <strong>Curabitur vel accumsan tellus. Vestibulum porttitor ante non quam aliquet mattis. Nam elementum quam quis dolor luctus tincidunt.</strong> Quisque erat ante, semper sit amet enim at, volutpat sollicitudin nibh. Cras iaculis fermentum libero porta pulvinar. Etiam dapibus posuere quam, eget sagittis diam pretium in. Integer dolor diam, eleifend eget tristique et, aliquet quis dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce tincidunt fringilla elit sed congue. Quisque consequat elementum sem, ut pellentesque sapien lobortis vitae. Sed accumsan scelerisque sapien, vel ultricies augue.

Donec neque metus, adipiscing eu sem ut, gravida laoreet dolor. Vestibulum porta vehicula nunc, non ornare ligula pulvinar ut. Fusce bibendum vitae nisl eget vestibulum. Donec ac ligula diam. Ut sapien lectus, elementum vel tempor a, laoreet sit amet arcu. Aenean interdum, dui vel scelerisque gravida, sapien risus gravida lacus, id pellentesque ligula ligula pulvinar turpis. Vestibulum id auctor diam. Ut lobortis ornare tristique. Integer et molestie enim.

Maecenas id mauris augue. Mauris nec lectus eget felis hendrerit iaculis. <strong>Duis nulla neque, elementum at mauris eget, lobortis dictum lorem.</strong> Proin in vulputate justo. Nunc molestie ligula justo, vel congue mi placerat et. Integer eu nisl nulla. Vivamus sed risus sed ante scelerisque posuere. Fusce vulputate felis eu sapien pellentesque tempus.
[intense_hr shadow="1" /]
[intense_row]
[intense_column size="5"]
[intense_animated type="fadeInLeftBig" trigger="scroll" scroll_percentage="10"]
[intense_image image="7474" size="medium500" title="young businessman with computer sitting on the cloud" /]
[/intense_animated]
[/intense_column]
[intense_column size="7"]
<h3>Integer in pharetra</h3>
Integer in pharetra risus, vel consequat justo. Vivamus pulvinar mi eu euismod rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus dictum, ante in congue vestibulum, sem justo sodales ligula, in vehicula justo nibh in lectus. Nulla eu nunc ac metus lacinia aliquet. Etiam bibendum dapibus est ac congue. Sed hendrerit magna eu sollicitudin vulputate. Cras eu turpis nec magna commodo porttitor. Nunc eget justo quis lectus blandit iaculis eget nec est. Aenean suscipit purus eget velit iaculis facilisis. Vivamus aliquet, nulla nec porta tincidunt, leo quam varius est, sit amet tincidunt sapien nibh eget nisl.

Quisque fringilla dapibus erat id vestibulum. Duis vel vehicula odio, id cursus tellus. Curabitur dictum in augue ac dictum. Fusce quis nunc sed diam scelerisque hendrerit. Nulla facilisi. Fusce quis posuere purus. In sed porta velit. Integer vehicula, arcu id lacinia auctor, justo augue commodo nulla, elementum gravida elit diam ut leo. Morbi dictum ipsum non facilisis blandit. Suspendisse eu dolor ultrices, mollis dui in, blandit odio.
[/intense_column]
[/intense_row]
Proin quis congue ipsum. Quisque malesuada diam nec dictum lacinia. Donec non convallis sapien. Mauris convallis gravida justo eu viverra. Nullam ultrices dui eu posuere mattis. Curabitur rutrum consequat tristique. Nunc ipsum arcu, tempor tincidunt lectus viverra, semper consectetur massa. Aliquam vestibulum nisl ac nulla consectetur, ac tempor enim tincidunt.
[intense_hr shadow="1" /]
[intense_row]
[intense_column size="7"]
<h3>Maecenas tincidunt</h3>
Maecenas tincidunt turpis vel massa mattis mollis. Sed eu orci eu tortor facilisis molestie. Fusce facilisis augue placerat molestie ultrices. Aliquam nulla massa, auctor quis dui vel, sollicitudin sagittis nisl. Donec non dui hendrerit, venenatis tortor nec, blandit purus. Pellentesque tempor dapibus dictum. Cras pretium ac nibh et convallis. Morbi commodo urna id semper laoreet. Morbi dictum at augue cursus interdum.

Praesent vel erat interdum, volutpat dui non, faucibus augue. Cras elit odio, rutrum at congue porta, volutpat sed odio. Nam in turpis tempor, convallis quam eget, scelerisque quam. Donec mattis tempus leo, id euismod libero euismod vel. Nullam condimentum aliquam dui vitae cursus. Nam interdum quam porttitor leo imperdiet, ac vehicula nunc ultrices. Integer semper faucibus mauris, nec bibendum diam sollicitudin quis. Sed eget pellentesque sem. Curabitur et tempus nunc, id fermentum nulla. Sed luctus arcu sed hendrerit gravida.
[/intense_column]
[intense_column size="5"]
[intense_animated type="fadeInRightBig" trigger="scroll" scroll_percentage="10"]
[intense_image image="7489" size="medium500" title="Big Ben at sunset panorama, London" /]
[/intense_animated]
[/intense_column]
[/intense_row]
[intense_hr shadow="1" /]
[intense_image image="7085" size="postWide" title="WhiteWall" type="picstrip" splits="12" hgutter="5px" vgutter="5px" bgcolor="#ffffff" /]
<h2>Sed luctus arcu sed hendrerit gravida</h2>
Duis cursus purus risus, quis pretium ligula porta vitae. Aenean feugiat turpis eros. Curabitur dapibus mi nec quam ornare, at malesuada massa sollicitudin. Praesent egestas luctus orci, at blandit leo lacinia id. Fusce porttitor mollis ullamcorper. Etiam scelerisque ultrices cursus. Maecenas lacinia mollis nibh, vel tincidunt ligula tristique at. Morbi at purus semper odio tempor sollicitudin nec sit amet sapien. Nullam laoreet vulputate libero, sed porta lorem molestie ac. Quisque mattis bibendum sodales. In quis sapien in mauris ullamcorper laoreet. Suspendisse quis elementum ipsum. Donec commodo eros quis condimentum aliquam. In hac habitasse platea dictumst.

Donec vitae mauris vel leo laoreet venenatis. Maecenas ligula mauris, auctor et dignissim non, scelerisque eget nibh. Etiam rutrum fermentum elit, et hendrerit libero pellentesque nec. Ut sollicitudin elit enim, et lobortis est luctus nec. Praesent vitae dolor nulla. Nulla pulvinar, magna in ornare rutrum, tortor mauris mollis risus, id viverra augue magna quis est. Cras ut risus sagittis, tristique diam eget, tempor sem. Donec a dui nisl. Integer viverra sed turpis ac mollis.

Suspendisse euismod nibh metus. Donec mattis faucibus mauris cursus ultrices. Suspendisse potenti. In consequat ligula urna, accumsan convallis sem elementum nec. In blandit volutpat porta. In sit amet tempus velit, sit amet dapibus nibh. Cras bibendum dui in nulla bibendum accumsan a ac dolor. Vestibulum cursus mattis massa. Nunc lobortis viverra sapien eget tincidunt. Sed egestas posuere sagittis. Cras sit amet est a tortor condimentum posuere.