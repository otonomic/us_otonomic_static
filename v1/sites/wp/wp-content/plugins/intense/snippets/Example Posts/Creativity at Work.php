<?php
/*
Intense Snippet Name: Creativity at Work
*/
?>

[intense_lead]Nunc congue, nisl a hendrerit commodo, enim nisl tincidunt quam, et pulvinar velit enim et mi. Vivamus tristique luctus massa eu semper. Aenean elit mi, auctor lobortis sollicitudin quis, sodales ac enim. Praesent sit amet dignissim nisi, at euismod odio. Morbi vel dictum nulla. Morbi at sodales turpis. Nullam placerat condimentum elit ut sodales.[/intense_lead]

Quisque tincidunt tincidunt ante, sit amet lacinia diam blandit eu. Nullam urna quam, consequat at lacinia at, molestie tincidunt metus. Curabitur mattis dapibus luctus. Donec rutrum molestie elit id bibendum.
<h2>Fusce Convallis Odio Vel Odio</h2>
Proin vel urna ut sem commodo fringilla at quis lacus. Curabitur accumsan mauris quis ante porta, ut accumsan justo gravida. Nunc vitae viverra nisl, scelerisque sollicitudin sem. Curabitur vitae dolor eu lectus facilisis convallis at vel arcu. <strong>In sit amet erat quis justo laoreet rhoncus vel ac nisl</strong>. Donec tempor tempus rutrum. Sed semper auctor metus nec luctus. Sed fringilla velit in libero rutrum, vel sodales odio luctus. Duis egestas leo ut dui cursus, a molestie est commodo. Duis venenatis risus ligula, eget cursus velit porta nec. Etiam a dolor mattis, rutrum leo eget, volutpat ante. Vivamus leo nunc, gravida vel lectus et, sollicitudin hendrerit orci. Proin eget malesuada elit, quis consectetur dui. Nam consectetur congue est ac rhoncus.

Vestibulum lacinia dictum enim, id ullamcorper felis commodo eget. Sed eget odio in libero vulputate molestie sed at elit. Cras condimentum felis odio, ac congue elit vestibulum eu. Nam porttitor ipsum sit amet ipsum tincidunt laoreet. Sed tristique luctus laoreet. Praesent ut ipsum sollicitudin, consequat felis non, tempor turpis. Aenean eu luctus lacus. Vestibulum ante ipsum primis in faucibus orci [intense_highlight ]cubilia Curae; Nunc a tempus metus. Nullam iaculis arcu non molestie aliquet.[/intense_highlight] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dolor nisi, congue nec lobortis sed, placerat id lacus. Ut consequat lobortis nisl. Praesent id sapien feugiat, imperdiet nisi eget, porta felis. In eget tortor eu mauris cursus vestibulum. Nunc venenatis lobortis risus vitae imperdiet.

Donec quis orci at sapien dapibus dapibus mattis non erat. Donec a velit vel mi adipiscing lacinia quis dictum ante. Nam non consequat massa, non fermentum enim. Aliquam euismod, risus quis vulputate pulvinar, elit erat laoreet velit, a congue enim mauris sit amet metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam egestas rutrum eleifend. Fusce a ornare libero.

[intense_image image="8100" size="postWide" title="A collection of seashells" caption="Mauris in tortor ac purus aliquet rhoncus nec nec lectus. Sed venenatis purus id turpis." effeckt="1" effecktcolor="#000000" effecktopacity="80" shadow="10" /]
<h2>Integer Vitae</h2>
Pellentesque imperdiet, augue molestie interdum imperdiet, enim nunc faucibus risus, non adipiscing lorem sapien eget turpis. Vestibulum eleifend mi quis elementum bibendum. Maecenas in lacus quis risus malesuada pellentesque eu.

[intense_icon_list]
[intense_icon_list_item type="asterisk" size="1" color="muted"]
Aliquam cursus odio in enim vestibulum, sit amet tempor ipsum pharetra.
[/intense_icon_list_item]
[intense_icon_list_item type="asterisk" size="1" color="muted"]
In tempus odio vitae hendrerit condimentum.
[/intense_icon_list_item]
[intense_icon_list_item type="asterisk" size="1" color="muted"]
Donec venenatis ipsum non nunc porta, ac ullamcorper diam viverra.
[/intense_icon_list_item]
[intense_icon_list_item type="asterisk" size="1" color="muted"]
Integer a ipsum et sem eleifend auctor sit amet vitae dolor.
[/intense_icon_list_item]
[/intense_icon_list]

Phasellus non molestie leo, a varius sem. Duis sed ultrices libero. Pellentesque orci nisl, molestie sit amet lectus a, ultricies sagittis eros. Morbi pellentesque volutpat pharetra. Aenean quis quam purus. Duis quis varius nisl. Proin augue metus, hendrerit vel nisi quis, ultrices ultrices ipsum. In commodo, nunc sit amet placerat dapibus, arcu leo lacinia tellus, ac condimentum massa orci sed enim. Phasellus sed quam dolor. Curabitur placerat ante eget nibh tincidunt, iaculis pretium leo sagittis.

[intense_image image="8106" size="postWide" title="A boat on the beach" caption="Phasellus hendrerit, magna ac ultricies mattis, tortor tellus euismod felis, tristique venenatis velit arcu ac." effeckt="2" effecktcolor="#000000" effecktopacity="80" shadow="10" /]
<h2>Vitae Dolor</h2>
Donec mattis posuere mi, vitae elementum eros fringilla ac. Pellentesque elit odio, vestibulum a risus in, mattis eleifend lacus. Quisque vitae augue ac lacus consequat posuere ac et mauris. Cras non tincidunt lectus. Ut porttitor leo sed tincidunt scelerisque. Nullam pellentesque purus lorem, eget mollis nulla convallis ac. In tempus tortor pellentesque metus bibendum scelerisque. Proin tristique vehicula metus at tempus.

Nunc consectetur ipsum ut erat gravida mollis sed sit amet nisi. Maecenas urna libero, bibendum eget mi vitae, condimentum pulvinar risus. Integer vel volutpat nisi. Quisque sodales diam felis, eget fringilla nunc lacinia ut. Fusce nec leo feugiat, bibendum magna hendrerit, sodales eros. Duis ultrices mi nec magna suscipit, a faucibus quam vulputate. Duis volutpat pulvinar ipsum, non dignissim justo ultrices id. Nullam sed nisl augue. Maecenas eget vehicula elit. Aliquam tristique cursus mattis. Duis volutpat placerat mi, a lacinia nisl consequat porta. Aenean non nisl pharetra, laoreet sem eget, eleifend tellus.

Curabitur id massa a ligula porttitor rhoncus. Quisque a purus sit amet orci lobortis lobortis sed vitae sem. Ut nec sapien sed elit volutpat malesuada ac sit amet velit. Proin porta diam velit, sed accumsan leo adipiscing sit amet. Morbi eu dui dictum, <strong>fermentum massa non</strong>, rhoncus ante. Cras condimentum lectus sit amet urna rutrum sollicitudin. Etiam ut dolor dui. Vestibulum pellentesque enim sit amet vestibulum consequat. Vestibulum venenatis mi non augue vulputate dictum. Sed sit amet fringilla arcu. Proin vestibulum velit consectetur arcu porta, id tincidunt orci vulputate.

Donec ut mattis eros. Nam vestibulum tellus eget nisi lacinia, et semper erat auctor. Donec pellentesque orci id urna tincidunt, in feugiat mi ornare. Integer eu dictum nulla, sit amet dictum quam. Cras lacinia augue vitae sapien interdum ornare sit amet at est. Phasellus nec gravida purus. Quisque pulvinar nunc ante, non commodo eros porttitor placerat. Nunc in sapien facilisis, placerat arcu eget, placerat velit. Nullam suscipit convallis aliquet. Donec tempus dolor blandit lectus placerat imperdiet. Duis eget sagittis eros, eget auctor sem. Proin et nibh eros. Donec et varius ante, vel fringilla urna. Suspendisse potenti.

[intense_emphasis color="primary"]Mauris vel augue cursus, dignissim dolor fringilla, volutpat elit. Vivamus ac mattis sapien. Mauris iaculis massa lectus, vel ultricies nulla hendrerit eget. Morbi pulvinar arcu non porta accumsan. Nulla facilisi. Ut bibendum placerat mauris, sed consectetur magna blandit condimentum. Pellentesque ut tempor leo.[/intense_emphasis]