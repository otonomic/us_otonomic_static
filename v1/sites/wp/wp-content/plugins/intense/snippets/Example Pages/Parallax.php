<?php
/*
Intense Snippet Name: Parallax
*/
?>

[intense_content_section background_color="#f3f3f3" image="5436" image_size="large1600" border_bottom="0" height="400" imagemode="parallax" margin_top="-30" margin_bottom="0"]
<div style="text-align: center;color: #fff !important;margin-top: 150px;font-size: 1.25em">
<h1 class="mega" style="color: #fff">Parallax Page</h1>
<p>Create stunning parallax pages.  Great for landing pages, product pages, services pages, and more!<br />Unleash your creativity.</p>
</div>
[/intense_content_section]
[intense_content_section]
[intense_row]
[intense_column size="3"]
<a href="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/FullSize.png"><img src="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/FullSize.png" alt="FullSize" width="173" height="150" class="alignnone size-full wp-image-5388" /></a>
[/intense_column]
[intense_column size="9"]
<h1 class="mega">Create</h1>
Maecenas viverra, metus eget dictum aliquam, nunc tortor pharetra felis, a aliquam mi dolor ut metus. Fusce hendrerit sem et rutrum condimentum. Donec viverra libero ut nibh laoreet egestas. Vivamus vehicula, erat quis dictum accumsan, odio dui lobortis neque, vel pharetra lorem tellus non leo. Nam consequat, augue nec congue consequat, sem nulla posuere turpis, a malesuada lacus enim sed.
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section background_color="#f3f3f3" image="5471" image_size="large1600" border_bottom="0" height="400" margin_top="50" margin_bottom="0" imagemode="parallax"]
<div style="height: 300px;text-align: center;color: #fff !important;padding: 50px 0;font-size: 1.25em">

</div>
[/intense_content_section]
[intense_content_section]
[intense_row]
[intense_column size="9"]
<h1 class="mega">Discover</h1>
Maecenas viverra, metus eget dictum aliquam, nunc tortor pharetra felis, a aliquam mi dolor ut metus. Fusce hendrerit sem et rutrum condimentum. Donec viverra libero ut nibh laoreet egestas. Vivamus vehicula, erat quis dictum accumsan, odio dui lobortis neque, vel pharetra lorem tellus non leo. Nam consequat, augue nec congue consequat, sem nulla posuere turpis, a malesuada lacus enim sed.
[/intense_column]
[intense_column size="3"]
<a href="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/FullSize.png"><img src="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/FullSize.png" alt="FullSize" width="173" height="150" class="alignnone size-full wp-image-5388" /></a>
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section height="400" background_color="#f3f3f3" image_size="large1600" image="5433" border_bottom="0" margin_top="50" margin_bottom="50" imagemode="parallax"]
<div style="height: 300px;text-align: center;color: #fff !important;padding: 50px 0;font-size: 1.25em">

</div>
[/intense_content_section]
[intense_content_section]
[intense_row]
[intense_column size="9"]
<h1 class="mega">Grow</h1>

Maecenas viverra, metus eget dictum aliquam, nunc tortor pharetra felis, a aliquam mi dolor ut metus. Fusce hendrerit sem et rutrum condimentum. Donec viverra libero ut nibh laoreet egestas. Vivamus vehicula, erat quis dictum accumsan, odio dui lobortis neque, vel pharetra lorem tellus non leo. Nam consequat, augue nec congue consequat, sem nulla posuere turpis, a malesuada lacus enim sed.
[/intense_column]
[intense_column size="3"]
<a href="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/calculator1.jpg"><img src="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/calculator1-300x300.jpg" alt="calculator" width="300" height="300" class="alignnone size-medium wp-image-5430" /></a>
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section height="400" background_color="#f3f3f3" image_size="large1600" image="5535" border_bottom="0" margin_bottom="-30" margin_top="0" imagemode="parallax"]
<div style="height: 300px;text-align: center;color: #fff !important;padding: 50px 0;font-size: 1.25em">

</div>
[/intense_content_section]