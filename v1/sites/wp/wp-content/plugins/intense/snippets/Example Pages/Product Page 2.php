<?php
/*
Intense Snippet Name: Product Page 2
*/
?>

[intense_content_section border_top="0" border_bottom="0" margin_bottom="0" margin_top="0" background_color="#fff"]
<div style="margin-top: 25px;margin-bottom: 25px;text-align: center">
<h1 class="mega">Intense Plugin <img class="alignnone size-full wp-image-5388" alt="FullSize" src="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/06/FullSize.png" width="173" height="150" /></h1>
[intense_lead]Intense is an all-inclusive plugin with many shortcodes and options.[/intense_lead]
[intense_button size="large" type="success" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Purchase Plugin[/intense_button]

</div>
[/intense_content_section]
[intense_animated trigger="scroll" type="bounceInUp"]
[intense_image image="7813" size="large2048" title="bungalows" /]
[/intense_animated]
<div class="pull-left" style="width: 50%">[intense_animated trigger="scroll" type="fadeInUp"]
[intense_image image="5471" size="large1024" title="bungalows" /]
[/intense_animated]</div>
<div class="pull-left" style="width: 50%">[intense_animated trigger="scroll" type="fadeInUp"]
[intense_image image="5465" size="large1024" title="bungalows" /]
[/intense_animated]</div>
<div class="clearfix"></div>
[intense_animated trigger="scroll" type="fadeInUp"]
[intense_image image="7815" size="large2048" title="bungalows" /]
[/intense_animated]

[intense_content_section border_top="0" border_bottom="0" margin_bottom="-30" margin_top="0" background_color="#efefef" image="3856" imagemode="repeat"]
[intense_hr type="dashed" icon_type="time" icon_size="2" title_position="center" title_background_color="#ddd" /]
<div style="margin-top: 100px;margin-bottom: 100px;text-align: center">
<h1 class="mega">What are you waiting for?</h1>
[intense_lead]The time has never been better than now.[/intense_lead]
[intense_button size="large" type="primary" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Purchase Plugin[/intense_button]

</div>
[intense_hr type="dashed" /]
[/intense_content_section]