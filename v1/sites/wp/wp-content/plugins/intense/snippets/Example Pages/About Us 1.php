<?php
/*
Intense Snippet Name: About Us 1
*/
?>

[intense_content_section]
[intense_row]
[intense_column size="6"]
[intense_slider transition="fade"]
[intense_slide]
[intense_image size="medium500" image="6007" /]
[/intense_slide]
[intense_slide]
[intense_image size="medium500" image="6014" /]
[/intense_slide]
[intense_slide]
[intense_image size="medium500" image="5422" /]
[/intense_slide]
[intense_slide]
[intense_image size="medium500" image="6016" /]
[/intense_slide]
[/intense_slider]
[/intense_column]
[intense_column size="6"]
<h2>Welcome to Intensity</h2>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris convallis nisl quis magna vehicula laoreet. Vivamus vestibulum dui et turpis pharetra cursus. Duis consectetur purus porttitor ante ultrices dapibus. Donec vitae sagittis est. Sed venenatis consequat accumsan. Mauris venenatis dapibus sapien sed fermentum. Fusce quis suscipit neque.

Nullam ornare justo eu lorem vehicula molestie. Nulla ultricies aliquet felis ut iaculis. Nulla facilisi. Nunc nulla magna, scelerisque ac diam ut, vestibulum dignissim odio. Aenean at mattis leo. Sed vitae cursus nisl. Aenean porttitor ullamcorper magna nec dapibus.

Duis lacinia elementum arcu. Nam porttitor mauris justo, id tristique velit fermentum a. Nunc sit amet erat ut mauris ullamcorper lobortis. Donec auctor metus ut velit ullamcorper, ut eleifend ligula eleifend.

Sed fermentum nulla arcu, a euismod nunc vehicula tincidunt. Nunc accumsan lobortis vestibulum. Phasellus auctor orci quis erat vulputate aliquet. In vitae velit non enim rutrum dictum.
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_hr size="large" /]
[intense_content_section]
<h2>Our Team</h2>
[intense_row]
[intense_column size="4"]
[intense_person image="5439" size="small320" name="Jim Wales" title="Chief Executive Officer"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vehicula lacus et orci commodo non tempus nibh pharetra. Nam et lacus eget mi aliquet dignissim ut eget est. Praesent quis urna nisl, sed euismod nibh.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5438" size="small320" name="Margaret Fetters" title="Chief Financial Officer"]
Nullam ornare justo eu lorem vehicula molestie. Nulla ultricies aliquet felis ut iaculis. Nulla facilisi. Nunc nulla magna, scelerisque ac diam ut, vestibulum dignissim odio. Aenean at mattis leo.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5440" size="small320" name="Alex Beck" title="Chief Technical Officer"]
Duis lacinia elementum arcu. Nam porttitor mauris justo, id tristique velit fermentum a. Nunc sit amet erat ut mauris ullamcorper lobortis. Donec auctor metus ut velit ullamcorper, ut eleifend ligula eleifend.
[/intense_person]
[/intense_column]
[/intense_row]
[intense_row]
[intense_column size="4"]
[intense_person image="5443" size="small320" name="Nadia Osin" title="Vice President of Sales"]
Sed fermentum nulla arcu, a euismod nunc vehicula tincidunt. Nunc accumsan lobortis vestibulum. Phasellus auctor orci quis erat vulputate aliquet. In vitae velit non enim rutrum dictum.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5441" size="small320" name="June Harper" title="Art Director"]
Quisque tortor nunc, feugiat ac facilisis sit amet, rutrum vitae magna. Aliquam fringilla sem non ante congue, id ultrices velit blandit. Vivamus at hendrerit nulla. Ut sit amet diam non nibh consequat sodales non fringilla nisi.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5442" size="small320" name="Andrew Parson" title="Account Executive"]
Ut id ligula facilisis, vulputate est in, feugiat libero. Fusce condimentum lorem risus, vel ultrices lectus laoreet vel. Integer eleifend eget ligula eget convallis. Cras magna erat, auctor eget tortor quis, eleifend egestas risus.
[/intense_person]
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_hr size="large" /]
[intense_content_section]
<h2>Our Clients</h2>

[intense_row]
[intense_column size="8"]
[intense_slider type="owl-carousel" navigation_color="#eee" navigation_font_color="#575757" navigation_position="middleoutside" items="2" items_desktop="2" items_tablet="2" items_mobile="1"]
[intense_slide][intense_image size="medium" image="8427" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8433" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8428" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8434" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8429" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8435" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8430" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8440" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8431" /][/intense_slide]
[intense_slide][intense_image size="medium" image="8437" /][/intense_slide]
[/intense_slider]
[/intense_column]
[intense_column size="4"]
[intense_testimonies type="slider" template="quote_bubble"]
[intense_testimony author="John Doe" company="Intense Visions" image="http://1.gravatar.com/avatar/929b8350708873d9e87c9dbd164add5d?s=50&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D50&amp;r=G"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ultrices tempor tristique. Curabitur convallis fermentum ante rutrum viverra. Aliquam pharetra, risus vel mattis luctus, tortor nibh pulvinar est, ac venenatis nunc justo a nulla. Nam enim arcu, consectetur eget vestibulum.
[/intense_testimony]
[intense_testimony author="Jane Doe" company="Intense Visions" link="http://intensity.intensevisions.com" ]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vulputate metus id dolor ornare vel ullamcorper augue ultrices. Ut posuere arcu eu velit cursus id ultricies purus condimentum. Duis sagittis, sem a dictum porttitor, urna lacus rutrum lectus, quis rutrum.[/intense_testimony]
[/intense_testimonies]
[/intense_column]
[/intense_row]
[/intense_content_section]