<?php
/*
Intense Snippet Name: Our Office 1
*/
?>

[intense_content_section margin_top="-30" margin_bottom="0" border_top="0" border_bottom="10" height="500" image="7151" imagemode="full"]

[/intense_content_section]
[intense_content_section]
[intense_row]
[intense_column size="6"]
[intense_hr title="Come Visit Us" /]
[intense_lead]We would love to have you stop in[/intense_lead]

In tempus magna vel fringilla iaculis. In nec rhoncus nunc, quis sodales magna. Donec in congue massa. Aenean in risus auctor, scelerisque eros et, condimentum tellus. Duis et lorem vel eros pretium adipiscing. Fusce eu ullamcorper odio. Curabitur faucibus libero sit amet ipsum pretium, nec adipiscing tellus accumsan. Cras facilisis neque vitae ultricies vulputate. Morbi bibendum, nulla ut ornare gravida, odio odio ultricies dolor, sit amet dictum ligula erat ac nunc.

In eget sollicitudin mauris. Pellentesque hendrerit elit velit, eu bibendum metus elementum non. Vestibulum nulla turpis, fringilla ut diam eget, venenatis adipiscing orci. Cras a faucibus metus. Donec ac enim convallis lorem vulputate tristique quis non nibh. Aliquam volutpat in nisl eu auctor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce nec est ac mauris eleifend adipiscing. Vestibulum lobortis eros et mattis sollicitudin.

[intense_emphasis type="inverse"]Cras libero dolor, venenatis in libero vitae, aliquam ultricies ligula. Pellentesque blandit nisi nec augue porta, eu fringilla quam commodo. Morbi mattis ipsum eu dui convallis adipiscing. Praesent quis purus mi. Phasellus lacinia tellus sed accumsan imperdiet.[/intense_emphasis]
[/intense_column]
[intense_column size="6"]
[intense_slider transition="fade"]
[intense_slide]
[intense_image image="5424" size="medium500" /]
[/intense_slide]
[intense_slide]
[intense_image image="5470" size="medium500" /]
[/intense_slide]
[intense_slide]
[intense_image image="6016" size="medium500" /]
[/intense_slide]
[/intense_slider]
[/intense_column]
[/intense_row]
[intense_row]
[intense_column size="6"]
[intense_hr title="Location" /]
[intense_lead]105 N. Main Street, Fort Wayne, Indiana 46806[/intense_lead][intense_icon type="phone" /] 1-800-867-5309 [intense_icon type="globe" /] www.yourwebsite.com

[intense_table columns="MON,TUE, WED, THU, FRI, Weekends" data=" 8am-5pm,8am-5pm,8am-5pm,8am-5pm,8am-5pm, Closed"/]
[/intense_column]
[intense_column size="6"]
[intense_map]
[intense_map_marker address="Fort Wayne, IN" title="Our Office" description="" /]
[/intense_map]
[/intense_column]
[/intense_row]
[/intense_content_section]