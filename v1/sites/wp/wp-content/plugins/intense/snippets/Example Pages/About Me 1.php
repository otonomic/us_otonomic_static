<?php
/*
Intense Snippet Name: About Me 1
*/
?>

[intense_content_section]
[intense_row]
[intense_column size="7"]
[intense_image image="5438" size="postWide"  shadow="8" /]
[/intense_column]
[intense_column size="5"]
[intense_hr title="Hi, My Name is Alice" type="double" /] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fringilla id orci id consectetur. Pellentesque purus metus, egestas non orci sed, tempor scelerisque massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc convallis vestibulum massa, imperdiet gravida quam. Cras eget metus dolor. Nulla facilisi. Duis iaculis pellentesque pharetra. Duis sed ante eu leo faucibus congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus ultricies est eu urna vestibulum pulvinar. Nullam viverra diam nec consectetur cursus. Nunc at mattis tellus.
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_hr type="double" icon_type="trophy" title_position="center" /]
[intense_content_section]
[intense_row]
[intense_column size="5"]
[intense_promo_box shadow="5"]
<h2>Skills</h2>
Sed sed commodo nulla. Pellentesque sed felis a nunc malesuada congue non ut turpis. Nulla mattis pharetra tincidunt. Curabitur dictum vestibulum sapien vulputate auctor. Vivamus sed imperdiet est. Mauris ac arcu massa. Vivamus rhoncus interdum hendrerit. Vivamus nec dapibus eros. Quisque ac adipiscing augue. Nullam tincidunt vel sapien in volutpat.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim arcu enim, ut porttitor magna tempor sit amet. Vestibulum enim erat, semper nec risus sit amet, tristique varius mauris. Vivamus mollis congue sem, eu congue nisl luctus sit amet. In eget diam sem. Donec laoreet metus sit amet mauris congue.
[/intense_promo_box]
[/intense_column]
[intense_column size="7"]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="95" text="HTML"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="90" text="CSS"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="80" text="JavaScript"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="75" text="WordPress"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="82" text="Photoshop"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="70" text="Illustrator"][/intense_progress_segment]
[/intense_progress]
[intense_progress animation="1" size="large"]
[intense_progress_segment color="primary" value="93" text="Design"][/intense_progress_segment]
[/intense_progress]
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_hr type="double" icon_type="bookmark" title_position="center" /]
[intense_content_section]
<h2>Recent Work</h2>
[intense_recent_portfolios columns="3" post_count="6" show_title="0" show_thumbnail="1" show_excerpt="0" image_size="medium500" show_missing_image="1" /]
[/intense_content_section]