<?php
/*
Intense Snippet Name: Parallax Full Height
*/
?>

[intense_content_section breakout="1" padding_top="150" image="9792" imagemode="parallax" full_height="1" show_advance="1" advance_color="primary" advance_text="More" advance_icon="hand-down" advance_icon_position="right"]
[intense_promo_box size="mega"]
<h1>Set Yourself Apart</h1>
Content sections are perfect for making a lasting impression. Choose many different background options including: parallax, repeating image, fixed image, full size image. You can also make the content sections fit the entire window width, height, or both.
[intense_button size="large" link="http://codecanyon.net/item/intense-shortcodes-wordpress-plugin/5600492" target="_blank"]Buy Now[/intense_button]
[/intense_promo_box]
[/intense_content_section]
[intense_content_section breakout="1" background_color="#1a8be2" border_bottom="0"  padding_top="250" full_height="1" show_advance="1" advance_color="success"]
[intense_row]
[intense_column size="6"]
[intense_promo_box shadow="8"]
<h2>Set Background Color</h2>
Add a content section, like this one, that has a background color of your choice.
[/intense_promo_box]
[/intense_column]
[intense_column size="6"]
<div style="color: #fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis commodo lectus eget fermentum. Proin molestie enim egestas odio mollis ultricies. Sed urna erat, hendrerit sed euismod sit amet, ullamcorper at felis. Sed mollis, tortor a blandit commodo, purus risus accumsan libero, vitae accumsan nunc felis fringilla eros. Fusce porttitor risus id magna scelerisque iaculis. In at elementum nibh. Etiam.</div>
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section breakout="1" background_color="#f3f3f3" image="3856" border_bottom="0" imagemode="repeat" margin_top="0" margin_bottom="0" padding_top="250" full_height="1" show_advance="1" advance_color="warning"]
[intense_row]
[intense_column size="6"]
[intense_promo_box background_color="primary" shadow="8"]
<h2>Set Background Image Modes <small>repeat, repeat-x, repeat-y</small></h2>
Add a content section that has a background image of your choice and make it repeat horizontally, vertically, or both.
[/intense_promo_box]
[/intense_column]
[intense_column size="6"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis commodo lectus eget fermentum. Proin molestie enim egestas odio mollis ultricies. Sed urna erat, hendrerit sed euismod sit amet, ullamcorper at felis. Sed mollis, tortor a blandit commodo, purus risus accumsan libero, vitae accumsan nunc felis fringilla eros. Fusce porttitor risus id magna scelerisque iaculis. In at elementum nibh. Etiam.
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section breakout="1" background_color="#f3f3f3" image="9769" border_bottom="0" imagemode="parallax" margin_top="0" margin_bottom="0" padding_top="250" full_height="1" show_advance="1" advance_color="inverse"]
[intense_row]
[intense_column size="6"]
[intense_promo_box shadow="8"]
<h2>Set Background Image Mode <small>parallax</small></h2>
Add a content section that has a background image with a parallax effect. Parallax is an effect where the background moves at a different speed from the foreground. In this example, the background image is moving slightly faster than the rest of the page as you scroll.
[/intense_promo_box]
[/intense_column]
[intense_column size="6"]
<div style="color: #333; background: rgba(255,255,255,0.75); padding: 19px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis commodo lectus eget fermentum. Proin molestie enim egestas odio mollis ultricies.Sed urna erat, hendrerit sed euismod sit amet, ullamcorper at felis. Sed mollis, tortor a blandit commodo, purus risus accumsan libero, vitae accumsan nunc felis fringilla eros. Fusce porttitor risus id magna scelerisque iaculis. In at elementum nibh.

</div>
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section breakout="1" background_color="#f3f3f3" image="8181" imagemode="fixed" margin_top="0" margin_bottom="0" full_height="1" padding_top="250" full_height="1" show_advance="1" advance_color="muted"]
[intense_row]
[intense_column size="6"]
[intense_promo_box shadow="8"]
<h2>Set Background Image Mode <small>fixed</small></h2>
Add a content section that has a background image with a fixed parallax effect. Parallax is an effect where the background moves at a different speed from the foreground. In this example, the background image is staying fixed while the rest of the page is scrolled.
[/intense_promo_box]
[/intense_column]
[intense_column size="6"]
<div style="color: #333; background: rgba(255,255,255,0.75); padding: 19px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis commodo lectus eget fermentum. Proin molestie enim egestas odio mollis ultricies.Sed urna erat, hendrerit sed euismod sit amet, ullamcorper at felis. Sed mollis, tortor a blandit commodo, purus risus accumsan libero, vitae accumsan nunc felis fringilla eros. Fusce porttitor risus id magna scelerisque iaculis. In at elementum nibh.

</div>
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section breakout="1" background_color="#f3f3f3" image="7489" imagemode="full" margin_top="0" margin_bottom="-30" full_height="1" padding_top="250" full_height="1"]
[intense_row]
[intense_column size="6"]
[intense_promo_box shadow="8"]
<h2>Set Background Image Mode <small>full</small></h2>
Add a content section that has a background image that fills the full content box.
[/intense_promo_box]
[/intense_column]
[intense_column size="6"]
<div style="color: #333; background: rgba(255,255,255,0.75); padding: 19px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis commodo lectus eget fermentum. Proin molestie enim egestas odio mollis ultricies.Sed urna erat, hendrerit sed euismod sit amet, ullamcorper at felis. Sed mollis, tortor a blandit commodo, purus risus accumsan libero, vitae accumsan nunc felis fringilla eros. Fusce porttitor risus id magna scelerisque iaculis. In at elementum nibh.

</div>
[/intense_column]
[/intense_row]
[/intense_content_section]