<?php
/*
Intense Snippet Name: Product Page 1
*/
?>

[intense_content_section border_top="0" border_bottom="0" margin_bottom="0" margin_top="-30" background_color="#EFF2F5" ]
<div style="height: 200px"></div>
<h1 class="mega">Hello and Welcome</h1>
[intense_lead]We create beautiful things used by great people all over the world. Come and create with us.[/intense_lead]

[intense_lead]<i class="icon-quote-left"></i> Beauty is unbearable, drives us to despair, offering us for a minute the glimpse of an eternity that we should like to stretch out over the whole of time. <i class="icon-quote-right"></i> -Albert Camus
[/intense_lead]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum commodo augue et blandit. Mauris vel ultrices lectus. Etiam at sapien non odio suscipit tincidunt. Proin euismod ligula et leo commodo, vel ornare elit posuere. Cras egestas tellus a lectus gravida, vel consectetur sapien vulputate. Ut pellentesque, velit id commodo tristique, velit augue interdum arcu, vel scelerisque mi ante quis mi.

Donec vel augue velit. Etiam sollicitudin pharetra massa id auctor. Nam mi odio, pharetra vel eros eget, ultrices hendrerit urna. Donec vitae metus dolor. Aenean non enim libero. Nulla accumsan risus elementum, ultrices neque quis, pulvinar ligula. Donec nec tortor fermentum, rhoncus augue a, facilisis nulla. Cras vulputate dictum venenatis. Etiam porta eu tellus at imperdiet.
<div style="height: 200px"></div>
[/intense_content_section]
[intense_content_section border_top="0" border_bottom="0" margin_bottom="0" margin_top="0" background_color="#BE4B39"]
<div style="margin-top: 150px;margin-bottom: 150px;text-align: center;color: #fff">
<h1 class="mega" style="color: #fff">Intense Plugin <i class="icon-code"></i></h1>
[intense_lead]Add power to your WordPress site. How can you possibly go wrong?[/intense_lead]
[intense_button size="large" type="inverse" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Purchase Plugin[/intense_button]

</div>
[/intense_content_section]
[intense_content_section border_top="0" border_bottom="0" margin_bottom="0" margin_top="0" background_color="#FFB600"]
<div style="margin-top: 150px;margin-bottom: 150px;text-align: center;color: #fff">
<h1 class="mega" style="color: #fff">World Class Support <i class="icon-globe"></i></h1>
[intense_lead]You won't be left alone. We are here to help you when needed.[/intense_lead]
[intense_button size="large" type="primary" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Get Support[/intense_button]

</div>
[/intense_content_section]
[intense_content_section border_top="0" border_bottom="0" margin_bottom="-30" margin_top="0" background_color="#EFF2F5"]
<div style="margin-top: 200px;margin-bottom: 200px">
<h1 class="mega">What are you waiting for?</h1>
[intense_lead]The time has never been better than now.[/intense_lead]
[intense_button size="large" type="primary" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Purchase Plugin[/intense_button]

</div>
[/intense_content_section]