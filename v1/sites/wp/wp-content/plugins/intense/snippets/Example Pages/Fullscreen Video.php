<?php
/*
Intense Snippet Name: Fullscreen Video
*/
?>

[intense_content_section]
[intense_fullscreen_video video="http://www.youtube.com/watch?v=cmRaVoqA5Fk" key_control="1" highdef="1" annotations="0" captions="0" /]
[intense_promo_box link="#" button_text="Buy Now" button_link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions" shadow="12"]
<h2>Simply Beautiful Fullscreen Video</h2>
[intense_lead]Fullscreen videos from YouTube and Vimeo.[/intense_lead]

[intense_button size="medium" link="http://www.youtube.com/user/mockmoon2000/" color="#d6d6d6" icon="youtube" icon_position="right"]Video by mockmoon2000[/intense_button]
[/intense_promo_box]
[/intense_content_section]
