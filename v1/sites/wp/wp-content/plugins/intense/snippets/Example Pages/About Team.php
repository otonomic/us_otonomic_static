<?php
/*
Intense Snippet Name: About Team
*/
?>

[intense_content_section border_bottom="1" border_top="0" background_color="#efefef" image="3856" imagemode="repeat"]
<h2>Executive Leaders</h2>
[intense_row]
[intense_column size="4"]
[intense_person image="5439" imageshadow="10" size="square400" name="Jim Wales" title="Chief Executive Officer" facebook="#" googleplus="#" linkedin="#" twitter="#"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vehicula lacus et orci commodo non tempus nibh pharetra. Nam et lacus eget mi aliquet dignissim ut eget est. Praesent quis urna nisl, sed euismod nibh.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5438" imageshadow="10" size="square400" name="Margaret Fetters" title="Chief Financial Officer" facebook="#" googleplus="#" linkedin="#"]
Nullam ornare justo eu lorem vehicula molestie. Nulla ultricies aliquet felis ut iaculis. Nulla facilisi. Nunc nulla magna, scelerisque ac diam ut, vestibulum dignissim odio. Aenean at mattis leo.
[/intense_person]
[/intense_column]
[intense_column size="4"]
[intense_person image="5440" imageshadow="10" size="square400" name="Alex Beck" title="Chief Technical Officer" facebook="#" googleplus="#" linkedin="#"]
Duis lacinia elementum arcu. Nam porttitor mauris justo, id tristique velit fermentum a. Nunc sit amet erat ut mauris ullamcorper lobortis. Donec auctor metus ut velit ullamcorper, ut eleifend ligula eleifend.
[/intense_person]
[/intense_column]
[/intense_row]
[/intense_content_section]
[intense_content_section]
<h3>Additional Leaders</h3>
[intense_row]
[intense_column size="6"]
[intense_person image="5443" size="square150" name="Nadia Osin" title="Vice President of Sales" template="left" facebook="#" twitter="#"]
Sed fermentum nulla arcu, a euismod nunc vehicula tincidunt. Nunc accumsan lobortis vestibulum. Phasellus auctor orci quis erat vulputate aliquet. In vitae velit non enim rutrum dictum.
[/intense_person]
[/intense_column]
[intense_column size="6"]
[intense_person image="5441" size="square150" name="June Harper" title="Art Director" template="left" dribbble="#" twitter="#" ]
Quisque tortor nunc, feugiat ac facilisis sit amet, rutrum vitae magna. Aliquam fringilla sem non ante congue, id ultrices velit blandit. Vivamus at hendrerit nulla. Ut sit amet diam non nibh consequat sodales non fringilla nisi.
[/intense_person]
[/intense_column]
[/intense_row]
[intense_row]
[intense_column size="6"]
[intense_person image="5442" size="square150" name="Andrew Parson" title="Account Executive" template="left" linkedin="#" facebook="#"]
Ut id ligula facilisis, vulputate est in, feugiat libero. Fusce condimentum lorem risus, vel ultrices lectus laoreet vel. Integer eleifend eget ligula eget convallis. Cras magna erat, auctor eget tortor quis, eleifend egestas risus.
[/intense_person]
[/intense_column]
[intense_column size="6"]
[intense_person image="7082" size="square150" name="Sara Jones" title="Account Executive" template="left" googleplus="#" facebook="#"]
Consequat sodales non fringilla, vulputate est in, feugiat libero. Fusce nam porttitor mauris justo, id tristique velit, vel ultrices lectus laoreet vel. Integer eleifend eget ligula eget convallis. Cras magna erat, auctor eget tortor quis, eleifend egestas risus.
[/intense_person]
[/intense_column]
[/intense_row]
[intense_row]
[intense_column size="6"]
[intense_person image="7081" size="square150" name="Hannah Erikson" title="Public Relations" template="left" googleplus="#" facebook="#"]
Consequat sodales non fringilla, vulputate est in, feugiat libero. Fusce nam porttitor mauris justo, id tristique velit, vel ultrices lectus laoreet vel. Integer eleifend eget ligula eget convallis. Cras magna erat, auctor eget tortor quis, eleifend egestas risus.
[/intense_person]
[/intense_column]
[intense_column size="6"]
[intense_person image="7079" size="square150" name="William Parker" title="Hardware Engineering" template="left" linkedin="#" facebook="#"]
Ut id ligula facilisis, vulputate est in, feugiat libero. Fusce condimentum lorem risus, vel ultrices lectus laoreet vel. Integer eleifend eget ligula eget convallis. Cras magna erat, auctor eget tortor quis, eleifend egestas risus.
[/intense_person]
[/intense_column]
[/intense_row]
[/intense_content_section]