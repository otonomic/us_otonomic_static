<?php
/*
Intense Snippet Name: Landing Page
*/
?>

[intense_content_section padding_top="15" padding_bottom="40" show_advance_arrow="1" advance_arrow_background_color="#efefef" advance_arrow_position="50"]
[intense_heading font_size="58" line_height="60" align="center"]
Powerful WordPress Site Builder Plugin
[/intense_heading]
[intense_heading tag="h3" align="center" font_color="primary" font_size="25" line_height="26"]
Intense lets you easily create websites using shortcodes, custom post types, <br />templates, snippets, and more![/intense_heading]

[intense_row padding_top="0"]
[intense_column size="9" medium_size="9"]
[intense_spacer height="30"]
[intense_icon_list]
[intense_icon_list_item type="ok" color="#24b45e" stack_color="#ffffff"]
<strong>Save Time</strong> - create websites faster without sacrificing quality
[/intense_icon_list_item]
[intense_icon_list_item type="ok" color="#24b45e" stack_color="#ffffff"]
<strong>Impress Your Clients</strong> - easily build websites and become a superhero for your clients
[/intense_icon_list_item]
[intense_icon_list_item type="ok" color="#24b45e" stack_color="#ffffff"]
<strong>Eliminate Repetitive Tasks</strong> - use a growing library of reusable content or create your own 
[/intense_icon_list_item]
[intense_icon_list_item type="ok" color="#24b45e" stack_color="#ffffff"]
<strong>Save Money</strong> - skip purchasing dozens of plugins to get the job done and save hundreds of dollars
[/intense_icon_list_item]
[intense_icon_list_item type="ok" color="#24b45e" stack_color="#ffffff"]
<strong>Top Support and Free Updates</strong> - get the best support possible including free updates forever
[/intense_icon_list_item]
[intense_icon_list_item type="heart" color="error" stack_color="#ffffff"]
<strong>Help Build Schools</strong> - Part of each sale is given to help build schools in developing countries. [intense_permalink id="2797"]Learn more[/intense_permalink]
[/intense_icon_list_item]
[/intense_icon_list]

[intense_spacer height="50"]

<div style="text-align: center; padding-bottom: 25px;">
<span style="font-size: 50px; padding: 20px; vertical-align: middle;">[intense_icon type="double-angle-right" color="#2CD36F"]</span> [intense_button color="#24B45E" size="large" border_radius="2px" padding_right="60" padding_left="60" link="http://codecanyon.net/item/intense-shortcodes-wordpress-plugin/5600492?ref=IntenseVisions"][intense_snippet snippet_id="Child Theme | Buttons/buy-text" snippet_title="Buy Text"][/intense_button] <span style="font-size: 50px; padding: 20px; vertical-align: middle;">[intense_icon type="double-angle-left" color="#2CD36F"]</span><br/><smaller>Free updates forever!</smaller>
</div>

[/intense_column]
[intense_column size="3" medium_size="3"][intense_image image="901" alt="site-builder" width="230" height="300" class="alignnone size-medium wp-image-899" title="swiss-army-knife" bgcolor="#ffffff"][/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section background_type="color" background_color="#efefef" border_top="1" border_bottom="1" show_advance_arrow="0" advance_arrow_position="25"]
[intense_heading align="center"]Happy Customers Worldwide[/intense_heading]
[intense_heading tag="h3" align="center" font_color="primary" font_size="20" line_height="24"]
Our customers are found all over the world and have built small to very large, high-traffic, websites.[/intense_heading]

[intense_testimonials post_type="intense_testimonials" taxonomy="intense_testimonials_category" template="text" infinite_scroll="1" show_filter="0" filter_effects="fade, scale" navigation_color="#111111" categories="landing-page" is_masonry="1" gutter="10" masonry_width="50%" masonry_sticky_width="100%" posts_per_page="30"]

<center>
[intense_button border_radius="2px" color="#E6674A" link="15"]
More Testimonials
[/intense_button]
</center>
[/intense_content_section]

[intense_content_section background_type="image" imagesize="large1024" image="2799" imagemode="parallax" show_advance_arrow="1" advance_arrow_position="25" padding_top="25" ]
[intense_row]
[intense_column size="8" medium_size="8" small_size="8" extra_small_size="8"][/intense_column]
[intense_column size="4" medium_size="4" small_size="4" extra_small_size="4"]
<h2>We Proudly Support</h2>
[intense_image image="2801" alt="Pencils of Promise" title="Pencils of Promise"]

A portion of each sale is given to build schools and help educate children in developing countries.
[intense_spacer height="5"]
<center>
[intense_button color="#f7b205" hover_brightness="20" size="large" border_radius="2px" link="2797"]
Learn More
[/intense_button]
</center>
[intense_spacer height="50"]
<div class="pull-right">PHOTO CREDIT: NICK ONKEN</div>
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section show_advance_arrow="1" advance_arrow_background_color="#efefef" advance_arrow_position="69" padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="8" medium_size="8"]
<h1>[intense_template template="shortcode-count"] Incredible Shortcodes</h1>
Shortcodes are the building blocks of a great site. They save you time and let you focus on your content and design.

Intense includes the most basic to advanced shortcodes available. Each shortcode includes useful options to customize the shortcode to fit your design perfectly.

Building pages doesn't have to be so painful. Explore the complete list and start creating content today.

[intense_button border_radius="2px" color="#E6674A" link="40"]
Explore Shortcodes
[/intense_button]
[/intense_column]
[intense_column size="4" medium_size="4"]
[intense_image image="119" title="amazing shortcodes" alt="amazing shortcodes"]
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section background_type="color" background_color="#efefef" border_top="1" border_bottom="1" show_advance_arrow="1" advance_arrow_position="25" padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="4" medium_size="4"]
[intense_image image="1236" title="site-builder-gear" alt="Custom Post Types"]
[/intense_column]
[intense_column size="8" medium_size="8"]
<h1>[intense_template template="custom-post-type-count"] Useful Custom Post Types</h1>
Custom post types create content beyond the standard WordPress post or page. Intense includes the most useful custom post types used to supercharge your site.

Want to add a portfolio, recipe, job opening, and more to your site? Easily do so with Intense custom post types. There are too many to list here so explore the complete listing along with detailed information about each custom post type.

[intense_button border_radius="2px" color="#E6674A" link="45"]
Explore Custom Post Types
[/intense_button]
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section show_advance_arrow="1" advance_arrow_background_color="#efefef" advance_arrow_position="43" padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="8" medium_size="8"]
<h1>Powerful Template Engine</h1>
Intense comes with several beautiful shortcode layouts. When these layouts aren't exactly what you need or if you just want to do a little tweaking, Intense allows you to easily edit these layouts using templates. If you find a layout on a site you love, quickly make it your own by creating a template to match. 

For example, if you came across a site that showed their employee listing in a way that caught your eye, you could create a template for the person shortcode to match the look for your own site. The templates are open to your imagination and needs.

[intense_button border_radius="2px" color="#E6674A" link="50"]
Explore Template Engine
[/intense_button]
[/intense_column]
[intense_column size="4" medium_size="4"]
[intense_image image="1231" size="square300" title="Template Engine" caption="Powerful Template Engine"]
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section show_advance_arrow="1" background_color="#efefef" padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="3" medium_size="3"]
[intense_image image="982" alt="Snippets" size="square300" title="Snippets" caption="Snippets are powerful reusable content"]
[/intense_column]
[intense_column size="1" medium_size="1"]
[/intense_column]
[intense_column size="8" medium_size="8"]
<h1>Snippets</h1>
Building a website doesn't have to start from scratch. Reuse content snippets to get you started. There are dozens of included snippets or create your own. Save them as files or in WordPress to reuse over and over again. 

When you have a stroke of design brilliance, it is comforting to know you can easily take it with you on your next project or page.

[intense_button border_radius="2px" color="#E6674A" link="47"]
Explore Snippets
[/intense_button]
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section border_top="1" border_bottom="1" padding_bottom="50" padding_top="50"]
<center>
<h1>Easy Shortcode Creation with Previews and Presets</h1>
Creating shortcodes couldn't be easier. Get a glimpse at how the shortcode will look using previews. Play around with the settings until you are happy with the way the shortcode looks. Once you have a shortcode set up just right, save the settings using presets.
</center>
[intense_row padding_top="0"]
[intense_column size="1" medium_size="1" small_size="1" extra_small_size="1"][/intense_column]
[intense_column size="10" medium_size="10" small_size="10" extra_small_size="10"][intense_video video_type="youtube" video_url="https://www.youtube.com/watch?v=CRtgnF_b2po"][/intense_column]
[intense_column size="1" medium_size="1" small_size="1" extra_small_size="1"][/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section background_type="color" background_color="#efefef" border_top="1" border_bottom="1" padding_top="70" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="6" medium_size="6"]
[intense_image image="3242" alt="Performance Minded" title="Performance Minded"]
[/intense_column]
[intense_column size="6" medium_size="6"]
<h1>Performance Minded</h1>
Your site performance is a big deal. Intense is designed and created with performance in mind. We constantly work to tune Intense to optimal performance so that you get the best experience possible. 

We use the best tools available to test and remove bottlenecks to keep your website going. 
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section]
<h1 style="text-align:center">Intense Is Mobile Responsive</h1>
<center>
[intense_image image="3239" size="postWide" title="Intense is Mobile Responsive" bgcolor="#ffffff"]
</center>
[/intense_content_section]

[intense_content_section background_type="color" background_color="#efefef" border_top="1" border_bottom="1" padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="8" medium_size="8"]
[intense_heading]Add Strength to Visual Composer[/intense_heading]
If you prefer to use a drag and drop page builder, the integration between Visual Composer and Intense is like a match made in heaven. Easily add Intense shortcodes within your Visual Composer layouts. You can even use several of the Intense Shortcodes as containers.

Rather not use a drag and drop page builder? Feel free to use our intuitive shortcode creation tools by themselves. The choice and flexibility are available to you.

[intense_button border_radius="2px" color="#E6674A" link="http://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=IntenseVisions"]
Buy Visual Composer - $30
[/intense_button]
<br />
*Not included
[/intense_column]
[intense_column size="4" medium_size="4"]
[intense_image image="1898" size="square300" title="Visual Composer Integration" caption="Works with Visual Composer"]
[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section padding_top="50" padding_bottom="70"]
[intense_row padding_top="0"]
[intense_column size="4" medium_size="4"]

[intense_image image="1314" size="square300" title="Top Support" caption="Get the best support possible"]

[/intense_column]
[intense_column size="8" medium_size="8"]
<h1>#1 Support - You Are in Good Hands</h1>
Intense comes with the best support available. We go above and beyond to provide the help that you need. No problem is too big or small. When problems arise, you won't be left alone.

We love to answer pre-purchase questions about Intense's features. Contact us today.

[intense_spacer height="30"]

[intense_testimonials post_type="intense_testimonials" taxonomy="intense_testimonials_category" template="text" categories="support" filter_effects="fade, scale" is_slider="1" items="1" items_desktop="1" items_desktop_small="1" items_tablet="1" navigation_size="small" navigation_color="#ffffff" navigation_font_color="#444444"]

[/intense_column]
[/intense_row]
[/intense_content_section]

[intense_content_section padding_bottom="0" background_type="color" background_color="#efefef" border_top="1" border_bottom="1" id="notification"]
[intense_promo_box size="large" background_color="#ffffff"]
[intense_row padding_top="0"]
[intense_column size="10" medium_size="10" small_size="8" extra_small_size="8"]
<h1>Get Notified About Important News and Updates</h1>
We have a lot of exciting changes in the works. Enter your email below and get notified about new releases. 

[/intense_column]
[intense_column size="2" medium_size="2" small_size="4" extra_small_size="4"]
<span style="font-size: 140px">[intense_icon type="bullhorn" color="#efefef"]</span>
[/intense_column]
[/intense_row]
[/intense_promo_box]
[/intense_content_section]

[intense_content_section margin_bottom="-30px"]
[intense_heading align="center"]Thank You for Your Interest[/intense_heading]
[intense_heading tag="h3" align="center"]Purchase today and get free updates forever. With Intense you get a incredible value at a low price.[/intense_heading]
<center>
[intense_button color="#24B45E" size="large" border_radius="2px" link="http://codecanyon.net/item/intense-shortcodes-wordpress-plugin/5600492?ref=IntenseVisions"][intense_snippet snippet_id="Child Theme | Buttons/buy-text" snippet_title="Buy Text"][/intense_button]
</center>

[intense_row]
[intense_column size="4" medium_size="4"][/intense_column]
[intense_column size="4" medium_size="4"]
[intense_testimonies template="text"]
[/intense_column]
[intense_column size="4" medium_size="4"][/intense_column]
[/intense_row]

[/intense_content_section]