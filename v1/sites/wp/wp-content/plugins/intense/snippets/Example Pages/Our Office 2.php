<?php
/*
Intense Snippet Name: Our Office 2
*/
?>

[intense_content_section size="full" margin_top="-30" padding_top="0" padding_bottom="0"]
[intense_map type="Roadmap" width="100%" fit_bounds="0" address="299 E Jefferson Blvd, Fort Wayne, IN 46802" height="450px" zoom_control="0" map_type_control="0" street_view="0" zoom="10" hue="#333" saturation="-100"]
[intense_map_marker address="299 E Jefferson Blvd, Fort Wayne, IN 46802" markerimage="http://intense.intensevisions.com/wp-content/uploads/sites/4/2013/09/LogoEyeMarker64x64.png" markeranchor="32,64" /]
[/intense_map]
[/intense_content_section]
[intense_content_section background_color="#000" image="3877" imagemode="repeat" padding_top="0" padding_bottom="0"]
<div style="text-align: center">
<h1 class="mega" style="color: #fff">Our Office</h1>
</div>
[/intense_content_section]
[intense_content_section background_color="#efefef"]
<h1>Come and Visit Us</h1>
[intense_lead]We would love to have you stop by[/intense_lead]Ei odio tota appareat his. At sit bonorum omittantur. Clita labore aliquando per in, nam te mundi signiferumque, no nostro probatus cotidieque eos. Malorum comprehensam pri cu, est ut civibus aliquando. Te sed inani concludaturque, ullum ignota cum cu. Accusam eloquentiam adversarium nam te, ei quando probatus sed. Has ne oratio impetus fabellas, ipsum adipiscing id ius.

Sea mutat percipitur no, ius an quidam urbanitas honestatis. Vim lorem sensibus ex, vis ea eius erant. Qui ad omnes solet causae. Ad has nobis graeci principes.

Ea nec laudem graece, mei et unum dolor. Iisque fierent scribentur nec te. Vim vitae electram ea, ea ullum urbanitas sea. Te qui virtute labores, mei tollit minimum facilisis ex. Ne sed nibh dicta omnium. Mel ea duis solum mundi, id facer viris elitr qui.
[/intense_content_section]
[intense_content_section margin_bottom="-30" border_top="1"]
[intense_row]
[intense_column size="6"]
<h3>[intense_icon type="map-marker" /] Address</h3>
[intense_lead]299 E Jefferson Blvd, Fort Wayne, IN 46802[/intense_lead]
<h3>[intense_icon type="phone" /] Phone</h3>
[intense_lead]1-800-867-5309[/intense_lead]
<h3>[intense_icon type="globe" /] Website</h3>
[intense_lead]www.yourwebsite.com[/intense_lead]
[/intense_column]
[intense_column size="6"]

[intense_slider transition="fade"]
[intense_slide]
[intense_image image="5424" size="postWide" /]
[/intense_slide]
[intense_slide]
[intense_image image="5470" size="postWide" /]
[/intense_slide]
[intense_slide]
[intense_image image="6016" size="postWide" /]
[/intense_slide]
[/intense_slider]

[/intense_column]
[/intense_row]
[/intense_content_section]