<?php
/*
Intense Snippet Name: Supersized Photos
*/
?>

[intense_gallery include="8098,8099,8175,8176,8100,8180,8181,8106,8161,8182" size="large2048" type="supersized"]
[intense_content_section]
[intense_promo_box size="mega"]
<h1>Supersize Your Photos [intense_icon type="icon-camera" /]</h1>
Display your photos from WordPress, Flickr, 500px, Facebook, SmugMug, Instagram, Zenfolio, and more in full screen using Supersized.
[intense_button type="primary" size="large" link="http://codecanyon.net/user/IntenseVisions?ref=IntenseVisions"]Buy Now[/intense_button]
[/intense_promo_box]
[/intense_content_section]