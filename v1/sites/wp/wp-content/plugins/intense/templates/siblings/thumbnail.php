<?php
/*
Intense Template Name: Thumbnail
*/

global $intense_active_page;

echo ' <a href="' . get_permalink( $intense_active_page->ID ) . '" title="' . esc_attr( $intense_active_page->post_title ) . '">'  . intense_get_post_thumbnails( 
	"square75", 
	null, 
	false, 
	true, 
	null, 
	null, 
	null, 
	null,
	"",
	true, 
	true ) .'</a>';
