<?php
/*
Intense Template Name: Link
*/

global $intense_active_page;

echo ' <a href="' . get_permalink( $intense_active_page->ID ) . '">'  . esc_html( $intense_active_page->post_title ) .'</a>';
