<?php

require_once 'photosource/Intense-photo.php';

/**
 * Photo Sources used for gallery, widgets, etc.
 */

class Intense_Photo_Source {
    public $source = "wordpress";
    public $wordpress_attachments;
    public $userid;
    public $groupid;
    public $setid;
    public $additionalid;
    public $full_size;

    private $default_full_size = "large";

    function Intense_Photo_Source( $source, $wordpress_attachments, $userid, $groupid, $setid, $additionalid ) {
        global $intense_visions_options;

        $this->source = $source;
        $this->wordpress_attachements = $wordpress_attachments;
        $this->userid = $userid;
        $this->groupid = $groupid;
        $this->setid = $setid;
        $this->additionalid = $additionalid;
        $this->full_size = ( isset( $intense_visions_options['intense_photosource_full_size'] ) ? $intense_visions_options['intense_photosource_full_size'] : $this->default_full_size );
    }

    static function get_wordpress_photo_sizes() {
        global $_wp_additional_image_sizes;

        $intermediate_sizes = get_intermediate_image_sizes();

        $sizes = array();

        foreach ( $intermediate_sizes as $s ) {
            $sizes[ $s ] = array( 0, 0 );
            if ( in_array( $s, array( 'thumbnail', 'medium', 'large' ) ) ) {
                $sizes[ $s ]["width"] = get_option( $s . '_size_w' );
                $sizes[ $s ]["height"] = get_option( $s . '_size_h' );
            } else if ( isset( $_wp_additional_image_sizes ) && isset( $_wp_additional_image_sizes[ $s ] ) ) {
                $sizes[ $s ]["width"] = $_wp_additional_image_sizes[ $s ]['width'];
                $sizes[ $s ]["height"] = $_wp_additional_image_sizes[ $s ]['height'];
            }
        }

        return $sizes;
    }

    /**
     * Gets random wordpress size that is between min and max sizes
     *
     * @param string  $min
     * @param string  $max
     * @return array
     */
    static function get_random_wordpress_size( $min = null, $max = null ) {
        $sizes = Intense_Photo_Source::get_wordpress_photo_sizes();

        if ( !isset( $min ) ) {
            $available_sizes = array_keys( $sizes );
            $min = $available_sizes[0];
        }

        if ( !isset( $max ) ) {
            $available_sizes = array_keys( $sizes );
            $max = $available_sizes[ count( $sizes ) - 1 ];
        }

        $start_offset = Intense_Photo_Source::array_offset( $sizes, $min );
        $end_offset = Intense_Photo_Source::array_offset( $sizes, $max );
        $random_offset = rand( $start_offset, $end_offset );
        $sizes = array_values( $sizes );

        return $sizes[ $random_offset ];
    }

    static function array_offset( $array, $offset_key ) {
        $offset = 0;
        foreach ( $array as $key=>$val ) {
            if ( $key == $offset_key )
                return $offset;
            $offset++;
        }
        return -1;
    }

    static function get_wordpress_size( $size ) {
        $wordpress_sizes = Intense_Photo_Source::get_wordpress_photo_sizes();
        $wordpress_size = ( isset( $wordpress_sizes[ $size ] ) ? $wordpress_sizes[ $size ] : $wordpress_sizes[ 'large' ] );

        return $wordpress_size;
    }

    function get_full_size() {
        $wordpress_sizes = Intense_Photo_Source::get_wordpress_photo_sizes();
        $wordpress_full_size = $wordpress_sizes[ $this->full_size ];

        return $wordpress_full_size;
    }

    function get_cache_key( $pagesize, $page ) {
        // echo '<pre>'; echo(
        //     "source: " . ( isset( $this->source ) ? $this->source : '' ).
        //     " attachments: " . ( isset( $this->wordpress_attachements ) ? json_encode( array_keys( $this->wordpress_attachements ) ) : '' ).
        //     " userid: " . ( isset( $this->userid ) ? $this->userid : '' ).
        //     " groupid: " . ( isset( $this->groupid ) ? $this->groupid : '' ).
        //     " setid: " . ( isset( $this->setid ) ? $this->setid : '' ).
        //     " additionalid: " . ( isset( $this->additionalid ) ? $this->additionalid : '' ).
        //     " fullsize: " . ( isset( $this->full_size ) ? $this->full_size : '' ).
        //     " pagesize: " . ( isset( $pagesize ) ? $pagesize : '' ).
        //     " page: " . ( isset( $page ) ? $page : '' ) ); echo '</pre>';

        return hash( 'sha1',
            ( isset( $this->source ) ? $this->source : '' ).
            ( isset( $this->wordpress_attachements ) ? json_encode( array_keys( $this->wordpress_attachements ) ) : '' ).
            ( isset( $this->userid ) ? $this->userid : '' ).
            ( isset( $this->groupid ) ? $this->groupid : '' ).
            ( isset( $this->setid ) ? $this->setid : '' ).
            ( isset( $this->additionalid ) ? $this->additionalid : '' ).
            ( isset( $this->full_size ) ? $this->full_size : '' ).
            ( isset( $pagesize ) ? $pagesize : '' ).
            ( isset( $page ) ? $page : '' ) );
    }

    function get_cache( $pagesize, $page ) {
        $photos = null;
        $key = $this->get_cache_key( $pagesize, $page );
        $uploads = wp_upload_dir();

        $intense_cache_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );
        $cache_file = $intense_cache_dir . $key . '.cache';
        $exists = file_exists( $cache_file );

        if ( $exists && ( filemtime( $cache_file ) > ( time() - 60 * 30 ) ) ) {
            $raw_photos = file_get_contents( $cache_file );
            $photos = unserialize( $raw_photos );
        } else if ( $exists ) {
                unlink( $cache_file );
            }

        return $photos;
    }

    function set_cache( $photos, $pagesize, $page ) {
        $key = $this->get_cache_key( $pagesize, $page );
        $raw_photos = serialize( $photos );
        $uploads = wp_upload_dir();

        $intense_cache_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );
        $cache_file = $intense_cache_dir . $key . '.cache';

        if ( !file_exists( $intense_cache_dir ) ) {
            mkdir( $intense_cache_dir, 0777, true );
        }

        file_put_contents( $cache_file, $raw_photos );
    }

    function get_photos( $pagesize, $page ) {
        $photos = $this->get_cache( $pagesize, $page );

        if ( !isset( $photos ) ) {

            switch ( $this->source ) {
            case 'flickr':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-flickr.php';

                $flickr = new Intense_Flickr();
                $photos = $flickr->get_photos( $this->userid, $this->setid, $this->groupid, $page, $pagesize );
                break;
            case 'smugmug':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-smugmug.php';

                $smugmug = new Intense_SmugMug();
                $photos = $smugmug->get_photos( $this->userid, $this->setid, $page, $pagesize );
                break;
            case 'zenfolio':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-zenfolio.php';

                $zenfolio = new Intense_Zenfolio();
                $photos = $zenfolio->get_photos( $this->setid, $page, $pagesize );
                break;
            case 'instagram':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-instagram.php';

                $instagram = new Intense_Instagram();
                $photos = $instagram->get_photos( $this->userid, $this->groupid, $page, $pagesize );
                break;
            case 'deviantart':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-deviantart.php';

                $deviantART = new Intense_DeviantArt();
                $photos = $deviantART->get_photos( $this->userid, $page, $pagesize );
                break;
            case 'facebook':
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-facebook.php';

                $facebook = new Intense_Facebook();
                $photos = $facebook->get_photos( $this->userid, $this->additionalid, $this->groupid, $this->setid, $page, $pagesize );
                break;
            case "500px":
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-500px.php';

                $px500 = new Intense_500px();
                $photos = $px500->get_photos( $this->userid, $page );
                break;
            case "picasa":
            case "google+":
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-picasa.php';

                $picasa = new Intense_Picasa();
                $photos = $picasa->get_photos( $this->userid, $this->setid, $page, $pagesize ); //groupid = album, setid = tag
                break;
            default:
                require_once INTENSE_PLUGIN_FOLDER . '/inc/photosource/Intense-wordpress.php';

                $wordpress_sizes = $this->get_wordpress_photo_sizes();
                $wordpress = new Intense_WordPress();
                $photos = $wordpress->get_photos( $this->wordpress_attachements, $wordpress_sizes );
                break;
            }

            $this->set_cache( $photos, $pagesize, $page );
        }

        return $photos;
    }
}
