<?php

global $intense_shortcodes;

require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/data.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/shortcode.php';

// Shortcodes
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/alert.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/animated-image.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/animated.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/audio.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/badge.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/blockquote.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/button.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/chart.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/code.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/collapsible.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/content-box.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/content-section.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/counter.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/custom-post.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/definition.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/dropcap.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/emphasis.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/faq.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/filler.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/fullscreen-video.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/gallery.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/googledocviewer.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/guests.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/heading.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/highlight.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/hover-box.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/hr.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/icon-list.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/icon.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/image-compare.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/image.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/label.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/layout.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/lead.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/lightbox.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/map.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/masonry.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/members.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/menu.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/metadata.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/parallax-scene.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/permalink.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/person.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/popover.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/post-fields.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/pricingtable.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/progress.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/promo.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/qrcode.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/recent-posts.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/rss.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/siblings.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/slider.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/snippet.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/social.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/social-share.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/spacer.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/subpages.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/tab.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/table.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/testimony.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/template.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/timeline.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/tooltip.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/video.php';

require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/ajax.php';

class Intense_Shortcodes {
    private $shortcodes = array();

    function __construct() {
        global $intense_visions_options;

        add_action( 'media_buttons', array( __CLASS__, 'button' ), 1000 );

        if ( defined ( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '4.3.2', '>=' ) ) {            
            add_action( 'vc_before_init', array( 'Intense_Shortcodes', 'map_visual_composer' ) );
        } else {
            add_action( 'after_setup_theme', array( 'Intense_Shortcodes', 'map_visual_composer' ) );
        }

        add_filter( 'the_content', array( 'Intense_Shortcodes', 'cleaner' ) );
        add_filter( 'widget_text', array( 'Intense_Shortcodes', 'cleaner' ) );

        $this->shortcodes = Intense_Shortcodes::get_list();
        $this->add_shortcodes();

        do_action( 'intense/shortcodes', $this->shortcodes );
    }

    public static function get_list() {
        global $intense_post_types, $intense_visions_options;

        $shortcodes = array();

        $active_shortcodes = $intense_visions_options['intense_active_shortcodes'];

        //Locate all the children classes of Intense_Shortcode and
        //add them to the shortcode list
        foreach ( get_declared_classes() as $class ) {
            if ( is_subclass_of( $class, 'Intense_Shortcode' ) ) {
                if ( !empty( $active_shortcodes[ strtolower( $class ) ] ) && $active_shortcodes[ strtolower( $class ) ] ) {
                    $shortcode = new $class();

                    if ( !isset( $shortcode->custom_post_type ) || $intense_post_types->is_type_enabled( $shortcode->custom_post_type ) ) {
                        $shortcodes[ strtolower( $class ) ] = $shortcode;
                    }
                }
            }
        }

        return $shortcodes;
    }

    /**
     * Enqueue assets needed for editor
     */
    public static function enqueue_dialog_scripts() {
        intense_add_script( 'typewatch' );
        intense_add_script( 'minicolors' );
        intense_add_script( 'smooth-scroll' );
        intense_add_script( 'magnific-popup' );
        intense_add_script( 'selectize' );
        intense_add_script( 'bootstrap-tab' );

        intense_add_style( 'selectize' );
        intense_add_style( 'intense-font-awesome' );
        intense_add_style( 'intense.dialogs' );
        intense_add_style( 'magnific-popup' );
        intense_add_style( 'intense-custom-css' );
        intense_add_style( 'minicolors' );
    }

    /**
     * Used for sorting the list of shortcodes
     *
     * @param Intense_Shortcode $a the first item to compare
     * @param Intense_Shortcode $b the second item to compare
     * @return int    0, 1, -1
     */
    public static function shortcode_compare( $a, $b ) {
        $category_a = strtolower( $a->category );
        $category_b = strtolower( $b->category );
        $title_a = strtolower( $a->title );
        $title_b = strtolower( $b->title );

        if ( $category_a == $category_b ) {
            if ( $title_a == $title_b ) {
                return 0;
            }

            return ( $title_a > $title_b ) ? +1 : -1;
        } else {
            if ( $category_a == $category_b ) {
                return 0;
            }
            return ( $category_a > $category_b ) ? +1 : -1;
        }
    }

    public function add_shortcodes() {
        foreach ( $this->shortcodes as $key => $value ) {
            add_shortcode( $key, array( $value, 'shortcode' ) );
        }
    }

    public function run_shortcode( $shortcode, $atts = null, $content = null ) {
        if ( !empty ( $this->shortcodes[ $shortcode ] ) ) {
            $shortcode = $this->shortcodes[ $shortcode ];

            return $shortcode->shortcode( $atts, $content );
        } else {
            return "<span style='background: red; color: #fff; font-size: 12px; padding: 10px;'>[Oops: the " . $shortcode . ' shortcode is disabled or missing. Check the shortcode options section for Intense to make sure it is turned on.]</span> ';
        }
    }

    public function get_shortcode( $key ) {
        return $this->shortcodes[ $key ];
    }

    public static function map_visual_composer() {
        global $intense_visions_options;

        $hideShortcodes = $intense_visions_options['intense_hide_shortcodes_in_vc'];

        if ( is_admin() && $hideShortcodes === '0' ) {
            //Visual Composer Shortcode Mappings
            if ( in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
                if ( function_exists( "vc_map" ) ) {
                    require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/composer/parameter-types.php';

                    intense_add_style( 'intence.visualcomposer' );

                    $shortcodes = Intense_Shortcodes::get_list();

                    foreach ( $shortcodes as $key => $shortcode ) {
                        $shortcode->map_visual_composer();
                    }
                }
            }
        }
    }

    public function to_json() {
        $data = array();

        uasort( $this->shortcodes, array( $this, "shortcode_compare" ) );

        foreach ( $this->shortcodes as $key => $value ) {
            if ( !isset( $value->hidden ) || !$value->hidden ) {
                $data[] = array(
                    'group' => $value->category,
                    'icon' => $value->icon,
                    'url' => "action=intense_shortcode_dialog_action&shortcode=" . $key,
                    'shortcode_title' => $value->title,
                    'shortcode' => $key
                );
            }
        }

        return json_encode( $data );
    }

    public function to_documentation() {
        $output = '';

        uasort( $this->shortcodes, array( $this, "shortcode_compare" ) );

        $shortcodes = $this->shortcodes;

        foreach ( $shortcodes as $key => $value ) {
            $output .= $value->to_documentation();
        }

        return $output;
    }

    public static function button( $args = array() ) {
        $args = wp_parse_args( $args, array(
                'echo'      => true
            ) );

        add_action( 'wp_footer', array( __CLASS__, 'dialog' ) );
        add_action( 'admin_footer', array( __CLASS__, 'dialog' ) );

        intense_add_script( 'intense.shortcodes' );

        $output = '<a class="intense-shortcode button" data-mfp-src="#intense-shortcode-selector" href="javascript:void(0);">' .
            '<i class="dashicons dashicons-intense-intense-logo"></i> ' . __( 'Shortcodes', 'intense' ) .
            '</a>';
        $output .= '<a class="intense-snippet button" data-mfp-src="#intense-snippet-selector" href="javascript:void(0);">' .
            '<i class="dashicons dashicons-intense-scissors"></i> ' . __( 'Snippets', 'intense' ) .
            '</a>';

        wp_enqueue_media();

        Intense_Shortcodes::enqueue_dialog_scripts();

        if ( $args['echo'] ) echo $output;

        return $output;
    }

    public static function dialog( ) {
        $output = '';

        ob_start();
?>
        <div id="intense-shortcode-selector-wrapper" style="display: none;">
            <div id="intense-shortcode-selector" class="mfp-hide"><button title="Close (Esc)" type="button" class="mfp-close">&times;</button>
                <div class="intense-shortcode-selector-title">
                    <img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/icon_32x32.png"> <?php echo __( 'Intense Shortcodes', 'intense' ); ?>
                    <div class="intense-shortcode-filter"><i class="dashicons dashicons-search"></i> <input type="text" placeholder="<?php _e( 'Search' ); ?>..." /></div>
                    <div class="intense-shortcode-skin">
                        <?php _e( 'Skin', 'intense' ); ?> <select id="intense-active-skin">
                        <?php
        global $intense_visions_options;

        $skins = Intense_Skins::get_overridden_list();
        $default_skin = isset( $intense_visions_options['intense_skin'] ) ? $intense_visions_options['intense_skin'] : 'default';

        foreach ( $skins as $key => $skin ) {
            echo '<option value="' . $skin->key . '" ' . ( $skin->key == $default_skin ? 'selected="selected"' : '' ) . '>' . $skin->name . '</option>';
        }
?>
                        </select>
                    </div>
                </div>
                <div class="intense-shortcodes-back" style="display:none;">
                    <?php echo __( 'Show all shortcodes', 'intense' ); ?>
                </div>
                <div class="intense-shortcodes">
                    <?php
        $shortcode_list = '';
        $category = '';

        $shortcodes =  Intense_Shortcodes::get_list();

        uasort( $shortcodes, array( "Intense_Shortcodes", "shortcode_compare" ) );

        foreach ( $shortcodes as $key => $shortcode ) {
            if ( !isset( $shortcode->hidden ) || !$shortcode->hidden ) {
                if ( $shortcode->category != $category ) {
                    if ( $category != '' ) {
                        $shortcode_list .= '</ul><div style="clear: both"></div>';
                    }

                    $category = $shortcode->category;

                    $shortcode_list .= '<div class="intense shortcode-group">' . $category . '</div><ul class="intense shortcode-list">';
                }

                $shortcode_list .= '<li class="' . $shortcode->category . '" data-url="action=intense_shortcode_dialog_action&shortcode="' . $key . '" data-name="' . $shortcode->title . '" data-shortcode="' . $key . '">';
                $shortcode_list .= '<i class="dashicons ' . $shortcode->icon . '"></i>';
                $shortcode_list .= $shortcode->title . "</li>";
            }
        }

        $shortcode_list .= '</ul>';

        echo $shortcode_list;
?>
                    <div style="clear: both;"></div>
                </div>
                <div class="intense-shortcode-input" style="display:none;"></div>
            </div>
        </div>
        <div id="intense-snippet-selector-wrapper" style="display: none;">
            <div id="intense-snippet-selector" class="mfp-hide"><button title="Close (Esc)" type="button" class="mfp-close">&times;</button>
                <div class="intense-snippet-selector-title">
                    <img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/icon_32x32.png"> <?php echo __( 'Intense Snippets', 'intense' ); ?>
                </div>
                <div class="intense-snippets">
                    <div class="intense-snippet-title"><i class="dashicons dashicons-intense-scissors" style="font-size:22px; padding-right: 15px; margin-top:-3px;"></i> <?php echo __( 'Insert editable snippet content directly into the page or post content', 'intense' ); ?></div>
                    <form id="snippet-form" name="snippet-form" action="#" class="intense form-horizontal" role="form">
                        <?php
        $ajax_nonce = wp_create_nonce( "intense-plugin-noonce" );
        echo '<input type="hidden" id="nonce" value="' . $ajax_nonce . '" />';
?>
                        <div class="intense form-group  selectize animation">
                            <label class="intense col-sm-2 control-label" for="snippet-selection" data-placeholder="<?php echo __( 'Snippet', 'intense' ); ?>"><?php echo __( 'Snippet', 'intense' ); ?></label>
                            <div class="intense col-sm-8">
                                <select id="snippet-selection" style="background: transparent; border: 0; border-radius: 0; height: 36px; -webkit-appearance: none; box-shadow: none;">
                                    <option><?php echo __( 'Loading...', 'intense' ); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="intense form-group  selectize animation">
                            <label class="intense col-sm-2 control-label" for="snippet-content" data-placeholder="<?php echo __( 'Content', 'intense' ); ?>"><?php echo __( 'Content', 'intense' ); ?></label>
                            <div class="intense col-sm-8">
                                <textarea id="snippet-content" disabled="disabled"></textarea>
                                <span class="intense help-block"><?php echo __( 'Select the snippet, make changes to the content, then press the insert button. If you prefer not to change the content, you can also add snippets using the [intense_snippet] shortcode.', 'intense' ); ?></span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="pull-left">
                                <input type="button" class="button button-primary button-large" id="insert-snippet" value="<?php _e( "Insert Snippet", 'intense' ); ?>"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php

        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
    }

    public function render_skin_settings() {
        $output = '';

        ob_start();
?>
        <div class="intense-skin-shortcodes">
            <?php
        $shortcode_list = '';
        $category = '';

        $shortcodes =  Intense_Shortcodes::get_list();

        uasort( $shortcodes, array( "Intense_Shortcodes", "shortcode_compare" ) );

        foreach ( $shortcodes as $key => $shortcode ) {
            //if ( !isset( $shortcode->hidden ) || !$shortcode->hidden ) {
            if ( $shortcode->category != $category ) {
                if ( $category != '' ) {
                    $shortcode_list .= '</ul><div style="clear: both"></div>';
                }

                $category = $shortcode->category;

                $shortcode_list .= '<div class="intense skin-shortcode-group">' . $category . '</div><ul class="intense skin-shortcode-list">';
            }

            $shortcode_list .= '<li class="' . $shortcode->category . '" data-url="action=intense_shortcode_dialog_action&shortcode=' . $key . '" data-name="' . $shortcode->title . '" data-shortcode="' . $key . '">';
            $shortcode_list .= '<i class="dashicons ' . $shortcode->icon . '"></i>';
            $shortcode_list .= $shortcode->title . "</li>";
            //}
        }

        $shortcode_list .= '</ul>';

        echo $shortcode_list;
?>
            <div style="clear: both;"></div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
    }

    /**
     * Remove extra p and br tags
     * See: http://www.wpexplorer.com/clean-up-wordpress-shortcode-formatting/
     */
    public static function cleaner( $content ) {
        return strtr( $content, array (
                '<p>[' => '[',
                ']</p>' => ']',
                ']<br />' => ']'
            ) );
    }
}

$intense_shortcodes = new Intense_Shortcodes();

add_shortcode( 'intense_shortcode_docs', 'intense_shortcode_docs_shortcode' );
function intense_shortcode_docs_shortcode( $atts = null, $content = null ) {
    global $intense_shortcodes;

    extract( shortcode_atts( array(
                'shortcode' => null
            ), $atts ) );

    if ( !empty( $shortcode ) ) {
        $shortcode = $intense_shortcodes->get_shortcode( $shortcode );

        return $shortcode->to_documentation();
    } else {
        return $intense_shortcodes->to_documentation();
    }
}

if ( INTENSE_DEBUG ) {
add_shortcode( 'intense_default_skin_json', 'intense_default_skin_json_shortcode' );
function intense_default_skin_json_shortcode( $atts = null, $content = null ) {
    global $intense_shortcodes;

    $skin = new Intense_Skin( 'default', 'Default', array() );

    $shortcodes = $intense_shortcodes->get_list();

    foreach( $shortcodes as $key => $shortcode ) {
        $defaults = $shortcode->get_shortcode_defaults( null, null, true );

        $skin->set_shortcode_defaults( strtolower( get_class( $shortcode ) ), array_filter( $defaults, 'intense_filter_skin_defaults' ) );
    }

    // $shortcode = $intense_shortcodes->get_shortcode( 'intense_alert' );

    // $defaults = $shortcode->get_shortcode_defaults( null, null, true );

    // echo '<pre>'; print_r($defaults); echo '</pre>';

    // $skin->set_shortcode_defaults( strtolower( get_class( $shortcode ) ), array_filter( $defaults, 'intense_filter_skin_defaults' ) );

    unset( $skin->can_edit );
    unset( $skin->source_location );

    return '<pre><code>' . json_encode( $skin, JSON_PRETTY_PRINT ) . '</code></pre>';
}

function intense_filter_skin_defaults( $var ) {
    return isset( $var );
}
}

/**
 * Helper function for running shortcodes
 *
 * @param string  $shortcode the shortcode to run including intense_
 * @param array   $atts      the attributes
 * @param string  $content   content
 * @return string            the html for the shortcode
 */
function intense_run_shortcode( $shortcode, $atts = null, $content = null ) {
    global $intense_shortcodes;

    return $intense_shortcodes->run_shortcode( $shortcode, $atts, $content );
}
