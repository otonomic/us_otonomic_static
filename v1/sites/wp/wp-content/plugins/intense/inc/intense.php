<?php

require_once INTENSE_PLUGIN_FOLDER . '/inc/tools/tools.php';

class Intense {
	function __construct() {
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 10 );
		add_action( 'init', array( __CLASS__, 'update' ), 20 );
		register_activation_hook( INTENSE_PLUGIN_FILE, array( __CLASS__, 'activation' ) );
		register_activation_hook( INTENSE_PLUGIN_FILE, array( __CLASS__, 'deactivation' ) );
		$this->add_image_sizes();
	}

	/**
	 * Plugin initialization
	 */
	public static function init() {
		require_once INTENSE_PLUGIN_FOLDER . '/inc/plugins/IVersionWP-Client.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/options/options.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/assets.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/metaboxes.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/custom-post-types.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes.php';
		require_once INTENSE_PLUGIN_FOLDER . '/inc/widgets.php';	

		load_plugin_textdomain( 'intense', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );		

		add_filter( 'load_textdomain_mofile', array( __CLASS__, 'replace_default_language_files' ), 10, 2 );
		add_filter( 'mime_types', array( __CLASS__, 'add_mime_types' ) );

		do_action( 'intense/init' );
	}

	/**
	 * Plugin update
	 */
	public static function update() {
		global $intense_visions_options;

		if ( !empty( $intense_visions_options['intense_auto_update'] ) && $intense_visions_options['intense_auto_update'] ) {
			$ivwp = new IVersionWP_Client( INTENSE_ACTIVATION_SERVER, INTENSE_PRODUCT_KEY, 'intense/intense.php', INTENSE_PLUGIN_VERSION );
			$ivwp->autoupdate( $intense_visions_options['intense_envato_username'], $intense_visions_options['intense_api_key'] );
		}
			
		$version = get_option( 'intense_version' );
		if ( $version !== INTENSE_PLUGIN_VERSION ) {
			update_option( 'intense_version', INTENSE_PLUGIN_VERSION );
			flush_rewrite_rules(); //reset the permalinks
			do_action( 'intense/update' );
		}
	}

	/**
	 * Plugin activation
	 */
	public static function activation() {
		update_option( 'intense_version', INTENSE_PLUGIN_VERSION );
		flush_rewrite_rules(); //reset the permalinks
		do_action( 'intense/activation' );
	}

	/**
	 * Plugin deactivation
	 */
	public static function deactivation() {
		do_action( 'intense/deactivation' );
	}

	/**
	 * Adds additional image sizes
	 */
	private function add_image_sizes() {

		if ( function_exists( 'add_image_size' ) ) {
			/* flickr sizes */
			add_image_size( 'square75',  75,   75, true );
			add_image_size( 'square150', 150,  150, true );
			add_image_size( 'square400', 400,  400, true );
			add_image_size( 'small240',  240,  160, true );
			add_image_size( 'small320',  320,  213, true );
			add_image_size( 'medium500', 500,  333, true );
			add_image_size( 'medium640', 640,  427, true );
			add_image_size( 'medium800', 800,  534, true );
			add_image_size( 'large1024', 1024, 683, true );
			add_image_size( 'large1600', 1600, 1067, true );
			add_image_size( 'large2048', 2048, 1365, true );
			add_image_size( 'portraitSmall', 400, 500, false );
			add_image_size( 'portraitMedium', 800, 1000, false );
			add_image_size( 'portraitLarge', 1600, 2000, false );

			/* post thumbnail sizes */
			add_image_size( 'postWide', 940, 383, true );
			add_image_size( 'postExtraWide', 1410, 575, true );
			add_image_size( 'postFull', 940, 623, true );
		}
	}

	/**
	 * Adds missing mime types
	 *
	 * @param array   $existing_mimes list of mime types
	 */
	public static function add_mime_types( $existing_mimes ) {
		$existing_mimes['otf'] = 'font/opentype';
		$existing_mimes['eot'] = 'application/vnd.ms-fontobject';
		$existing_mimes['svg'] = 'image/svg+xml';
		$existing_mimes['ttf'] = 'application/octet-stream';
		$existing_mimes['woff'] = 'application/x-woff';

		// return the array back to the function with our added mime type
		return $existing_mimes;
	}

	/**
	 * Manually set the directory since load_plugin_textdomain doesn't like plugins as symlinks
	 *
	 * @param string  $mofile translation file
	 * @param string  $domain domain
	 * @return string         translation file
	 */
	public static function replace_default_language_files( $mofile, $domain ) {
		if ( 'intense' === $domain ) {
			return INTENSE_PLUGIN_FOLDER . '/languages/' . $domain . '-' . get_locale() . '.mo';
		}

		return $mofile;
	}
}

/**
 * Function that can be used to check if Intense is installed
 *
 * @return bool will always return true
 */
function intense_installed() {
	return true;
}

new Intense();
