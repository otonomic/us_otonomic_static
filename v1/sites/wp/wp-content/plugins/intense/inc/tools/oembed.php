<?php

/**
 * Adds extra OEmbed providers
 */
function intense_oembed() {
	wp_oembed_add_provider( '#https?://(www\.)?collegehumor.com/.*#i', 'http://www.collegehumor.com/oembed.json', true ); //Comming in 4.0
	wp_oembed_add_provider( 'http?://(www\.)?ted.com/talks/*', 'http://www.ted.com/talks/oembed.json' ); //Comming in 4.0
	wp_oembed_add_provider( 'http://www.screenr.com/*', 'http://www.screenr.com/api/oembed.json', false );
	wp_oembed_add_provider( '#https?://(www\.)?speakerdeck.com/.*#i', 'https://speakerdeck.com/oembed.json', true );
	wp_oembed_add_provider( '#https?://(www\.)?ifixit.com/.*#i', 'http://www.ifixit.com/Embed.json', true );
}

add_action( 'init', 'intense_oembed' );