<?php
/**
 * Functions related to dates and time
 */

/**
 * convert minutes into hours and minutes (ex... 1 hr 20 mins).
 * @param  int $duration number of minutes
 * @return string           hour/minutes representation of minutes
 */
function intense_convert_minutes_to_hours( $duration ) {
	$hours = (int)( $duration / 60 );
	$minutes = $duration - ( $hours * 60 );
	$total_time = '';

	if ( $hours > 0 ) {
		if ( $hours == 1 ) {
			$total_time = $hours . ' hr ';
		} else {
			$total_time = $hours . ' hrs ';
		}
	}

	if ( $minutes > 0 ) {
		if ( $minutes == 1 ) {
			$total_time .= $minutes . ' min';
		} else {
			$total_time .= $minutes . ' mins';
		}
	}

	return $total_time;
}