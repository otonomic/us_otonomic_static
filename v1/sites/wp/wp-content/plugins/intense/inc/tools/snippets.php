<?php
/**
 * Functions related to templates
 */

// loads the content of a snippet file
function intense_load_plugin_snippet( $snippet_file ) {
	//make sure they aren't trying to traverse up the directory tree
	if ( isset( $snippet_file ) && !stripos( $snippet_file, "../" ) ) {
		$found_beginning = false;
		$found_php_end = false;
		$found_snippet_name = false; //used to validate that the file is a snippet file
		$file_lines = file( $snippet_file );
		$lines = array();

		//skip to past the php header by finding the first closing php tag
		foreach ($file_lines as $key => $line) {
			if ( !$found_snippet_name && stripos( $line, "Intense Snippet Name:" ) > -1 ) {
				$found_snippet_name = true;
			}

			if ( $found_php_end && trim( $line ) != '' ) {
				$found_beginning = true;
			}

			if ( $found_beginning ) {
				$lines[] = $line;
			}

			if ( $found_snippet_name && !$found_php_end && strpos( $line, '?>' ) > -1 ) {
				$found_php_end = true;
			}
		}

		return implode( "", $lines );
	} else {
		return 'Unable to locate snippet file at one of the following locations in the order listed: ' . "\n" .
			' ' . get_stylesheet_directory() . '/intense_snippets' . ", " .
			' ' . get_template_directory() . '/intense_snippets' . ", " .
			' ' . INTENSE_PLUGIN_FOLDER  . '/snippets';			
	}
}

// Checks available paths for the plugin template file
// function intense_locate_plugin_snippet( $snippet_name ) {
// 	if ( file_exists( get_stylesheet_directory() . '/intense_snippets' . $snippet_name ) ) {
// 		return get_stylesheet_directory()  . '/intense_snippets' . $snippet_name;
// 	} else if ( file_exists( get_template_directory()  . '/intense_snippets' . $snippet_name ) ) {
// 		return get_template_directory()  . '/intense_snippets' . $snippet_name;
// 	} else if ( file_exists( INTENSE_PLUGIN_FOLDER  . '/snippets' . $snippet_name ) ) {
// 		return INTENSE_PLUGIN_FOLDER  . '/snippets' . $snippet_name;
// 	}  else {
// 		return null;
// 	}
// }

function intense_get_snippet_location_path( $location ) {
	$theme = wp_get_theme();

	switch ( $location ) {
		case 'Intense':
			return INTENSE_PLUGIN_FOLDER . '/snippets/';
			break;
		case $theme->get( 'Name' ):
			return get_template_directory() . '/intense_snippets/';
			break;
		case 'Child Theme':
			return get_stylesheet_directory() . '/intense_snippets/';
			break;
	}
}

function intense_get_plugin_snippet_name( $path ) {
	$content = file_get_contents( $path );

	$lines = explode( "\n", $content);

	foreach ( $lines as $line ) {
		$remove_template_start_text =  str_replace( "Intense Snippet Name:", "", $line );

		// early terminate if name found
		if ( $remove_template_start_text != $line ) {
			return trim( $remove_template_start_text );
		}
	}

	return str_replace( ".php", "", substr( $path, strrpos( $path, "/" ) + 1 ) );
}

function intense_locate_available_plugin_snippets( ) {
	$theme = wp_get_theme();
	$plugin_files = array();
	$theme_files = array();
	$child_theme_files = array();
	$snippets = array();		
	
	$snippets['Plugin'] = array();
	$snippets[ $theme->get( 'Name' ) ] = array();
	$snippets['Child Theme'] = array();

	intense_location_snippets_in_directory( INTENSE_PLUGIN_FOLDER . '/snippets/', null, $snippets['Intense'] );
	intense_location_snippets_in_directory( get_template_directory() . '/intense_snippets/', null, $snippets[ $theme->get( 'Name' ) ] );

	if ( get_stylesheet_directory() != get_template_directory() ) {
		intense_location_snippets_in_directory( get_stylesheet_directory() . '/intense_snippets/', null, $snippets['Child Theme'] );
	}

	return array_filter( $snippets );
}

function intense_location_snippets_in_directory( $root, $path, &$snippets ) {
	if ( $path == null ) $path = $root;

	if ( is_dir( $path ) ) {
		$relative_path = str_replace( $root, '', $path );
		$paths = scandir( $path );
		$paths = array_flip( $paths );

		foreach ( $paths as $key => $value ) {	
			if ($key != '.' && $key != '..' && substr( $key, -strlen( ".php" )) == ".php" ) {			
				$value = intense_get_plugin_snippet_name( $path . '/' . $key );
				$snippets[ $relative_path ][ str_replace( ".php", "", $key ) ] = $value;
			}

			if ( $key != '.' && $key != '..' && is_dir( $path . $key ) ) {
				intense_location_snippets_in_directory( $root, $path . $key, $snippets );
			}
		}
	}
}