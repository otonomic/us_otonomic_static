<?php

class Intense_Lightboxes {
	public $atts = array();
	public $selector = '';
	public $type_string = '';

	/**
	 * Factory pattern function to create lightbox
	 *
	 * @param type    of lightbox $type     [description]
	 * @param [type]  $selector [description]
	 * @param [type]  $atts     [description]
	 * @return [type]           [description]
	 */
	public static function create( $type, $selector, $atts = array() ) {
		$type = 'Intense_Lightbox_' . ucfirst( $type );
		return new $type( $selector, $atts );
	}

	function __construct( $selector, $atts ) {
		$this->atts = $atts;
		$this->selector = $selector;
		$this->type_string = strtolower( str_ireplace( 'Intense_Lightbox_', '', get_class( $this ) ) );
	}

	public function get_image_script() {

	}

	public function get_html_script() {

	}

	public function get_iframe_script() {

	}

	public function get_css() {

	}

	public function get_html( $content, $source_element ) {

	}

	public function get_iframe( $content, $url ) {

	}

	public function get_image( $grouping, $title, $description, $link, $image ) {
		return "<a rel='$grouping' class='" . $this->type_string . " $grouping gallery-item' title='" . esc_attr( $title ) . "' data-content='" . esc_attr( $description ) . "' href='" . $link . "'>" . $image . "</a>";
	}
}

class Intense_Lightbox_Colorbox extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		global $intense_visions_options;
		$atts = array_merge( array (
				'slideshow' => ( $intense_visions_options['intense_colorbox_slideshow'] != null && $intense_visions_options['intense_colorbox_slideshow'] == 1 ? "true" : "false" ),
				'slideshowStart' => intense_run_shortcode( 'intense_icon', array( 'type' => 'play' ) ),
				'slideshowStop' => intense_run_shortcode( 'intense_icon', array( 'type' => 'pause' ) ),
				'transition' => ( $intense_visions_options['intense_colorbox_transition'] != null ? $intense_visions_options['intense_colorbox_transition'] : "elastic" )
			), $atts );

		parent::__construct( $selector, $atts );

		$language = substr( get_bloginfo ( 'language' ), 0, 2 );
		intense_add_style( 'colorbox' . ( $intense_visions_options['intense_colorbox_theme'] != null ? $intense_visions_options['intense_colorbox_theme'] : 1 ) );
		intense_add_script( 'colorbox' );

		if ( $language != 'en' ) {
			intense_add_script( 'colorbox-language' );
		}
	}

	public function get_image_script() {
		$output = "$('" . $this->selector . "').colorbox({
            maxWidth: '100%',
            maxHeight: '100%',
            slideshow: " . $this->atts['slideshow'] . ",
            slideshowStart: \"" . $this->atts['slideshowStart']  . "\",
            slideshowStop: \"" . $this->atts['slideshowStop']  . "\",
            transition: '" . $this->atts['transition'] . "'
        });";

		return $output;
	}

	public function get_html_script() {
		$output = "$('" . $this->selector . "').colorbox({
            maxWidth: '100%',
            maxHeight: '100%',
            inline: true
        });";

		return $output;
	}

	public function get_iframe_script() {
		$output = "$('" . $this->selector . "').colorbox({
            iframe: true,
            width: '70%',
            height: '80%',
        });";

		return $output;
	}

	public function get_html( $content, $source_element ) {
		return  '<a href="#' . $source_element . '">' . $content . '</a>';
	}

	public function get_iframe( $content, $url ) {
		return  '<a href="' . $url . '">' . $content . '</a>';
	}
}

class Intense_Lightbox_Swipebox extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		parent::__construct( $selector, $atts );

		intense_add_style( 'swipebox' );
		intense_add_script( 'swipebox' );
	}

	public function get_image_script() {
		$output = "$('" . $this->selector . "').swipebox({ supportSVG: false });";

		return $output;
	}

	public function get_css() {
		return '#swipebox-overlay { direction: ltr; }';
	}
}

class Intense_Lightbox_Prettyphoto extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		global $intense_visions_options;

		$atts = array_merge( array (
				'slideshow' => ( $intense_visions_options['intense_prettyphoto_slideshow'] != null && $intense_visions_options['intense_prettyphoto_slideshow'] == 1 ? "5000" : "false" ), // false OR interval time in ms
				'theme' => ( $intense_visions_options['intense_prettyphoto_theme'] != null ? $intense_visions_options['intense_prettyphoto_theme'] : "pp_default" ),
				'social_tools' => ( isset( $intense_visions_options['intense_prettyphoto_social'] ) ?  json_encode( $intense_visions_options['intense_prettyphoto_social'] ) : "false" ), // html or false to disable
			), $atts );

		parent::__construct( $selector, $atts );

		intense_add_style( 'prettyphoto' );
		intense_add_script( 'prettyphoto' );
	}

	public function get_image_script() {
		$output = "$('" . $this->selector . "').prettyPhoto({
            slideshow: " . $this->atts['slideshow'] . ",
            theme: '" . $this->atts['theme'] . "',
            social_tools: " . $this->atts['social_tools'] . "
        });";

		return $output;
	}

	public function get_html_script() {
		$output = "$('" . $this->selector . "').prettyPhoto({
            theme: '" . $this->atts['theme'] . "',
            social_tools: " . $this->atts['social_tools'] . "
        });";

		return $output;
	}

	public function get_iframe_script() {
		$output = "$('" . $this->selector . "').prettyPhoto({
            theme: '" . $this->atts['theme'] . "',
            social_tools: " . $this->atts['social_tools'] . "
        });";

		return $output;
	}

	public function get_css() {
		return '.pp_pic_holder { direction: ltr; }';
	}

	public function get_image( $grouping, $title, $description, $link, $image ) {
		return "<a rel='" . $this->type_string . "[$grouping]' class='" . $this->type_string . " $grouping gallery-item' title='" . esc_attr( $title ) . "' data-content='" . esc_attr( $description ) . "' href='" . $link . "'>" . $image . "</a>";
	}

	public function get_html( $content, $source_element ) {
		return  '<a rel="' . $this->type_string . '" href="#' . $source_element . '">' . $content . '</a>';
	}

	public function get_iframe( $content, $url ) {
		return  '<a rel="' . $this->type_string . '" href="' . $url . ( stripos( $url, "?" ) >= 0 ? "&" : "?" ) . 'iframe=true&width=70%&height=80%">' . $content . '</a>';
	}
}

class Intense_Lightbox_Touchtouch extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		parent::__construct( $selector, $atts );

		intense_add_style( 'touchtouch' );
		intense_add_script( 'touchtouch' );
	}

	public function get_image_script() {
		$output = "$('" . $this->selector . "').touchTouch();";

		return $output;
	}

	public function get_css() {
		return '.touchtouch-galleryOverlay { direction: ltr; }';
	}

	public function get_image( $grouping, $title, $description, $link, $image ) {
		return "<a data-gallery='$grouping' class='" . $this->type_string . " $grouping gallery-item' title='" . esc_attr( $title ) . "' data-content='" . esc_attr( $description ) . "' href='" . $link . "'>" . $image . "</a>";
	}
}

class Intense_Lightbox_Thickbox extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		parent::__construct( $selector, $atts );
		// Thickbox is included as part of WordPress but is no longer maintained.
		// If removed in the future, this code may be deprecated.
		add_thickbox();
	}

	public function get_css() {
		return '#TB_window { direction: ltr; }';
	}
}

class Intense_Lightbox_Photoswipe extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		parent::__construct( $selector, $atts );

		intense_add_script( 'klass.' );
		intense_add_script( 'photoswipe' );
		intense_add_style( 'photoswipe' );
	}

	public function get_image_script() {
		$output = 'var myPhotoSwipe = $("' . $this->selector . '").photoSwipe({ });';

		return $output;
	}

	public function get_css() {
		return '.ps-carousel, .ps-uilayer, .ps-toolbar { direction: ltr; }';
	}
}

class Intense_Lightbox_Magnificpopup extends Intense_Lightboxes {
	function __construct( $selector, $atts ) {
		parent::__construct( $selector, $atts );

		intense_add_script( 'magnific-popup' );
		intense_add_style( 'magnific-popup' );
	}

	public function get_image_script() {
		$output = '$("' . $this->selector . '").parent().magnificPopup({ 
			type: "image", 
			delegate: "a", 
			mainClass: "mfp-img-mobile", 
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			}, 
		});';

		return $output;
	}

	public function get_html_script() {
		$output = 'var source_element = $( $("' . $this->selector . '").attr("href") );
		source_element.addClass("white-popup")
		$("' . $this->selector . '").magnificPopup({ 
			type: "inline"
		});';

		return $output;
	}

	public function get_iframe_script() {
		$output = '$("' . $this->selector . '").magnificPopup({ 
			type: "iframe"
		});';

		return $output;
	}

	public function get_css() {
		return '.mfp-wrap { direction: ltr; } .mfp-figure figure { margin: 0; } 
		.white-popup {
		  position: relative;
		  width:auto;
		  max-width: 70%;
		  max-height: 80%;
		  margin: 20px auto;
		}';
	}

	public function get_html( $content, $source_element ) {
		return  '<a href="#' . $source_element . '">' . $content . '</a>';
	}

	public function get_iframe( $content, $url ) {
		return  '<a href="' . $url . '">' . $content . '</a>';
	}
}
