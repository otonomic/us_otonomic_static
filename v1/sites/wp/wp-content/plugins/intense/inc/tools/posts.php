<?php

/**
 * Functions related to working with posts
 */

/**
 * Gets the post featured images urls
 *
 * @param string  $size              size of image
 * @param boolean $only_first        return only the first featured image
 * @param boolean $use_missing_image return missing image url if no image is found
 * @return array                     list of featured image urls
 */
function intense_get_post_thumbnail_src( $size, $only_first = false, $use_missing_image = false ) {
	global $intense_visions_options;

	$thumbnail_ids = intense_get_post_thumbnail_ids();
	$thumbnail_src = null;

	foreach ( $thumbnail_ids as $id ) {
		$image = wp_get_attachment_image_src( $id, $size );
		$thumbnail_src[] = $image[0];
	}

	if ( !isset( $thumbnail_src ) ) {
		$thumbnail_src[] = $intense_visions_options['intense_missing_image']['url'];
	}

	return $thumbnail_src;
}

/**
 * Gets a list of attachemnt ids for post featured images
 *
 * @return array list of attachemnt ids for post featured images
 */
function intense_get_post_thumbnail_ids( ) {
	global $post, $intense_active_page;

	if ( isset( $intense_active_page ) ) {
		$thumbnail_post = $intense_active_page;
	} else {
		$thumbnail_post = $post;
	}

	$ids[] = get_post_thumbnail_id( $thumbnail_post->ID );

	if ( class_exists( 'MultiPostThumbnails' ) ) {
		for ( $i= 1; $i <= 6 ; $i++ ) {
			$image_name = 'feature-image-' . $i;  // sets image name as feature-image-1, feature-image-2 etc.

			if ( MultiPostThumbnails::has_post_thumbnail( $thumbnail_post->post_type, $image_name ) ) {
				$ids[] = MultiPostThumbnails::get_post_thumbnail_id( $thumbnail_post->post_type, $image_name, $thumbnail_post->ID );
			}
		}
	}

	return $ids;
}

/**
 * Gets html used to show featured images/videos for a post
 *
 * @param string  $size                 the size of the images
 * @param string  $attributes           not used
 * @param boolean $show_only_first      show only the first image/video
 * @param boolean $show_missing_image   show missing image if no images found
 * @param integer $shadow               shadow to show
 * @param string  $hover_effect         hover effect to show
 * @param string  $hover_effect_color   color of hover effect
 * @param int     $hover_effect_opacity opacity of hover effect
 * @param string  $image_border_radius  border radius for iamges
 * @param boolean $show_video           show featured videos
 * @param boolean $show_audio           show featured audio
 * @return string                        html used to show post featured images/videos
 */
function intense_get_post_thumbnails( $size, $attributes = null, $show_only_first = false, $show_missing_image = false, $shadow = 0, $hover_effect = null, $hover_effect_color = null, $hover_effect_opacity = null, $image_border_radius = null, $show_video = true, $show_audio = true, $thumbnail_post = null ) {
	global $post, $intense_visions_options, $intense_active_page;

	if ( isset( $intense_active_page ) ) {
		$thumbnail_post = $intense_active_page;
	} else {
		$thumbnail_post = $post;
	}

	$thumbnail_ids = intense_get_post_thumbnail_ids( );
	$image_type = get_field( 'intense_featured_image_type' );
	$video_type = get_field( 'intense_featured_video_type' );
	$audio_url = get_field( 'intense_featured_audio_url' );
	$margin_top = "0";
	$hovereffect = array();
	if ( empty( $thumbnail_post ) || empty( $thumbnail_post->ID ) ) {
		$post_title = '';
	} else {
		$post_title = get_the_title( $thumbnail_post->ID );
	}
	$hasVideo = false;
	$hasAudio = false;
	$showSlider = false;
	$showShadow = false;

	if ( !isset( $image_type ) || $image_type == '' ) {
		$image_type = "standard";
	}

	$output = "";

	if ( isset( $video_type ) && $video_type != '' && $show_video ) {
		$hasVideo = true;
	}

	if ( isset( $audio_url ) && $audio_url != '' && $show_audio ) {
		$hasAudio = true;
	}

	if ( $hasVideo && $show_video ) {
		if ( get_field( 'intense_featured_video_type' ) == 'wordpress' ) {
			$output .= intense_run_shortcode( 'intense_video', array(
					'video_type' => $video_type ,
					'video_size' => '',
					'poster' => get_field( 'intense_featured_video_poster_image' ),
					'mp4' => get_field( 'intense_featured_video_mp4' ),
					'ogv' => get_field( 'intense_featured_video_ogv' ),
					'webm' => get_field( 'intense_featured_video_webm' )
				) );
		} else {
			$output .= intense_run_shortcode( 'intense_video', array(
					'video_type' => $video_type ,
					'video_url' => get_field( 'intense_featured_video_url' ),
					'video_size' => ''
				) );
		}
	} else if ( $hasAudio && $show_audio ) {
			$output .= intense_run_shortcode( 'intense_audio', array(
					'url' => $audio_url,
					'width' => '100%',
					'autoplay' => '0',
					'loop' => '0'
				) );
		} else {
		if ( isset( $shadow ) && $shadow != '' && $shadow != '0' && $shadow && ( $image_type == 'caman' || $image_type == 'standard' || $image_type == 'parallax' ) ) {
			$output .= "<div><div style='margin-bottom: 0;'>";
			$showShadow = true;
		}

		if ( isset( $hover_effect ) && $hover_effect != '' && $image_type != 'adipoli' ) {
			$hovereffect['effeckt'] = $hover_effect;

			if ( isset( $hover_effect_color ) ) {
				$hovereffect['effecktcolor'] = $hover_effect_color;
			}

			if ( isset( $hover_effect_opacity ) ) {
				$hovereffect['effecktopacity'] = $hover_effect_opacity;
			}
		}

		if ( count( $thumbnail_ids ) > 1 && !$show_only_first ) {
			$showSlider = true;

			if ( isset( $shadow ) && $shadow != '' && $shadow != '0' && $shadow ) {
				$output .= "[intense_slider shadow='" . $shadow . "']";
			} else {
				$output .= "[intense_slider]";
			}
		}

		foreach ( $thumbnail_ids as $id ) {
			$extra_attributes = array();
			$thumbnail_title = '';
			$thumbnail_caption = '';
			$thumbnail_image = get_posts( array( 'p' => $id, 'post_type' => 'attachment', 'post_status' => 'any' ) );

			if ( $thumbnail_image && isset( $thumbnail_image[0] ) ) {
				$thumbnail_title = $thumbnail_image[0]->post_title;
				$thumbnail_caption = $thumbnail_image[0]->post_excerpt;
				$extra_attributes = array( 'title' => $post_title, 'caption' => $thumbnail_caption );
			}

			if ( is_numeric( $image_border_radius ) ) {
				$image_border_radius .= "px";
			}

			if ( $image_border_radius != '' ) {
				$extra_attributes['border_radius'] = $image_border_radius;
			}

			if ( $showSlider ) {
				$output .= "[intense_slide]";
			}

			if ( $image_type == 'caman' ) {
				$extra_attributes['effect'] = get_field( 'intense_caman_effect' );
				$margin_top = "-6";
			} elseif ( $image_type == 'picstrip' ) {
				$extra_attributes['splits'] = get_field( 'intense_picstrip_splits' );
				$extra_attributes['hgutter'] = get_field( 'intense_picstrip_hgutter' ) . 'px';
				$extra_attributes['vgutter'] = get_field( 'intense_picstrip_vgutter' ) . 'px';
				$extra_attributes['bgcolor'] = "#fff";
			} elseif ( $image_type == 'adipoli' ) {
				$extra_attributes['starteffect'] = get_field( 'intense_adipoli_start_effect' );
				$extra_attributes['hovereffect'] = get_field( 'intense_adipoli_hover_effect' );
			}

			if ( ( !isset( $id ) || $id == '' ) && $show_missing_image ) {
				$output .= intense_run_shortcode( 'intense_image', array_merge( $extra_attributes, $hovereffect, array(
							'type' => $image_type,
							'imageurl' => $intense_visions_options['intense_missing_image']['url'],
							'size' => $size
						) ) );
				$margin_top = "-6";
			} else if ( isset( $id ) && $id != '' ) {
					$output .= intense_run_shortcode( 'intense_image', array_merge( $extra_attributes, $hovereffect, array(
								'type' => $image_type,
								'imageid' => $id,
								'size' => $size
							) ) );
				} else {
				//TODO: show blank pixel the size of the image that should be there
			}

			if ( $showSlider ) {
				$output .= "[/intense_slide]";
			}

			if ( $show_only_first ) break;
		}

		if ( $showSlider ) {
			$output .= "[/intense_slider]";
			$output = do_shortcode( $output );
		}

		if ( $showShadow && !$showSlider ) {
			$output .= "</div><img class='intense shadow' src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow" . $shadow . ".png' style='margin-top: " . $margin_top . "px; vertical-align: top;' /></div>";
		}
	}

	return $output;
}

if ( ! function_exists( 'intense_return_posted_on' ) ) :
	/**
	 * Returns string with meta information for the current post-date/time and author.
	 */
	function intense_return_posted_on() {
		$read_more = "";
		$output = "";

		$output .= intense_run_shortcode( 'intense_icon', array( 'type' => 'calendar' ) ) . " <a href='" . esc_url( get_permalink() ) . "' title='" . esc_attr( get_the_time() ) . "' rel='bookmark'><time class='entry-date' datetime='" . esc_attr( get_the_date( 'c' ) ) . "'>" . esc_html( get_the_date() ) . "</time></a>";

		if ( intense_categorized_blog() ) {
			$categories_list = get_the_category_list( __( ', ', 'intense' ) );
			$categories_list = " " . intense_run_shortcode( 'intense_icon', array( 'type' => 'book' ) ) . " " . $categories_list;
		}

		$comment_count = " " . intense_run_shortcode( 'intense_icon', array( 'type' => 'comments' ) ) . " <a href='" . get_permalink() . "#comments'>" . get_comments_number() . " " . __( "comments", "intense" ) . "</a>";

		if ( !is_author() ) {
			$author = " " . intense_run_shortcode( 'intense_icon', array( 'type' => 'user' ) ) . " <span class='byline'> " . __( " by ", "intense" ) . " <span class='author vcard'><a class='url fn n' href='" . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . "' title='" . __( "View all posts by", "intense" ) . " " .  get_the_author() . "' rel='author'>" . get_the_author() . "</a></span></span>";
		} else {
			$author = "";
		}

		$output .= $comment_count;
		if ( isset( $categories_list ) ) $output .= $categories_list;
		$output .= $author;

		return $output;
	}

/**
 * Right to left display of post metadata
 *
 * @return string html for psot metadata
 */
function intense_return_posted_on_rtl() {
	$read_more = "";
	$output = "";

	if ( intense_categorized_blog() ) {
		$categories_list = "<span style='padding: 0 3px; display: inline-block;'>" . intense_run_shortcode( 'intense_icon', array( 'type' => 'book' ) ) . " " . get_the_category_list( __( ' ,', 'intense' ) ) . "</span>";
	}

	$comment_count = "<span style='padding: 0 3px; display: inline-block;'>" . intense_run_shortcode( 'intense_icon', array( 'type' => 'comments' ) ) . " <a href='" . get_permalink() . "#comments'>" . get_comments_number() . " " . __( "comments", "intense" ) . "</a></span>";

	if ( !is_author() ) {
		$author =  "<span style='padding: 0 3px; display: inline-block;'>" . intense_run_shortcode( 'intense_icon', array( 'type' => 'user' ) ) . "<span class='author vcard'><a class='url fn n' href='" . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . "' title='" . __( "View all posts by", "intense" ) . " " .  get_the_author() . "' rel='author'>" . get_the_author() . "</a></span></span>";
	} else {
		$author = "";
	}

	$output .= "<span style='padding: 0 0 0 3px; display: inline-block;'>" . intense_run_shortcode( 'intense_icon', array( 'type' => 'calendar' ) ). " <a href='" . esc_url( get_permalink() ) . "' title='" . esc_attr( get_the_time() ) . "' rel='bookmark'><time class='entry-date' datetime='" . esc_attr( get_the_date( 'c' ) ) . "'>" . esc_html( get_the_date() ) . "</time></a></span>";

	$output .= $comment_count;
	if ( isset( $categories_list ) ) $output .= $categories_list;
	$output .= $author;

	return $output;
}
endif;

/**
 * Returns navigation to next/previous pages when applicable
 *
 * @param string  $nav_id client id of navigation
 * @param integer $range  how many pages of navigation
 * @return  string the html for the navigation
 */
function intense_return_content_nav( $nav_id, $range = 2 ) {
	ob_start();

	intense_content_nav( $nav_id, $range );

	$output = ob_get_contents();

	ob_end_clean();

	return $output;
}

/**
 * Displays navigation to next/previous pages when applicable
 *
 * @param string  $nav_id client id of navigation
 * @param integer $range  how many pages of navigation
 * @return  string the html for the navigation
 */
function intense_content_nav( $nav_id, $range = 2 ) {
	global $wp_query, $post, $paged, $pages;

	$showitems = ( $range * 2 ) + 1;

	if ( empty( $paged ) ) $paged = 1;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';

?>
  <nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?>">

  <?php if ( is_single() ) : // navigation links for single posts ?>
    <ul class="pager">
    <?php previous_post_link( '<li class="previous">%link</li>', '<span class="meta-nav">' . _x( intense_run_shortcode( 'intense_icon', array( 'type' => "angle-left" ) ), 'Previous post link', 'intense' ) . '</span> Previous' ); ?>
    <?php next_post_link( '<li class="next">%link</li>', 'Next <span class="meta-nav">' . _x( intense_run_shortcode( 'intense_icon', array( 'type' => "angle-right" ) ), 'Next post link', 'intense' ) . '</span>' ); ?>
    </ul>
  <?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>
        <div class='intense pagination pagination-right pagination-mini clearfix'><ul>
        <?php
		if ( get_previous_posts_link() ) echo "<li><a class='pagination-prev' href='".get_pagenum_link( $paged - 1 )."'><span class='page-prev'>" . intense_run_shortcode( 'intense_icon', array( 'type' => "angle-left" ) ) . "</span> ".__( 'Previous', 'intense' )."</a></li>";

		for ( $i=1; $i <= $wp_query->max_num_pages; $i++ ) {
		if ( 1 != $pages &&( !( $i >= $paged+$range+1 || $i <= $paged-$range-1 ) || $pages <= $showitems ) ) {
			echo "<li class='" . ( ( $paged == $i ) ? "active" : "" ) .  "'><a href='" . get_pagenum_link( $i ) . "' >".$i."</a></li>";
		}
	}

	if ( get_next_posts_link() ) echo "<li><a class='pagination-next' href='".get_pagenum_link( $paged + 1 )."'>".__( 'Next', 'intense' )." <span class='page-next'>" . intense_run_shortcode( 'intense_icon', array( 'type' => "angle-right" ) ) . "</span></a></li>";
?>
        </ul></div>
  <?php endif; ?>

  </nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
  <?php
}

/**
 * Returns true if a blog has more than 1 category
 */
function intense_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
				'hide_empty' => 1,
			) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so intense_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so intense_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in intense_categorized_blog
 */
function intense_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'intense_category_transient_flusher' );
add_action( 'save_post', 'intense_category_transient_flusher' );

/**
 * Post excerpt
 *
 * @param int     $limit number of characters to show
 * @return string        excerpt text
 */
function intense_excerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );

	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode( " ", $excerpt ).'...';
	} else {
		$excerpt = implode( " ", $excerpt );
	}

	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );

	return $excerpt;
}

/**
 * Post content
 *
 * @param int     $limit number of characters to show
 * @return string        content text
 */
function intense_content( $limit ) {
	$original_content = get_the_content();
	$original_content = preg_replace( "~(?:\[/?)[^/\]]+/?\]~s", '', $original_content );

	$content = explode( ' ', $original_content, $limit );

	if ( count( $content ) >= $limit ) {
		array_pop( $content );
		$content = implode( " ", $content ).'...';
	} else {
		$content = implode( " ", $content );
	}

	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );

	return $content;
}

/**
 * Walker class for wp_list_pages function calls
 * The walker will run a template for each individual item
 * The template and template_path are sent in args
 */
class Intense_List_Pages_Walker extends Walker_page {
	function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
		global $intense_active_page;

		$intense_active_page = $object;

		if ( $depth )
			$indent = str_repeat( "\t", $depth );
		else
			$indent = '';

		extract( $args, EXTR_SKIP );

		$css_class = array( 'page_item', 'page-item-'.$intense_active_page->ID );

		if ( !empty( $current_page ) ) {
			$_current_page = get_page( $current_page );
			//     _get_post_ancestors($_current_page);
			if ( isset( $_current_page->ancestors ) && in_array( $intense_active_page->ID, (array) $_current_page->ancestors ) )
				$css_class[] = 'current_page_ancestor';
			if ( $intense_active_page->ID == $current_page )
				$css_class[] = 'current_page_item';
			elseif ( $_current_page && $intense_active_page->ID == $_current_page->post_parent )
				$css_class[] = 'current_page_parent';
		} elseif ( $intense_active_page->ID == get_option( 'page_for_posts' ) ) {
			$css_class[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $intense_active_page, $depth, $args, $current_page ) );

		$template_file = intense_locate_plugin_template( $template_path . $template . '.php' );

		if ( !isset( $template_file ) ) {
			$template_file = intense_locate_plugin_template( $template_path . 'link.php' );
		}

		$output .= $indent . '<li class="' . $css_class . '">' . intense_load_plugin_template( $template_file ) . '</li>';

		if ( !empty( $show_date ) ) {
			if ( 'modified' == $show_date )
				$time = $intense_active_page->post_modified;
			else
				$time = $intense_active_page->post_date;

			$output .= " " . mysql2date( $date_format, $time );
		}
	}
}

/**
 * Modified from get_avatar function defined
 * at http://core.trac.wordpress.org/browser/tags/3.5.2/wp-includes/pluggable.php?order=name
 */
function intense_get_avatar_image_url( $id_or_email, $size = '96', $default = '', $alt = false ) {
	if ( ! get_option( 'show_avatars' ) )
		return false;

	if ( false === $alt )
		$safe_alt = '';
	else
		$safe_alt = esc_attr( $alt );

	if ( !is_numeric( $size ) )
		$size = '96';

	$email = '';
	if ( is_numeric( $id_or_email ) ) {
		$id = (int) $id_or_email;
		$user = get_userdata( $id );
		if ( $user )
			$email = $user->user_email;
	} elseif ( is_object( $id_or_email ) ) {
		// No avatar for pingbacks or trackbacks
		$allowed_comment_types = apply_filters( 'get_avatar_comment_types', array( 'comment' ) );
		if ( ! empty( $id_or_email->comment_type ) && ! in_array( $id_or_email->comment_type, (array) $allowed_comment_types ) )
			return false;

		if ( !empty( $id_or_email->user_id ) ) {
			$id = (int) $id_or_email->user_id;
			$user = get_userdata( $id );
			if ( $user )
				$email = $user->user_email;
		} elseif ( !empty( $id_or_email->comment_author_email ) ) {
			$email = $id_or_email->comment_author_email;
		}
	} else {
		$email = $id_or_email;
	}

	if ( empty( $default ) ) {
		$avatar_default = get_option( 'avatar_default' );
		if ( empty( $avatar_default ) )
			$default = 'mystery';
		else
			$default = $avatar_default;
	}

	if ( !empty( $email ) )
		$email_hash = md5( strtolower( trim( $email ) ) );

	if ( is_ssl() ) {
		$host = 'https://secure.gravatar.com';
	} else {
		if ( !empty( $email ) )
			$host = sprintf( "http://%d.gravatar.com", ( hexdec( $email_hash[0] ) % 2 ) );
		else
			$host = 'http://0.gravatar.com';
	}

	if ( 'mystery' == $default )
		$default = "$host/avatar/ad516503a11cd5ca435acc9bb6523536?s={$size}"; // ad516503a11cd5ca435acc9bb6523536 == md5('unknown@gravatar.com')
	elseif ( 'blank' == $default )
		$default = $email ? 'blank' : includes_url( 'images/blank.gif' );
	elseif ( !empty( $email ) && 'gravatar_default' == $default )
		$default = '';
	elseif ( 'gravatar_default' == $default )
		$default = "$host/avatar/?s={$size}";
	elseif ( empty( $email ) )
		$default = "$host/avatar/?d=$default&amp;s={$size}";
	elseif ( strpos( $default, 'http://' ) === 0 )
		$default = add_query_arg( 's', $size, $default );

	if ( !empty( $email ) ) {
		$out = "$host/avatar/";
		$out .= $email_hash;
		$out .= '?s='.$size;
		$out .= '&amp;d=' . urlencode( $default );

		$rating = get_option( 'avatar_rating' );
		if ( !empty( $rating ) )
			$out .= "&amp;r={$rating}";

		return $out;
	} else {
		return $default;
	}
}
