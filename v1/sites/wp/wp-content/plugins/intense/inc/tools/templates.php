<?php
/**
 * Functions related to templates
 */

// Executes a plugin template and returns the content
function intense_load_plugin_template( $template_file ) {
	ob_start();

	if ( isset( $template_file ) ) {
		include( $template_file );
	} else {
		?>
		<div>
		Unable to locate template file at one of the following locations in the order listed: <br />
			- <?php echo get_stylesheet_directory() . '/intense_templates';?>
			- <?php echo get_template_directory() . '/intense_templates'; ?>
			- <?php echo INTENSE_PLUGIN_FOLDER  . '/templates'; ?>
			<hr />
		</div>
		<?php 
	}

	$var = ob_get_contents();

	ob_end_clean();

	return $var;
}

// Checks available paths for the plugin template file
function intense_locate_plugin_template( $template_name ) {
	if ( file_exists( get_stylesheet_directory() . '/intense_templates' . $template_name ) ) {
		return get_stylesheet_directory()  . '/intense_templates' . $template_name;
	} else if ( file_exists( get_template_directory()  . '/intense_templates' . $template_name ) ) {
		return get_template_directory()  . '/intense_templates' . $template_name;
	} else if ( file_exists( INTENSE_PLUGIN_FOLDER  . '/templates' . $template_name ) ) {
		return INTENSE_PLUGIN_FOLDER  . '/templates' . $template_name;
	}  else {
		return null;
	}
}

function intense_get_plugin_template_name( $path ) {
	$content = file_get_contents( $path );

	$lines = explode( "\n", $content);

	foreach ( $lines as $line ) {
		$remove_template_start_text =  str_replace( "Intense Template Name:", "", $line );

		// early terminate if name found
		if ( $remove_template_start_text != $line ) {
			return trim( $remove_template_start_text );
		}
	}

	return str_replace( ".php", "", substr( $path, strrpos( $path, "/" ) + 1 ) );
}

function intense_locate_single_cpt_templates( $template_type, $single = false ) {
    global $post, $wpdb;
    $templates = array();
    $count = 0;

    $querystr = "
      SELECT $wpdb->posts.* 
      FROM $wpdb->posts
      WHERE $wpdb->posts.post_status = 'publish' 
      AND $wpdb->posts.post_type = 'intense_templates'
      ORDER BY $wpdb->posts.post_date DESC";

    $pageposts = $wpdb->get_results( $querystr, OBJECT );

    if ($pageposts):
      	foreach ( $pageposts as $template ):
	        $post = get_post( $template->ID );
	        $type =  get_post_meta( $post->ID, 'intense_templates_post_type', true );
	        $is_single = get_post_meta( $post->ID, 'intense_templates_type', true );

	        if ( $type === $template_type ) {
		          if ( $single && $is_single === 'single' ) {
		            $templates[ "template_" .  $post->ID ] = get_the_title();
		          } elseif ( !$single && $is_single === 'multiple' ) {
		            $templates[ "template_" .  $post->ID ] = get_the_title();
		          }        
	        }

      	endforeach;
    endif;

      return $templates;
}

function intense_locate_available_plugin_templates( $relative_path ) {
	$plugin_files = array();
	$theme_files = array();
	$child_theme_files = array();
	$templates = array();

	if ( file_exists( INTENSE_PLUGIN_FOLDER . '/templates' . $relative_path ) ) {
		$plugin_files = scandir( INTENSE_PLUGIN_FOLDER . '/templates' . $relative_path );
		$plugin_files = array_flip( $plugin_files );

		foreach ( $plugin_files as $key => &$value ) {
			if ($key != '.' && $key != '..' && substr( $key, -strlen( ".php" )) == ".php" ) {
				$value = intense_get_plugin_template_name( INTENSE_PLUGIN_FOLDER . '/templates' . $relative_path . $key );
				$templates[ str_replace( ".php", "", $key ) ] = $value;
			}
		}
	}
	
	if ( file_exists( get_template_directory() . '/intense_templates' . $relative_path ) ) {
		$theme_files = scandir( get_template_directory() . '/intense_templates' . $relative_path );
		$theme_files = array_flip( $theme_files );

		foreach ( $theme_files as $key => $value ) {
			if ($key != '.' && $key != '..' && substr( $key, -strlen( ".php" )) == ".php" ) {
				$value = intense_get_plugin_template_name( get_template_directory() . '/intense_templates' . $relative_path . $key );
				$templates[ str_replace( ".php", "", $key ) ] = $value;
			}
		}
	}

	if ( file_exists( get_stylesheet_directory() . '/intense_templates' . $relative_path ) ) {
		$child_theme_files = scandir( get_stylesheet_directory() . '/intense_templates' . $relative_path );
		$child_theme_files = array_flip( $child_theme_files );

		foreach ( $child_theme_files as $key => $value ) {
			if ($key != '.' && $key != '..' && substr( $key, -strlen( ".php" )) == ".php" ) {
				$value = intense_get_plugin_template_name( get_stylesheet_directory() . '/intense_templates' . $relative_path . $key );
				$templates[ str_replace( ".php", "", $key ) ] = $value;
			}
		}
	}	

	return array_filter( $templates );
}