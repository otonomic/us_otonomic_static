<?php

class Intense_Field_Border_Style extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array(
				'dotted' => __( 'Dotted', 'intense' ),
				'dashed' => __( 'Dashed', 'intense' ),
				'solid' => __( 'Solid', 'intense' ),
				'double' => __( 'Double', 'intense' ),
				'groove' => __( 'Groove', 'intense' ),
				'ridge' => __( 'Ridge', 'intense' ),
				'inset' => __( 'Inset', 'intense' ),
				'outset' => __( 'Outset', 'intense' ),
			);
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function map_visual_composer() {
		$this->set_default_options();

		return parent::map_visual_composer();
	}
}
