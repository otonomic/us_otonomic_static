<?php

class Intense_Field_Icon extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			global $intense_icons;
			
			$this->args['options'] = array_merge(
				array( '' => __( 'Select', 'intense' ) . '...' ),
				array_combine( $intense_icons, $intense_icons )
			);			
		}
	}

	public function render_field() {
		$this->class .= ' selectize icon';
		return parent::render_field();
	}

	public function render_init_script() {
		return "$('.selectize.icon select').not('.repeater-item-model .selectize.icon select').selectize({
    				sortField: 'text',
    				render: {
				        option: function(item, escape) {
				        	return '<div style=\"float: left; width: 20px; text-align: center;\">' + 
				        	'<i class=\"intense icon-1x icon-' + item.value + '\" title=\"' + item.text + '\"></i>'  +
				        	'</div>';
				        },
				        item: function(item, escape) {
				        	return '<div>' + 
				        	'<i class=\"intense icon-' + item.value + '\" title=\"' + item.text + '\"></i> ' + item.text  +
				        	'</div>';
				        }
			        }
				});";
	}	

	protected function render() {
		if ( empty( $this->args['class'] ) ) $this->args['class'] = '';
		if ( empty( $this->args['style'] ) ) $this->args['style'] = '';

		$this->set_default_options();

		return parent::render();
	}

	public function map_visual_composer() {
		$this->set_default_options();

		return parent::map_visual_composer();
	}
}
