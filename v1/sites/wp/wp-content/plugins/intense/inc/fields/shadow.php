<?php

class Intense_Field_Shadow extends Intense_Field {
	protected function render() {
		$shadow_count = 14;

		if ( empty( $this->value ) ) {
			$this->value = 0;
		}

		$output = '';
		$output .= '<div class="pull-left">';
		$output .= '	<select name="' . $this->id . '" id="' . $this->id . '" class="shadowselector">';
		$output .= '		<option value="0" ' . ( $this->value == "0" ? ' selected' : '' ) . '>' . __( 'None', 'intense' ) . '</option>';

		for ( $i = 1; $i <= $shadow_count; $i++ ) {
			$output .= '<option' . ( $this->value == $i ? ' selected' : '' ) . '>' . $i . '</option>';
		}

		$output .= '	</select>';
		$output .= '</div>';
		$output .= '<div class="pull-right" style="width: 300px; margin-right: 250px;">';
		$output .= '	<div style="height:30px; background:#dfdfdf;"></div>';
		$output .= '	<img class="shadowpreview" src="' . INTENSE_PLUGIN_URL . '/assets/img/shadow' . $this->value . '.png" style="vertical-align: top;" />';
		$output .= '</div>';

		return $output;
	}

	public function render_init_script() {
		return '$(".shadowselector").unbind("change"); $(".shadowselector").change(function() {
					$(this).parent().next().children(".shadowpreview").attr("src", "' . INTENSE_PLUGIN_URL . '/assets/img/shadow" + $(this).val() + ".png");
					intenseRefreshPreview();
				});	';
	}

	public function render_value_script() {
		return '($(\'#' . $this->id . '\').val().length > 0 && $(\'#' . $this->id . '\').val() !== \'0\' ? \' ' . $this->id . '="\' + $(\'#' . $this->id . '\').val() + \'"\' : "" ) + ';
	}

	public function map_visual_composer() {
		return array(
			"type" => "intense_shadow",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}
