<?php

class Intense_Field_Link_Target extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array(
				'_blank' => __( 'Blank', 'intense' ),
				'_self' => __( 'Self', 'intense' ),
				'_parent' => __( 'Parent', 'intense' ),
				'_top' => __( 'Top', 'intense' )
			);
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function map_visual_composer() {
		$this->set_default_options();

		return parent::map_visual_composer();
	}
}
