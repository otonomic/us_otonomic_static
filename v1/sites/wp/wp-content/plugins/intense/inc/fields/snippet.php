<?php

class Intense_Field_Snippet extends Intense_Field {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$query = new WP_Query( array( 'post_type' => 'intense_snippets' ) );

			$snippets = array();

			$snippets['Saved in WordPress'] = array(
				'title' => 'Saved in WordPress',
				'snippets' => array()
			);

			while ( $query->have_posts() ) : $query->the_post();
				$id = get_the_ID();
				$title = get_the_title( $id );
				$content = get_the_content();

				$snippets['Saved in WordPress']['snippets'][] = array(
					'id' => $id,
					'title' => $title,
					'content' => $content
				);
			endwhile;

			$file_snippets = intense_locate_available_plugin_snippets();

			foreach ( $file_snippets as $snippet_location_name => $snippet_location ) {
				$root = intense_get_snippet_location_path( $snippet_location_name );

				foreach ( $snippet_location as $directory => $snippet_list ) {
					$location_key = $snippet_location_name . ( !empty( $directory ) ? ' - ' . $directory : '' );

					if ( !isset( $snippets[ $location_key ] ) ) {
						$snippets[ $location_key ] = array(
							'title' => $location_key,
							'snippets' => array()
						);
					}

					foreach ( $snippet_list as $snippet => $title ) {
						$snippet_path = $root . ( !empty( $directory ) ? $directory . '/' : '' ) . $snippet . '.php';
						$content = intense_load_plugin_snippet( $snippet_path );

						$snippets[ $location_key ]['snippets'][] = array(
							'id' => $snippet_location_name . ' | ' . str_replace( '.php', '', str_replace( $root, '', $snippet_path ) ),
							'title' => $title,
							'content' => $content
						);
					}
				}
			}

			$this->args['options'] = $snippets;
		}
	}

	protected function render() {
		$this->set_default_options();

		$snippets_locations = $this->args['options'];

		$output = '<select class="dialog snippet" name="' . $this->id . '" id="' . $this->id . '">';
		$output .= '<option></option>';

		foreach ($snippets_locations as $snippet_location_name => $snippet_location) {
			$output .= '<optgroup label="' . $snippet_location_name . '">';

			foreach ($snippet_location['snippets'] as $key => $snippet) {
				$output .= '<option value="' . esc_attr( $snippet['id'] ) . '">' . esc_html( $snippet['title'] ) . '</option>';
			}

			$output .= '</optgroup>';
		}

		$output .= '</select>';
		$output .= $this->description;

		return $output;
	}

	public function render_init_script() {
		return "jQuery('.dialog.snippet').selectize({
					create: false,
					sortField: 'text',
					render: {
						option: function(item, escape) {
							return '<div>' + item.text + '</div>';
						},
						item: function(item, escape) {
							return '<div><i class=\"dashicons dashicons-intense-scissors\" style=\"font-size: 15px;\" title=\"' + item.text + '\"></i> ' + item.text + '</div>';
						}
					},
					onChange: function(value) {
						jQuery('#snippet_title').val( jQuery('.selectize-input .dashicons-intense-scissors').attr('title') );
						intenseRefreshPreview();
					}
				});";
	}

	// public function map_visual_composer() {
	// 	$this->set_default_options();

	// 	return parent::map_visual_composer();
	// }
}
