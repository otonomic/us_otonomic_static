<?php

class Intense_Field_Author extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array();

			$authors = get_users('who=authors');

			foreach ( $authors as $user ) {
				$this->args['options'][ $user->ID ] = $user->display_name;
			}
		}
	}

	protected function render() {
		$this->set_default_options();

		$this->args['multiple'] = true;

		return parent::render();
	}

	public function render_value_script() {
		$this->args['multiple'] = true;

		return parent::render_value_script();
	}

	public function map_visual_composer() {
		$this->set_default_options();
		
		return parent::map_visual_composer();
	}	
}