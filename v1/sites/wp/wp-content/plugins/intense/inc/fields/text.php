<?php

class Intense_Field_Text extends Intense_Field {
	protected function render() {
		return '<input class="dialog text" type="text" name="' . $this->id . '" id="' . $this->id . '" value="' . $this->value . '" /> ' . $this->description;
	}

	public function render_init_script() {
		return '$(".dialog.text").typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});';
	}

	public function map_visual_composer() {
		return array(
			"type" => "textfield",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}
