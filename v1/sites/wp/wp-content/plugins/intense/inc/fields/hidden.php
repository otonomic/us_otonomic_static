<?php

class Intense_Field_Hidden extends Intense_Field {
	public function render_field() {
		return '<input type="hidden" name="' . $this->id . '" id="' . $this->id . '" value="' . $this->value . '" />';
	}
}