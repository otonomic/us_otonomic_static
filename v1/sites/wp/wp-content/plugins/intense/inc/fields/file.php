<?php

class Intense_Field_File extends Intense_Field {
	protected function render() {
		$output = '<div class="pull-left">';
		$output .= '<div class="input-append">';
		$output .= '  <input class="dialog file" type="text" id="' . $this->id . '" name="' . $this->id . '"' . $this->placeholder . $this->style . ' value="' . $this->value . '"/>';
		$output .= '  <input type="button" class="upload_file_button button" data-image-number="1" data-target-id="' . $this->id . '" data-uploader_title="' . __( 'Select File', 'intense' ) . '" data-uploader_button_text="' . __( 'Select', 'intense' ) . '" value="' . __( "Select File", 'intense' ) . '" />';
		$output .= '</div>';
		$output .= $this->description;
		$output .= '</div>';		

		return $output;
	}

	public function render_init_script() {
		return '$(".dialog.file").typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});';
	}

	public function map_visual_composer() {
		return array(
			"type" => "intense_file",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}