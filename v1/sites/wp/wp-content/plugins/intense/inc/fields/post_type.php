<?php

class Intense_Field_Post_type extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array();

			$postargs = array(
			   'public' => true
			);

			$outputtype = 'names'; // names or objects, note names is the default
			$operator = 'and'; // 'and' or 'or'

			$post_types = get_post_types( $postargs, $outputtype, $operator );

			foreach ( $post_types as $post_type ) {
				if ( $post_type != 'attachment' && $post_type != 'product_variation' && $post_type != 'shop_coupon' && $post_type != 'intense_faq' ) {
					$type = get_post_type_object( $post_type );
					$this->args['options'][ $post_type ] = $type->label;
				}
			}
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function render_value_script() {
		return parent::render_value_script();
	}

	public function map_visual_composer() {
		$this->set_default_options();
		
		return parent::map_visual_composer();
	}
}