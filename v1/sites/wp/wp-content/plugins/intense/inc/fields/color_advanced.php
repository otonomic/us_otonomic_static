<?php

class Intense_Field_Color_Advanced extends Intense_Field {
	protected function render() {
		$output = '';
		$output .= '<div class="dialog color-advanced"><select class="dialog color-advanced-select" name="' . $this->id . '" id="' . $this->id . '"' . $this->style . '>';
		$output .= '<option value=""' . ( empty( $this->value ) ? ' selected' : '' ) . '>' . __( 'Preset color...', 'intense' ) . '</option>';

		$colors = array(
			'primary' => __( 'Primary', 'intense' ),
			'error' => __( 'Error', 'intense' ),
			'info' => __( 'Info', 'intense' ),
			'inverse' => __( 'Inverse', 'intense' ),
			'muted' => __( 'Muted', 'intense' ),
			'success' => __( 'Success', 'intense' ),
			'warning' => __( 'Warning', 'intense' ),
			'link' => __( 'Link', 'intense' ),
		);

		foreach ($colors as $key => $color_name) {
			$output .= '<option value="' . $key . '"' . ( $this->value == $key ? ' selected' : '' ) . '>' . $color_name . '</option>';			
		}
		
		$output .= '</select> ';
		$output .= __( 'or', 'intense' );
		$output .= ' <div style="display: inline-block; line-height: 35px;">
		<input name="hex' . $this->id . '" id="hex' . $this->id . '" class="of-color hex" type="text" value="' . ( !empty( $this->value ) && $this->value[0] == '#' ? $this->value : ''  ) . '" data-default-color="' . $this->value . '" />
		</div></div>';
		$output .= $this->description;

		return $output;
	}

	public function render_init_script() {
		return "$('.of-color.hex.minicolors-input').minicolors('destroy');
			$('.of-color.hex').minicolors({
                    changeDelay: 200,
                    animationEasing: 'swing',
                    //opacity: true,
                    change: function() {
                    	if ($(this).minicolors('value') != '') {
	                        var id = $(this).attr('id');
	                        var presetid = id.replace('hex', '');

	                        $('#' + presetid).val('');

	                        intenseRefreshPreview();
                    	}
                    }
                });
			$('.dialog.color-advanced-select').unbind('change');
			$('.dialog.color-advanced-select').change(function(e) {
				$(this).parent().find('.of-color.hex').minicolors('value', '');
				e.preventDefault();				
				intenseRefreshPreview();
			});
		";
	}

	public function render_value_script() {
		if ( $this->is_skin_dialog ) {
			$this->default = null;
		}

		return '(( $(\'#' . $this->id . '\').val() === "" ? $(\'#hex' . $this->id . '\').val() : $(\'#' . $this->id . '\').val() ).length > 0' . ( $this->default != null ? ' && ( $(\'#' . $this->id . '\').val() === "" ? $(\'#hex' . $this->id . '\').val() : $(\'#' . $this->id . '\').val() ) != \'' . $this->default . '\'' : '' ) . ' ? \' ' . $this->id . '="\' + ( $(\'#' . $this->id . '\').val() === "" ? $(\'#hex' . $this->id . '\').val() : $(\'#' . $this->id . '\').val() ) + \'"\' : "" ) + ';
	}

	public function map_visual_composer() {
		return array(
			"type" => "intense_color",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}
