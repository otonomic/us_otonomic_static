<?php

class Intense_Field {
	public $id;
	public $args = array();
	protected $class;
	protected $style;
	protected $placeholder;
	protected $default;
	protected $value;
	protected $composer_show_value;

	function __construct( $id, $args ) {
		$this->id = $id;
		$this->args = $args;
		$this->title = isset( $this->args['title'] ) ? $this->args['title'] : null;
		$this->description = isset( $this->args['description'] ) ? $this->args['description'] : null;
		$this->placeholder = isset( $this->args['placeholder'] ) ? ' data-placeholder="' . $this->args['placeholder'] . '"' : '';
		$this->style = isset( $this->args['style'] ) ? ' style="' . $this->args['style'] . '"' : '';
		$this->class = isset( $this->args['class'] ) ? $this->args['class'] : '';
		$this->default = isset( $this->args['default'] ) ? $this->args['default'] : null;
		$this->value = isset( $this->args['value'] ) ? $this->args['value'] : null;
		//override value with default if value isn't present
		$this->value = ( isset( $this->value ) && !empty( $this->value ) ? $this->value : $this->default );
		if ( !empty( $this->description ) ) $this->description = '<span class="intense help-block">' . $this->description . '</span>';
		$this->composer_show_value = isset( $this->args['composer_show_value'] ) ? $this->args['composer_show_value'] : false;
		$this->is_skin_dialog = isset( $this->args['is_skin_dialog'] ) ? $this->args['is_skin_dialog'] : false;	
	}

	public function render_field() {
		$output = '';
		$output .= '<div class="intense form-group ' . $this->class . '">';
		$output .= '<label class="intense col-sm-2 control-label" for="' . $this->id . '"' . $this->placeholder . $this->style . '>' . $this->title . '</label>';
		$output .= '<div class="intense col-sm-8">';
		$output .= $this->render();
		$output .= '</div>';
		$output .= '</div>';

		return $output;
	}

	protected function render() {}

	public function render_init_script() { return ''; }

	public function render_value_script() {
		//don't check for default value if you are working with the skin dialog
		if ( $this->is_skin_dialog ) {
			$js_default = null;
		} else {
			$js_default = $this->default;
		}		

		if ( is_string( $js_default ) ) {
			$js_default = '\'' . $js_default . '\'';
		}

		return '($(\'#' . $this->id . '\').val() != null && $(\'#' . $this->id . '\').val().length > 0' . ( $js_default != null ? ' && $(\'#' . $this->id . '\').val() !== ' . $js_default : '' )  . ' ? \' ' . $this->id . '="\' + $(\'#' . $this->id . '\').val() + \'"\' : "" ) + ';
	}

	public function map_visual_composer() {
		return null;
	}
}

require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/dropdown.php';

require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/animation.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/author.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/border.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/border_radius.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/border_style.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/category.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/checkbox.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/color.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/color_advanced.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/deprecated.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/file.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/gallery.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/hidden.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/icon.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/image.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/image_size.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/link_target.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/post_type.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/repeater.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/shadow.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/snippet.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/taxonomy.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/template.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/text.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/textarea.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/video.php';
