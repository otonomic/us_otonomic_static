<?php

class Intense_Field_Image_Size extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$sizes = get_intermediate_image_sizes();
			$this->args['options'] = array_merge( 
				array( '' => __( 'Select', 'intense' ) . '...' ), 
				array_combine( $sizes, $sizes ) 
			);
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function map_visual_composer() {
		$this->set_default_options();

		return parent::map_visual_composer();
	}
}