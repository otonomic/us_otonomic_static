<?php

class Intense_Field_Template extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array();
			$templates = intense_locate_available_plugin_templates($this->args[ 'path' ]);

			foreach ($templates as $key => $value) {
				$this->args['options'][ $key ] = $value;
			}			
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function map_visual_composer() {
		$this->set_default_options();
		
		return parent::map_visual_composer();
	}
}