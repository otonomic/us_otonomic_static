<?php

class Intense_Field_Repeater extends Intense_Field {
	protected function render() {
        $i = 0;
        $tab_started = false;
        $tab_contents = '';
        $is_container = isset( $this->args['is_container'] ) && $this->args['is_container'] ? true : false;
        $preview_content = isset( $this->args['preview_content'] ) ? $this->args['preview_content'] : '';

		//$output = '<div class="repeater-container" style="max-height: 250px; padding: 10px; overflow: auto;">';
		$output = '<div class="repeater-item-model" style="display: none; margin-bottom: 10px;">';
		$output .= '<div class="repeater-title " style="background: #ccc; padding: 5px;">';
		$output .= '	<div class="repeater-number" style="border-radius: 50%; background: #fff; display: inline-block; width: 20px; height: 20px; text-align: center; font-size: 10px; line-height: 20px;">1</div>';
		$output .= '	<div class="repeater-remove" style="display: inline-block; float: right; cursor: pointer; font-size: 20px;">&times;</div>';
		$output .= '	<div class="repeater-hide" style="display: inline-block; float: right; cursor: pointer; font-size: 20px; padding-right: 5px;">&minus;</div>';		
		$output .= '</div>';
		$output .= '<div class="repeater-content" style="border-color: #ccc; background: #f1f1f1; padding: 5px;">';

		$repeater_script = '<script>function intenseRepeaterShortcode( id ) {' . "\n\t" . 'var $ = jQuery.noConflict();' . "\n\t" . 'return \'[intense_' . strtolower( $this->id ) . "' + ";

		foreach ( $this->args['fields'] as $key => $value ) {			
			
            $type = $value['type'];

            if ( $type == 'tab' ) {
                $title = $value['title'];
                $tab_fields = $value['fields'];

                if ( !$tab_started ) {
                    $output .= '<ul class="nav nav-tabs">';
                    $tab_started = true;
                    $class = 'active';
                } else {
                    $class = '';
                }

                $output .= '<li class="' . $class . '"><a href="#' . $key . '" data-toggle="tab">' . $title . '</a></li>';
                $tab_contents .= '<div class="tab-pane ' . $class . '" id="' . $key . '">';

                foreach ( $tab_fields as $tab_key => $tab_value ) {
                    $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $tab_value['type'] ) ) );
                    $field = new $field_class_name( $tab_key . '_id', $tab_value );
                    $tab_contents .= $field->render_field();
                    $repeater_script .= "\n\t " . str_ireplace( "_id'", "_' + id", $field->render_value_script() );
                }

                $tab_contents .= '</div>';
            } else {
                $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $type ) ) );
                $field = new $field_class_name( $key . '_id', $value );                
                $output .= $field->render_field();
                $repeater_script .= "\n\t " . str_ireplace( "_id'", "_' + id", $field->render_value_script() );                
            }            
        }

        $repeater_script = str_ireplace("_id", "", $repeater_script);

        $repeater_script .= "\n\t " .'\']' .
            ( isset( $preview_content ) || $is_container ? $preview_content . '[/intense_' . strtolower( $this->id ) . ']' : '' ) .
            '\';' . "\n " . '}</script>';

        if ( $tab_started ) {
            $output .= '</ul><div class="tab-content">' . $tab_contents . '</div>';
        }	

        $output .= '</div>'; //content
        $output .= '</div>'; //item
        //$output .= '</div>'; //container
       	$output .= '<a href="#" class="button repeater-add ">Add</a>';
       	$output .= $repeater_script;
       	$output .= $this->description;
        $output .= '</div>'; //field
        $output .= '</div>'; //group

        return $output;
	}

	public function render_value_script() {
		return '';
	}
}
