<?php

class Intense_Field_Color extends Intense_Field {
	protected function render() {
		$output = '<input name="' . $this->id . '" id="' . $this->id . '" class="of-color color" type="text" value="' . $this->value . '" data-default-color="' . $this->default . '"' . $this->style . '/> ';
		$output .= $this->description;

		return $output;
	}

	public function render_init_script() {
		return "$('.of-color.color.minicolors-input').minicolors('destroy');
			$('.of-color.color').minicolors({
                    changeDelay: 200,
                    animationEasing: 'swing',
                    //opacity: true,
                    change: function() {
                        intenseRefreshPreview();
                    }
                });";
	}

	public function map_visual_composer() {
		return array(
			"type" => "colorpicker",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}