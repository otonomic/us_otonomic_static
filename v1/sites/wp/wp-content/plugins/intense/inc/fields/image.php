<?php

class Intense_Field_Image extends Intense_Field {
	protected function render() {
		$output = '<div class="pull-left">';
		$output .= '<div class="input-append">';
		$output .= '  <input class="dialog image" type="text" id="' . $this->id . '" name="' . $this->id . '"' . $this->placeholder . $this->style . ' value="' . $this->value . '"/>';
		$output .= '  <input type="button" class="upload_image_button button" data-image-number="1" data-target-id="' . $this->id . '" data-uploader_title="' . __( 'Select Image', 'intense' ) . '" data-uploader_button_text="' . __( 'Select', 'intense' ) . '" value="' . __( "Select Image", 'intense' ) . '" />';
		$output .= '</div>';
		$output .= $this->description;
		$output .= '</div>';
		$output .= '<div class="pull-left">&nbsp;';
		$output .= '	<img id="' . $this->id . '-thumb" src="' . INTENSE_PLUGIN_URL . '/assets/img/missing_image.png" style="max-width: 150px; max-height: 30px; vertical-align: top;" />';
		$output .= '</div>';

		return $output;
	}

	public function render_init_script() {
		return '$(".dialog.image").typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});';
	}

	public function map_visual_composer() {
		return array(
			"type" => "attach_image",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			//'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}