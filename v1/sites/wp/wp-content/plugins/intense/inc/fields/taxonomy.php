<?php

class Intense_Field_Taxonomy extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array();
			$post_type = $this->args['category'];

			$taxonomy_names = get_object_taxonomies( $post_type );

			foreach ($taxonomy_names as $taxonomy_name ) {
				$tax_obj = get_taxonomy( $taxonomy_name );
				$this->args['options'][ $taxonomy_name ] = $tax_obj->name;
			}
		}
	}

	protected function render() {
		$this->set_default_options();

		return parent::render();
	}

	public function render_value_script() {
		return parent::render_value_script();
	}

	public function map_visual_composer() {
		$this->set_default_options();
		
		return parent::map_visual_composer();
	}
}