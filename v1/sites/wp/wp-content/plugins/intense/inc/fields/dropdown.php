<?php

class Intense_Field_Dropdown extends Intense_Field {

	function __construct( $id, $args ) {
		parent::__construct( $id, $args );

		// check to make sure you are working with a dropdown since other
		// classes inherit from the dropdown field
		if ( $args['type'] == 'dropdown') {
			$multiple = isset( $this->args[ 'multiple' ] ) ? ' multiple' : '';

			$this->default = isset( $this->args['default'] ) ? $this->args['default'] : null;
			$this->value = isset( $this->args['value'] ) ? $this->args['value'] : null;
			//override value with default if value isn't present
			$this->value = ( $this->value != null ? $this->value : $this->default );

			// if still no value, use the first option
			if ( $this->value == null ) {
				reset( $this->args[ 'options' ] );
		 		$this->value = key( $this->args[ 'options' ] );
			}
		}
		
	}

	protected function render() {
		$multiple = isset( $this->args[ 'multiple' ] ) ? ' multiple' : '';

		if ( $multiple ) {
			$values = explode( ',', $this->value );
			$this->style = ' style="width: 180px"';
		} else {			
			$values = array( $this->value );
			$this->style = ' style="min-width: 180px"';
		}

		$output = '<select class="dialog select" name="' . $this->id . '" id="' . $this->id . '"' . $multiple . $this->placeholder . $this->style . '>';

		if ( isset( $this->args[ 'options' ] ) ) {
			foreach ( $this->args[ 'options' ] as $key => $name ) {
				//optgroup
				if ( is_array( $name ) ) {
					$output .= '<optgroup label="' . $key . '">';

					foreach ( $name as $optgroupvalue => $optgroupname ) {
						//non string values means that no specific value was set so we will use the name as the value
						if ( !is_string( $optgroupvalue ) ) $optgroupvalue = $optgroupname;

						$selected = ( in_array( $optgroupvalue, $values )  ? ' selected="selected"' : '' );
						$output .= '<option value="' . $optgroupvalue . '"' . $selected . '>' . $optgroupname . '</option>';
					}

					$output .= '</optgroup>';
				} else {
					$selected = ( in_array( $key, $values )  ? ' selected="selected"' : '' );
					$output .= '<option value="' . $key . '"' . $selected . '>' . $name . '</option>';
				}
			}
		}

		$output .= '</select>';
		$output .= $this->description;

		return $output;
	}

	public function render_init_script() {
		return '$(".dialog.select").change(intenseRefreshPreview);';
	}

	public function render_value_script() {
		if ( !empty( $this->args['multiple'] ) &&  $this->args['multiple'] ) {
			$valueScript = "$('#" . $this->id . " option:selected').map(function(){ return this.value }).get().join(', ')";
			return '((' . $valueScript . ').length > 0 ? \' ' . $this->id . '="\' + ' . $valueScript . ' + \'"\' : "" ) + ';
		} else {
			return parent::render_value_script();
		}
	}

	public function map_visual_composer() {
		return array(
			"type" => "dropdown",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => ( isset( $this->args['options'] ) ?  $this->array_swap_assoc( $this->args['options'] ) : null ),
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}

	private function array_swap_assoc($array) {
	  $newArray = array ();
	  foreach ($array as $key => $value) {	    
	    if (!is_array($value)) {
	    	$newArray[ $value ] = $key;
		}
	  }
	  return $newArray;
	}
}
