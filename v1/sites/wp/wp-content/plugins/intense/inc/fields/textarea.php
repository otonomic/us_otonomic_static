<?php

class Intense_Field_Textarea extends Intense_Field {
	protected function render() {
		$rows = isset( $this->args[ 'rows' ] ) ? ' rows="' . $this->args[ 'rows' ] . '"' : '';
		$columns = isset( $this->args[ 'columns' ] ) ? ' cols="' . $this->args[ 'columns' ] . '"' : '';

		return '<textarea class="dialog textarea" type="text" name="' . $this->id . '" id="' . $this->id . '" value="' . $this->value . '"' . $rows . $columns . '>' . $this->default . '</textarea>' . $this->description;
	}

	public function render_init_script() {
		return '$(".dialog.textarea").typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});';
	}

	public function map_visual_composer() {
		return array(
			"type" => "textarea",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}