<?php

class Intense_Field_Checkbox extends Intense_Field {
	protected function render() {
		$checked = isset( $this->value ) && ( $this->value == 1 || $this->value == '1' ) ? ' checked' : '';

		return '<input type="checkbox" class="dialog chk" name="' . $this->id . '" id="' . $this->id . '" ' . $this->id . $checked . '> ' . $this->description;
	}

	public function render_init_script() {
		return '$(".dialog.chk").change(intenseRefreshPreview);';
	}

	public function render_value_script() {
		if ( empty( $this->default ) ) {
			$this->default = '0';
		}

		if ( $this->is_skin_dialog ) {
			return '(\' ' . $this->id . '="\' + ( $(\'#' . $this->id . '\').is(\':checked\') ? "1" : "0" ) + \'"\' ) + ';
		}

		return '($(\'#' . $this->id . '\').is(\':checked\') != ' . ( $this->default != '0' ? 'true' : 'false' ) . ' ? \' ' . $this->id . '="\' + ( $(\'#' . $this->id . '\').is(\':checked\') ? "1" : "0" ) + \'"\' : "" ) + ';
	}

	public function map_visual_composer() {
		if ( $this->default == '0' ) {
			$value = array( __( "No", "intense" ) => false );
		} else {
			$value = array( __( "Yes", "intense" ) => true );
		}

		return array(
			"type" => "checkbox",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			//"value" => $value,
			"value" => array( __( "Yes, please", "intense" ) => true ),
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : null ),
			"admin_label" => $this->composer_show_value,
		);
	}
}
