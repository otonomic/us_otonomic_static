<?php

class Intense_Field_Border_Radius extends Intense_Field {
	protected function render() {
		$output = '<input class="dialog border-radius" type="text" name="' . $this->id . '" id="' . $this->id . '" value="' . $this->value . '"' . $this->style . '/>';
		$output .= '<span class="intense help-block">' . __( '%, em, px - examples: 25% or 10em or 20px', 'intense' ) . '</span>';

		return $output;
	}

	public function render_init_script() {
		return '$(".dialog.border-radius").typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});';
	}

	public function map_visual_composer() {
		return array(
			"type" => "textfield",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => __( '%, em, px - examples: 25% or 10em or 20px', 'intense' ),
			"admin_label" => $this->composer_show_value,
		);
	}
}