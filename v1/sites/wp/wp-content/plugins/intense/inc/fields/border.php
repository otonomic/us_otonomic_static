<?php

class Intense_Field_Border extends Intense_Field {
	protected function render() {
		//parse border parts from $value and set the size, style, and color
		$parts = explode( ' ', $this->value );

		if ( empty( $this->value ) || count( $parts ) < 3 ) {
			$parts = array('', '', '');
		}

		$border_styles = array(
			'' => '',
			'dotted' => __( 'Dotted', 'intense' ),
			'dashed' => __( 'Dashed', 'intense' ),
			'solid' => __( 'Solid', 'intense' ),
			'double' => __( 'Double', 'intense' ),
			'groove' => __( 'Groove', 'intense' ),
			'ridge' => __( 'Ridge', 'intense' ),
			'inset' => __( 'Inset', 'intense' ),
			'outset' => __( 'Outset', 'intense' ),
		);

		$output = '<input class="dialog bordersize" type="text" style="width:80px;" name="bordersize_' . $this->id . '" id="bordersize_' . $this->id . '" value="' . str_replace( 'px', '', $parts[0] ) . '" />px';
		$output .= '<select class="dialog borderstyle" name="borderstyle_' . $this->id . '" id="borderstyle_' . $this->id . '">';

		foreach ($border_styles as $key => $border_style) {
			$output .= '<option value="' . $key . '"' . ( $parts[1] == $key ? ' selected' : '' ) . '>' . $border_style . '</option>';
		}

		$output .= '</select>';
		$output .= '<div style="display: inline-block; line-height: 35px;">';
		$output .= '	<input name="bordercolor_' . $this->id . '" id="bordercolor_' . $this->id . '" class="of-color bordercolor" type="text" value="' . $parts[2] . '" data-default-color="' . $parts[2] . '" />';
		$output .= '</div>';
		$output .= $this->description;

		return $output;
	}

	public function render_init_script() {
		return "
			$('.dialog.bordersize').typeWatch({
				callback: intenseRefreshPreview,
				wait: 500,
				highlight: false,
				captureLength: 0
			});
			$('.dialog.borderstyle').unbind('change').change(intenseRefreshPreview);
			$('.of-color.bordercolor.minicolors-input').minicolors('destroy');
			$('.of-color.bordercolor').minicolors({
                    changeDelay: 200,
                    animationEasing: 'swing',
                    //opacity: true,
                    change: function() {
                        intenseRefreshPreview();
                    }
                });";
	}

	public function render_value_script() {
		return '($(\'#bordersize_' . $this->id . '\').val().length > 0 &&  $(\'#borderstyle_' . $this->id . '\').val().length > 0 && $(\'#bordercolor_' . $this->id . '\').val().length > 0  ? \' ' . $this->id . '="\' + $(\'#bordersize_' . $this->id . '\').val() + "px " + $(\'#borderstyle_' . $this->id . '\').val() + " " + $(\'#bordercolor_' . $this->id . '\').val() + \'"\' : "" ) + ';
	}

	public function map_visual_composer() {
		return array(
			"type" => "textfield",
			//"holder" => "button",
			"class" => "",
			"heading" => $this->title,
			"param_name" => $this->id,
			"value" => !empty( $this->default ) ? $this->default : '',
			'description' => ( isset( $this->args['description'] ) ? $this->args['description'] : __('CSS border shorthand property (ex. 1px solid red)', 'intense') ),
			"admin_label" => $this->composer_show_value,
		);
	}
}