<?php

class Intense_Field_Category extends Intense_Field_Dropdown {
	private function set_default_options() {
		if ( empty( $this->args['options'] ) ) {
			$this->args['options'] = array();

			$categories = get_terms( $this->args['category'] );

			foreach ( $categories as $post_cat ) {
				$this->args['options'][ $post_cat->slug ] = $post_cat->name;
			}
		}
	}

	protected function render() {
		$this->set_default_options();

		$this->args['multiple'] = true;

		return parent::render();
	}

	public function render_value_script() {
		$this->args['multiple'] = true;

		return parent::render_value_script();
	}

	public function map_visual_composer() {
		// $this->set_default_options();
		
		// return parent::map_visual_composer();
		return null;
	}
}