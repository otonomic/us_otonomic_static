<?php
class Intense_Photo {
	public $source, $link, $title, $description, $sizes;

	function Intense_Photo( $source, $link, $title, $description ) {
		$this->source = $source;
		$this->link = $link;
		$this->title = $title;
		$this->description = $description;
		$this->sizes = array();
	}

	function compare_sizes( $a, $b ) {
		$a_area = $a['width'] * $a['height'];
		$b_area = $b['width'] * $b['height'];

		if ($a_area == $b_area) 
			return 0;

		return ( $a_area > $b_area ) ? +1 : -1;
	}

	function add_size( $name, $width, $height, $sourceurl ) {
		//$this->sizes[ $width . "X" . $height ] = $sourceurl;
		$this->sizes[ $name ]['width'] = $width;
		$this->sizes[ $name ]['height'] = $height;
		$this->sizes[ $name ]['sourceurl'] = $sourceurl;

		usort( $this->sizes, array($this, 'compare_sizes'));
	}

	/**
	 * Gets the photo that is closest in size.  
	 * It will try to find one just bigger if 
	 * it can't find an exact match.  If there 
	 * isn't one bigger, it will use the largest
	 * possible.
	 *
	 * Assumes sizes are in order of smallest to largest
	 * this is safe to assume because they are sorted
	 * as they are added
	 */
	function get_closest_size( $width, $height ) {
		$photo_area = $width * $height;
		$closest_size = null;
		$found = false;

		usort( $this->sizes, array($this, 'compare_sizes') );		

		foreach ( $this->sizes as $size ) {			
			$size_area = $size["width"] * $size["height"];

			if ( $size_area < $photo_area ) {
				$closest_size = $size;
			} else if ( $size_area == $photo_area ) {
				$closest_size = $size;
				$found = true;
			} else if ( $size_area > $photo_area ) {
				$closest_size = $size;
				$found = true;
			}

			// early termination
			if ( $found ) {
				break;
			}
		}

		return $closest_size;
	}

	function get_size( $name ) {
		return $this->sizes[ $name ];
	}
}
