<?php
require_once 'Intense-photo.php';
require_once  INTENSE_PLUGIN_FOLDER . '/inc/libraries/phpSmug/phpSmug.php';

// https://smugmug.atlassian.net/wiki/display/API/Home

class Intense_SmugMug {
	public $api_key, $photos;

	// TODO: get dimensions from smugmug.  These are still zenfolio dimensions
	private $native_sizes = array(
		"Tiny"      => array( 'width' => 60, 'height' => 60 ),
		"Thumb"     => array( 'width' => 120, 'height' => 120 ),
		"Small"     => array( 'width' => 200, 'height' => 200 ),
		"Medium"    => array( 'width' => 400, 'height' => 400 ),
		"Large"     => array( 'width' => 580, 'height' => 450 ),
		"XLarge"    => array( 'width' => 800, 'height' => 630 ),
		"X2Large"   => array( 'width' => 1100, 'height' => 960 ),
		"X3Large"   => array( 'width' => 1550, 'height' => 960 ),
		"Original"  => array( 'width' => 1550, 'height' => 960 ),
	);

	function Intense_SmugMug(  ) {
		global $intense_visions_options;
		$this->api_key = ( isset( $intense_visions_options['intense_smug_api_key'] ) ? $intense_visions_options['intense_smug_api_key'] : null );
		$this->photos = array();
	}

	function get_photos( $userid, $album, $page, $pagesize ) {
		$f = new phpSmug( array( "APIKey" => $this->api_key ) );
		$f->login();

		// Get list of public albums
		$albums = $f->albums_get( 'NickName=' . $userid );
		$images = array();

		// Get list of public images and other useful information
		foreach ( $albums as $key => $item ) {
			if ( ! empty( $album ) ) {
				if ( $item['Title'] == $album ) {
					$images = $f->images_get( "AlbumID=" . $item['id'], "AlbumKey=" . $item['Key'], "Heavy=1" );
					$images = ( $f->APIVer == "1.2.2" ) ? $images['Images'] : $images;
				}
			} else {
				if ( count( $images ) < $pagesize ) {
					$raw_images = $f->images_get( "AlbumID=" . $item['id'], "AlbumKey=" . $item['Key'], "Heavy=1" );
					$images = array_merge( $images, ( $f->APIVer == "1.2.2" ) ? $raw_images['Images'] : $raw_images );
				}
			}
		}

		$i = 0;
		foreach ( $images as $item ) {
			if ( $i <= $pagesize ) {
				$photo = new Intense_Photo( "smugmug", $item['URL'], $item['FileName'], $item['Caption'] );

				foreach ( $this->native_sizes as $key => $size ) {
					if ( isset( $item[ $key . "URL" ] ) ) {
						$photo->add_size(
							$key,
							$size["width"],
							$size["height"],
							$item[ $key . "URL" ] );
					}
				}

				$this->photos[] = $photo;
			} else {
				break;
			}

			$i++;
		}

		return $this->photos;
	}
}
