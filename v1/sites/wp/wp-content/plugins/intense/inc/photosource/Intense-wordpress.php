<?php
require_once 'Intense-photo.php';

class Intense_WordPress {
	public $photos;

	function Intense_WordPress(  ) {
		$this->photos = array();
	}

	function get_photos( $attachments, $sizes ) {
		foreach ( $attachments as $id => $attachment ) {
			$photo = new Intense_Photo( "wordpress", get_attachment_link( $id ), $attachment->post_title, $attachment->post_excerpt );

			foreach ( $sizes as $key => $value ) {
				$url = wp_get_attachment_image_src( $id, $key, false );
				
				$photo->add_size(
					$key,
					$value["width"],
					$value["height"],
					$url[0]
				);
			}

			$this->photos[] = $photo;
		}

		return $this->photos;
	}
}
