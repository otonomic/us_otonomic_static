<?php
require_once 'Intense-photo.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/phpFlickr/phpFlickr.php';

class Intense_Flickr {
	public $api_key, $secret_key, $photos;

	public $native_sizes = array(
		"_sq" => array( 'width' => 75, 'height' => 75 ),
		"_q" => array( 'width' => 150, 'height' => 150 ),
		"_t" => array( 'width' => 100, 'height' => 67 ),
		"_s" => array( 'width' => 240, 'height' => 160 ),
		"_n" => array( 'width' => 320, 'height' => 213 ),
		"_m" => array( 'width' => 500, 'height' => 333 ),
		"_z" => array( 'width' => 640, 'height' => 427 ),
		"_c" => array( 'width' => 800, 'height' => 534 ),
		"_l" => array( 'width' => 1024, 'height' => 683 ),
		"_h" => array( 'width' => 1600, 'height' => 1067 ),
		"_k" => array( 'width' => 2048, 'height' => 1366 ),
		"_o" => array( 'width' => 3200, 'height' => 2174 ) //will vary depending on image
	);

	function Intense_Flickr(  ) {
		global $intense_visions_options;
		$this->api_key = ( isset( $intense_visions_options['intense_flickr_consumer_key'] ) ? $intense_visions_options['intense_flickr_consumer_key'] : null );
		$this->secret_key = ( isset( $intense_visions_options['intense_flickr_consumer_secret'] ) ? $intense_visions_options['intense_flickr_consumer_secret'] : null );
		$this->photos = array();
	}

	function get_photos( $userid, $setid, $groupid, $page, $pagesize ) {
		$f = new phpFlickr( $this->api_key, $this->secret_key );

		$extras = "";

		foreach ( $this->native_sizes as $key => $value ) {
			$extras .= "url" . $key . ",";
		}

		$extras .= "description";

		$person = $f->people_findByUsername( $userid );

		if ( ! empty( $groupid ) ) {
			$photos_url = $f->urls_getGroup( $groupid );
			$flickrphotos = $f->groups_pools_getPhotos( $groupid, NULL, NULL, NULL, $extras, $pagesize, $page );
			$foundphotos = (array)$flickrphotos['photos']['photo'];
		} else if ( ! empty( $setid ) ) {
			$photos_url = $f->urls_getUserPhotos( $person['id'] );			
			$flickrphotos = $f->photosets_getPhotos( $setid, $extras, NULL, $pagesize, $page );
			$foundphotos = (array)$flickrphotos['photoset']['photo'];
		} else if ( ! empty( $userid ) ) {
			$photos_url = $f->urls_getUserPhotos( $person['id'] );
			$flickrphotos = $f->people_getPublicPhotos( $person['id'], NULL, $extras, $pagesize, $page );
			$foundphotos = (array)$flickrphotos['photos']['photo'];
		}		

		foreach ( $foundphotos as $item ) {
			$photo = new Intense_Photo( "flickr", $photos_url . $item['id'], $item['title'], $item['description'] );

			foreach ( $this->native_sizes as $key => $value ) {
				if ( !empty( $item[ 'url' . $key ] ) ) {
					$photo->add_size(
						$key,
						$item[ 'width' . $key ],
						$item[ 'height' . $key ],
						$item[ 'url' . $key ] );
				}
			}

			$this->photos[] = $photo;
		}		

		return $this->photos;
	}

}
