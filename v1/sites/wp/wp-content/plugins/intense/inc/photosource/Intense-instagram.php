<?php
require_once 'Intense-photo.php';

class Intense_Instagram {
	public $access_token, $endpoints, $photos;

	public $native_sizes = array(
		"thumbnail"           => array( 'width' => 150, 'height' => 150 ),
		"low_resolution"      => array( 'width' => 306, 'height' => 306 ),
		"standard_resolution" => array( 'width' => 612, 'height' => 612 )
	);

	function Intense_Instagram(  ) {
		global $intense_visions_options;
		$this->access_token = ( isset( $intense_visions_options['intense_instagram_access_token'] ) ? $intense_visions_options['intense_instagram_access_token'] : null );

		$this->endpoints = array(
			'self'    => 'https://api.instagram.com/v1/users/self/media/recent?count=%1$s&access_token=%2$s',
			'user'    => 'https://api.instagram.com/v1/users/%1$s/media/recent?count=%2$s&access_token=%3$s',
			'hashtag' => 'https://api.instagram.com/v1/tags/%1$s/media/recent?count=%2$s&access_token=%3$s',
			'usersearch' => 'https://api.instagram.com/v1/users/search?q=%1$s&access_token=%2$s'
		);
		$this->photos = array();
	}

	function get_photos( $userid, $hashtag, $page, $pagesize ) {
		$api_url = null;

		if ( $userid ) {
			if ( !is_numeric( $userid ) ) { //look up the userid by username
				$userid = $this->get_userid( $userid );
			}

			$api_url  = sprintf( $this->endpoints['user'], $userid, $pagesize, $this->access_token );
		} else if ( $hashtag ) {
				$api_url  = sprintf( $this->endpoints['hashtag'], $hashtag, $pagesize, $this->access_token );
			} else {
			$api_url  = sprintf( $this->endpoints['self'], $pagesize, $this->access_token );
		}

		$response = wp_remote_get( $api_url,
			array(
				'sslverify' => apply_filters( 'https_local_ssl_verify', false )
			)
		);

		if ( !is_wp_error( $response ) && $response['response']['code'] < 400 && $response['response']['code'] >= 200 ) {
			$data = json_decode( $response['body'] );

			if ( $data->meta->code == 200 ) {
				foreach ( $data->data as $item ) {
					if ( isset( $instance['hashtag'], $item->caption->text ) ) {
						$image_title = $item->user->username . ': &quot;' . filter_var( $item->caption->text, FILTER_SANITIZE_STRING ) . '&quot;';
					} else if ( isset( $instance['hashtag'] ) && !isset( $item->caption->text ) ) {
							$image_title = "instagram by " . $item->user->username;
						} else if ( isset( $item->caption->text ) ) {
							$image_title = filter_var( $item->caption->text, FILTER_SANITIZE_STRING );
						} else {
						$image_title = '';
					}

					if ( isset( $item->caption->text ) ) {
						$caption = $item->caption->text;
					} else {
						$caption = '';
					}

					$photo = new Intense_Photo( "instagram", $item->link, $image_title, $caption );
					$photo->add_size(
						"thumbnail",
						$this->native_sizes["thumbnail"]["width"],
						$this->native_sizes["thumbnail"]["height"],
						$item->images->thumbnail->url );
					$photo->add_size(
						"low_resolution",
						$this->native_sizes["low_resolution"]["width"],
						$this->native_sizes["low_resolution"]["height"],
						$item->images->low_resolution->url );
					$photo->add_size(
						"standard_resolution",
						$this->native_sizes["standard_resolution"]["width"],
						$this->native_sizes["standard_resolution"]["height"],
						$item->images->standard_resolution->url );

					$this->photos[] = $photo;
				}
			}
		}

		return $this->photos;
	}

	function get_userid( $username ) {
		$response = wp_remote_get(
			sprintf( $this->endpoints['usersearch'], $username, $this->access_token ),
			array(
				'sslverify' => apply_filters( 'https_local_ssl_verify', false )
			)
		);

		if ( !is_wp_error( $response ) && $response['response']['code'] < 400 && $response['response']['code'] >= 200 ) {
			$data = json_decode( $response['body'] );

			if ( $data->meta->code == 200 ) {
				foreach ( $data->data as $item ) {
					if ( $item->username == $username ) {
						return $item->id;
					}
				}
			}
		}

		return $username; //nothing found, return what you were given
	}
}
