<?php
if (version_compare(PHP_VERSION, '5.4.0', '<')) {
  //echo '<pre>'; print_r("The Facebook SDK requires PHP 5.4 or higher. We are sorry but you will not be able to show Facebook photos until this requirement is met."); echo '</pre>';
  throw new Exception('The Facebook SDK v4 requires PHP version 5.4 or higher.');
}


require_once 'Intense-photo.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/HttpClients/FacebookHttpable.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/HttpClients/FacebookCurl.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/HttpClients/FacebookCurlHttpClient.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/Entities/AccessToken.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/Entities/SignedRequest.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookSession.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookRedirectLoginHelper.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookRequest.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookResponse.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookSDKException.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookRequestException.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookOtherException.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/FacebookAuthorizationException.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/GraphObject.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/facebook-php-sdk/src/Facebook/GraphSessionInfo.php';

use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;

use Facebook\Entities\AccessToken;
use Facebook\Entities\SignedRequest;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookSignedRequestFromInputHelper; // added in v4.0.9
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;

// these two classes required for canvas and tab apps
use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookPageTabHelper;

// TODO: the autentication token expires after 60 days.  If this becomes a problem,
// request a new one sometime

class Intense_Facebook {
	public $access_token, $app_id, $app_secret, $queries, $photos;

	function Intense_Facebook() {
		global $intense_visions_options;
		$this->access_token = ( isset( $intense_visions_options['intense_facebook_access_token'] ) ? $intense_visions_options['intense_facebook_access_token'] : null );
		$this->app_id = ( isset( $intense_visions_options['intense_facebook_client_id'] ) ? $intense_visions_options['intense_facebook_client_id'] : null );
		$this->app_secret = ( isset( $intense_visions_options['intense_facebook_client_secret'] ) ? $intense_visions_options['intense_facebook_client_secret'] : null );

		$this->queries = array(
			'user'    => '/me/photos/uploaded?limit=%1$s&offset=%2$s',
			'album' => '/me/albums/?limit=500',
			'album/photos' => '/%1$s/photos/?limit=%2$s&offset=%3$s',
			'page' => '/%1$s/photos/uploaded?limit=%2$s&offset=%3$s',
			'pagealbum' => '/%1$s/albums/',
			'group' => '/%1$s/photos/uploaded?limit=%2$s&offset=%3$s'
		);
		$this->photos = array();
	}

	function get_photos( $userid, $page, $group, $album, $pagenumber, $pagesize ) {	
		FacebookSession::setDefaultApplication( $this->app_id, $this->app_secret );
		$session = new FacebookSession( $this->access_token );

		if ( empty( $page ) && !empty( $album ) ) {
			$albumid = 0;
			$query  = sprintf( $this->queries['album'], $pagesize, $pagenumber * $pagesize );
			
			$facebook_albums = ( new FacebookRequest(
				$session, 'GET', $query
			) )->execute()->getGraphObject();

			if ( isset( $facebook_albums ) && !empty( $facebook_albums->getProperty( 'data' ) ) ) {

				$facebook_albums = $facebook_albums->getProperty( 'data' )->asArray();

				foreach ($facebook_albums as $key => $facebook_album) {
					if ( $facebook_album->name == $album ) {
						$albumid = $facebook_album->id;
					}				
				}

				$query  = sprintf( $this->queries['album/photos'], $albumid, $pagesize, $pagenumber * $pagesize );
			}
		} else if ( !empty( $page ) && !empty( $album ) ) {
			$albumid = 0;
			$query  = sprintf( $this->queries['pagealbum'], $page, $pagesize, $pagenumber * $pagesize );
			
			$facebook_albums = ( new FacebookRequest(
				$session, 'GET', $query
			) )->execute()->getGraphObject();

			if ( isset( $facebook_albums ) && !empty( $facebook_albums->getProperty( 'data' ) ) ) {
				$facebook_albums = $facebook_albums->getProperty( 'data' )->asArray();

				foreach ($facebook_albums as $key => $facebook_album) {
					if ( $facebook_album->name == $album ) {
						$albumid = $facebook_album->id;
					}				
				}

				$query  = sprintf( $this->queries['album/photos'], $albumid, $pagesize, $pagenumber * $pagesize );
			}
		} else if ( !empty( $page ) ) {
			$query  = sprintf( $this->queries['page'], $page, $pagesize, $pagenumber * $pagesize );
		} else if ( !empty( $group ) ) {
			$query  = sprintf( $this->queries['group'], $group, $pagesize, $pagenumber * $pagesize );
		} else {
			$query  = sprintf( $this->queries['user'], $pagesize, $pagenumber * $pagesize );
		}

		$facebook_photos = ( new FacebookRequest(
				$session, 'GET', $query
			) )->execute()->getGraphObject();

		if ( isset( $facebook_photos ) && !empty( $facebook_photos->getProperty( 'data' ) ) ) {
			$facebook_photos = $facebook_photos->getProperty( 'data' )->asArray();

			foreach ( $facebook_photos as $key => $facebook_photo ) {
				$photo = new Intense_Photo( "facebook", $facebook_photo->link, '', !empty( $facebook_photo->name ) ? $facebook_photo->name : '' );

				foreach ( $facebook_photo->images as $image ) {
					$width = $image->width;
					$height = $image->height;

					$photo->add_size(
						"$width X $height",
						$width,
						$height,
						$image->source );
				}

				// add fake size to hold the largest possible
				// facebook photo
				// $photo->add_size(
				//  "src_big",
				//  2400,
				//  2400,
				//  $facebook_photo->source );

				$this->photos[] = $photo;
			}
		}

		return $this->photos;
	}
}
