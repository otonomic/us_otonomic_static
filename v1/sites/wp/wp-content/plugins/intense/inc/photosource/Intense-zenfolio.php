<?php
require_once 'Intense-photo.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/libraries/phpZenfolio/phpZenfolio.php';

//http://www.zenfolio.com/zf/tools/api.aspx

class Intense_Zenfolio {
	public $app_name, $photos;

	private $native_sizes = array(
		"1"  => array( 'width' => 60, 'height' => 60 ),
		"10" => array( 'width' => 120, 'height' => 120 ),
		"11" => array( 'width' => 200, 'height' => 200 ),
		"2"  => array( 'width' => 400, 'height' => 400 ),
		"3"  => array( 'width' => 580, 'height' => 450 ),
		"4"  => array( 'width' => 800, 'height' => 630 ),
		"5"  => array( 'width' => 1100, 'height' => 960 ),
		"6"  => array( 'width' => 1550, 'height' => 960 )
	);

	function Intense_Zenfolio(  ) {
		global $intense_visions_options;
		$this->app_name = ( isset( $intense_visions_options['intense_zenfolio_app_name'] ) ? $intense_visions_options['intense_zenfolio_app_name'] : null );
		$this->photos = array();
	}

	function get_photos( $setid, $page, $pagesize ) {
		$f = new phpZenfolio( 'AppName=' . $this->app_name );
		$pictures = $f->LoadPhotoSetPhotos( str_replace( "p", "", $setid ), $page, $pagesize );

		foreach ( $pictures as $key => $item ) {
			$photo = new Intense_Photo( "zenfolio", $item['PageUrl'], $item['Title'], ( isset( $item['Caption'] ) ? $item['Caption'] . "<br />" : "" ) . $item['Copyright'] );

			foreach ( $this->native_sizes as $key => $size ) {
				$photo->add_size(
					$key,
					$size["width"],
					$size["height"],
					phpZenfolio::imageUrl( $item, $key ) );
			}

			$this->photos[] = $photo;
		}

		return $this->photos;
	}
}
