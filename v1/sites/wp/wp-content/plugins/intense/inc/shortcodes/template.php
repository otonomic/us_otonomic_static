<?php

class Intense_Template extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Template', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-editor-paste-text';
		$this->show_preview = true;

		$this->fields = array(
			'template' => array(
				'type' => 'template',
				'title' => __( 'Template', 'intense' ),
				'description' => __( 'the template files should be located in the intense_templates/template/ folder in your theme or child theme.', 'intense' ),
				'path' => '/template/',
				'default' => 'blank'
			),		
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$template_file = intense_locate_plugin_template( '/template/' . $template . '.php' );

		if ( !isset( $template_file ) ) {
			$template_file = intense_locate_plugin_template( '/template/blank.php' );
		}

		return intense_load_plugin_template( $template_file );
	}
}
