<?php

class Intense_Tabs extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Tabs', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-newtab';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
				'description' => __( 'must be unique for the post/page', 'intense' ),
                'skinnable' => '0'
			),
			'direction' => array(
				'type' => 'dropdown',
				'title' => __( 'Direction', 'intense' ),
				'options' => array(
					'' => __( 'Top', 'intense' ),
					'below' => __( 'Bottom', 'intense' ),
					'left' => __( 'Left', 'intense' ),
					'right' => __( 'Right', 'intense' )
				),
			),
			'animation' => array(
				'type' => 'dropdown',
				'title' => __( 'Animation', 'intense' ),
				'default' => 'fade',
				'options' => array(
					'none' => __( 'None', 'intense' ),
					'cycle-down' => __( 'Cycle Down', 'intense' ),
					'cycle-left' => __( 'Cycle Left', 'intense' ),
					'cycle-right' => __( 'Cycle Right', 'intense' ),
					'cycle-up' => __( 'Cycle Up', 'intense' ),
					'fade' => __( 'Fade', 'intense' ),
					'slide-flip' => __( 'Flip', 'intense' ),
					'scale' => __( 'Scale', 'intense' ),
					'scale-up' => __( 'Scale Up', 'intense' ),
					'slide-down' => __( 'Slide Down', 'intense' ),
					'slide-left' => __( 'Slide Left', 'intense' ),
					'slide-right' => __( 'Slide Right', 'intense' ),
					'slide-up' => __( 'Slide Up', 'intense' ),
				),
			),
			'active_tab_background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Active Tab Background Color', 'intense' ),
			),
			'active_tab_font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Active Tab Font Color', 'intense' ),
			),
			'tab' => array(
				'type' => 'repeater',
				'title' => __( 'Tabs', 'intense' ),
				'preview_content' => __( 'Tab Content', 'intense' ),
				'fields' => array(
					'id' => array( 'type' => 'deprecated' ),
					'general_tab' => array(
						'type' => 'tab',
						'title' => __( 'General', 'intense' ),
						'fields' =>  array(
							'title' => array(
								'type' => 'text',
								'title' => __( 'Title', 'intense' ),
								'default' => 'Tab',
								'skinnable' => '0'
							),
							'active' => array(
								'type' => 'checkbox',
								'title' => __( 'Active', 'intense' ),
								'default' => "0",
							),
							'border_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Border Color', 'intense' ),
								'default' => 'boxed'
							),
						)
					),
					'tab_tab' => array(
						'type' => 'tab',
						'title' => __( 'Tab', 'intense' ),
						'fields' =>  array(
							'link' => array(
								'type' => 'text',
								'title' => __( 'Link', 'intense' ),
								'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
								'default' => ''
							),
							'link_target' => array(
								'type' => 'link_target',
								'title' => __( 'Link Target', 'intense' ),
								'default' => '_self',
							),
							'background_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Tab Background Color', 'intense' ),
							),
							'font_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Tab Font Color', 'intense' ),
							),
						)
					),
					'content_tab' => array(
						'type' => 'tab',
						'title' => __( 'Content', 'intense' ),
						'fields' =>  array(
							'content_background_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Content Background Color', 'intense' ),
							),
							'content_font_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Content Font Color', 'intense' ),
							),
						)
					),
					'icon_tab' => array(
						'type' => 'tab',
						'title' => __( 'Icon', 'intense' ),
						'fields' =>  array(
							'icon_type' => array(
								'type' => 'icon',
								'title' => __( 'Icon', 'intense' ),
							),
							'icon_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Color', 'intense' ),
							),
							'icon_stack_type' => array(
								'type' => 'icon',
								'title' => __( 'Stack Icon', 'intense' ),
							),
							'icon_stack_color' => array(
								'type' => 'color_advanced',
								'title' => __( 'Stack Icon Color', 'intense' ),
							),
							'icon_size' => array(
								'type' => 'dropdown',
								'title' => __( 'Icon Size', 'intense' ),
								'default' => '1',
								'options' => array(
									'1' => '1',
									'2' => '2',
									'3' => '3',
									'4' => '4',
									'5' => '5'
								)
							),
							'custom_icon' => array(
								'type' => 'image',
								'title' => __( 'Custom Icon', 'intense' ),
							),
							'icon_position' => array(
								'type' => 'dropdown',
								'title' => __( 'Position', 'intense' ),
								'default' => 'left',
								'options' => array(
									'left' => __( 'Left', 'intense' ),
									'right' => __( 'Right', 'intense' ),
								)
							),
							'icon_spin' => array(
								'type' => 'checkbox',
								'title' => __( 'Spin', 'intense' ),
								'default' => "0",
							),
							'icon_rotate' => array(
								'type' => 'dropdown',
								'title' => __( 'Rotate', 'intense' ),
								'default' => '',
								'options' => array(
									'' => '',
									'90' => __( '90 degrees', 'intense' ),
									'180' => __( '180 degrees', 'intense' ),
									'270' => __( '270 degrees', 'intense' )
								)
							),
							'icon_flip' => array(
								'type' => 'dropdown',
								'title' => __( 'Flip', 'intense' ),
								'default' => '',
								'options' => array(
									'' => '',
									'horizontal' => __( 'Horizontal', 'intense' ),
									'vertical' => __( 'Vertical', 'intense' )
								)
							),
						)
					)
				)
			),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = "";

		if ( isset( $direction ) ) {
			$direction = "tabs-" . $direction;
		}

		$GLOBALS['IntensePluginInlineStyle']['Intense Tab'] = "
	        	.intense.nav-tabs>.active>a, .intense.nav-tabs>.active>a:hover, .intense.nav-tabs>.active>a:focus {
	        		background-color: " . intense_get_plugin_color( "boxed" ) . ";
	        	}

	        	.intense.nav-tabs>li>a {
		            background-color: " . intense_adjust_color_brightness( intense_get_plugin_color( "boxed" ), 15 ) . ";
		        }

		        .intense.nav-tabs>.active>a, .intense.nav-tabs>.active>a:hover, .intense.nav-tabs>.active>a:focus{
		        	color: " . intense_get_plugin_color( "primary" ) . ";
		        }
	        ";

		intense_add_script( 'effeckt_tabs' );

		//old technique using intense_tab_list, intense_tab, intense_tab_contents, and intense_tab_pane shortcodes
		if ( stripos( $content, 'intense_tab_list' ) !== false ) {			
			$content = str_replace( 'intense_tab ', 'intense_tab_original ', $content );
			$content = str_replace( 'intense_tab]', 'intense_tab_original]', $content );
			$content = str_replace( 'intense_tab_contents', 'intense_tab_original_contents', $content );
			$content = str_replace( 'intense_tab_pane', 'intense_tab_original_pane', $content );
			$content = str_replace( 'intense_tab_list', 'intense_tab_original_list', $content );

			$output .= '<div class="intense tabbable preload effeckt-tabs-wrap ' . $direction . '" data-effeckt-type="' . $animation . '">';
			$output .= do_shortcode( $content );
			$output .= '</div>';
		} else {
			global $intense_tab_array;

			$intense_tab_array = array();

			//setup id for ul so colors can be set for active tab
			$tab_id = 'tab_' . rand();

			//gather array of tabs
			do_shortcode( $content );

			$tab_list = '<style>';

			if ( !empty( $active_tab_background_color ) ) {
				$tab_list .= '#' . $tab_id . '.intense.nav-tabs>li.active a { background-color: ' . $active_tab_background_color . ' !important; }';
			}

			if ( !empty( $active_tab_font_color ) ) {
				$tab_list .= '#' . $tab_id . '.intense.nav-tabs>li.active h4, #' . $tab_id . '.intense.nav-tabs>li.active i { color: ' . $active_tab_font_color . ' !important; }';
			}
			
			$tab_list .= '</style>';
			
			$tab_list .= '<ul id="' . $tab_id . '" class="intense nav nav-tabs effeckt-tabs ' . ( $rtl ? 'rtl ' : '' ) . '" >';

		 	foreach ( $intense_tab_array as $key => $value ) {
				$tab_list .= $this->render_tab( $id . '_' . $key, $value );
		 	}

	 		$tab_list .= '</ul>';
			

		 	$output .= '<div class="intense tabbable preload effeckt-tabs-wrap ' . $direction . '" data-effeckt-type="' . $animation . '">';

		 	if ( $direction != 'tabs-below' ) {
		 		$output .= $tab_list;
		 	}

	 		$output .= '<div class="intense tab-content effeckt-tabs-container" style="background-color: transparent;">';
			
			foreach ( $intense_tab_array as $key => $value ) {
				$output .= $this->render_tab_pane( $id . '_' . $key, $value );
		 	}

			$output .= '</div>';

			if ( $direction == 'tabs-below' ) {
		 		$output .= $tab_list;
		 	}

			$output .= '</div>';
		}

		return $output;
	}

	function render_tab( $id, $tab ) {
		extract( shortcode_atts( array(				
				"title" => "Tab",
				"active" => 0,
				"link" => null,
				'link_target' => '',
				"background_color" => null,
				"font_color" => null,
				"border_color" => "boxed",
				"icon_position" => "left",
				"icon_type"  => null,
				"icon_size"  => '1',
				"icon_spin"  => 0,
				"icon_color" => '',
				"icon_rotate" => null,
				"icon_flip" =>  null,
				"icon_stack_type" => null,
				"icon_stack_color" => null,
				'custom_icon' => null,
				'content_background_color' => 'boxed',
				'content_font_color' => null,
				'content' => null
			), $tab ) );

		$icon = "";
		$data_toggle = "";
		$link_class = '';

		if ( isset( $icon_type ) ) {
			$icon = intense_run_shortcode( 'intense_icon', array(
				'type' => $icon_type,
				'color' => $icon_color,
				'size' => $icon_size,
				'spin' => $icon_spin,
				'rotate' => $icon_rotate,
				'flip' => $icon_flip,
				'stack_type' => $icon_stack_type,
				'stack_color' => $icon_stack_color
			));			
		}

		if ( isset( $custom_icon ) ) {
			if ( is_numeric( $custom_icon ) ) {
		      $photo_info = wp_get_attachment_image_src( $custom_icon, null );
		      $custom_icon = $photo_info[0];
		    }
		    
			$icon = '<img class="intense img-icon' . ( $icon_size - 1 ) . 'x" src="' . $custom_icon . '" alt="" />';
		}

		if ( !isset( $link ) ) {
			$link = "#" . $id;
			//$data_toggle = 'data-toggle="tab"';
			$link_class = "effeckt-tab";
		}

		if ( is_numeric( $link ) ) {
			$link = get_permalink( $link );
		}

		$border_color = intense_get_plugin_color( $border_color );

		if ( !empty( $border_color ) ) {
			$border_color = " border-color: " . $border_color . ";";
		}

		$background_color = intense_get_plugin_color( $background_color );

		if ( isset( $background_color ) ) {
			$background_color = " background-color: " . $background_color . ";";
		}

		$font_color = intense_get_plugin_color( $font_color );

		if ( isset( $font_color ) ) {
			$font_color = " color: " . $font_color . ";";
		}

		return '<li class="' . ( $active ? "active" : "" ) . '"><a href="' . $link . '" class="' . $link_class . '" target="' . $link_target . '" ' . $data_toggle . ' style="' . $border_color . $background_color . '"><h4 style="' . $font_color . '">' . ( $icon_position == "left" ? $icon . " " : "" ) . $title . ( $icon_position == "right" ? " " . $icon : "" ) . '</h4></a></li>';
	}

	function render_tab_pane( $id, $tab ) {
		extract( shortcode_atts( array(				
				"title" => "Tab",
				"active" => 0,
				"link" => null,
				'link_target' => '',
				"background_color" => null,
				"font_color" => null,
				"border_color" => "boxed",
				"icon_position" => "left",
				"icon_type"  => null,
				"icon_size"  => '1',
				"icon_spin"  => 0,
				"icon_color" => '',
				"icon_rotate" => null,
				"icon_flip" =>  null,
				"icon_stack_type" => null,
				"icon_stack_color" => null,
				'custom_icon' => null,
				'content_background_color' => 'boxed',
				'content_font_color' => null,
				'content' => null				
			), $tab ) );

		$content_background_color = intense_get_plugin_color( $content_background_color );

		if ( isset( $content_background_color ) ) {
			$content_background_color = " background-color: " . $content_background_color . " !important;";
		}

		$content_font_color = intense_get_plugin_color( $content_font_color );

		if ( isset( $content_font_color ) ) {
			$content_font_color = " color: " . $content_font_color . " !important;";
		}

		return '<div class="intense tab-pane effeckt-tab-content ' . ( $active ? "active effeckt-show" : "" ) . '" id="' . $id . '" style="visibility: hidden;' . $content_font_color . $content_background_color . '">' . do_shortcode( $content ) . "</div>";
	}

	function render_dialog_script() {
		?>
		<script>
			jQuery(function($) {
				$(document).on("click", '[id^="active"]', function() {
					$('[id^="active"]').not(this).attr('checked', false);
				});	

				$('.upload_image_button').live('click', function( event ){
		          event.preventDefault();
		          var target = $(this).data('target-id');

		          window.parent.loadImageFrame($(this), function(attachment) {
		            jQuery("#" + target).val(attachment.id);
		            
		            if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }

		            if ( $('#preview-content').length > 0 ) {
		              $('#preview').click();
		            }
		          });
		        });
			});
		</script>
		<?php
	}
}

class Intense_Tab extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Tab', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-newtab';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_tabs';
		$this->is_container = true;

		$this->fields = array(
			'id' => array( 'type' => 'deprecated' ),
			'general_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
						'default' => 'Tab',
                		'skinnable' => '0'
					),
					'active' => array(
						'type' => 'checkbox',
						'title' => __( 'Active', 'intense' ),
						'default' => "0",
                		'skinnable' => '0'
					),
					'border_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Border Color', 'intense' ),
						'default' => 'boxed'
					),
				)
			),
			'tab_tab' => array(
				'type' => 'tab',
				'title' => __( 'Tab', 'intense' ),
				'fields' =>  array(
					'link' => array(
						'type' => 'text',
						'title' => __( 'Link', 'intense' ),
						'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
						'default' => '',
                		'skinnable' => '0'
					),
					'link_target' => array(
						'type' => 'link_target',
						'title' => __( 'Link Target', 'intense' ),
						'default' => '_self',
					),
					'background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Tab Background Color', 'intense' ),
					),
					'font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Tab Font Color', 'intense' ),
					),
				)
			),
			'content_tab' => array(
				'type' => 'tab',
				'title' => __( 'Content', 'intense' ),
				'fields' =>  array(
					'content_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Content Background Color', 'intense' ),
					),
					'content_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Content Font Color', 'intense' ),
					),
				)
			),
			'icon_tab' => array(
				'type' => 'tab',
				'title' => __( 'Icon', 'intense' ),
				'fields' =>  array(
					'icon_type' => array(
						'type' => 'icon',
						'title' => __( 'Icon', 'intense' ),
					),
					'icon_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
					),
					'icon_stack_type' => array(
						'type' => 'icon',
						'title' => __( 'Stack Icon', 'intense' ),
					),
					'icon_stack_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Stack Icon Color', 'intense' ),
					),
					'icon_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Icon Size', 'intense' ),
						'default' => '1',
						'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5'
						)
					),
					'custom_icon' => array(
						'type' => 'image',
						'title' => __( 'Custom Icon', 'intense' ),
                		'skinnable' => '0'
					),
					'icon_position' => array(
						'type' => 'dropdown',
						'title' => __( 'Position', 'intense' ),
						'default' => 'left',
						'options' => array(
							'left' => __( 'Left', 'intense' ),
							'right' => __( 'Right', 'intense' ),
						)
					),
					'icon_spin' => array(
						'type' => 'checkbox',
						'title' => __( 'Spin', 'intense' ),
						'default' => "0",
					),
					'icon_rotate' => array(
						'type' => 'dropdown',
						'title' => __( 'Rotate', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'90' => __( '90 degrees', 'intense' ),
							'180' => __( '180 degrees', 'intense' ),
							'270' => __( '270 degrees', 'intense' )
						)
					),
					'icon_flip' => array(
						'type' => 'dropdown',
						'title' => __( 'Flip', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'horizontal' => __( 'Horizontal', 'intense' ),
							'vertical' => __( 'Vertical', 'intense' )
						)
					),
				)
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_tab_array;

		$intense_tab_array[] = array_merge( array( 'content' => $content ), is_array( $atts ) ? $atts : array() );

		return '';
	}
}

add_shortcode( 'intense_tab_original_list', 'intense_tab_list_shortcode' );
add_shortcode( 'intense_tab_original', 'intense_tab_original_shortcode' );
add_shortcode( 'intense_tab_original_contents', 'intense_tab_contents_shortcode' );
add_shortcode( 'intense_tab_original_pane', 'intense_tab_pane_shortcode' );

function intense_tab_list_shortcode( $atts, $content = null ) {
	global $intense_visions_options;

	//setup id for ul so colors can be set for active tab
	$id = 'tab_' . rand();

	extract( shortcode_atts( array(
				'rtl' => $intense_visions_options['intense_rtl']
			), $atts ) );

	$output = '<ul class="intense nav nav-tabs effeckt-tabs ' . ( $rtl ? 'rtl ' : '' ) . '" >';
	$output .= do_shortcode( $content );
	$output .= '</ul>';

	return $output;
}

function intense_tab_original_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
				"id" => "tab1",
				"active" => 0,
				"link" => null,
				'link_target' => '',
				"background_color" => null,
				"font_color" => null,
				"border_color" => "boxed",
				"icon_position" => "left",
				"icon_type"  => null,
				"icon_size"  => '1',
				"icon_spin"  => 0,
				"icon_color" => '',
				"icon_rotate" => null,
				"icon_flip" =>  null,
				"icon_stack_type" => null,
				"icon_stack_color" => null,
				'custom_icon' => null
			), $atts ) );

	$icon = "";
	$data_toggle = "";
	$link_class = '';

	if ( isset( $icon_type ) ) {
		$icon = intense_run_shortcode( 'intense_icon', array(
				'type' => $icon_type,
				'color' => $icon_color,
				'size' => $icon_size,
				'spin' => $icon_spin,
				'rotate' => $icon_rotate,
				'flip' => $icon_flip,
				'stack_type' => $icon_stack_type,
				'stack_color' => $icon_stack_color,
				'custom_icon' => $custom_icon
			));
	}

	if ( !isset( $link ) ) {
		$link = "#" . $id;
		//$data_toggle = 'data-toggle="tab"';
		$link_class = "effeckt-tab";
	}

	$border_color = intense_get_plugin_color( $border_color );
	
	if ( !empty( $border_color ) ) {
			$border_color = " border-color: " . $border_color . ";";
	}
	
	$background_color = intense_get_plugin_color( $background_color );

	if ( isset( $background_color ) ) {
		$background_color = " background-color: " . $background_color . " !important;";
	}

	$font_color = intense_get_plugin_color( $font_color );

	if ( isset( $font_color ) ) {
		$font_color = " color: " . $font_color . " !important;";
	}

	return '<li class="' . ( $active ? "active" : "" ) . '"><a href="' . $link . '" class="' . $link_class . '" target="' . $link_target . '" ' . $data_toggle . ' style="' . $border_color . $background_color . '"><h4 style="' . $font_color . '">' . ( $icon_position == "left" ? $icon . " " : "" ) . do_shortcode( $content ) . ( $icon_position == "right" ? " " . $icon : "" ) . '</h4></a></li>';
}

function intense_tab_contents_shortcode( $atts, $content = null ) {

	$output = '<div class="intense tab-content effeckt-tabs-container" style="background-color: transparent;">';
	$output .= do_shortcode( $content );
	$output .= '</div>';

	return $output;
}

function intense_tab_pane_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
				"id" => "tab1",
				"active" => 0,
				"background_color" =>  intense_get_plugin_color( "boxed" ),
				"font_color" => null
			), $atts ) );

	$background_color = intense_get_plugin_color( $background_color );

	if ( isset( $background_color ) ) {
		$background_color = " background-color: " . $background_color . " !important;";
	}

	$font_color = intense_get_plugin_color( $font_color );

	if ( isset( $font_color ) ) {
		$font_color = " color: " . $font_color . " !important;";
	}

	return '<div class="intense tab-pane effeckt-tab-content ' . ( $active ? "active effeckt-show" : "" ) . '" id="' . $id . '" style="visibility: hidden;' . $font_color . $background_color . '">' . do_shortcode( $content ) . "</div>";
}
