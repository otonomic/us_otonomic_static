<?php

class Intense_Chart extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Chart', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-stats';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'type' => array(
            	'type' => 'dropdown',
                'title' => __( 'Type', 'intense' ),
                'default' => 'line',
				'options' => array(
					'line' => __( 'Line', 'intense' ),
					'bar' => __( 'Bar', 'intense' ),
					'radar' => __( 'Radar', 'intense' ),
					'polararea' => __( 'Polar Area', 'intense' ),
					'pie' => __( 'Pie', 'intense' ),
					'doughnut' => __( 'Doughnut', 'intense' )
				),			
            ),
			'width' => array(
				'type' => 'text',
				'title' => __( 'Width', 'intense' ),
				'description' => __( 'measured in pixels', 'intense' ),
				'default' => '400'
			),
			'height' => array(
				'type' => 'text',
				'title' => __( 'Height', 'intense' ),
				'description' => __( 'measured in pixels', 'intense' ),
				'default' => '400'
			),
			'text' => array(
                'type' => 'text',
                'title' => __( 'Text', 'intense' ),
                'skinnable' => '0'
            ),
            'icon' => array(
                'type' => 'icon',
                'title' => __( 'Icon', 'intense' ),
            ),
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Text/Icon Color', 'intense' ),
            ),
            'animation' => array(
                'type' => 'checkbox',
                'title' => __( 'Animated', 'intense' ),
                'default' => "1",
            ),
            'animation_steps' => array(
                'type' => 'text',
                'title' => __( 'Animation Steps', 'intense' ),
                'default' => '60'
            ),
            'animation_easing' => array(
            	'type' => 'dropdown',
                'title' => __( 'Animation Easing', 'intense' ),
                'default' => 'easeOutQuart',
				'options' => array(
					'linear' => 'linear',
					'easeInQuad' => 'easeInQuad',
					'easeOutQuad' => 'easeOutQuad',
					'easeInOutQuad' => 'easeInOutQuad',
					'easeInCubic' => 'easeInCubic',
					'easeOutCubic' => 'easeOutCubic',
					'easeInOutCubic' => 'easeInOutCubic',
					'easeInQuart' => 'easeInQuart',
					'easeOutQuart' => 'easeOutQuart',
					'easeInOutQuart' => 'easeInOutQuart',
					'easeInQuint' => 'easeInQuint',
					'easeOutQuint' => 'easeOutQuint',
					'easeInOutQuint' => 'easeInOutQuint',
					'easeInSine' => 'easeInSine',
					'easeOutSine' => 'easeOutSine',
					'easeInOutSine' => 'easeInOutSine',
					'easeInExpo' => 'easeInExpo',
					'easeOutExpo' => 'easeOutExpo',
					'easeInOutExpo' => 'easeInOutExpo',
					'easeInCirc' => 'easeInCirc',
					'easeOutCirc' => 'easeOutCirc',
					'easeInOutCirc' => 'easeInOutCirc',
					'easeInElastic' => 'easeInElastic',
					'easeOutElastic' => 'easeOutElastic',
					'easeInOutElastic' => 'easeInOutElastic',
					'easeInBack' => 'easeInBack',
					'easeOutBack' => 'easeOutBack',
					'easeInOutBack' => 'easeInOutBack',
					'easeInBounce' => 'easeInBounce',
					'easeOutBounce' => 'easeOutBounce',
					'easeInOutBounce' => 'easeInOutBounce',
				),			
            ),
			'labels' => array(
                'type' => 'text',
                'title' => __( 'Labels', 'intense' ),
                'description' => __( 'comma separated', 'intense' ),
                'skinnable' => '0'
            ),
            'percentage_inner_cutout' => array(
                'type' => 'text',
                'title' => __( 'Inner Cutout', 'intense' ),
                'default' => '50'
            ),
            'chart_data' => array(
				'type' => 'repeater',
				'title' => __( 'Data', 'intense' ),
				'fields' => array(
					'fill_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Fill Color', 'intense' ),
						'default' => 'rgba(220,220,220,0.5)'
					),
					'stroke_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Stroke Color', 'intense' ),
						'default' => 'rgba(220,220,220,1)'
					),
					'point_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Point Color', 'intense' ),
						'default' => 'rgba(220,220,220,1)'
					),
					'point_stroke_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Point Stroke Color', 'intense' ),
						'default' => '#ffffff'
					),
					'values' => array(
		                'type' => 'text',
		                'title' => __( 'Values', 'intense' ),
                        'skinnable' => '0'
		            ),
		            'colors' => array(
		                'type' => 'text',
		                'title' => __( 'Colors', 'intense' ),
                		'skinnable' => '0'             
		            ),					
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;

		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		intense_add_script( 'chartjs'  );
		intense_add_script( 'intense_modernizr' );
		intense_add_script( 'intense.compatible.canvas' );
		intense_add_script( 'jquery.appear' );

		$type = strtolower( $type );
		$random = rand();
		$labels = explode( ",", str_replace( " ", "", $labels ) );
		$font_size = $height / 4;
		$icon = ( isset( $icon ) ? intense_run_shortcode( 'intense_icon', array( 'type' => $icon ) ) : "" );

        $color = intense_get_plugin_color( $color );

		$output = '<div class="canvas-container" style="width:' . $width . 'px; height:' . $height . 'px; display: inline-block; position: relative; direction: ltr;">';
		$output .= '<div id="chart_text_' . $random . '" style="color: ' . $color . '; font-size: ' . $font_size . 'px; position: absolute;">' . $icon . ' ' . $text  . '</div>';
		$output .= '<canvas class="responsive-canvas" id="chart_' . $random . '" width="' . $width . '" height="' . $height . '"></canvas></div>';
		$output .= "<script> jQuery(function($){";
		$output .= '	$(window).load(function() {
    					var options = {
							animation: ' . ( $animation ? "true" : "false" ) . ',
							animationSteps: ' . $animation_steps . ',
							animationEasing: "' . $animation_easing . '",
							percentageInnerCutout: "' . $percentage_inner_cutout . '",
						};';

		if ( in_array( $type, array( "line", "bar", "radar" ) ) ) {
			$output .= 'var data = {';
			$output .= '	labels : [';

			foreach ( $labels as $label ) {
				$output .= "'$label',";
			}

			$output = rtrim( $output, "," );

			$output .= '],datasets : [';
			$output .= rtrim( rtrim( do_shortcode( $content ) ), "," );
			$output .= ']};';
		} else if ( in_array( $type, array( "polararea", "pie", "doughnut" ) ) ) {
			if ( $type == "polararea" ) $type = "PolarArea";
			$output .= 'var data = [';
			$output .= rtrim( rtrim( do_shortcode( $content ) ), "," );
			$output .= '];';
		}

		$output .= '		var $textBox = $("#chart_text_' . $random . '");';
		$output .= '		var $chartBox = $("#chart_text_' . $random . '").parent();';
		$output .= '		$textBox.css("top", (($chartBox.height() / 2) - ($textBox.height() / 2)) + "px");';
		$output .= '		$textBox.css("left", (($chartBox.width() / 2) - ($textBox.width() / 2)) + "px");';

		$output .= "		$('#chart_$random').appear(function() {";
		$output .= '			var canvas = $("#chart_' . $random . '").get(0);';
		$output .= "			window.G_vmlCanvasManager && (canvas = G_vmlCanvasManager.initElement(canvas)) && (options.animation = false);";
		$output .= '			var ctx = canvas.getContext("2d");';
		$output .= '			var chart = new Chart(ctx).' . ucfirst( $type ) . '(data, options);';
		$output .= '		},{accX: 0, accY: -' . $height / 3.5 . '});';
		$output .= "	});";
		$output .= "}); </script>";

		$output .= "<script>var compatibleCanvas = '" . INTENSE_PLUGIN_URL . '/assets/js/excanvas/excanvas.min.js' . "';</script>";

		return $output;
	}

	function render_dialog_script() {
	?>
		<script>
		jQuery(function($) {
			$('#type').change(function() {
				var typeImageName = $(this).val().replace(' ', '').toLowerCase();
				
				//$('#charttypeimage').attr('src', '<?php echo INTENSE_PLUGIN_URL . '/assets/img/chart-types/'; ?>' + typeImageName + '.png');
				$('#percentage_inner_cutout').parent().parent().hide();
				$('#labels').parent().parent().hide();

				$('.repeater-add').show();

				switch(typeImageName) {
					case "line":
						$('#labels').parent().parent().show();
						break;
					case "bar":
						$('#labels').parent().parent().show();
						break;
					case "radar":
						$('#labels').parent().parent().show();
						break;
					case "polararea":
						break;
					case "pie":
						break;
					case "doughnut":
						$('#percentage_inner_cutout').parent().parent().show();
						break;

				}

				hideInvalidData();
			});

			$('#type').change();

			$('#animation_steps').parent().parent().toggle();
			$('#animation_easing').parent().parent().toggle();	
		

			$('#animation').change(function() {
				$('#animation_steps').parent().parent().toggle();
				$('#animation_easing').parent().parent().toggle();							
			});

			function hideInvalidData() {
				var type = $('#type').val();
				var $form = $('#shortcode-form');

				$('.repeater-item').remove();

				if (type == 'line' || type == 'bar' || type == 'radar') {					
					$('[id^="fill_color"]', $form).parent().parent().show();
					$('[id^="stroke_color"]', $form).parent().parent().show();
					$('[id^="point_color"]', $form).parent().parent().show();
					$('[id^="point_stroke_color"]', $form).parent().parent().show();
					$('[id^="colors"]', $form).parent().parent().hide();	
					$('.repeater-add').show();				
				} else if (type == 'polararea' || type == 'pie' || type == 'doughnut') {					
					$('[id^="fill_color"]', $form).parent().parent().hide();
					$('[id^="stroke_color"]', $form).parent().parent().hide();
					$('[id^="point_color"]', $form).parent().parent().hide();
					$('[id^="point_stroke_color"]', $form).parent().parent().hide();
					$('[id^="colors"]', $form).parent().parent().show();
					$('.repeater-add').click();
					$('.repeater-add').hide();
				}
			}
		});
	</script>
	<?php
	}
}

class Intense_Chart_Data extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Chart Data', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-stats';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_chart';

		$this->fields = array(
			'fill_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Fill Color', 'intense' ),
				'default' => 'rgba(220,220,220,0.5)'
			),
			'stroke_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Stroke Color', 'intense' ),
				'default' => 'rgba(220,220,220,1)'
			),
			'point_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Point Color', 'intense' ),
				'default' => 'rgba(220,220,220,1)'
			),
			'point_stroke_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Point Stroke Color', 'intense' ),
				'default' => '#ffffff'
			),
			'values' => array(
                'type' => 'text',
                'title' => __( 'Values', 'intense' ),
                'skinnable' => '0'
            ),
            'colors' => array(
                'type' => 'text',
                'title' => __( 'Colors', 'intense' ),
                'skinnable' => '0'             
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( isset( $values ) && isset( $colors ) ) {
			$values = explode( ",", str_replace( " ", "", $values ) );
			$colors = explode( ",", str_replace( " ", "", $colors ) );
			$output = '';

			$i = 0;

			foreach ( $values as $value ) {
				$color = $colors[ $i ];
				$output .= "{ value : $value, color: '$color' },";
				$i++;
			}
			$output = rtrim( $output, "," );
		} else {
			$values = explode( ",", str_replace( " ", "", $values ) );
			$output = '{fillColor : "' . $fill_color . '", strokeColor : "' . $stroke_color . '", pointColor : "' . $point_color . '", pointStrokeColor : "' . $point_stroke_color . '", data : [';

			foreach ( $values as $value ) {
				$output .= "$value,";
			}

			$output = rtrim( $output, "," );
			$output .= ']},';
		}

		return $output;
	}
}
