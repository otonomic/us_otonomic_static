<?php

class Intense_Content_Box extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Content Box', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-box';
		$this->show_preview = true;
		$this->preview_content = __( "Content Box Text", 'intense' );
		$this->is_container = true;

		$this->fields = array(
			'content_box_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
					),
					'boxed' => array(
						'type' => 'checkbox',
						'title' => __( 'Boxed', 'intense' ),
						'default' => "1",
					),
					'shadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
					),
					'background' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background Color', 'intense' ),
						'default' => $intense_visions_options['intense_boxed_color'],
						'skinnable' => '0'
					),
					'background_opacity' => array(
						'type' => 'text',
						'title' => __( 'Background Opacity', 'intense' ),
						'description' => __( '0 - 100: leave blank for solid color', 'intense' )
					),
					'link' => array(
						'type' => 'text',
						'title' => __( 'Link', 'intense' ),
						'description' => __( 'will show in the bottom right corner. URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        'skinnable' => '0'
					),
					'link_target' => array(
						'type' => 'link_target',
						'title' => __( 'Link Target', 'intense' ),
						'default' => '_self',
					),
					'link_text' => array(
						'type' => 'text',
						'title' => __( 'Link Text', 'intense' ),
						'description' => __( 'default if a link is provided: Learn More >', 'intense' ),
                        'skinnable' => '0'
					),
					'rtl' => array(
						'type' => 'hidden',
						'description' => __( 'right-to-left', 'intense' ),
						'default' => $intense_visions_options['intense_rtl']
					)
				)
			),
			'icon_tab' => array(
				'type' => 'tab',
				'title' => __( 'Icon', 'intense' ),
				'fields' =>  array(
					'icon' => array(
						'type' => 'icon',
						'title' => __( 'Icon', 'intense' ),
						'default' => 'asterisk',
					),
					'icon_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
						'default' => '#ffffff'
					),
					'icon_stack_type' => array(
						'type' => 'icon',
						'title' => __( 'Stack Icon', 'intense' ),
						'default' => 'circle',
					),
					'icon_stack_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Stack Icon Color', 'intense' ),
						'default' => $intense_visions_options['intense_icon_color'],
						'skinnable' => '0'
					),
					'size' => array(
						'type' => 'dropdown',
						'title' => __( 'Icon Size', 'intense' ),
						'default' => '3',
						'options' => array( 
							'1' => '1', 
							'2' => '2', 
							'3' => '3', 
							'4' => '4', 
							'5' => '5' 
						)
					),
					'custom_icon' => array(
						'type' => 'image',
						'title' => __( 'Custom Icon', 'intense' ),
					),
					'position' => array(
						'type' => 'dropdown',
						'title' => __( 'Position', 'intense' ),
						'default' => 'topcenter',
						'options' => array(
							'topcenter' => __( 'Top Center', 'intense' ),
							'topright' => __( 'Top Right', 'intense' ),
							'topleft' => __( 'Top Left', 'intense' ),
							'bottomcenter' => __( 'Bottom Center', 'intense' ),
							'bottomright' => __( 'Bottom Right', 'intense' ),
							'bottomleft' => __( 'Bottom Left', 'intense' ),
							'inside' => __( 'Inside', 'intense' ),
							'insidetop' => __( 'Inside Top', 'intense' ),
							'insidebottom' => __( 'Inside Bottom', 'intense' ),
						)
					),
					'animation' => array(
						'type' => 'animation',
						'title' => __( 'Icon Animation', 'intense' ),
						'placeholder' => __( 'Select an animation', 'intense' ),
						'default' => ''
					),
				)
			),
			'border_tab' => array(
				'type' => 'tab',
				'title' => __( 'Border', 'intense' ),
				'fields' =>  array(
					'border_size' => array(
						'type' => 'text',
						'title' => __( 'Border Size', 'intense' ),
						'description' => 'measured in pixels',
						'default' => $intense_visions_options['intense_content_box_style']['border-top'],
						'skinnable' => '0'
					),
					'border_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Border Color', 'intense' ),
						'default' => $intense_visions_options['intense_content_box_style']['border-color'],
						'skinnable' => '0'
					),
					'border_style' => array(
						'type' => 'border_style',
						'title' => __( 'Border Style', 'intense' ),
						'default' => $intense_visions_options['intense_content_box_style']['border-style'],
						'skinnable' => '0'
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
					),
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_numeric( $border_size ) && $border_size != 0 ) $border_size .= "px";

		$border_color = intense_get_plugin_color( $border_color );

		if ( isset( $background ) ) {
			if ( in_array( $background, array( "warning", "error", "success", "info", "inverse", "muted", "primary" ) ) ) {
				$background_color_style = "background-color: " . intense_get_plugin_color( $background ) . " !important;";
				$backcolor = intense_get_plugin_color( $background );
			} else {
				$background_color_style = "background-color: " . $background . " !important;";
				$backcolor = $background;
			}
		} else {
			$background_color_style = "background-color: " . intense_get_plugin_color( "boxed" ) . " !important;";
		}

		if ( isset( $background_opacity ) && strlen( $backcolor ) > 0 ) {
			$hex = str_replace( "#", "", $backcolor );
			$a = $background_opacity / 100;

			if ( strlen( $hex ) == 3 ) {
				$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
				$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
				$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
			} else {
				$r = hexdec( substr( $hex, 0, 2 ) );
				$g = hexdec( substr( $hex, 2, 2 ) );
				$b = hexdec( substr( $hex, 4, 2 ) );
			}

			$background_color_style = "background-color: rgba(" . $r . "," . $g . "," . $b . "," . $a . ") !important;";
		}

		$radius = '';
		if ( isset( $border_radius ) ) {
			$radius = 'border-radius:' . $border_radius . ';';
		}

		intense_add_script( 'intense.contentbox' );

		if ( !empty( $animation ) ) {
			intense_load_animation_CSS( $animation );
			intense_add_style( 'intense.animated' );
			intense_add_script( 'intense.animated' );
		}

		if ( is_numeric( $custom_icon ) ) {
			$photo_info = wp_get_attachment_image_src( $custom_icon, $size );
			$custom_icon = $photo_info[0];
		}

		$icon_shortcode = intense_run_shortcode( 'intense_icon', array( 'type' => $icon, 'stack_type' => $icon_stack_type, 'color' => $icon_color, 'stack_color' => $icon_stack_color, 'size' => $size ) );

		$shadow_style = '';
		$output = '';
		$iconwidth = '';
		$output .= '<div class="intense content-box ' . $position . ' ' . ( !$boxed ? "unboxed " : "boxed " ) . ( $rtl ? 'rtl ' : '' ) . '" ' . ( isset( $animation ) ? "data-animation='$animation'" : "" ) . '>';

		if ( $size == '4' ) {
			$iconwidth = 'width: 84px;';
		}

		if ( $size == '5' ) {
			$iconwidth = 'width: 112px;';
		}


		if ( in_array( $position, array( "topcenter", "topleft", "topright" ) ) ) {
			$output .= '  <div class="content-box-icon" style="' . $iconwidth . '">';

			if ( isset( $custom_icon ) ) {
				$output .= "<img style='width:100%;' src='" . $custom_icon . "' alt='' />";
			} else {
				$output .= $icon_shortcode;
			}

			$output .= '  </div>';
		}

		if ( $boxed ) {
			$style = $background_color_style . ' border:' . $border_size . ' ' . $border_style . ' ' . $border_color . '; ' . $radius . ';';
		} else {
			$style = '';
		}

		$output .= "<div class='content-box-text-wrapper'>";

		if ( isset( $shadow ) && $shadow != 0 && !in_array( $position, array( "bottomcenter", "bottomleft", "bottomright" ) ) ) {
			$output .= "<div style='$shadow_style'>";
		}

		$output .= '  <div class="content-box-text" style="' . $style . '">';

		if ( $position == "insidetop" ) {
			$output .= "<div class='content-box-icon' style='" . $iconwidth . "'>";

			if ( isset( $custom_icon ) ) {
				$output .= "<img style='width:100%;' src='" . $custom_icon . "' alt='' />";
			} else {
				$output .= $icon_shortcode;
			}

			$output .= "</div>";
		}

		if ( isset( $title ) ) {
			if ( isset( $custom_icon ) ) {
				$icon = "<img style='width:50px;' src='" . $custom_icon . "' alt='' />";
			} else {
				$icon = $icon_shortcode;
			}
			$output .= '    <div class="content-box-title"><h2>' . ( $position == "inside" ? $icon . " " : "" ) . $title . '</h2></div>';
		}

		$output .= '    <div class="content-box-excerpt">' . do_shortcode( $content ) . '</div>';

		if ( !empty( $link ) ) {
			if ( is_numeric( $link ) ) {
				$link = get_permalink( $link );
			}
			
			$output .= '    <div class="pull-right content-box-link"><a href="' . $link . '" target="' . $link_target . '">' . $link_text . '</a></div><div class="clearfix"></div>';
		}

		if ( $position == "insidebottom" ) {
			$output .= "<div class='content-box-icon' style='" . $iconwidth . "'>";

			if ( isset( $custom_icon ) ) {
				$output .= "<img style='width:100%;' src='" . $custom_icon . "' alt='' />";
			} else {
				$output .= $icon_shortcode;
			}

			$output .= "</div>";
		}

		$output .= '  </div>';

		if ( isset( $shadow ) && $shadow != 0 && !in_array( $position, array( "bottomcenter", "bottomleft", "bottomright" ) ) ) {
			$output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0;' />";
		}

		$output .= "</div>";

		if ( in_array( $position, array( "bottomcenter", "bottomleft", "bottomright" ) ) ) {
			$output .= '  <div class="content-box-icon" style="' . $iconwidth . '">';

			if ( isset( $custom_icon ) ) {
				$output .= "<img style='width:100%;' src='" . $custom_icon . "' alt='' />";
			} else {
				$output .= $icon_shortcode;
			}

			$output .= '  </div>';
		}

		$output .= '</div>';

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
				jQuery(function($) {
					// Uploading files
			  		$('.upload_image_button').live('click', function( event ){
					    event.preventDefault();

					    window.parent.loadImageFrame($(this), function(attachment) {
					    	jQuery("#custom_icon").val(attachment.url);
					    	$('#custom_icon-thumb').attr('src', attachment.url);
					    	
					    	if ( $('#preview-content').length > 0 ) {
		                      $('#preview').click();
		                    }
					    });
					});
				});
			</script>
		<?php
	}
}
