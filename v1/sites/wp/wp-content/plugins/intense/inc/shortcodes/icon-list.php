<?php

class Intense_Icon_List extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Icon List', 'intense' );
    $this->category = __( 'Layout', 'intense' );
    $this->icon = 'dashicons-intense-list';
    $this->show_preview = true;
    $this->is_container = true;

    $this->fields = array(
      'icon_list_item' => array(
        'type' => 'repeater',
        'title' => __( 'List Items', 'intense' ),
        'preview_content' => __( 'Item', 'intense' ),
        'fields' => array(
          'type' => array(
            'type' => 'icon',
            'title' => __( 'Icon', 'intense' ),
            'default' => '',
          ),
          'size' => array(
            'type' => 'dropdown',
            'title' => __( 'Size', 'intense' ),
            'default' => '1',
            'options' => array( 
              '1' => '1', 
              '2' => '2', 
              '3' => '3', 
              '4' => '4', 
              '5' => '5' 
            )
          ),
          'color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Color', 'intense' ),
            'default' => ''
          ),
          'spin' => array(
            'type' => 'checkbox',
            'title' => __( 'Spin', 'intense' ),
            'default' => "0",
          ),
          'rotate' => array(
            'type' => 'dropdown',
            'title' => __( 'Rotate', 'intense' ),
            'default' => '',
            'options' => array(
              '' => '',
              '90' => __( '90 degrees', 'intense' ),
              '180' => __( '180 degrees', 'intense' ),
              '270' => __( '270 degrees', 'intense' )
            )
          ),
          'flip' => array(
            'type' => 'dropdown',
            'title' => __( 'Flip', 'intense' ),
            'default' => '',
            'options' => array(
              '' => '',
              'horizontal' => __( 'Horizontal', 'intense' ),
              'vertical' => __( 'Vertical', 'intense' )
            )
          ),
          'stack_type' => array(
            'type' => 'icon',
            'title' => __( 'Stack Icon', 'intense' ),
            'default' => '',
          ),
          'stack_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Stack Color', 'intense' ),
            'default' => '#ffffff'
          ),
          'custom_icon' => array(
            'type' => 'image',
            'title' => __( 'Custom Icon', 'intense' ),
            'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
            'skinnable' => '0'
          ),
          'rtl' => array(
            'type' => 'hidden',
            'description' => __( 'right-to-left', 'intense' ),
            'default' => $intense_visions_options['intense_rtl']
          )
        )
      ),
      'rtl' => array(
        'type' => 'hidden',
        'description' => __( 'right-to-left', 'intense' ),
        'default' => $intense_visions_options['intense_rtl']
      )
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    $output = "<ul class='intense icons-ul " . ( $rtl ? "rtl " : "" ) . "'>";
    $output .= do_shortcode( $content );
    $output .="</ul>";

    return $output;
  }

  function render_dialog_script() {
?>
    <script>
    jQuery(function($) {
        $('.upload_image_button').live('click', function( event ){
          event.preventDefault();
          var target = $(this).data('target-id');

          window.parent.loadImageFrame($(this), function(attachment) {
            jQuery("#" + target).val(attachment.id);
            
            if ( attachment.sizes.thumbnail ) {
              jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
            } else {
              jQuery("#" + target + "-thumb").attr("src", attachment.url);
            }

            if ( $('#preview-content').length > 0 ) {
              $('#preview').click();
            }
          });
        });
    });
  </script>

  <?php
  }
}

class Intense_Icon_List_Item extends Intense_Shortcode {
  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Icon List Item', 'intense' );
    $this->category = __( 'Typography', 'intense' );
    $this->icon = 'dashicons-intense-list';
    $this->show_preview = true;
    $this->hidden = true;
    $this->parent = 'intense_icon_list';

    $this->fields = array(
      'type' => array(
        'type' => 'icon',
        'title' => __( 'Icon', 'intense' ),
        'default' => '',
      ),
      'size' => array(
        'type' => 'dropdown',
        'title' => __( 'Size', 'intense' ),
        'default' => '1',
        'options' => array( 
              '1' => '1', 
              '2' => '2', 
              '3' => '3', 
              '4' => '4', 
              '5' => '5' 
        )
      ),
      'color' => array(
        'type' => 'color_advanced',
        'title' => __( 'Color', 'intense' ),
        'default' => ''
      ),
      'spin' => array(
        'type' => 'checkbox',
        'title' => __( 'Spin', 'intense' ),
        'default' => "0",
      ),
      'rotate' => array(
        'type' => 'dropdown',
        'title' => __( 'Rotate', 'intense' ),
        'default' => '',
        'options' => array(
          '' => '',
          '90' => __( '90 degrees', 'intense' ),
          '180' => __( '180 degrees', 'intense' ),
          '270' => __( '270 degrees', 'intense' )
        )
      ),
      'flip' => array(
        'type' => 'dropdown',
        'title' => __( 'Flip', 'intense' ),
        'default' => '',
        'options' => array(
          '' => '',
          'horizontal' => __( 'Horizontal', 'intense' ),
          'vertical' => __( 'Vertical', 'intense' )
        )
      ),
      'stack_type' => array(
        'type' => 'icon',
        'title' => __( 'Stack Icon', 'intense' ),
        'default' => '',
      ),
      'stack_color' => array(
        'type' => 'color_advanced',
        'title' => __( 'Stack Color', 'intense' ),
        'default' => '#ffffff'
      ),
      'custom_icon' => array(
        'type' => 'image',
        'title' => __( 'Custom Icon', 'intense' ),
        'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
        'skinnable' => '0'
      ),
      'rtl' => array(
        'type' => 'hidden',
        'description' => __( 'right-to-left', 'intense' ),
        'default' => $intense_visions_options['intense_rtl']
      )
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    if ( is_numeric( $custom_icon ) ) {
      $photo_info = wp_get_attachment_image_src( $custom_icon, null );
      $custom_icon = $photo_info[0];
    }

    if ( in_array( $color, array( "warning", "error", "success", "info", "inverse", "muted", "primary" ) ) ) {
      $color_class = "{$color}-icon";
      $color_style = "";
    } else {
      $color_class = "";
      $color_style = "color: {$color} !important;";
    }

    if ( isset( $custom_icon ) ) {
      $output = '<li class="intense ' . ( $rtl ? "rtl " : "" ) . '" style="left: -1.8em; margin-left: 0px;"><img class="intense img-icon' . ( $size - 1 ) . 'x" src="' . $custom_icon . '" alt="" /><div class="intense icons-content-' . ( $size - 1 ) . 'x" style="display:inline;">' . do_shortcode( $content ) . '</div></li>';
    } else {
      $icon = intense_run_shortcode( 'intense_icon', array( 
        'type' => $type,
        'color' => $color,
        'size' => $size,
        'spin' => $spin,
        'rotate' => $rotate,
        'flip' => $flip,
        'stack_type' => $stack_type,
        'stack_color' => $stack_color,
        'extra_class' => 'icon-li'
      ));
      $output = '<li class="intense icons-li-' . ( $size - 1 ) . 'x ' . ( $rtl ? "rtl " : "" ) . '">' . $icon . '<div class="intense icons-content-' . ( $size - 1 ) . 'x">' . do_shortcode( $content ) . '</div></li>';
    }

    return $output;
  }
}
