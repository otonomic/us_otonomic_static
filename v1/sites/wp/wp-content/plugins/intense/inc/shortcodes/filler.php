<?php

class Intense_Filler extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Filler', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-editor-justify';
		$this->show_preview = true;

		$this->fields = array(
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'default' => 'text',
				'options' => array(
					'text' => __( 'Text', 'intense' ),
					'image' => __( 'image', 'intense' ),
				),
			),
			'paragraphs' => array(
				'type' => 'text',
				'title' => __( 'Paragraphs', 'intense' ),
				'class' => 'textsettings'
			),
			'words' => array(
				'type' => 'text',
				'title' => __( 'Words', 'intense' ),
				'class' => 'textsettings'
			),
			'bytes' => array(
				'type' => 'text',
				'title' => __( 'Bytes', 'intense' ),
				'class' => 'textsettings'
			),
			'list' => array(
				'type' => 'text',
				'title' => __( 'List Items', 'intense' ),
				'class' => 'textsettings'
			),
			'paragraph_separator' => array(
				'type' => 'dropdown',
				'title' => __( 'Paragraph Separator', 'intense' ),
				'default' => 'p',
				'options' => array(
					'br' => __( 'Break', 'intense' ),
					'p' => __( 'Paragraph', 'intense' ),
					'span' => __( 'Span', 'intense' ),
				),
				'class' => 'textsettings'
			),
			'start_with_lorem' => array(
				'type' => 'checkbox',
				'title' => __( 'Start with Lorem', 'intense' ),
				'default' => "1",
				'class' => 'textsettings'
			),
			'is_title' => array(
				'type' => 'checkbox',
				'title' => __( 'Is Title', 'intense' ),
				'description' => __( 'indicates that the text should have the first character of each word uppercased', 'intense' ),
				'default' => "0",
				'class' => 'textsettings'
			),
			'width' => array(
				'type' => 'text',
				'title' => __( 'Width', 'intense' ),
				'description' => __( 'max 1920', 'intense' ),
				'default' => '400',
				'class' => 'imagesettings'
			),
			'height' => array(
				'type' => 'text',
				'title' => __( 'Height', 'intense' ),
				'description' => __( 'max 1920', 'intense' ),
				'default' => '250',
				'class' => 'imagesettings'
			),
			'grayscale' => array(
				'type' => 'checkbox',
				'title' => __( 'Grayscale', 'intense' ),
				'default' => "0",
				'class' => 'imagesettings'
			),
			'category' => array(
				'type' => 'dropdown',
				'title' => __( 'Category', 'intense' ),
				'default' => '',
				'options' => array(
					'' => __( 'Random', 'intense' ),
					'abstract' => __( 'Abstract', 'intense' ),
					'animals' => __( 'Animals', 'intense' ),
					'business' => __( 'Business', 'intense' ),
					'cats' => __( 'Cats', 'intense' ),
					'city' => __( 'City', 'intense' ),
					'food' => __( 'Food', 'intense' ),
					'nature' => __( 'Nature', 'intense' ),
					'people' => __( 'People', 'intense' ),
					'sports' => __( 'Sports', 'intense' ),
					'technics' => __( 'Technics', 'intense' ),
					'transport' => __( 'Transport', 'intense' ),
				),
				'class' => 'imagesettings'
			),
			'align' => array(
				'type' => 'dropdown',
				'title' => __( 'Align', 'intense' ),
				'default' => '',
				'options' => array(
					'' => '',
					'left' => __( 'Left', 'intense' ),
					'right' => __( 'Right', 'intense' ),
					'middle' => __( 'Middle', 'intense' ),
					'top' => __( 'Top', 'intense' ),
					'bottom' => __( 'Bottom', 'intense' ),
				),
				'class' => 'imagesettings'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$lispum = array();

		if ( $type == 'text' ) {
			$lipsum[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
			$lipsum[] = "Duis varius lectus lobortis ipsum tempor, ut interdum eros facilisis.";
			$lipsum[] = "Curabitur varius odio a adipiscing tempor.";
			$lipsum[] = "Aliquam adipiscing risus et vestibulum sollicitudin.";
			$lipsum[] = "Quisque pharetra eros convallis diam bibendum, ac vulputate augue aliquet.";
			$lipsum[] = "Fusce tristique dolor nec turpis euismod, a tempus arcu elementum.";
			$lipsum[] = "Vestibulum vel metus sit amet enim sollicitudin euismod eget nec justo.";
			$lipsum[] = "Praesent mollis lacus sit amet tortor malesuada placerat.";
			$lipsum[] = "Nullam eget metus sed velit laoreet viverra eu a augue.";
			$lipsum[] = "Nulla condimentum nisl vestibulum sapien feugiat, eget facilisis felis ornare.";
			$lipsum[] = "Integer iaculis purus sed congue porta.";
			$lipsum[] = "Pellentesque quis nisi sed elit aliquet fermentum ut at augue.";
			$lipsum[] = "Proin rutrum enim quis enim sagittis, at mattis purus rutrum.";
			$lipsum[] = "Vestibulum id erat quis ante dictum consequat a sed dui.";
			$lipsum[] = "Nulla ultricies magna a dui dapibus, non gravida urna imperdiet.";
			$lipsum[] = "Aenean laoreet eros eu tellus luctus, sed tristique augue commodo.";
			$lipsum[] = "Sed tristique justo id ipsum luctus fringilla.";
			$lipsum[] = "Pellentesque posuere neque sed dignissim gravida.";
			$lipsum[] = "Sed sed mi facilisis, pellentesque ipsum at, viverra purus.";
			$lipsum[] = "Fusce ut enim id libero aliquet ullamcorper.";
			$lipsum[] = "Quisque quis nibh commodo, ornare nisi eu, fringilla urna.";
			$lipsum[] = "In in nulla vel arcu bibendum aliquet.";
			$lipsum[] = "In ac lorem feugiat, molestie ligula ac, congue risus.";
			$lipsum[] = "Pellentesque faucibus lacus ac adipiscing fringilla.";
			$lipsum[] = "Curabitur rutrum leo id dolor egestas ullamcorper.";
			$lipsum[] = "Cras et velit in ante gravida elementum et molestie ipsum.";
			$lipsum[] = "Curabitur id risus vulputate, aliquet est id, tempus leo.";
			$lipsum[] = "Cras hendrerit tortor eget tortor pharetra consequat.";
			$lipsum[] = "Phasellus varius justo viverra, scelerisque nunc vitae, dapibus lorem.";
			$lipsum[] = "Fusce vitae libero ac ipsum ultrices mollis.";
			$lipsum[] = "Aliquam eleifend nibh eget arcu ornare dapibus dignissim fringilla nibh.";
			$lipsum[] = "Maecenas quis lorem hendrerit, vulputate nulla et, mollis risus.";
			$lipsum[] = "Quisque ac arcu ut magna semper eleifend.";
			$lipsum[] = "Quisque ut dolor nec nulla ullamcorper vestibulum.";
			$lipsum[] = "Nulla id mi sed velit scelerisque fringilla.";
			$lipsum[] = "Sed scelerisque velit vel enim molestie porta.";
			$lipsum[] = "Aenean quis nulla vel dolor blandit mattis et vel sem.";
			$lipsum[] = "Nullam feugiat felis et massa accumsan, ut dictum nisl rhoncus.";
			$lipsum[] = "Curabitur quis sem at massa fermentum egestas.";
			$lipsum[] = "Suspendisse sed ante tempus, lobortis massa quis, aliquet elit.";
			$lipsum[] = "Praesent aliquam nisl et lacus fringilla dignissim.";
			$lipsum[] = "Vestibulum non odio adipiscing nulla elementum semper eu porttitor orci.";
			$lipsum[] = "Aliquam tincidunt nibh vitae leo fringilla gravida nec eget velit.";
			$lipsum[] = "Nullam non lorem mollis, cursus libero non, consectetur erat.";
			$lipsum[] = "Sed a sapien et urna sagittis lacinia eget id nisi.";
			$lipsum[] = "Proin non tortor venenatis nisl condimentum viverra sit amet eu ligula.";
			$lipsum[] = "Donec ut tortor in risus eleifend porttitor.";
			$lipsum[] = "Mauris porta nulla et tortor convallis, id posuere est rutrum.";
			$lipsum[] = "Integer feugiat neque in sem rutrum laoreet.";
			$lipsum[] = "Nullam a sem a nunc facilisis suscipit sit amet sed ipsum.";
			$lipsum[] = "Phasellus sit amet odio sed leo laoreet consectetur.";
			$lipsum[] = "Vestibulum ut nulla nec turpis pulvinar sodales sed vel purus.";
			$lipsum[] = "Mauris a eros placerat, sodales erat sed, hendrerit erat.";
			$lipsum[] = "Donec porttitor ipsum ac facilisis mattis.";
			$lipsum[] = "In vitae ligula interdum, auctor quam vitae, eleifend quam.";
			$lipsum[] = "Donec molestie urna ut velit facilisis fermentum.";
			$lipsum[] = "Donec at massa id leo imperdiet tincidunt id at ipsum.";
			$lipsum[] = "Nulla euismod justo adipiscing ante porttitor adipiscing.";
			$lipsum[] = "Proin egestas tortor sit amet venenatis posuere.";
			$lipsum[] = "Curabitur tristique ligula ut elit luctus convallis.";
			$lipsum[] = "Mauris sit amet nibh sagittis, elementum metus et, facilisis massa.";
			$lipsum[] = "Etiam pretium massa malesuada vestibulum volutpat.";
			$lipsum[] = "Duis consectetur est eget mi consectetur, sed dignissim mauris lacinia.";
			$lipsum[] = "Etiam vulputate lectus eget enim porttitor euismod.";
			$lipsum[] = "Morbi at libero at sapien ultricies feugiat in quis orci.";
			$lipsum[] = "Phasellus molestie massa id posuere cursus.";
			$lipsum[] = "Nunc molestie dui eget accumsan mattis.";
			$lipsum[] = "Cras sit amet eros eget augue ornare posuere ac vel odio.";
			$lipsum[] = "Vestibulum eu lectus non nunc vehicula blandit.";
			$lipsum[] = "In feugiat lacus et velit ultrices volutpat.";
			$lipsum[] = "Aenean venenatis neque non ipsum sollicitudin porta.";
			$lipsum[] = "Donec eget quam non ligula placerat ultrices.";
			$lipsum[] = "Morbi sit amet massa nec elit lobortis dictum id at leo.";
			$lipsum[] = "Nam non mi ut orci commodo bibendum.";
			$lipsum[] = "Donec dapibus ante eu tellus tempus sodales.";
			$lipsum[] = "Nam laoreet sapien ac odio ullamcorper posuere.";
			$lipsum[] = "Morbi egestas ipsum sit amet lorem porttitor eleifend.";
			$lipsum[] = "Vivamus mollis libero nec turpis lobortis, nec congue augue lacinia.";
			$lipsum[] = "Vestibulum porta justo a velit porttitor tincidunt.";
			$lipsum[] = "In scelerisque sapien quis eros convallis imperdiet.";
			$lipsum[] = "Curabitur eu nisl in tortor lacinia suscipit eu quis nulla.";
			$lipsum[] = "In nec tellus at neque malesuada malesuada et sed nibh.";
			$lipsum[] = "Fusce a odio sollicitudin, euismod ipsum at, pellentesque leo.";
			$lipsum[] = "Proin ultricies massa vitae nisl ultrices imperdiet.";
			$lipsum[] = "Cras laoreet diam ac mauris placerat sollicitudin.";
			$lipsum[] = "Nulla feugiat orci non libero ultrices dignissim.";
			$lipsum[] = "Curabitur blandit elit nec ipsum dapibus suscipit.";
			$lipsum[] = "Duis feugiat orci quis tempor faucibus.";
			$lipsum[] = "Praesent in justo sit amet lectus venenatis auctor quis sed risus.";
			$lipsum[] = "Quisque ac orci condimentum, suscipit nibh vitae, viverra elit.";
			$lipsum[] = "Praesent porta tellus ac tortor dapibus, non faucibus ligula malesuada.";
			$lipsum[] = "Nunc fermentum tortor pulvinar, auctor arcu sit amet, molestie ligula.";
			$lipsum[] = "Nunc ac lectus vel tortor rutrum feugiat.";
			$lipsum[] = "Nam cursus urna sit amet lacus laoreet, pulvinar laoreet lectus lobortis.";
			$lipsum[] = "Aliquam iaculis tortor ac dapibus condimentum.";
			$lipsum[] = "Mauris at ipsum vel metus viverra ultricies.";
			$lipsum[] = "Mauris facilisis felis eu nunc auctor, ac porttitor ante pellentesque.";
			$lipsum[] = "Suspendisse semper neque adipiscing nibh ullamcorper tristique.";
			$lipsum[] = "Ut egestas magna sed ipsum faucibus pellentesque.";
			$lipsum[] = "Quisque rutrum libero vitae ullamcorper consequat.";
			$lipsum[] = "Ut eu leo at libero tempor blandit.";
			$lipsum[] = "Cras sed mauris non massa tincidunt viverra a consequat ligula.";
			$lipsum[] = "Aliquam dapibus metus vitae libero sollicitudin, vel sodales sapien tristique.";
			$lipsum[] = "Vestibulum at diam tristique, interdum sapien in, blandit nulla.";
			$lipsum[] = "Ut egestas lectus sit amet ante posuere molestie.";
			$lipsum[] = "Donec tempor nisl eget libero imperdiet auctor.";
			$lipsum[] = "Vivamus suscipit magna vitae placerat dapibus.";
			$lipsum[] = "Quisque feugiat sem nec nisi sodales ullamcorper.";
			$lipsum[] = "Maecenas sollicitudin purus sodales leo molestie interdum.";
			$lipsum[] = "Duis id lacus mollis, lacinia mauris eget, accumsan mi.";
			$lipsum[] = "Vivamus sit amet lacus a justo venenatis pulvinar.";
			$lipsum[] = "Sed eget urna vel orci aliquam molestie quis sed elit.";
			$lipsum[] = "Praesent dictum augue eu urna ultricies adipiscing.";
			$lipsum[] = "Ut nec dui quis erat pellentesque interdum.";
			$lipsum[] = "Morbi varius dui non nulla pretium, at auctor augue sollicitudin.";
			$lipsum[] = "Suspendisse vel velit eu diam molestie ornare nec quis turpis.";
			$lipsum[] = "Fusce sit amet risus et diam congue venenatis.";
			$lipsum[] = "Aliquam rutrum quam non interdum egestas.";
			$lipsum[] = "Suspendisse feugiat tortor sed ante egestas viverra.";
			$lipsum[] = "Nunc congue sem vel sapien hendrerit, quis pellentesque mi viverra.";
			$lipsum[] = "Mauris vel arcu tincidunt quam convallis viverra in vel ante.";
			$lipsum[] = "Nullam laoreet dui nec lorem vestibulum, nec viverra lacus bibendum.";
			$lipsum[] = "Sed vehicula est non massa porttitor interdum.";
			$lipsum[] = "Morbi et ligula a enim euismod lobortis.";
			$lipsum[] = "Duis ut dui ac lacus congue suscipit sed eu augue.";
			$lipsum[] = "Pellentesque elementum erat sed justo rhoncus eleifend.";
			$lipsum[] = "Donec at nunc a arcu congue posuere.";
			$lipsum[] = "Curabitur dapibus ipsum eu ligula dignissim scelerisque.";
			$lipsum[] = "Suspendisse sed mauris sed leo feugiat consequat.";
			$lipsum[] = "Cras tincidunt ipsum et scelerisque ultrices.";
			$lipsum[] = "Nulla dignissim mi in orci venenatis condimentum.";
			$lipsum[] = "Vestibulum vitae dui non diam tincidunt malesuada.";
			$lipsum[] = "Praesent commodo diam vitae sodales laoreet.";
			$lipsum[] = "Pellentesque ullamcorper dui tincidunt pellentesque tempus.";
			$lipsum[] = "Vivamus ac leo mollis, posuere sapien id, accumsan sapien.";
			$lipsum[] = "Nunc sed velit mollis enim gravida malesuada.";
			$lipsum[] = "Aliquam in augue congue, consectetur est eget, gravida enim.";
			$lipsum[] = "Etiam gravida elit a dui ornare, eu consequat tellus fringilla.";
			$lipsum[] = "Cras eu neque sodales, ultricies lectus nec, laoreet est.";
			$lipsum[] = "Donec euismod purus id blandit faucibus.";
			$lipsum[] = "Sed a elit lobortis, facilisis risus vel, fringilla enim.";
			$lipsum[] = "Sed ultricies justo non diam vulputate, adipiscing dictum purus ullamcorper.";
			$lipsum[] = "Quisque tempor metus vel nisl mollis, sit amet sollicitudin elit pretium.";
			$lipsum[] = "Nullam et lorem sodales, accumsan nisl quis, fermentum ante.";
			$lipsum[] = "Phasellus eleifend massa at euismod rutrum.";
			$lipsum[] = "Vestibulum faucibus sapien eu justo posuere, sit amet tempus magna pretium.";
			$lipsum[] = "Nulla eu justo vitae diam bibendum volutpat vitae sit amet leo.";
			$lipsum[] = "Donec tincidunt erat in vestibulum interdum.";
			$lipsum[] = "Proin vel orci mattis nunc ullamcorper gravida et viverra orci.";
			$lipsum[] = "Morbi ut massa a orci imperdiet euismod vel nec urna.";
			$lipsum[] = "In convallis velit tincidunt eleifend vulputate.";
			$lipsum[] = "Cras at dolor sed felis volutpat aliquet vel et enim.";
			$lipsum[] = "Suspendisse eu nunc rutrum, vulputate enim sed, lobortis erat.";
			$lipsum[] = "Nunc accumsan arcu ac blandit semper.";
			$lipsum[] = "Praesent ac mi quis dui tincidunt suscipit.";
			$lipsum[] = "Morbi vitae sem eu lacus laoreet scelerisque ac a dui.";
			$lipsum[] = "Praesent mattis nibh eu enim laoreet adipiscing.";
			$lipsum[] = "In vel dui in mauris eleifend luctus.";
			$lipsum[] = "Mauris sed dolor viverra, accumsan nisl ac, interdum metus.";
			$lipsum[] = "Aliquam pretium justo id orci porta, ut tincidunt ipsum iaculis.";
			$lipsum[] = "Suspendisse sit amet ipsum placerat, dictum purus et, dignissim nunc.";
			$lipsum[] = "Aenean vitae nisi id nulla tristique egestas at eget odio.";
			$lipsum[] = "Aliquam viverra eros quis cursus sagittis.";
			$lipsum[] = "Pellentesque id dolor interdum, fermentum ligula id, pretium urna.";
			$lipsum[] = "Proin ultricies quam quis nisi pharetra, ut malesuada nibh varius.";
			$lipsum[] = "Praesent et enim eu orci aliquam eleifend.";
			$lipsum[] = "Aliquam iaculis risus congue, dignissim eros ut, lacinia nulla.";
			$lipsum[] = "Fusce sagittis metus in magna vulputate lobortis.";
			$lipsum[] = "Ut vel nibh consectetur, sagittis tellus vel, ultricies purus.";
			$lipsum[] = "Pellentesque sit amet quam nec tellus condimentum suscipit non et purus.";
			$lipsum[] = "Sed quis augue vel ligula aliquet bibendum at a lectus.";
			$lipsum[] = "Donec feugiat justo sed odio accumsan, sit amet placerat tortor laoreet.";
			$lipsum[] = "Proin eu sem sit amet libero tristique accumsan in non dui.";
			$lipsum[] = "Aliquam ut turpis eu est auctor laoreet volutpat nec ante.";
			$lipsum[] = "Nunc vulputate metus et magna consectetur accumsan.";
			$lipsum[] = "Phasellus quis lectus sit amet libero tincidunt lobortis.";
			$lipsum[] = "Praesent a justo mattis, pellentesque lacus placerat, ornare est.";
			$lipsum[] = "Nullam eu tortor vitae turpis malesuada aliquam.";
			$lipsum[] = "Praesent at massa tincidunt, malesuada sem sed, mattis lorem.";
			$lipsum[] = "Ut condimentum lacus eu felis placerat, in adipiscing turpis sollicitudin.";
			$lipsum[] = "Duis molestie magna sit amet lacinia commodo.";
			$lipsum[] = "Fusce pulvinar nisi nec leo lobortis, quis rutrum eros feugiat.";
			$lipsum[] = "Etiam quis ligula eleifend dolor convallis sagittis.";
			$lipsum[] = "Donec varius magna nec ante vehicula pulvinar.";
			$lipsum[] = "Sed ultricies purus vitae est ornare vulputate.";
			$lipsum[] = "Ut in nulla tincidunt nulla malesuada tempor ac non risus.";
			$lipsum[] = "Aliquam vel massa vulputate, dictum nulla sodales, suscipit nisi.";
			$lipsum[] = "Nulla mattis nulla sed ultricies sodales.";
			$lipsum[] = "Sed ut augue viverra, lobortis lacus nec, tincidunt mi.";
			$lipsum[] = "Aliquam eu nisl ultrices, aliquet tortor faucibus, convallis magna.";
			$lipsum[] = "Donec in tellus ut libero scelerisque venenatis.";
			$lipsum[] = "Donec tempor urna at auctor dictum.";
			$lipsum[] = "Suspendisse quis justo et tellus mollis suscipit ut sed metus.";
			$lipsum[] = "Quisque tempus leo quis tempus hendrerit.";
			$lipsum[] = "Etiam sit amet arcu egestas, viverra dui sit amet, sodales urna.";
			$lipsum[] = "Mauris quis ante sed mauris porta lacinia sed a dui.";
			$lipsum[] = "Nunc sed erat in nisl malesuada vestibulum.";
			$lipsum[] = "Nam ac dolor et lorem euismod aliquet.";
			$lipsum[] = "Cras tincidunt ante sed elit posuere, sed placerat purus hendrerit.";
			$lipsum[] = "Nam fermentum urna nec risus consectetur egestas.";
			$lipsum[] = "Etiam commodo neque quis aliquam congue.";
			$lipsum[] = "Integer eget sem aliquet, laoreet tortor sit amet, fringilla lorem.";
			$lipsum[] = "Nulla egestas ipsum nec turpis auctor, id suscipit nulla tempor.";
			$lipsum[] = "Maecenas ac libero vestibulum, imperdiet nibh quis, tincidunt eros.";
			$lipsum[] = "In at augue ullamcorper, pellentesque arcu aliquet, ullamcorper turpis.";
			$lipsum[] = "Nullam placerat nulla a sagittis sagittis.";
			$lipsum[] = "Pellentesque sit amet urna eget nisl luctus tristique at non odio.";
			$lipsum[] = "Proin blandit magna ullamcorper luctus dictum.";
			$lipsum[] = "Morbi tincidunt justo vel felis interdum, quis pharetra libero porta.";
			$lipsum[] = "Nulla a dui consectetur, gravida ligula et, viverra purus.";
			$lipsum[] = "Praesent in purus dignissim, malesuada orci vitae, lacinia dolor.";
			$lipsum[] = "Fusce tristique dui et nisi hendrerit ultrices.";
			$lipsum[] = "Proin adipiscing quam facilisis lectus tristique, id euismod sem interdum.";
			$lipsum[] = "Sed non dolor posuere, malesuada turpis vitae, consequat lectus.";
			$lipsum[] = "Curabitur sit amet leo vehicula, luctus tortor nec, adipiscing risus.";
			$lipsum[] = "Vivamus accumsan nisl adipiscing sem sollicitudin ultricies.";
			$lipsum[] = "Quisque vel ante non ipsum malesuada egestas.";
			$lipsum[] = "Cras sit amet orci vulputate, tempus massa eget, rhoncus eros.";
			$lipsum[] = "Sed placerat est nec diam lobortis auctor.";
			$lipsum[] = "Nulla ac tortor hendrerit felis accumsan pulvinar.";
			$lipsum[] = "Vivamus porttitor leo in tincidunt hendrerit.";
			$lipsum[] = "Phasellus nec neque sed eros lobortis elementum eget quis elit.";
			$lipsum[] = "Nunc ut nisl in lacus tempus interdum.";
			$lipsum[] = "Nunc at urna eleifend, tristique diam vel, posuere odio.";
			$lipsum[] = "Maecenas blandit orci convallis mi accumsan, sed venenatis orci accumsan.";
			$lipsum[] = "Morbi vehicula mi sed lacus commodo, aliquet eleifend odio tristique.";
			$lipsum[] = "Ut euismod urna et sapien sollicitudin, sed convallis dui hendrerit.";
			$lipsum[] = "Vestibulum ut lacus vitae urna faucibus sollicitudin.";
			$lipsum[] = "Vivamus at turpis luctus metus commodo fermentum.";
			$lipsum[] = "Quisque consectetur lorem sit amet nisi porttitor, sed bibendum nunc sodales.";
			$lipsum[] = "Suspendisse vitae erat ut lorem scelerisque venenatis.";
			$lipsum[] = "Curabitur sagittis justo id orci scelerisque, non sagittis nunc tincidunt.";
			$lipsum[] = "Proin eget est eu ante venenatis iaculis.";
			$lipsum[] = "Sed id leo dictum, hendrerit urna vitae, ullamcorper felis.";
			$lipsum[] = "Nullam interdum tortor sit amet lacinia auctor.";
			$lipsum[] = "Nulla in lorem sed ipsum cursus scelerisque.";
			$lipsum[] = "Mauris sit amet tellus eget neque gravida volutpat.";
			$lipsum[] = "Nulla sed lorem dignissim, blandit ipsum ac, sollicitudin elit.";
			$lipsum[] = "Quisque vitae elit accumsan, mattis magna id, bibendum dui.";
			$lipsum[] = "Vestibulum porttitor augue a sapien rhoncus, eget blandit dui mattis.";
			$lipsum[] = "Etiam lobortis diam sit amet facilisis dictum.";
			$lipsum[] = "Fusce varius tellus eget eros varius dapibus quis eget urna.";
			$lipsum[] = "Sed interdum magna a nibh lacinia, in rhoncus ipsum porttitor.";
			$lipsum[] = "Cras in ante et erat porta luctus.";
			$lipsum[] = "Maecenas vestibulum lectus eu dui elementum, non porta magna dictum.";
			$lipsum[] = "Curabitur ornare lacus adipiscing nisl porta, quis faucibus felis vestibulum.";
			$lipsum[] = "Mauris ultrices arcu ac lacus mollis, ut lobortis libero auctor.";
			$lipsum[] = "Nullam et lacus non enim cursus varius eget vitae ante.";
			$lipsum[] = "Aliquam pharetra ligula vel lacus euismod lacinia.";
			$lipsum[] = "Quisque sed erat eu orci ultrices vestibulum.";
			$lipsum[] = "Proin ut lacus ac mi sollicitudin interdum.";
			$lipsum[] = "Phasellus commodo dolor a mi condimentum tristique.";
			$lipsum[] = "Fusce ac justo lobortis, sollicitudin nibh vitae, porttitor odio.";
			$lipsum[] = "Quisque ac turpis sit amet nibh ultricies egestas quis quis neque.";
			$lipsum[] = "Nunc non turpis eu eros tincidunt vestibulum in id purus.";
			$lipsum[] = "Pellentesque quis lacus laoreet, eleifend nisl sed, facilisis mauris.";
			$lipsum[] = "In quis velit scelerisque, tincidunt tellus vitae, scelerisque nibh.";
			$lipsum[] = "Nulla porta turpis vel est tempus dictum.";
			$lipsum[] = "Sed lacinia neque a nulla bibendum hendrerit.";
			$lipsum[] = "Etiam fermentum erat quis adipiscing ornare.";
			$lipsum[] = "Donec egestas erat eget ante suscipit, vitae iaculis neque sollicitudin.";
			$lipsum[] = "Sed faucibus risus nec pharetra euismod.";
			$lipsum[] = "In ac ligula non mauris imperdiet auctor.";
			$lipsum[] = "Nunc eleifend tortor quis blandit pellentesque.";
			$lipsum[] = "Integer id velit scelerisque lectus eleifend mattis.";
			$lipsum[] = "Proin molestie libero ac risus pellentesque mollis.";
			$lipsum[] = "Pellentesque semper nibh ac magna dapibus, eget sollicitudin risus condimentum.";
			$lipsum[] = "Integer pulvinar risus ut magna ornare, id tincidunt arcu porta.";
			$lipsum[] = "Aliquam et nibh placerat purus pellentesque semper nec quis sem.";
			$lipsum[] = "Donec ac erat non lectus sollicitudin luctus imperdiet eu velit.";
			$lipsum[] = "Integer eu ligula blandit metus facilisis viverra.";
			$lipsum[] = "Nulla a metus quis ipsum tincidunt blandit vitae sit amet turpis.";
			$lipsum[] = "Vestibulum non urna in felis lobortis vestibulum.";
			$lipsum[] = "Ut egestas ligula malesuada, fermentum dui a, ultrices enim.";
			$lipsum[] = "Vestibulum imperdiet urna vitae sapien faucibus porttitor.";
			$lipsum[] = "Vestibulum tincidunt tellus tristique arcu ornare, a fringilla purus sollicitudin.";
			$lipsum[] = "In vitae purus vitae elit congue imperdiet vel sit amet dolor.";
			$lipsum[] = "Maecenas mollis nunc ac sem placerat luctus.";
			$lipsum[] = "Vivamus facilisis purus nec justo pharetra, nec semper justo sollicitudin.";
			$lipsum[] = "Duis id odio in mi ultricies ultricies.";
			$lipsum[] = "In ullamcorper lectus in aliquet pellentesque.";
			$lipsum[] = "Duis tempor est non orci convallis rhoncus.";
			$lipsum[] = "Integer laoreet mauris a augue bibendum blandit.";
			$lipsum[] = "Suspendisse suscipit dui faucibus dui sodales convallis.";
			$lipsum[] = "Proin ut leo non arcu malesuada suscipit.";
			$lipsum[] = "Cras fringilla enim sed ante laoreet, eu dapibus mauris iaculis.";
			$lipsum[] = "Aliquam posuere nisi ac consequat venenatis.";
			$lipsum[] = "Quisque dapibus justo a urna lobortis cursus.";
			$lipsum[] = "Suspendisse nec ipsum sed metus lacinia vestibulum.";
			$lipsum[] = "Morbi lacinia mauris nec lectus rhoncus imperdiet.";
			$lipsum[] = "Nullam suscipit massa non purus hendrerit, pellentesque semper augue consequat.";
			$lipsum[] = "Quisque consectetur felis non nulla accumsan tempor.";
			$lipsum[] = "Praesent nec neque cursus, congue neque et, tempor odio.";
			$lipsum[] = "Aliquam dapibus nulla condimentum, egestas risus eget, rutrum leo.";
			$lipsum[] = "Nam varius magna at sem ultricies hendrerit.";
			$lipsum[] = "Duis accumsan nibh sed orci lobortis imperdiet.";
			$lipsum[] = "Duis ornare sem nec mi euismod posuere.";
			$lipsum[] = "Nam quis odio id purus adipiscing consequat eget ac risus.";
			$lipsum[] = "Sed dapibus nulla sollicitudin lectus viverra, eget luctus nunc vestibulum.";
			$lipsum[] = "Quisque fermentum enim nec felis scelerisque sodales.";
			$lipsum[] = "Aenean vehicula arcu in imperdiet posuere.";
			$lipsum[] = "Proin nec ligula nec libero tristique ornare.";
			$lipsum[] = "Nam eleifend lectus rutrum dui pretium commodo.";
			$lipsum[] = "Sed nec ligula id lacus gravida consequat.";
			$lipsum[] = "Cras dignissim arcu id molestie blandit.";
			$lipsum[] = "Sed et nisi ut ante auctor aliquet nec vel velit.";
			$lipsum[] = "Nunc porta ante eu tellus venenatis suscipit.";
			$lipsum[] = "Duis et justo elementum libero iaculis pulvinar.";
			$lipsum[] = "Vestibulum imperdiet neque id consectetur eleifend.";
			$lipsum[] = "Curabitur blandit ante id nibh convallis, vel euismod velit commodo.";
			$lipsum[] = "Etiam sodales orci eget sapien sodales adipiscing.";
			$lipsum[] = "Quisque id massa imperdiet ante fringilla volutpat nec porta sem.";
			$lipsum[] = "Duis euismod arcu sit amet scelerisque iaculis.";
			$lipsum[] = "Mauris pharetra sem non nulla gravida, non condimentum enim tempus.";
			$lipsum[] = "Sed blandit lorem eu velit tempus, id ornare arcu fringilla.";
			$lipsum[] = "Suspendisse scelerisque nunc pulvinar aliquam lacinia.";
			$lipsum[] = "Vivamus euismod nibh ut eros vehicula lacinia.";
			$lipsum[] = "Phasellus vel neque nec eros rutrum placerat at non neque.";
			$lipsum[] = "Nulla semper ipsum sed sapien fringilla vehicula.";
			$lipsum[] = "Vivamus iaculis dolor consequat, euismod orci in, egestas ante.";
			$lipsum[] = "Curabitur et justo dignissim, tincidunt lacus a, tristique tortor.";
			$lipsum[] = "Pellentesque eu mauris consectetur, accumsan leo in, venenatis nisi.";
			$lipsum[] = "Pellentesque dictum libero eu odio ultricies pellentesque.";
			$lipsum[] = "Aenean interdum augue sit amet justo ultrices gravida.";
			$lipsum[] = "Morbi scelerisque mi non tellus vestibulum, a rutrum tellus porttitor.";
			$lipsum[] = "Nulla cursus velit a libero molestie, ornare pharetra purus facilisis.";
			$lipsum[] = "Nam ac quam mattis, auctor erat eu, sollicitudin est.";
			$lipsum[] = "Praesent at ante quis urna aliquam tristique.";
			$lipsum[] = "Proin ullamcorper diam et semper tristique.";
			$lipsum[] = "Pellentesque accumsan enim id convallis pellentesque.";
			$lipsum[] = "Cras quis nisl mattis, pharetra metus at, mattis magna.";
			$lipsum[] = "Duis dapibus nisi ut ante tincidunt ultrices.";
			$lipsum[] = "Cras non erat egestas, vehicula orci ac, suscipit metus.";
			$lipsum[] = "Pellentesque egestas leo aliquam vehicula iaculis.";
			$lipsum[] = "Aenean viverra ante id nunc lacinia aliquam.";
			$lipsum[] = "Cras bibendum nunc et turpis facilisis, id porttitor est ultricies.";
			$lipsum[] = "Aliquam quis arcu non magna aliquam iaculis.";
			$lipsum[] = "Nullam mattis sapien commodo mauris elementum pulvinar.";
			$lipsum[] = "Pellentesque ac est consectetur, ornare lorem varius, sagittis ligula.";
			$lipsum[] = "Phasellus tincidunt dolor a sapien fringilla lacinia.";
			$lipsum[] = "Duis semper purus ac arcu ullamcorper blandit.";
			$lipsum[] = "Duis scelerisque nulla ac augue vulputate, in luctus ligula convallis.";
			$lipsum[] = "Vivamus sed mi sit amet mauris semper ullamcorper.";
			$lipsum[] = "Praesent feugiat tellus et nisi rhoncus, vel mattis quam laoreet.";
			$lipsum[] = "Sed et quam sit amet ligula porttitor placerat at eu diam.";
			$lipsum[] = "Cras id urna eu justo sodales lacinia quis at turpis.";
			$lipsum[] = "Maecenas facilisis augue et nisi ullamcorper condimentum.";
			$lipsum[] = "Nam ut leo a tellus laoreet ornare at vel massa.";
			$lipsum[] = "Sed vitae risus sit amet nisl molestie iaculis.";
			$lipsum[] = "Cras pulvinar nibh vitae tellus pharetra dapibus.";
			$lipsum[] = "Mauris varius turpis eget magna accumsan varius.";
			$lipsum[] = "Sed vitae lectus dapibus, consequat nulla sit amet, gravida arcu.";
			$lipsum[] = "Etiam et odio sit amet massa aliquam convallis.";
			$lipsum[] = "Aliquam imperdiet nibh gravida, sodales nulla a, venenatis neque.";
			$lipsum[] = "Suspendisse sed sem sed lorem sagittis auctor non non augue.";
			$lipsum[] = "Sed eget nisi at metus varius gravida ac eu mi.";
			$lipsum[] = "Nam in est in tortor pretium posuere.";
			$lipsum[] = "Nulla dictum purus in vehicula volutpat.";
			$lipsum[] = "Cras sit amet mi eu sem congue fringilla.";
			$lipsum[] = "Suspendisse nec sem non enim vehicula luctus eu eget nisi.";
			$lipsum[] = "Integer quis nibh lobortis, posuere tortor nec, semper arcu.";
			$lipsum[] = "Donec quis enim egestas, pharetra ipsum vel, rutrum turpis.";
			$lipsum[] = "Donec et turpis posuere, euismod tortor at, sagittis massa.";
			$lipsum[] = "Proin accumsan diam tempus enim placerat, vel eleifend lectus dignissim.";
			$lipsum[] = "Sed malesuada libero eget viverra tristique.";
			$lipsum[] = "Mauris a velit eget odio sollicitudin laoreet.";
			$lipsum[] = "Duis sed lectus in turpis rutrum bibendum sit amet a ante.";
			$lipsum[] = "Etiam ornare felis sit amet purus tristique, in placerat velit faucibus.";
			$lipsum[] = "Cras bibendum erat rhoncus felis ultrices posuere.";
			$lipsum[] = "Aenean nec mauris et quam interdum tristique.";
			$lipsum[] = "Nunc et velit et dolor consequat laoreet id accumsan neque.";
			$lipsum[] = "In egestas ipsum at ante suscipit, ac posuere metus elementum.";
			$lipsum[] = "Maecenas a dolor suscipit, sagittis nisl ac, rutrum tellus.";
			$lipsum[] = "Etiam porttitor tellus sed mi vestibulum, sed ornare dui tempor.";
			$lipsum[] = "Etiam venenatis massa a turpis vulputate pharetra.";
			$lipsum[] = "Ut mattis orci pharetra, suscipit augue a, faucibus felis.";
			$lipsum[] = "Duis accumsan risus quis metus dapibus bibendum.";
			$lipsum[] = "Praesent interdum est sed rutrum dapibus.";
			$lipsum[] = "Donec feugiat libero eget est eleifend, ut fermentum libero viverra.";
			$lipsum[] = "Vestibulum et odio dictum, elementum augue non, volutpat sapien.";
			$lipsum[] = "Morbi tristique nulla vitae eros pharetra blandit.";
			$lipsum[] = "Phasellus sollicitudin sapien facilisis augue dapibus varius.";
			$lipsum[] = "Phasellus suscipit urna id mauris fringilla accumsan.";
			$lipsum[] = "Sed vel odio sit amet nulla cursus laoreet ut ut ante.";
			$lipsum[] = "In ultrices magna et auctor porta.";
			$lipsum[] = "Donec a ipsum vitae metus facilisis imperdiet in nec mi.";
			$lipsum[] = "Morbi pulvinar elit a felis vulputate volutpat.";
			$lipsum[] = "Sed a nisl sed neque tristique volutpat eget eu augue.";
			$lipsum[] = "Quisque vitae metus luctus, tempus nulla at, ullamcorper lectus.";
			$lipsum[] = "Cras quis odio ac leo accumsan pharetra.";
			$lipsum[] = "Curabitur pulvinar lacus at nulla elementum, eget suscipit neque imperdiet.";
			$lipsum[] = "Vestibulum consectetur ante vel mattis venenatis.";
			$lipsum[] = "Proin vitae ipsum et nisl tristique posuere.";
			$lipsum[] = "Mauris porttitor sapien eget nibh imperdiet, sed ultrices enim porta.";
			$lipsum[] = "Duis et dui ac orci dapibus imperdiet.";
			$lipsum[] = "Ut sed nunc venenatis, interdum nisi non, tristique velit.";
			$lipsum[] = "Sed at turpis sed justo tincidunt blandit.";
			$lipsum[] = "Sed vel dolor tempus, imperdiet erat in, varius orci.";
			$lipsum[] = "Curabitur ullamcorper quam vel est imperdiet, a eleifend metus adipiscing.";
			$lipsum[] = "Fusce sagittis libero id lorem imperdiet, ac euismod ipsum venenatis.";
			$lipsum[] = "In dapibus purus ultricies, porta sapien ac, tincidunt sapien.";
			$lipsum[] = "Aliquam mollis magna elementum, dapibus leo ac, mollis metus.";
			$lipsum[] = "Nam eget mi ut risus fermentum fringilla.";
			$lipsum[] = "Quisque molestie tortor quis enim suscipit convallis.";
			$lipsum[] = "Duis sollicitudin erat vitae nibh rutrum vehicula.";
			$lipsum[] = "Nullam quis nulla pulvinar, auctor mauris ultricies, interdum felis.";
			$lipsum[] = "Pellentesque scelerisque velit sit amet massa semper, quis rutrum nunc venenatis.";
			$lipsum[] = "Mauris rhoncus leo id enim venenatis consequat.";
			$lipsum[] = "Vivamus ac tellus vitae nibh dapibus vulputate vitae quis urna.";
			$lipsum[] = "Nullam eu lorem posuere, hendrerit purus a, pellentesque leo.";
			$lipsum[] = "In accumsan nunc venenatis convallis auctor.";
			$lipsum[] = "Suspendisse ac risus nec velit faucibus pellentesque sit amet ac ipsum.";
			$lipsum[] = "Pellentesque dignissim sapien nec sagittis commodo.";
			$lipsum[] = "Vivamus ut lectus vitae nulla pharetra accumsan sed eget nisi.";
			$lipsum[] = "Vestibulum non enim blandit, eleifend neque vel, convallis tellus.";
			$lipsum[] = "Nam vel massa eleifend, pellentesque turpis a, ullamcorper erat.";
			$lipsum[] = "Nunc non justo ultricies, tristique libero vitae, ornare odio.";
			$lipsum[] = "Phasellus aliquam ipsum ut aliquet dapibus.";
			$lipsum[] = "Praesent a sapien nec enim rutrum ultrices in adipiscing augue.";
			$lipsum[] = "Duis at mauris et turpis convallis gravida sit amet quis lectus.";
			$lipsum[] = "Fusce ac neque congue urna tristique scelerisque.";
			$lipsum[] = "Mauris tempus risus sed dolor euismod, id suscipit ante tincidunt.";
			$lipsum[] = "Nullam vel lectus tincidunt, iaculis mi eget, rhoncus velit.";
			$lipsum[] = "Fusce eget dolor congue, ultrices tortor at, fermentum risus.";
			$lipsum[] = "Vestibulum dictum nunc ut mi semper, non suscipit mauris sollicitudin.";
			$lipsum[] = "Aliquam at orci ultrices, vehicula erat eget, ornare urna.";
			$lipsum[] = "Vestibulum cursus ligula sed augue tempus, dictum interdum mi tempor.";
			$lipsum[] = "Donec semper arcu eget porta accumsan.";
			$lipsum[] = "In vitae diam ac risus sodales sagittis vel sed tortor.";
			$lipsum[] = "Donec laoreet augue vel neque tincidunt, porttitor rutrum dolor condimentum.";
			$lipsum[] = "Cras sed neque sed lectus aliquam viverra vestibulum in ligula.";
			$lipsum[] = "Sed in nulla in nibh sollicitudin consectetur sed vel mauris.";
			$lipsum[] = "Sed eu est ut diam aliquam eleifend sed eget neque.";
			$lipsum[] = "Mauris vel nisi feugiat, condimentum est vel, eleifend diam.";
			$lipsum[] = "Etiam tristique leo at arcu tempus, in dapibus dui tempor.";
			$lipsum[] = "Proin quis magna nec nisl lacinia vulputate.";
			$lipsum[] = "Phasellus nec neque consequat, commodo nulla sit amet, pellentesque enim.";
			$lipsum[] = "Praesent ut est nec eros pharetra ultrices vitae vitae nisi.";
			$lipsum[] = "Suspendisse varius metus nec massa malesuada laoreet.";
			$lipsum[] = "Fusce at orci vel orci suscipit commodo quis ac sem.";
			$lipsum[] = "Aenean tristique lacus non vestibulum hendrerit.";
			$lipsum[] = "Proin in tortor vehicula, porttitor elit at, egestas mi.";
			$lipsum[] = "Nullam et odio sit amet libero consectetur congue ac vitae tortor.";
			$lipsum[] = "Donec pretium mi eget orci convallis faucibus.";
			$lipsum[] = "Nulla ac dolor ultricies ligula vehicula consequat et et lectus.";
			$lipsum[] = "Quisque eget libero vel enim volutpat vulputate non quis nulla.";
			$lipsum[] = "Nam vel libero id dui pellentesque fermentum.";
			$lipsum[] = "In ut enim vel mi sodales facilisis non id ipsum.";
			$lipsum[] = "Maecenas consequat mi quis erat vehicula, vel egestas diam condimentum.";
			$lipsum[] = "Praesent sed lectus vitae libero accumsan mollis.";
			$lipsum[] = "Pellentesque et sem interdum, pulvinar ligula quis, suscipit erat.";
			$lipsum[] = "Etiam eu dolor eget arcu sodales posuere sit amet in dolor.";
			$lipsum[] = "Curabitur imperdiet nisi a arcu bibendum dapibus.";
			$lipsum[] = "Suspendisse non justo nec est aliquet semper.";
			$lipsum[] = "Sed tristique urna non vehicula imperdiet.";
			$lipsum[] = "Phasellus eu mi quis nibh consectetur auctor.";
			$lipsum[] = "Sed mattis augue ut blandit eleifend.";
			$lipsum[] = "Maecenas ultricies ante quis sodales accumsan.";
			$lipsum[] = "Donec quis dolor congue, consequat nibh ac, auctor nibh.";
			$lipsum[] = "Vestibulum fermentum risus et mollis ullamcorper.";
			$lipsum[] = "Duis varius felis nec lectus ornare ultricies.";
			$lipsum[] = "Morbi non leo dictum metus lobortis hendrerit sit amet in nunc.";
			$lipsum[] = "Nunc sit amet arcu ultricies, sodales arcu sit amet, facilisis tortor.";
			$lipsum[] = "Pellentesque bibendum mauris at diam pretium, sit amet tempor tortor ultricies.";
			$lipsum[] = "Nullam et mi aliquam, pellentesque neque aliquam, tincidunt diam.";
			$lipsum[] = "Fusce egestas sem ac vulputate consectetur.";
			$lipsum[] = "Morbi vel nunc id mauris mollis congue sed vitae lacus.";
			$lipsum[] = "Proin ac mi sit amet mi tincidunt sagittis.";
			$lipsum[] = "Ut hendrerit neque ut tellus lobortis, at volutpat ipsum tincidunt.";
			$lipsum[] = "Vestibulum sit amet orci ac nulla blandit vestibulum.";
			$lipsum[] = "Aenean at dolor aliquet nisl feugiat dignissim.";
			$lipsum[] = "Suspendisse vitae ipsum tempus, accumsan ante vel, venenatis erat.";
			$lipsum[] = "Integer suscipit risus sed nisi sodales imperdiet eu id est.";
			$lipsum[] = "Aliquam sagittis leo eu sodales tristique.";
			$lipsum[] = "Nullam ullamcorper lacus at dui consequat, vitae molestie orci dignissim.";
			$lipsum[] = "Nam scelerisque ligula condimentum nulla tincidunt, vel pharetra metus euismod.";
			$lipsum[] = "Proin blandit augue sit amet nunc sollicitudin iaculis.";
			$lipsum[] = "Mauris vitae quam vel dui facilisis consequat eget vitae nibh.";
			$lipsum[] = "Praesent id sem at risus adipiscing feugiat.";
			$lipsum[] = "Nullam sodales purus id velit pharetra commodo.";
			$lipsum[] = "Nullam ornare tellus vitae accumsan suscipit.";
			$lipsum[] = "Phasellus molestie sapien at felis blandit placerat.";
			$lipsum[] = "Nunc nec ligula sit amet nibh commodo sagittis et ac tellus.";
			$lipsum[] = "Integer tempor leo eget tellus fermentum sollicitudin.";
			$lipsum[] = "Integer rutrum neque et massa aliquet ultricies.";
			$lipsum[] = "Etiam eget purus suscipit, ullamcorper orci eget, posuere purus.";
			$lipsum[] = "Nullam accumsan lacus sit amet varius faucibus.";
			$lipsum[] = "Proin viverra dui a quam sagittis, vitae condimentum dolor consectetur.";
			$lipsum[] = "Donec quis nunc vitae nibh sagittis lacinia.";
			$lipsum[] = "Aenean blandit urna vel lectus fermentum facilisis eu ut metus.";
			$lipsum[] = "Morbi non odio sed mauris euismod pulvinar quis quis nisl.";
			$lipsum[] = "Proin vehicula turpis eu arcu venenatis condimentum.";
			$lipsum[] = "Morbi sit amet eros id lectus ultricies euismod.";
			$lipsum[] = "Nunc eu mauris ac felis sodales lacinia.";
			$lipsum[] = "Cras at nisi eget mauris dictum vehicula eu non massa.";
			$lipsum[] = "Donec porta orci vestibulum commodo ullamcorper.";
			$lipsum[] = "Nunc vestibulum tortor volutpat orci tincidunt suscipit eget quis lorem.";
			$lipsum[] = "Ut nec massa a nulla tempor volutpat sed et purus.";
			$lipsum[] = "Vestibulum ut massa dapibus, aliquet magna lacinia, volutpat tellus.";
			$lipsum[] = "Nam pretium metus nec lacus auctor, in lacinia lorem ullamcorper.";
			$lipsum[] = "Integer congue sapien id nibh mattis interdum.";
			$lipsum[] = "Vestibulum sit amet urna scelerisque, rutrum nisl vitae, aliquet lectus.";
			$lipsum[] = "Nulla sed lectus ac tellus rhoncus pellentesque.";
			$lipsum[] = "Praesent lacinia ante vitae elementum adipiscing.";
			$lipsum[] = "Nunc aliquet lorem eu luctus dictum.";
			$lipsum[] = "Mauris elementum sem ac condimentum tempor.";
			$lipsum[] = "Ut et neque posuere, malesuada dui quis, pulvinar metus.";
			$lipsum[] = "Nam posuere turpis ut semper ullamcorper.";
			$lipsum[] = "Cras scelerisque odio non vehicula porta.";
			$lipsum[] = "Mauris non dui eget justo bibendum imperdiet ut non velit.";
			$lipsum[] = "Suspendisse sed ipsum tincidunt, mattis turpis quis, blandit nulla.";
			$lipsum[] = "Maecenas ac tortor eu est porttitor sodales in id odio.";
			$lipsum[] = "Suspendisse id ante mattis, facilisis felis a, scelerisque est.";
			$lipsum[] = "Fusce tristique est non ligula iaculis, et accumsan neque euismod.";
			$lipsum[] = "Phasellus non lacus convallis, ultrices purus eget, molestie risus.";
			$lipsum[] = "Nunc vitae metus nec nulla iaculis dictum nec in nunc.";
			$lipsum[] = "Pellentesque ac sapien luctus est pellentesque cursus sed vitae lectus.";
			$lipsum[] = "Pellentesque consectetur mauris sed est porttitor, at imperdiet erat laoreet.";
			$lipsum[] = "Ut molestie urna ut nulla hendrerit, in dictum odio consectetur.";
			$lipsum[] = "Nulla euismod lorem eu elit ornare, ac consectetur sem elementum.";
			$lipsum[] = "Vivamus placerat odio non sem cursus, at sodales tortor tincidunt.";
			$lipsum[] = "Ut eu velit in nisl accumsan tempus.";
			$lipsum[] = "Maecenas a massa eu turpis suscipit sollicitudin vitae et tellus.";
			$lipsum[] = "Maecenas eu ipsum in purus bibendum placerat tincidunt molestie ipsum.";
			$lipsum[] = "Phasellus fermentum tellus vitae nisi adipiscing elementum.";
			$lipsum[] = "Aliquam lobortis elit laoreet, pharetra quam ut, fermentum nisl.";
			$lipsum[] = "Sed ac sapien in sapien aliquet eleifend.";
			$lipsum[] = "Cras at eros iaculis, euismod nunc vitae, cursus erat.";
			$lipsum[] = "Aenean mattis nisl et sem adipiscing, at aliquam est dictum.";


			$output = '';

			if ( $paragraphs > 0 || $words > 0 || $bytes > 0 ) {
				$content = '';

				if ( $words > 0 ) $paragraphs = ceil( $words * .5 ); //pull off enough words to use later
				if ( $bytes > 0 ) $paragraphs = ceil( $bytes * .5 ); //pull off enough bytes to use later

				for ( $i=0; $i < $paragraphs; $i++ ) {
					$sentences = rand( 3, 12 );

					$content .= '<p>';

					for ( $j=0; $j < $sentences; $j++ ) {
						if ( $content === '<p>' && $start_with_lorem ) {
							$content .= $lipsum[ 0 ] . ' ';
						} else {
							$content .= $lipsum[ rand( 0, count( $lipsum ) - 1 ) ] . ' ';
						}
					}

					$content .= '</p>';
				}

				if ( $words > 0 ) {
					$word_list = explode( ' ', $content );

					for ( $i=0; $i < $words; $i++ ) {
						$output .= $word_list[ $i ] . ' ';
					}

					if ( substr_compare( $output, '</p>', -strlen( '</p>' ), strlen( '</p>' ) ) !== 0 ) {
						$output = trim( $output ) . '.</p>';
					}

					if ( $is_title ) {
						$output = str_replace( array( "<p>", "</p>", "." ), '', $output );
						$output = rtrim( $output, ',' );
						$output = ucwords( $output );
					}
				} else if ( $bytes > 0 ) {
						$output = str_replace( array( "<p>", "</p>", "." ), '', $content );
						$output = substr( $output, 0, $bytes );

						if ( $is_title ) {
							$output = str_replace( array( "<p>", "</p>", "." ), '', $output );
							$output = rtrim( $output, ',' );
							$output = ucwords( $output );
						} else {
							$output = '<p>' . $output . '</p>';
						}
					} else {
					$output = $content;
				}
			} else if ( $list > 0 ) {
					$start_index = 0;

					$output .= '<ul>';

					if ( $start_with_lorem ) {
						$start_index = 0;
					} else {
						$start_index = rand( 0, count( $lipsum ) - 1 );
					}

					for ( $i=0; $i < $list; $i++ ) {
						$output .= '<li>' . $lipsum[ $start_index ] . '</li>';

						$start_index = rand( 0, count( $lipsum ) - 1 );
					}

					$output .= '</ul>';
				}

			if ( $paragraph_separator == 'br' ) {
				$output = str_replace( array( "</p><p>" ), "<br /><br />", $output );
				$output = str_replace( array( "<p>" ), "", $output );
			} else if ( $paragraph_separator == 'span' ) {
				$output = str_replace( array( "<p>" ), "<span>", $output );
				$output = str_replace( array( "</p>" ), "</span>", $output );				
			}
		} else if ( $type == 'image' ) {
				$output = '<img src="http://lorempixel.com/' .
					( $grayscale ? 'g/' : '' ) . $width . '/' . $height . '/' . $category . '" ' .
					( !empty( $align ) ? ' align="' . $align . '" style="padding: 15px;"' : '' ) . ' />';
			}

		return $output;
	}

	function render_dialog_script() {
	?>
		<script>
		jQuery(function($) {
			$('.imagesettings').hide();

			$('#type').change(function() {
				$('.imagesettings').hide();
				$('.textsettings').hide();

				if ( $(this).val() === 'text' ) {
					$('.textsettings').show();
				} else {
					$('.imagesettings').show();
				}
			});
		});
	</script>
	<?php

	}
}
