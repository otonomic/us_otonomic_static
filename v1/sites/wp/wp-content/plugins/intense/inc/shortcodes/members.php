<?php

class Intense_Members extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;
		global $post;

		$this->title = __( 'Members', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-lock';
		$this->preview_content = __( "Member Only Content", 'intense' );
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'text' => array(
				'type' => 'textarea',
				'title' => __( 'Text', 'intense' ),
				'default' => __('This content is for registered users only', 'intense'),
				'skinnable' => '0'
			),
			'background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Background Color', 'intense' ),
				'default' => ''
			),
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'default' => '#333333'
			),
			'login_title' => array(
				'type' => 'text',
				'title' => __( 'Login Title', 'intense' ),
				'default' => __('Login', 'intense'),
				'skinnable' => '0'
			),
			'login_url' => array(
				'type' => 'text',
				'title' => __( 'Login URL', 'intense' ),
				'description' => __( 'leave blank to use the default WordPress login url with a redirect back to this page/post', 'intense' ),
			),
			'login_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Login Button Color', 'intense' ),
				'default' => 'primary'
			),					
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_feed() ) return;

		if ( !is_user_logged_in() ) {
			if ( empty( $login_url ) ) {
				$login_url = wp_login_url( get_permalink() );
			}

			$output = intense_run_shortcode( 'intense_promo_box', array( 
				'button_color' => $login_color,
				'background_color' => $background_color,
				'color' => $font_color,
				'size' => 'small',
				'button_link' => $login_url,
				'button_text' => $login_title,
				'button_size' => 'default'
			), $text );

			return '<div class="intense members">' . $output . '</div><style>.intense.members a.promo { margin: 0 !important; }</style>';
		} else {
			return do_shortcode( $content );
		}		
	}
}
