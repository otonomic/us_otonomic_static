<?php

class Intense_Timeline extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Timeline', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-clock';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			// 'lightbox' => array(
			// 	'type' => 'checkbox',
			// 	'title' => __( 'Horizontal', 'intense' ),
			// 	'default' => "1",
			// ),
			'column_mode' => array(
			    'type' => 'dropdown',
			    'title' => __( 'Column Mode', 'intense' ),
			    'default' => 'dual',
			    'options' => array(
			        'dual' => __( 'Dual', 'intense' ),
			        'left' => __( 'Left', 'intense' ),
			        'right' => __( 'Right', 'intense' ),
			        'center' => __( 'Center', 'intense' ),
			    )
			),
			'animation' => array(
				'type' => 'checkbox',
				'title' => __( 'Animation', 'intense' ),
				'default' => "1",
			),
            'animation_type' => array(
            	'type' => 'animation',
                'title' => __( 'Animation Type', 'intense' ),			
				'placeholder' => __( 'Select an animation', 'intense' ),
				'default' => 'fadeInUp',
				'class' => 'showAnimation',
            ),
			'show_year' => array(
				'type' => 'checkbox',
				'title' => __( 'Show Year', 'intense' ),
				'default' => "1",
			),
			'timeline_event' => array(
                'type' => 'repeater',
                'title' => __( 'Event', 'intense' ),
                'preview_content' => __( '', 'intense' ),
                'fields' => array(
					'type' => array(
					    'type' => 'dropdown',
					    'title' => __( 'Type', 'intense' ),
					    'default' => 'blog_post',
					    'options' => array(
					        'blog_post' => __( 'Post', 'intense' ),
					        'slider' => __( 'Slider', 'intense' ),
					        'custom' => __( 'Custom', 'intense' ),
					    ),			    
						'description' => __( 'Custom uses content you enter into the editor.', 'intense' ),
					),
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
					),
					'date' => array(
						'type' => 'text',
						'title' => __( 'Date', 'intense' ),
						'description' => __( 'Must be in YYYY-mm-dd format.', 'intense' ),
                        'skinnable' => '0'
					),
					'use_content' => array(
						'type' => 'checkbox',
						'title' => __( 'Use Content', 'intense' ),
						'description' => __( 'Use inner content instead of using this layout.', 'intense' ),
						'default' => '0',
						'class' => 'usecontent'
					),
					'images' => array(
					    'type' => 'gallery',
					    'title' => __( 'Image(s)', 'intense' ),
					    'description' => __( 'enter WordPress attachment ID(s)', 'intense' ),
                        'skinnable' => '0'
					),
					'image_size' => array(
					    'type' => 'image_size',
					    'title' => __( 'Image Size', 'intense' ),
					    'default' => 'medium800'
					),
		            'image_border_radius' => array(
		                'type' => 'border_radius',
		                'title' => __( 'Image Border Radius', 'intense' ), 
		                'description' => __( '%, em, px - examples: 25% or 10em or 20px', 'intense' )               
		            ),
		            'color' => array(
		                'type' => 'color_advanced',
		                'title' => __( 'Background Color', 'intense' ),
		                'default' => '#efefef'
		            ),
		            'event_border_radius' => array(
		                'type' => 'border_radius',
		                'title' => __( 'Event Border Radius', 'intense' ), 
		                'description' => __( '%, em, px - examples: 25% or 10em or 20px', 'intense' )               
		            ),
					'readmore_text' => array(
						'type' => 'text',
						'title' => __( '"Read More" text', 'intense' ),
						'description' => __( 'Must be in YYYY-mm-dd format.', 'intense' ),
					),
					'readmore' => array(
						'type' => 'text',
						'title' => __( 'Read More URL', 'intense' ),
						'description' => __( 'Link to post or URL of your choice.', 'intense' ),
					),
					'readmore_target' => array(
					    'type' => 'link_target',
					    'title' => __( 'Read More Target', 'intense' ),
					    'default' => '_self',					    		    
						'description' => __( 'Link to post or URL of your choice.', 'intense' ),
					),
					'featured' => array(
						'type' => 'checkbox',
						'title' => __( 'Featured', 'intense' ),
						'default' => "1",
					),
		            'animation_type' => array(
		            	'type' => 'animation',
		                'title' => __( 'Animation Type', 'intense' ),			
						'placeholder' => __( 'Select an animation', 'intense' ),
		            ),
		            'animation_trigger' => array(
		            	'type' => 'dropdown',
		                'title' => __( 'Animation Trigger', 'intense' ),
		                'default' => 'scroll',
						'options' => array(
							'scroll' => __( 'Scroll', 'intense' ),
							'hover' => __( 'Hover', 'intense' ),
							'click' => __( 'Click', 'intense' ),
							'delay' => __( 'Delay', 'intense' )
						),			
		            ),
		            'animation_scroll_percent' => array(
		            	'type' => 'dropdown',
		                'title' => __( 'Scroll Percentage', 'intense' ),
						'description' => __( 'animate when <span id="scrollpercentagevalue"></span>% of item is in view', 'intense' ),
						'options' => array(
							'0' => '0',
							'10' => '10',
							'20' => '20',
							'30' => '30',
							'40' => '40',
							'50' => '50',
							'60' => '60',
							'70' => '70',
							'80' => '80',
							'90' => '90',
							'100' => '100'
						),
						'default' => '10',
						'class' => 'scrollpercentagesettings'           
		            ),
		            'animation_delay' => array(
		            	'type' => 'text',
		                'title' => __( 'Animation Delay', 'intense' ),
						'description' => __( 'amount in milliseconds', 'intense' ),
						'default' => '0'
		            ), 
                )
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;		
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $showyear;
		global $event_number;
		global $currentYear;

		$output = '';

		$mode = 'timeline';

		//TODO: allow the user to choose which animation is used.
		intense_load_animation_CSS( $animation_type );

		$showyear = $show_year;

		$currentYear = '';
		$event_number = 0;

		$random = rand();
		$timeline_events = do_shortcode( $content );
		$timeline_events = str_replace( '<br />', '', $timeline_events );
		$timeline_events = str_replace( '<p>', '', $timeline_events );
		$timeline_events = str_replace( '</p>', '', $timeline_events );

		if ( strlen( $timeline_events ) > 0 )
			$timeline_events = substr( trim( $timeline_events ), 0, -1 );
		
		if ( $column_mode == 'left' ) {
			$mode = 'timeline_left';
		} elseif ( $column_mode == 'right' ) {
			$mode = 'timeline_right';
		} elseif ( $column_mode == 'center' ) {
			$mode = 'timeline_center';
		}

		$output .= "<ul id='timeline" . $random . "' class='intense " . $mode . "' style='display: none;'>" . $timeline_events . "</ul>";
		$output .= "<script> jQuery(function($){";
		$output .= " $(document).ready(function() {";	
		$output .= "	$('#timeline" . $random . "').show().addClass('animated').addClass('" . $animation_type . "');";

		if ( $column_mode == 'dual' ) {
			$output .= "    var isIE8 = $('#intense-browser-check').hasClass('ie8');";	
			$output .= "	if (isIE8) { var float = 'right';";
			$output .= "		$('#timeline" . $random . " > li').each(function() {
									var element = $(this);

									if (element.hasClass('year')) {
										float = 'right';
										element.css('clear', 'both');
									} else {
										element.css('float', float);

										if (float == 'left') {
											element.css('clear', 'left');
										}
									}							

									if (float == 'left') {
										float = 'right';
									} else {
										float = 'left';
									}
								});";
			$output .= "	}";
		}
		$output .= " })";
		$output .= "})";
		$output .= "</script>";

		return $output;
	}
}

class Intense_Timeline_Event extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Timeline Event', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-clock';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_timeline';
		$this->is_container = true;

		// 'use_content' => 0, //options: 1,0 - will use inner content instead of using this layout

		$this->fields = array(
			'type' => array(
			    'type' => 'dropdown',
			    'title' => __( 'Type', 'intense' ),
			    'default' => 'blog_post',
			    'options' => array(
			        'blog_post' => __( 'Post', 'intense' ),
			        'slider' => __( 'Slider', 'intense' ),
			        'custom' => __( 'Custom', 'intense' ),
			    ),			    
				'description' => __( 'Custom uses content you enter into the editor.', 'intense' ),
			),
			'title' => array(
				'type' => 'text',
				'title' => __( 'Title', 'intense' ),
                'skinnable' => '0'
			),
			'date' => array(
				'type' => 'text',
				'title' => __( 'Date', 'intense' ),
				'description' => __( 'Must be in YYYY-mm-dd format.', 'intense' ),
                'skinnable' => '0'
			),
			'use_content' => array(
				'type' => 'checkbox',
				'title' => __( 'Use Content', 'intense' ),
				'description' => __( 'Use inner content instead of using this layout.', 'intense' ),
				'default' => '0',
				'class' => 'usecontent'
			),
			'images' => array(
			    'type' => 'gallery',
			    'title' => __( 'Image(s)', 'intense' ),
			    'description' => __( 'enter WordPress attachment ID(s)', 'intense' ),
                'skinnable' => '0'
			),
			'image_size' => array(
			    'type' => 'image_size',
			    'title' => __( 'Image Size', 'intense' ),
			    'default' => 'medium800'
			),
            'image_border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Image Border Radius', 'intense' ), 
                'description' => __( '%, em, px - examples: 25% or 10em or 20px', 'intense' )               
            ),
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Background Color', 'intense' ),
                'default' => '#efefef'
            ),
            'event_border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Event Border Radius', 'intense' ), 
                'description' => __( '%, em, px - examples: 25% or 10em or 20px', 'intense' )               
            ),
			'readmore_text' => array(
				'type' => 'text',
				'title' => __( '"Read More" text', 'intense' ),
				'description' => __( 'Must be in YYYY-mm-dd format.', 'intense' ),
			),
			'readmore' => array(
				'type' => 'text',
				'title' => __( 'Read More URL', 'intense' ),
				'description' => __( 'Link to post or URL of your choice.', 'intense' ),
			),
			'readmore_target' => array(
			    'type' => 'link_target',
			    'title' => __( 'Read More Target', 'intense' ),
			    'default' => '_self',			    		    
				'description' => __( 'Link to post or URL of your choice.', 'intense' ),
			),
			'featured' => array(
				'type' => 'checkbox',
				'title' => __( 'Featured', 'intense' ),
				'default' => "1",
			),
            'animation_type' => array(
            	'type' => 'animation',
                'title' => __( 'Animation Type', 'intense' ),			
				'placeholder' => __( 'Select an animation', 'intense' ),
            ),
            'animation_trigger' => array(
            	'type' => 'dropdown',
                'title' => __( 'Animation Trigger', 'intense' ),
                'default' => 'scroll',
				'options' => array(
					'scroll' => __( 'Scroll', 'intense' ),
					'hover' => __( 'Hover', 'intense' ),
					'click' => __( 'Click', 'intense' ),
					'delay' => __( 'Delay', 'intense' )
				),			
            ),
            'animation_scroll_percent' => array(
            	'type' => 'dropdown',
                'title' => __( 'Scroll Percentage', 'intense' ),
				'description' => __( 'animate when <span id="scrollpercentagevalue"></span>% of item is in view', 'intense' ),
				'options' => array(
					'0' => '0',
					'10' => '10',
					'20' => '20',
					'30' => '30',
					'40' => '40',
					'50' => '50',
					'60' => '60',
					'70' => '70',
					'80' => '80',
					'90' => '90',
					'100' => '100'
				),
				'default' => '10',
				'class' => 'scrollpercentagesettings'           
            ),
            'animation_delay' => array(
            	'type' => 'text',
                'title' => __( 'Animation Delay', 'intense' ),
				'description' => __( 'amount in milliseconds', 'intense' ),
				'default' => '0'
            ),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';
		global $currentYear;
		global $showyear;
		global $event_number;
		global $intense_visions_options;
		global $previous_featured;

		$radius = '';
	    if ( isset( $event_border_radius ) ) {
	        $radius = ' border-radius:' . $event_border_radius . ' !important;';
	    }

		if ( isset( $images ) && strpos( $images, "," ) > 0 ) {
			$slider = "[intense_slider]";
			$images = preg_split("/[,]/", $images);

			foreach ( $images as $image ) {
				$slider .= "[intense_slide][intense_image imageid='" . trim( $image ) . "' border_radius='" . $image_border_radius . "' size='" . $image_size . "'][/intense_slide]";
			}

			$slider .= "[/intense_slider]";

			$image = do_shortcode( $slider );
		} elseif ( isset( $images ) ) {
			$image = do_shortcode( "[intense_image imageid='" . trim( $images ) . "' border_radius='" . $image_border_radius . "' size='" . $image_size . "']" );
		} else {
			$image = '';
		}

		if ( !empty( $animation_type ) ) {
			$animation_wrapper_start = intense_get_animation_wrapper_start( $animation_type, $animation_trigger, $animation_scroll_percent, $animation_delay );
			$animation_wrapper_end = intense_get_animation_wrapper_end();
		} else {
			$animation_wrapper_start = null; 
			$animation_wrapper_end = null;
		}

		$eventYear = date( 'Y', strtotime( $date ) );
		$colorset = "color" . rand();
		$featured_class = ( $featured ? 'featured ' : '' );

		$output .= $animation_wrapper_start;

		if ( $previous_featured && $event_number % 2 != 0 && !$featured ) {
			$output .= "<li class='intense event' style='visibility: hidden;'></li>";
			$event_number++;
		}

		if ( $featured ) {
			$previous_featured = 1;
		} else {
			$previous_featured = 0;
		}

		if ( ( $currentYear == '' || $currentYear != $eventYear ) ) {
			$currentYear = $eventYear;

			if ( $showyear ) {
				$output .= "<li class='intense intense_post year'>" . $currentYear . "</li>";
			}

			if ( $showyear || $event_number == 0 ) {
				if ( !$showyear ) {
					$output .= "<li></li>";
				}
				$output .= "<li class='intense intense_post " . $colorset . " event offset-first $featured_class" . ( $rtl ? "rtl " : "" ) . "'>";
			} else {
				$output .= "<li class='intense intense_post " . $colorset . " event $featured_class'>";
			}

			$output .= "<style>." . $colorset . " { background: " . $color . " !important; " . $radius . " } ." . $colorset . ":hover { background: " . intense_adjust_color_brightness( $color, -20 ) . " !important; }</style>";
		} else {
			$output .= "<li class='intense intense_post " . $colorset . " event $featured_class'>";			
			$output .= "<style>." . $colorset . " { background: " . $color . " !important; " . $radius . " } ." . $colorset . ":hover { background: " . intense_adjust_color_brightness( $color, -20 ) . " !important; }</style>";
		}

		if ($type == 'blog_post') {
			if ( $use_content ) {
				$output .= trim( do_shortcode( $content ) );
			} else {
				$post_shortcode = "[intense_row padding_top='5']";
				$post_shortcode .= "[intense_column size='6']<div class='title'><h3 class='title'>" . $title . "</h3></div>[/intense_column][intense_column size='6']<div class='date'>" . date( 'F j, Y', strtotime( $date ) ) . "</div>[/intense_column]";
				$post_shortcode .= "[/intense_row]";
				$post_shortcode .= "<div>";
				$post_shortcode .= $image;
				$post_shortcode .= "</div>";
				$post_shortcode .= "<div class='content'>" . trim( do_shortcode( $content ) ) . "</div><div style='height:25px;'><a class='pull-right' href='" . $readmore . "' target='" . $readmore_target . "'>" . $readmore_text . "</a></div>";	
				$output .= do_shortcode( $post_shortcode );	
			}	
		}

		if ($type == 'custom') {
			$output .= trim( do_shortcode( $content ) );
		}

		if ( $type == 'slider' ) {
			if ( $title != '' ) {
				$title_shortcode = "[intense_row padding_top='5']";
				$title_shortcode .= "[intense_column size='12']<div class='title'><h3>" . $title . "</h3></div>[/intense_column]";
				$title_shortcode .= "[/intense_row]";
				$output .= do_shortcode( $title_shortcode );
			}

			$output .= "<div>";
			$output .= $image;
			$output .= "</div>";
		}

		$output .= "</li>";
		$output .= $animation_wrapper_end;

		$event_number++;

		return $output;

		return $output;
	}

     function render_dialog_script() {
?>
        <script>
            jQuery(function($) {
		  		$('.upload_images_button' + number).live('click', function( event ){
				    event.preventDefault();
			
				    window.parent.loadGalleryFrame($(this), function(attachments) {
				    	$.each(attachments, function (i, attachment) {
				    		var ids = jQuery("#images" + number).val() + ',' + attachment.id;

				    		if (jQuery("#images" + number).val().length == 0) {
			    				ids = attachment.id;
				    		}

				    		jQuery("#images" + number).val(ids);
				    	});
				    });
				});

				$("#type").change(function() {
					var type = $(this).val();

					switch(type) {
						case "blog_post":
							$(".usecontent").show();
							break;
						default:
							$(".usecontent").hide();
							break;
					}
				});
            });
        </script>
    <?php
    }

}

