<?php

class Intense_Map extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Map', 'intense' );
        $this->category = __( 'Media', 'intense' );
        $this->icon = 'dashicons-intense-map';
        //$this->show_preview = true;
        $this->is_container = true;

        $this->fields = array(
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
                    'type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Type', 'intense' ),
                        'default' => 'Roadmap',
                        'options' => array(
                            'Roadmap' => __( 'Roadmap', 'intense' ),
                            'Satellite' => __( 'Satellite', 'intense' ),
                            'Hybrid' => __( 'Hybrid', 'intense' ),
                        )
                    ),
                    'width' => array(
                        'type' => 'text',
                        'title' => __( 'Width', 'intense' ),
                        'description' => __( 'example: 100% or 300px', 'intense' ),
                        'default' => '100%'
                    ),
                    'height' => array(
                        'type' => 'text',
                        'title' => __( 'Height', 'intense' ),
                        'description' => __( 'example: 100% or 300px', 'intense' ),
                        'default' => '300px'
                    ),
                    'zoom' => array(
                        'type' => 'text',
                        'title' => __( 'Zoom', 'intense' ),
                        'description' => __( 'Enter a number from 0 - 19 (0 = world, 19 = street)', 'intense' ),
                        'default' => '10'
                    ),
                    'fit_bounds' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Fit Bounds', 'intense' ),
                        'description' => __( 'Fit map bounds to show all markers - zoom level will change', 'intense' ),
                        'default' => '0',
                    ),
                    'shadow' => array(
                        'type' => 'shadow',
                        'title' => __( 'Shadow', 'intense' ),
                    ),
                    'address' => array(
                        'type' => 'text',
                        'title' => __( 'Address', 'intense' ),
                        'description' => 'Only add an address or latitude and longitude coordinates if you want a map to display with no markers. This will set the center of the map.',
                        'default' => ''
                    ),
                    'latitude' => array(
                        'type' => 'text',
                        'title' => __( 'Latitude', 'intense' ),
                        'description' => '',
                        'default' => ''
                    ),
                    'longitude' => array(
                        'type' => 'text',
                        'title' => __( 'Longitude', 'intense' ),
                        'description' => '',
                        'default' => ''
                    ),
                )
            ),
            'controls_tab' => array(
                'type' => 'tab',
                'title' => __( 'Controls', 'intense' ),
                'fields' =>  array(
                    'scroll_wheel' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Zoom with Scroll Wheel', 'intense' ),
                        'default' => "0",
                    ),
                    'scale_control' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Scale Control', 'intense' ),
                        'default' => "0",
                    ),
                    'street_view' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Street View', 'intense' ),
                        'default' => "1",
                    ),
                    'overview_map' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Overview Map', 'intense' ),
                        'default' => "0",
                    ),
                    'pan_control' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Pan Control', 'intense' ),
                        'default' => "0",
                    ),
                    'map_type_control' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Map Type Control', 'intense' ),
                        'default' => "1",
                    ),
                    'map_type_style' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Map Type Control Style', 'intense' ),
                        'default' => 'DEFAULT',
                        'options' => array(
                            'DEFAULT' => __( 'Auto', 'intense' ),
                            'DROPDOWN_MENU' => __( 'Dropdown Menu', 'intense' ),
                            'HORIZONTAL_BAR' => __( 'Horizontal Bar', 'intense' ),
                        )
                    ),
                    'zoom_control' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Zoom Control', 'intense' ),
                        'default' => "1",
                    ),
                    'zoom_control_style' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Zoom Control Style', 'intense' ),
                        'default' => 'DEFAULT',
                        'options' => array(
                            'DEFAULT' => __( 'Auto', 'intense' ),
                            'LARGE' => __( 'Large', 'intense' ),
                            'SMALL' => __( 'Small', 'intense' ),
                        )
                    ),
                )
            ),
            'layers_tab' => array(
                'type' => 'tab',
                'title' => __( 'Layers', 'intense' ),
                'fields' =>  array(
                    'traffic_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Traffic Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'transit_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Transit Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'bicycle_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Bicycle Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'cloud_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Cloud Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'weather_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Weather Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'temperature_unit' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Temperature Unit', 'intense' ),
                        'default' => 'FAHRENHEIT',
                        'options' => array(
                            'FAHRENHEIT' => __( 'Fahrenheit', 'intense' ),
                            'CELSIUS' => __( 'Celsius', 'intense' ),
                        )
                    ),
                    'panoramio_layer' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Panoramio Layer', 'intense' ),
                        'default' => "0",
                    ),
                    'panoramio_tag' => array(
                        'type' => 'text',
                        'title' => __( 'Panoramio Tag', 'intense' ),
                        'description' => '',
                        'default' => ''
                    ),
                    'panoramio_user' => array(
                        'type' => 'text',
                        'title' => __( 'Panoramio User', 'intense' ),
                        'description' => __( 'limit panoramio images on map to one user', 'intense' ),
                        'default' => ''
                    ),
                )
            ),
            'colors_tab' => array(
                'type' => 'tab',
                'title' => __( 'Colors', 'intense' ),
                'fields' =>  array(
                    'hue' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Hue', 'intense' ),
                        'description' => 'Indicates the basic color.',
                        'default' => 'primary'
                    ),
                    'saturation' => array(
                        'type' => 'text',
                        'title' => __( 'Saturation', 'intense' ),
                        'description' => 'Enter a value between -100 and 100 <br />Indicates the percentage change in intensity of the basic color to apply to the element.',
                        'default' => ''
                    ),
                    'lightness' => array(
                        'type' => 'text',
                        'title' => __( 'Lightness', 'intense' ),
                        'description' => 'Enter a value between -100 and 100 <br />Indicates the percentage change in brightness of the element. Negative values increase darkness (where -100 specifies black) while positive values increase brightness (where +100 specifies white).',
                        'default' => ''
                    ),
                    'gamma' => array(
                        'type' => 'text',
                        'title' => __( 'Gamma', 'intense' ),
                        'description' => 'Enter a value between 0.01 and 10.0, where 1.0 applies no correction <br />Indicates the amount of gamma correction to apply to the element. Gammas modify the lightness of hues in a non-linear fashion, while not impacting white or black values. Gammas are typically used to modify the contrast of multiple elements. For example, you could modify the gamma to increase or decrease the contrast between the edges and interiors of elements. Low gamma values (< 1) increase contrast, while high values (> 1) decrease contrast.',
                        'default' => ''
                    ),
                    'lightness_invert' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Invert Lightness', 'intense' ),
                        'description' => 'Inverts the existing lightness. This is useful, for example, for quickly switching to a darker map with white text.',
                        'default' => "0",
                    ),
                )
            ),
            'markers_tab' => array(
                'type' => 'tab',
                'title' => __( 'Markers', 'intense' ),
                'fields' =>  array(
                    'map_marker' => array(
                        'type' => 'repeater',
                        'title' => __( 'Markers', 'intense' ),
                        'preview_content' => __( '', 'intense' ),
                        'fields' => array(
                            'address' => array(
                                'type' => 'text',
                                'title' => __( 'Address', 'intense' ),
                                'description' => '',
                                'default' => ''
                            ),
                            'latitude' => array(
                                'type' => 'text',
                                'title' => __( 'Latitude', 'intense' ),
                                'description' => '',
                                'default' => ''
                            ),
                            'longitude' => array(
                                'type' => 'text',
                                'title' => __( 'Longitude', 'intense' ),
                                'description' => '',
                                'default' => ''
                            ),
                            'title' => array(
                                'type' => 'text',
                                'title' => __( 'Title', 'intense' ),
                                'skinnable' => '0'
                            ),
                            'description' => array(
                                'type' => 'text',
                                'title' => __( 'Description', 'intense' ),
                            ),
                            'markertype' => array(
                                'type' => 'dropdown',
                                'title' => __( 'Type', 'intense' ),
                                'default' => 'MARKER',
                                'options' => array(
                                    'MARKER' => __( 'Marker', 'intense' ),
                                    'BUBBLE' => __( 'Bubble', 'intense' ),
                                )
                            ),
                            'markertext' => array(
                                'type' => 'text',
                                'title' => __( 'Text', 'intense' ),
                                'description' => '',
                            ),
                            'color' => array(
                                'type' => 'color_advanced',
                                'title' => __( 'Color', 'intense' ),
                                'default' => 'primary'
                            ),
                            'animation' => array(
                                'type' => 'dropdown',
                                'title' => __( 'Animation', 'intense' ),
                                'default' => '',
                                'options' => array(
                                    '' => __( 'None', 'intense' ),
                                    'DROP' => __( 'Drop', 'intense' ),
                                    'BOUNCE' => __( 'Bounce', 'intense' ),
                                )
                            ),
                            'markerimage' => array(
                                'type' => 'image',
                                'title' => __( 'Custom Marker', 'intense' ),
                                'description' => __( 'enter a WordPress attachment ID or a URL - Suggested size is 32x32, anchor will be bottom center', 'intense' ),
                            ),
                            'markeranchor' => array(
                                'type' => 'text',
                                'title' => __( 'Marker Anchor', 'intense' ),
                                'description' => '0,0 is the top left of the image, so the anchor would be 16,32 for a 32x32 image when you want the point at the bottom center - default is 16,32 if a custom marker is added) examples... "16,32", "0,32", "0,16"',
                            ),
                            'markershadow' => array(
                                'type' => 'image',
                                'title' => __( 'Marker Shadow', 'intense' ),
                                'description' => __( 'enter a WordPress attachment ID or a URL - suggested size is 32x32, anchor will be bottom center', 'intense' ),
                            ),
                            'markershadowanchor' => array(
                                'type' => 'text',
                                'title' => __( 'Shadow Anchor', 'intense' ),
                                'description' => '0,0 is the top left of the image, so the anchor would be 16,32 for a 32x32 image when you want the point at the bottom center - default is 16,32 if a custom marker is added) examples... "16,32", "0,32", "0,16"',
                            ),
                        )
                    ),
                )
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        //google maps api and styled marker
        intense_add_script( 'googlemap' );
        intense_add_script( 'styledmarker' );

        $output = '';
        $random = rand();
        $GLOBALS['map_number'] = $random;
        $mapID = 'map_' . $random;
        $mapFunction = 'Map' . $random;
        $shadow_style = "margin-bottom: 0;";
        $GLOBALS['fit_bounds'] = $fit_bounds;
        $stylers = '';
        $styler_options = '';

        if ( isset( $hue ) || isset( $saturation ) || isset( $lightness ) || isset( $gamma ) || $lightness_invert != 0 ) {
            if ( isset( $hue ) ) {
                $styler_options .= "          { 'hue': '" . $hue . "' },";
            }

            if ( isset( $saturation ) ) {
                $styler_options .= "          { 'saturation': '" . $saturation . "' },";
            }

            if ( isset( $lightness ) ) {
                $styler_options .= "          { 'lightness': '" . $lightness . "' },";
            }

            if ( isset( $gamma ) ) {
                $styler_options .= "          { 'gamma': '" . $gamma . "' },";
            }

            if ( $lightness_invert != 0 ) {
                $styler_options .= "          { 'invert_lightness': true },";
            }

            $styler_options = substr($styler_options, 0, -1);
        }

        $output = '';

        if ( $shadow ) {
            $output .= "<div><div style='$shadow_style'>";
        }

        if ( is_numeric( $width ) ) {
            $width .= 'px';
        }

        if ( is_numeric( $height ) ) {
            $height .= 'px';
        }

        $output .= "<div id='" . $mapID . "' class='mapcanvas' style='width: " . $width . "; height: " . $height . "; direction: ltr;'></div>";

        if ( $shadow ) {
            $output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='" . ( isset( $width ) ? 'width: ' . $width . '; height: auto;' : '' ) . "vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0;' alt='' /></div>";
        }

        $output .= "<script type='text/javascript'>";

        if ( !empty( $styler_options ) ) {
            $output .= "      var styles" . $random . " = [";
            $output .= "        {";
            $output .= "          stylers: [";
            $output .= $styler_options;
            $output .= "          ]";
            $output .= "        }";
            $output .= "      ];";
        }

        $output .= "  var panoramioLayer" . $random . ";";
        $output .= "  function " . $mapFunction . "() {";
        $output .= "    latitude" . $random . " = '';";
        $output .= "    longitude" . $random . " = '';";
        $output .= "    var mapOptions" . $random . " = {";
        $output .= "      zoom: " . $zoom . ",";
        $output .= "      scrollwheel: " . ( $scroll_wheel ? 'true' : 'false' ) . ",";
        $output .= "      scaleControl: " . ( $scale_control ? 'true' : 'false' ) . ",";
        $output .= "      streetViewControl: " . ( $street_view ? 'true' : 'false' ) . ",";
        $output .= "      overviewMapControl: " . ( $overview_map ? 'true' : 'false' ) . ",";
        $output .= "      panControl: " . ( $pan_control ? 'true' : 'false' ) . ",";
        $output .= "      mapTypeControl: " . ( $map_type_control ? 'true' : 'false' ) . ",";
        $output .= "      mapTypeControlOptions: {";
        $output .= "        style: google.maps.MapTypeControlStyle." . strtoupper( $map_type_style );
        $output .= "      },";
        $output .= "      zoomControl: " . ( $zoom_control ? "true" : 'false' ) . ",";
        $output .= "      zoomControlOptions: {";
        $output .= "        style: google.maps.ZoomControlStyle." . strtoupper( $zoom_control_style );
        $output .= "      },";
        $output .= "      mapTypeId: google.maps.MapTypeId." . strtoupper( $type ) . ",";
        $output .= "    };";
        $output .= "    var map" . $random . " = new google.maps.Map(document.getElementById('" . $mapID . "'), mapOptions" . $random . ");";

        if ( !empty( $styler_options ) ) {
            $output .= "map" . $random . ".setOptions({styles: styles" . $random . "});";
        }

        $output .= "    var bounds" . $random . " = new google.maps.LatLngBounds();";

        if ( !empty( $latitude ) && !empty( $longitude ) ) {
            $output .= '    map' . $random . '.setCenter(new google.maps.LatLng(' . $latitude . ', ' . $longitude . '));';
        }

        if ( !empty( $address ) ) {
            $output .= 'var geocoder' . $random . ' = new google.maps.Geocoder();';
            $output .= 'geocoder' . $random . '.geocode({ "address": "' . $address . '" }, function (results, status) {';
            $output .= '  if (status == google.maps.GeocoderStatus.OK) {';
            $output .= '    map' . $random . '.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));';
            $output .= '  }';
            $output .= '});';
        }

        $output .= do_shortcode( $content );

        if ( $traffic_layer != 0 ) {
            $output .= "    var trafficLayer" . $random . " = new google.maps.TrafficLayer();";
            $output .= "    trafficLayer" . $random . ".setMap(map" . $random . ");";
        }

        if ( $transit_layer != 0 ) {
            $output .= "    var transitLayer" . $random . " = new google.maps.TransitLayer();";
            $output .= "    transitLayer" . $random . ".setMap(map" . $random . ");";
        }

        if ( $bicycle_layer != 0 ) {
            $output .= "    var bicycleLayer" . $random . " = new google.maps.BicyclingLayer();";
            $output .= "    bicycleLayer" . $random . ".setMap(map" . $random . ");";
        }

        if ( $weather_layer != 0 ) {
            $output .= "    var weatherLayer" . $random . " = new google.maps.weather.WeatherLayer({ temperatureUnits: google.maps.weather.TemperatureUnit." . strtoupper( $temperature_unit ) . " });";
            $output .= "    weatherLayer" . $random . ".setMap(map" . $random . ");";
        }

        if ( $cloud_layer != 0 ) {
            $output .= "    var cloudLayer" . $random . " = new google.maps.weather.CloudLayer();";
            $output .= "    cloudLayer" . $random . ".setMap(map" . $random . ");";
        }

        if ( $panoramio_layer != 0 ) {
            $output .= "    panoramioLayer" . $random . " = new google.maps.panoramio.PanoramioLayer();";
            $output .= "    panoramioLayer" . $random . ".setMap(map" . $random . ");";
        }

        $output .= "  }";

        if ( isset( $panoramio_user ) && !isset( $panoramio_tag ) ) {
            $output .= "  function getTaggedPhotos() {";
            $output .= "    var tagFilter = " . $panoramio_user . ";";
            $output .= "    panoramioLayer" . $random . ".setUserId(tagFilter);";
            $output .= "  }";
        }

        if ( isset( $panoramio_tag ) && !isset( $panoramio_user ) ) {
            $output .= "  function getTaggedPhotos() {";
            $output .= "    var tagFilter = " . $panoramio_tag . ";";
            $output .= "    panoramioLayer" . $random . ".setTag(tagFilter);";
            $output .= "  }";
        }

        $output .= "  function toggleBounce(markerId) {";
        $output .= "    var tmarker = eval('marker' + markerId);";
        $output .= "    if (tmarker.getAnimation() != null) {";
        $output .= "      tmarker.setAnimation(null);";
        $output .= "    } else {";
        $output .= "      tmarker.setAnimation(google.maps.Animation.BOUNCE);";
        $output .= "    }";
        $output .= "  }";
        $output .= "</script>";

        $output .= "<script>  jQuery(function($){";
        $output .= " $(document).ready(function() {";
        $output .= "    google.maps.event.addDomListener(window, 'load', " . $mapFunction . ");";
        $output .= " });";
        $output .= "}); </script>";

        return $output;
    }

     function render_dialog_script() {
?>
        <script>
            jQuery(function($) {
                // Uploading files
                $('.upload_image_button').live('click', function( event ){
                  event.preventDefault();
                  var target = $(this).data('target-id');

                  window.parent.loadImageFrame($(this), function(attachment) {
                    jQuery("#" + target).val(attachment.id);
                    
                    if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }

                    if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
                  });
                });
            });
        </script>
    <?php
    }
}

class Intense_Map_Marker extends Intense_Shortcode {
    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Map Marker', 'intense' );
        $this->category = __( 'Media', 'intense' );
        $this->icon = 'dashicons-intense-map';
        $this->show_preview = true;       
        $this->hidden = true;
        $this->parent = 'intense_map';

        $this->fields = array(
            'address' => array(
                'type' => 'text',
                'title' => __( 'Address', 'intense' ),
                'skinnable' => '0'
            ),
            'latitude' => array(
                'type' => 'text',
                'title' => __( 'Latitude', 'intense' ),
                'skinnable' => '0'
            ),
            'longitude' => array(
                'type' => 'text',
                'title' => __( 'Longitude', 'intense' ),
                'skinnable' => '0'
            ),
            'title' => array(
                'type' => 'text',
                'title' => __( 'Title', 'intense' ),
                'skinnable' => '0'
            ),
            'description' => array(
                'type' => 'text',
                'title' => __( 'Description', 'intense' ),
                'skinnable' => '0'
            ),
            'markertype' => array(
                'type' => 'dropdown',
                'title' => __( 'Type', 'intense' ),
                'default' => 'MARKER',
                'options' => array(
                    'MARKER' => __( 'Marker', 'intense' ),
                    'BUBBLE' => __( 'Bubble', 'intense' ),
                )
            ),
            'markertext' => array(
                'type' => 'text',
                'title' => __( 'Text', 'intense' ),
                'skinnable' => '0'
            ),
            'color' => array(
                'type' => 'color',
                'title' => __( 'Color', 'intense' ),
            ),
            'animation' => array(
                'type' => 'dropdown',
                'title' => __( 'Animation', 'intense' ),
                'default' => '',
                'options' => array(
                    '' => __( 'None', 'intense' ),
                    'DROP' => __( 'Drop', 'intense' ),
                    'BOUNCE' => __( 'Bounce', 'intense' ),
                )
            ),
            'markerimage' => array(
                'type' => 'image',
                'title' => __( 'Custom Marker', 'intense' ),
                'description' => __( 'enter a WordPress attachment ID or a URL - Suggested size is 32x32, anchor will be bottom center', 'intense' ),
                'skinnable' => '0'
            ),
            'markeranchor' => array(
                'type' => 'text',
                'title' => __( 'Marker Anchor', 'intense' ),
                'description' => '0,0 is the top left of the image, so the anchor would be 16,32 for a 32x32 image when you want the point at the bottom center - default is 16,32 if a custom marker is added) examples... "16,32", "0,32", "0,16"',
            ),
            'markershadow' => array(
                'type' => 'image',
                'title' => __( 'Marker Shadow', 'intense' ),
                'description' => __( 'enter a WordPress attachment ID or a URL - suggested size is 32x32, anchor will be bottom center', 'intense' ),
                'skinnable' => '0'
            ),
            'markershadowanchor' => array(
                'type' => 'text',
                'title' => __( 'Shadow Anchor', 'intense' ),
                'description' => '0,0 is the top left of the image, so the anchor would be 16,32 for a 32x32 image when you want the point at the bottom center - default is 16,32 if a custom marker is added) examples... "16,32", "0,32", "0,16"',
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $output = '';
        $random = $GLOBALS['map_number'];
        $bounds = $GLOBALS['fit_bounds'];
        $random_marker = rand();
        $anchor = '16,32';
        $shadowanchor = '16,32';

        if ( is_numeric( $markerimage ) ) {
          $photo_info = wp_get_attachment_image_src( $markerimage, null );
          $markerimage = $photo_info[0];
        }

        if ( is_numeric( $markershadow ) ) {
          $photo_info = wp_get_attachment_image_src( $markershadow, null );
          $markershadow = $photo_info[0];
        }

        if ( !empty( $markerimage ) && !empty( $markeranchor ) ) {
            $anchor = $markeranchor;
        }

        if ( !empty( $markershadow ) && !empty( $markershadowanchor ) ) {
            $shadowanchor = $markershadowanchor;
        }

        if ( !empty( $latitude ) && !empty( $longitude ) ) {
            $output .= '      map' . $random . '.setCenter(new google.maps.LatLng(' . $latitude . ', ' . $longitude . '));';
        }

        $output .= 'var infowindow' . $random_marker . ' = null;';

        //SETUP MARKER
        if ( !empty( $markerimage ) ) {
            $output .= '    image' . $random_marker . ' = {';
            $output .= '        url: "' . $markerimage . '",';
            //$output .= '      size: new google.maps.Size(32, 32),';
            //$output .= '      origin: new google.maps.Point(0, 0),';
            $output .= '        anchor: new google.maps.Point(' . $anchor . ')';
            $output .= '    };';

            if ( !empty( $markershadow ) ) {
                $output .= '    shadow' . $random_marker . ' = {';
                $output .= '        url: "' . $markershadow . '",';
                $output .= '        anchor: new google.maps.Point(' . $shadowanchor . ')';
                $output .= '    };';
            }

            $output .= '    marker' . $random_marker . ' = new google.maps.Marker({';

            if ( !empty( $latitude ) && !empty( $longitude ) ) {
                $output .= '      position: new google.maps.LatLng(' . $latitude . ', ' . $longitude . '),';
            }

            if ( !empty( $animation ) ) {
                $output .= '      animation: google.maps.Animation.' . strtoupper( $animation ) . ',';
            }

            $output .= '        icon: image' . $random_marker . ',';

            if ( !empty( $markershadow ) ) {
                $output .= '        shadow: shadow' . $random_marker . ',';
            }

            $output .= '        map: map' . $random . '';
            $output .= '    });';
        } else {
            $output .= '    marker' . $random_marker . ' = new StyledMarker({';

            if ( strtoupper( $markertype ) == 'MARKER' ) {
                if ( !empty( $markertext ) ) {
                    $output .= '      styleIcon: new StyledIcon(StyledIconTypes.MARKER,{color:"' . ( !empty( $color ) ? $color : $intense_visions_options['intense_icon_color'] ) . '", text:"' . $markertext . '"}),';
                } else {
                    $output .= '      styleIcon: new StyledIcon(StyledIconTypes.MARKER,{color:"' . ( !empty( $color ) ? $color : $intense_visions_options['intense_icon_color'] ) . '"}),';
                }
            } elseif ( strtoupper( $markertype ) == 'BUBBLE' && $markerimage == '' ) {
                $output .= '      styleIcon: new StyledIcon(StyledIconTypes.BUBBLE,{color:"' . ( !empty( $color ) ? $color : $intense_visions_options['intense_icon_color'] ) . '", text:"' . $markertext . '"}),';
            }

            $output .= '        draggable: false,';

            if ( !empty( $latitude ) && !empty( $longitude ) ) {
                $output .= '      position: new google.maps.LatLng(' . $latitude . ', ' . $longitude . '),';
            }

            if ( !empty( $animation ) ) {
                $output .= '      animation: google.maps.Animation.' . strtoupper( $animation ) . ',';
            }

            $output .= '        map: map' . $random . '';
            $output .= '    });';
        }
        //END MARKER

        if ( !empty( $address ) ) {
            $output .= 'var geocoder' . $random_marker . ' = new google.maps.Geocoder();';
            $output .= 'geocoder' . $random_marker . '.geocode({ "address": "' . $address . '" }, function (results, status) {';
            $output .= '  if (status == google.maps.GeocoderStatus.OK) {';
            $output .= '    marker' . $random_marker . '.setPosition(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));';

            if ( $bounds == 1 ) {
                $output .= '    bounds' . $random . '.extend(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));';
                $output .= '    map' . $random . '.fitBounds(bounds' . $random . ');';
            } else {
                $output .= '    map' . $random . '.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));';
            }
            $output .= '  }';
            $output .= '});';
        }

        $output .= 'infowindow = new google.maps.InfoWindow({';
        $output .= 'content: "loading..."';
        $output .= '});';

        if ( $animation == 'BOUNCE' ) {
            $output .= 'google.maps.event.addListener(marker' . $random_marker . ', "click", function() {';
            $output .= '  infowindow.setContent("<b>' . $title . '</b><br />' . $address . '<br />' . $description . '");';
            $output .= '  infowindow.open(map' . $random . ', this);';
            $output .= '  toggleBounce("' . $random_marker . '")';
            $output .= '});';
        } else {
            $output .= 'google.maps.event.addListener(marker' . $random_marker . ', "click", function () {';
            $output .= '  infowindow.setContent("<b>' . $title . '</b><br />' . $address . '<br />' . $description . '");';
            $output .= '  infowindow.open(map' . $random . ', this);';
            $output .= '});';
        }

        return $output;
    }
}