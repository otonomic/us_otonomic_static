<?php

class Intense_Post_Fields extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Post Fields', 'intense' );
		$this->category = __( 'Posts', 'intense' );
		$this->icon = 'dashicons-editor-paste-text';
		$this->show_preview = false;
		$this->show_preset = false;

		$this->fields = array(
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'default' => '',
				'options' => array(
					'' => __( 'Select a post type', 'intense' ),
					'books' => __( 'Books', 'intense' ),
					'clients' => __( 'Clients', 'intense' ),
					'coupons' => __( 'Coupons', 'intense' ),
					'events' => __( 'Events', 'intense' ),
					'faq' => __( 'FAQ', 'intense' ),
					'jobs' => __( 'Jobs', 'intense' ),
					'locations' => __( 'Locations', 'intense' ),
					'movies' => __( 'Movies', 'intense' ),
					'news' => __( 'News', 'intense' ),
					'portfolio' => __( 'Portfolios', 'intense' ),
					'post' => __( 'Posts', 'intense' ),
					'project' => __( 'Projects', 'intense' ),
					'quotes' => __( 'Quotes', 'intense' ),
					'recipes' => __( 'Recipes', 'intense' ),
					'team' => __( 'Team', 'intense' ),
					'testimonials' => __( 'Testimonials', 'intense' ),
				),
			),
			'field' => array(
				'type' => 'dropdown',
				'title' => __( 'Field', 'intense' ),
				'options' => array(
					'title' => __( 'Title', 'intense' ),
					'content' => __( 'Content', 'intense' ),
					'featured_images' => __( 'Featured Images', 'intense' ),
					'featured_color' => __('Featured Color', 'intense' ),
					'permalink' => __( 'Permalink', 'intense' ),
					'book_subtitle' => __( 'Subtitle', 'intense' ),
					'book_category' => __( 'Categories', 'intense' ),
					'book_author' => __( 'Authors', 'intense' ),
					'book_publisher' => __( 'Publishers', 'intense' ),
					'book_image' => __( 'Cover Image', 'intense' ),
					'book_audio_clip' => __( 'Audio Clip', 'intense' ),
					'book_website' => __( 'Website', 'intense' ),
					'book_purchase_link' => __( 'Purchase Link', 'intense' ),
					'book_languages' => __( 'Languages', 'intense' ),
					'book_excerpt' => __( 'Excerpt', 'intense' ),
					'book_types' => __( 'Type(s)', 'intense' ),
					'book_release_date' => __( 'Release Date', 'intense' ),
					'book_recommended_age' => __( 'Recommended Age', 'intense' ),
					'book_back_cover' => __( 'Back Cover Text', 'intense' ),
					'book_awards' => __( 'Awards', 'intense' ),
					'book_reviews' => __( 'Reviews', 'intense' ),
					'client_slogan' => __( 'Slogan', 'intense' ),
					'clients_category' => __( 'Categories', 'intense' ),
					'client_logo' => __( 'Logo', 'intense' ),
					'client_sector' => __( 'Business Sector', 'intense' ),
					'client_website' => __( 'Website', 'intense' ),
					'client_address' => __( 'Address', 'intense' ),
					'client_employees' => __( 'Employees', 'intense' ),
					'client_contact' => __( 'Contact Name', 'intense' ),
					'client_contact_email' => __( 'Contact Email', 'intense' ),
					'client_contact_phone' => __( 'Contact Phone', 'intense' ),
					'coupons_category' => __( 'Categories', 'intense' ),
					'coupon_start_date' => __( 'Start Date', 'intense' ),
					'coupon_end_date' => __( 'End Date', 'intense' ),
					'event_subtitle' => __( 'Subtitle', 'intense' ),
					'events_category' => __( 'Categories', 'intense' ),
					'event_type' => __( 'Type', 'intense' ),
					'event_start_date' => __( 'Start Date', 'intense' ),
					'event_end_date' => __( 'End Date', 'intense' ),
					'event_address' => __( 'Address', 'intense' ),
					'event_website' => __( 'Website', 'intense' ),
					'event_entrance_cost' => __( 'Entrance Cost', 'intense' ),
					'event_contact_name' => __( 'Contact Name', 'intense' ),
					'event_contact_phone' => __( 'Contact Phone', 'intense' ),
					'event_contact_email' => __( 'Contact Email', 'intense' ),
					'event_venue_name' => __( 'Venue Name', 'intense' ),
					'event_venue_type' => __( 'Venue Type', 'intense' ),
					'event_venue_capacity' => __( 'Venue Capacity', 'intense' ),
					'faq_category' => __( 'Categories', 'intense' ),
					'job_category' => __( 'Categories', 'intense' ),
					'job_type' => __( 'Types', 'intense' ),
					'job_post_date' => __( 'Post Date', 'intense' ),
					'job_expire_date' => __( 'Expire Date', 'intense' ),
					'job_status' => __( 'Status', 'intense' ),
					'job_featured' => __( 'Featured', 'intense' ),
					'job_company' => __( 'Company Name', 'intense' ),
					'job_tagline' => __( 'Company Tagline', 'intense' ),
					'job_link' => __( 'Page Link', 'intense' ),
					'job_qualifications' => __( 'Company Tagline', 'intense' ),
					'job_responsibilities' => __( 'Company Tagline', 'intense' ),
					'job_competencies' => __( 'Company Tagline', 'intense' ),
					'job_compensation' => __( 'Company Tagline', 'intense' ),
					'job_tagline' => __( 'Company Tagline', 'intense' ),
					'job_company_logo' => __( 'Company Logo', 'intense' ),
					'job_location' => __( 'Company Location', 'intense' ),
					'job_company_website' => __( 'Company Website', 'intense' ),
					'job_contact' => __( 'Contact Name', 'intense' ),
					'job_contact_email' => __( 'Contact Email', 'intense' ),
					'job_contact_phone' => __( 'Contact Phone', 'intense' ),
					'locations_category' => __( 'Categories', 'intense' ),
					'location_type' => __( 'Type', 'intense' ),
					'location_address' => __( 'Address', 'intense' ),
					'location_website' => __( 'Website', 'intense' ),
					'location_attractions' => __( 'Attractions', 'intense' ),
					'location_history' => __( 'History', 'intense' ),
					'movie_subtitle' => __( 'Subtitle', 'intense' ),
					'movie_genre' => __( 'Genres', 'intense' ),
					'movie_cast' => __( 'Cast', 'intense' ),
					'movie_director' => __( 'Directors', 'intense' ),
					'movie_image' => __( 'Cover Image', 'intense' ),
					'movie_website' => __( 'Website', 'intense' ),
					'movie_purchase_link' => __( 'Purchase Link', 'intense' ),
					'movie_rating' => __( 'MPAA Rating', 'intense' ),
					'movie_trailer' => __( 'Trailer', 'intense' ),
					'movie_runtime' => __( 'Runtime', 'intense' ),
					'movie_release_date' => __( 'Theatrical Release Date', 'intense' ),
					'movie_dvd_release_date' => __( 'DVD Release Date', 'intense' ),
					'movie_languages' => __( 'Languages', 'intense' ),
					'movie_subtitles' => __( 'Subtitles ', 'intense' ),
					'movie_reviews' => __( 'Reviews', 'intense' ),
					'news_subtitle' => __( 'Subtitle', 'intense' ),
					'news_category' => __( 'Categories', 'intense' ),
					'news_author' => __( 'Author', 'intense' ),
					'news_date' => __( 'Date', 'intense' ),
					'news_website' => __( 'Website', 'intense' ),
					'news_publisher' => __( 'Publisher', 'intense' ),
					'news_publisher_website' => __( 'Publisher Website', 'intense' ),
					'portfolio_subtitle' => __( 'Subtitle', 'intense' ),
					'portfolio_category' => __( 'Categories', 'intense' ),
					'portfolio_skills' => __( 'Skills', 'intense' ),
					'portfolio_date' => __( 'Date', 'intense' ),
					'portfolio_designed_by' => __( 'Designed By', 'intense' ),
					'portfolio_built_by' => __( 'Built By', 'intense' ),
					'portfolio_produced_by' => __( 'Produced By', 'intense' ),
					'portfolio_year_completed' => __( 'Year Completed', 'intense' ),
					'portfolio_video_embed_code' => __( 'Video Embed Code', 'intense' ),
					'portfolio_video_url' => __( 'Youtube/Vimeo Video URL for Lightbox', 'intense' ),
					'portfolio_project_url' => __( 'Project URL', 'intense' ),
					'portfolio_project_url_text' => __( 'Project URL Text', 'intense' ),
					'portfolio_copy_url' => __( 'Copyright URL', 'intense' ),
					'portfolio_copy_url_text' => __( 'Copyright URL Text', 'intense' ),
					'post_subtitle' => __( 'Subtitle', 'intense' ),
					'project_subtitle' => __( 'Subtitle', 'intense' ),
					'project_category' => __( 'Categories', 'intense' ),
					'project_skills' => __( 'Skills', 'intense' ),
					'project_date' => __( 'Date', 'intense' ),
					'project_designed_by' => __( 'Designed By', 'intense' ),
					'project_built_by' => __( 'Built By', 'intense' ),
					'project_produced_by' => __( 'Produced By', 'intense' ),
					'project_location' => __( 'Location Name', 'intense' ),
					'project_location_address' => __( 'Location Address', 'intense' ),
					'project_company' => __( 'Company Name', 'intense' ),
					'project_company_address' => __( 'Company Address', 'intense' ),
					'project_quantity' => __( 'Quantity', 'intense' ),
					'quotes_category' => __( 'Categories', 'intense' ),
					'quote_author' => __( 'Author', 'intense' ),
					'quote_date' => __( 'Date', 'intense' ),
					'quote_year' => __( 'Year', 'intense' ),
					'recipe_ingredients' => __( 'Ingredients', 'intense' ),
					'recipes_cuisines' => __( 'Cuisines', 'intense' ),
					'recipes_courses' => __( 'Courses', 'intense' ),
					'recipes_skill_levels' => __( 'Skill Levels', 'intense' ),
					'recipe_yield' => __( 'Yield', 'intense' ),
					'recipe_servings' => __( 'Servings', 'intense' ),
					'recipe_prep_time' => __( 'Prep Time', 'intense' ),
					'recipe_cook_time' => __( 'Cook Time', 'intense' ),
					'recipe_instructions' => __( 'Instructions', 'intense' ),
					'team_position' => __( 'Positions', 'intense' ),
					'team_skills' => __( 'Skills', 'intense' ),
					'team_member_title' => __( 'Member Title', 'intense' ),
					'team_member_photo' => __( 'Member Photo', 'intense' ),
					'team_member_facebook' => __( 'Facebook', 'intense' ),
					'team_member_googleplus' => __( 'Google Plus', 'intense' ),
					'team_member_twitter' => __( 'Twitter', 'intense' ),
					'team_member_dribbble' => __( 'Dribbble', 'intense' ),
					'team_member_linkedin' => __( 'Linked In', 'intense' ),
					'team_member_custom_social_icon' => __( 'Custom Social Icon', 'intense' ),
					'team_member_custom_social_link' => __( 'Custom Social Icon Link', 'intense' ),
					'testimonials_category' => __( 'Categories', 'intense' ),
					'testimonial_author' => __( 'Author', 'intense' ),
					'testimonial_background' => __( 'Background Color', 'intense' ),
					'testimonial_font_color' => __( 'Font Color', 'intense' ),
					'testimonial_author_image' => __( 'Author Image', 'intense' ),
					'testimonial_company' => __( 'Company', 'intense' ),
					'testimonial_link' => __( 'Company Link', 'intense' ),
					'testimonial_link_target' => __( 'Company Link Target', 'intense' ),
				),
				'default' => 'title',
				'class' => 'allfields' 
			),
			'size' => array(
				'type' => 'image_size',
				'title' => __( 'Image Size', 'intense' ),
				'class' => 'imagesize'
			),
            'excerpt_length' => array(
                'type' => 'text',
                'title' => __( 'Excerpt Length', 'intense' ),
                'description' => __( 'Only this number of words will be shown for the content (best for use when creating a template for a list of posts). Leave blank to show all of the content.', 'intense' ),
                'default' => '',
				'class' => 'excerptlength'
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options, $post;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$returnString = '';

		if ( $excerpt_length != '' && is_numeric( $excerpt_length ) ) {
			$original_content = $post->post_content;
			$original_content = preg_replace( "~(?:\[/?)[^/\]]+/?\]~s", '', $original_content );

			$content = explode( ' ', $original_content, $excerpt_length );

			if ( count( $content ) >= $excerpt_length ) {
				array_pop( $content );
				$content = implode( " ", $content ).'...';
			} else {
				$content = implode( " ", $content );
			}

			$content = apply_filters( 'the_content', $content );
			$content = str_replace( ']]>', ']]&gt;', $content );
		} else {
			$content = $post->post_content;
		}

		if ( $field === '' ) {
			return '';
		}

		if ( $field === 'title' ) {
			return the_title_attribute( 'echo=0' );
		}

		if ( $field === 'permalink' ) {
			return get_permalink();
		}

		if ( $field === 'content' ) {
			return do_shortcode( $content );
		}

		if ( $field === 'featured_images' ) {
			return intense_get_post_thumbnails( ( $size === '' ? 'full' : $size ) );
		}

		if ( $field === 'featured_color' ) {
			return get_field( 'intense_' . $field ); 
		}

		if ( strpos( $field, '_category' ) !== false || $field === 'book_author' || $field === 'book_publisher' || $field === 'job_type' || $field === 'movie_genre' || $field === 'movie_cast' || $field === 'movie_director' || $field === 'portfolio_skills' || $field === 'project_skills' || $field === 'recipes_ingredients' || $field === 'recipes_cuisines' || $field === 'recipes_courses' || $field === 'recipes_skill_levels' || $field === 'team_position' || $field === 'team_skills' ) {
			$taxonomy = 'intense_' . $field;

			if ( $field === 'portfolio_category' || $field === 'portfolio_skills' ) {
				$taxonomy = $field;
			}

			$categories = wp_get_post_terms( get_the_ID(), $taxonomy, array("fields" => "all") );
			
			if ( $categories ) {
				$category_list = array();

				foreach ( $categories as $category ) {
					$category_list[] = $category->name;
				}

				return join( ', ', $category_list );
			}
		} elseif ( strpos( $field, 'portfolio' ) !== false ) {
		 	if ( $field === 'portfolio_subtitle' ) {
		 		return get_field( 'intense_' . $field );
		 	} elseif ( $field === 'portfolio_date' ) {
		 		return date("M d, Y", strtotime( get_field( 'intense_' . $field ) ) );
		 	} else {
		 		return get_field( str_replace( 'portfolio', 'intense', $field ) );
		 	}
		} elseif ( substr( $field, 0, 7 ) === "project" ) {
		 	return get_field( 'intense_' . $field );
		} elseif ( strpos( $field, '_date' ) !== false ) {
			return date("M d, Y", strtotime( get_field( 'intense_' . $field ) ) );
		} elseif ( strpos( $field, '_image' ) !== false || strpos( $field, '_logo' ) !== false || strpos( $field, '_photo' ) !== false ) {
			$image = get_field( 'intense_' . str_replace( 'team_', '', $field ) );			
		
			return intense_run_shortcode( 'intense_image', array( 
					'image' => $image['id'],
					'size' => ( $size === '' ? 'full' : $size )
				) );
		} elseif ( strpos( $field, '_audio' ) !== false ) {
			return intense_run_shortcode( 'intense_audio', array( 
		          'url' => get_field( 'intense_' . $field )
		        ) );
		// } elseif ( $field === 'team_member_facebook' || $field === 'team_member_googleplus' || $field === 'team_member_twitter' || $field === 'team_member_dribbble' || $field === 'team_member_linkedin' ) {
		// 	return intense_run_shortcode( 'intense_social_icon', array(
		// 			'type' => str_replace( 'team_member_', '', str_replace( 'plus', '-plus', $field ) ),
		// 			'link' => get_field( 'intense_' . str_replace( 'team_', '', $field ) ),
		// 			'link_target' => '_blank',
		// 			'color' => 'primary',
		// 			'size' => '20'
		// 		) );
		// } elseif ( $field === 'team_member_custom_social_icon' ) {
		// 	return intense_run_shortcode( 'intense_social_icon', array(
		// 			'mode' => 'custom',
		// 			'image' => get_field( 'intense_' . str_replace( 'team_', '', $field ) )['id'],
		// 			'link' => get_field( 'intense_member_custom_social_link' ),
		// 			'link_target' => '_blank'
		// 		) );
		} else {
			return get_field( 'intense_' . str_replace( 'team_', '', $field ) );
		}
	}



	function render_dialog_script() {
	?>
		<script>
		jQuery(function($) {
			$('.allfields').hide();
			$('.imagesize').hide();
			$('.excerptlength').hide();
			var options = $("#field").html();

			$('#type').change(function() {
				$('.allfields').hide();
				$('.imagesize').hide();
				$('.excerptlength').hide();

				var text = $("#type :selected").text().toLowerCase();

				$("#field").html(options);
				$('#field :not( [value^="' + text.substr(0, 3) + '"], [value="title"], [value^="content"], [value^="featured_images"], [value^="featured_color"], [value^="permalink"] )' ).remove();
				$('.allfields').show();
			});

			$('#field').change(function() {
				var selected = $("#field :selected").text().toLowerCase();
				$('.imagesize').hide();
				$('.excerptlength').hide();

				if ( selected === 'featured images' || selected === 'cover image' || selected === 'author image' || selected === 'logo' || selected === 'company logo' || selected === 'member photo' ) {
					$('.imagesize').show();
				} else {
					var featimg = document.getElementById('size');
					featimg.selectedIndex = 0;
				}

				if ( selected === 'content' ) {
					$('.excerptlength').show();
				} else {
					var exclen = document.getElementById('excerpt_length');
					exclen.value = '';
				}
			});
		});
	</script>
	<?php

	}
}
