<?php

class Intense_Emphasis extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Emphasis', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-pencil';
		$this->show_preview = true;
		$this->preview_content = __( "Emphasis Paragraph", 'intense' );
		$this->vc_map_content = 'textfield';

		$this->fields = array(
			'color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Color', 'intense' ),
				'default' => 'primary'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$color = intense_get_plugin_color( $color );

		return "<p style='color: " . $color . ";'>" . do_shortcode( $content ) . "</p>";
	}
}
