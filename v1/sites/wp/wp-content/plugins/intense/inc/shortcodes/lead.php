<?php

class Intense_Lead extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Lead', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-editor-justify';
		$this->show_preview = true;
		$this->preview_content = __( "Lead Paragraph", 'intense' );
		$this->vc_map_content = 'textarea';

		$this->fields = array(
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'default' => ''
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$style = '';
        $font_color = intense_get_plugin_color( $font_color );

		if ( !empty( $font_color ) ) {			
			$style = "color: " . $font_color . ";";
		}

		return "<p class='intense lead' style='$style'>" . do_shortcode( $content ) . "</p>";
	}
}
