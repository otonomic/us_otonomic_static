<?php

class Intense_Blockquote extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Blockquote', 'intense' );
        $this->category = __( 'Typography', 'intense' );
        $this->icon = 'dashicons-editor-quote';
        $this->show_preview = true;
        $this->preview_content = __( "Blockquote Text", 'intense' );
        $this->vc_map_content = 'textarea_html';

        $this->fields = array(
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Color', 'intense' ),
                'default' => 'muted'
            ),
            'font_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Font Color', 'intense' ),
                'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                'default' => ''
            ),
            'border_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Border Color', 'intense' ),
            ),
            'border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Border Radius', 'intense' ),
            ),
            'author' => array(
                'type' => 'text',
                'title' => __( 'Author', 'intense' ),
                'description' => __( 'optional - add to the content of the shortcode to manually format the layout', 'intense' ),
                'default' => '',
                'skinnable' => '0'
            ),
            'author_link' => array(
                'type' => 'text',
                'title' => __( 'Author Link', 'intense' ),
                'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'default' => '',
                'skinnable' => '0'
            ),
            'width' => array(
                'type' => 'text',
                'title' => __( 'Width', 'intense' ),
                'description' => __( '%, em, px - examples: 25% or 10em or 20px. If set, the blockquote will float to the right or left and will act like a pull-quote', 'intense' ),
                'default' => ''
            ),
            'rightalign' => array(
                'type' => 'checkbox',
                'title' => __( 'Right Align', 'intense' ),
                'default' => "0",
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $align = '';
        $authorText = '';
        $radius = '';
        $margin = '';

        if ( !empty( $border_radius ) ) {
            $radius = ' border-radius:' . $border_radius . ';';
        }

        if ( !empty( $width ) ) {
            $width = ' width:' . $width . ';';
            $align = 'pull-left';
            $margin = ' margin: 0 20px 20px 0;';
        }

        if ( $rightalign ) {
            $align = 'pull-right';

            if ( !empty( $width ) ) {
                $margin = ' margin: 0 0 20px 20px;';
            }
        }

        $color = intense_get_plugin_color( $color );
        $font_color = intense_get_plugin_color( $font_color );

        if ( $font_color == '' ) {
            $font_color = intense_get_contract_color( $color );
        }

        if ( !empty( $border_color ) ) {
            $border_color = intense_get_plugin_color( $border_color );
            $border_color = " border-color: " . $border_color . ';';
        }

        if ( $author != '' ) {
            if ( $author_link != '' ) {
                if ( is_numeric( $author_link ) ) {
                    $author_link = get_permalink( $author_link );
                }

                $authorText = '<a href="' . $author_link . '" target="_blank" style="text-decoration: none;"><small style="padding-top: 10px;">' . $author . '</small></a>';
            } else {
                $authorText = '<small style="padding-top:10px;">' . $author . '</small>';
            }
        }


        return "<blockquote style='background: " . $color  .
            "; color: " . $font_color .
            ";" . $radius . $width . $margin . $border_color . "' class='intense " . $align . "'>" . do_shortcode( $content ) . ( $authorText != "" ? $authorText : "" ) . "</blockquote>";
    }
}
