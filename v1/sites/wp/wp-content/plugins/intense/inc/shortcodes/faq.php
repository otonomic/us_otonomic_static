<?php

class Intense_Faq extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'FAQ', 'intense' );
		$this->category = __( 'Posts', 'intense' );
		$this->icon = 'dashicons-editor-help';
		$this->show_preview = true;

		$this->fields = array(
			'categories' => array(
				'type' => 'category',
				'category' => 'faq_category',
				'title' => __( 'Category(s)', 'intense' ),
				'description' => __( 'by default, all categories will be shown', 'intense' ),
			),
			'posts_per_page' => array(
				'type' => 'text',
				'title' => __( 'Posts Per Page', 'intense' ),
				'description' => __( 'default will be the value you have set in your theme settings', 'intense' ),
				'default' => get_option( 'posts_per_page' ),
				'skinnable' => '0'
			),
			'show_all' => array(
				'type' => 'checkbox',
				'title' => __( 'Show All', 'intense' ),
				'description' => __( 'show all on one page', 'intense' ),
				'default' => "0",
			),
			'show_filter' => array(
				'type' => 'checkbox',
				'title' => __( 'Show Filter', 'intense' ),
				'default' => "1",
			),
			'filter_easing' => array(
				'type' => 'dropdown',
				'title' => __( 'Filter Easing', 'intense' ),
				'options' => array(
					'smooth' => __( 'Smooth', 'intense' ),
					'snap' => __( 'Snap', 'intense' ),
					'windup' => __( 'Wind Up', 'intense' ),
					'windback' => __( 'Wind Back', 'intense' ),
				),
				'class' => 'filtersettings'
			),
			'filter_effects' => array(
				'type' => 'dropdown',
				'title' => __( 'Filter Effects', 'intense' ),
				'description' => __( 'transition effects, default is Fade and Scale', 'intense' ),
				'options' => array(
					'fade' => __( 'Fade', 'intense' ),
					'scale' => __( 'Scale', 'intense' ),
					'rotateX' => __( 'Rotate X', 'intense' ),
					'rotateY' => __( 'Rotate Y', 'intense' ),
					'rotateZ' => __( 'Rotate Z', 'intense' ),
					'blur' => __( 'Blur', 'intense' ),
					'grayscale' => __( 'Grayscale', 'intense' ),
				),
				'class' => 'filtersettings'
			),
			'single_toggle' => array(
				'type' => 'checkbox',
				'title' => __( 'Single Toggle', 'intense' ),
				'default' => "0",
			),
			'expand_all' => array(
				'type' => 'checkbox',
				'title' => __( 'Expand All', 'intense' ),
				'description' => __( 'expand all FAQ\'s by default. Single Toggle option must not be checked.', 'intense' ),
				'default' => "0",
			),
			'title_border' => array(
				'type' => 'border',
				'title' => __( 'Title Border', 'intense' ),
			),
			'content_border' => array(
				'type' => 'border',
				'title' => __( 'Content Border', 'intense' ),
			),
			'expand_icon' => array(
				'type' => 'icon',
				'title' => __( 'Expand Icon', 'intense' ),
				'default' => 'plus-sign-alt',
			),
			'collapse_icon' => array(
				'type' => 'icon',
				'title' => __( 'Collapse Icon', 'intense' ),
				'default' => 'minus-sign-alt',
			),
			'icon_size' => array(
				'type' => 'dropdown',
				'title' => __( 'Icon Size', 'intense' ),
				'default' => '2',
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5'
				)
			),
			'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';
		$faqid = '';

		global $post, $wp_query;

		$padding_top = $intense_visions_options['intense_layout_row_default_padding']['padding-top'];
		$padding_bottom = $intense_visions_options['intense_layout_row_default_padding']['padding-bottom'];
		$margin_top = $intense_visions_options['intense_layout_row_default_margin']['margin-top'];
		$margin_bottom = $intense_visions_options['intense_layout_row_default_margin']['margin-bottom'];

		$layout_style = '';

		if ( isset( $padding_top ) ) {
			$layout_style .= 'padding-top: ' . $padding_top . 'px; ';
		}

		if ( isset( $padding_bottom ) ) {
			$layout_style .= 'padding-bottom: ' . $padding_bottom . 'px; ';
		}

		if ( isset( $margin_top ) ) {
			$layout_style .= 'margin-top: ' . $margin_top . 'px; ';
		}

		if ( isset( $margin_bottom ) ) {
			$layout_style .= 'margin-bottom: ' . $margin_bottom . 'px; ';
		}

		$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

		$original_query = $wp_query;
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$wp_query = null;
		$iscat_array = false;
		$cat_array = null;
		$faqid = 'faq_list_' . rand();

		$args = array(
			'post_type' => 'intense_faq',
			'orderby' => 'id',
			'order' => 'DESC',
			'paged'=> $paged,
			'nopaging' => ( $show_all == 0 ? false : true ),
			'tax_query' => '',
			'posts_per_page' => $posts_per_page
		);

		if ( $categories != '' ) {
			if ( strpos( $categories, ',' ) ) {
				$iscat_array = true;
				$cat_array = explode( ',', $categories );
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'faq_category',
						'field'  => 'slug',
						'terms'  => $cat_array
					)
				);
			} else {
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'faq_category',
						'field'  => 'slug',
						'terms'  => array( $categories )
					)
				);
			}
		}

		$wp_query = new WP_Query( $args );

		//Setup filters
		if ( ( $show_filter && $categories == '' ) || ( $iscat_array && $show_filter ) ) {
			$faq_category = get_terms( 'faq_category' );
			$filter_shortcode = "";

			if ( $faq_category ) {
				$filter_shortcode .= "<nav role='navigation' id='nav-below' class='filter-post'>";
				$filter_shortcode .= "<ul class='intense pager " . ( $rtl ? "rtl " : "" ) . "'>";
				$filter_shortcode.= "<li><a class='filter btn-mini active' data-filter='all' href='#allfilter'>" . __( 'All', 'Intense' ) . "</a></li>";

				foreach ( $faq_category as $faq_cat ) {
					if ( $iscat_array ) {
						foreach ( $cat_array as &$value ) {
							if ( $value == $faq_cat->slug ) {
								$filter_shortcode .= "<li><a class='filter btn-mini' data-filter='" . $faq_cat->slug . "' href='#" . $faq_cat->slug . "filter'>" . esc_html( $faq_cat->name ) . "</a></li>";
							}
						}
					} else {
						$filter_shortcode .= "<li><a class='filter btn-mini' data-filter='" . $faq_cat->slug . "' href='#" . $faq_cat->slug . "filter'>" . esc_html( $faq_cat->name ) . "</a></li>";
					}
				}

				$filter_shortcode .= "</ul>";
				$filter_shortcode .= "</nav>";
			}

			$output .= do_shortcode( $filter_shortcode );
		}

		$i = 0;
		$j = 1;

		$collapsibles_shortcode = '<div id="' . $faqid . '" class="intense row" style="' . $layout_style . '"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
		$collapsibles_shortcode .= '[intense_collapsibles single_toggle="' . $single_toggle . '" id="' . $faqid . '" title_border="' . $title_border . '" content_border="' . $content_border . '" expand_icon="' . $expand_icon . '" collapse_icon="' . $collapse_icon . '" icon_size="' . $icon_size . '"]';

		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			$faq_link = '';
			$faq_link_target = '';
			$css_class = '';
			$title_background_color = '';
			$content_background_color = '';
			$title_font_color = '';
			$content_font_color = '';
			$set_active = '';

			if ( get_field( 'intense_faq_link' ) != '' ) {
				$faq_link = ' external_link="' . get_field( 'intense_faq_link' ) . '"';
			}

			if ( get_field( 'intense_faq_link_target' ) != '' ) {
				$faq_link_target = ' external_link_target="' . get_field( 'intense_faq_link_target' ) . '"';
			}

			if ( get_field( 'intense_faq_css_class' ) != '' ) {
				$css_class = ' class="' . get_field( 'intense_faq_css_class' ) . '"';
			}

			if ( get_field( 'intense_faq_title_background' ) != '' ) {
				$title_background_color = ' title_background_color="' . get_field( 'intense_faq_title_background' ) . '"';
			}

			if ( get_field( 'intense_faq_content_background' ) != '' ) {
				$content_background_color = ' content_background_color="' . get_field( 'intense_faq_content_background' ) . '"';
			}

			if ( get_field( 'intense_faq_title_font_color' ) != '' ) {
				$title_font_color = ' title_font_color="' . get_field( 'intense_faq_title_font_color' ) . '"';
			}

			if ( get_field( 'intense_faq_content_font' ) != '' ) {
				$content_font_color = ' content_font_color="' . get_field( 'intense_faq_content_font' ) . '"';
			}

			$item_classes = '';
			$item_cats = get_the_terms( $post->ID, 'faq_category' );

			if ( $item_cats ) {
				foreach ( $item_cats as $item_cat ) {
					$item_classes .= $item_cat->slug . ' ';
				}
			}

			if ( ( $show_filter && $categories == '' ) || ( $iscat_array && $show_filter ) ) {
				$item_classes .= 'intense mix ';
			}

			if ( !$single_toggle && $expand_all ) {
				$set_active = ' active="1"';
			}

			$collapsibles_shortcode .= '[intense_collapse title="' . the_title_attribute( 'echo=0' ) . '" class="' . $item_classes . '"' . $faq_link . $faq_link_target . $css_class . $title_background_color . $content_background_color . $title_font_color . $content_font_color . $set_active . ']' . do_shortcode( get_the_content() ) . '[/intense_collapse]';

			$i++;
		}

		$collapsibles_shortcode .= "[/intense_collapsibles]";

		$collapsibles_shortcode .= "</div></div>";

		$output .= do_shortcode( $collapsibles_shortcode );

		if ( ( $show_filter && $categories == '' ) || ( $iscat_array && $show_filter ) ) {
			// mixitup jquery plugin
			intense_add_script( 'mixitup' );

			$output .= "<script> jQuery(function($){";
			$output .= " $(document).ready(function() {";
			$output .= "  $('[id^=\"" . $faqid . "-accord\"]').mixitup({";

			if ( strlen( $filter_effects ) > 0 ) {
				$output .= "   effects: ['" . str_replace( ',', '\',\'', $filter_effects ) . "'],";
			} else {
				$output .= "   effects: ['fade','scale'],";
			}

			if ( strlen( $filter_easing ) > 0 ) {
				$output .= "   easing: '" . $filter_easing . "'";
			} else {
				$output .= "   easing: 'smooth'";
			}

			$output .= "  }); $('.mix').css('visibility', 'visible');";
			$output .= " });";
			$output .= "}); </script>";

			$output .= "<style type='text/css'>";
			$output .= " #" . $faqid . " {";
			$output .= "  overflow: hidden; }";
			$output .= " #" . $faqid . " .mix {";
			$output .= "  width: 100%;";
			$output .= "  opacity: 0;";
			$output .= "  display: none; }";
			$output .= " #" . $faqid . " h3, #" . $faqid . " h1 {";
			$output .= "  line-height: normal !important; }";
			$output .= " #" . $faqid . " div.mix {";
			$output .= "  vertical-align: top; }";
			$output .= "</style>";
		}

		$output .= intense_return_content_nav( 'nav-below' );

		$wp_query = null;
		$wp_query = $original_query;
		wp_reset_postdata();

		return $output;
	}

	function render_dialog_script() {
		?>
		<script>
			jQuery(function($) {
				$("input:checkbox[name ='show_filter']").change(function() {
					if ( jQuery('#show_filter').is(':checked') ) {
						$(".filtersettings").show();
					} else {
						$(".filtersettings").hide();
					}
				});
			});
		</script>
		<?php
	}
}
