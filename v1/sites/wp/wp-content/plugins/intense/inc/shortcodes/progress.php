<?php

class Intense_Progress extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Progress', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-progress-2';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'progress_segment' => array(
				'type' => 'repeater',
				'title' => __( 'Segments', 'intense' ),
				'description' => __( 'If animated, <code>{0}</code> found within the text will be replaced with the percent (ex. My percent: {0}% -> My percent: 50%)', 'intense' ),
				'fields' => array(
					'color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
					),
					'value' => array(
						'type' => 'text',
						'title' => __( 'Value', 'intense' ),
                        'skinnable' => '0'
					),
					'text' => array(
						'type' => 'text',
						'title' => __( 'Text', 'intense' ),
                        'skinnable' => '0'
					),					
				)
			),
			'colors' => array( 'type' => 'deprecated', 'description' => 'use intense_progress_segment shortcode instead' ),
			'values' => array( 'type' => 'deprecated', 'description' => 'use intense_progress_segment shortcode instead' ),
			'texts' => array( 'type' => 'deprecated', 'description' => 'use intense_progress_segment shortcode instead' ),
			'animation' => array(
				'type' => 'checkbox',
				'title' => __( 'Animated', 'intense' ),
				'default' => "0",
			),
			'animation_speed' => array(
				'type' => 'text',
				'title' => __( 'Animation Tick Speed', 'intense' ),
				'description' => __( 'amount in milliseconds', 'intense' ),
				'default' => "35"
			),
			'animate_text' => array( 'type' => 'deprecated', 'description' => 'use intense_progress_segment shortcode instead' ),
			'gradient' => array(
				'type' => 'checkbox',
				'title' => __( 'Gradient', 'intense' ),
				'default' => "0",
			),
			'size' => array(
				'type' => 'dropdown',
				'title' => __( 'Size', 'intense' ),
				'default' => 'medium',
				'options' => array(
					'mini' => __( 'mini', 'intense' ),
					'small' => __( 'small', 'intense' ),
					'medium' => __( 'medium', 'intense' ),
					'large' => __( 'large', 'intense' ),
					'mega' => __( 'mega', 'intense' ),
				)
			),
			'border_radius' => array(
				'type' => 'border_radius',
				'title' => __( 'Border Radius', 'intense' ),
			),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_progress_animate_text;
		global $intense_progress_rtl;
		global $intense_progress_height;
		global $intense_progress_font_size;
		global $intense_progress_animation;
		global $intense_progress_gradient;

		$intense_progress_animate_text = $animate_text;
		$intense_progress_rtl = $rtl;
		$intense_progress_animation = $animation;
		$intense_progress_gradient = $gradient;

		$radius = '';

		if ( isset( $border_radius ) ) {
			$radius = ' border-radius:' . $border_radius . ';';
		}

		$color_list = explode( ",", str_replace( " ", "", $colors ) );
		$value_list = explode( ",", str_replace( " ", "", $values ) );
		$texts_list = explode( ",", $texts );

		intense_add_script( 'jquery.appear' );
		intense_add_script( 'intense.progress' );

		switch ( $size ) {
		case 'mini':
			$height = 10;
			$font_size = 8;
			break;
		case 'small':
			$height = 20;
			$font_size = 10;
			break;
		case 'large':
			$height = 40;
			$font_size = 14;
			break;
		case 'mega':
			$height = 50;
			$font_size = 16;
			break;
		case 'medium':
		default:
			$height = 30;
			$font_size = 12;
			break;
		}

		$intense_progress_height = $height;
		$intense_progress_font_size	= $font_size;

		$output = '<div class="intense progress" style="filter: none; height: ' . $height . 'px; line-height: ' . $height . 'px;' . $radius . '"' . ( $animation ? ' data-animate-speed="' . $animation_speed . '"' : '' ) . '>';
		$base_span_style = 'padding: 0 ' . $height / 2 . 'px; font-size: ' . $font_size . 'px;';

		// This makes the old method still work
		foreach ( $color_list as $i => $color ) {
			if ( !empty( $color ) ) {
				$output .= intense_run_shortcode( 'intense_progress_segment', array(
					'color' => $color,
					'value' => ( isset( $value_list[$i] ) ? $value_list[$i] : '' ),
					'text' => ( isset( $texts_list[$i] ) ? $texts_list[$i] : '' )
				));
			}
		}

		$output .= do_shortcode( $content );

		$output .= "</div>";

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
			jQuery(function($) {
				
			});
		</script>
		<?php
	}
}

class Intense_Progress_Segment extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Progress Segment', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-progress-2';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_progress';

		$this->fields = array(
			'color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Color', 'intense' ),
			),
			'value' => array(
				'type' => 'text',
				'title' => __( 'Value', 'intense' ),
				'composer_show_value' => true,
                'skinnable' => '0'
			),
			'text' => array(
				'type' => 'text',
				'title' => __( 'Text', 'intense' ),
				'description' => __( 'If animated, <code>{0}</code> will be replaced with the percent (ex. My percent: {0}% -> My percent: 50%)', 'intense' ),
				'composer_show_value' => true,
                'skinnable' => '0'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_progress_animate_text;
		global $intense_progress_rtl;
		global $intense_progress_height;
		global $intense_progress_font_size;
		global $intense_progress_animation;
		global $intense_progress_gradient;

		$animate_text = ( strpos($text, '{0}') >= 0 );

		if ( isset( $intense_progress_animate_text ) ) $animate_text = $intense_progress_animate_text;				

		$base_span_style = 'padding: 0 ' . $intense_progress_height / 2 . 'px; font-size: ' . $intense_progress_font_size . 'px;';

		intense_add_script( 'jquery.appear' );
		intense_add_script( 'intense.progress' );

		$output = '';

		$color = intense_get_plugin_color( $color );

		$gradient_css = '';

		if ( $intense_progress_gradient ) {
			$end_color = intense_adjust_color_brightness( $color, -60 );
			$gradient_css = intense_linear_gradient_css( $color, $end_color );
		} else {
			$gradient_css = "filter: none !important;";
		}

		if ( $intense_progress_animation ) {
			$width = 'data-progress="' . ( isset( $value ) ? $value : "0" ) . '" style="background: ' . $color  . '; ' . $gradient_css . '" data-animate-text="' . $animate_text . '"';
		} else {
			$width = 'style="width: ' . ( isset( $value ) ? $value : "0" ) . '%; background: ' . $color  . '; ' . $gradient_css . '"';
		}

		$output .= '<div role="progress-bar" aria-valuenow="' . ( isset( $value ) ? $value : "0" ) . '" aria-valuemin="0" aria-valuemax="100" class="intense progress-bar ' . ( $intense_progress_rtl ? 'rtl ' : '' ) . '" ' . $width . '>' .
			( isset( $text ) && strlen( $text ) > 0 ? '<span' . ( $intense_progress_animation ? ' style="display: none; ' . $base_span_style . '"' : ' style="' . $base_span_style . '"' ) . '>' . $text . '</span>' : "<span></span>" ) .
			'</div>';

		return $output;
	}
}
