<?php

class Intense_Highlight extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Highlight', 'intense' );
        $this->category = __( 'Typography', 'intense' );
        $this->icon = 'dashicons-intense-pen';
        $this->show_preview = true;
        $this->preview_content = __( "Highlighted Text", 'intense' );
        $this->vc_map_content = 'textfield';

        $this->fields = array(
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Color', 'intense' ),
                'default' => 'primary'
            ),
            'font_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Font Color', 'intense' ),
                'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                'default' => ''
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $color = intense_get_plugin_color( $color );
        $font_color = intense_get_plugin_color( $font_color );

        if ( empty( $font_color ) ) {
            $font_color = intense_get_contract_color( $color );
        }

        return "<span style='color: " . $font_color . "; background: " . $color  . "; padding: 0 3px;'>" . $content . "</span>";
    }
}
