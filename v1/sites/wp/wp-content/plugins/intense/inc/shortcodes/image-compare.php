<?php

class Intense_Image_Compare extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Image Compare', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-images';
		$this->show_preview = true;

		$this->fields = array(
			'image1' => array(
				'type' => 'image',
				'title' => __( 'Image 1', 'intense' ),
                'skinnable' => '0'
			),
			'image1_id' => array( 'type' => 'deprecated', 'description' => 'use image1 instead' ),
            'image1_url' => array( 'type' => 'deprecated', 'description' => 'use image1 instead' ),
			'image2' => array(
				'type' => 'image',
				'title' => __( 'Image 2', 'intense' ),
                'skinnable' => '0'
			),
			'image2_id' => array( 'type' => 'deprecated', 'description' => 'use image2 instead' ),
            'image2_url' => array( 'type' => 'deprecated', 'description' => 'use image2 instead' ),
			'size' => array(
				'type' => 'image_size',
				'title' => __( 'Image Size', 'intense' )
			),
			'slider_offset' => array(
				'type' => 'text',
				'title' => __( 'Slider Offset', 'intense' ),
				'description' => __( '0 - 100: default is 50', 'intense' ),
				'default' => '50'
			),
			'show_overlay' => array(
                'type' => 'checkbox',
                'title' => __( 'Show Overlay', 'intense' ),
                'default' => "1",
            ),
            'before_text' => array(
				'type' => 'text',
				'title' => __( 'Before Text', 'intense' ),
			),
			'after_text' => array(
				'type' => 'text',
				'title' => __( 'After Text', 'intense' ),
			),
			'shadow' => array(
                'type' => 'shadow',
                'title' => __( 'Shadow', 'intense' ),
            ),
            'border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Border Radius', 'intense' ),                
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';

		$slider_offset *= 0.01;

		$container_id = 'img_compare_' . rand();
		$shadow_style = "margin-bottom: 0;";

		intense_add_style( 'twentytwenty' );
		intense_add_script( 'jquery.event.move' );
		intense_add_script( 'twentytwenty' );

		if ( is_numeric( $image1 ) ) {
			$image1_id = $image1;
		} else if ( !empty( $image1 ) ) {
			$image1_url = $image1;
		}

		if ( is_numeric( $image2 ) ) {
			$image2_id = $image2;
		} else if ( !empty( $image2 ) ) {
			$image2_url = $image2;
		}

		if ( $image1_id != '' ) {
			$photo_info = wp_get_attachment_image_src( $image1_id, $size );
			$photo1_url = $photo_info[0];
		} else {
			$photo1_url = $image1_url;
		}

		if ( $image2_id != '' ) {
			$photo_info = wp_get_attachment_image_src( $image2_id, $size );
			$photo2_url = $photo_info[0];
		} else {
			$photo2_url = $image2_url;
		}

		if ( $shadow ) {
			$output .= "<div><div style='$shadow_style'>";
		}

		$output .= "<div id='" . $container_id . "' style='" . ( isset( $border_radius ) ? "border-radius: " . $border_radius . "; " : "" ) . "' class='twentytwenty-container'>
			  <img src='" . $photo1_url . "'>
			  <img src='" . $photo2_url . "'></div>";

		if ( $shadow ) {
			$output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow hidden' style='" . ( isset( $width ) ? 'width: ' . $width . '; height: auto;' : '' ) . "vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0;' alt='' /></div>";
		}

		$output .= '<script>
		jQuery(window).load(function() {
		  jQuery("#' . $container_id . '").twentytwenty({default_offset_pct: ' . $slider_offset . '});
		  jQuery("#' . $container_id . '").width(jQuery("#' . $container_id . '").children().first().width());
		  jQuery("#' . $container_id . '").parent().parent().next(".intense.shadow").width(jQuery("#' . $container_id . '").children().first().width()).removeClass("hidden");
		  ' . ( !$show_overlay ? 'jQuery("#' . $container_id . '").children(".twentytwenty-overlay").remove();' : '' ) . '
		});</script>';

		if ( $show_overlay ) {
			$output .= '<style>
			#' . $container_id . ' .twentytwenty-before-label:before {
				content: "' . $before_text . '";
			}
			#' . $container_id . ' .twentytwenty-after-label:before {
				content: "' . $after_text . '";
			}
		</style>';
		}

		return $output;
	}

	function render_dialog_script() {
	?>
		<script>
			jQuery(function($) {
				// Uploading files
		  		$('.upload_image_button').live('click', function( event ){
	              event.preventDefault();
	              var target = $(this).data('target-id');
	        
	              window.parent.loadImageFrame($(this), function(attachment) {
	                jQuery("#" + target).val(attachment.id);
	                
	                if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }

	                if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
	              });
	          });
			});
		</script>
	<?php
	}
}
