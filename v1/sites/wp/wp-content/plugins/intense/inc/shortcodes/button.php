<?php

class Intense_Button extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Button', 'intense' );
        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-intense-progress-0';
        $this->show_preview = true;
        $this->preview_content = __( "Button Text", 'intense' );
        $this->vc_map_content = 'textarea';

        $this->fields = array(
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
                    'id' => array(
                        'type' => 'text',
                        'title' => __( 'ID', 'intense' ),
                        'description' => __( 'the client-side id (optional)', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Color', 'intense' ),
                        'default' => 'primary'
                    ),
                    'font_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Font Color', 'intense' ),
                        'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                    ),
                    'hover_brightness' => array(
                        'type' => 'text',
                        'title' => __( 'Hover Brightness', 'intense' ),
                        'default' => '-20',
                        'description' => __( 'The amount of brightness to adjust the button when the mouse is hovered over. A positive number will make it lighter and a negative number darker.', 'intense' ),
                    ),
                    'size' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Size', 'intense' ),
                        'default' => 'default',
                        'options' => array(
                            'default' => __( 'default', 'intense' ),
                            'mini' => __( 'mini', 'intense' ),
                            'small' => __( 'small', 'intense' ),
                            'medium' => __( 'medium', 'intense' ),
                            'large' => __( 'large', 'intense' )
                        )
                    ),
                    'link' => array(
                        'type' => 'text',
                        'title' => __( 'Link', 'intense' ),
                        'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'target' => array(
                        'type' => 'link_target',
                        'title' => __( 'Link Target', 'intense' ),
                        'default' => '_self',
                    ),
                    'title' => array(
                        'type' => 'text',
                        'title' => __( 'Title', 'intense' ),
                        'description' => __( 'title to show when the mouse is hovered over the button', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'icon' => array(
                        'type' => 'icon',
                        'title' => __( 'Icon', 'intense' ),
                        'default' => '',
                        'class' => 'select2'
                    ),
                    'icon_position' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Icon Position', 'intense' ),
                        'default' => 'left',
                        'options' => array(
                            'left' => __( 'left', 'intense' ),
                            'right' => __( 'right', 'intense' ),
                        )
                    ),
                    'border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Border Radius', 'intense' ),
                        'default' => '0'
                    ),
                    'class' => array(
                        'type' => 'text',
                        'title' => __( 'CSS Class', 'intense' ),
                        'default' => ''
                    ),
                    'gradient' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Gradient', 'intense' ),
                        'default' => "0",
                    ),
                    'block' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Block', 'intense' ),
                        'default' => "0",
                    ),
                    'angle_3d' => array(
                        'type' => 'dropdown',
                        'title' => __( '3D Angle', 'intense' ),
                        'default' => '-1',
                        'options' => array(
                            '-1' => 'None',
                            '0' => '0',
                            '45' => '45',
                            '90' => '90',
                            '135' => '135',
                            '180' => '180',
                            '225' => '225',
                            '270' => '270',
                            '315' => '315'
                        )
                    ),
                    'size_3d' => array(
                        'type' => 'text',
                        'title' => __( '3D Size', 'intense' ),
                        'default' => '3',
                        'class' => '3dsettings'
                    ),
                    'hover_size_3d' => array(
                        'type' => 'text',
                        'title' => __( 'Hover 3D Size', 'intense' ),
                        'class' => '3dsettings'
                    ),
                )
            ),
            'padding_tab' => array(
                'type' => 'tab',
                'title' => __( 'Padding', 'intense' ),
                'fields' =>  array(
                    'padding_top' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Top', 'intense' ),
                    ),
                    'padding_bottom' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Bottom', 'intense' ),
                    ),
                    'padding_left' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Left', 'intense' ),
                    ),
                    'padding_right' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Right', 'intense' ),
                    ),
                )
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $random = rand();
        $smooth_scroll = null;

        if ( is_numeric( $padding_top ) && $padding_top != 0 ) $padding_top .= "px";
        if ( is_numeric( $padding_bottom ) && $padding_bottom != 0 ) $padding_bottom .= "px";
        if ( is_numeric( $padding_left ) && $padding_left != 0 ) $padding_left .= "px";
        if ( is_numeric( $padding_right ) && $padding_right != 0 ) $padding_right .= "px";

        $padding = ( !empty( $padding_top ) ? ' padding-top: ' . $padding_top . '; ' : '' ) .
            ( !empty( $padding_bottom ) ? ' padding-bottom: ' . $padding_bottom . '; ' : '' ) .
            ( !empty( $padding_left ) ? ' padding-left: ' . $padding_left . '; ' : '' ) .
            ( !empty( $padding_right ) ? ' padding-right: ' . $padding_right . '; ' : '' );

        switch ( $size ) {
        case 'mini':
            $size = 'xs';
            break;
        case 'small':
            $size = 'sm';
            break;
        case 'medium':
            $size = 'md';
            break;
        case 'large':
            $size = 'lg';
            break;
        }

        if ( $color == 'link' ) {
            $color = 'transparent';
            $font_color = intense_get_plugin_color( 'primary' );
        }

        $color = intense_get_plugin_color( $color );

        if ( intense_get_web_color_hex( $color ) ) {
            $color = intense_get_web_color_hex( $color );
        }

        $font_color = intense_get_plugin_color( $font_color );

        if ( empty( $font_color ) ) {
            $font_color = intense_get_contract_color( $color );
        }

        if ( $color == 'transparent' ) {
            $hover_color = $color;
        } else {
            $hover_color = intense_adjust_color_brightness( $color, $hover_brightness );
        }

        $gradient_css = '';

        if ( $gradient && $color != 'transparent' ) {
            $end_color = intense_adjust_color_brightness( $color, -60 );
            $gradient_css = intense_linear_gradient_css( $color, $end_color );
        }

        $icon_left = '';
        $icon_right = '';

        if ( !empty( $icon ) ) {
            $icon = intense_run_shortcode( 'intense_icon', array( 'type' => $icon, 'color' => $font_color ) );

            if ( $icon_position == "left" ) {
                $icon_left = $icon . " ";
            } else {
                $icon_right = " " . $icon;
            }
        }

        if ( is_numeric( $border_radius ) ) $border_radius .= "px";

        $radius = '';

        if ( $border_radius != '' ) {
            $radius = ' border-radius:' . $border_radius . ';';
        } else {
            $radius = ' border-radius: 0;';
        }

        if ( $gradient ) {
            $class .= " gradient";
        }

        $angle_3d_style = '';
        $hover_angle_3d_style = '';

        if ( !empty( $angle_3d ) || $angle_3d == 0 ) {
            $x_offset = 0;
            $y_offset = 0;
            $x_hover_offset = 0;
            $y_hover_offset = 0;
            $color_3d = intense_adjust_color_brightness( $color, -50 );
            $hover_color_3d = intense_adjust_color_brightness( $hover_color, -50 );
            $angle_3d_style = '';            

            if ( is_null( $hover_size_3d ) ) $hover_size_3d = $size_3d;

            switch ( $angle_3d ) {
            case '0':
                $x_offset = $size_3d;
                $x_hover_offset = $hover_size_3d;
                $hover_margin_left = ( $size_3d - $hover_size_3d );
                break;
            case '45':
                $x_offset = $size_3d;
                $y_offset = -$size_3d;
                $x_hover_offset = $hover_size_3d;
                $y_hover_offset = -$hover_size_3d;
                $hover_margin_left = ( $size_3d - $hover_size_3d );
                $hover_margin_top = -( $size_3d - $hover_size_3d );
                break;
            case '90':
                $y_offset = -$size_3d;
                $y_hover_offset = -$hover_size_3d;
                $hover_margin_top = -( $size_3d - $hover_size_3d );
                break;
            case '135':
                $x_offset = -$size_3d;
                $y_offset = -$size_3d;
                $x_hover_offset = -$hover_size_3d;
                $y_hover_offset = -$hover_size_3d;
                $hover_margin_left = -( $size_3d - $hover_size_3d );
                $hover_margin_top = -( $size_3d - $hover_size_3d );
                break;
            case '180':
                $x_offset = -$size_3d;
                $x_hover_offset = -$hover_size_3d;
                $hover_margin_left = -( $size_3d - $hover_size_3d );
                break;
            case '225':
                $x_offset = -$size_3d;
                $y_offset = $size_3d;
                $x_hover_offset = -$hover_size_3d;
                $y_hover_offset = $hover_size_3d;
                $hover_margin_left = -( $size_3d - $hover_size_3d );
                $hover_margin_top = ( $size_3d - $hover_size_3d );
                break;
            case '270':
                $y_offset = $size_3d;
                $y_hover_offset = $hover_size_3d;
                $hover_margin_top = ( $size_3d - $hover_size_3d );
                break;
            case '315':
                $x_offset = $size_3d;
                $y_offset = $size_3d;
                $x_hover_offset = $hover_size_3d;
                $y_hover_offset = $hover_size_3d;
                $hover_margin_left = ( $size_3d - $hover_size_3d );
                $hover_margin_top = ( $size_3d - $hover_size_3d );
                break;
            }

            if ( $x_offset != 0 || $y_offset != 0 ) {
                $angle_3d_style = ' box-shadow: ' . $x_offset . 'px ' . $y_offset . 'px 0px 0px ' . $color_3d . ';';
            }

            if ( $x_hover_offset != 0 || $y_hover_offset != 0 || $hover_size_3d != $size_3d ) {
                $hover_angle_3d_style = ' box-shadow: ' . $x_hover_offset . 'px ' . $y_hover_offset . 'px 0px 0px ' . $hover_color_3d . ' !important;';
            }

            if ( $hover_size_3d != $size_3d ) {
                if ( !empty( $hover_margin_top ) ) $hover_angle_3d_style .= ' margin-top: ' . $hover_margin_top . 'px !important;';
                if ( !empty( $hover_margin_bottom ) )$hover_angle_3d_style .= ' margin-bottom: ' . $hover_margin_bottom . 'px !important;';
                if ( !empty( $hover_margin_left ) ) $hover_angle_3d_style .= ' margin-left: ' . $hover_margin_left . 'px !important;';
                if ( !empty( $hover_margin_right ) ) $hover_angle_3d_style .= ' margin-right: ' . $hover_margin_right . 'px !important;';
            }
        }

        $GLOBALS['IntensePluginInlineStyle']['#btn_' . $random] =
            "
        #btn_$random:hover {
            filter: none !important;
            background: none !important;
            background-color: $hover_color !important;
            $hover_angle_3d_style
        }";

        if ( isset( $link ) ) {
            intense_add_script( "smooth-scroll" );
            $smooth_scroll = '<script>jQuery(document).ready(function() { jQuery(\'#btn_' . $random . '\').smoothScroll({ speed: 1200 }); });</script>';
        }

        if ( is_numeric( $link ) ) {
            $link = get_permalink( $link );
        }

        if ( empty( $id ) ) $id = "btn_$random";

        return "<a id='$id' href='$link' target='$target' " . ( !empty( $title ) ? 'title="' . $title . '"' : '' ) . " style='color: " . $font_color . " !important; $radius $angle_3d_style $padding background: " . $color  . "; " . $gradient_css . ";' class='intense $class btn" . ( $size != '' ? " btn-$size" : "" ) . ( $block ? ' btn-block': '' ) . " '>$icon_left" . do_shortcode( $content ) . "$icon_right</a>$smooth_scroll";
    }

    function render_dialog_script() {
?>
    <script>
      jQuery(function($) {
        if ( $("#angle_3d").val() == -1 ) $(".3dsettings").hide();

        $("#angle_3d").change(function() {
          var angle = $(this).val();

          $(".3dsettings").hide();

          if ( angle > -1 ) {
            $(".3dsettings").show();
          }
        });
      });
    </script>
    <?php
    }
}
