<?php

class Intense_Masonry extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Masonry', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-screenoptions';
		$this->show_preview = false;
		$this->is_container = true;

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
				'description' => __( 'optional', 'intense' ),
                'skinnable' => '0'
			),			
			'gutter' => array(
				'type' => 'text',
				'title' => __( 'Gutter', 'intense' ),
				'description' => __( 'measured in px', 'intense' ),
				'default' => '10',				
			),
			'masonry_item' => array(
				'type' => 'repeater',
				'title' => __( 'Items', 'intense' ),
				'fields' => array(
					'width' => array(
						'type' => 'text',
						'title' => __( 'Manual Width', 'intense' ),
						'description' => __( 'measured as a % or px amount', 'intense' ),
					),
					'size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'medium_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Medium Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'extra_small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Extra Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
				)
			),			
			'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_masonry_gutter;

		$intense_masonry_gutter = $gutter;

		if ( empty( $intense_masonry_gutter ) ) $intense_masonry_gutter = '0';
		if ( empty( $id ) ) $id = rand();

		intense_add_script( 'packery' );		

		$init_script = '<script>
			function intenseInitMasonry() {
				var $ = jQuery.noConflict();
					var $container = $(".intense.masonry");
					$container.packery({
					  itemSelector: ".item",
					  gutter: 0,
					  isOriginLeft: ' . ( $rtl ? 'false' : 'true' ) . '
					});
				return $container;
			}

			jQuery(function($){ $(window).load(function() { 
				intenseInitMasonry();
            }); });</script>';

		return $init_script . '<div id="' . $id . '" class="intense masonry" style="position: relative;">' . do_shortcode( $content ) . '</div><div class="clearfix"></div>';
	}
}

class Intense_Masonry_Item extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Masonry Item', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-screenoptions';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_masonry';
		$this->is_container = true;

		$this->fields = array(
			'width' => array(
				'type' => 'text',
				'title' => __( 'Manual Width', 'intense' ),
				'description' => __( 'measured as a % or px amount', 'intense' ),
			),
			'size' => array(
				'type' => 'dropdown',
				'title' => __( 'Size', 'intense' ),
				'composer_show_value' => true,
				'options' => array(
					'' => '',
					'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
					'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
				)
			),
			'medium_size' => array(
				'type' => 'dropdown',
				'title' => __( 'Medium Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
				'composer_show_value' => true,
				'options' => array(
					'' => '',
					'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
					'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
				)
			),
			'small_size' => array(
				'type' => 'dropdown',
				'title' => __( 'Small Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
				'composer_show_value' => true,
				'options' => array(
					'' => '',
					'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
					'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
				)
			),
			'extra_small_size' => array(
				'type' => 'dropdown',
				'title' => __( 'Extra Small Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
				'composer_show_value' => true,
				'options' => array(
					'' => '',
					'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
					'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_masonry_gutter;

		$size_class = '';

		if ( empty( $width ) && empty( $size ) && empty( $medium_size ) && empty( $small_size ) && empty( $extra_small_size ) ) {
			$width = 50 * ( rand(0,1) ? .5 : 1 ); //50% or 25% randomly
		}

		if ( is_numeric( $width ) ) $width .= '%';

		if ( !empty( $size ) || !empty( $medium_size ) || !empty( $small_size ) || !empty( $extra_small_size ) ) {
			$size_class = " intense col-lg-$size " .
				( isset( $extra_small_size ) ? "col-xs-$extra_small_size ": "col-xs-12 " ) .
				( isset( $small_size ) ? "col-sm-$small_size " : "col-sm-12 " ) .
				( isset( $medium_size ) ? "col-md-$medium_size " : "col-md-$size " ) .
				"nogutter ";
			$width = null;
		}

		return '<div class="item' . $size_class . '" style="float:left; overflow: hidden; ' . ( !empty( $width ) ? 'width: ' . $width  . ';' : '' ) . '"><div style="margin: 0 ' . $intense_masonry_gutter / 2 . 'px ' . ( $intense_masonry_gutter ). 'px;">' . do_shortcode( $content ) . '</div></div>';
	}
}
