<?php

class Intense_Shortcode {
    protected $fields = array();
    public $title;
    public $category;
    public $icon;
    public $show_preview;
    public $preview_content;
    public $preview_wrapper_start;
    public $preview_wrapper_end;
    public $is_container = false;
    public $map_visual_composer = true;

    function __construct() {

    }

    public function get_field( $key ) {
        return $this->fields[ $key ];
    }

    public function save_preset( $key, $content ) {
        if ( empty( $key ) ) {
            return false;
        }

        $key = $this->get_preset_key( $key );
        $presets = get_option( 'intense_shortcode_presets' );

        if ( !is_array( $presets ) ) $presets = array();

        $presets[ $key ] = $content;

        update_option( 'intense_shortcode_presets', $presets );

        return true;
    }

    public function get_presets( ) {
        $presets = get_option( 'intense_shortcode_presets' );

        //presets for this shortcode will start with the class name
        $shortcode_presets = array();

        if ( !empty( $presets ) ) {
            foreach ( $presets as $key => $value ) {
                if ( stripos( $key, strtolower( get_class( $this ) ) ) === 0 ) {
                    $shortcode_presets[ $key ] = $value;
                }
            }
        }

        return $shortcode_presets;
    }

    public function load_preset( $key ) {
        if ( !empty( $key ) && $key != '' ) {
            $attr = $this->get_preset_attributes( $key );
            $first_field = current( $this->fields );
            $has_tabs = ( $first_field['type'] === 'tab' );

            if ( !empty( $attr ) ) {
                foreach ( $attr as $id => $value ) {
                    if ( !$has_tabs && isset( $this->fields[ $id ] ) ) {
                        $this->fields[ $id ]['value'] = $value;
                    } else if ( $has_tabs ) {
                            foreach ( $this->fields as $tab => $settings ) {
                                if ( isset( $settings['fields'][ $id ] ) ) {
                                    $this->fields[ $tab ]['fields'][ $id ]['value'] = $value;
                                }
                            }
                        }
                }
            }
        }
    }

    public static function parse_atts( $m ) {
        global $attr;

        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr( $m[0], 1, -1 );
        }

        $tag = $m[2];
        $attr = shortcode_parse_atts( $m[3] );

        return '';
    }

    public function get_preset_attributes( $key ) {
        global $attr;

        $key = $this->get_preset_key( $key );
        $presets = get_option( 'intense_shortcode_presets' );
        $pattern = get_shortcode_regex();
        $shortcode = $presets[ $key ];
        $attr = null;

        preg_replace_callback( "/$pattern/s",
            array( __CLASS__, 'parse_atts' ),
            $shortcode );

        return $attr;
    }

    public function remove_preset( $key ) {
        $key = $this->get_preset_key( $key );
        $presets = get_option( 'intense_shortcode_presets' );

        unset( $presets[ $key ] );

        update_option( 'intense_shortcode_presets', $presets );
    }

    protected function get_preset_key( $key ) {
        return sanitize_key( get_class( $this ) . "-" . $key );
    }

    static public function load_shortcode( $m ) {
        global $shortcode_tags;

        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr( $m[0], 1, -1 );
        }

        $tag = $m[2];
        $attr = shortcode_parse_atts( $m[3] );

        return $attr;
    }

    public function get_shortcode_defaults( $fields = null, $atts, $is_skin_export = false ) {
        global $intense_visions_options;
        global $intense_skins;

        if ( !$is_skin_export ) {
            if ( !empty( $atts['skin'] ) ) {
                $skin = $intense_skins->load_skin( $atts['skin'] );
            }

            if ( empty( $skin ) ) {
                $skin = $intense_skins->load_skin( $intense_visions_options['intense_skin'] );
            }
        }

        if ( $fields == null ) {
            $fields = $this->fields;
        }

        $defaults = array();

        foreach ( $fields as $key => $value ) {
            $type = $value['type'];

            if ( $type == 'tab' ) {
                $defaults = array_merge( $defaults, $this->get_shortcode_defaults( $value['fields'], $atts, $is_skin_export ) );
            } else {
                // don't skin non skinnable fields
                if ( !$is_skin_export || !isset( $value['skinnable'] ) || $value['skinnable'] != '0' ) {

                    // get default value from fields array
                    if ( isset( $value['default'] ) ) {
                        $defaults[ $key ] = $value['default'];
                    } else {
                        $defaults[ $key ] = null;
                    }
                }

                // don't skin hidden fields
                if ( $is_skin_export && $value['type'] == 'hidden' ) {
                    unset( $defaults[ $key ] );
                }
            }
        }

        if ( isset( $skin ) ) {
            $skin_defaults = $skin->get_shortcode_defaults( strtolower( get_class( $this ) ) );

            if ( is_array( $skin_defaults ) ) {
                $defaults = array_merge( $defaults, $skin_defaults );
            }
        }

        return $defaults;
    }

    public function render_presets() {
        $output = '';
        $presets = $this->get_presets();

        ksort( $presets );

        foreach ( $presets as $key => $value ) {
            $key_name = substr( $key, stripos( $key, '-' ) + 1 );

            $output .= '<div class="preset-list-item-wrapper" id="preset-list-item-' . esc_attr( $key_name ) . '"><span class="preset-list-item">' . esc_html( $key_name ) . '</span> <span class="preset-list-remove">&times;</span></div>';
        }

        if ( !is_array( $presets ) || count( $presets ) == 0 ) {
            $output .= __( "No presets yet! Change the shortcode settings, type in a name, and press save below.", 'intense' );
        }

        $output .= '<div class="clearfix"></div>';

        return $output;
    }

    public function render_dialog( $is_skin_dialog = false, $skin_key = null ) {
        require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/field.php';

        ob_start();

        global $intense_visions_options;
        global $intense_skins;

        $ajax_nonce = wp_create_nonce( "intense-plugin-noonce" );
        $tab_started = false;
        $tab_contents = '';
        $has_repeater = false;

        // Load the skin so that the defaults can be used when rendering the dialog
        if ( empty( $skin_key ) ) {
            $skin = $intense_skins->load_skin( $intense_visions_options['intense_skin'] );
        } else {
            $skin = $intense_skins->load_skin( $skin_key );
        }
?>
        <script>
            function intenseRepeaterShortcodes() {
                var $ = jQuery.noConflict();
                var i = 0;
                var shortcodes = '';

                $('.repeater-item').each(function() {
                    shortcodes += intenseRepeaterShortcode(i) + "\n";
                    i++;
                });

                return shortcodes;
            }
        </script>
        <?php
        echo '<div class="intense-shortcode-title"><i class="dashicons ' . $this->icon . '" style="font-size:22px; padding-right: 15px; margin-top:-3px;"></i>' . $this->title . '</div>';
        echo '<form id="shortcode-form" name="shortcode-form" data-shortcode="' . strtolower( get_class( $this ) ) . '" action="#" class="intense form-horizontal" role="form">';

        // if ( !$is_skin_dialog ) {
        //     echo '<div class="pull-right">';
        //     echo '<div id="presets" class=" pull-right button button-secondary button-large">' . __( 'Presets', 'intense' ) . '</div>';
        //     echo '<div id="preset-list" class="clearfix" style="display:none; float: right;">';
        //     echo '<div id="preset-list-content">';
        //     echo $this->render_presets();
        //     echo '</div>';
        //     echo '<div id="preset-list-footer"> ';
        //     echo __( 'Add', 'intense' );
        //     echo ' <input type="text" name="key" id="key" placeholder="' . __( 'Name', 'intense' ) . '"/> ';
        //     echo '<input type="button" class="button button-primary button-large" name="save" id="save" value="' . __( "Save", 'intense' ) . '" data-function="intenseRenderShortcode" data-shortcode-class="' . get_class( $this ) . '" />';
        //     echo '</div>';
        //     echo '</div>';
        //     echo '</div>';
        // }

        echo '<input type="hidden" id="nonce" value="' . $ajax_nonce . '" />';
        $script = '<script type="text/javascript">' .
            'function intenseRenderShortcode() {' . "\n\t" .
            'var $ = jQuery.noConflict();' . "\n\t" .
            'return \'[' . strtolower( get_class( $this ) ) . "' + ";

        if ( $skin->key != $intense_visions_options['intense_skin'] ) {
            $script .= ' \' skin="' . $skin->key . '"\' + ';
        }

        foreach ( $this->fields as $key => $value ) {
            $type = $value['type'];

            if ( $type == 'repeater' ) $has_repeater = true;

            if ( $type == 'tab' ) {
                $title = $value['title'];
                $tab_fields = $value['fields'];

                if ( !$tab_started ) {
                    echo '<ul class="nav nav-tabs">';
                    $tab_started = true;
                    $class = 'active';
                } else {
                    $class = '';
                }

                if ( isset( $value['class'] ) ) {
                    $class .= ' ' . $value['class'];
                }

                echo '<li class="' . $class . '"><a href="#' . $key . '" data-toggle="tab">' . $title . '</a></li>';
                $tab_contents .= '<div class="tab-pane ' . $class . '" id="' . $key . '">';

                foreach ( $tab_fields as $tab_key => $tab_value ) {
                    if ( !empty( $skin ) ) {
                        $skin_value = $skin->get_shortcode_default( strtolower( get_class( $this ) ), $tab_key );

                        if ( isset( $skin_value ) ) {
                            $tab_value['value'] = $skin_value;
                            $tab_value['default'] = $tab_value['value'];
                        }
                    }

                    $tab_value['is_skin_dialog'] = $is_skin_dialog;

                    if ( $tab_value['type'] == 'repeater' ) $has_repeater = true;

                    // show the field if it is a regular dialog
                    // don't show for skins if it is a repeater or if it isn't skinnable
                    if ( !$is_skin_dialog || ( $tab_value['type'] != 'repeater' && ( !isset( $tab_value['skinnable'] ) || $tab_value['skinnable'] ) ) ) {
                        $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $tab_value['type'] ) ) );
                        $field = new $field_class_name( $tab_key, $tab_value );
                        $tab_contents .= $field->render_field();
                        $script .= "\n\t " . $field->render_value_script();
                    }
                }

                $tab_contents .= '</div>';
            } else {
                if ( !empty( $skin ) ) {
                    $skin_value = $skin->get_shortcode_default( strtolower( get_class( $this ) ), $key );

                    if ( isset( $skin_value ) ) {                        
                        $value['value'] = $skin_value;
                        $value['default'] = $value['value'];
                    }
                }

                $value['is_skin_dialog'] = $is_skin_dialog;

                // show the field if it is a regular dialog
                // don't show for skins if it is a repeater or if it isn't skinnable
                if ( !$is_skin_dialog || ( $value['type'] != 'repeater' && ( !isset( $value['skinnable'] ) || $value['skinnable'] ) ) ) {
                    $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $type ) ) );
                    $field = new $field_class_name( $key, $value );
                    echo $field->render_field();
                    $script .= "\n\t " . $field->render_value_script();
                }
            }
        }

        if ( $tab_started ) {
            echo '</ul><div class="tab-content">' . $tab_contents . '</div>';
        }

        $script .= "\n\t " .'\']' .
            ( isset( $this->preview_content ) || $has_repeater || $this->is_container ? $this->preview_content . '\' + "\n" + intenseRepeaterShortcodes() + \'[/' . strtolower( get_class( $this ) ) . ']' : '' ) .
            '\';' . "\n " . '}';

        $script .= $this->render_init_script();
        $script .= '</script>';

        echo $script;

        if ( !$is_skin_dialog ) {
            echo $this->render_dialog_script();

            $this->form_actions( get_class() . '_submit', array(
                    'function' => 'intenseRenderShortcode',
                    'shortcode_class' => get_class( $this ),
                    'preview_content' => $this->preview_content
                ) );
        }

        echo "</form>";

        $output = ob_get_contents();
        ob_end_clean();

        echo $output;
    }

    public function render_init_script() {
        $has_tabs = false;
        $initialized_types = array();
        $script = "\n\t" . 'function intenseInitShortcode() {' . "\n\t" . 'var $ = jQuery.noConflict();' . "\n\t";

        foreach ( $this->fields as $key => $value ) {
            $type = $value['type'];

            if ( $type == 'tab' ) {
                $tab_fields = $value['fields'];
                $has_tabs = true;

                foreach ( $tab_fields as $tab_key => $tab_value ) {
                    if ( !array_key_exists( $tab_value['type'] , $initialized_types ) ) {
                        $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $tab_value['type'] ) ) );
                        $field = new $field_class_name( $tab_key, $tab_value );
                        $script .= $field->render_init_script();
                        $initialized_types[ $tab_value['type'] ] = true;
                    }

                    if ( $type == 'repeater' ) {
                        $repeater_fields = $value['fields'];

                        foreach ( $repeater_fields as $repeater_key => $repeater_value ) {
                            if ( !array_key_exists( $repeater_value['type'] , $initialized_types ) ) {
                                $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $repeater_value['type'] ) ) );
                                $field = new $field_class_name( $repeater_key, $repeater_value );
                                $script .= $field->render_init_script();
                                $initialized_types[ $repeater_value['type'] ] = true;
                            }
                        }
                    }
                }
            } else if ( $type == 'repeater' ) {
                    $repeater_fields = $value['fields'];

                    foreach ( $repeater_fields as $repeater_key => $repeater_value ) {
                        if ( $repeater_value['type'] != 'tab' && !array_key_exists( $repeater_value['type'] , $initialized_types ) ) {
                            $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $repeater_value['type'] ) ) );
                            $field = new $field_class_name( $repeater_key, $repeater_value );
                            $script .= $field->render_init_script();
                            $initialized_types[ $repeater_value['type'] ] = true;
                        }

                        if ( $repeater_value['type'] == 'tab' ) {
                            $tab_fields = $repeater_value['fields'];
                            $has_tabs = true;

                            foreach ( $tab_fields as $tab_key => $tab_value ) {
                                if ( !array_key_exists( $tab_value['type'] , $initialized_types ) ) {
                                    $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $tab_value['type'] ) ) );
                                    $field = new $field_class_name( $tab_key, $tab_value );
                                    $script .= $field->render_init_script();
                                    $initialized_types[ $tab_value['type'] ] = true;
                                }
                            }
                        }
                    }
                }

            if ( $type != 'tab' && !array_key_exists( $type , $initialized_types ) ) {
                $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $type ) ) );
                $field = new $field_class_name( $key, $value );
                $script .= $field->render_init_script();
                $initialized_types[ $type ] = true;
            }
        }

        $script .= '} jQuery(function($) { intenseInitShortcode();';

        if ( $has_tabs ) {
            $script .= '$(".nav.nav-tabs").click(intenseInitShortcode);';
        }

        $script .= ' });';

        return $script;
    }

    public function form_actions( $id, $args ) {
        extract( wp_parse_args( $args, array(
                    'function' => null,
                    'preview_content' => null,
                    'shortcode_class' => null,
                    'default' => null,
                    'style' => null,
                    'class' => null
                ) ), EXTR_SKIP );
?>
        <div class="form-actions">
            <div class="pull-left">
                <input type="button" class="button button-primary button-large" name="insert" value="<?php _e( "Insert Shortcode", 'intense' ); ?>" data-preview-content="<?php echo esc_attr( $preview_content ); ?>" data-function="<?php echo $function; ?>" data-shortcode-class="<?php echo $shortcode_class; ?>"/>
                <input type="button" class="button button-secondary button-large" name="preview" id="preview" value="<?php _e( "Preview", 'intense' ); ?>" data-function="<?php echo $function; ?>" data-shortcode-class="<?php echo $shortcode_class; ?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <?php
    }

    public function map_visual_composer() {
        if ( !$this->map_visual_composer ) return; //early terminate if mapping is disabled

        require_once INTENSE_PLUGIN_FOLDER . '/inc/fields/field.php';

        $has_repeater = false;        

        $vc_mapping = array(
            'name' => $this->title,
            'base' => strtolower( get_class( $this ) ),
            'class' => "intense_wpb",
            'icon' => 'intense-icon dashicons ' . $this->icon,            
            'category' => __( "Intense" ),
            'params' => array()
        );

        if ( isset( $this->parent ) ) {
            $vc_mapping[ "as_child" ] = array( 'only' => $this->parent );
        }

        if ( $this->is_container ) {
            // This array is a temporary fix since VC doesn't seem to handle
            // containers that can have anything in them. It maps the parent
            // child relationship for shortcodes that are directly tied to each other
            $parent_child_shortcodes = array(
                'intense_progress',
                'intense_masonry',
                'intense_pricing_table',
                'intense_tabs',
                'intense_timeline',
                'intense_row',
                'intense_slider',
                'intense_person',
                'intense_testimonies' );

            if ( class_exists( 'WPBakeryShortCodesContainer' ) && in_array( strtolower( get_class( $this ) ), $parent_child_shortcodes ) ) {
                require_once INTENSE_PLUGIN_FOLDER . '/inc/shortcodes/composer/intense_shortcodes_container.php';

                $vc_mapping[ "content_element" ] = $this->is_container;

                if ( !class_exists( "WPBakeryShortCode_" . get_class( $this ) ) ) {
                    eval( "class WPBakeryShortCode_" . get_class( $this ) . " extends IntenseShortCodesContainer {

                    };" );
                }

                foreach ( $this->fields as $key => $field ) {
                    $type = $field['type'];

                    if ( $type == 'tab' ) {
                        foreach ( $field['fields'] as $tab_key => $tab_field ) {
                            $tab_type = $tab_field['type'];
                            $this->vc_map_parent( $tab_type, $tab_key, $vc_mapping );
                        }
                    } else {
                        $this->vc_map_parent( $type, $key, $vc_mapping );
                    }
                }

                $vc_mapping[ "js_view" ] = 'VcColumnView';
            } else {
                $vc_mapping['params'][] =  array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Content", 'intense' ),
                    "param_name" => "content",
                    "value" => ""
                );
            }
        } else if ( isset( $this->vc_map_content ) ) {
                $vc_mapping[ 'params' ][] = array(
                    "type" => $this->vc_map_content,
                    "holder" => "div",
                    "heading" => __( "Content", 'intense' ),
                    "param_name" => "content",
                    "value" => ""
                );
            }

        foreach ( $this->fields as $key => $value ) {
            $type = $value['type'];

            if ( $type == 'repeater' ) $has_repeater = true;

            if ( $type == 'tab' ) {
                $title = $value['title'];
                $tab_fields = $value['fields'];

                $vc_mapping[ 'params' ][] = array(
                    "type" => "intense_tab",
                    //"holder" => "button",
                    "class" => "",
                    "heading" => $title,
                    "param_name" => "tab_" . rand()
                );

                foreach ( $tab_fields as $tab_key => $tab_value ) {
                    if ( $tab_value['type'] == 'repeater' ) $has_repeater = true;

                    $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $tab_value['type'] ) ) );
                    $field = new $field_class_name( $tab_key, $tab_value );
                    $param = $field->map_visual_composer();

                    if ( isset( $param ) ) {
                        $vc_mapping[ 'params' ][] = $param;
                    }
                }
            } else {
                $field_class_name = 'Intense_Field_' . str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $type ) ) );
                $field = new $field_class_name( $key, $value );
                $param = $field->map_visual_composer();

                if ( isset( $param ) ) {
                    $vc_mapping[ 'params' ][] = $param;
                }
            }
        }

        vc_map( $vc_mapping );
    }

    private function vc_map_parent( $type, $key, &$vc_mapping ) {
        if ( $type == 'repeater'
            && $key != 'column'
            && $key != 'masonry_item'
            && $key != 'pricing_section'
            && $key != 'slide'
            && $key != 'tab'
            && $key != 'timeline_event'
            && $key != 'person_custom_social'
            && $key != 'testimony' ) {
            if ( !class_exists( "WPBakeryShortCode_Intense_" . ucfirst( $key ) ) ) {
                eval( "class WPBakeryShortCode_Intense_" . ucfirst( $key ) . " extends WPBakeryShortCode {

                };" );
            }

            $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_' . $key );
        } else if ( $key == 'column' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_column' );
            } else if ( $key == 'masonry_item' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_masonry_item' );
            } else if ( $key == 'pricing_section' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_pricing_section' );
            } else if ( $key == 'slide' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_slide' );
            } else if ( $key == 'tab' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_tab' );
            } else if ( $key == 'timeline_event' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_timeline_event' );
            } else if ( $key == 'person_custom_social' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_person_custom_social' );
            } else if ( $key == 'testimony' ) {
                $vc_mapping[ "as_parent" ] = array( 'only' => 'intense_testimony' );
            }
    }

    public function to_documentation() {
        global $intense_animations;
        global $intense_icons;

        $categories = intense_get_animation_categories();
        $animation_list = '';
        $icon_list = '';

        foreach ( $categories as $category ) {
            $category_animations = array();

            $animation_list .= '<strong>' . $category . '</strong> - ';

            foreach ( $intense_animations as $animation => $attributes ) {
                $animation_source = $attributes[0];
                $animation_category = $attributes[1];
                $starthidden = $attributes[2];
                $endhidden = $attributes[3];

                if ( $category == $animation_category ) {
                    $category_animations[] = $animation;
                }
            }

            $animation_options[ $category ] = array();

            natsort( $category_animations );

            $animation_list .= implode( ', ', $category_animations );

            $animation_list .= '<br /><br />';
        }

        $advanced_color_list = '<strong>option colors</strong> - primary, error, info, inverse, muted, success, warning, link<br><br><strong>web color</strong> - hex, rgb, rgba, hsl, hsla, color name (see wikipedia <a href="http://en.wikipedia.org/wiki/Web_colors">web colors</a>)';

        $icon_list = implode( ', ', $intense_icons );

        $link_target_list = '_blank, _self, _parent, _top';

        $output = '';

        $output = '<h1>' . $this->title . '</h1>';

        $output .= '<table class="intense table"><tr><th>Attribute</th><th>Type</th><th>Dialog Title</th><th>Description</th><th>Default</th><th>Available Values</th></tr>';

        foreach ( $this->fields as $key => $value ) {
            $type = $value['type'];

            if ( $type == 'tab' ) {
                $tab_fields = $value['fields'];
                $has_tabs = true;

                foreach ( $tab_fields as $tab_key => $tab_value ) {
                    $tab_type = $tab_value['type'];

                    if ( $tab_type == 'repeater' ) {
                        //     $repeater_fields = $value['fields'];

                        //     $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td></td></tr>';
                    } else if ( $tab_type == 'dropdown' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            if ( isset( $tab_value['options'] ) ) {
                                foreach ( $tab_value['options'] as $option_key => $option_value ) {
                                    $output .= '<strong>' . $option_key . '</strong> - <em>' . $option_value . '</em><br />';
                                }
                            }

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'animation' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= $animation_list;

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'color_advanced' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= $advanced_color_list;

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'icon' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            //$output .= $icon_list;
                            $output .= 'see icon list in icon shortcode documentation';

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'link_target' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= $link_target_list;

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'checkbox' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? ( $tab_value['default'] ? '1' : '0' ) : '' ) . '</td><td>';

                            $output .= '1 = true, 0 = false';

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'border_radius' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= '%, em, px - examples: 25% or 10em or 20px';

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'border' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= 'standard CSS border style';

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'image' || $tab_type == 'video' || $tab_type == 'file' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td>';

                            $output .= 'WordPress ID or any URL';

                            $output .= '</td></tr>';
                        } else if ( $tab_type == 'shadow' ) {
                            $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' ) . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td style="width:400px">';

                            $output .= '<table>
                            <tr><td>0</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow0.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>1</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow1.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>2</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow2.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>3</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow3.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>4</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow4.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>5</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow5.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>6</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow6.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>7</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow7.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>8</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow8.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>9</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow9.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>10</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow10.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>11</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow11.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>12</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow12.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>13</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow13.png" style="vertical-align: top;" /></td></tr>
                            <tr><td>14</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow14.png" style="vertical-align: top;" /></td></tr>
                        </table>';

                            $output .= '</td></tr>';
                        } else {
                        $output .= "<tr><td>" . $tab_key . '</td><td>' . $tab_value['type'] . '</td><td>' . ( !empty( $tab_value['title'] ) ? $tab_value['title'] : '' ) . '</td><td>' . ( !empty( $tab_value['description'] ) ? $tab_value['description'] : '' )  . '</td><td>' . ( !empty( $tab_value['default'] ) ? $tab_value['default'] : '' ) . '</td><td></td></tr>';
                    }
                }
            } else if ( $type == 'repeater' ) {
                    //     $repeater_fields = $value['fields'];

                    //     $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td></td></tr>';
                } else if ( $type == 'dropdown' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    if ( isset( $value['options'] ) ) {
                        foreach ( $value['options'] as $option_key => $option_value ) {
                            if ( is_array( $option_value ) ) {
                                foreach ( $option_value as $optgroup_key => $optgroup_value ) {
                                    $output .= '<strong>' . $optgroup_key . '</strong> - <em>' . $optgroup_value . '</em><br />';
                                }
                            } else {
                                $output .= '<strong>' . $option_key . '</strong> - <em>' . $option_value . '</em><br />';
                            }
                        }
                    }

                    $output .= '</td></tr>';
                } else if ( $type == 'animation' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= $animation_list;

                    $output .= '</td></tr>';
                } else if ( $type == 'color_advanced' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= $advanced_color_list;

                    $output .= '</td></tr>';
                } else if ( $type == 'icon' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    //$output .= $icon_list;
                    $output .= 'see icon list in icon shortcode documentation';

                    $output .= '</td></tr>';
                } else if ( $type == 'link_target' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= $link_target_list;

                    $output .= '</td></tr>';
                } else if ( $type == 'checkbox' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? ( $value['default'] ? '1' : '0' ) : '' ) . '</td><td>';

                    $output .= '1 = true, 0 = false';

                    $output .= '</td></tr>';
                } else if ( $type == 'border_radius' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= '%, em, px - examples: 25% or 10em or 20px';

                    $output .= '</td></tr>';
                } else if ( $type == 'border' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= 'standard CSS border style';

                    $output .= '</td></tr>';
                }else if ( $type == 'image' || $type == 'video' || $type == 'file' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td>';

                    $output .= 'WordPress ID or any URL';

                    $output .= '</td></tr>';
                } else if ( $type == 'shadow' ) {
                    $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td style="width:400px">';

                    $output .= '<table>
                    <tr><td>0</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow0.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>1</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow1.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>2</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow2.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>3</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow3.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>4</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow4.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>5</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow5.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>6</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow6.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>7</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow7.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>8</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow8.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>9</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow9.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>10</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow10.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>11</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow11.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>12</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow12.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>13</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow13.png" style="vertical-align: top;" /></td></tr>
                    <tr><td>14</td><td><div style="height: 25px; background: #dfdfdf;"></div><img src="../img/shadows/shadow14.png" style="vertical-align: top;" /></td></tr>
                </table>';

                    $output .= '</td></tr>';
                } else {
                $output .= "<tr><td>" . $key . '</td><td>' . $value['type'] . '</td><td>' . ( !empty( $value['title'] ) ? $value['title'] : '' ) . '</td><td>' . ( !empty( $value['description'] ) ? $value['description'] : '' ) . '</td><td>' . ( !empty( $value['default'] ) ? $value['default'] : '' ) . '</td><td></td></tr>';
            }
        }

        $output .='</table>';

        return $output;
    }

    public function shortcode( $atts, $content = null ) {}
    public function render_dialog_script() {}
}
