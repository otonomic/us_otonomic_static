<?php

class Intense_Icon extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Icon', 'intense' );
    $this->category = __( 'Elements', 'intense' );
    $this->icon = 'dashicons-intense-uniF696';
    $this->show_preview = true;

    $this->fields = array(
      'type' => array(
        'type' => 'icon',
        'title' => __( 'Icon', 'intense' ),
        'default' => '',
        'composer_show_value' => true
      ),
      'size' => array(
        'type' => 'dropdown',
        'title' => __( 'Size', 'intense' ),
        'default' => '1',
        'options' => array( 
          '1' => '1', 
          '2' => '2', 
          '3' => '3', 
          '4' => '4', 
          '5' => '5' 
        )
      ),
      'color' => array(
          'type' => 'color_advanced',
          'title' => __( 'Color', 'intense' ),
          'default' => ''
      ),
      'spin' => array(
          'type' => 'checkbox',
          'title' => __( 'Spin', 'intense' ),
          'default' => "0",
      ),
      'rotate' => array(
        'type' => 'dropdown',
        'title' => __( 'Rotate', 'intense' ),
        'default' => '',
        'options' => array( 
          '' => '',
          '90' => __( '90 degrees', 'intense' ),
          '180' => __( '180 degrees', 'intense' ),
          '270' => __( '270 degrees', 'intense' ) 
        )
      ),
      'flip' => array(
        'type' => 'dropdown',
        'title' => __( 'Flip', 'intense' ),
        'default' => '',
        'options' => array( 
          '' => '',
          'horizontal' => __( 'Horizontal', 'intense' ),
          'vertical' => __( 'Vertical', 'intense' )
        )
      ),
      'stack_type' => array(
        'type' => 'icon',
        'title' => __( 'Stack Icon', 'intense' ),
        'default' => '',
      ),
      'stack_color' => array(
          'type' => 'color_advanced',
          'title' => __( 'Stack Color', 'intense' ),
      ),
      'extra_class' => array(
        'type' => 'text',
        'title' => __( 'Extra CSS Class', 'intense' ),
      ),
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    intense_add_style( 'intense-font-awesome' );

    $size -= 1;
    $output = "";

    $size .= "x";

    if ( !empty( $rotate ) ) {
      $rotate = " icon-rotate-" . $rotate;
    }
    if ( !empty( $flip ) ) {
      $flip = " icon-flip-" . $flip;
    }

    $color_class = "";
    $color_style = "";

    $color = intense_get_plugin_color( $color );

    if ( !empty( $color ) ) {
      $color_style = "color: {$color};";
    }

    if ( !empty( $stack_type ) ) {
      $stack_color_style = "";

      $stack_color = intense_get_plugin_color( $stack_color );

      if ( !empty( $stack_color ) ) {
        $stack_color_style = "color: ${stack_color};";
      }

      $output .= "<span class='intense icon-stack icon-{$size}'>";
      $output .= "<i class='intense icon-$stack_type icon-stack-base $rotate $flip $extra_class" . ( $spin ? " icon-spin" : "" ) . "' style='$stack_color_style'></i>";
    }

    $output .= "<i class='intense icon-$type" . ( !empty( $stack_type )  ? "" : " icon-{$size}" ) . $rotate . $flip . " $extra_class" . ( $spin ? " icon-spin" : "" ) . "' style='$color_style'></i>";

    if ( !empty( $stack_type ) ) {
      $output .= "</span>";
    }

    return $output;
  }
}
