<?php

class Intense_Row extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Layout Row', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-columns';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
				'description' => __( 'optional - the client-side id of the row. This id will be set on the HMTL tag.', 'intense' ),
                'skinnable' => '0'
			),
			'margin_top' => array(
				'type' => 'text',
				'title' => __( 'Margin Top', 'intense' ),
				'default' => $intense_visions_options['intense_layout_row_default_margin']['margin-top'],
				'skinnable' => '0'
			),
			'margin_bottom' => array(
				'type' => 'text',
				'title' => __( 'Margin Bottom', 'intense' ),
				'default' => $intense_visions_options['intense_layout_row_default_margin']['margin-bottom'],
				'skinnable' => '0'
			),
			'padding_top' => array(
				'type' => 'text',
				'title' => __( 'Padding Top', 'intense' ),
				'default' => $intense_visions_options['intense_layout_row_default_padding']['padding-top'],
				'skinnable' => '0'
			),
			'padding_bottom' => array(
				'type' => 'text',
				'title' => __( 'Padding Bottom', 'intense' ),
				'default' => $intense_visions_options['intense_layout_row_default_padding']['padding-bottom'],
				'skinnable' => '0'
			),
			'nogutter' => array(
				'type' => 'checkbox',
				'title' => __( 'Hide Gutter', 'intense' ),
				'description' => __( 'removes the default gutter at the sides of the row', 'intense' ),
				'default' => "0",
			),
			'column' => array(
				'type' => 'repeater',
				'title' => __( 'Columns', 'intense' ),
				'preview_content' => __( 'Column Content', 'intense' ),
				'class' => 'columnrepeater',
				'fields' => array(
					'size' => array(
						'type' => 'text',
						'title' => __( 'Size', 'intense' ),
						'default' => '12'
					),
					'medium_size' => array(
						'type' => 'text',
						'title' => __( 'Medium Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
						'default' => '12',
						'composer_show_value' => true
					),
					'small_size' => array(
						'type' => 'text',
						'title' => __( 'Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
						'default' => '12',
						'composer_show_value' => true
					),
					'extra_small_size' => array(
						'type' => 'text',
						'title' => __( 'Extra Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
						'default' => '12',
						'composer_show_value' => true
					),
					'nogutter' => array(
						'type' => 'checkbox',
						'title' => __( 'Hide Gutter', 'intense' ),
						'description' => __( 'removes the default gutter at the sides of the column', 'intense' ),
						'default' => "0",
					),					
				)
			),
			'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_numeric( $padding_top ) && $padding_top != 0 ) $padding_top .= "px";
		if ( is_numeric( $padding_bottom ) && $padding_bottom != 0 ) $padding_bottom .= "px";
		if ( is_numeric( $margin_top ) && $margin_top != 0 ) $margin_top .= "px";
		if ( is_numeric( $margin_bottom ) && $margin_bottom != 0 ) $margin_bottom .= "px";

		$style = '';

		if ( isset( $padding_top ) ) {
			$style .= 'padding-top: ' . $padding_top . '; ';
		}

		if ( isset( $padding_bottom ) ) {
			$style .= 'padding-bottom: ' . $padding_bottom . '; ';
		}

		if ( isset( $margin_top ) ) {
			$style .= 'margin-top: ' . $margin_top . '; ';
		}

		if ( isset( $margin_bottom ) ) {
			$style .= 'margin-bottom: ' . $margin_bottom . '; ';
		}

		return "<div " . ( isset( $id ) ? "id='$id' " : "" ) . " class='intense row " .
			( $rtl ? "rtl " : "" ) .
			( $nogutter ? "nogutter " : "" ) .
			"' " . ( isset( $style ) ? "style='$style'" : "" ) . ">" . do_shortcode( $content ) . "</div>";
	}

	function render_dialog_script() {
	?>
		<style>
			col:first-child {background: #ccc}
			col:nth-child(2n) {background: #fff; }

			col {
				border-right: 1px solid #ccc;
			}

			.columns td {
				padding: 7px;
				text-align: right;
			}

			.columns tr:first-child > td { 
				background: #999;
				text-align: center;
				border: none;			
			}

			.columns tr:first-child > td:first-child {
				background: #f1f1f1;
			}

			.columns tr:first-child > td label {
				color: #fff;
			}

			.columns td select {
				width: 75px;
			}
		</style>
		<div class="form-group">
			<label class="intense col-sm-2 control-label"><?php _e( 'Quick Add', 'intense' ); ?></label>			
			<div class="intense col-sm-8">				
				<a href="#" id="quickfull" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout.png" /> <?php _e( 'Full', 'intense' ); ?></a>
				<a href="#" id="quickhalf" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout-2-equal.png" /> 1/2</a>
				<a href="#" id="quickquarter" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout-1_4.png" /> 1/4</a>
				<a href="#" id="quickthird" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout-3.png" /> 1/3</a>
				<a href="#" id="quickthreequarter" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout-3_4.png" /> 3/4</a>
				<a href="#" id="quicktwothirds" class="button"><img src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/fugue-icons/icons/layout-2-inverse.png" /> 2/3</a>
			</div>
		</div>
		<div id="columnlist" class="form-group">
			<label class="intense col-sm-2 control-label"><?php _e( 'Columns', 'intense' ); ?></label>
			<div class="intense col-sm-8">
				<div style="height: 270px; width: 650px; overflow-x: auto; overflow-y: hidden; background: #f1f1f1; padding: 0px;">
					<table class="columns" >
						<colgroup>
							<col><col>
						</colgroup>
						<tr>
							<td class="meta"></td>
							<td><label><?php _e( 'Column', 'intense' ); ?> 1</label></td>
						</tr>
						<tr>
							<td class="meta" style="white-space: nowrap"><label><?php _e( 'Standard Size', 'intense' ); ?></label></td>
							<td><select class="standardsize" id="standardsize1"><?php 
								for ($i = 1; $i < 13; $i++) {
									echo '<option' . ( $i == 12 ? ' selected' : '' ) . '>' . $i . '</option>';
								}
							?></select></td>
						</tr>
						<tr>
							<td class="meta" style="white-space: nowrap"><label><?php _e( 'Medium Size', 'intense' ); ?></label></td>
							<td><select class="mediumsize" id="mediumsize1"><?php 
								for ($i = 1; $i < 13; $i++) {
									echo '<option' . ( $i == 12 ? ' selected' : '' ) . '>' . $i . '</option>';
								}
							?></select></td>
						</tr>
						<tr>
							<td class="meta" style="white-space: nowrap"><label><?php _e( 'Small Size', 'intense' ); ?></label></td>
							<td><select class="smallsize" id="smallsize1"><?php 
								for ($i = 1; $i < 13; $i++) {
									echo '<option' . ( $i == 12 ? ' selected' : '' ) . '>' . $i . '</option>';
								}
							?></select></td>
						</tr>
						<tr>
							<td class="meta" style="white-space: nowrap"><label><?php _e( 'Extra Small Size', 'intense' ); ?></label></td>
							<td><select class="extrasmallsize" id="extrasmallsize1"><?php 
								for ($i = 1; $i < 13; $i++) {
									echo '<option' . ( $i == 12 ? ' selected' : '' ) . '>' . $i . '</option>';
								}
							?></select></td>
						</tr>
						<tr>
							<td class="meta" style="white-space: nowrap"><label><?php _e( 'Hide Gutter', 'intense' ); ?></label></td>
							<td><input class="hidegutter" type="checkbox" class="" name="hidegutter1" id="hidegutter1"></td>
						</tr>
					</table>				
				</div>
				<input type="button" id="addcolumn" class="button" value="<?php _e( 'Add Column', 'intense' ); ?>" />
				<span class="help-block"><?php _e( 'the columns for each row should add up to 12. Anything beyond that will spill into another row.' ); ?></span>
			</div>
		</div>		
		<script>
			jQuery(function($) {
				$('.columnrepeater').hide();			

				function addColumn(size) {
					var itemCount = $("#columnlist .columns").find("tr:first").children("td").length;
					var repeaterCount = itemCount - 1;

					$('.repeater-add').click();
				
					var rows = $("#columnlist .columns").find("tr");

					$("#columnlist .columns colgroup").append("<col>");

					$(rows[0]).append('<td><label><?php _e('Column ', 'intense' ); ?>' + itemCount + '</label></td>');
					$(rows[1]).append('<td><select class="standardsize" id="standardsize' + itemCount + '"">' + options + '</select></td>');
					$(rows[2]).append('<td><select class="mediumsize" id="mediumsize' + itemCount + '"">' + options + '</select></td>');
					$(rows[3]).append('<td><select class="smallsize" id="smallsize' + itemCount + '"">' + options + '</select></td>');
					$(rows[4]).append('<td><select class="extrasmallsize" id="extrasmallsize' + itemCount + '"">' + options + '</select></td>');
					$(rows[5]).append('<td><input class="hidegutter" type="checkbox" class="" name="hidegutter' + itemCount + '" id="hidegutter' + itemCount + '"></td>');
					
					$('#standardsize' + itemCount).change(function() {
						$('#size_' + repeaterCount).val($(this).val());
					});
					$('#mediumsize' + itemCount).change(function() {
						$('#medium_size_' + repeaterCount).val($(this).val());
					});
					$('#smallsize' + itemCount).change(function() {
						$('#small_size_' + repeaterCount).val($(this).val());
					});
					$('#extrasmallsize' + itemCount).change(function() {
						$('#extra_small_size_' + repeaterCount).val($(this).val());
					});
					$('#hidegutter' + itemCount).change(function() {
						//$('#nogutter_' + repeaterCount).val($(this).val());
						$('#nogutter_' + repeaterCount).prop('checked', $(this).prop('checked'));
					});

					if (size) {
						$('#standardsize' + itemCount).val(size);
						$('#size_' + repeaterCount).val(size);
						$('#mediumsize' + itemCount).val(size);
						$('#medium_size_' + repeaterCount).val(size);
						$('#smallsize' + itemCount).val(12);
						$('#extrasmallsize' + itemCount).val(12);
					}
				}				

				function removeAllColumns() {				
					$("#columnlist .columns").find("tr").each(function() {
	    				$(this).find("td").not(".meta").remove();
					});

					$('.repeater-item').remove();
				}

				var options = '';
				var defaultColumnSetting =  $("#columnlist > .controls").children();
						
				for (var i = 1; i < 13; i++) {
					options += '<option>' + i + '</option>';
				};

				$('#quickfull').click(function(event) {
					event.preventDefault();

					removeAllColumns();				
					addColumn(12);
				});

				$('#quickhalf').click(function(event) {
					event.preventDefault();
					
					removeAllColumns();	
					addColumn(6);
					addColumn(6);
				});

				$('#quickquarter').click(function(event) {
					event.preventDefault();
					
					removeAllColumns();	
					addColumn(3);
					addColumn(3);
					addColumn(3);
					addColumn(3);
				});

				$('#quickthird').click(function(event) {
					event.preventDefault();
					
					removeAllColumns();	
					addColumn(4);
					addColumn(4);
					addColumn(4);
				});

				$('#quickthreequarter').click(function(event) {
					event.preventDefault();
					
					removeAllColumns();	
					addColumn(9);
					addColumn(3); 
				});

				$('#quicktwothirds').click(function(event) {
					event.preventDefault();
					
					removeAllColumns();	
					addColumn(8);
					addColumn(4);
				});

				$('#addcolumn').click(function() {				
					addColumn(12);
				});

				setTimeout(function() {
					removeAllColumns();				
					addColumn(12);
				}, 200);
				
			});
		</script>
		<?php
	}
}

class Intense_Column extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Column', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-columns';
		$this->show_preview = true;
		$this->preview_content = __( 'Column Content', 'intense' );
		$this->is_container = true;
		$this->hidden = true;
		$this->parent = 'intense_row';

		$this->fields = array(
			'size' => array(
				'type' => 'text',
				'title' => __( 'Size', 'intense' ),
				'default' => '12',
				'composer_show_value' => true
			),
			'medium_size' => array(
				'type' => 'text',
				'title' => __( 'Medium Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
				'default' => '12',
				'composer_show_value' => true
			),
			'small_size' => array(
				'type' => 'text',
				'title' => __( 'Small Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
				'default' => '12',
				'composer_show_value' => true
			),
			'extra_small_size' => array(
				'type' => 'text',
				'title' => __( 'Extra Small Size', 'intense' ),
				'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
				'default' => '12',
				'composer_show_value' => true
			),
			'nogutter' => array(
				'type' => 'checkbox',
				'title' => __( 'Show Gutter', 'intense' ),
				'description' => __( 'removes the default gutter at the sides of the column', 'intense' ),
				'default' => "0",
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		return "<div class='intense col-lg-$size " .
		( isset( $extra_small_size ) ? "col-xs-$extra_small_size ": "col-xs-12 " ) .
		( isset( $small_size ) ? "col-sm-$small_size " : "col-sm-12 " ) .
		( isset( $medium_size ) ? "col-md-$medium_size " : "col-md-$size " ) .
		( $nogutter ? "nogutter " : "" ) .
		"'>" . do_shortcode( $content ) . "</div>";
	}
}

//TODO: add Responsive column reset shortcode see http://getbootstrap.com/css/#grid
