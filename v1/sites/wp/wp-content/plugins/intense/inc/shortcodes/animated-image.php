<?php

class Intense_Animated_Image extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Animated Image', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-movie5';
		$this->show_preview = true;

		$this->fields = array(
			'image' => array(
				'type' => 'image',
				'title' => __( 'Image', 'intense' ),
				'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
				'skinnable' => '0',
			),
			'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
          	'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
			'size' => array(
				'type' => 'image_size',
				'title' => __( 'Image Size', 'intense' )
			),
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'default' => 'slideleft',
				'options' => array(
					'slideleft' => __( 'Slide Left', 'intense' ),
					'slideright' => __( 'Slide Right', 'intense' ),
					'slideup' => __( 'Slide Up', 'intense' ),
					'slidedown' => __( 'Slide Down', 'intense' )
				)
			),
			'anchor' => array(
				'type' => 'dropdown',
				'title' => __( 'Anchor', 'intense' ),
				'description' => __( 'position to anchor the image', 'intense' ),
				'default' => 'slideleft',
				'options' => array(
					'top' => __( 'Top (applies to Slide Left/Right)', 'intense' ),					
					'bottom' => __( 'Bottom (applies to Slide Left/Right)', 'intense' ),
					'left' => __( 'Left (applies to Slide Up/Down)', 'intense' ),
					'right' => __( 'Right (applies to Slide Up/Down)', 'intense' ),
					'middle' => __( 'Middle (applies to Slide Left/Right/Up/Down)', 'intense' ),
				)
			),
			// 'height' => array(
			// 	'type' => 'text',
			// 	'title' => __( 'Height', 'intense' ),
			// 	'description' => __( 'measured in pixels', 'intense' ),
			// ),
			'speed' => array(
				'type' => 'text',
				'title' => __( 'Speed', 'intense' ),
				'description' => __( 'measured in milliseconds', 'intense' ),
				'default' => '30'
			),
			'zindex' => array(
				'type' => 'hidden',
				'description' => __( 'z-index', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = "";

		intense_add_script( 'intense.animated.image' );

		if ( is_numeric( $image ) ) {
			$imageid = $image;
		} else if ( !empty( $image ) ) {
				$imageurl = $image;
			}

		if ( !empty( $imageid ) ) {
			$photo_info = wp_get_attachment_image_src( $imageid, $size );
			$photo_url = $photo_info[0];
			$width = $photo_info[1];

			// if ( !isset( $height ) ) {
			// 	$height = $photo_info[2];
			// }
		} else if ( !empty( $imageurl ) ) {
			$photo_url = $imageurl;
		} else {
			$photo_url = '';
		}

		switch ($anchor) {
			case 'top':
				$background_position_percent = "0";
				break;
			case 'bottom':
				$background_position_percent = "100";
				break;
			case 'left':
				$background_position_percent = "0";
				break;
			case 'right':
				$background_position_percent = "100";
				break;
			case 'middle':
				$background_position_percent = "50";
				break;
			default:
				$background_position_percent = "0";
				break;
		}

		if ( $type == "slideleft" || $type == "slideright" || $type == "slideup" || $type == "slidedown" ) {
			$output .= '<div class="intense animated-image slide" style="' .
				'z-index: ' . $zindex . '; ' .
				'height: 100%;' .
				( isset( $width ) && ( $type == 'slideup' || $type == 'slidedown' ) ? 'width: ' . $width . 'px;' : '' ) .
				'position: absolute; top: 0; bottom: 0; left: 0; right: 0; ' .
				'background-image: url(' . $photo_url . '); background-repeat: repeat-' . ( ( $type == 'slideup' || $type == 'slidedown' ) ? 'y' : 'x' ) . ';' .
				'background-position-' . ( ( $type == 'slideup' || $type == 'slidedown' ) ? 'x:' . $background_position_percent . '%;' : 'y:' . $background_position_percent . '%;' ) . 
				'" data-speed="' . $speed . '" data-direction="' . $type . '" data-anchor="' . $background_position_percent . '"></div>';
		}

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
				jQuery(function($) {
					// Uploading files
			  		$('.upload_image_button').live('click', function( event ){
					    event.preventDefault();

					    window.parent.loadImageFrame($(this), function(attachment) {
					    	jQuery("#image").val(attachment.id);
					    	$('#image-thumb').attr('src', attachment.url);

					    	if ( $('#preview-content').length > 0 ) {
		                      $('#preview').click();
		                    }
					    });
					});
				});
			</script>
		<?php
	}
}
