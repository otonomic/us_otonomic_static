<?php

class Intense_Pricing_Table extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Pricing Table', 'intense' );
        $this->category = __( 'Layout', 'intense' );
        $this->icon = 'dashicons-intense-coin';
        $this->show_preview = true;
        $this->is_container = true;

        $this->fields = array(
            'even_background_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Even Background Color', 'intense' ),
                'default' => '#ebebeb'
            ),
            'odd_background_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Odd Background Color', 'intense' ),
                'default' => '#f5f5f5'
            ),
            'featured' => array(
                'type' => 'checkbox',
                'title' => __( 'Featured', 'intense' ),
                'default' => "0",
            ),
            'hover_expand' => array(
                'type' => 'checkbox',
                'title' => __( 'Hover Expand', 'intense' ),
                'description' => __( 'expand the table when the mouse is hovered over', 'intense' ),
                'default' => '1'
            ),
            'shadow' => array(
                'type' => 'shadow',
                'title' => __( 'Shadow', 'intense' ),
            ),
            'grouping' => array(
                'type' => 'text',
                'title' => __( 'Grouping', 'intense' ),
                'description' => __( 'used to group several pricing tables together into one table', 'intense' ),
                'skinnable' => '0'
            ),
            'pricing_section' => array(
                'type' => 'repeater',
                'title' => __( 'Sections', 'intense' ),
                'fields' => array(
                    'template' => array(
                        'type' => 'template',
                        'title' => __( 'Template', 'intense' ),
                        'default' => 'standard',
                        'path' => '/pricing-table/',
                    ),
                    'title' => array(
                        'type' => 'text',
                        'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'background_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Background Color', 'intense' ),
                    ),
                    'font_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Font Color', 'intense' ),
                    ),
                    'amount' => array(
                        'type' => 'text',
                        'title' => __( 'Amount', 'intense' ),
                    ),
                    'sale_amount' => array(
                        'type' => 'text',
                        'title' => __( 'Sale Amount', 'intense' ),
                    ),
                    'frequency' => array(
                        'type' => 'text',
                        'title' => __( 'Billing Frequency', 'intense' ),
                    ),
                    'border' => array(
                        'type' => 'border',
                        'title' => __( 'All Borders', 'intense' ),
                    ),
                    'border_top' => array(
                        'type' => 'border',
                        'title' => __( 'Top Border', 'intense' ),
                    ),
                    'border_right' => array(
                        'type' => 'border',
                        'title' => __( 'Right Border', 'intense' ),
                    ),
                    'border_bottom' => array(
                        'type' => 'border',
                        'title' => __( 'Bottom Border', 'intense' ),
                    ),
                    'border_left' => array(
                        'type' => 'border',
                        'title' => __( 'Left Border', 'intense' ),
                    ),
                    'border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Border Radius', 'intense' ),
                    ),
                )
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        global $intense_pricing_table_index;
        global $intense_pricing_table_odd_background_color;
        global $intense_pricing_table_even_background_color;
        global $intense_pricing_table_featured;

        $intense_pricing_table_index = 0;
        $intense_pricing_table_featured = $featured;

        $output = '';
        $style = '';
        $id = rand();

        intense_add_script( 'intense.pricingtable' );

        $even_background_color = intense_get_plugin_color( $even_background_color );
        $odd_background_color = intense_get_plugin_color( $odd_background_color );
        $intense_pricing_table_odd_background_color = $odd_background_color;
        $intense_pricing_table_even_background_color = $even_background_color;

        if ( $shadow ) {
            $output .= "<div><div style='margin-bottom: 0;'>";
        }

        $style .= ' margin: 10px 0; position: relative; width: 100%; transition: all .2s linear;';

        if ( $intense_pricing_table_featured ) {
            $style .= ' margin-top: -5px; box-shadow: 0 0 15px rgba(0,0,0,.4); z-index: 2;';
        } else {
            $style .= ' z-index: 1;';
        }

        if ( !empty( $grouping ) ) {
            $grouping = ' data-grouping="' . $grouping . '"';
        } 

        if ( !empty( $hover_expand ) && $hover_expand ) {
            $hover_expand = ' data-hover-expand="true"';
        }

        $output .= '<div style="position: relative;"><div id="pricing_table_' . $id . '" ' . $grouping . $hover_expand . ' class="intense pricing-table' . ( $featured ? ' featured' : '' ) . '" style="' . $style . '">' . do_shortcode( $content ) . '</div><div class="clearfix"></div></div>';

        if ( $shadow ) {
            $output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='vertical-align: top; ' alt='' /></div>";
        }

        return $output;
    }
}

class Intense_Pricing_Section extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Pricing Section', 'intense' );
        $this->category = __( 'Layout', 'intense' );
        $this->icon = 'dashicons-intense-coin';
        $this->show_preview = true;
        $this->hidden = true;
        $this->is_container = true;
        $this->parent = 'intense_pricing_table';

        $this->fields = array(
            'template' => array(
                'type' => 'template',
                'title' => __( 'Template', 'intense' ),
                'default' => 'standard',
                'path' => '/pricing-table/',
            ),
            'title' => array(
                'type' => 'text',
                'title' => __( 'Title', 'intense' ),
                'skinnable' => '0'
            ),
            'background_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Background Color', 'intense' ),
            ),
            'font_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Font Color', 'intense' ),
            ),
            'amount' => array(
                'type' => 'text',
                'title' => __( 'Amount', 'intense' ),
                'skinnable' => '0'
            ),
            'sale_amount' => array(
                'type' => 'text',
                'title' => __( 'Sale Amount', 'intense' ),
                'skinnable' => '0'
            ),
            'frequency' => array(
                'type' => 'text',
                'title' => __( 'Billing Frequency', 'intense' ),
                'skinnable' => '0'
            ),
            'border' => array(
                'type' => 'border',
                'title' => __( 'All Borders', 'intense' ),
            ),
            'border_top' => array(
                'type' => 'border',
                'title' => __( 'Top Border', 'intense' ),
            ),
            'border_right' => array(
                'type' => 'border',
                'title' => __( 'Right Border', 'intense' ),
            ),
            'border_bottom' => array(
                'type' => 'border',
                'title' => __( 'Bottom Border', 'intense' ),
            ),
            'border_left' => array(
                'type' => 'border',
                'title' => __( 'Left Border', 'intense' ),
            ),
            'border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Border Radius', 'intense' ),
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $output = '';
        $style = '';
        $font_color_css = '';
        $border_style = null;
        $border_top_style = null;
        $border_right_style = null;
        $border_bottom_style = null;
        $border_left_style = null;

        global $intense_pricing_table_index;
        global $intense_pricing_table_odd_background_color;
        global $intense_pricing_table_even_background_color;
        global $intense_pricing_table_featured;
        global $intense_pricing_section;

        $background_color = intense_get_plugin_color( $background_color );
        $font_color = intense_get_plugin_color( $font_color );

        if ( !empty( $background_color ) ) {
            $background_color_css = ' background: ' . $background_color . ' !important;';
        } else {
            if ( $intense_pricing_table_index % 2 == 0 ) {
                $background_color = $intense_pricing_table_even_background_color;
            } else {
                $background_color = $intense_pricing_table_odd_background_color;
            }

            if ( $template == 'footer' ) {
                $background_color = '#fff';
            }

            $background_color_css = ' background: ' . $background_color . ' !important;';
        }

        if ( !empty( $font_color ) ) {
            $font_color_css = ' color: ' . $font_color . ' !important;';
        }

        if ( isset( $border ) ) $border_style = "border: " . $border . ' !important;';
        if ( isset( $border_top ) ) $border_top_style = "border-top: " . $border_top . ' !important;';
        if ( isset( $border_right ) ) $border_right_style = "border-right: " . $border_right . ' !important;';
        if ( isset( $border_bottom ) ) $border_bottom_style = "border-bottom: " . $border_bottom . ' !important;';
        if ( isset( $border_left ) ) $border_left_style = "border-left: " . $border_left . ' !important;';      
        if ( isset( $border_radius ) ) $radius = 'border-radius:' . $border_radius . ';';

        if ( !isset( $atts['frequency'] ) ) $atts['frequency'] = null;
        if ( !isset( $atts['amount'] ) ) $atts['amount'] = null;
        if ( !isset( $atts['sale_amount'] ) ) $atts['sale_amount'] = null;
        if ( !isset( $atts['title'] ) ) $atts['title'] = null;

        $intense_pricing_section = array_merge( $atts, array(
                'background_color_css' => $background_color_css,
                'font_color_css' => $font_color_css,
                'is_featured' => $intense_pricing_table_featured,
                'content' => $content,
                'index' => $intense_pricing_table_index,
                'font_color' => $font_color,
                'background_color' => $background_color,
                'even_background_color' => $intense_pricing_table_even_background_color,
                'odd_background_color' => $intense_pricing_table_odd_background_color,
                'border_css' => $border_style . $border_top_style . $border_right_style . $border_bottom_style . $border_left_style . $border_radius
            ) );

        $template_file = intense_locate_plugin_template( '/pricing-table/' . $template . '.php' );
        $output .= intense_load_plugin_template( $template_file );

        $intense_pricing_table_index++;

        $style .= ' .intense.pricing-table-section {
            transition: all .2s linear;
        }';

        if ( !empty( $style ) ) {
            $output .= '<style>' . $style . '</style>';
        }

        return $output;
    }
}
