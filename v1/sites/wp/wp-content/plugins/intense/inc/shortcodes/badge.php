<?php

class Intense_Badge extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Badge', 'intense' );
        $this->category = __( 'Typography', 'intense' );
        $this->icon = 'dashicons-intense-tag';
        $this->show_preview = true;
        $this->preview_content = __( "Badge Text", 'intense' );
        $this->vc_map_content = 'textfield';

        $this->fields = array(
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Color', 'intense' ),
                'default' => 'primary'
            ),
            'font_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Font Color', 'intense' ),
                'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                'default' => ''
            ),
            'border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Border Radius', 'intense' ),                
            ),            
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );        
        
        $radius = '';
        if ( isset( $border_radius ) ) {
            $radius = ' border-radius:' . $border_radius . ';';
        }

        $color = intense_get_plugin_color( $color );
        $font_color = intense_get_plugin_color( $font_color );

        if ( $font_color == '' ) {
            $font_color = intense_get_contract_color( $color );
        }

        return "<span class='intense badge' style='color: " . $font_color . "; background: " . $color  . ";" . $radius . "'>" . do_shortcode( $content ) . "</span>";
    }
}