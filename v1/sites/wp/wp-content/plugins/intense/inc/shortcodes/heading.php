<?php

class Intense_Heading extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Heading', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-uniF7F3';
		$this->show_preview = true;
		$this->preview_content = __( 'Heading Text', 'intense' );
		$this->vc_map_content = 'textfield';

		$this->fields = array(
			'font_size' => array(
				'type' => 'text',
				'title' => __( 'Font Size', 'intense' ),
			),
			'line_height' => array(
				'type' => 'text',
				'title' => __( 'Line Height', 'intense' ),
				'description' => __( 'If left blank, a line height will automatically be chosen.', 'intense' ),
			),
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
			),
			'font_family' => array(
				'type' => 'text',
				'title' => __( 'Font Family', 'intense' ),
				'description' => 'leave blank to inherit from the selected tag'
			),
			'align' => array(
				'type' => 'dropdown',
				'title' => __( 'Align', 'intense' ),
				'options' => array(
					'left' => __( 'Left', 'intense' ),
					'center' => __( 'Center', 'intense' ),
					'right' => __( 'Right', 'intense' ),
				),
				'default' => ( $intense_visions_options['intense_rtl'] ? 'right' : 'left' ),
				'skinnable' => '0'
			),
			'font_weight' => array(
				'type' => 'dropdown',
				'title' => __( 'Font Weight', 'intense' ),
				'default' => '',
				'options' => array(
					'' => '',
					'lighter' => __( 'Lighter', 'intense' ),
					'100' => '100',
					'200' => '200',
					'300' => '300',
					'400' => __( '400 (Normal)', 'intense' ),
					'500' => '500',
					'600' => '600',
					'700' => __( '700 (Bold)', 'intense' ),
					'800' => '800',
					'900' => '900',
					'bolder' => __( 'Bolder', 'intense' ),
				),
			),
			'tag' => array(
				'type' => 'dropdown',
				'title' => __( 'Tag', 'intense' ),
				'options' => array(
					'h1' => 'h1',
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
					'div' => 'div'
				),
				'default' =>'h1'
			),
			'margin_top' => array(
				'type' => 'text',
				'title' => __( 'Margin Top', 'intense' ),
				'default' => '',
			),
			'margin_bottom' => array(
				'type' => 'text',
				'title' => __( 'Margin Bottom', 'intense' ),
				'default' => '',
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$font_color = intense_get_plugin_color( $font_color );

		if ( is_numeric( $font_size ) ) $font_size .= "px";
		if ( is_numeric( $line_height ) ) $line_height .= "px";
		if ( is_numeric( $margin_top ) ) $margin_top .= "px";
    	if ( is_numeric( $margin_bottom ) ) $margin_bottom .= "px";

		$font_size = ( !empty( $font_size ) ? " font-size: " . $font_size . ';' : '' );
		$line_height = ( !empty( $line_height ) ? " line-height: " . $line_height . ';' : '' );
		$font_color = ( !empty( $font_color ) ? " color: " . $font_color . ';' : '' );
		$align = ( !empty( $align ) ? " text-align: " . $align . ';' : '' );
		$font_weight = ( !empty( $font_weight ) ? " font-weight: " . $font_weight . ';' : '' );
		$margin_top = ( !empty( $margin_top ) ? " margin-top: " . $margin_top . ';' : '' );
		$margin_bottom = ( !empty( $margin_bottom ) ? " margin-bottom: " . $margin_bottom . ';' : '' );
		$font_family = ( !empty( $font_family ) ? " font-family: " . $font_family . ';' : '' );

		return '<' . $tag . ' class="intense heading" style="' . $font_color . $font_size . $line_height . $align . $font_weight . $margin_top . $margin_bottom . $font_family . '">' . do_shortcode( $content ) . '</' . $tag . '>';
	}
}
