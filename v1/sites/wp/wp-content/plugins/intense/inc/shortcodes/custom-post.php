<?php

class Intense_Custom_Post extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Custom Post Type', 'intense' );
		$this->category = __( 'Posts', 'intense' );
		$this->icon = 'dashicons-admin-post';
		$this->show_preview = true;
		$this->map_visual_composer = false;

		$this->fields = array(
			'general_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'post_type' => array(
						'type' => 'post_type',
						'title' => __( 'Post Type', 'intense' ),
                        'skinnable' => '0'
					),
					'taxonomy' => array(
						'type' => 'taxonomy',
						'category' => 'category',
						'title' => __( 'Taxonomy', 'intense' ),
						'class' => 'wpsettings timelinesettings',
                        'skinnable' => '0'
					),
					'template' => array(
						'type' => 'dropdown',
						'title' => __( 'Template', 'intense' ),
						'default' => 'one'
					),
					'type' => array( 'type' => 'deprecated', 'description' => 'use template instead' ),
					'categories' => array(
						'type' => 'category',
						'category' => 'category',
						'title' => __( 'Category(s)', 'intense' ),
						'description' => __( 'By default, all categories will be shown.', 'intense' ),
						'class' => 'wpsettings timelinesettings',
						'skinnable' => '0'
					),
					'include_post_ids' => array(
						'type' => 'text',
						'title' => __( 'Include Post IDs', 'intense' ),
						'description' => __( 'Comma delimited list of post IDs for a set list of posts that you want to show. Leave blank to not limit posts shown.', 'intense' ),
						'default' => '',
						'class' => 'wpsettings timelinesettings',
						'skinnable' => '0'
					),
					'authors' => array(
						'type' => 'author',
						'title' => __( 'Author(s)', 'intense' ),
						'description' => __( 'By default, all authors will be shown', 'intense' ),
						'class' => 'wpsettings timelinesettings'
					),
					'order_by' => array(
						'type' => 'dropdown',
						'title' => __( 'Order By', 'intense' ),
						'default' => 'id',
						'options' => array(
							'id' => __( 'Post ID', 'intense' ),
							'author' => __( 'Author', 'intense' ),
							'title' => __( 'Title', 'intense' ),
							'date' => __( 'Date', 'intense' ),
							'modified' => __( 'Modified Date', 'intense' ),
							'rand' => __( 'Random Order', 'intense' ),
							'comment_count' => __( 'Comment Count', 'intense' ),
						),
						'class' => 'wpsettings timelinesettings'
					),
					'order' => array(
						'type' => 'dropdown',
						'title' => __( 'Order', 'intense' ),
						'default' => 'desc',
						'options' => array(
							'desc' => __( 'Descending', 'intense' ),
							'asc' => __( 'Ascending', 'intense' ),
						),
						'class' => 'wpsettings timelinesettings'
					),
					'posts_per_page' => array(
						'type' => 'text',
						'title' => __( 'Posts Per Page', 'intense' ),
						'description' => __( 'Enter a number (default will be the value you have set in your settings)', 'intense' ),
						'class' => '',
						'default' => get_option( 'posts_per_page' ),
						'class' => 'wpsettings',
						'skinnable' => '0'
					),
					'post_count' => array(
						'type' => 'text',
						'title' => __( 'Post Count', 'intense' ),
						'description' => __( 'Enter a number to limit the number of posts shown (leave blank for all to be shown)', 'intense' ),
						'default' => '',
						'class' => 'wpsettings timelinesettings'
					),
					'infinite_scroll' => array(
						'type' => 'checkbox',
						'title' => __( 'Infinite Scroll', 'intense' ),
						'description' => __( 'Show all posts with infinite scroll', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings timelinesettings'
					),
					'show_all' => array(
						'type' => 'checkbox',
						'title' => __( 'Show All', 'intense' ),
						'description' => __( 'Show all on one page', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings'
					),
					'show_meta' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Meta', 'intense' ),
						'description' => __( 'Show post meta information (post date, comment count, category, and author)', 'intense' ),
						'default' => '1',
						'class' => 'wpsettings'
					),
					'show_author' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Author Image', 'intense' ),
						'description' => __( 'Show author image (only shown on some templates - One Column for instance)', 'intense' ),
						'default' => '1',
						'class' => 'wpsettings'
					),
					'show_social_sharing' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Social Sharing buttons', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings'
					),
					'show_read_more' => array(
						'type' => 'checkbox',
						'title' => __( 'Show "Read More" button', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings timelinesettings'
					),
					'read_more_text' => array(
		            	'type' => 'text',
		                'title' => __( '"Read More" Display Text', 'intense' ),
					    'description' => __( 'Only visible if Show "Read More" button above is checked.', 'intense' ),						
						'default' => __( 'Read More', 'intense' ),
						'class' => 'wpsettings timelinesettings',
						'skinnable' => '0' 
		            ),
					'sticky_mode' => array(
						'type' => 'dropdown',
						'title' => __( 'Sticky Post Mode', 'intense' ),
						'description' => __( '<strong>top</strong> - Show the sticky posts at the top of the post lists.<br>
												<strong>inline</strong> - Show the sticky posts inline. Inline will also change the layout to highlight the sticky post.<br>
												<strong>ignore</strong> - Ignore the sticky setting and show the posts like non-sticky posts.', 'intense' ),
						'default' => 'top',
						'options' => array(
							'ignore' => __( 'Ignore', 'intense' ),
							'inline' => __( 'Inline', 'intense' ),
							'top' => __( 'Top', 'intense' ),
						)
					),
					'timeline_mode' => array(
						'type' => 'dropdown',
						'title' => __( 'Mode', 'intense' ),
						'default' => 'dual',
						'options' => array(
							'dual' => __( 'Dual', 'intense' ),
							'left' => __( 'Left', 'intense' ),
							'right' => __( 'Right', 'intense' ),
							'center' => __( 'Center', 'intense' ),
						),
						'class' => 'timelinesettings'
					),
					'timeline_layout' => array( 'type' => 'deprecated', 'description' => 'use template/timeline_mode instead' ),
					'timeline_order' => array( 'type' => 'deprecated', 'description' => 'use template/timeline/order instead' ),
					'timeline_border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Timeline Border Radius', 'intense' ),
						'class' => 'timelinesettings'
					),
					'timeline_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
						'description' => __( 'Background color of timeline events' , 'intense' ),
						'default' => '#efefef',
						'class' => 'timelinesettings'
					),
					'timeline_showyear' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Year', 'intense' ),
						'description' => __( 'Show year in timeline', 'intense' ),
						'default' => '1',
						'class' => 'timelinesettings'
					),
					'timeline_readmore' => array( 'type' => 'deprecated', 'description' => 'use show_read_more and read_more_text instead' ),
					'rtl' => array(
						'type' => 'hidden',
						'description' => __( 'right-to-left', 'intense' ),
						'default' => $intense_visions_options['intense_rtl']
					)
				),
			),
			'filter_tab' => array(
				'type' => 'tab',
				'title' => __( 'Filter', 'intense' ),
				'fields' =>  array(
					'show_filter' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Filter', 'intense' ),
						'default' => '1',
						'class' => 'wpsettings'
					),
					'filter_all_text' => array(
		            	'type' => 'text',
		                'title' => __( 'Filter All Display Text', 'intense' ),						
						'default' => __( 'All', 'intense' ),
						'class' => 'wpsettings filtersettings',
						'skinnable' => '0' 
		            ),
					'filter_easing' => array(
						'type' => 'dropdown',
						'title' => __( 'Filter Easing', 'intense' ),
						'default' => '',
						'options' => array(
							'' => __( 'Select a Filter', 'intense' ),
							'smooth' => __( 'Smooth', 'intense' ),
							'snap' => __( 'Snap', 'intense' ),
							'windup' => __( 'Wind Up', 'intense' ),
							'windback' => __( 'Wind Back', 'intense' ),
						),
						'class' => 'wpsettings filtersettings'
					),
					'filter_effects' => array(
						'type' => 'dropdown',
						'title' => __( 'Filter Effects', 'intense' ),
						'default' => 'fade,scale',
						'description' => __( 'Transition effects, default is Fade and Scale', 'intense' ),
						'multiple' => '1',
						'options' => array(
							'fade' => __( 'Fade', 'intense' ),
							'scale' => __( 'Scale', 'intense' ),
							'rotateX' => __( 'Rotate X', 'intense' ),
							'rotateY' => __( 'Rotate Y', 'intense' ),
							'rotateZ' => __( 'Rotate Z', 'intense' ),
							'blur' => __( 'Blur', 'intense' ),
							'grayscale' => __( 'Grayscale', 'intense' ),
						),
						'class' => 'wpsettings filtersettings'
					)
				)
			),
			'animation_tab' => array(
				'type' => 'tab',
				'title' => __( 'Animation', 'intense' ),
				'fields' =>  array(
					'animation_type' => array(
						'type' => 'animation',
						'title' => __( 'Animation', 'intense' ),
						'placeholder' => __( 'Select an animation', 'intense' ),
						'class' => 'wpsettings timelinesettings'
					),
		            'animation_trigger' => array(
		            	'type' => 'dropdown',
		                'title' => __( 'Animation Trigger', 'intense' ),
		                'default' => 'scroll',
						'options' => array(
							'scroll' => __( 'Scroll', 'intense' ),
							'hover' => __( 'Hover', 'intense' ),
							'click' => __( 'Click', 'intense' ),
							'delay' => __( 'Delay', 'intense' )
						),
						'class' => 'wpsettings timelinesettings'
		            ),
		            'animation_scroll_percent' => array(
		            	'type' => 'dropdown',
		                'title' => __( 'Scroll Percentage', 'intense' ),
						'description' => __( 'animate when <span id="scrollpercentagevalue"></span>% of item is in view', 'intense' ),
						'options' => array(
							'0' => '0',
							'10' => '10',
							'20' => '20',
							'30' => '30',
							'40' => '40',
							'50' => '50',
							'60' => '60',
							'70' => '70',
							'80' => '80',
							'90' => '90',
							'100' => '100'
						),
						'default' => '10',
						'class' => 'wpsettings timelinesettings'           
		            ),
		            'animation_delay' => array(
		            	'type' => 'text',
		                'title' => __( 'Animation Delay', 'intense' ),
						'description' => __( 'amount in milliseconds', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings timelinesettings' 
		            )
				)
			),
			'exclude_tab' => array(
				'type' => 'tab',
				'title' => __( 'Exclude', 'intense' ),
				'fields' =>  array(
					'exclude_categories' => array(
						'type' => 'category',
						'category' => 'category',
						'title' => __( 'Exclude Category(s)', 'intense' ),
						'description' => __( 'Select categories that you do not want to show.', 'intense' ),
						'class' => 'wpsettings timelinesettings'
					),
					'exclude_post_ids' => array(
						'type' => 'text',
						'title' => __( 'Exclude Post IDs', 'intense' ),
						'description' => __( 'Comma delimited list of post IDs that you do not want to show.', 'intense' ),
						'default' => '',
						'class' => 'wpsettings timelinesettings',
						'skinnable' => '0'
					)
				)
			),
			'image_tab' => array(
				'type' => 'tab',
				'title' => __( 'Image', 'intense' ),
				'fields' =>  array(
					'image_size' => array(
						'type' => 'image_size',
						'title' => __( 'Image Size', 'intense' ),
						'default' => 'postFull'
					),
					'show_images' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Images', 'intense' ),
						'description' => __( 'Show or hide images.', 'intense' ),
						'default' => '1',
						'class' => 'wpsettings'
					),
					'show_missing_image' => array(
						'type' => 'checkbox',
						'title' => __( 'Show Missing Image', 'intense' ),
						'description' => __( 'Show missing image if no image is set.', 'intense' ),
						'default' => '0',
						'class' => 'wpsettings'
					),
					'image_border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Image Border Radius', 'intense' ),
						'class' => 'wpsettings timelinesettings'
					),
					'hover_effect' => array(
						'type' => 'dropdown',
						'title' => __( 'Hover Effect', 'intense' ),
						'default' => '',
						'options' => array(
							'' => __( 'Select an effect', 'intense' ),
							'1' => __( 'Appear', 'intense' ),
							'2' => __( 'Slide Up', 'intense' ),
							'3' => __( 'Sqkwoosh', 'intense' ),
							'4' => __( 'Slide Side', 'intense' ),
							'5' => __( 'Cover', 'intense' ),
							'6' => __( 'Fall In', 'intense' ),
							'7' => __( 'Two-Step', 'intense' ),
							'8' => __( 'Move', 'intense' ),
							'9' => __( 'Scale', 'intense' ),
							'10' => __( 'Flip', 'intense' ),
						),
						'class' => 'wpsettings'
					),
					'hover_effect_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Effect Color', 'intense' ),
						'description' => __( 'Hover effect background color.' , 'intense' ),
						'default' => '#efefef',
						'class' => 'wpsettings'
					),
					'hover_effect_opacity' => array(
						'type' => 'text',
						'title' => __( 'Effect Opacity', 'intense' ),
						'default' => '80',
						'description' => __( '0 - 100: hover effect background opacity', 'intense' ),
						'class' => 'wpsettings'
					),
					'image_shadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
						'class' => 'wpsettings'
					)
				)
			),
			'owlslider_tab' => array(
				'type' => 'tab',
				'title' => __( 'Slider', 'intense' ),
				'class' => 'owlslidersettings',
				'fields' =>  array(
					'is_slider' => array(
						'type' => 'checkbox',
						'title' => __( 'Slider', 'intense' ),
						'default' => '0',
					),
					'slide_padding_right' => array(
						'type' => 'text',
						'title' => __( 'Slide Padding Right', 'intense' ),
						'default' => '15',
					),
					'items' => array(
						'type' => 'text',
						'title' => __( 'Max Items', 'intense' ),
						'default' => '5',
					),
					'items_desktop' => array(
						'type' => 'text',
						'title' => __( 'Items Desktop', 'intense' ),
						'description' => __( 'window width <= 1199', 'intense' ),
						'default' => '4',
					),
					'items_desktop_small' => array(
						'type' => 'text',
						'title' => __( 'Items Desktop Small', 'intense' ),
						'description' => __( 'window width <= 979', 'intense' ),
						'default' => '3',
					),
					'items_tablet' => array(
						'type' => 'text',
						'title' => __( 'Items Tablet', 'intense' ),
						'description' => __( 'window width <= 768', 'intense' ),
						'default' => '2',
					),
					'items_mobile' => array(
						'type' => 'text',
						'title' => __( 'Items Mobile', 'intense' ),
						'description' => __( 'window width <= 479', 'intense' ),
						'default' => '1',
					),
					'navigation_position' => array(
						'type' => 'dropdown',
						'title' => __( 'Navigation Position', 'intense' ),
						'default' => 'middleoutside',
						'options' => array(
							'' => '',
							'topleft' => __( 'Top Left', 'intense' ),
							'topcenter' => __( 'Top Center', 'intense' ),
							'topright' => __( 'Top Right', 'intense' ),
							'middleinside' => __( 'Middle Inside', 'intense' ),
							'middleoutside' => __( 'Middle Outside', 'intense' ),
							'bottomleft' => __( 'Bottom Left', 'intense' ),
							'bottomcenter' => __( 'Bottom Center', 'intense' ),
							'bottomright' => __( 'Bottom Right', 'intense' ),
						),
					),
					'navigation_prev_icon' => array(
						'type' => 'icon',
						'title' => __( 'Navigation Previous Icon', 'intense' ),
						'default' => 'chevron-left',
					),
					'navigation_next_icon' => array(
						'type' => 'icon',
						'title' => __( 'Navigation Next Icon', 'intense' ),
						'default' => 'chevron-right',
					),
					'navigation_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Navigation Size', 'intense' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'default', 'intense' ),
							'mini' => __( 'mini', 'intense' ),
							'small' => __( 'small', 'intense' ),
							'medium' => __( 'medium', 'intense' ),
							'large' => __( 'large', 'intense' )
						)
					),
					'navigation_prev_text' => array(
						'type' => 'text',
						'title' => __( 'Navigation Previous Text', 'intense' ),
					),
					'navigation_next_text' => array(
						'type' => 'text',
						'title' => __( 'Navigation Next Text', 'intense' ),
					),
					'navigation_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Navigation Color', 'intense' ),
						'default' => '#111111',
					),
					'navigation_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Navigation Font Color', 'intense' ),
						'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
					),
					'navigation_prev_border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Navigation Previous Border Radius', 'intense' ),
					),
					'navigation_next_border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Navigation Next Border Radius', 'intense' ),
					),
					'navigation_pagination_type' => array(
						'type' => 'dropdown',
						'title' => __( 'Navigation Pagination Type', 'intense' ),
						'default' => 'bullets',
						'options' => array(
							'' => __( 'None', 'intense' ),
							'bullets' => __( 'Bullets', 'intense' ),
							'numbers' => __( 'Numbers', 'intense' ),
						),
					),
				)
			),
			'masonry_tab' => array(
				'type' => 'tab',
				'title' => __( 'Masonry', 'intense' ),
				'class' => 'masonrysettings',
				'fields' =>  array(
					'is_masonry' => array(
						'type' => 'checkbox',
						'title' => __( 'Masonry', 'intense' ),
						'default' => '0',
					),					
					'gutter' => array(
						'type' => 'text',
						'title' => __( 'Gutter', 'intense' ),
						'default' => '0',
					),					
					'masonry_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_medium_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Medium Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_extra_small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Extra Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_width' => array(
						'type' => 'text',
						'title' => __( 'Item Manual Width', 'intense' ),
						'description' => __( 'measured as a % or px amount', 'intense' ),
					),					
					'masonry_sticky_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Sticky Item Size', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_sticky_medium_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Sticky Item Medium Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on medium devices - desktops (≥992px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_sticky_small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Sticky Item Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on small devices - tablets (≥768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_sticky_extra_small_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Sticky Item Extra Small Size', 'intense' ),
						'description' => __( 'used to change the layout when viewed on extra small devices - Phones (<768px)', 'intense' ),
						'composer_show_value' => true,
						'options' => array(
							'' => '',
							'1' => '1', '2' => '2', '3' => '3','4' => '4','5' => '5','6' => '6',
							'7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12',
						)
					),
					'masonry_sticky_width' => array(
						'type' => 'text',
						'title' => __( 'Sticky Item Manual Width', 'intense' ),
						'description' => __( 'measured as a % or px amount', 'intense' ),
					),								
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options, $post, $wp_query, $intense_custom_post, $intense_post_types, $paged;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';
		$category_list = '';

		if ( $is_slider ) {
			$infinite_scroll = false;
			$show_filter = false;
		}

		if ( $infinite_scroll ) {
			intense_add_style( 'intense_infinite_scroll' );
			intense_add_script( 'ias_infinite_scroll' );
			intense_add_style( 'flexslider' );
			intense_add_script( 'flexslider' );
			intense_add_style( 'intense_video_js_css' );
			intense_add_script( 'intense_video_js' );
			intense_add_script( 'intense_fitvids' );
		}

		if ( !empty( $type ) ) {
			$template = $type;
		}

		if ( !empty( $timeline_order ) ) {
			$order = $timeline_order;
		}

		$is_timeline = stristr( $template, "timeline" );

		$padding_top = $intense_visions_options['intense_layout_row_default_padding']['padding-top'];
		$padding_bottom = $intense_visions_options['intense_layout_row_default_padding']['padding-bottom'];
		$margin_top = $intense_visions_options['intense_layout_row_default_margin']['margin-top'];
		$margin_bottom = $intense_visions_options['intense_layout_row_default_margin']['margin-bottom'];

		$layout_style = '';

		if ( !empty( $padding_top ) ) {
			$layout_style .= 'padding-top: ' . $padding_top . '; ';
		}

		if ( !empty( $padding_bottom ) ) {
			$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
		}

		if ( !empty( $margin_top ) ) {
			$layout_style .= 'margin-top: ' . $margin_top . '; ';
		}

		if ( !empty( $margin_bottom ) ) {
			$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
		}

		$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

		if ( !empty( $animation_type ) ) {
			$animation_wrapper_start = intense_get_animation_wrapper_start( $animation_type, $animation_trigger, $animation_scroll_percent, $animation_delay );
			$animation_wrapper_end = intense_get_animation_wrapper_end();
		} else {
			$animation_wrapper_start = null;
			$animation_wrapper_end = null;
		}

		$intense_post_type = $intense_post_types->get_post_type( $post_type );

		if ( $is_timeline && isset( $timeline_readmore ) && $timeline_readmore != 'Read More' ) {
			$read_more_text = $timeline_readmore;
		}

		$intense_custom_post = array(
			'plugin_layout_style' => $layout_style,
			'cancel_plugin_layout_style' => $no_layout_style,
			'post_type' => $intense_post_type,
			'taxonomy' => $taxonomy,
			'template' => $template,
			'categories' => $categories,
			'exclude_categories' => $exclude_categories,
			'exclude_post_ids' => $exclude_post_ids,
			'authors' => $authors,
			'order_by' => $order_by,
			'order' => $order,
			'posts_per_page' => $posts_per_page,
			'post_count' => $post_count,
			'image_size' => $image_size,
			'image_shadow' => $image_shadow,
			'hover_effect' => $hover_effect,
			'show_all' => $show_all,
			'show_meta' => $show_meta,
			'show_author' => $show_author,
			'infinite_scroll' => $infinite_scroll,
			'show_filter' => $show_filter,
			'show_images' => $show_images,
			'show_missing_image' => $show_missing_image,
			'show_social_sharing' => $show_social_sharing,
			'show_read_more' => $show_read_more,
			'read_more_text' => $read_more_text,
			'timeline_mode' => $timeline_mode,
			'timeline_showyear' => $timeline_showyear,
			'timeline_readmore' => $timeline_readmore,
			'timeline_color' => $timeline_color,
			'filter_easing' => $filter_easing,
			'filter_effects' => $filter_effects,
			'hover_effect_color' => $hover_effect_color,
			'hover_effect_opacity' => $hover_effect_opacity,
			'sticky_mode' => $sticky_mode,
			'image_border_radius' => $image_border_radius,
			'timeline_border_radius' => $timeline_border_radius,
			'animation_type' => $animation_type,
			'animation_trigger' => ( isset( $animation_trigger ) ? $animation_trigger : null ) ,
			'animation_scroll_percent' => ( isset( $animation_scroll_percent ) ? $animation_scroll_percent : null ),
			'animation_delay' => ( isset( $animation_delay ) ? $animation_delay : null ),
			'animation_wrapper_start' => $animation_wrapper_start,
			'animation_wrapper_end' => $animation_wrapper_end,
			'is_slider' => $is_slider,
			'category_list' => $category_list,
			'rtl' => $rtl
		);

		$original_query = $wp_query;
		$page_query_var = get_query_var( 'page' );
		$paged_query_var =  get_query_var( 'paged' );

		if ( !empty( $page_query_var ) ) {
			$paged = $page_query_var;
		} else if ( !empty( $paged_query_var ) ) {
			$paged = $paged_query_var;
		} else {
			$paged = 1;
		}

		$wp_query = null;
		$iscat_array = false;
		$cat_array = array();
		$blog_cat_array = array();
		$timeline_events = '';
		$mixid = 'mix_list_' . rand();
		$blogid = 'cpt_list_' . rand();
		$filter_output = '';

		$args = array(
			'post_type' => $post_type,
			'orderby' => $order_by,
			'order' => $order,
			'paged'=> $paged,
			'nopaging' => ( $show_all == 0 ? false : true ),
			'tax_query' => ''
		);

		if ( 'inline' == $sticky_mode || 'ignore' == $sticky_mode ) {
			$args['ignore_sticky_posts'] = 1;
		}

		if ( $post_type == "intense_portfolio" && $order_by == "meta_value" ) {
			$args["meta_key"] = 'intense_portfolio_date';
		}

		if ( $post_type == "intense_project" && $order_by == "meta_value" ) {
			$args["meta_key"] = 'intense_project_date';
		}

		if ( $authors != '' ) {
			$args['author'] = $authors;
		}

		if ( $categories != '' && isset( $taxonomy ) && $taxonomy != '' ) {
			$iscat_array = true;

			if ( strpos( $categories, ',' ) ) {
				$cat_array = explode( ',', str_replace( ' ', '', $categories ) );

				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'  => 'slug',
						'terms'  => $cat_array
					)
				);
			} else {
				$cat_array = array( $categories );
				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'  => 'slug',
						'terms'  => array( $categories )
					)
				);
			}
		}

		if ( $include_post_ids != '' ) {
			if ( strpos( $include_post_ids, ',' ) ) {
				$id_array = explode( ',', $include_post_ids );
				$args['post__in'] = $id_array;
			} else {
				$args['post__in'] = array( $include_post_ids );
			}
		}

		$args['category__not_in'] = array();

		if ( $exclude_categories != '' ) {
			$ex_array = array();
			if ( strpos( $exclude_categories, ',' ) ) {
				$ex_array = explode( ',', $exclude_categories );
			} else {
				$ex_array = array( $exclude_categories );
			}

			if ( is_array( $ex_array ) ) {
				$ex_cat_array = array();

				foreach ( $ex_array as $ex_cat ) {
					$term = get_term_by( 'slug', $ex_cat, $taxonomy );
					$ex_cat_array[] = $term->term_id;
				}

				if ( is_array( $ex_cat_array ) ) {
					$args['category__not_in'] = $ex_cat_array;
				}
			}

		}

		if ( $exclude_post_ids != '' ) {
			if ( strpos( $exclude_post_ids, ',' ) ) {
				$id_array = explode( ',', $exclude_post_ids );
				$args['post__not_in'] = $id_array;
			} else {
				$args['post__not_in'] = array( $exclude_post_ids );
			}
		}

		if ( $post_count != '' && is_numeric( $post_count ) ) {
			$args2 = $args;
			$post_in_array = array();
			$count = 0;
			$args2['nopaging'] = true;

			$theposts = get_posts( $args2 );

			foreach( $theposts as $post ) {				
				$post_in_array[] = $post->ID;
				$count++;

				if ( $count == $post_count ) {
				 	break;
				}
			}

			wp_reset_postdata();

			if ( is_array( $post_in_array ) ) {
				$args['post__in'] = $post_in_array;
			}
		}

		$args['posts_per_page'] = $posts_per_page;

		$blog_category = get_terms( $taxonomy, array( 'exclude' => $args['category__not_in'] ) );

		if ( $iscat_array ) {
			if ( is_array( $blog_category ) ) {
				$cat_array = $this->addParents( $blog_category, $cat_array, $taxonomy );
			}

			$cat_array = $this->addChildren( $cat_array, $taxonomy );
		}

		$wp_query = new WP_Query( $args );

		$output .= "<div id='" . $mixid . "' class='intense row' style='$no_layout_style'><div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>";

		//Setup filters
		$filter_output .= "<div class='intense row' style='$no_layout_style'><div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>";

		if ( is_array( $blog_category ) ) {
			$filter_output .= "<nav role='navigation' id='nav-below-filter' class='filter-post'>";
			$filter_output .= "<ul class='intense pager " . ( $rtl ? "rtl " : "" ) . "'>";
			$filter_output .= "<li><a class='filter btn-mini active' data-filter='all' href='#allfilter'>" . $filter_all_text . "</a></li>";

			foreach ( $blog_category as $blog_cat ) {
				if ( $iscat_array ) {
					if ( in_array( $blog_cat->slug, $cat_array ) ) {
						$filter_output .= "<li><a class='filter btn-mini' data-filter='" . $blog_cat->slug . "' href='#" . $blog_cat->slug . "filter'>" . esc_html( $blog_cat->name ) . "</a></li>";
					}
				} else {
					$filter_output .= "<li><a class='filter btn-mini' data-filter='" . $blog_cat->slug . "' href='#" . $blog_cat->slug . "filter'>" . esc_html( $blog_cat->name ) . "</a></li>";
				}
				$blog_cat_array[] = $blog_cat->slug;
			}

			$filter_output .= "</ul>";
			$filter_output .= "</nav>";
		}

		$filter_output .= "</div></div>";

		//Add filter to output
		if ( !$is_timeline && !$infinite_scroll && ( ( $show_filter && $categories == '' ) || ( $iscat_array && $show_filter ) ) ) {
			$output .= $filter_output;
		}


		if ( $categories != '' ) {
			$intense_custom_post['category_list'] = $categories;
		} else {
			$intense_custom_post['category_list'] = implode(', ', $blog_cat_array);
		}

		if ( $infinite_scroll ) {
			$blogid = 'intense_infinite';
		}

		$output .= "<div id='" . $blogid . "' class='intense row' style='" . $layout_style . " padding-left: 15px; padding-right: 15px; overflow-y: inherit;'>";

		$i = 0;

		$slider = '';

		if ( $is_slider ) {
			$output = ''; //kill the output
			$slider .= '[intense_slider type="owl-carousel" 
		    			items="' . $items . '"
                    	items_desktop="' . $items_desktop . '"
                    	items_desktop_small="' . $items_desktop_small . '"
                    	items_tablet="' . $items_tablet . '"
                    	items_mobile="' . $items_mobile . '"
                    	navigation_position="' . $navigation_position . '"
                    	navigation_prev_icon="' . $navigation_prev_icon . '"
                    	navigation_next_icon="' . $navigation_next_icon . '"
                    	navigation_size="' . $navigation_size . '"
                    	navigation_prev_text="' . $navigation_prev_text . '"
                    	navigation_next_text="' . $navigation_next_text . '"
                    	navigation_color="' . $navigation_color . '"
                    	navigation_font_color="' . $navigation_font_color . '"
                    	navigation_prev_border_radius="' . $navigation_prev_border_radius . '"
                    	navigation_next_border_radius="' . $navigation_next_border_radius . '"
                    	navigation_pagination_type="' . $navigation_pagination_type . '"
		    		]';
		}

		$masonry = '';

		if ( $is_masonry ) {
			$output = ''; //kill the output
			$masonry .= '[intense_masonry id="' . $blogid  . '" gutter="' . $gutter . '"]';
		}

		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			$item_classes = '';
			$editlink = '';
			$portfolio_categories = '';
			$item_cats = get_the_terms( $post->ID, $taxonomy );
			$intense_custom_post['image_shadow'] = '';
			$intense_custom_post['hover_effect'] = '';
			$intense_custom_post['hover_effect_color'] = '';
			$intense_custom_post['hover_effect_opacity'] = '';
			$intense_custom_post['index'] = $i;
			$margintop = '0px';

			if ( $post_type == "intense_portfolio" ) {
				$portfolio_date = get_field( 'intense_portfolio_date' );
			}

			if ( $post_type == "intense_project" ) {
				$portfolio_date = get_field( 'intense_project_date' );
			}

			if ( has_post_thumbnail() ) {
				if ( isset( $image_shadow ) ) {
					$intense_custom_post['image_shadow'] = $image_shadow;
				} elseif ( get_field( 'intense_image_shadow' ) != '' ) {
					$intense_custom_post['image_shadow'] = get_field( 'intense_image_shadow' );
				}

				if ( isset( $hover_effect ) ) {
					$intense_custom_post['hover_effect'] = $hover_effect;
				} elseif ( get_field( 'intense_hover_effect' ) != '' ) {
					$intense_custom_post['hover_effect'] = get_field( 'intense_hover_effect' );
				}

				if ( isset( $hover_effect_color ) ) {
					$intense_custom_post['hover_effect_color'] = $hover_effect_color;
				} elseif ( get_field( 'intense_effect_color' ) != '' ) {
					$intense_custom_post['hover_effect_color'] = get_field( 'intense_effect_color' );
				}

				if ( isset( $hover_effect_opacity ) ) {
					$intense_custom_post['hover_effect_opacity'] = $hover_effect_opacity;
				} elseif ( get_field( 'intense_effect_opacity' ) != '' ) {
					$intense_custom_post['hover_effect_opacity'] = get_field( 'intense_effect_opacity' );
				}
			}

			$terms = get_the_terms( $post->ID, $taxonomy );

			if ( $terms ) {
				$categories_list = array();

				foreach ( $terms as $term ) {
					$item_classes .= $term->slug . ' ';
					$categories_list[] = $term->name;
				}

				$intense_custom_post['categories'] = join( ", ", $categories_list );
			}

			if ( current_user_can( 'manage_options' ) ) {

				$intense_custom_post['edit_link'] = "&nbsp;" . intense_run_shortcode( 'intense_button', array( 'size' => 'mini', 'color' => 'inverse', 'class' => 'post-edit-link' , 'link' => get_edit_post_link( $post->ID ), 'title' => __( "Edit Post", "intense" ) ), __( "Edit", "intense" ) );
			} else {
				$intense_custom_post['edit_link'] = '';
			}

			if ( !$is_timeline && !$infinite_scroll && ( ( $show_filter && $categories == '' ) || ( $iscat_array && $show_filter ) ) ) {
				$item_classes .= 'intense mix ';
			}

			$intense_custom_post['post_classes'] = $item_classes;

			// if the template is numeric then it is a templates that they have saved in WordPress
			if ( is_numeric( str_replace("template_", "", $template ) ) ) {
				$template_post = get_post( str_replace("template_", "", $template ) );
				$template_file = Intense_Post_Type_Templates::get_template_cache( $template_post->ID );
			} else {
				$template_file = intense_locate_plugin_template( '/custom-post/' .  get_post_type() . '/' . $template . '.php' );
			}

			if ( !isset( $template_file ) ) {
				$template_file = intense_locate_plugin_template( '/custom-post/post/' . $template . '.php' );
			}

			if ( !isset( $template_file ) ) {
				$template_file = intense_locate_plugin_template( '/custom-post/post/one.php' );
			}

			$post_content = intense_load_plugin_template( $template_file );

			if ( $is_timeline ) {
				$timeline_events .= "[intense_timeline_event animation_type='$animation_type' animation_trigger='$animation_trigger' animation_scroll_percent='$animation_scroll_percent' animation_delay='$animation_delay' type='blog_post' featured='" . ( is_sticky() && 'ignore' != $sticky_mode ? "1" : "0" ) . "' date='" . esc_html( get_the_date( 'Y-m-d' ) ) . "' column_mode='" . $timeline_mode . "' color='" . $timeline_color . "' image_border_radius='" . $image_border_radius . "' event_border_radius='" . $timeline_border_radius . "' use_content='1']" . $post_content . "[/intense_timeline_event]";
			} else if ( $is_slider ) {
				$slider .= '[intense_slide]<div style="padding: 0 ' . $slide_padding_right . 'px 0 0">' . $post_content . '</div>[/intense_slide]';
			} else if ( $is_masonry ) {
				$masonry .= '[intense_masonry_item ' . 
				'size="' . ( is_sticky() && 'ignore' != $sticky_mode ? $masonry_sticky_size : $masonry_size ) . '" ' .
				'medium_size="' . ( is_sticky() && 'ignore' != $sticky_mode ? $masonry_sticky_medium_size : $masonry_medium_size ) . '" ' . 
				'small_size="' . ( is_sticky() && 'ignore' != $sticky_mode ? $masonry_sticky_small_size : $masonry_small_size ) . '" ' . 
				'extra_small_size="' . ( is_sticky() && 'ignore' != $sticky_mode ? $masonry_sticky_extra_small_size : $masonry_extra_small_size ) . '" ' .
				'width="' . ( is_sticky() && 'ignore' != $sticky_mode ? $masonry_sticky_width : $masonry_width ) . '"]' . $post_content . '[/intense_masonry_item]';
			} else {
				$output .= $post_content;
			}

			$i++;
		}

		if ( $is_timeline ) {
			$output .= do_shortcode( "[intense_timeline show_year='" . $timeline_showyear . "' column_mode='" . $timeline_mode . "' order='" . $order . "']" . $timeline_events . "[/intense_timeline]" );
		} else if ( $is_slider ) {
			$slider .= '[/intense_slider]';
	    	$output = do_shortcode( $slider );	  	
		} else if ( $is_masonry ) {
			$masonry .= '[/intense_masonry]';
	    	$output = do_shortcode( $masonry );
	    	$output .= intense_return_content_nav( 'nav-below' );	

	    	if ( $infinite_scroll ) {
	    		$output .= "<script> jQuery(function($){";
				$output .= " $(document).ready(function() {";
				$output .= "
					var lastVisibleYear = null;
					var scrollTop = null;

					jQuery(window).scroll(function() {
						scrollTop = jQuery(window).scrollTop();
					});

					jQuery.ias({
			            container : '#intense_infinite',
			            item: '.item',
			            pagination: '.intense.pagination',
						triggerPageThreshold: 100,
			            next: 'a.pagination-next',
			            loader: '<img src=\"" . INTENSE_PLUGIN_URL . "/assets/img/loader.gif\" />',
			            onRenderComplete: function(items) {
			            	intenseSetupAnimations();
			            	
						    jQuery.each(items, function(i, el) {
								if ( lastVisibleYear && jQuery(el).hasClass('year') && jQuery(el).text() == lastVisibleYear ) {
									jQuery(el).next().removeClass('offset-first');
									jQuery(el).remove();
								}
						    });
							jQuery('#intense_infinite').packery('destroy');	
							var packery = intenseInitMasonry();						
							jQuery(window).scrollTop(scrollTop);
							setTimeout(function() {
								packery.packery();	
							}, 500);
							packery.packery();							
						},
						beforePageChange: function(scrollOffset, nextPageUrl) {							
							
							lastVisibleYear = jQuery('.year').last().text();
						},
						onPageChange: function() {														
							var top = ($('#intense_infinite').height()) + 'px';
							var left = (($('#intense_infinite').width() / 2) - 25) + 'px';
							
							$('.ias_loader, .ias_trigger').css({ 
								position: 'absolute',
								top: top,
								width: '50px',
								height: '50px',
								left: left
							});
						}
			        });";
				$output .= " });";
				$output .= "}); </script>";
			}  	
		}

		if ( !$is_slider && !$is_masonry ) {

			$output .= "</div>";

			$output .= "<script> jQuery(function($){";
			$output .= " $(document).ready(function() {";

			if ( ( $show_filter && $categories == '' && !$is_timeline && !$infinite_scroll ) || ( $iscat_array && $show_filter && !$is_timeline && !$infinite_scroll ) ) {
				// mixitup jquery plugin
				intense_add_script( 'mixitup' );

				$output .= "  $('#" . $blogid . "').mixitup({";

				if ( strlen( $filter_effects ) > 0 ) {
					$output .= "   effects: ['" . str_replace( ',', '\',\'', $filter_effects ) . "'],";
				} else {
					$output .= "   effects: ['fade','scale'],";
				}

				if ( strlen( $filter_easing ) > 0 ) {
					$output .= "   easing: '" . $filter_easing . "',";
				} else {
					$output .= "   easing: 'smooth',";
				}

				$output .= "	filterSelector: '#" . $mixid . " .filter'";

				$output .= "  }); $('.intense.mix').css('visibility', 'visible');";

				$GLOBALS['IntensePluginInlineStyle']['#' . $blogid] = "
					#" . $blogid . " {
						overflow-y: hidden; }
					#" . $blogid . " .mix {
					  	opacity: 0;
					  	display: none; }
					#" . $blogid . " h3, #" . $blogid . " .widget-title, #" . $blogid . " h1 {
					  	line-height: normal !important; }
					#" . $blogid . " div.mix {
					  	vertical-align: top; }";
			}

			if ( $infinite_scroll ) {
				$output .= "
					var lastVisibleYear = null;
					jQuery.ias({
			            container : '#intense_infinite',
			            item: '.intense.intense_post',
			            pagination: '.intense.pagination',
			            triggerPageThreshold: 100,
			            next: 'a.pagination-next',
			            loader: '<img src=\"" . INTENSE_PLUGIN_URL . "/assets/img/loader.gif\" />',
			            onRenderComplete: function(items) {
			            	intenseSetupAnimations();
			            	
						    jQuery.each(items, function(i, el) {
								if ( lastVisibleYear && jQuery(el).hasClass('year') && jQuery(el).text() == lastVisibleYear ) {
									jQuery(el).next().removeClass('offset-first');
									jQuery(el).remove();
								}
						    });
						},
						beforePageChange: function(scrollOffset, nextPageUrl) {
							lastVisibleYear = jQuery('.year').last().text();
						}
			        });";
			}

			$output .= " });";
			$output .= "}); </script>";
			$output .= "</div></div>";
			$output .= intense_return_content_nav( 'nav-below' );

		}

		$wp_query = null;
		$wp_query = $original_query;
		wp_reset_postdata();

		return $output;
	}

	function render_dialog_script() {
		$ajax_nonce = wp_create_nonce( "intense-plugin-noonce" );
?>
		<script>
			jQuery(function($) {
				//$('#animation').select2();
				$(".timelinesettings").hide();
				$(".wpsettings").show();

				$("#template").change(function() {
					var template = $(this).val();

					$(".wpsettings").hide();
					$(".timelinesettings").hide();

					if (/timeline/i.test(template)) {
						$(".timelinesettings").show();
					} else {
						$(".wpsettings").show();
					}
				});

				$("input:checkbox[name ='infinite_scroll']").change(function() {
					if ( jQuery('#infinite_scroll').is(':checked') ) {
						$("#show_filter").attr('checked', false);
						$("#show_filter").attr('disabled', true);
						$("#show_all").attr('checked', false);
						$("#show_all").attr('disabled', true);
						$(".filtersettings").hide();
					} else {
						$("#show_filter").attr('checked', true);
						$("#show_filter").removeAttr("disabled");
						$("#show_all").removeAttr("disabled");
						$(".filtersettings").show();
					}
				});

				$("input:checkbox[name ='show_filter']").change(function() {
					if ( jQuery('#show_filter').is(':checked') ) {
						$(".filtersettings").show();
					} else {
						$(".filtersettings").hide();
					}
				});

				$('#post_type').change(function() {
					setupLists();
				});

				$('#taxonomy').change(function() {
					setupCategory();
				});

				setupLists();
				setupCategory();

				function setupLists() {
					$("#taxonomy").empty();
					$("#categories").empty();
					$("#exclude_categories").empty();
					$("#template").empty();

					if ( $("#post_type").val() != '' ) {
						var $loader = $('<img class="loader" src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/loader.gif" style="margin-left: -105px; margin-top: 5px; position: absolute;" />');
						var $loader2 = $loader.clone();

						$("#taxonomy").after($loader);
						$("#template").after($loader2);

						$.ajax({
							type: "POST",
							url: ajaxurl + '?action=intense_generic_post_action',
							data: {
								security: '<?php echo $ajax_nonce; ?>',
								postType: $("#post_type").val(),
								postTaxonomy: $("#taxonomy").val()
						 	}
						}).done(function (data) {
							var results = jQuery.parseJSON(data);
							var templates = results.templates;
							var taxonomies = results.taxonomies;

							$.each(taxonomies, function(index, taxonomy) {
								$("#taxonomy").append('<option value="' + taxonomy.key + '">' + taxonomy.value + '</option>');
							});

							setupCategory();

							$.each(templates, function(index, template) {
								$("#template").append('<option value="' + template.key + '">' + template.value + '</option>');
							});

							$loader.remove();
							$loader2.remove();
						});
					}
				}

				function setupCategory() {
					$("#categories").empty();

					if ( $("#taxonomy").val() !== null && $("#taxonomy").val().length ) {
						var $loader = $('<img class="loader" src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/loader.gif" style="margin-left: -105px; margin-top: 35px; position: absolute;" />');
						var $loader2 = $loader.clone();

						$("#categories").after($loader);
						$("#exclude_categories").after($loader2);

						$.ajax({
							type: "POST",
							url: ajaxurl + '?action=intense_generic_post_action',
							data: {
								security: '<?php echo $ajax_nonce; ?>',
								postType: $("#post_type").val(),
								postTaxonomy: $("#taxonomy").val()
						 	}
						}).done(function (data) {
							var results = jQuery.parseJSON(data);
							var categories = results.categories;

							$.each(categories, function(index, category) {
								$("#categories").append('<option value="' + category.key + '">' + category.value + '</option>');
								$("#exclude_categories").append('<option value="' + category.key + '">' + category.value + '</option>');
							});

							$loader.remove();
							$loader2.remove();
						});
					}
				}
			});
		</script>
    <?php
	}

	function addParents( $blog_category, $cat_array, $taxonomy ) {
		foreach ( $blog_category as $blog_cat ) {
			$parent = get_term( $blog_cat->parent, $taxonomy ); // get parent term

			if ( !is_wp_error( $parent ) ) {
				if ( in_array( $parent->slug, $cat_array ) && !in_array( $blog_cat->slug, $cat_array ) ) {
					array_push( $cat_array, $blog_cat->slug );
				}
			}
		}

		return $cat_array;
	}

	function addChildren( $cat_array, $taxonomy ) {		
		foreach ( $cat_array as &$value ) {
			$value_id = get_term_by( 'slug', $value, $taxonomy );
			$children = get_term_children( $value_id->term_id, $taxonomy ); // get children

			if ( sizeof( $children ) > 0 ) {
				foreach ( $children as $child_cat ) {
					$term = get_term_by( 'id', $child_cat, $taxonomy );
					if ( !in_array( $term->slug, $cat_array ) ) {
						array_push( $cat_array, $term->slug );
					}
				}
			}
		}

		return $cat_array;
	}
}

class Intense_Blog extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Blog', 'intense' );
		$this->custom_post_type = 'post';
		$this->taxonomy = 'category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "post";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "post";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Portfolio extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Portfolio', 'intense' );
		$this->custom_post_type = 'intense_portfolio';
		$this->icon = 'dashicons-portfolio';
		$this->taxonomy = 'portfolio_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_portfolio";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_portfolio";		

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "portfolio_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Project extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Project', 'intense' );
		$this->custom_post_type = 'intense_project';
		$this->icon = 'dashicons-portfolio';
		$this->taxonomy = 'intense_project_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_project";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_project";		

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_project_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Books extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Books', 'intense' );
		$this->custom_post_type = 'intense_books';
		$this->icon = 'dashicons-book-alt';
		$this->taxonomy = 'intense_book_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_books";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["post_excerpt"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["post_excerpt"]["value"] = "intense_book_excerpt";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_books";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_book_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Clients extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Clients', 'intense' );
		$this->custom_post_type = 'intense_clients';
		$this->icon = 'dashicons-businessman';
		$this->taxonomy = 'intense_clients_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_clients";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_clients";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_clients_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Coupons extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Coupons', 'intense' );
		$this->custom_post_type = 'intense_coupons';
		$this->icon = 'dashicons-intense-coupon';
		$this->taxonomy = 'intense_coupons_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_coupons";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_coupons";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_coupons_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Events extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Events', 'intense' );
		$this->custom_post_type = 'intense_events';
		$this->icon = 'dashicons-calendar';
		$this->taxonomy = 'intense_events_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_events";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_events";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_events_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Jobs extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Jobs', 'intense' );
		$this->custom_post_type = 'intense_jobs';
		$this->icon = 'dashicons-hammer';
		$this->taxonomy = 'intense_job_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_jobs";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_jobs";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_job_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Locations extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Locations', 'intense' );
		$this->custom_post_type = 'intense_locations';
		$this->icon = 'dashicons-location-alt';
		$this->taxonomy = 'intense_locations_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_locations";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_locations";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_locations_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Movies extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Movies', 'intense' );
		$this->custom_post_type = 'intense_movies';
		$this->icon = 'dashicons-video-alt';
		$this->taxonomy = 'intense_movie_genre';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_movies";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["post_excerpt"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["post_excerpt"]["value"] = "intense_movie_excerpt";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_movies";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_movie_genre";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_News extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'News', 'intense' );
		$this->custom_post_type = 'intense_news';
		$this->icon = 'dashicons-intense-newspaper';
		$this->taxonomy = 'intense_news_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_news";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_news";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_news_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Pages extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Pages', 'intense' );
		$this->icon = 'dashicons-admin-page';
		$this->taxonomy = '';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "page";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "page";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Quotes extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Quotes', 'intense' );
		$this->custom_post_type = 'intense_quotes';
		$this->icon = 'dashicons-editor-quote';
		$this->taxonomy = 'intense_quotes_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_quotes";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_quotes";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_quotes_category";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Recipes extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recipes', 'intense' );
		$this->custom_post_type = 'intense_recipes';
		$this->icon = 'dashicons-intense-food';
		$this->taxonomy = 'intense_recipes_ingredients';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_recipes";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_recipes";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_recipes_ingredients";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Team extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Team', 'intense' );
		$this->custom_post_type = 'intense_team';
		$this->icon = 'dashicons-groups';
		$this->taxonomy = 'intense_team_position';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_team";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_team";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_team_position";

		return parent::shortcode( $atts, $content );
	}
}

class Intense_Testimonials extends Intense_Custom_Post {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Testimonials', 'intense' );
		$this->custom_post_type = 'intense_testimonials';
		$this->icon = 'dashicons-testimonial';
		$this->taxonomy = 'intense_testimonials_category';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_testimonials";
		$this->fields["general_tab"]["fields"]["post_type"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["authors"]["type"] = "hidden";
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_testimonials";

		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_testimonials_category";

		return parent::shortcode( $atts, $content );
	}
}
