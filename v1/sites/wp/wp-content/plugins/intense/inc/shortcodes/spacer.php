<?php

class Intense_Spacer extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Spacer', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-resize-vertical';
		$this->show_preview = true;

		$this->fields = array(
			'height' => array(
				'type' => 'text',
				'title' => __( 'Height', 'intense' ),
				'description' => __( 'measured in pixels', 'intense' ),
				'default' => '100'
			),
			'clear' => array(
				'type' => 'dropdown',
				'title' => __( 'Clear', 'intense' ),
				'description' => __( 'use in correlation with floated elements', 'intense' ),
				'default' => '',
				'options' => array(
					'' => '',
					'left' => __( 'Left', 'intense' ),
					'right' => __( 'Right', 'intense' ),
					'both' => __( 'Both (clearfix)', 'intense' )
				)				
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$style = '';
		$class = '';

		switch ( $clear ) {
		case 'right':
			$style = 'clear: right;';
			break;
		case 'left':
			$style = 'clear: left;';
			break;
		case 'both':
			$class = 'clearfix';
			break;
		}

		return "<div class='intense spacer $class' style='height: " . $height . "px; $style'></div>";
	}
}
