<?php

class Intense_Fullscreen_Video extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Fullscreen Video', 'intense' );
    $this->category = __( 'Media', 'intense' );
    $this->icon = 'dashicons-intense-film';
    $this->show_preview = false;

    $this->fields = array(
      'video' => array(
        'type' => 'text',
        'title' => __( 'Video', 'intense' ),
        'description' => __( 'Youtube or Vimeo url or id', 'intense' ),
        'skinnable' => '0'
      ),
      'key_control' => array(
        'type' => 'checkbox',
        'title' => __( 'Allow Key Control', 'intense' ),
        'default' => "0",
      ),
      'captions' => array(
        'type' => 'checkbox',
        'title' => __( 'Show Captions', 'intense' ),
        'default' => "0",
        'class' => 'youtubesettings'
      ),
      'loop' => array(
        'type' => 'checkbox',
        'title' => __( 'Loop Video', 'intense' ),
        'default' => "0",
      ),
      'highdef' => array(
        'type' => 'checkbox',
        'title' => __( 'Show in High Definition', 'intense' ),
        'default' => "0",
        'class' => 'youtubesettings'
      ),
      'annotations' => array(
        'type' => 'checkbox',
        'title' => __( 'Show Annotations', 'intense' ),
        'default' => "0",
      ),
      'volume' => array(
        'type' => 'text',
        'title' => __( 'Volume', 'intense' ),
        'description' => __( '0 - 100: default is 50. A negative number will completely mute the volume.', 'intense' ),
        'default' => '50',
      ),
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    intense_add_script( 'okvideo' );

    return
      "<script>
      jQuery(function($){
          $.okvideo({
            video: '$video',
            disablekeyControl:  " . ( $key_control ? 0 : 1 ) . ",
            captions: " . $captions . ",
            loop: " . $loop . ",
            hd: " . $highdef . ",
            annotations: " . $annotations . ",
            volume: " . $volume . "
          });
      });
    </script>";
  }

  function render_dialog_script() {
  ?>
    <script>
      jQuery(function($) {
        $(".youtubesettings").hide();
        $(".vimeosettings").hide();

        $(document).on('keyup paste', "#video", function() {
          setTimeout(function () {
            var type = $("#video").val();

            if (/youtube.com/.test(type)){
              type = 'youtube';
            } else if (/vimeo.com/.test(type)) {
                type = 'vimeo';
            } else if (/[A-Za-z0-9_]+/.test(type)) {
              var id = new String(type.match(/[A-Za-z0-9_]+/));
              if (id.length == 11) {
                type = 'youtube';
              } else if ($.isNumeric(type)) {
                type = 'vimeo';
              }
            }

            $(".youtubesettings").hide();
            $(".vimeosettings").hide();

            switch(type) {
              case "youtube":
                $(".youtubesettings").show();
                break;
              case "vimeo":
                $(".vimeosettings").show();
            }
          }, 100);
        });
      });
    </script>
    <?php
  }
}
