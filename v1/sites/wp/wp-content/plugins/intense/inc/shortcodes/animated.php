<?php

class Intense_Animated extends Intense_Shortcode {
	function __construct() {
        global $intense_visions_options;
        global $intense_animations;

        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-intense-movie5';
        $this->title = __( 'Animated', 'intense' );
        $this->show_preview = false;
        // $this->preview_content = '<img src="' . INTENSE_PLUGIN_URL . '/assets/img/icon_32x32.png" />';
        $this->is_container = true;

        $this->fields = array(
            'type' => array(
            	'type' => 'animation',
                'title' => __( 'Animation', 'intense' ),			
				'placeholder' => __( 'Select an animation', 'intense' ),
            ),
            'trigger' => array(
            	'type' => 'dropdown',
                'title' => __( 'Trigger', 'intense' ),
                'description' => __( '<strong>scroll</strong> - triggered when the user scrolls their browser window<br><strong>hover</strong> - triggered when the user\'s mouse hovers over the item<br><strong>click</strong> - triggered when the user clicks on an item<br><strong>delay</strong> - triggered after a given number of milliseconds', 'intense' ),
                'default' => 'scroll',
				'options' => array(
					'scroll' => __( 'Scroll', 'intense' ),
					'hover' => __( 'Hover', 'intense' ),
					'click' => __( 'Click', 'intense' ),
					'delay' => __( 'Delay', 'intense' )
				),			
            ), 
            'trigger_client_id' => array(
            	'type' => 'text',
            	'title' => __( 'Trigger Client ID', 'intense' ),
            	'description' => __( 'animate when an html element with this id is triggered (optional)', 'intense' ),
            	'class' => 'triggeridsettings',
            	'skinnable' => '0'            	
            ),          
            'scroll_percent' => array(
            	'type' => 'dropdown',
                'title' => __( 'Scroll Percentage', 'intense' ),
				'description' => __( 'animate when <span id="scrollpercentagevalue"></span>% of item is in view', 'intense' ),
				'options' => array(
					'0' => '0',
					'10' => '10',
					'20' => '20',
					'30' => '30',
					'40' => '40',
					'50' => '50',
					'60' => '60',
					'70' => '70',
					'80' => '80',
					'90' => '90',
					'100' => '100'
				),
				'default' => '10',
				'class' => 'scrollpercentagesettings'           
            ),
            'delay' => array(
            	'type' => 'text',
                'title' => __( 'Delay', 'intense' ),
				'description' => __( 'Number of milliseconds to wait before triggering the animation. For the delay trigger, this is the number of milliseconds after the page loads. For all other triggers, this is the number of milliseconds after the trigger occurs.', 'intense' ),
				'default' => '0'
            ),
            'reset' => array(
                'type' => 'text',
                'title' => __( 'Reset', 'intense' ),
                'description' => __( 'Number of milliseconds to wait before resetting the animation. Leave blank to not reset.', 'intense' ),
                'default' => "",
                'class' => 'clicksetttings',
            ),            
        );
    }    
	
	function shortcode( $atts, $content = null ) {
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		return intense_get_animation_wrapper_start( $type, $trigger, $scroll_percent, $delay, $trigger_client_id, $reset ) .
			do_shortcode( $content ) . intense_get_animation_wrapper_end();
	}

	function render_dialog_script() {
		?>
		<script>
		jQuery(function($) {
			$('#scrollpercentagevalue').html($('#scroll_percent').val());
			$('.triggeridsettings').hide();
			$('.clicksetttings').hide();
			$('.hoversettings').hide();

			$("#trigger").change(function() {
				var trigger = $(this).val();

				$('.scrollpercentagesettings').hide();
				$('.triggeridsettings').hide();
				$('.clicksetttings').hide();

				switch(trigger) {
					case 'scroll':
						$('.scrollpercentagesettings').show();
						break;
					case 'click':
						$('.clicksetttings').show();
						$('.triggeridsettings').show();
						break;
					case 'hover':
						$('.hoversettings').show();
						$('.triggeridsettings').show();
						break;
				}
			});

			$('#scroll_percent').change(function() {
				$('#scrollpercentagevalue').html($('#scroll_percent').val());
			});
		});
		</script>
		<?php
	}
}

function intense_get_animation_wrapper_start( $type, $trigger, $scroll_percent, $delay, $trigger_client_id = null, $reset = false ) {
	global $intense_animations;

	$animation = $intense_animations[ $type ];
	$animation_source = $animation[0];
	$animation_category = $animation[1];
	$starthidden = $animation[2];
	$endhidden = $animation[3];

	intense_load_animation_CSS( $type );	
	intense_add_script( 'intense.animated' );

	if ( !empty( $trigger_client_id ) ) {
		$trigger_client_id = ' data-client-id="' . $trigger_client_id . '"';
	} else {
		$trigger_client_id = '';
	}

	if ( !empty( $reset ) && $reset ) {
		$reset = ' data-reset="' . $reset . '"';
	} else {
		$reset = '';
	}

	return "<div class='intense $trigger-animated' " . ( $starthidden ? 'style="display:none;"' : '' ) . " data-delay='$delay' data-scrollpercent='$scroll_percent' data-starthidden='$starthidden' data-endhidden='$endhidden' data-animation='$type'" . $trigger_client_id . $reset . ">";
}

function intense_get_animation_wrapper_end() {
	return '</div>';
}

function intense_get_animations_start_hidden() {
	global $intense_animations;

	$animations = array();

	foreach ($intense_animations as $animation => $attributes) {
		if ( $attributes[2] ) {
			$animations[ $animation ] = $animation;
		}
	}	

	return $animations;
}

function intense_get_animations_end_hidden() {
	global $intense_animations;

	$animations = array();

	foreach ($intense_animations as $animation => $attributes) {
		if ( $attributes[3] ) {
			$animations[ $animation ] = $animation;
		}
	}	

	return $animations;
}

function intense_get_animation_categories() {
	global $intense_animations;

	$categories = array();

	foreach ($intense_animations as $animation => $attributes) {
		if ( !in_array( $attributes[1], $categories ) ) {
			$categories[] = $attributes[1];			
		}
	}

	natsort( $categories );

	return $categories;
}

function intense_load_animation_CSS( $type ) {
	global $intense_animations;

	$minified = ( INTENSE_DEBUG ? "min." : "" );	

	if ( isset( $intense_animations[ $type ] ) ) {
		$animation = $intense_animations[ $type ];
		$animation_source = $animation[0];
		$animation_category = $animation[1];
		$starthidden = $animation[2];
		$endhidden = $animation[3];
		$css = "";

		switch ( $animation_source ) {
			case 'animate':
				$css = file_get_contents( INTENSE_PLUGIN_FOLDER . '/assets/animations/animate/' . $type . '.' . $minified . 'css' );
				break;
			case 'magic':
				$css = file_get_contents( INTENSE_PLUGIN_FOLDER . '/assets/animations/magic/' . $type . '.' . $minified . 'css' );
				break;		
			case 'intense':
				$css = file_get_contents( INTENSE_PLUGIN_FOLDER . '/assets/animations/intense/' . $type . '.' . $minified . 'css' );
				break;
			default:
				# code...
				break;
		}	

		$GLOBALS['IntensePluginInlineStyle'][ $animation_source . '_animate_' . $type ] = $css;

		intense_add_style( 'intense.animated' );
	}
}

add_shortcode( 'intense_animated_demo', 'intense_animated_demo_shortcode' );
function intense_animated_demo_shortcode( $atts = null, $content = null ) {
    global $intense_animations;

    $output = '';

    $categories = intense_get_animation_categories();

    foreach ( $categories as $category ) {
        $category_animations = array();

        foreach ( $intense_animations as $animation => $attributes ) {
            $animation_source = $attributes[0];
            $animation_category = $attributes[1];
            $starthidden = $attributes[2];
            $endhidden = $attributes[3];

            if ( $category == $animation_category ) {
                $category_animations[] = $animation;
            }
        }

        switch ( $category ) {
        case 'Repeating':
            $trigger = 'delay';
            $triggerHint = '';
            break;
        case 'Attention Seekers':
            $trigger = 'hover';
            $triggerHint = ' <small>(' . $trigger . ' to activate)</small>';
            break;
        default:
            $trigger = 'click';
            $triggerHint = ' <small>(' . $trigger . ' to activate)</small>';
            break;
        }

        $output .= '<h2>' . $category . $triggerHint . '</h2>';
        //$output .= '<strong>' . $category . ':</strong> ';

        natsort( $category_animations );

        foreach ( $category_animations as $animation ) {
        	$button = intense_run_shortcode( 'intense_button', array( 
        		'size' => 'medium',
        		'link' => 'javascript: return false;',
        		'trigger' => $trigger,
    		), $animation );

        	$output .= intense_run_shortcode( 'intense_animated', array( 
        		'type' => $animation,  
        		'trigger' => $trigger,
    		), $button );
        }       

        $output .= intense_run_shortcode( 'intense_hr', array( 'size' => 'large' ) );
        
        //$output = rtrim($output, ', ');
        //$output .= '<br /><br />';
    }

    return $output;
}
