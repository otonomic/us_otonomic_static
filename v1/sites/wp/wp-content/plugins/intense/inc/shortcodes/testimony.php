<?php

class Intense_Testimonies extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Testimonies', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-testimonial';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'source' => array(
				'type' => 'dropdown',
				'title' => __( 'Source', 'intense' ),
				'default' => 'manual',
				'options' => array(
					'manual' => __( 'Manual', 'intense' ),
					'posts' => __( 'Intense Testimonies', 'intense' ),
				),
			),
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'options' => array(
					'slider' => __( 'Slider', 'intense' ),
					'' => __( 'Standard', 'intense' ),
				),
			),
			'template' => array(
				'type' => 'template',
				'title' => __( 'Template', 'intense' ),
				'path' => '/testimonies/',
			),
			'speed' => array(
				'type' => 'text',
				'title' => __( 'Speed', 'intense' ),
				'description' => __( 'measured in milliseconds', 'intense' ),
				'default' => '7000',
				'class' => 'slidersettings'
			),
			'pause_on_hover' => array(
				'type' => 'checkbox',
				'title' => __( 'Pause on Hover', 'intense' ),
				'default' => "1",
				'class' => 'slidersettings'
			),
			'order_by' => array(
				'type' => 'text',
				'title' => __( 'Order By', 'intense' ),
				'default' => 'id',
				'class' => 'postsettings'
			),
			'order' => array(
				'type' => 'dropdown',
				'title' => __( 'Order', 'intense' ),
				'default' => 'asc',
				'options' => array(
					'asc' => __( 'Ascending', 'intense' ),
					'desc' => __( 'Descending', 'intense' ),
				),
				'class' => 'postsettings'
			),
			'testimony' => array(
                'type' => 'repeater',
                'title' => __( 'Testimonies', 'intense' ),
                'preview_content' => __( 'Testimony Text', 'intense' ),
                'fields' => array(
                    'author' => array(
		                'type' => 'text',
		                'title' => __( 'Author', 'intense' ),
                        'skinnable' => '0'
		            ),
		            'company' => array(
		                'type' => 'text',
		                'title' => __( 'Company', 'intense' ),
                        'skinnable' => '0'
		            ),
		            'link' => array(
		                'type' => 'text',
		                'title' => __( 'Link', 'intense' ),
		                'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        'skinnable' => '0'
		            ),
		            'link_target' => array(
		                'type' => 'link_target',
		                'title' => __( 'Link Target', 'intense' ),
		                'default' => '_blank'
		            ),
		            'image' => array(
						'type' => 'image',
						'title' => __( 'Image', 'intense' ),
						'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                        'skinnable' => '0'
					),
					'background' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background', 'intense' ),
						'default' => intense_get_plugin_color( "boxed" ),
						'skinnable' => '0'
					),
					'font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Font Color', 'intense' ),
					),
                ),
				'class' => 'manualsettings'
            ),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';

		intense_add_style( 'intense_testimonial' );

		if ( $type == 'slider' ) {
			intense_add_script( 'intense.basicslider' );

			$slider = "data-basicslider='true'";
			$slider_script = "<script> jQuery(function($){";
			$slider_script .= " $(document).ready(function() {";
			$slider_script .= "    $('[data-basicslider=\"true\"]').basicslider({
                            speed: " . $speed . ",
                            pauseOnHover: " . ( $pause_on_hover ? 'true' : 'false' ) . "
                        });";
			$slider_script .= "	});";
			$slider_script .= "}); </script>";
			$extra_class = "testimonies-slider";
		} else {
			$slider = null;
			$slider_script = "";
			$extra_class = null;
		}		

        if ( !empty( $template ) ) {
	        global $intense_testimonies;
	        global $intense_testimony;

			$intense_testimonies = array();

			if ( $source == 'manual' ) {
				//gather array of testimonies
				do_shortcode( $content );
			} else {
				//get testimonies from custom post type

				global $post, $wp_query;

				$original_query = $wp_query;
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$wp_query = null;

				$args = array(
					'post_type' => 'intense_testimonials',
					'orderby' => $order_by,
					'order' => $order,
					'paged'=> $paged,
					'tax_query' => ''
				);

				$wp_query = new WP_Query( $args );

				while ( $wp_query->have_posts() ) {
					$wp_query->the_post();

					$link = get_field( 'intense_testimonial_link' );

					if ( is_numeric ( $link ) ) {
						$link = get_permalink( $link );
					}
					
					$intense_testimonies[] = array(
						'author' => get_field( 'intense_testimonial_author' ),
						'company' => get_field( 'intense_testimonial_company' ),
						'link' => $link,
						'link_target' => get_field( 'intense_testimonial_link_target' ),
						'image' => get_field( 'intense_testimonial_author_image' ),
						'background' => get_field( 'intense_testimonial_background' ),
						'font_color' => get_field( 'intense_testimonial_font_color' ),
						'content' => intense_content( 50 ),
					);
				}				

				$wp_query = null;
				$wp_query = $original_query;
				wp_reset_postdata();
			}

			$template_file = intense_locate_plugin_template( '/testimonies/' . $template . '.php' );

			if ( !isset( $template_file ) ) {
				$template_file = intense_locate_plugin_template( '/testimonies/original.php' );
			}

			$output .= "<div class='intense testimonies $extra_class " . ( $rtl ? 'rtl ' : '' ) . "' $slider>";

			foreach ($intense_testimonies as $key => $testimony) {
				$testimony['rtl'] = $rtl;

				if ( !empty( $testimony['background'] ) && in_array( $testimony['background'], array( "warning", "error", "success", "info", "inverse", "muted", "primary" ) ) ) {
		            $testimony['background'] = intense_get_plugin_color( $testimony['background'] );
		        }

		        if ( !empty( $testimony['font_color'] ) && in_array( $testimony['font_color'], array( "warning", "error", "success", "info", "inverse", "muted", "primary" ) ) ) {
		            $testimony['font_color'] = intense_get_plugin_color( $testimony['font_color'] );
		        }

				$intense_testimony = $testimony;
				$post_content = intense_load_plugin_template( $template_file );
				$output .= $post_content;
			}

			$output .= "</div>$slider_script";

			return $output;
		} else {
			//old method
			
			$testimony_style = "
	        <style type='text/css'>
	        	.testimony > .content {
	        		background-color: " . intense_get_plugin_color( "boxed" ) . " !important;
	        	}
	            .testimony > .content:after {
	           		border-right-color: " . intense_get_plugin_color( "boxed" ) . " !important;
	        	}
	        </style>";

			
			return $testimony_style . "<div class='intense testimonies $extra_class " . ( $rtl ? 'rtl ' : '' ) . "' $slider>" . do_shortcode( $content ) . "</div>$slider_script";
		}
	}

	function render_dialog_script() {
	?>
		<script>
		jQuery(function($) {
	  		$('.upload_image_button').live('click', function( event ){
              	event.preventDefault();
              	var target = $(this).data('target-id');

          		window.parent.loadImageFrame($(this), function(attachment) {
	                jQuery("#" + target).val(attachment.id);
	                
	                if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }	                

			    	if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
      			});
          	});

          	$('.slidersettings').hide();
          	$('.postsettings').hide();
          	//$('.manualsettings').hide();

          	$('#type').change(function() {
          		var value = $(this).val();

          		$('.slidersettings').hide();          		

          		switch(value) {
          			case 'slider':
          				$('.slidersettings').show();
          				break;					
          		}
          	});

          	$('#source').change(function() {
          		var value = $(this).val();

          		$('.postsettings').hide();
          		$('.manualsettings').hide();

          		switch(value) {
          			case 'manual':
          				$('.manualsettings').show();
          				break;
          			case 'posts':
          				$('.postsettings').show();
          				break;
          		}
          	});
		});
	</script>

	<?php
	}
}

class Intense_Testimony extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Testimony', 'intense' );
        $this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-testimonial';
        $this->show_preview = true;       
        $this->hidden = true;
        $this->parent = 'intense_testimonies';
        $this->vc_map_content = 'textarea';

        $this->fields = array(
        	'author' => array(
                'type' => 'text',
                'title' => __( 'Author', 'intense' ),
                'skinnable' => '0'
            ),
            'company' => array(
                'type' => 'text',
                'title' => __( 'Company', 'intense' ),
                'skinnable' => '0'
            ),
            'link' => array(
                'type' => 'text',
                'title' => __( 'Link', 'intense' ),
                'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
            ),
            'link_target' => array(
                'type' => 'link_target',
                'title' => __( 'Link Target', 'intense' ),
                'default' => '_blank'
            ),
            'image' => array(
				'type' => 'image',
				'title' => __( 'Image', 'intense' ),
				'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                'skinnable' => '0'
			),
			'background' => array(
				'type' => 'color_advanced',
				'title' => __( 'Background', 'intense' ),
				'default' => intense_get_plugin_color( "boxed" )
			),
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
			),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        // deprecated method
        if ( !isset( $author ) && !isset( $company ) ) {
       		return "<div class='testimony'>" . do_shortcode( $content ) . "</div>";
       	} else {   
			global $intense_testimonies;

			if ( !empty( $atts['link'] ) && is_numeric( $atts['link'] ) ) {
				$atts['link'] = get_permalink( $atts['link'] );
			}			

			if ( empty( $atts['link_target'] ) ) $atts['link_target']  = '_blank';

			$intense_testimonies[] = array_merge( array( 'content' => $content ), is_array( $atts ) ? $atts : array() );

			return '';
		}
    }
}

// deprecated shortcodes
add_shortcode( 'intense_testimony_author', 'intense_testimony_author_shortcode' );
add_shortcode( 'intense_testimony_text', 'intense_testimony_text_shortcode' );

function intense_testimony_text_shortcode( $atts, $content = null ) {
	return "<div class='content'>" . do_shortcode( $content ) . "</div>";
}

function intense_testimony_author_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
				"company" => '',
				"link" => '',
				'link_target' => '',
				"image" => '',
			), $atts ) );

	if ( strlen( $image ) > 0 ) {
		if ( is_numeric( $image ) ) {
			$image_url = wp_get_attachment_image_src( $image ); // returns an array			
			$image = '<img src="' . $image_url[0] . '" alt="' . esc_attr( $company ) . '" />';
		} else {
			$image = '<img src="' . $image . '" alt="' . esc_attr( $company ) . '" />';
		}
	} else {
		$image = intense_run_shortcode( 'intense_icon', array( 'type' => 'user', 'size' => '2' ) );
	}

	return "<div class='author'>$image " . do_shortcode( $content ) . ( strlen( $company ) > 0 ? "<span class='company'>, " . ( strlen( $link ) ? "<a href='$link' target='$link_target'>" : "" ) . $company . ( strlen( $link ) ? "</a></span>" : "</span>" ) : "" )  . "</div>";
}
