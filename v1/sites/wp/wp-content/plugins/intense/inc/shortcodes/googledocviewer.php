<?php

class Intense_Google_Docs_Viewer extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Google Docs Viewer', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-file-alt';
		$this->show_preview = true;
		$this->preview_content = __( "Link Text", 'intense' );

		$this->fields = array(
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'default' => 'embedded',
				'options' => array(
					'embedded' => __( 'Embedded', 'intense' ),
					'link' => __( 'Link', 'intense' ),
					'url' => __( 'URL', 'intense' ),
				)
			),
			'document' => array(
				'type' => 'file',
				'title' => __( 'Document', 'intense' ),
				'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                'skinnable' => '0'
			),
			'target' => array(
                'type' => 'link_target',
                'title' => __( 'Link Target', 'intense' ),
                'default' => '_self',
                'class' => 'linksetting'
            ),
			'width' => array(
				'type' => 'text',
				'title' => __( 'Width', 'intense' ),
				'description' => __( 'leave blank for auto', 'intense' ),
				'class' => 'embeddedsetting'
			),
			'height' => array(
				'type' => 'text',
				'title' => __( 'Height', 'intense' ),
				'description' => __( 'leave blank for auto', 'intense' ),
				'class' => 'embeddedsetting'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_numeric( $document ) ) {
			$url = wp_get_attachment_url( $document );
		} else if ( !empty( $document ) ) {
			$url = $document;
		}

		if ( empty( $width ) ) {
			$width = "100%";			
		}

		if ( empty( $height ) ) {
			$height = "800px";
		}

		if ( !empty ($url) ) {
			switch ($type) {
				case 'url':
					return 'http://docs.google.com/viewer?url=' . urlencode( $url );
					break;
				case 'link':
					return '<a href="http://docs.google.com/viewer?url=' . urlencode( $url ) . '" target="' . $target . '">' . do_shortcode( $content ) . '</a>';
					break;
				case 'embedded':
					return '<iframe src="http://docs.google.com/viewer?url=' . urlencode( $url ) . '&embedded=true" width="' . $width . '" height="' . $height . '" style="border: none;"></iframe>';
					break;
			}
		}
	}

	function render_dialog_script() {
	?>
		<script>
			jQuery(function($) {
				// Uploading files
		  		$('.upload_file_button').live('click', function( event ){
	              event.preventDefault();
	              var target = $(this).data('target-id');
	        
	              window.parent.loadFileFrame($(this), function(attachment) {
	                jQuery("#" + target).val(attachment.id);

	                if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
	              });
	          	});

	          	$('.linksetting').hide();

	  			$('#type').change(function() {
	  				var value = $(this).val();

	  				$('.linksetting').hide();
	  				$('.embeddedsetting').hide();

	  				if ( value == 'embedded' ) {
	  					$('.embeddedsetting').show();
	  				} else if ( value == 'link' ) {
	  					$('.linksetting').show();
	  				}
	  			});
			});
		</script>
	<?php
	}
}
