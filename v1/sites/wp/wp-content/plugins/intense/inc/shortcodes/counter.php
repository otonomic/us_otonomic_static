<?php

class Intense_Counter extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Counter', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-sort-numerically';
		$this->show_preview = true;

		$this->fields = array(
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'default' => ''
			),
			'background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Background Color', 'intense' ),
				'default' => ''
			),
			'border_radius' => array(
				'type' => 'border_radius',
				'title' => __( 'Border Radius', 'intense' ),
			),
			'start' => array(
				'type' => 'text',
				'title' => __( 'Start Value', 'intense' ),
				'default' => '1',
				'composer_show_value' => true
			),
			'end' => array(
				'type' => 'text',
				'title' => __( 'End Value', 'intense' ),
				'description' => __( 'For counting down, make <strong>end</strong> be lower than <strong>start</strong>.', 'intense' ),
				'default' => '100',
				'composer_show_value' => true
			),
			'text' => array(
				'type' => 'text',
				'title' => __( 'HTML Text', 'intense' ),
				'description' => __( 'HTML text should include {0} which will be substituted with value.', 'intense' ),
				'default' => '{0}',
				'composer_show_value' => true
			),
			'speed' => array(
				'type' => 'text',
				'title' => __( 'Speed', 'intense' ),
				'description' => __( 'Number of milliseconds between counter ticks.', 'intense' ),
				'default' => '45'
			),
			'thousands_separator' => array(
				'type' => 'text',
				'title' => __( 'Thousands Separator', 'intense' ),
				'default' => ','
			),
			'decimal_point' => array(
				'type' => 'text',
				'title' => __( 'Decimal Point', 'intense' ),
				'default' => '.'
			),
			'decimal_places' => array(
				'type' => 'text',
				'title' => __( 'Decimal Places', 'intense' ),
				'description' => __( 'The number of decimal places to show. If not 0, then the increment value will be relative to the number of decimal places (ex. decimal places = 2, increment = 1, counter will increment .01 each time)', 'intense' ),
				'default' => '0'
			),
			'increment' => array(
				'type' => 'text',
				'title' => __( 'Increment Amount', 'intense' ),
				'description' => __( 'Should always be a positive whole number. It also is relative to the number of decimal places.', 'intense' ),
				'default' => '1'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		intense_add_script( 'jquery.appear' );
		intense_add_script( 'accounting.js' );
		intense_add_script( 'intense.counter' );

		$font_color = intense_get_plugin_color( $font_color );

		if ( $font_color == '' ) {
			$style = "";
		} else {
			$style = "color: " . $font_color . ";";
		}

		if ( isset( $border_radius ) ) {
			$style .= ' border-radius:' . $border_radius . ';';
		}

		$background_color = intense_get_plugin_color( $background_color );

		if ( isset( $background_color ) ) {
			$style .= ' background-color:' . $background_color . ';';
		}

		return '<div class="intense counter" style="display: inline-block; min-width: 5px;' . $style . '"
			data-text="' . $text . '"
			data-start="' . $start . '"
			data-end="' . $end . '"
			data-speed="' . $speed . '"
			data-decimal-places="' . $decimal_places . '"
			data-thousands-separator="' . $thousands_separator . '"
			data-decimal-point="' . $decimal_point . '"
			data-increment="' . $increment . '"
			></div>';
	}
}
