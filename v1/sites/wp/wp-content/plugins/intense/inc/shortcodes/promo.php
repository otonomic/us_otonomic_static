<?php

class Intense_Promo_Box extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Promotional Box', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-bullhorn';
		$this->show_preview = true;
		$this->preview_content = __( "Promotional Box Content", 'intense' );
		$this->is_container = true;

		$this->fields = array(
			'promo_box_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'default' => 'medium',
						'options' => array(
							'small' => __( 'Small', 'intense' ),
							'medium' => __( 'Medium', 'intense' ),
							'large' => __( 'Large', 'intense' ),
							'mega' => __( 'Mega', 'intense' ),
						)
					),
					'color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Font Color', 'intense' ),
						'default' => '#333333'
					),
					'shadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
					),
					'background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background Color', 'intense' ),
						'default' => ''
					),
					'background_opacity' => array(
						'type' => 'text',
						'title' => __( 'Background Opacity', 'intense' ),
						'description' => __( '0 - 100: leave blank for solid color', 'intense' )
					),
					'image' => array(
						'type' => 'image',
						'title' => __( 'Background Image', 'intense' ),
						'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                        'skinnable' => '0'
					),
					'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
            		'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'imagesize' => array(
						'type' => 'image_size',
						'title' => __( 'Background Image Size', 'intense' ),
						'default' => 'medium800'
					),
					'imagemode' => array(
						'type' => 'dropdown',
						'title' => __( 'Background Mode', 'intense' ),
						'default' => 'repeat',
						'options' => array(
							'no-repeat' => __( 'No Repeat', 'intense' ),
							'repeat' => __( 'Repeat', 'intense' ),
							'repeat-x' => __( 'Repeat-X', 'intense' ),
							'repeat-y' => __( 'Repeat-Y', 'intense' ),
						)
					),
				)
			),
			'button_tab' => array(
				'type' => 'tab',
				'title' => __( 'Button', 'intense' ),
				'fields' =>  array(
					'button_text' => array(
						'type' => 'text',
						'title' => __( 'Text', 'intense' ),
                        'skinnable' => '0'
					),
					'button_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
						'default' => 'primary'
					),
					'button_link' => array(
						'type' => 'text',
						'title' => __( 'Link', 'intense' ),
						'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
						'default' => '#',
                		'skinnable' => '0'
					),
					'button_target' => array(
						'type' => 'link_target',
						'title' => __( 'Target', 'intense' ),
						'default' => '_self'
					),
					'button_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'default' => 'large',
						'options' => array(
							'mini' => __( 'Mini', 'intense' ),
							'small' => __( 'Small', 'intense' ),
							'default' => __( 'Default', 'intense' ),
							'medium' => __( 'Medium', 'intense' ),
							'large' => __( 'Large', 'intense' ),
						)
					),
					'button_position' => array(
						'type' => 'dropdown',
						'title' => __( 'Position', 'intense' ),
						'default' => 'right',
						'options' => array(
							'left' => __( 'Left', 'intense' ),
							'right' => __( 'Right', 'intense' ),
						)
					),
				)
			),
			'borders_tab' => array(
				'type' => 'tab',
				'title' => __( 'Borders', 'intense' ),
				'fields' =>  array(
					'border' => array(
						'type' => 'border',
						'title' => __( 'All Borders', 'intense' ),
					),
					'border_top' => array(
						'type' => 'border',
						'title' => __( 'Top Border', 'intense' ),
					),
					'border_right' => array(
						'type' => 'border',
						'title' => __( 'Right Border', 'intense' ),
					),
					'border_bottom' => array(
						'type' => 'border',
						'title' => __( 'Bottom Border', 'intense' ),
					),
					'border_left' => array(
						'type' => 'border',
						'title' => __( 'Left Border', 'intense' ),
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
					),
				)
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_numeric( $image ) ) {
			$imageid = $image;
		} else if ( !empty( $image ) ) {
			$imageurl = $image;
		}

		$backcolor = "";
		$background_image = "";
		$color_style = "";

		if ( isset( $button_type ) ) {
			$button_color = $button_type;
		}

		if ( !empty( $background_color ) ) {
			$backcolor = intense_get_plugin_color( $background_color );
			$background_color_style = "background-color: " . $backcolor . " !important;";
		} else {
			$background_color_style = "background-color: " . intense_get_plugin_color( "muted" ) . " !important;";
		}

		if ( isset( $background_opacity ) && strlen( $backcolor ) > 0 ) {
			$background_color_style = "background-color: " . intense_get_rgb_color( $backcolor, $background_opacity ) . " !important;";
		}

		if ( !empty( $imageid ) ) {
			$photo_info = wp_get_attachment_image_src( $imageid, $imagesize );
			$photo_url = $photo_info[0];
		} else if ( !empty( $imageurl ) ) {
			$photo_url = $imageurl;
		}

		if ( isset( $photo_url ) ) {
			if ( $imagemode == 'repeat-x' ) {
				$background_image .= " background-image: url($photo_url); background-repeat: repeat-x;";
			} else if ( $imagemode == 'repeat-y' ) {
				$background_image .= " background-image: url($photo_url); background-repeat: repeat-y;";
			} else if ( $imagemode == 'repeat' ) {
				$background_image .= " background-image: url($photo_url); background-repeat: repeat;";
			} else if ( $imagemode == 'no-repeat' ) {
				$background_image .= " background-image: url($photo_url); background-repeat: no-repeat;";
			}
		}

		if ( isset( $color ) ) {
			$color_style = "color: " . intense_get_plugin_color( $color ) . " !important;";
		} 

		$border_style = null;
		$border_top_style = null;
		$border_right_style = null;
		$border_bottom_style = null;
		$border_left_style = null;
		$radius = '';

		if ( isset( $border ) ) $border_style = "border: " . $border . ' !important;';
		if ( isset( $border_top ) ) $border_top_style = "border-top: " . $border_top . ' !important;';
		if ( isset( $border_right ) ) $border_right_style = "border-right: " . $border_right . ' !important;';
		if ( isset( $border_bottom ) ) $border_bottom_style = "border-bottom: " . $border_bottom . ' !important;';
		if ( isset( $border_left ) ) $border_left_style = "border-left: " . $border_left . ' !important;';		
		if ( isset( $border_radius ) ) $radius = 'border-radius:' . $border_radius . ';';

		$output = '';

		if ( $shadow ) {
			$shadow_style = "margin-bottom: 0;";
			$output .= "<div>";
		} else {
			$shadow_style = null;
		}

		switch ( $size ) {
		case 'small':
			$box_class = "well well-sm";
			break;
		case 'medium':
			$box_class = "well";
			break;
		case 'large':
			$box_class = "well well-lg";
			break;
		case 'mega':
			$box_class = "jumbotron";
			break;
		default:
			// code...
			$box_class = "well";
			break;
		}

		$output .= "<div class='intense $box_class' style='$shadow_style $background_color_style $color_style $border_style $border_top_style $border_right_style $border_bottom_style $border_left_style $background_image $radius'>";
		if ( isset( $button_text ) && strlen( $button_text ) > 0 ) {
			if ( is_numeric( $button_link ) ) {
				$button_link = get_permalink( $button_link );
			}

			$output .= intense_run_shortcode( 'intense_button', array(
				'color' => $button_color,
				'size' => $button_size,
				'class' => 'promo pull-' . $button_position,
				'target' => $button_target,
				'link' => $button_link
			), $button_text );
		}
		$output .= do_shortcode( $content );
		$output .= "<div class='clearfix'></div></div>";

		if ( $shadow ) {
			$output .= "<img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0;' alt='' /></div>";
		}

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
				jQuery(function($) {
					// Uploading files
			  		$('.upload_image_button').live('click', function( event ){
					    event.preventDefault();

					    window.parent.loadImageFrame($(this), function(attachment) {
					    	jQuery("#image").val(attachment.id);
					    	$('#image-thumb').attr('src', attachment.url);
					    	
					    	if ( $('#preview-content').length > 0 ) {
		                      $('#preview').click();
		                    }
					    });
					});
				});
			</script>
		<?php
	}
}
