<?php

class Intense_Hr extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Horizontal Ruler', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-resize-horizontal';
		$this->show_preview = true;

		$this->fields = array(
			'general_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'direction' => array(
						'type' => 'dropdown',
						'title' => __( 'Direction', 'intense' ),
						'default' => 'horizontal',
						'options' => array(
							'horizontal' => __( 'Horizontal', 'intense' ),
							'vertical' => __( 'Vertical', 'intense' ),
						)
					),
					//only applies to vertical
					'height' => array(
						'type' => 'text',
						'title' => __( 'Height', 'intense' ),
						'default' => "300",
						'class' => 'heightsetting'
					),
					'type' => array(
						'type' => 'dropdown',
						'title' => __( 'Type', 'intense' ),
						'default' => 'solid',
						'options' => array(
							'solid' => __( 'Solid', 'intense' ),
							'dotted' => __( 'Dotted', 'intense' ),
							'doubledotted' => __( 'Double Dotted', 'intense' ),
							'dashed' => __( 'Dashed', 'intense' ),
							'doubledashed' => __( 'Double Dashed', 'intense' ),
							'double' => __( 'Double', 'intense' ),
							'image' => __( 'Image', 'intense' ),
						)
					),
					'image' => array(
						'type' => 'image',
						'title' => __( 'Image', 'intense' ),
						'class' => 'imagesettings',
                        'skinnable' => '0'
					),
					'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
                    'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'imagemode' => array(
						'type' => 'dropdown',
						'title' => __( 'Image Mode', 'intense' ),
						'default' => 'full',
						'options' => array(
							'full' => __( 'Full', 'intense' ),
							'repeat' => __( 'Repeat', 'intense' ),
						),
						'class' => 'imagesettings'
					),
					'imagedimension' => array(
						'type' => 'text',
						'title' => __( 'Image Dimension (in px)', 'intense' ),
						'description' => __( 'used for repeat mode to indicate how wide or tall to repeat', 'intense' ),
						'class' => 'imagesettings'
					),
					'size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'options' => array(
							'' => '',
							'small' => __( 'Small', 'intense' ),
							'medium' => __( 'Medium', 'intense' ),
							'large' => __( 'Large', 'intense' ),
						)
					),
					'color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
						'default' => $intense_visions_options['intense_content_box_style']['border-color'],
						'skinnable' => '0'
					),
					'shadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
					),
				)
			),
			'title_tab' => array(
				'type' => 'tab',
				'title' => __( 'Title', 'intense' ),
				'fields' =>  array(
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
						'composer_show_value' => true,
                        'skinnable' => '0'
					),
					'title_tag' => array(
						'type' => 'text',
						'title' => __( 'Tag', 'intense' ),
						'description' => __( 'h1, h2, h3, etc... - default is h2', 'intense' ),
						'default' => "h2"
					),
					'title_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
					),
					'title_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background Color', 'intense' ),
						'default' => $intense_visions_options['intense_title_background_color'],
						'skinnable' => '0'
					),
					'title_position' => array(
						'type' => 'dropdown',
						'title' => __( 'Position', 'intense' ),
						'default' => 'left',
						'options' => array(
							'left' => __( 'Left', 'intense' ),
							'center' => __( 'Center', 'intense' ),
							'right' => __( 'Right', 'intense' ),
							'top' => __( 'Top', 'intense' ),
							'bottom' => __( 'Bottom', 'intense' ),
						)
					),
					'title_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Title Radius', 'intense' ),
					),
					'title_shadowed' => array(
						'type' => 'checkbox',
						'title' => __( 'Shadow', 'intense' ),
						'default' => "0",
					),
				)
			),
			'icon_tab' => array(
				'type' => 'tab',
				'title' => __( 'Icon', 'intense' ),
				'fields' =>  array(
					'icon_position' => array(
						'type' => 'dropdown',
						'title' => __( 'Position', 'intense' ),
						'default' => 'left',
						'options' => array(
							'left' => __( 'Left', 'intense' ),
							'right' => __( 'Right', 'intense' ),
						)
					),
					'icon_type' => array(
						'type' => 'icon',
						'title' => __( 'Icon', 'intense' ),
						'default' => '',
					),
					'icon_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'default' => '1',
						'options' => array( 
							'1' => '1', 
							'2' => '2', 
							'3' => '3', 
							'4' => '4', 
							'5' => '5' 
						)
					),
					'icon_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Color', 'intense' ),
						'default' => ''
					),
					'icon_spin' => array(
						'type' => 'checkbox',
						'title' => __( 'Spin', 'intense' ),
						'default' => "0",
					),
					'icon_rotate' => array(
						'type' => 'dropdown',
						'title' => __( 'Rotate', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'90' => __( '90 degrees', 'intense' ),
							'180' => __( '180 degrees', 'intense' ),
							'270' => __( '270 degrees', 'intense' )
						)
					),
					'icon_flip' => array(
						'type' => 'dropdown',
						'title' => __( 'Flip', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'horizontal' => __( 'Horizontal', 'intense' ),
							'vertical' => __( 'Vertical', 'intense' )
						)
					),
					'icon_stack_type' => array(
						'type' => 'icon',
						'title' => __( 'Stack Icon', 'intense' ),
						'default' => '',
					),
					'icon_stack_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Stack Color', 'intense' ),
						'default' => '#ffffff'
					),
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_numeric( $image ) ) {
			$imageid = $image;
		} else if ( !empty( $image ) ) {
			$imageurl = $image;
		}

		$color = intense_get_plugin_color( $color );

		if ( empty( $color ) ) {
			$color = $intense_visions_options['intense_content_box_style']['border-color'];
		}

		$border_color = 'border-color: ' . $color . ';';

		$title_background_color = intense_get_plugin_color( $title_background_color );

		if ( empty( $title_background_color ) ) {
			$title_background_color = $intense_visions_options['intense_title_background_color'];
		}

		$title_color = intense_get_plugin_color( $title_color );

		if ( !empty( $icon_type ) ) {
			$icon_shortcode = intense_run_shortcode( 'intense_icon', array( 
				'type' => $icon_type, 
				'size' => $icon_size, 
				'color' => $icon_color, 
				'spin' => $icon_spin, 
				'rotate' => $icon_rotate, 
				'flip' => $icon_flip,
				'stack_type' => $icon_stack_type,
				'stack_color' => $icon_stack_color ) ) ;		
		}

		if ( !empty( $imageid ) ) {
			$photo_info = wp_get_attachment_image_src( $imageid, $size );
			$photo_url = $photo_info[0];
			$w = $photo_info[1];
			$h = $photo_info[2];

			if ( empty( $imagedimension ) && $direction == "horizontal" ) {
				$imagedimension = $h;
			}

			if ( empty( $imagedimension ) && $direction == "vertical" ) {
				$imagedimension = $w;
			}
		} else if ( !empty( $imageurl ) ) {
			$photo_url = $imageurl;
		}

		$output = "<div class='intense hr $direction " .
			( $direction == 'vertical' ? 'hr-' . $type : '' ) .
			" $size' style='" .
			( $direction == 'vertical' ? 'height: ' . $height . 'px; ' . $border_color . ' ' : '' ) .
			( !empty( $photo_url ) && $direction == "vertical" ? " width: " . ( !empty( $imagedimension ) ? $imagedimension : 16 ) . "px; " : "" ) .
			"'>";

		if ( !empty( $title ) || !empty( $icon_type ) ) {
			$padding_adjustment = null;

			if ( $size != "large" && ( empty( $title_background_color ) || $title_background_color == $intense_visions_options['intense_title_background_color'] ) ) {				
				if ( $title_position == 'left' ) {
					$padding_adjustment = "padding-left: 0px;";
				} else if ( $title_position == 'right' ) {
					$padding_adjustment = "padding-right: 0px;";
				}
			}

			if ( $direction == 'vertical' ) {
				$padding_adjustment = '';
			}

			$output .= "<$title_tag class='intense hr-title hr-title-$title_position " . ( $title_shadowed ? "hr-title-shadowed" : "" ) . "' style='display: none; background-color: $title_background_color; border-radius: {$title_radius}px; color: $title_color; $padding_adjustment'>";

			intense_add_script( 'intense.hr' );
		}

		if ( !empty( $icon_type ) && $icon_position == 'left' ) {
			$output .= $icon_shortcode . " ";
		}

		$output .= $title;

		if ( !empty( $icon_type ) && $icon_position == 'right' ) {
			$output .= " " . $icon_shortcode;
		}

		if ( !empty( $title ) || !empty( $icon_type )  ) {
			$output .= "</$title_tag>";
		}

		$output .= "<span class='intense hr-inner hr-$type " . ( $shadow ? "shadow " : "" ) . "' style='" .
			( $shadow ? "$border_color border-width: 0; border:0px; border-radius: 0px; box-shadow: 0 0 0; " : "$border_color " ) .
			( !empty( $photo_url ) && $direction == "vertical" ? " width: " . ( !empty( $imagedimension ) ? $imagedimension : 16 ) . "px; " : "" ) .
			"'>" .
			( $shadow ? "<img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' />" : "" ) .
			( !empty( $photo_url ) && ( $imagemode == 'full' ) ? "<div style='text-align: center;'><img src='" . $photo_url . "' style='max-width:100%;' /></div>" : "" ) .
			( !empty( $photo_url ) && ( $imagemode == 'repeat' ) ? "<div style='text-align: center; " .
			( !empty( $photo_url ) && $direction == "horizontal" ? "height: " . ( !empty( $imagedimension ) ? $imagedimension : 16 ) . "px;" : "" ) .
			( !empty( $photo_url ) && $direction == "vertical" ? "width: " . ( !empty( $imagedimension ) ? $imagedimension : 16 ) . "px; height: " . $height . "px; " : "" ) .
			" background: url(" . $photo_url . ") repeat;'></div>" : "" ) .
			"</span>";
		$output .= "</div>";

		return $output;
	}

	function render_dialog_script() {
?>
    <script>
      jQuery(function($) {
        $('.upload_image_button').live('click', function( event ){
          event.preventDefault();
          var target = $(this).data('target-id');

          window.parent.loadImageFrame($(this), function(attachment) {
            jQuery("#" + target).val(attachment.id);
            
            if ( attachment.sizes.thumbnail ) {
              jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
            } else {
              jQuery("#" + target + "-thumb").attr("src", attachment.url);
            }

            if ( $('#preview-content').length > 0 ) {
              $('#preview').click();
            }
          });
        });

        $('.heightsetting').hide();

        $('#direction').change(function() {
			$('.heightsetting').hide();

			if ($(this).val() == 'vertical') {
				$('.heightsetting').show();
			}
		});

		$('.imagesettings').hide();

		$('#type').change(function() {				
			if ($(this).val() == 'image') {
				$('.imagesettings').show();
			} else {
				$('.imagesettings').hide();
			}
		});

      });
    </script>
    <?php
	}
}
