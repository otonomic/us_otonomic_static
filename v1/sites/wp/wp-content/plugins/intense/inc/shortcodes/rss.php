<?php

class Intense_Rss extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'RSS', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-rss';
		$this->show_preview = true;

		$this->fields = array(
			'url' => array(
				'type' => 'text',
				'title' => __( 'URL', 'intense' ),
                'skinnable' => '0'
			),
			'max' => array(
				'type' => 'text',
				'title' => __( 'Max Items', 'intense' ),
				'default' => '5',								
			),
			'no_items' => array(
				'type' => 'text',
				'title' => __( 'No Items Text', 'intense' ),
				'default' => __( 'No Items', 'intense' ),
				'skinnable' => '0'							
			),
			'template' => array(
				'type' => 'template',
				'title' => __( 'Template', 'intense' ),
				'description' => __( 'the template files should be located in the intense_templates/rss/ folder in your theme or child theme.', 'intense' ),
				'path' => '/rss/',
				'default' => 'title'
			),	
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options, $rss, $rss_item;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';

		$rss = fetch_feed( $url );

		if ( ! is_wp_error( $rss ) ) {
			$maxitems = $rss->get_item_quantity( $max ); 
			$rss_items = $rss->get_items( 0, $maxitems );
		}

		$output .= '<ul class="intense rss">';

		if ( $maxitems == 0 ) {
			$output .= '<li>' . $no_items . '</li>';
		} else {
			$template_file = intense_locate_plugin_template( '/rss/' . $template . '.php' );

			if ( !isset( $template_file ) ) {
				$template_file = intense_locate_plugin_template( '/rss/title.php' );
			}

			foreach ( $rss_items as $item ) {
				$rss_item = $item;	

				$output .= '<li>';
				$output .= intense_load_plugin_template( $template_file );
				$output .= '</li>';
			}
		}

		$output .= '</ul>';

		return $output;
	}
}
