<?php

class Intense_Subpages extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$post = get_post();

		$this->title = __( 'Sub Pages', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-admin-page';
		$this->show_preview = true;

		$this->fields = array(
			'id' => array(
                'type' => 'text',
                'title' => __( 'ID', 'intense' ),
                'description' => __( 'the client-side id (optional)', 'intense' ),
                'default' => '',
                'skinnable' => '0'
            ),
			'title' => array(
				'type' => 'text',
				'title' => __( 'Title', 'intense' ),
				'default' => __( 'Pages' ),
				'skinnable' => '0'
			),
			'parent' => array(
				'type' => 'text',
				'title' => __( 'Parent', 'intense' ),
				'description' => __( 'if left blank, the current page/post will be used', 'intense' ),
				'default' => ( isset( $post ) ? $post->ID : null ),
				'skinnable' => '0'
			),
			'depth' => array(
				'type' => 'text',
				'title' => __( 'Max Depth', 'intense' ),
				'description' => __( 'depth of children pages', 'intense' ),
				'default' => '1'
			),
			'template' => array(
				'type' => 'template',
				'title' => __( 'Template', 'intense' ),
				'description' => __( 'the template files should be located in the intense_templates/subpages/ folder in your theme or child theme.', 'intense' ),
				'path' => '/subpages/',
				'default' => 'link'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $post;

		$walker = new Intense_List_Pages_Walker();

		if ( empty( $parent ) && isset( $post ) ) {
			$parent = $post->ID;
		}

		$args = array(
			'authors'      => '',
			'child_of'     => !empty( $parent ) ? $parent : 0,
			'date_format'  => get_option( 'date_format' ),
			'depth'        => !empty( $depth ) ? $depth : 0,
			'echo'         => 0,
			'exclude'      => '',
			'include'      => '',
			'link_after'   => '',
			'link_before'  => '',
			'post_type'    => 'page',
			'post_status'  => 'publish',
			'show_date'    => '',
			'sort_column'  => 'menu_order, post_title',
			'title_li'     => $title,
			'walker' => $walker,
			'template' => $template,
			'template_path' => '/subpages/'
		);

		$pages = wp_list_pages( $args );

		if ( empty( $id ) ) $id = "subpages_" . rand();

		return '<ul id="' . $id . '" class="intense subpages">' . $pages . '</ul>';
	}
}
