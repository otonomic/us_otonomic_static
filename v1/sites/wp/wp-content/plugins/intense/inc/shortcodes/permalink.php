<?php

class Intense_Permalink extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Permalink', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-link';
		$this->show_preview = true;
		$this->preview_content = __( "Link", 'intense' );

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
				'description' => __( 'post or page ID', 'intense' ),
                'skinnable' => '0'
			),
			'target' => array(
                'type' => 'link_target',
                'title' => __( 'Link Target', 'intense' ),
                'default' => '_self',
            ),
            'class' => array(
				'type' => 'text',
				'title' => __( 'CSS Class', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( !empty( $class ) ) {
			$class = ' class="' . $class . '"';
		}

		return '<a href="' . get_permalink( $id ) . '" target="' . $target . '"' . $class . '>' . do_shortcode( $content ) . '</a>';
	}
}
