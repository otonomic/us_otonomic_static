<?php

class Intense_Parallax_Scene extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Parallax Scene', 'intense' );
    $this->category = __( 'Layout', 'intense' );
    $this->icon = 'dashicons-intense-rectangleselection';
    $this->show_preview = true; 
    $this->preview_content = __( "", 'intense' );
    $this->is_container = true;

    $this->fields = array(
      'general_tab' => array(
        'type' => 'tab',
        'title' => __( 'General', 'intense' ),
        'fields' =>  array(
          'id' => array(
            'type' => 'text',
            'title' => __( 'ID', 'intense' ),
            'description' => __( 'optional', 'intense' ),
            'skinnable' => '0'
          ),
          'size' => array(
            'type' => 'dropdown',
            'title' => __( 'Size', 'intense' ),
            'default' => 'fullboxed', //full, partial, fullboxed (this will be used when parallax is called from image shortcode)
            'options' => array(
              'fullboxed' => __( '100% Full Width with Boxed Content', 'intense' ),
              'full' => __( '100% Full Width', 'intense' ),
              'partial' => __( 'Partial Width with Boxed Content', 'intense' ),
            )
          ),
          'background_type' => array(
            'type' => 'dropdown',
            'title' => __( 'Background Type', 'intense' ),
            'options' => array(
              '' => __( 'None', 'intense' ),
              'color' => __( 'Color', 'intense' ),
              'image' => __( 'Image', 'intense' )
            )
          ),
          'image' => array(
            'type' => 'image',
            'title' => __( 'Image', 'intense' ),
            'class' => 'imagesettings',
            'skinnable' => '0'
          ),
          'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
          'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
          'imagesize' => array(
            'type' => 'image_size',
            'title' => __( 'Image Size', 'intense' ),
            'default' => 'large1600',
            'class' => 'imagesettings',
          ),
          'imagemode' => array(
            'type' => 'dropdown',
            'title' => __( 'Background Mode', 'intense' ),
            'default' => 'full',
            'options' => array(
              'full' => __( 'Full', 'intense' ),
              'parallax' => __( 'Parallax', 'intense' ),
              'fixed' => __( 'Parallax - Fixed', 'intense' ),
              'repeat' => __( 'Repeat', 'intense' ),
              'repeat-x' => __( 'Repeat-X', 'intense' ),
              'repeat-y' => __( 'Repeat-Y', 'intense' ),
            ),
            'class' => 'imagesettings',
          ),
          'speed' => array(
            'type' => 'text',
            'title' => __( 'Parallax Speed', 'intense' ),
            'description' => __( 'Parallax mode only - default 2', 'intense' ),
            'default' => '2',
            'class' => 'parallaxsettings'
          ),
          'background_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Background Color', 'intense' ),
            'class' => 'colorsettings'
          ),
          'box_class' => array(
            'type' => 'text',
            'title' => __( 'Boxed Content Class', 'intense' ),
            'description' => __( 'optional css class name to apply to the boxed content within the content section', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_box_class'],
            'skinnable' => '0'
          ),
          'height' => array(
            'type' => 'text',
            'title' => __( 'Height', 'intense' ),
            'description' => __( 'optional - manually sets the height of the content section', 'intense' ),
          ),
          'full_height' => array(
            'type' => 'checkbox',
            'title' => __( 'Full Height', 'intense' ),
            'description' => __( 'automatically sized to the window height', 'intense' ),
            'default' => "0",
          ),
          'height_adjustment' => array(
            'type' => 'text',
            'title' => __( 'Height Adjustment', 'intense' ),
            'description' => __( 'subtracted from computed full height', 'intense' ),
          ),
          'breakout' => array(
            'type' => 'checkbox',
            'title' => __( 'Break Out', 'intense' ),
            'description' => __( 'tries to break out of its parent container to force full width. May be needed if theme doesn\'t support full width pages/posts', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_breakout'],
            'skinnable' => '0'
          ),
        )
      ),
      'layout_tab' => array(
        'type' => 'tab',
        'title' => __( 'Layout', 'intense' ),
        'fields' =>  array(
          'margin_top' => array(
            'type' => 'text',
            'title' => __( 'Margin Top', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_margin']['margin-top'],
            'skinnable' => '0'
          ),
          'margin_bottom' => array(
            'type' => 'text',
            'title' => __( 'Margin Bottom', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_margin']['margin-bottom'],
            'skinnable' => '0'
          ),
          'margin_left' => array(
            'type' => 'text',
            'title' => __( 'Margin Left', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_margin']['margin-left'],
            'skinnable' => '0'
          ),
          'margin_right' => array(
            'type' => 'text',
            'title' => __( 'Margin Right', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_margin']['margin-right'],
            'skinnable' => '0'
          ),
          'padding_top' => array(
            'type' => 'text',
            'title' => __( 'Padding Top', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_padding']['padding-top'],
            'skinnable' => '0'
          ),
          'padding_bottom' => array(
            'type' => 'text',
            'title' => __( 'Padding Bottom', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_padding']['padding-bottom'],
            'skinnable' => '0'
          ),
          'padding_left' => array(
            'type' => 'text',
            'title' => __( 'Padding Left', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_padding']['padding-left'],
            'skinnable' => '0'
          ),
          'padding_right' => array(
            'type' => 'text',
            'title' => __( 'Padding Right', 'intense' ),
            'default' => $intense_visions_options['intense_content_section_default_padding']['padding-right'],
            'skinnable' => '0'
          ),
        )
      ),
      'overlay_tab' => array(
        'type' => 'tab',
        'title' => __( 'Overlay', 'intense' ),
        'fields' =>  array(
          'overlay_image' => array(
            'type' => 'image',
            'title' => __( 'Image', 'intense' ),
            'description' => __( 'Use a semi-transparent image to overlay the background image.', 'intense' ),
            'skinnable' => '0'
          ),
          'overlay_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Color', 'intense' ),
            'description' => __( 'This color will overlay the background image. Make sure to set an opacity.', 'intense' )
          ),
          'overlay_opacity' => array(
            'type' => 'text',
            'title' => __( 'Opacity', 'intense' ),
            'description' => __( '0 - 100: leave blank for solid color (used with Color).', 'intense' )
          ),
          'overlay_gradient' => array(
            'type' => 'dropdown',
            'title' => __( 'Gradient Type', 'intense' ),
            'default' => '',
            'options' => array(
              '' => __( 'None', 'intense' ),
              'linear' => __( 'Linear', 'intense' ),
              'radial' => __( 'Radial', 'intense' )
            ),
            'description' => __( 'This option will set an overlay that has a gradient effect. For best results, set all gradient options below.', 'intense' )
          ),
          'overlay_gradient_start_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Gradient Start Color', 'intense' ),
            'description' => __( 'This color will overlay the background image. Make sure to set the gradient start opacity.', 'intense' )
          ),
          'overlay_gradient_start_opacity' => array(
            'type' => 'text',
            'title' => __( 'Gradient Start Opacity', 'intense' ),
            'description' => __( '0 - 100: leave blank for solid color (used with Gradient Start Color).', 'intense' )
          ),
          'overlay_gradient_end_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Gradient End Color', 'intense' ),
            'description' => __( 'This color will overlay the background image. Make sure to set the gradient end opacity.', 'intense' )
          ),
          'overlay_gradient_end_opacity' => array(
            'type' => 'text',
            'title' => __( 'Gradient End Opacity', 'intense' ),
            'description' => __( '0 - 100: leave blank for solid color (used with Gradient End Color).', 'intense' )
          ),
        )
      ),
      'borders_tab' => array(
        'type' => 'tab',
        'title' => __( 'Borders', 'intense' ),
        'fields' =>  array(
          'border' => array(
            'type' => 'border',
            'title' => __( 'All Borders', 'intense' ),
          ),
          'border_top' => array(
            'type' => 'border',
            'title' => __( 'Top Border', 'intense' ),
          ),
          'border_right' => array(
            'type' => 'border',
            'title' => __( 'Right Border', 'intense' ),
          ),
          'border_bottom' => array(
            'type' => 'border',
            'title' => __( 'Bottom Border', 'intense' ),
          ),
          'border_left' => array(
            'type' => 'border',
            'title' => __( 'Left Border', 'intense' ),
          ),
          'border_radius' => array(
            'type' => 'border_radius',
            'title' => __( 'Border Radius', 'intense' ),
          ),
        )
      ),
      'advance_tab' => array(
        'type' => 'tab',
        'title' => __( 'Advance Button', 'intense' ),
        'fields' =>  array(
          'show_advance' => array(
            'type' => 'checkbox',
            'title' => __( 'Enable', 'intense' ),
            'description' => __( 'when enabled, a button will show that allows users to advance to another part of the page', 'intense' ),
            'default' => "0",
          ),
          'advance_target_id' => array(
            'type' => 'text',
            'title' => __( 'Target ID', 'intense' ),
            'description' => __( 'leave blank to advance to just below the content section', 'intense' )
          ),
          'advance_size' => array(
            'type' => 'dropdown',
            'title' => __( 'Size', 'intense' ),
            'default' => 'large',
            'options' => array(
              'default' => __( 'default', 'intense' ),
              'mini' => __( 'mini', 'intense' ),
              'small' => __( 'small', 'intense' ),
              'medium' => __( 'medium', 'intense' ),
              'large' => __( 'large', 'intense' )
            )
          ),
          'advance_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Color', 'intense' ),
            'default' => 'primary',
          ),
          'advance_icon' => array(
            'type' => 'icon',
            'title' => __( 'Icon', 'intense' ),
            'default' => 'chevron-down',
            'class' => 'select2'
          ),
          'advance_icon_position' => array(
            'type' => 'dropdown',
            'title' => __( 'Icon Position', 'intense' ),
            'default' => 'left',
            'options' => array(
              'left' => __( 'Left', 'intense' ),
              'right' => __( 'Right', 'intense' ),
            )
          ),
          'advance_text' => array(
            'type' => 'text',
            'title' => __( 'Text', 'intense' ),            
          ),          
        )
      ),
      'advance_arrow_tab' => array(
        'type' => 'tab',
        'title' => __( 'Advance Arrow', 'intense' ),
        'fields' =>  array(
          'show_advance_arrow' => array(
            'type' => 'checkbox',
            'title' => __( 'Enable', 'intense' ),
            'description' => __( 'when enabled, an arrow will show that encourages the users to advance to the next part of the page', 'intense' ),
            'default' => "0",
          ),
          'advance_arrow_background_color' => array(
            'type' => 'color_advanced',
            'title' => __( 'Background Color', 'intense' ),
            'description' => __( 'will usually be set to match the next section to create a seemless look', 'intense' ),
            'default' => '#fff',
          ),
          'advance_arrow_position' => array(
            'type' => 'text',
            'title' => __( 'Position', 'intense' ),
            'default' => '50',
            'description' => __( 'percent offset from the left (ex. 50 is in the center of the page)', 'intense' ),         
          ),
        )
      ),
      'layers_tab' => array(
        'type' => 'tab',
        'title' => __( 'Layers', 'intense' ),
        'fields' =>  array(
            'parallax_scene_layer' => array(
                'type' => 'repeater',
                'title' => __( 'Layers', 'intense' ),
                'fields' => array(
                      'layer_depth' => array(
                      'type' => 'text',
                      'title' => __( 'Depth', 'intense' ),
                      'description' => __( 'A number between 0.00 and 1.00 (depth of 0.00 will remain stationary wile a depth 1.00 will cause the layer to move by the total effect of the calculated motion', 'intense' ),
                    ),
                      'layer_opacity' => array(
                      'type' => 'text',
                      'title' => __( 'Opacity', 'intense' ),
                      'description' => __( '0 - 100: leave blank for solid.', 'intense' )
                    ),
                      'layer_class' => array(
                      'type' => 'text',
                      'title' => __( 'CSS Class', 'intense' ),
                      'description' => __( 'optional', 'intense' ),
                    ),
                )
            ),
        )
      ),
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    if ( isset( $id ) ) {
      $content_section_id = $id;
    } else {
      $content_section_id = "parallax-section-" . rand();
    }

    $border_style = null;
    $border_top_style = null;
    $border_right_style = null;
    $border_bottom_style = null;
    $border_left_style = null;

    if ( isset( $border ) ) $border_style = "border: " . $border . ' !important;';
    if ( isset( $border_top ) ) $border_top_style = "border-top: " . $border_top . ' !important;';
    if ( isset( $border_right ) ) $border_right_style = "border-right: " . $border_right . ' !important;';
    if ( isset( $border_bottom ) ) $border_bottom_style = "border-bottom: " . $border_bottom . ' !important;';
    if ( isset( $border_left ) ) $border_left_style = "border-left: " . $border_left . ' !important;';    

    //handle case where border_top and border_bottom were only used to set width
    if ( empty( $border_style ) && empty( $border_top_style ) && empty( $border_right_style ) && empty( $border_bottom_style ) && empty( $border_left_style ) && ( !empty( $border_top ) || !empty( $border_bottom ) ) ) {
      if ( is_numeric( $border_top ) && $border_top != 0 ) $border_top .= "px";
      if ( is_numeric( $border_bottom ) && $border_bottom != 0 ) $border_bottom .= "px";

      if ( empty( $border_top ) ) $border_top = '0';
      if ( empty( $border_bottom ) ) $border_bottom = '0';

      $border_color = $intense_visions_options['intense_content_box_style']['border-color'];
      $border_style = $intense_visions_options['intense_content_box_style']['border-style'];
      $border = 'border-style:' . $border_style . '; border-top-color:' . $border_color . '; border-bottom-color:' . $border_color . '; border-top-width: ' . $border_top . '; border-bottom-width: ' . $border_bottom  . ';';
    } else {
      $border = $border_style . $border_top_style . $border_right_style . $border_bottom_style . $border_left_style;
    }

    if ( !empty ( $border_radius ) ) {
      $border .= 'border-radius:' . $border_radius . ';';
    }

    if ( $show_advance_arrow && is_numeric( $padding_bottom ) ) {
      $padding_bottom += 30; //height of the arrow
    }

    if ( is_numeric( $margin_top ) && $margin_top != 0 ) $margin_top .= "px";
    if ( is_numeric( $margin_bottom ) && $margin_bottom != 0 ) $margin_bottom .= "px";
    if ( is_numeric( $margin_left ) && $margin_left != 0 ) $margin_left .= "px";
    if ( is_numeric( $margin_right ) && $margin_right != 0 ) $margin_right .= "px";

    if ( is_numeric( $padding_top ) && $padding_top != 0 ) $padding_top .= "px";
    if ( is_numeric( $padding_bottom ) && $padding_bottom != 0 ) $padding_bottom .= "px";
    if ( is_numeric( $padding_left ) && $padding_left != 0 ) $padding_left .= "px";
    if ( is_numeric( $padding_right ) && $padding_right != 0 ) $padding_right .= "px";

    intense_add_script( 'intense.contentsection' );
    intense_add_script( 'jquery.parallax' );

    $background_color = intense_get_plugin_color( $background_color );

    if ( isset( $background_color ) ) $background_color = 'background-color: ' . $background_color . ';';

    if ( is_numeric( $image ) ) {
      $imageid = $image;
    } else if ( !empty( $image ) ) {
      $imageurl = $image;
    }

    if ( !empty( $imageid ) ) {
      $photo_info = wp_get_attachment_image_src( $imageid, $imagesize );
      $photo_url = $photo_info[0];
    } else if ( !empty( $imageurl ) ) {
      $photo_url = $imageurl;
    }

    if ( is_numeric( $overlay_image ) ) {
      $overlay_info = wp_get_attachment_image_src( $overlay_image, "full" );
      $overlay_image = "url(" . $overlay_info[0] . ")";
    } else if ( !empty( $overlay_image ) ) {
      $overlay_image = "url(" . $overlay_image . ")";
    } else {
      $overlay_image = '';
      $overlay_bg_image = '';
    }

    if ( $overlay_gradient != '' ) {
      if ( isset( $overlay_gradient_start_color ) ) {
        $overlaystartcolor = intense_get_plugin_color( $overlay_gradient_start_color );
      } else {
        $overlaystartcolor = '#000000';
      }

      if ( isset( $overlay_gradient_end_color ) ) {
        $overlayendcolor = intense_get_plugin_color( $overlay_gradient_end_color );
      } else {
        $overlayendcolor = '#000000';
      }

      if ( !isset( $overlay_gradient_start_opacity ) && !is_numeric( $overlay_gradient_start_opacity ) ) {
        $overlay_gradient_start_opacity = '0';
      }

      if ( !isset( $overlay_gradient_end_opacity ) && !is_numeric( $overlay_gradient_end_opacity ) ) {
        $overlay_gradient_end_opacity = '80';
      }

      $overlay_start_percent = '';
      $overlay_end_percent = '';

      if ( $overlay_gradient == 'radial' ) {
        $overlay_location = 'ellipse';
        $overlay_start_percent = ' 20%';
        $overlay_end_percent = ' 80%';
      } else {
        $overlay_location = 'top';
        $overlay_start_percent = ' 20%';
        $overlay_end_percent = ' 0%';
      }

      if ( $overlay_image != '' ) {
        $overlay_bg_image = 'background-image: ' . $overlay_image . ', -webkit-gradient(' . $overlay_gradient . ', left top, left bottom, from(' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . '), to(' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . '));';
        $overlay_bg_image .= 'background-image: ' . $overlay_image . ', -webkit-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: ' . $overlay_image . ', -moz-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: ' . $overlay_image . ', -ms-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: ' . $overlay_image . ', -o-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: ' . $overlay_image . ', ' . $overlay_gradient . '-gradient(to bottom, ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . ');';

      } else {
        $overlay_bg_image = 'background-image: -webkit-gradient(' . $overlay_gradient . ', left top, left bottom, from(' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . '), to(' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . '));';
        $overlay_bg_image .= 'background-image: -webkit-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: -moz-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: -ms-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: -o-' . $overlay_gradient . '-gradient(' . $overlay_location . ', ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . $overlay_start_percent . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . $overlay_end_percent . ');';
        $overlay_bg_image .= 'background-image: ' . $overlay_gradient . '-gradient(to bottom, ' . intense_get_rgb_color( $overlaystartcolor, $overlay_gradient_start_opacity ) . ', ' . intense_get_rgb_color( $overlayendcolor, $overlay_gradient_end_opacity ) . ');';
      }
    } else {
      if ( $overlay_image != '' ) {
        $overlay_bg_image = 'background-image: ' . $overlay_image . ';';
      }
    }

    if ( isset( $overlay_color ) ) {
        $overlaycolor = intense_get_plugin_color( $overlay_color );
    }

    if ( isset( $overlay_opacity ) && strlen( $overlaycolor ) > 0 ) {
      $overlay_style = "background-color: " . intense_get_rgb_color( $overlaycolor, $overlay_opacity ) . ";";
    } else {
      $overlay_style = '';
    }

    $background_image = "";
    $parallax_image = "";
    $output = "";
    $advance = "";
    $advance_target = '';
    $extra_class = "";
    $advance_arrow = '';

    if ( isset( $photo_url ) ) {
      if ( $imagemode == 'parallax' ) {
        intense_add_script( "skrollr" );
        $start_location = -160 * ( 1 / $speed );
        $end_location = 80 * ( 1 / $speed );
        $extra_class = "parallax";
        $parallax_image = " data-anchor-target='#" . $content_section_id . "' data-bottom-top='background-position: 0px " . $end_location . "px;' data--1000-top='background-position: 0px " . $start_location . "px;'";
        $background_image .= " background-image: url($photo_url); background-size: cover; background-position: 50% 50%; background-attachment: fixed; ";
        $speed = ' data-speed="' . $speed . '"';
      } else if ( $imagemode == 'repeat-x' ) {
          $background_image .= " background-image: url($photo_url); background-repeat: repeat-x; background-size: auto auto; ";
        } else if ( $imagemode == 'repeat-y' ) {
          $background_image .= " background-image: url($photo_url); background-repeat: repeat-y; background-size: auto auto; ";
        } else if ( $imagemode == 'repeat' ) {
          $background_image .= " background-image: url($photo_url); background-repeat: repeat; background-size: auto auto; ";
        } else if ( $imagemode == 'full' ) {
          $background_image .= " background-image: url($photo_url); background-size: cover; background-position: 50% 50%; ";
        } else if ( $imagemode == 'fixed' ) {
          $extra_class = 'fixed';
          $background_image .= " background-image: url($photo_url); background-size: cover; background-position: 50% 50%; background-attachment: fixed; ";
        }
    }

    if ( $size == 'partial' ) {
      $extra_class .= ' partial';
      $output .= '<div class="intense container' . ( isset( $box_class ) ? " $box_class" : '' ) . '">';
    } else {
      if ( $breakout ) {
        $extra_class .= ' breakout';
      }

      if ( $full_height ) {
        $extra_class .= ' full-height';
      }
    }

    if ( $show_advance ) {
      if ( !isset( $advance_target_id ) ) {
        $advance_target_id = "content-section-advance-" . rand();
        $advance_target = '<div id="' . $advance_target_id . '" style="height: 0px;"></div>';
      }

      $advance_color= intense_get_plugin_color( $advance_color );

      $advance = '<div class="advance-button">' .
        intense_run_shortcode( 'intense_button', array( 'size' => $advance_size, 'color' => $advance_color, 'link' => '#' . $advance_target_id , 'icon' => $advance_icon, 'icon_position' => $advance_icon_position ), $advance_text ) .         
        '</div>';
    }

    if ( $show_advance_arrow ) {
      $advance_arrow_background_color= intense_get_plugin_color( $advance_arrow_background_color );

      $advance_position_left = '';
      $advance_position_right = '';

      if ( $advance_arrow_position != '50' ) {
        $advance_position_left = 'width: ' . $advance_arrow_position . '.25%';
        $advance_position_right = 'width: ' . ( 100 - $advance_arrow_position ) . '.25%';
      }

      $advance_arrow = '<div class="advance-arrow">
        <div class="advance-arrow-left" style="' . ( !empty( $advance_arrow_color ) ?  'border-bottom-color: ' . $advance_arrow_color . ';' : '' ) . $advance_position_left . '"></div>
        <div class="advance-arrow-right" style="' . ( !empty( $advance_arrow_color ) ?  'border-bottom-color: ' . $advance_arrow_color . ';' : '' ) . $advance_position_right . '"></div>
      </div>';
    }

    if ( isset( $height_adjustment ) ) {
      $height_adjustment = ' data-height-adjustment="' . $height_adjustment . '"';
    }

    $output .= '<section id="' . $content_section_id . '" class="intense content-section ' . $extra_class . '"' . $parallax_image . $height_adjustment . $speed . ' style="' . ( isset( $height ) ? 'height:' . $height . 'px; ' : '' ) . ' ' . $background_color . $border . ' margin-bottom: ' . $margin_bottom . '; margin-top: ' . $margin_top . '; margin-left: ' . $margin_left . '; margin-right: ' . $margin_right . '; ' . $background_image . ' padding-top: ' . $padding_top . '; padding-bottom:' . $padding_bottom . '; padding-left: ' . $padding_left . '; padding-right:' . $padding_right . '; overflow: hidden;">';

    if ( $size == 'fullboxed' ) {
      $output .= '<div class="intense container ' . ( isset( $box_class ) ? " $box_class" : '' ) . '">';
    }

    $parallax_scene = "parallax-scene-" . rand();

    if ( isset( $layer_opacity ) ) {

    }

    $output .= "<ul id='" . $parallax_scene . "' class='" . $parallax_scene . "'>";
    $output .= do_shortcode( $content );
    $output .= '</ul>';
    $output .= "<script>
                    jQuery(function($) {
                        $(document).ready(function() {
                            $('#" . $parallax_scene . "').parallax();
                        });
                    });
                </script>";

    if ( $size == 'fullboxed' ) {
      $output .= '</div>';
    }

    $output .= '<div style="position: absolute; ' . $overlay_bg_image . $overlay_style . ' top: 0;left: 0;z-index: -1;right: 0;bottom: 0;"></div>';
    $output .= $advance;
    $output .= $advance_arrow;
    $output .= '</section>';


    if ( $size == 'partial' ) {
      $output .= '</div>';
    }

    $output .= $advance_target;

    return do_shortcode( $output );
  }

  function render_dialog_script() {
?>
    <script>
      jQuery(function($) {
        $('.upload_image_button').live('click', function( event ){
          event.preventDefault();
          var target = $(this).data('target-id');
    
          window.parent.loadImageFrame($(this), function(attachment) {
            jQuery("#" + target).val(attachment.id);
            
            if ( attachment.sizes.thumbnail ) {
                jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
            } else {
                jQuery("#" + target + "-thumb").attr("src", attachment.url);
            }

            if ( $('#preview-content').length > 0 ) {
                $('#preview').click();
            }
          });
        });

        $('.upload_video_button').live('click', function( event ){
          event.preventDefault();
          var target = $(this).data('target-id');
    
          window.parent.loadVideoFrame($(this), function(attachment) {
            jQuery("#" + target).val(attachment.id);
            // jQuery("#imagethumb").attr("src", attachment.sizes.thumbnail.url);
            
            if ( $('#preview-content').length > 0 ) {
                $('#preview').click();
            }
          });
        }); 

        $(".colorsettings").hide();  
        $(".imagesettings").hide();
        $(".parallaxsettings").hide();

        $("#background_type").change(function() {
            var type = $(this).val();   
            $(".colorsettings").hide();
            $(".imagesettings").hide(); 
            switch(type) {
              case "color":
                $(".colorsettings").show();
                break;
              case "image":
                $(".imagesettings").show();
                break;
              default:
                $(".colorsettings").hide();
                $(".imagesettings").hide();
                break;
            }
        });

        $("#imagemode").change(function() {
          var type = $(this).val();

          $(".parallaxsettings").hide();

          switch(type) {
            case "parallax":
                $(".parallaxsettings").show();
                break;
            default:
                $(".parallaxsettings").hide();
                break;
          }
        });

      });
    </script>
    <?php
  }
}

class Intense_Parallax_Scene_Layer extends Intense_Shortcode {
    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Parallax Scene Layer', 'intense' );
        $this->category = __( 'Layout', 'intense' );
        $this->icon = 'dashicons-intense-rectangleselection';
        $this->show_preview = true;
        $this->hidden = true;
        $this->parent = 'intense_parallax_scene';
        $this->is_container = true;

        $this->fields = array(
              'layer_depth' => array(
              'type' => 'text',
              'title' => __( 'Depth', 'intense' ),
              'description' => __( 'A number between 0.00 and 1.00 (depth of 0.00 will remain stationary wile a depth 1.00 will cause the layer to move by the total effect of the calculated motion', 'intense' ),
            ),
              'layer_opacity' => array(
              'type' => 'text',
              'title' => __( 'Opacity', 'intense' ),
              'description' => __( '0 - 100: leave blank for solid.', 'intense' )
            ),
              'layer_class' => array(
              'type' => 'text',
              'title' => __( 'CSS Class', 'intense' ),
              'description' => __( 'optional', 'intense' ),
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $layerclass = '';
        $layerstyle = '';

        if ( isset( $layer_class ) && $layer_class != '' ) {
            $layerclass = $layer_class;
        }

        if ( isset( $layer_opacity ) && $layer_opacity != '' ) {
            $layerstyle = 'style="opacity:' . $layer_opacity / 100 . ';"';
        }

        $output = '<li class="layer ' . $layerclass . '" data-depth="' . $layer_depth . '"' . $layerstyle . '>' . do_shortcode( $content ) . '</li>';

        return $output;
    }
}
