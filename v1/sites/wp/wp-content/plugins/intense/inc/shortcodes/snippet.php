<?php

class Intense_Snippet extends Intense_Shortcode {

    function __construct() {
        $this->title = __( 'Snippet', 'intense' );
        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-intense-scissors';
        $this->show_preview = false;
        $this->is_container = false;

        $this->fields = array(
            'snippet_id' => array(
                'type' => 'snippet',
                'title' => __( 'Snippet', 'intense' ),
                'description' => __( 'Select the snippet you want to include.', 'intense' ),
                'skinnable' => '0'
            ),            
            'snippet_title' => array(
                'type' => 'hidden',
                'title' => __( 'Snippet Title', 'intense' ),
                'description' => __( 'The title is not required. It is prefilled and will help to distinguish between other snippet shortcodes.', 'intense' ),
                'default' => ''
            )
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        if ( is_numeric( $snippet_id ) ) {
            $snippet = get_post_field( 'post_content', $snippet_id );        
        } else {
            //id will look like: location | relative path/template name
            $parts = explode( " | ", $snippet_id );
            $snippet_location_name = $parts[0];            
            $location_path = intense_get_snippet_location_path( $snippet_location_name );

            // unset the first to remove the location and then put back together so that the name can have a | in it
            unset( $parts[0] ); 
            $snippet_file = implode( " | ", $parts ) . ".php";

            // load snippet from the file
            $snippet = intense_load_plugin_snippet( $location_path . $snippet_file );
        }

        return do_shortcode( $snippet );
    }

    public function map_visual_composer() {
        // $this->set_default_options();
        
        // return parent::map_visual_composer();
        return null;
    }
}