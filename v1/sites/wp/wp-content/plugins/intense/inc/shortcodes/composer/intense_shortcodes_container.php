<?php

class IntenseShortCodesContainer extends WPBakeryShortCodesContainer {
    public function mainHtmlBlockParams( $width, $i ) {
        return 'data-element_type="'.$this->settings["base"].'" class="wpb_'.$this->settings['base'].' wpb_sortable wpb_content_holder vc_shortcodes_container intense_vc_shortcodes_container"'.$this->customAdminBlockParams();
    }
    public function containerHtmlBlockParams( $width, $i ) {
        return 'class="wpb_column_container vc_container_for_children intense_column_container"';
    }
    public function contentAdmin( $atts, $content = null ) {
        $width = $el_class = '';
        extract( shortcode_atts( $this->predefined_atts, $atts ) );
        $output = '';

        $column_controls = $this->getColumnControls( $this->settings( 'controls' ) );
        $column_controls_bottom =  $this->getColumnControls( 'add', 'bottom-controls' );
        for ( $i=0; $i < count( $width ); $i++ ) {
            $output .= '<div '.$this->mainHtmlBlockParams( $width, $i ).'>';
            $output .= $column_controls;
            $output .= '<div class="wpb_element_wrapper parent"><h4 class="wpb_element_title"><span class="intense-icon dashicons '.__( $this->settings['icon'], "intense" ) . '"></span> '.__( $this->settings['name'], "intense" ) . '</h4>';
            $output .= '<div '.$this->containerHtmlBlockParams( $width, $i ).'>';
            $output .= do_shortcode( shortcode_unautop( $content ) );
            $output .= '</div>';
            if ( isset( $this->settings['params'] ) ) {
                $inner = '';
                foreach ( $this->settings['params'] as $param ) {
                    $param_value = isset( $$param['param_name'] ) ? $$param['param_name'] : '';
                    if ( is_array( $param_value ) ) {
                        // Get first element from the array
                        reset( $param_value );
                        $first_key = key( $param_value );
                        $param_value = $param_value[$first_key];
                    }
                    $inner .= $this->singleParamHtmlHolder( $param, $param_value );
                }
                $output .= $inner;
            }
            $output .= '</div>';
            $output .= $column_controls_bottom;
            $output .= '</div>';
        }
        return $output;
    }
}