<?php

class Intense_Audio extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Audio', 'intense' );
        $this->category = __( 'Media', 'intense' );
        $this->icon = 'dashicons-format-audio';
        $this->show_preview = true;
        $this->is_container = false;

        $this->fields = array(
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
                    'title' => array(
                        'type' => 'hidden',
                        'title' => __( 'Title', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'url' => array(
                        'type' => 'text',
                        'title' => __( 'URL', 'intense' ),
                        'description' => __( 'Audio file url or audio attachment id. Supported formats: mp3, ogg', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'width' => array(
                        'type' => 'text',
                        'title' => __( 'Width', 'intense' ),
                        'description' => __( 'Examples: auto, 100%, or 300px', 'intense' ),
                        'default' => 'auto'
                    ),
                    'autoplay' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Autoplay', 'intense' ),
                        'description' => __( 'Play audio file automatically when page is loaded.', 'intense' ),
                        'default' => "0",
                    ),
                    'loop' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Loop', 'intense' ),
                        'description' => __( 'Repeat when playback has ended.', 'intense' ),
                        'default' => "0",
                    ),
                    'class' => array(
                        'type' => 'text',
                        'title' => __( 'CSS Class', 'intense' ),
                        'default' => ''
                    )
                )
            ),
            'color_tab' => array(
                'type' => 'tab',
                'title' => __( 'Color', 'intense' ),
                'fields' =>  array(
                    'background_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Background Color', 'intense' ),
                        'default' => '#dddddd'
                    ),
                    'button_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Button Color', 'intense' ),
                        'default' => 'primary'
                    ),
                    'button_background_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Button Background Color', 'intense' ),
                        'default' => '#333333'
                    ),
                    'text_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Text Color', 'intense' ),
                        'default' => '#444444'
                    ),
                    'progress_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Progress Color', 'intense' ),
                        'default' => 'primary'
                    ),
                    'progress_background_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Progress Background Color', 'intense' ),
                        'default' => '#cccccc'
                    )
                )
            )
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        intense_add_style( 'intense.audio' );
        intense_add_script( 'jquery.jplayer' );
        intense_add_script( 'intense.audio' );

        $id = 'audio' . rand();
        $audio_width = ( $width !== 'auto' ) ? 'max-width:' . $width : '';
        $play_icon = intense_run_shortcode( 'intense_icon', array( 'type' => 'play', 'color' => $button_color ) );
        $pause_icon = intense_run_shortcode( 'intense_icon', array( 'type' => 'pause', 'color' => $button_color ) );
        $backgroundcolor = ' background:' . intense_get_plugin_color( $background_color ) . ';';
        $buttonbackground = 'style="background:' . intense_get_plugin_color( $button_background_color ) . ';"';
        $textcolor = 'style="color:' . intense_get_plugin_color( $text_color ) . ';"';
        $progresscolor = 'style="background:' . intense_get_plugin_color( $progress_color ) . ';"';
        $progressbackground = 'style="background:' . intense_get_plugin_color( $progress_background_color ) . ';"';

        if ( is_numeric( $url ) ) {
            $url = wp_get_attachment_url( $url );
        }

        $output = '<div class="intense audio ' . $class . '" data-id="' . $id . '" data-audio="' . $url . '" data-swf="' . INTENSE_PLUGIN_URL . '/assets/js/jquery.jplayer/Jplayer.swf' . '" data-autoplay="' . $autoplay . '" data-loop="' . $loop . '" style="' . $backgroundcolor . '"><div id="' . $id . '" class="jp-jplayer"></div><div id="' . $id . '_container" class="jp-audio"><div class="jp-type-single"><div class="jp-gui jp-interface"><div class="jp-controls"><span class="jp-play" ' . $buttonbackground . '>' . $play_icon . '</span><span class="jp-pause" ' . $buttonbackground . '>' . $pause_icon . '</span><span class="jp-stop"></span><span class="jp-mute"></span><span class="jp-unmute"></span><span class="jp-volume-max"></span></div><div class="jp-progress" style=""><div class="jp-seek-bar" ' . $progressbackground . '><div class="jp-play-bar" ' . $progresscolor . '></div></div></div><div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div><div class="jp-current-time" ' . $textcolor . '></div><div class="jp-duration" ' . $textcolor . '></div></div><div class="jp-title">' . $title . '</div></div></div></div>';

        return $output;
    }

}