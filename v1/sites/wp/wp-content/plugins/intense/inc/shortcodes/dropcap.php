<?php

class Intense_Dropcap extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Dropcap', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-insertpictureleft';
		$this->show_preview = true;
		$this->preview_content = __( "D", 'intense' );
		$this->vc_map_content = 'textfield';

		$this->fields = array(
			'color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Color', 'intense' ),
				'default' => ''
			),
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
				'default' => ''
			),
			'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
		);
	}

	function shortcode( $atts, $content = null ) {
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$background_color = "";
		$color = intense_get_plugin_color( $color );

		if ( !empty( $color ) ) {
			$background_color = "background: " . $color . ";";
		}

		$font_color = intense_get_plugin_color( $font_color );

		if ( $font_color == '' && $color != '' ) {
            $font_color = intense_get_contract_color( $color );
        }

		if ( $font_color == '' ) {
			$style = "";
		} else {
			$style = "color: " . $font_color . ";";
		}

		return "<span class='intense dropcap " . ( $rtl ? "rtl" : "" ) . "' style='$background_color $style'>" . do_shortcode( $content ) . "</span>";
	}
}
