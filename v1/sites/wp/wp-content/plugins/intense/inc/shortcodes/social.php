<?php

class Intense_Social_Icon extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Social Icon', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-intense-share';
		$this->show_preview = true;
		$this->map_visual_composer = false;

		$this->fields = array(
			'mode' => array(
				'type' => 'dropdown',
				'title' => __( 'Mode', 'intense' ),
				'default' => 'fontawesome',
				'options' => array(
					'fontawesome' => __( 'Font Awesome', 'intense' ),
					'image' => __( 'Image', 'intense' ),
					'custom' => __( 'Custom', 'intense' ),
				)
			),
			'image' => array(
				'type' => 'image',
				'title' => __( 'Custom Icon', 'intense' ),
				'class' => 'custommode',
                'skinnable' => '0'
			),
			'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
			'type' => array(
				'type' => 'dropdown',
				'title' => __( 'Type', 'intense' ),
				'default' => '',
				'options' => array(					
					'Font Awesome' => array(
						'' => '',
						'adn' => 'adn',
						'android' => 'android',
						'apple' => 'apple',
						'bitbucket' => 'bitbucket',
						'bitbucket-sign' => 'bitbucket-sign',
						'bitcoin' => 'bitcoin',
						'btc' => 'btc',
						'css3' => 'css3',
						'dribbble' => 'dribbble',
						'dropbox' => 'dropbox',
						'facebook' => 'facebook',
						'facebook-sign' => 'facebook-sign',
						'flickr' => 'flickr',
						'foursquare' => 'foursquare',
						'github' => 'github',
						'github-alt' => 'github-alt',
						'github-sign' => 'github-sign',
						'gittip' => 'gittip',
						'google-plus' => 'google-plus',
						'google-plus-sign' => 'google-plus-sign',
						'html5' => 'html5',
						'instagram' => 'instagram',
						'linkedin' => 'linkedin',
						'linkedin-sign' => 'linkedin-sign',
						'linux' => 'linux',
						'maxcdn' => 'maxcdn',
						'pinterest' => 'pinterest',
						'pinterest-sign' => 'pinterest-sign',
						'renren' => 'renren',
						'skype' => 'skype',
						'stackexchange' => 'stackexchange',
						'trello' => 'trello',
						'tumblr' => 'tumblr',
						'tumblr-sign' => 'tumblr-sign',
						'twitter' => 'twitter',
						'twitter-sign' => 'twitter-sign',
						'vk' => 'vk',
						'weibo' => 'weibo',
						'windows' => 'windows',
						'xing' => 'xing',
						'xing-sign' => 'xing-sign',
						'youtube' => 'youtube',
						'youtube-play' => 'youtube-play',
						'youtube-sign' => 'youtube-sign',
					),
					'Image' => array(
						'Facebook' => 'Facebook',
						'Dribbble' => 'Dribbble',
						'Blogger' => 'Blogger',
						'Twitter' => 'Twitter',
						'Tumblr' => 'Tumblr',
						'Google Plus' => 'Google Plus',
						'RSS' => 'RSS',
						'Reddit' => 'Reddit',
						'Email' => 'Email',
						'YouTube' => 'YouTube',
						'Yahoo' => 'Yahoo',
						'Vimeo' => 'Vimeo',
						'LinkedIn' => 'LinkedIn',
						'Flickr' => 'Flickr',
						'Foursquare' => 'Foursquare',
						'Skype' => 'Skype',
						'StumbleUpon' => 'StumbleUpon',
						'Deviant Art' => 'Deviant Art',
						'Pinterest' => 'Pinterest',
						'Digg' => 'Digg',
						'Forrst' => 'Forrst',
						'MySpace' => 'MySpace',
						'500px' => '500px',
						'Aboutme' => 'Aboutme',
						'Bing' => 'Bing',
						'Add This' => 'Add This',
						'Amazon' => 'Amazon',
						'AOL' => 'AOL',
						'App Store' => 'App Store',
						'Apple' => 'Apple',
						'Bebo' => 'Bebo',
						'Behance' => 'Behance',
						'Blip' => 'Blip',
						'Coroflot' => 'Coroflot',
						'Daytum' => 'Daytum',
						'Delicious' => 'Delicious',
						'Design Bump' => 'Design Bump',
						'DesignFloat' => 'DesignFloat',
						'Dropplr' => 'Dropplr',
						'Drupal' => 'Drupal',
						'EBay' => 'EBay',
						'Ember' => 'Ember',
						'Etsy' => 'Etsy',
						'FeedBurner' => 'FeedBurner',
						'FoodSpotting' => 'FoodSpotting',
						'FriendFeed' => 'FriendFeed',
						'Friendster' => 'Friendster',
						'gdgt' => 'gdgt',
						'github' => 'github',
						'Google Talk' => 'Google Talk',
						'Google' => 'Google',
						'GrooveShark' => 'GrooveShark',
						'Hyves' => 'Hyves',
						'IconDock' => 'IconDock',
						'ICQ' => 'ICQ',
						'Identi' => 'Identi',
						'iMessage' => 'iMessage',
						'Instagram' => 'Instagram',
						'iTune' => 'iTune',
						'LastFM' => 'LastFM',
						'Meetup' => 'Meetup',
						'Metacafe' => 'Metacafe',
						'Microsoft' => 'Microsoft',
						'Mister Wong' => 'Mister Wong',
						'Mixx' => 'Mixx',
						'MobileMe' => 'MobileMe',
						'NetVibes' => 'NetVibes',
						'NewsVine' => 'NewsVine',
						'PayPal' => 'PayPal',
						'PhotoBucket' => 'PhotoBucket',
						'Podcast' => 'Podcast',
						'Posterous' => 'Posterous',
						'Qik' => 'Qik',
						'Quora' => 'Quora',
						'Scribd' => 'Scribd',
						'Sharethis' => 'Sharethis',
						'Slashdot' => 'Slashdot',
						'Slideshare' => 'Slideshare',
						'SmugMug' => 'SmugMug',
						'SoundCloud' => 'SoundCloud',
						'Spotify' => 'Spotify',
						'Squidoo' => 'Squidoo',
						'StackOverflow' => 'StackOverflow',
						'Technorati' => 'Technorati',
						'Viddler' => 'Viddler',
						'Virb' => 'Virb',
						'Wikipedia' => 'Wikipedia',
						'WordPress' => 'WordPress',
						'Xing' => 'Xing',
						'Yelp' => 'Yelp',
					),
				),
				'class' => 'fontmode',
                'skinnable' => '0'
			),			
			'link' => array(
				'type' => 'text',
				'title' => __( 'Link', 'intense' ),
				'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
			),
			'link_target' => array(
				'type' => 'link_target',
				'title' => __( 'Target', 'intense' ),
				'default' => '_self'
			),
			'size' => array(
				'type' => 'dropdown',
				'title' => __( 'Size', 'intense' ),
				'default' => '32',
				'options' => array(
					"32" => '32',
					"31" => '31',
					"30" => '30',
					"29" => '20',
					"28" => '28',
					"27" => '27',
					"26" => '26',
					"25" => '25',
					"24" => '24',
					"23" => '23',
					"22" => '22',
					"21" => '21',
					"20" => '20',
					"19" => '19',
					"18" => '18',
					"17" => '17',
					"16" => '16',
					"15" => '15',
					"14" => '14',
					"13" => '13',
					"12" => '12',
					"11" => '11',
					"10" => '10',
					"9" => '9',
					"8" => '8'
				),
			),
			'label' => array(
				'type' => 'text',
				'title' => __( 'Label', 'intense' ),
                'skinnable' => '0'
			),
			'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Color', 'intense' ),
                'default' => '',
                'class' => 'fontmode'
            ),
			'onclick' => array(
				'type' => 'text',
				'title' => __( 'On Click', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( !isset( $label ) ) $label = $type;
		if ( isset( $onclick ) ) $onclick = ' onclick="' . $onclick . '" ';

        $color = intense_get_plugin_color( $color );

        if ( is_numeric( $link ) ) {
        	$link = get_permalink( $link );
        }

		$output = '<a class="intense ' . $type . ' social-link" href="' . $link . '" target="' . $link_target . '" rel="external nofollow" title="' . $label . '" style="color: ' . $color . '; text-decoration: none; font-size: ' . $size . 'px; display: inline-block; padding: 5px;"' . $onclick . '>';

		if ( $mode == "image" ) {
			$output .= '<img width="' . $size . '" height="' . $size . '" src="' . INTENSE_PLUGIN_URL . '/assets/img/social/PNG/32px/' . str_replace( " ", "-", mb_strtolower( $type ) ) . '.png" alt="' . $label . '" />';
		} else if ( $mode == "custom" ) {
			if ( is_numeric( $image ) ) {
				$image_info = wp_get_attachment_image_src( $image, 'square75' );
				$imageurl = $image_info[0];
			} else if ( !empty( $image ) ) {
				$imageurl = $image;
			}

			$output .= '<img style="height:' . $size . 'px;" src="' . $imageurl . '" alt="' . $label . '" />';
		} else {
			$output .= intense_run_shortcode( 'intense_icon', array( 'type' => $type, 'color' => $color ) );
		}

		$output .= '</a>';

		return $output;
	}

	function render_dialog_script() {
		?>
		<script>
		jQuery(function($) {
			$(".custommode").hide();

			$("#mode").change(function() {
				var type = $(this).val();

				$(".custommode").hide();

				switch(type) {
					case "custom":
						$(".custommode").show();
						$(".fontmode").hide();
						break;
					case "image":
						$(".fontmode").show();
						$(".custommode").hide();
					default:
						$(".fontmode").show();
						$(".custommode").hide();
						break;
				}
			});

			// Uploading files
	  		$('.upload_image_button').live('click', function( event ){
			    event.preventDefault();

			    window.parent.loadImageFrame($(this), function(attachment) {
			    	jQuery("#image").val(attachment.id);
			    	jQuery("#image-thumb").attr('src', attachment.url);

			    	if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
			    });
			});
		});
	</script>
	<?php
	}
}
