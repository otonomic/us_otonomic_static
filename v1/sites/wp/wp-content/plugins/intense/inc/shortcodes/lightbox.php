<?php

require_once INTENSE_PLUGIN_FOLDER . '/inc/tools/lightboxes.php';

class Intense_Lightbox extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Lightbox', 'intense' );
    $this->category = __( 'Elements', 'intense' );
    $this->icon = 'dashicons-intense-gallery';
    $this->show_preview = false;
    $this->is_container = true;

    $this->fields = array(
      'type' => array(
        'type' => 'dropdown',
        'title' => __( 'Type', 'intense' ),
        'default' => 'colorbox',
        'options' => array(
          'colorbox' => __( 'Colorbox', 'intense' ), //image, iframe, html
          'magnificpopup' => __( 'Magnific Popup', 'intense' ), //image, iframe, html
          'photoswipe' => __( 'PhotoSwipe (image only)', 'intense' ), //image
          'prettyphoto' => __( 'PrettyPhoto', 'intense' ), //image, iframe, html
          'swipebox' => __( 'Swipebox (image only)', 'intense' ), //image
          'thickbox' => __( 'Thickbox (image only)', 'intense' ), //image
          'touchtouch' => __( 'TouchTouch (image only)', 'intense' ), //image
        ),
      ),
      'content_type' => array(
        'type' => 'dropdown',
        'title' => __( 'Content Type', 'intense' ),
        'default' => 'image',
        'options' => array(
          'html' => 'html',
          'image' => 'image',
          'iframe' => 'iframe'
        ),
      ),
      'url' => array(
        'type' => 'text',
        'title' => __( 'URL', 'intense' ),
        'description' => __( 'used for the url of the iframe', 'intense' ),
        'class' => 'iframesettings',
        'skinnable' => '0'
      ),
      'html_element' => array(
        'type' => 'text',
        'title' => __( 'HTML Element ID', 'intense' ),
        'description' => __( 'the content of the HTML element will be shown in the lightbox', 'intense' ),
        'class' => 'htmlsettings',
        'skinnable' => '0'
      ),
      'image' => array(
        'type' => 'image',
        'title' => __( 'Image', 'intense' ),
        'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
        'class' => 'imagesettings',
        'skinnable' => '0'
      ),
      'image_size' => array(
        'type' => 'image_size',
        'title' => __( 'Image Size', 'intense' ),
        'default' => 'full',
        'class' => 'imagesettings'
      ),
      'title' => array(
        'type' => 'text',
        'title' => __( 'Link Title', 'intense' ),
        'class' => 'imagesettings',
        'skinnable' => '0'
      ),
      'description' => array(
        'type' => 'text',
        'title' => __( 'Link Description', 'intense' ),
        'class' => 'imagesettings',
        'skinnable' => '0'
      ),
      'grouping' => array(
        'type' => 'text',
        'title' => __( 'Lightbox Group', 'intense' ),
        'description' => __( 'optional - allows for lightbox slideshows', 'intense' ),
        'class' => 'imagesettings'
      ),
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    $output = '';
    $id = rand();

    $lightbox = Intense_Lightboxes::create( $type, '#intense-lightbox-' . $id . ' a' );

    $output .= '<span id="intense-lightbox-' . $id . '" class="intense lightbox">';

    switch ( $content_type ) {
    case 'html':
      $output .= $lightbox->get_html( do_shortcode( $content ), $html_element  );
      $output .= '<script>jQuery(function($) { $("#' . $html_element . '").wrap("<div style=\'display:none\'>"); ' . $lightbox->get_html_script() . '});</script>';
      $output .= '<style type="text/css">' . $lightbox->get_css() . '</style>';
      break;
    case 'image':
      if ( is_numeric( $image ) ) {
        $photo_info = wp_get_attachment_image_src( $image, $image_size );
        $display_image = $photo_info[0];
        $full_size = ( isset( $intense_visions_options['intense_photosource_full_size'] ) ? $intense_visions_options['intense_photosource_full_size'] : 'large' );
        $photo_info = wp_get_attachment_image_src( $image, $full_size );
        $full_size_image = $photo_info[0];
      } else if ( !empty( $image ) ) {
          $full_size_image = $image;
        }

      $output .= '<script>jQuery(function($) {' . $lightbox->get_image_script() . '});</script>';
      $output .= $lightbox->get_image( $grouping, $title, $description, $full_size_image, '<img src="' . $display_image . '">' );
      $output .= '<style type="text/css">' . $lightbox->get_css() . '</style>';
      break;
    case 'iframe':
      $output .= $lightbox->get_iframe( do_shortcode( $content ), $url );
      $output .= '<script>jQuery(function($) { ' . $lightbox->get_iframe_script() . '});</script>';
      $output .= '<style type="text/css">' . $lightbox->get_css() . '</style>';
      break;
    }

    $output .= '</span>';

    return $output;
  }

  function render_dialog_script() {
?>
    <script>
        jQuery(function($) {
          // Uploading files
            $('.upload_image_button').live('click', function( event ){
              event.preventDefault();
              var target = $(this).data('target-id');

              window.parent.loadImageFrame($(this), function(attachment) {
                jQuery("#" + target).val(attachment.id);

                if ( $('#preview-content').length > 0 ) {
                  $('#preview').click();
                }
              });
          });

          $(".htmlsettings").hide();
          $(".iframesettings").hide();

          $("#content_type").change(function() {
            var type = $(this).val();

            $(".htmlsettings").hide();
            $(".iframesettings").hide();
            $(".imagesettings").hide();

            switch(type) {
              case "html":
                $(".htmlsettings").show();
                break;
               case "iframe":
                $(".iframesettings").show();
                break;
               case "image":
                $(".imagesettings").show();
                break;
            }
          });

          $("#type").change(function() {
          var type = $(this).val();

          $("#content_type").children().attr('disabled','disabled');

          switch(type) {
            case "colorbox":
            case "magnificpopup":
            case "prettyphoto":
              $("#content_type").children().removeAttr('disabled');
            break;
            default:
              $("#content_type").children('[value="image"]').removeAttr('disabled');
              $("#content_type").val('image');
              $("#content_type").change();
            break;
          }
          });
        });
      </script>
    <?php
  }
}
