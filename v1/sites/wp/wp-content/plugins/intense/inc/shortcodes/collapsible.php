<?php

class Intense_Collapsibles extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Collapsibles', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-collapse-alt';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
                'skinnable' => '0'
			),
			'single_toggle' => array(
				'type' => 'checkbox',
				'title' => __( 'Single Toggle', 'intense' ),
				'description' => __( 'show only one at a time', 'intense' ),
				'default' => '0'
			),
			'title_border' => array(
				'type' => 'border',
				'title' => __( 'Title Border', 'intense' ),
			),
			'content_border' => array(
				'type' => 'border',
				'title' => __( 'Content Border', 'intense' ),
			),
			'expand_icon' => array(
				'type' => 'icon',
				'title' => __( 'Expand Icon', 'intense' ),
				'default' => 'plus-sign-alt',
			),
			'collapse_icon' => array(
				'type' => 'icon',
				'title' => __( 'Collapse Icon', 'intense' ),
				'default' => 'minus-sign-alt',
			),
			'icon_size' => array(
				'type' => 'dropdown',
				'title' => __( 'Icon Size', 'intense' ),
				'default' => '2',
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5'
				)
			),
			'collapse' => array(
				'type' => 'repeater',
				'title' => __( 'Sections', 'intense' ),
				'preview_content' => __( 'Collapse Content', 'intense' ),
				'fields' => array(
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
					),
					'active' => array(
						'type' => 'checkbox',
						'title' => __( 'Active', 'intense' ),
						'description' => __( 'open collapse when the page loads', 'intense' ),
						'default' => '0',
						'skinnable' => '0'
					),
					'class' => array(
						'type' => 'text',
						'title' => __( 'CSS Class', 'intense' ),
					),
					'external_link' => array(
						'type' => 'text',
						'title' => __( 'Link', 'intense' ),
						'description' => __( 'link to external page instead of expand. URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        'skinnable' => '0'						
					),
					'external_link_target' => array(
						'type' => 'link_target',
						'title' => __( 'Link Target', 'intense' ),
						'default' => '_self'
					),
					'title_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Title Background Color', 'intense' ),
						'default' => '#ffffff'
					),
					'content_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Content Background Color', 'intense' ),
						'default' => '#ffffff'
					),
					'title_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Title Font Color', 'intense' ),
						'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
					),
					'content_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Content Font Color', 'intense' ),
						'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
					),
				)
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( isset( $GLOBALS['collapsibles_count'] ) )
			$GLOBALS['collapsibles_count']++;
		else
			$GLOBALS['collapsibles_count'] = 0;

		intense_add_script( 'intense.collapsible' );

		global $intense_collapsible_expand_icon, $intense_collapsible_collapse_icon,
		$intense_collapsible_title_border, $intense_collapsible_content_border;

		$intense_collapsible_expand_icon = intense_run_shortcode( 'intense_icon', array( 'type' => $expand_icon, 'extra_class' => 'expand-icon hide', 'size' => $icon_size ) );
		$intense_collapsible_collapse_icon = intense_run_shortcode( 'intense_icon', array( 'type' => $collapse_icon, 'extra_class' => 'collapse-icon hide', 'size' => $icon_size ) );
		$intense_collapsible_title_border = $title_border;
		$intense_collapsible_content_border = $content_border;

		// Extract the tab titles for use in the tab widget.
		preg_match_all( '/collapse *title *= *["\']([^\"\']+)"/i', $content, $matches, PREG_OFFSET_CAPTURE );

		$collapse_titles = array();
		if ( isset( $matches[1] ) ) { $collapse_titles = $matches[1]; }

		$output = '';

		if ( count( $collapse_titles ) ) {
			$output .= '<div class="intense panel-group" id="' . $id . '-accordion-' . $GLOBALS['collapsibles_count'] . '" data-single-toggle="' . $single_toggle . '">';
			$output .= do_shortcode( $content );
			$output .= '</div>';
		} else {
			$output .= do_shortcode( $content );
		}

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
			jQuery(function($) {
				$(document).on("click", '[id^="active"]', function() {
					$('[id^="active"]').not(this).attr('checked', false);
				});
			});
		</script>
	<?php
	}
}

class Intense_Collapse extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Collapse', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-collapse-alt';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_collapsibles';
		$this->is_container = true;

		$this->fields = array(
			'title' => array(
				'type' => 'text',
				'title' => __( 'Title', 'intense' ),
                'skinnable' => '0'
			),
			'active' => array(
				'type' => 'checkbox',
				'title' => __( 'Active', 'intense' ),
				'description' => __( 'open collapse when the page loads', 'intense' ),
				'default' => '0',
                'skinnable' => '0'
			),
			'class' => array(
				'type' => 'text',
				'title' => __( 'CSS Class', 'intense' ),
			),
			'external_link' => array(
				'type' => 'text',
				'title' => __( 'Link', 'intense' ),
				'description' => __( 'link to external page instead of expand. URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
			),
			'external_link_target' => array(
				'type' => 'link_target',
				'title' => __( 'Link Target', 'intense' ),
				'default' => '_self'
			),
			'title_background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Title Background Color', 'intense' ),
				'default' => '#ffffff'
			),
			'content_background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Content Background Color', 'intense' ),
				'default' => '#ffffff'
			),
			'title_font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Title Font Color', 'intense' ),
				'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
			),
			'content_font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Content Font Color', 'intense' ),
				'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_collapsible_expand_icon, $intense_collapsible_collapse_icon,
		$intense_collapsible_title_border, $intense_collapsible_content_border;

		$output = '';		
		$title_font_color_style = '';
		$content_font_color_style = '';

		$border_size = $intense_visions_options['intense_content_box_style']['border-top'];
		$border_color = $intense_visions_options['intense_content_box_style']['border-color'];
		$border_style = $intense_visions_options['intense_content_box_style']['border-style'];
		$border = 'border-bottom:' . $border_size . ' ' . $border_style . ' ' . $border_color . ';';

		$title_border_style = '';
		$content_border_style = '';

		if ( !empty ( $intense_collapsible_title_border ) ) {
			$title_border_style = "border: " . $intense_collapsible_title_border . ' !important;';
		}

		if ( !empty ( $intense_collapsible_content_border ) ) {
			$content_border_style = "border: " . $intense_collapsible_content_border . ' !important;';
			$border = '';
		}

		$title_background_color = intense_get_plugin_color( $title_background_color );
		$content_background_color = intense_get_plugin_color( $content_background_color );
		$title_font_color = intense_get_plugin_color( $title_font_color );

		if ( empty( $title_font_color ) ) {
			$title_font_color = intense_get_contract_color( $title_background_color );
		}

		$content_font_color = intense_get_plugin_color( $content_font_color );

		if ( empty( $content_font_color ) ) {
			$content_font_color = intense_get_contract_color( $content_background_color );
		}

		if ( !empty( $title_background_color ) ) {
			$title_background_color = 'background-color: ' . $title_background_color . ';';
		}

		if ( !empty( $content_background_color ) ) {
			$content_background_color = 'background-color: ' . $content_background_color . ';';
		}		

		if ( !empty( $title_font_color ) ) $title_font_color_style = 'color: ' . $title_font_color . ' !important;';
		if ( !empty( $content_font_color ) ) $content_font_color_style = 'color: ' . $content_font_color . ' !important;';

		if ( !isset( $GLOBALS['current_collapse'] ) )
			$GLOBALS['current_collapse'] = 0;
		else
			$GLOBALS['current_collapse']++;

		if ( $active ) {
			$active = 'in';
			$collapse_icon = str_replace( "hide", "", $intense_collapsible_collapse_icon );
			$expand_icon = $intense_collapsible_expand_icon;
		} else {
			$expand_icon = str_replace( "hide", "", $intense_collapsible_expand_icon );
			$collapse_icon = $intense_collapsible_collapse_icon;
		}

		$output = '<div class="intense panel ' . $class . '">';
		$output .= '	<div class="intense panel-heading" style="' . $title_border_style . $title_background_color . $title_font_color_style . '">';
		$output .= '	' . $collapse_icon . $expand_icon;

		if ( !empty( $external_link ) ) {
			if ( is_numeric( $external_link ) ) {
				$external_link = get_permalink( $external_link );
			}

			$output .= '	<a class="intense faq-title" href="' . $external_link . '" target="' . $external_link_target . '" style="text-decoration: none; ' . $title_font_color_style . '">';			
		} else {
			$output .= '	<a class="intense panel-title accordion-toggle" data-parent="#accordion-' . $GLOBALS['collapsibles_count'] . '" href="#collapse_' . $GLOBALS['current_collapse'] . '_'. sanitize_title( $title ) .'" style="text-decoration: none; ' . $title_font_color_style . '">';			
		}

		$output .= '	' . $title . '';
		$output .= '	</a>';

		$output .= '	</div>';
		$output .= '	<div id="collapse_' . $GLOBALS['current_collapse'] . '_'. sanitize_title( $title ) .'" class="intense panel-collapse collapse ' . $active . '" style="' . $border . $content_border_style . $content_background_color . $content_font_color_style . '";">';
		$output .= '		<div class="intense panel-body">';
		$output .= '		' . do_shortcode( $content ) . '';
		$output .= '		</div>';
		$output .= '	</div>';
		$output .= '</div>';

		return $output;
	}
}
