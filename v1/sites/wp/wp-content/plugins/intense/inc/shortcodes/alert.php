<?php

class Intense_Alert extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Alert', 'intense' );
        $this->category = __( 'Layout', 'intense' );
        $this->icon = 'dashicons-intense-alert';
        $this->show_preview = true;
        $this->preview_content = __( "Alert Content", 'intense' );
        $this->is_container = true;

        $this->fields = array(
            'color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Color', 'intense' ),
                'default' => 'primary'
            ),
            'font_color' => array(
                'type' => 'color_advanced',
                'title' => __( 'Font Color', 'intense' ),
                'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                'default' => ''
            ),
            'border_radius' => array(
                'type' => 'border_radius',
                'title' => __( 'Border Radius', 'intense' ),                
            ),
            'block' => array(
                'type' => 'checkbox',
                'title' => __( 'Block', 'intense' ),
                'description' => __( 'block alerts have slightly more padding', 'intense' ),
                'default' => "0",
            ),
            'close' => array(
                'type' => 'checkbox',
                'title' => __( 'Close Button', 'intense' ),
                'description' => __( 'If enabled, a close button will show in the top right corner of the alert. When clicked on, the alert will be removed from the page until the page is loaded again.', 'intense' ),
                'default' => "0",
            ),
            'shadow' => array(
                'type' => 'shadow',
                'title' => __( 'Shadow', 'intense' ),
            ),
            'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        if ( $close ) {
            intense_add_script( 'bootstrap-alert' );
        }

        $radius = '';
        if ( isset( $border_radius ) ) {
            $radius = ' border-radius:' . $border_radius . ';';
        }

        $color = intense_get_plugin_color( $color );
        $font_color = intense_get_plugin_color( $font_color );        

        if ( $font_color == '' ) {
            $font_color = intense_get_contract_color( $color );
        }

        $output = '';

        if ( $shadow ) {
            $output .= "<div><div style='margin-bottom: 0;'>";
        }

        $output .= "<div class='intense alert " . ( $close ? 'alert-dismissable ' : '' ) . ( $block ? "alert-block " : "" ) . ( $rtl ? "rtl " : "" ) . "' style='color: " . $font_color . "; background: " . $color  . "; " . $radius . ";'>" . ( $close ? '<button type="button" class="intense close" data-dismiss="alert" aria-hidden="true">&times;</button>' : "" ) . do_shortcode( $content ) . "</div>";

        if ( $shadow ) {
            $output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='vertical-align: top; margin-top: -20px;' alt='' /></div>";
        }

        return $output;
    }

}