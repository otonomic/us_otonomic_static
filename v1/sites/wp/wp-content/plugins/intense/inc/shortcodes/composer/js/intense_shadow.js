jQuery(function($) {
	$(document).ready(function() {
		$('.shadowblock').each(function() {
			var $shadowBlock = $(this);
			var $shadowPath = $('.shadowpreview', $shadowBlock).attr('src').replace('0.png', '');

			$('.shadows', $shadowBlock).change(function() {
				$('.shadowpreview', $shadowBlock).attr('src', $shadowPath + $(this).val() + '.png');
			});

			$('.shadowpreview', $shadowBlock).attr('src', $shadowPath + $('.shadows', $shadowBlock).val() + '.png');
		});
	});
});