<?php

class Intense_QR_Code extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'QR Code', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-qrcode';
		$this->show_preview = true;

		$this->fields = array(
			'data' => array(
				'type' => 'text',
				'title' => __( 'Data', 'intense' ),
				'description' => __( 'the data to embed in the QR code. Often this is used to embed URLs', 'intense' ),
				'default' => '',
        		'composer_show_value' => true,
                'skinnable' => '0'
			),
			'width' => array(
				'type' => 'text',
				'title' =>  __( 'Width', 'intense' ),
				'description' => __( 'measured in pixels', 'intense' ),
				'default' => '150'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		return '<img src="https://chart.googleapis.com/chart?chs=' . $width . 'x' . $width . '&cht=qr&chld=L|0&chl=' . urlencode( $data ) . '" width="' . $width . '" height="' . $width . '" />';
	}
}
