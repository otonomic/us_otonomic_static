<?php

require_once INTENSE_PLUGIN_FOLDER . '/inc/Intense-photosource.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/tools/lightboxes.php';

class Intense_Gallery extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Gallery', 'intense' );
        $this->category = __( 'Media', 'intense' );
        $this->icon = 'dashicons-intense-images';
        $this->show_preview = true;

        $post = get_post();

        $this->fields = array(
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
                    'type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Type', 'intense' ),
                        'default' => 'standard',
                        'options' => array(
                            'colorbox' => __( 'Colorbox', 'intense' ),
                            'magnificpopup' => __( 'Magnific Popup', 'intense' ),
                            'galleria' => __( 'Galleria', 'intense' ),
                            'photoswipe' => __( 'PhotoSwipe', 'intense' ),
                            'prettyphoto' => __( 'PrettyPhoto', 'intense' ),
                            'standard' => __( 'Standard', 'intense' ),
                            'supersized' => __( 'Supersized', 'intense' ),
                            'swipebox' => __( 'Swipebox', 'intense' ),
                            'thickbox' => __( 'Thickbox', 'intense' ),
                            'touchtouch' => __( 'TouchTouch', 'intense' ),
                        ),
                    ),                    
                    'show_animation' => array(
                        'type' => 'animation',
                        'title' => __( 'Show Animation', 'intense' ),            
                        'placeholder' => __( 'Select an animation', 'intense' ),
                        'default' => 'fadeInUp'
                    ),
                    'maximum_show_delay' => array(
                        'type' => 'text',
                        'title' => __( 'Maximum Show Delay', 'intense' ),
                        'default' => '600'
                    ),
                    'supersized_position' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Supersized Control Position', 'intense' ),
                        'default' => $intense_visions_options['intense_supersized_position'],
                        'options' => array(
                            'None' => __( 'None', 'intense' ),
                            'Bottom Left' => __( 'Bottom Left', 'intense' ),
                            'Top Left' => __( 'Top Left', 'intense' ),
                        ),
                        'class' => 'supersizedsettings',
                        'skinnable' => '0'         
                    ),
                    'grouping' => array(
                        'type' => 'text',
                        'title' => __( 'Lightbox Group', 'intense' ),
                        'description' => __( 'optional - allows for lightbox slideshows', 'intense' ),
                        'class' => 'grouping'
                    ),
                    'border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Border Radius', 'intense' ),                
                        'class' => 'radiussettings'
                    ),
                    'rtl' => array(
                        'type' => 'hidden',
                        'description' => __( 'right-to-left', 'intense' ),
                        'default' => $intense_visions_options['intense_rtl']
                    ),
                ),
            ),
            'layout_tab' => array(
                'type' => 'tab',
                'title' => __( 'Layout', 'intense' ),
                'fields' =>  array(
                    'layout' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Predefined Layout', 'intense' ),
                        'options' => array(
                            '' => '',
                            'square' => __( 'Square', 'intense' ),
                            'smallsquare' => __( 'Small Square', 'intense' ),
                            'circle' => __( 'Circle', 'intense' ),
                            'masonry' => __( 'Masonry', 'intense' )
                        ),          
                    ),
                    'size' => array(
                        'type' => 'image_size',
                        'title' => __( 'Image Size', 'intense' ),
                        'description' => __( 'For non WordPress photo sources, the closest matching image size will be selected. This may or may not match depending on the photo source.', 'intense'),
                        'class' => 'sizesettings'
                    ),
                    'columns' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Columns', 'intense' ),
                        'default' => '0',
                        'options' => array(
                            '0' => __( 'Auto', 'intense' ),
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                            '6' => '6',
                            '7' => '7',
                            '8' => '8',
                            '9' => '9',
                            '10' => '10'
                        ),
                        'class' => 'columnssettings'
                    ),
                    'marginpercent' => array(
                        'type' => 'text',
                        'title' => __( 'Image Margin', 'intense' ),
                        'description' => __( 'precent of margin space to show around the photos. Typically a 1% or less margin looks good.', 'intense'),
                        'default' => '0',
                        'class' => 'marginpercentsettings'
                    ),
                    'gutter' => array(
                        'type' => 'text',
                        'title' => __( 'Masonry Gutter', 'intense' ),
                        'default' => '10',
                        'class' => 'masonrysettings'
                    ),
                    'masonry_item_width' => array(
                        'type' => 'text',
                        'title' => __( 'Masonry Item Width', 'intense' ),
                        'description' => __( 'measured as a % or px amount', 'intense' ),
                        'default' => '25%',
                        'class' => 'masonrysettings'
                    ),
                    'masonry_alternate_width' => array(
                        'type' => 'text',
                        'title' => __( 'Masonry Item Alternate Width', 'intense' ),
                        'description' => __( 'measured as a % or px amount', 'intense' ),
                        'default' => '50%',
                        'class' => 'masonrysettings'
                    ),
                    'alternate_size' => array(
                        'type' => 'image_size',
                        'title' => __( 'Alternate Image Size', 'intense' ),
                        'description' => __( 'For non WordPress photo sources, the closest matching image size will be selected. This may or may not match depending on the photo source.', 'intense'),
                        'class' => 'masonrysettings'
                    ),
                    'masonry_alternate_count' => array(
                        'type' => 'text',
                        'title' => __( 'Masonry Item Alternate Count', 'intense' ),
                        'description' => __( 'alternate the width every given number of items', 'intense' ),
                        'default' => '10',
                        'class' => 'masonrysettings'
                    ),
                )
            ),
            'source_tab' => array(
                'type' => 'tab',
                'title' => __( 'Source', 'intense' ),
                'fields' =>  array(
                    'source' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Source', 'intense' ),
                        'default' => 'wordpress',
                        'options' => array(
                            '500px' => __( '500px', 'intense' ),
                            'deviantart' => __( 'DeviantART', 'intense' ),
                            'facebook' => __( 'Facebook', 'intense' ),
                            'flickr' => __( 'Flickr', 'intense' ),
                            'google+' => __( 'Google+', 'intense' ),
                            'instagram' => __( 'Instagram', 'intense' ),
                            'smugmug' => __( 'SmugMug', 'intense' ),
                            'wordpress' => __( 'WordPress', 'intense' ),
                            'zenfolio' => __( 'Zenfolio', 'intense' ),
                        ),          
                    ),
                    'id' => array(
                        'type' => 'text',
                        'title' => __( 'Post ID', 'intense' ),
                        'default' => ( isset( $post ) ? $post->ID : null ),
                        'class' => 'wpsettings',
                        'skinnable' => '0'
                    ),
                    'include' => array(
                        'type' => 'gallery',
                        'title' => __( 'Included IDs', 'intense' ),
                        'description' => __( 'enter WordPress attachment IDs', 'intense' ),
                        'class' => 'wpsettings',
                        'skinnable' => '0'
                    ),
                    'exclude' => array(
                        'type' => 'gallery',
                        'title' => __( 'Excluded IDs', 'intense' ),
                        'description' => __( 'enter WordPress attachment IDs', 'intense' ),
                        'class' => 'wpsettings',
                        'skinnable' => '0'
                    ),
                    'ids' => array( 'type' => 'deprecated', 'description' => 'use include/exclude instead' ),
                    'order' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Order', 'intense' ),
                        'default' => 'NONE',
                        'options' => array(
                            'NONE' => __( 'None (order of included ids as they are entered)', 'intense' ),
                            'ASC' => __( 'Ascending', 'intense' ),
                            'DESC' => __( 'Descending', 'intense' ),
                            'RAND' => __( 'Random', 'intense' ),
                        ),
                        'class' => 'wpsettings'
                    ),
                    'orderby' => array(
                        'type' => 'text',
                        'title' => __( 'Order By', 'intense' ),                        
                        'class' => 'wpsettings'
                    ),
                    'user' => array(
                        'type' => 'text',
                        'title' => __( 'User', 'intense' ),
                        'class' => 'flickrsettings googleplussettings 500pxsettings smugmugsettings instagramsettings deviantartsettings',
                        'skinnable' => '0'
                    ),
                    'groupid' => array(
                        'type' => 'text',
                        'title' => __( 'Group', 'intense' ),
                        'class' => 'flickrsettings facebooksettings',
                        'skinnable' => '0'
                    ),
                    'facebookpage' => array(
                        'type' => 'text',
                        'title' => __( 'Facebook Page', 'intense' ),
                        'class' => 'facebooksettings',
                        'skinnable' => '0'
                    ),
                    'tag' => array(
                        'type' => 'text',
                        'title' => __( 'Tag', 'intense' ),
                        'class' => 'instagramsettings',
                        'skinnable' => '0'
                    ),
                    'setid' => array(
                        'type' => 'text',
                        'title' => __( 'Set', 'intense' ),
                        'class' => 'flickrsettings zenfoliosettings',
                        'skinnable' => '0'
                    ),
                    'album' => array(
                        'type' => 'text',
                        'title' => __( 'Album', 'intense' ),
                        'class' => 'smugmugsettings googleplussettings facebooksettings',
                        'skinnable' => '0'
                    ),
                    'pagesize' => array(
                        'type' => 'text',
                        'title' => __( 'Page Size', 'intense' ),
                        'default' => '25',
                        'description' => __( '# of photos to return', 'intense' ),
                        'class' => 'flickrsettings googleplussettings instagramsettings deviantartsettings facebooksettings zenfoliosettings smugmugsettings'
                    ),
                    'page' => array(
                        'type' => 'text',
                        'title' => __( 'Results Page', 'intense' ),
                        'default' => '0',
                        'description' => __( '0 is the first page', 'intense' ),
                        'class' => 'flickrsettings googleplussettings instagramsettings deviantartsettings facebooksettings zenfoliosettings 500pxsettings'
                    ),
                ),
            ),
            'hover_tab' => array(
                'type' => 'tab',
                'title' => __( 'Hover', 'intense' ),
                'fields' =>  array(
                    'show_tooltip' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Tooltip/Popover', 'intense' ),
                        'default' => '1',
                        'description' => __( 'Show tooltip/popover on hover', 'intense' )
                    ),
                    'hover_effect' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Hover Effect', 'intense' ),
                        'options' => array(
                            '' => '',
                            'effeckt' => __( 'Effeckt', 'intense' ),
                            'adipoli' => __( 'Adipoli', 'intense' ),
                        )
                    ),
                    'starteffect' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Start Effect', 'intense' ),
                        'default' => '',
                        'options' => array(
                            '' => '',
                            'normal' => 'normal',
                            'overlay' => 'overlay',
                            'sliceDown' => 'sliceDown',
                            'transparent' => 'transparent',
                        ),
                        'class' => 'adipoli-settings'
                    ),
                    'hovereffect' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Hover Effect', 'intense' ),
                        'default' => '',
                        'options' => array(
                            '' => '',
                            'boxRain' => 'boxRain',
                            'boxRainGrow' => 'boxRainGrow',
                            'boxRainGrowReverse' => 'boxRainGrowReverse',
                            'boxRainReverse' => 'boxRainReverse',
                            'boxRandom' => 'boxRandom',
                            'fold' => 'fold',
                            'foldLeft' => 'foldLeft',
                            'normal' => 'normal',
                            'popout' => 'popout',
                            'sliceDown' => 'sliceDown',
                            'sliceDownLeft' => 'sliceDownLeft',
                            'sliceUp' => 'sliceUp',
                            'sliceUpDown' => 'sliceUpDown',
                            'sliceUpDownLeft' => 'sliceUpDownLeft',
                            'sliceUpLeft' => 'sliceUpLeft',
                            'sliceUpRandom' => 'sliceUpRandom',
                        ),
                        'class' => 'adipoli-settings'
                    ),
                    'effeckt' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Effeckt', 'intense' ),
                        'default' => '',
                        'options' => array(
                            '' => '',
                            '1'  => __( 'Appear', 'intense' ),
                            '2'  => __( 'Quarter Slide Up', 'intense' ),
                            '3'  => __( 'Sqkwoosh', 'intense' ),
                            '4'  => __( 'Quarter Slide Side', 'intense' ),
                            '5'  => __( 'Cover Fade', 'intense' ),
                            '6'  => __( 'Quarter Fall In', 'intense' ),
                            '7'  => __( 'Quarter Two-Step', 'intense' ),
                            '8'  => __( 'Cover Push Right', 'intense' ),
                            '9'  => __( 'Quarter Caption Zoom', 'intense' ),
                            '10' => __( 'Revolving Door', 'intense' ),
                            '11' => __( 'Caption Offset', 'intense' ),
                            '12' => __( 'Guillotine Reverse', 'intense' ),
                            '13' => __( 'Half Slide', 'intense' ),
                            '14' => __( 'Tunnel', 'intense' ),
                            '15' => __( 'Cover Slide Top', 'intense' ),
                        ),
                        'class' => 'effeckt-settings'
                    ),
                    'effeckt_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Background Color', 'intense' ),
                        'default' => '#000000',
                        'class' => 'effeckt-settings'
                    ),
                    'effeckt_opacity' => array(
                        'type' => 'text',
                        'title' => __( 'Background Opacity', 'intense' ),
                        'description' => __( '0 - 100', 'intense' ),
                        'default' => '80',
                        'class' => 'effeckt-settings'
                    ),
                )
            )
        );
    }

    function shortcode( $atts, $content = null ) {        
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $additionalscript = "";

        static $instance = 0;
        $instance++;

    	if ( empty( $grouping ) ) {
    		$grouping = $instance;
    	}

        if ( !empty( $ids ) ) {
            // 'ids' is explicitly ordered, unless you specify otherwise.
            if ( empty( $orderby ) ) {
                $orderby = 'post__in';
            }

            $include = $ids;
        }

        $output = '';

        if ( $output != '' ) {
            return $output;
        }

        // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
        if ( isset( $orderby ) ) {
            $orderby = sanitize_sql_orderby( $orderby );

            if ( !$orderby ) {
                unset( $orderby );
            }
        }

        intense_add_script( 'jquery.appear' );

        intense_load_animation_CSS( $show_animation );

        $selector = "gallery-{$instance}";
        //jquery method used to determine how the items should show: appear will be when they come in to view, each will be when they are loaded
        $show_loop_method = "appear";

        $id = intval( $id );
        if ( 'RAND' == $order )
            $orderby = 'rand';

        if ( !empty( $include ) ) {
            $_attachments = get_posts( array( 'include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

            $attachments = array();

            if ( 'NONE' == $order ) {
                $include_order = preg_split( '/[,]/', $include );
                $_new_attachments = array();

                //give the attachments keys that match the id of the attachment
                //prior to this, the key is just the item number in the array 0,1,2,3,...
                foreach ( $_attachments as $key => $val ) {
                    $_attachments[ $val->ID ] = $_attachments[ $key ];
                    unset( $_attachments[ $key ] );
                }

                //reorder the attachment array to match the include id order
                foreach ( $include_order as $key ) {
                    $_new_attachments[ $key ] = $_attachments[ trim( $key ) ];
                }

                $_attachments = $_new_attachments;
            }

            foreach ( $_attachments as $key => $val ) {
                $attachments[$val->ID] = $_attachments[$key];
            }
        } elseif ( !empty( $exclude ) ) {
            $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
        } else {
            $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
        }      

        if ( empty( $attachments ) && $source == 'wordpress' )
            return '';

        if ( is_feed() ) {
            $output = "\n";
            foreach ( $attachments as $att_id => $attachment )
                $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
            return $output;
        }

        switch ( $layout ) {
            case 'circle':
                $size = "square150";
                $columns = 4;
                $starteffect = '';
                $hovereffect = '';
                break;
            case 'square':
                $size = "square150";
                $columns = 4;
                break;
            case 'smallsquare':
            case 'squaresmall':
                $size = "square75";
                $columns = 8;
                break; 
            case 'masonry':
                $masonry_style = '#' . $selector . ' .effeckt-caption { float: none; }';
                break;          
            default:
                break;
        }

        $columns = intval( $columns );
        $itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
        $itemwidth = $itemwidth - ( $marginpercent * 2 ); //remove margin for right/left

        $float = is_rtl() ? 'right' : 'left';

        $galleria_style = "
            #$selector {
                width: 100%;
                height: 500px;
                background: #000;
            }
            .galleria-stage {
                position: absolute !important;
            }
            .galleria-fscr {
                width:20px;
                height:20px;
                position:absolute;
                bottom:15px;
                right:-3px;
                padding-left: 2px;
                background: black;
                z-index:4;
                cursor: pointer;
                opacity: .7;
                color: white;
            }

            .galleria-fscr:hover {
                opacity:1;
            }

            .galleria-lightbox-box {
                direction: ltr;
            }
        ";        

        $gallery_style = $gallery_div = '';        
        $gallery_style = "
            #{$selector} {
                position: relative;
                " . ( $layout != 'masonry' ? 'margin: auto;' : '' ) . "
            }

            #{$selector} .gallery-overlay {
                position: absolute;
                top: 0px;
                left: 0px;
                width: 100%;
                height: 100%;
                z-index: 50;
                text-align: center;
            }

            #{$selector} .gallery-overlay > img {
                padding: 50px;
            }

            #{$selector} .gallery-item {                
                opacity: 0;
                " . ( $layout != 'masonry' ? 'max-width: ' . $itemwidth . '%; text-align: center; float:' . $float . ';' : "" ) . "
                margin: 0 " . $marginpercent * 2 . "% " . $marginpercent * 2 . "% 0;
            }
            #{$selector} .gallery-caption {
                margin-left: 0;
            }
            " . ( $type == 'galleria' ? $galleria_style : "" ) . "
            " . ( isset( $masonry_style ) ? $masonry_style : "" ) . "
        ";

        $size_class = sanitize_html_class( $size );
        $gallery_div = "<div id='$selector' class='intense gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>
                        <div class='gallery-overlay'><img src='" . INTENSE_PLUGIN_URL . "/assets/img/loader.gif' /></div>";
        $output = $gallery_div;

        $init_script = " 
        var isOldIE = $('#intense-browser-check').hasClass('oldie');
        $('#{$selector} .gallery-overlay').remove();";

        // Add CSS/JS for different gallery types and add JavaScript setup code
        if ( $effeckt != '' ) {
            intense_add_style( 'effeckt_caption' );
        } else {
            if ( $show_tooltip == 1 && $type != 'galleria' ) {
                intense_add_style( 'qtip2' );
                intense_add_script( 'qtip2' );
                intense_add_script( 'imagesloaded' );
                $init_script .= "                
                    $('.$grouping').qtip({
                        position: {
                            at: 'top center',
                            my: 'bottom center',
                            target: 'mouse',
                            " . ( current_user_can( 'manage_options' ) ? 'adjust: { x: 0, y: -32 },' : '' ) . "
                        },
                        show: { delay: 1500 }               
                    });
                $('.$grouping img').removeAttr('title');";               
            }
        }

        $lightbox = null;

        switch ( $type ) {
            case 'standard':
                break;
            case "photoswipe":
            case "thickbox":                
            case 'colorbox':
            case 'magnificpopup':                
            case 'prettyphoto':                
            case 'swipebox':
            case 'touchtouch':                
                $lightbox = Intense_Lightboxes::create( $type, '#' . $selector . ' a' );
                $init_script .= $lightbox->get_image_script();
                $gallery_style .= $lightbox->get_css();
                break;
            case "galleria":
                intense_add_script( 'galleria' );
                intense_add_script( 'galleriatheme' );
                intense_add_style( 'galleria' );

                $init_script .= "Galleria.configure({
                                lightbox: " . ( $intense_visions_options['intense_galleria_lightbox'] != null ? $intense_visions_options['intense_galleria_lightbox'] : "true" ) . ",
                                transition: '" . ( $intense_visions_options['intense_galleria_transition'] != null ? $intense_visions_options['intense_galleria_transition'] : "fade" ) . "',
                                trueFullScreen: true,
                                fullscreenDoubleTap: true,
                                autoplay: " . ( $intense_visions_options['intense_galleria_slideshow'] != null && $intense_visions_options['intense_galleria_slideshow'] ? "5000" : "false" ) . ",
                            });

                            Galleria.run('#$selector');

                            Galleria.ready(function() {
                                var gallery = this;
                                this.addElement('fscr');
                                this.appendChild('stage','fscr');
                                var fscr = this.$('fscr').append(\"" . intense_run_shortcode( 'intense_icon', array( 'type' => 'fullscreen' ) ) . "\")
                                    .click(function() {
                                        gallery.toggleFullscreen();
                                    });
                                this.addIdleState(this.get('fscr'), { opacity:0 });
                            });";
                $size = 'medium640';
                break;
            case "supersized":
                intense_add_script( 'easing' );
                intense_add_script( 'supersized_js' );
                intense_add_script( 'supersized_shutter_js' );

                intense_add_style( 'supersized_style' );
                intense_add_style( 'supersized_shutter_style' );
                intense_add_style( 'supersized_theme' );

                $additionalscript .= " $('#supersizedcontrols').show();
                $.supersized({

                    //Functionality
                    slideshow               :   " . ( $intense_visions_options['intense_supersized_slideshow'] != null ? $intense_visions_options['intense_supersized_slideshow'] : 1 ) . ", //Slideshow on/off
                    autoplay                :   " . ( $intense_visions_options['intense_supersized_autoplay'] != null ? $intense_visions_options['intense_supersized_autoplay'] : 1 ) . ", //Slideshow starts playing automatically
                    start_slide             :   1,      //Start slide (0 is random)
                    slide_interval          :   5000,   //Length between transitions
                    transition              :   " . ( $intense_visions_options['intense_supersized_slideshow_transition'] != null ? $intense_visions_options['intense_supersized_slideshow_transition'] : 1 ) . ", //0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                    transition_speed        :   1000,    //Speed of transition
                    new_window              :   1,      //Image links open in new window/tab
                    pause_hover             :   " . ( $intense_visions_options['intense_supersized_pause_hover'] != null ? $intense_visions_options['intense_supersized_pause_hover'] : 0 ) . ", //Pause slideshow on hover
                    keyboard_nav            :   " . ( $intense_visions_options['intense_supersized_keyboard_nav'] != null ? $intense_visions_options['intense_supersized_keyboard_nav'] : 1 ) . ", //Keyboard navigation on/off
                    performance             :   1,      //0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                    image_protect           :   " . ( $intense_visions_options['intense_supersized_image_protect'] != null ? $intense_visions_options['intense_supersized_image_protect'] : 1 ) . ",      //Disables image dragging and right click with Javascript
                    //image_path              :   '" . get_stylesheet_directory_uri() . "/supersized/img/', //Default image path
                    stop_loop               :   " . ( $intense_visions_options['intense_supersized_stop_loop'] != null ? $intense_visions_options['intense_supersized_stop_loop'] : 0 ) . ",      //Pauses slideshow upon reaching the last slide.

                    //Size & Position
                    min_width               :   0,      //Min width allowed (in pixels)
                    min_height              :   0,      //Min height allowed (in pixels)
                    vertical_center         :   1,      //Vertically center background
                    horizontal_center       :   1,      //Horizontally center background
                    fit_portrait            :   1,      //Portrait images will not exceed browser height
                    fit_landscape           :   0,      //Landscape images will not exceed browser width

                    //Components
                    navigation              :   " . ( $intense_visions_options['intense_supersized_slideshow_navigation'] != null ? $intense_visions_options['intense_supersized_slideshow_navigation'] : 1 ) . ", //Slideshow controls on/off
                    thumbnail_navigation    :   " . ( $intense_visions_options['intense_supersized_thumbnail_navigation'] != null ? $intense_visions_options['intense_supersized_thumbnail_navigation'] : 0 ) . ",      //Thumbnail navigation
                    slide_counter           :   " . ( $intense_visions_options['intense_supersized_slide_counter'] != null ? $intense_visions_options['intense_supersized_slide_counter'] : 1 ) . ", //Display slide numbers
                    slide_captions          :   " . ( $intense_visions_options['intense_supersized_slide_captions'] != null ? $intense_visions_options['intense_supersized_slide_captions'] : 1 ) . ", //Slide caption (Pull from title in slides array)
                    progress_bar            :   " . ( $intense_visions_options['intense_supersized_progress_bar'] != null ? $intense_visions_options['intense_supersized_progress_bar'] : 1 ) . ",
                    mouse_scrub             :   0,
                    thumbnail_links         :   " . ( $intense_visions_options['intense_supersized_thumbnail_links'] != null ? $intense_visions_options['intense_supersized_thumbnail_links'] : 2 ) . ",
                    slides                  :   [       ";
                break;
            default:
                break;
        }

        // Add JS/CSS for Adipoli
        if ( $starteffect != '' && $hovereffect != '' ) {
            intense_add_style( 'adipoli' );
            intense_add_script( 'adipoli' );
            $init_script .= "
            if (!isOldIE) {
                $('.$grouping img').adipoli({
                    'startEffect' : '$starteffect',
                    'hoverEffect' : '$hovereffect'
                });
            }";
        }

        switch ( $layout ) {
            case 'circle':
                $init_script .= "$('.$grouping img').addClass('img-circle');";
                break;
            case 'masonry':
                $masonry_shortcode = '[intense_masonry gutter="' . $gutter . '"]';                
                $show_loop_method = "each";
                break;
            default:
                break;
        }

        $init_script .= "
        $('#{$selector} .gallery-item')." . $show_loop_method  . "(function() {
            var el = $(this);
            var delay = Math.floor((Math.random() * " . $maximum_show_delay . ")+1);

            setTimeout(function() {
                el.addClass('animated').addClass('" . $show_animation . "');
            }, delay);
        });";

        $photo_source = new Intense_Photo_Source( $source, $attachments, $user, ( $groupid ? $groupid : $tag ), ( $setid ? $setid : $album ), $facebookpage );
        $photos = $photo_source->get_photos( $pagesize, $page );

        $wordpress_size = $photo_source->get_wordpress_size( $size );

        if ( !empty( $alternate_size ) ) {            
            $wordpress_alt_size = $photo_source->get_wordpress_size( $alternate_size );
        } else {
            $wordpress_alt_size = $wordpress_size;
        }

        $wordpress_full_size = $photo_source->get_wordpress_size( ( isset( $intense_visions_options['intense_photosource_full_size'] ) ? $intense_visions_options['intense_photosource_full_size'] : 'large' ) );

        if ( ! isset( $photos ) ) {
            echo "<div class='alert'>Unable to load photos</div>";
        }

        if ( $effeckt != '' && isset( $effeckt_opacity ) && isset( $effeckt_color ) ) {
            $hex = str_replace( "#", "", $effeckt_color );
            $a = $effeckt_opacity / 100;

            if ( strlen( $hex ) == 3 ) {
                $r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
                $g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
                $b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
            } else {
                $r = hexdec( substr( $hex, 0, 2 ) );
                $g = hexdec( substr( $hex, 2, 2 ) );
                $b = hexdec( substr( $hex, 4, 2 ) );
            }

            $effeckt_style = "background: rgba(" . $r . "," . $g . "," . $b . "," . $a . ") !important; rgb(" . $r . "," . $g . "," . $b . ")\9 !important;";
        } else {
            $effeckt_style = "";
        }

        $radius = '';
        if ( isset( $border_radius ) && $type == 'standard' ) {
            $radius = ' border-radius:' . $border_radius . ';';
        }

        $i = 0;
        foreach ( $photos as $photo ) { 
            $photo_markup = '';
            $photo_script = '';            
            $alternate = ( !empty( $masonry_alternate_count ) && $i % $masonry_alternate_count == 0 );

            if ( $alternate ) {                
                $requested_size = $photo->get_closest_size( $wordpress_alt_size["width"], $wordpress_alt_size["height"] );
            } else {
                $requested_size = $photo->get_closest_size( $wordpress_size["width"], $wordpress_size["height"] );   
            }          

            $original_size = $photo->get_closest_size( $wordpress_full_size["width"], $wordpress_full_size["height"] );

            $title = $photo->title;
            $description = $photo->description;
            $link = $photo->link;
            $original_photo_url = $original_size['sourceurl'];
            $sized_photo_url = $requested_size['sourceurl'];
            $sized_photo_markup = "<img class='intense' style='$radius' src='$sized_photo_url' title='" . ( $show_tooltip == 1 ? esc_attr( $title ) : '' ) . "' alt='" . esc_attr( $description ) . "' />";
            $effeckt_photo_markup = "<figure class='effeckt-caption preload' data-effeckt-type='effeckt-caption-" . $effeckt . "'>" . $sized_photo_markup .
                "<figcaption style='" . $effeckt_style . "'><div class='effeckt-figcaption-wrap'><h3>" . esc_html( $title ) . "</h3><p>" . esc_html( $description ) . "</p></div></figcaption></figure>";

            switch ( $type ) {
                case 'standard':
                    $photo_markup .= "<a class='$type $grouping gallery-item' title='" . ( $show_tooltip == 1 ? esc_attr( $title ) : '' ) . "' data-content='" . esc_attr( $description ) . "' href='" . $link . "'>" .
                        ( $effeckt != '' ? $effeckt_photo_markup : $sized_photo_markup ) . "</a>";
                    break;
                case 'colorbox':
                case 'magnificpopup':
                case 'photoswipe':
                case 'prettyphoto':
                case 'swipebox':
                case 'thickbox':
                case 'touchtouch':
                    $photo_markup .= $lightbox->get_image( $grouping, $title, $description, $original_photo_url, ( $effeckt != '' ? $effeckt_photo_markup : $sized_photo_markup ) );
                    break;
                case 'galleria':
                    $photo_markup .= "<img src='" . $sized_photo_url . "' data-big='" . $original_photo_url . "'' data-title='" . esc_attr( $title ) . "' data-description='" . esc_attr( $description ) . "' style='display: none;' />";
                    break;
                case 'supersized':
                    $photo_script .= "{image:'" . $original_photo_url . "',title:" . json_encode( wptexturize( $title ) ) . ",caption:" . json_encode( wptexturize( $description ) ) . ",url:'" . $link . "'},";
                    break;
                default:
                    break;
            }

            switch ( $layout ) {
                case 'masonry':
                    if ( $alternate ) { 
                        $masonry_shortcode .= '[intense_masonry_item width="' . $masonry_alternate_width . '"]';
                    } else {
                        $masonry_shortcode .= '[intense_masonry_item width="' . $masonry_item_width . '"]';
                    }
                    $masonry_shortcode .= $photo_markup;
                    $masonry_shortcode .= '[/intense_masonry_item]';
                    break;
                default:
                    $output .= $photo_markup;
                    $additionalscript .= $photo_script;
                    
                    if ( $columns > 0 && $i > 0 && ($i + 1) % $columns == 0 )
                        $output .= "<div class='clearfix'></div>";
                    break;
            }

            $i++;
        }

        if ( $effeckt != '' ) {
            $additionalscript .= ' $(".preload").removeClass("preload"); ';
            $additionalscript .= ' if ($("#intense-browser-check").hasClass("ie8")) {
                $(".effeckt-caption").find("figcaption").fadeTo("fast", 0);
                $(".effeckt-caption").hover(
                    function() { $(this).find("figcaption").fadeTo("fast", ' . $effeckt_opacity / 100 . '); },
                    function() { $(this).find("figcaption").fadeTo("fast", 0); } );
            } ';
        }

        if ( $additionalscript ) {
            switch ( $type ) {
                case 'supersized':
                    $additionalscript = rtrim( $additionalscript, "," );
                    $additionalscript .= "]});";
                    break;
                default:
                    break;
            }

            $init_script .= $additionalscript;
        }


        switch ( $layout ) {
            case 'masonry':
                $masonry_shortcode .= '[/intense_masonry]';
                $output .= do_shortcode( $masonry_shortcode );
                break;
            default:
                $output .= "<div class='clearfix'></div>";
                break;
        }

        $output .= "</div>\n";
        $output .= '<script> jQuery(function($){ $(window).load(function() { ' . $init_script . ' }); });</script>';
        $output .= '<style type="text/css">' . $gallery_style . '</style>';

        if ( $type == 'supersized' && $supersized_position == 'Top Left' ) {
            $output .= '<div id="supersizedcontrols" class="top">';
            $output .= $this->get_supersized_controls();
            $output .= '</div>';
        } else if ( $type == 'supersized' && $supersized_position == 'Bottom Left' ) {
            $output .= '<div id="supersizedcontrols" class="bottom">';
            $output .= $this->get_supersized_controls();
            $output .= '</div>';
        }

        return $output;    
    }

    function render_dialog_script() {
        $ajax_nonce = wp_create_nonce("intense-plugin-noonce"); 
?>
        <div style="padding: 10px 30px 0 30px; margin: 0 -30px; overflow: auto; overflow-y: hidden; min-height: 100px; background: #dedede; border-top: 1px solid #ccc;">
            <div id="images">
                
            </div>
        </div>
        <script>
                jQuery(function($) {                    
                    $('.masonrysettings').hide();
                    $('.marginpercentsettings').show();

                    $('#layout').change(function() {
                        var layout = $(this).val();

                        $('.columnssettings').hide();
                        $('.sizesettings').hide();                       
                        $('.masonrysettings').hide();
                        $('.marginpercentsettings').show();

                        switch (layout) {
                            case '':
                                $('.columnssettings').show();
                                $('.sizesettings').show();
                                break;
                            case 'square':                          
                                break;
                            case 'smallsquare':
                                break;
                            case 'circle':
                                break;
                            case 'masonry':
                                $('.masonrysettings').show();
                                $('.sizesettings').show();
                                $('.marginpercentsettings').hide();
                                break;
                        }                        
                    });

                    $(".flickrsettings").hide();
                    $(".facebooksettings").hide();
                    $(".instagramsettings").hide();
                    $(".smugmugsettings").hide();
                    $(".zenfoliosettings").hide();
                    $(".deviantartsettings").hide();
                    $(".500pxsettings").hide();
                    $(".googleplussettings").hide();

                    $("#source").change(function() {
                        var source = $(this).val();

                        $('#images').html("");
                        $(".wpsettings").hide();
                        $(".flickrsettings").hide();
                        $(".facebooksettings").hide();
                        $(".instagramsettings").hide();
                        $(".smugmugsettings").hide();
                        $(".zenfoliosettings").hide();
                        $(".deviantartsettings").hide();
                        $(".500pxsettings").hide();
                        $(".googleplussettings").hide();                

                        switch(source) {
                            case "wordpress":
                                $(".wpsettings").show();
                                break;
                            case "flickr":
                                $(".flickrsettings").show();
                                break;
                            case "facebook":
                                $(".facebooksettings").show();
                                break;
                            case "instagram":
                                $(".instagramsettings").show();
                                break;
                            case "smugmug":
                                $(".smugmugsettings").show();
                                break;
                            case "zenfolio":
                                $(".zenfoliosettings").show();
                                break;
                            case "deviantart":
                                $(".deviantartsettings").show();
                                break;
                            case "500px":
                                $(".500pxsettings").show()
                                break;
                            case "google+":
                                $(".googleplussettings").show();
                                break;
                        }
                    });

                    $(".grouping").hide();
                    $(".supersizedsettings").hide();

                    $("#type").change(function() {
                        var gallerytype = $(this).val();

                        $(".grouping").hide();
                        $('.radiussettings').hide();
                        $('.supersizedsettings').hide();

                        switch(gallerytype) {
                            case "colorbox":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "galleria":
                                break;
                            case "photoswipe":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "prettyphoto":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "standard":
                                $('.radiussettings').show();
                                break;
                            case "supersized":
                                $('.supersizedsettings').show();
                                break;
                            case "swipebox":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "thickbox":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "touchtouch":
                                $(".grouping").show();
                                $('.radiussettings').show();
                                break;
                            case "":
                                $(".radiussettings").show()
                                break;                  
                        }
                    });

                    $(".adipoli-settings").hide();
                    $(".effeckt-settings").hide();

                    $("#hover_effect").change(function() {
                        var hovertype = $(this).val();
                        
                        $(".adipoli-settings").hide();
                        $(".effeckt-settings").hide();

                        switch(hovertype) {
                            case "adipoli":
                                $(".adipoli-settings").show();
                                break;
                            case "effeckt":
                                $(".effeckt-settings").show();
                                break;
                        }
                    });

                    var searchString = window.location.search;
                    var matches = searchString.match(/post=([^&]+)/);
                    
                    if (matches && matches.length > 0) {
                        var id = matches[1];

                        $("#id").val(id);
                    }
                    
                    $('.upload_gallery_button').live('click', function( event ){
                        event.preventDefault();

                        var target = $(this).data('target-id');
                
                        window.parent.loadGalleryFrame($(this), function(attachments) {

                            $.each(attachments, function (i, attachment) {
                                if (i == 0) { 
                                    jQuery("#" + target).val('');
                                }

                                var ids = jQuery("#" + target).val() + ',' + attachment.id;

                                if (jQuery("#" + target).val().length == 0) {
                                    ids = attachment.id;
                                }

                                jQuery("#" + target).val(ids);
                            });

                            setThumbnails();
                        });
                    });                    

                    $("#id").change(setThumbnails);
                    $("#order").change(setThumbnails);
                    $("#orderby").change(setThumbnails);
                    $("#include").change(setThumbnails);
                    $("#exclude").change(setThumbnails);
                    $("#user").change(setThumbnails);
                    $("#source").change(setThumbnails);
                    $("#groupid").change(setThumbnails);
                    $("#facebookpage").change(setThumbnails);
                    $("#tag").change(setThumbnails);
                    $("#setid").change(setThumbnails);
                    $("#album").change(setThumbnails);
                    $("#pagesize").change(setThumbnails);
                    $("#page").change(setThumbnails);

                    function setThumbnails() {
                        $("#images").html("Loading...");

                        $.ajax({
                            type: "POST",
                            url: ajaxurl + '?action=intense_gallery_preview',
                            data: {
                                security: '<?php echo $ajax_nonce; ?>',
                                source: $("#source").val(),
                                postid: $("#id").val(),
                                order: $("#order").val(),
                                orderby: $("#orderby").val(),
                                ids: $("#include").val(),
                                excludedids: $("#exclude").val(),
                                user: $("#user").val(),
                                group: $("#groupid").val(),
                                facebookpage: $("#facebookpage").val(),
                                tag: $("#tag").val(),
                                set: $("#setid").val(),
                                album: $("#album").val(),
                                pagesize: $("#pagesize").val(),
                                page: $("#page").val()
                            }
                        }).done(function (msg) {
                            $("#images").width(0);
                            $("#images").html(msg);
                            var newWidth = 0;

                            $("#images > img").css('visibility', 'hidden');

                            $("#images > img").load(function() {
                                $(this).css('visibility', 'visible');
                                newWidth += $(this).outerWidth() * 1.015;
                                $("#images").width(newWidth < 300 ? 300 : newWidth);
                            });
                        });
                    }
                });
            </script>
        <?php
    }

    function get_supersized_controls() {
        global $intense_visions_options;

        $control_color = $intense_visions_options['intense_supersized_color'];
        $control_font_color = intense_get_contract_color( $control_color );
        $navigation_color = intense_get_plugin_color( "primary" );

        // darken the font a little so it isn't as blaring
        // against dark colors
        if ( $control_font_color == 'white' ) {
            $control_font_color = "#ccc";
        }

        return '
        <style>
            #controlskew, #titleskew, #captionskew {
                background: ' . $control_color . ' !important;
            }

            #captionskew, #slidecaption, #slidecounter, #slideDescription {
                color: ' . $control_font_color . ' !important;
            }

            #colorskew {
                    background-color: ' . $navigation_color . ' !important;
            }
            #pauseplay, #prevslide, #nextslide {
                color: ' . $navigation_color . ' !important;
            }
            #tray-button {
                color: ' . $navigation_color . ' !important;
            }
        </style>
        <!-- Start of Supersized Navigation Controls -->
        <!--Supersized Thumbnail Navigation-->
        <div id="prevthumb"></div> <div id="nextthumb"></div>

        <div id="controlskew"></div>
        <div id="colorskew"></div>
        <div id="titleskew"></div>
        <div id="captionskew"></div>
        <div id="slideDescription"></div>

        <a href="javascript: void(0)" id="prevslide" title="Slide Left">' . intense_run_shortcode( 'intense_icon', array( 'type' => 'angle-left' ) ) . '</a>
        <a href="javascript: void(0)" id="nextslide" title="Slide Right">' . intense_run_shortcode( 'intense_icon', array( 'type' => 'angle-right' ) ) . '</a>
        <a id="pauseplay">' . intense_run_shortcode( 'intense_icon', array( 'type' => 'pause' ) ) . '</i></a>
        <a id="tray-button" title="Show/Hide thumbnails">' . intense_run_shortcode( 'intense_icon', array( 'type' => 'circle-arrow-up' ) ) . '</a>
        <a id="fullscreen-button" title="Toggle Full Screen">' . intense_run_shortcode( 'intense_icon', array( 'type' => 'fullscreen' ) ) . '</a>

        <div id="thumb-tray" class="load-item">
            <div id="thumb-back"></div>
            <div id="thumb-forward"></div>
        </div>

        <!--Time Bar-->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>

        <!--Control Bar-->
        <div id="controls-wrapper">
            <div id="controls">

                <!--Slide counter-->
                <div id="slidecounter">
                    <span class="slidenumber"></span> / <span class="totalslides"></span>
                </div>

                <!--Slide captions displayed here-->
                <div id="slidecaption"></div>

                <!--Navigation-->
                <!-- <ul id="slide-list"></ul> -->
            </div>
        </div>
        <!-- End of Supersized -->';
    }
}
