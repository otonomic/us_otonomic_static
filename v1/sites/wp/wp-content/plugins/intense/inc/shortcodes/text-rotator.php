<?php

add_shortcode( 'intense_text_rotator', 'intense_text_rotator' );

function intense_text_rotator( $atts, $content = null ) {
	extract( shortcode_atts( array(
				'background_color' => '',
        		'color' => '',
                'animation' => 'fade', //Options are dissolve, fade (default), flip, flipUp, flipCube, flipCubeUp and spin.
                'speed' => '2000' // How many milliseconds until the next word show.
			), $atts ) );

    intense_add_script( 'intense_text_rotator' );
    intense_add_style( 'intense_text_rotator' );

    $random = rand();

    $color = intense_get_plugin_color( $color );

    $background_color = intense_get_plugin_color( $background_color );

    if ( $color == '' && $background_color != '' ) {
        $color = intense_get_contract_color( $background_color );
    }

	$output .= "<span id='txt-rotate_" . $random . "' class='rotate' style='display: none; " 
        . ( isset( $color ) ? "color: " . $color . "; " : "") 
        . ( isset( $background_color ) ? "background_color: " . $background_color . "; " : "") 
        . "'>" . $content . "</span>";

    $output .= '<script>    
        jQuery(function($){
            $("#txt-rotate_' . $random . '").show();
            $("#txt-rotate_' . $random . '").textrotator({
              animation: "' . $animation . '", 
              separator: ",", 
              speed: ' . $speed . ' 
            });
        });
        </script>';

    return $output;
}
