<?php

class Intense_Metadata extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;
		global $post;

		$this->title = __( 'Post Meta Data', 'intense' );
		$this->category = __( 'Posts', 'intense' );
		$this->icon = 'dashicons-info';
		$this->show_preview = true;

		$this->fields = array(
			'post' => array(
				'type' => 'text',
				'title' => __( 'Post ID', 'intense' ),
				'description' => __( 'if left blank, the current post will be used. The preview may not function unless specified.', 'intense' ),
				'default' => ( isset( $post ) ? $post->ID : null ),
				'skinnable' => '0'
			),
			'meta_key' => array(
				'type' => 'text',
				'title' => __( 'Key', 'intense' ),				
				'description' => __( 'if you are unsure which keys are available, enter a post id and preview to see a list of keys', 'intense' ),
			),	
			'fallback' => array(
				'type' => 'text',
				'title' => __( 'Fallback text', 'intense' ),
				'description' => __( 'text to show if there isn\'t a value', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( empty( $post ) ) {
			$post = get_the_ID();
		}

		if ( empty( $post ) ) {
			return "Error: a post must be specified";
		}

		$data = get_post_meta( $post, $meta_key, true );

		if ( empty($meta_key) ) {
			return "Error: a key must be specified. Use one of the following: <hr>" . implode( '<br>', array_filter( array_keys( $data ), array( __CLASS__, 'filter_intense_keys' ) ) );
		}

		if ( empty( $data ) ) {
			$data = $fallback;
		}

		return $data;
	}

	function filter_intense_keys( $var ) {
		return substr($var, 0, 8) !== '_intense';
	}
}
