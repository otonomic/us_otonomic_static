<?php

class Intense_Slider extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Slider', 'intense' );
        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-slides';
        $this->show_preview = false;
        $this->is_container = true;

        $this->fields = array(
            'slide' => array(
                'type' => 'repeater',
                'title' => __( 'Slides', 'intense' ),
                'preview_content' => __( 'Slide', 'intense' ),
                'is_container' => 'true',
                'fields' => array(
                    'link' => array(
                        'type' => 'text',
                        'title' => __( 'Link', 'intense' ),
                        'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'title' => array(
                        'type' => 'text',
                        'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
                    ),
                )
            ),
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(                    
                    'type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Type', 'intense' ),
                        'default' => 'flexslider',
                        'options' => array(
                            'flexslider' => __( 'Flex Slider 2', 'intense' ),
                            'owl-carousel' => __( 'Owl Carousel', 'intense' ),
                            'basic' => __( 'Basic', 'intense' ),
                        ),          
                    ),
                    'speed' => array(
                        'type' => 'text',
                        'title' => __( 'Speed', 'intense' ),
                        'description' => __( 'measured in milliseconds', 'intense' ),
                        'default' => '7000'
                    ),
                    'transition' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Transition', 'intense' ),
                        'default' => 'slide',
                        'options' => array(
                            'fade' => __( 'Fade', 'intense' ),
                            'slide' => __( 'Slide', 'intense' ), 
                            'backSlide' => __( 'backSlide', 'intense' ),
                            'goDown' => __( 'goDown', 'intense' ),                   
                            'goDown' => __( 'scaleUp', 'intense' ),
                        ),   
                        'class' => 'flexslidersettings'       
                    ),
                    'id' => array(
                        'type' => 'text',
                        'title' => __( 'ID', 'intense' ),
                        'description' => __( 'A unique id for the slider (optional)', 'intense' ),
                        'default' => '',
                        'skinnable' => '0'
                    ),
                    'pause_on_hover' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Pause on Hover', 'intense' ),
                        'default' => "1",
                    ),
                    'shadow' => array(
                        'type' => 'shadow',
                        'title' => __( 'Shadow', 'intense' ),
                    ),
                )
            ),
            'owlslider_tab' => array(
                'type' => 'tab',
                'title' => __( 'Owl Carousel', 'intense' ),
                'fields' =>  array(
                    'items' => array(
                        'type' => 'text',
                        'title' => __( 'Max Items', 'intense' ),
                        'default' => '5',
                        'class' => 'owlslidersettings'
                    ),
                    'items_desktop' => array(
                        'type' => 'text',
                        'title' => __( 'Items Desktop', 'intense' ),
                        'description' => __( 'window width <= 1199', 'intense' ),
                        'default' => '4',
                        'class' => 'owlslidersettings'
                    ),
                    'items_desktop_small' => array(
                        'type' => 'text',
                        'title' => __( 'Items Desktop Small', 'intense' ),
                        'description' => __( 'window width <= 979', 'intense' ),
                        'default' => '3',
                        'class' => 'owlslidersettings'
                    ),
                    'items_tablet' => array(
                        'type' => 'text',
                        'title' => __( 'Items Tablet', 'intense' ),
                        'description' => __( 'window width <= 768', 'intense' ),
                        'default' => '2',
                        'class' => 'owlslidersettings'
                    ),
                    'items_mobile' => array(
                        'type' => 'text',
                        'title' => __( 'Items Mobile', 'intense' ),
                        'description' => __( 'window width <= 479', 'intense' ),
                        'default' => '1',
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_position' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Position', 'intense' ),
                        'default' => 'middleinside',
                        'options' => array(
                            '' => '',
                            'topleft' => __( 'Top Left', 'intense' ),
                            'topcenter' => __( 'Top Center', 'intense' ),
                            'topright' => __( 'Top Right', 'intense' ),
                            'middleinside' => __( 'Middle Inside', 'intense' ),
                            'middleoutside' => __( 'Middle Outside', 'intense' ),
                            'bottomleft' => __( 'Bottom Left', 'intense' ),
                            'bottomcenter' => __( 'Bottom Center', 'intense' ),
                            'bottomright' => __( 'Bottom Right', 'intense' ),
                        ),
                        'class' => 'owlslidersettings'        
                    ),
                    'navigation_prev_icon' => array(
                        'type' => 'icon',
                        'title' => __( 'Navigation Previous Icon', 'intense' ),
                        'default' => 'chevron-left',
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_next_icon' => array(
                        'type' => 'icon',
                        'title' => __( 'Navigation Next Icon', 'intense' ),
                        'default' => 'chevron-right',
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_size' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Size', 'intense' ),
                        'default' => 'default',
                        'options' => array(
                            'default' => __( 'default', 'intense' ),
                            'mini' => __( 'mini', 'intense' ),
                            'small' => __( 'small', 'intense' ),
                            'medium' => __( 'medium', 'intense' ),
                            'large' => __( 'large', 'intense' )
                        )
                    ),
                    'navigation_prev_text' => array(
                        'type' => 'text',
                        'title' => __( 'Navigation Previous Text', 'intense' ),
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_next_text' => array(
                        'type' => 'text',
                        'title' => __( 'Navigation Next Text', 'intense' ),
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Navigation Color', 'intense' ),
                        'default' => '#111111',
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_font_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Navigation Font Color', 'intense' ),
                        'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                        'class' => 'owlslidersettings'
                    ),
                    'navigation_prev_border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Navigation Previous Border Radius', 'intense' ),
                        'class' => 'owlslidersettings'                
                    ),
                    'navigation_next_border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Navigation Next Border Radius', 'intense' ),
                        'class' => 'owlslidersettings'                
                    ),
                    'navigation_pagination_type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Pagination Type', 'intense' ),
                        'default' => 'bullets',
                        'options' => array(
                            'none' => __( 'None', 'intense' ),
                            'bullets' => __( 'Bullets', 'intense' ),
                            'numbers' => __( 'Numbers', 'intense' ),
                        ),
                        'class' => 'owlslidersettings'         
                    ),
                )
            ),
            'flexslider_tab' => array(
                'type' => 'tab',
                'title' => __( 'Flex Slider 2', 'intense' ),
                'fields' =>  array(                    
                    'item_width' => array(
                        'type' => 'text',
                        'title' => __( 'Item Width', 'intense' ),
                        'class' => 'flexslidersettings'
                    ),
                )
            ),  
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        global $intense_slider_type;

        $intense_slider_type = $type;
        $wrapper_start = "";
        $wrapper_end = "";
        $shadow_style = "";
        $controls = "";
        $controls_before = true;
        $slider_id = 'slider_' . rand();
        $random = rand();

        if ( !empty( $id ) && $id != '' ) {
            $slider_id = $id;
            $random = $id;
        } 

        $output = "<script> jQuery(function($){";

        $slider = null;

        switch ( $type ) {
        case 'owl-carousel':
            intense_add_style( 'owlcarousel' );
            intense_add_style( 'owlcarousel-theme' );            
            intense_add_script( 'owlcarousel' );

            if ( $transition != 'slide' && !empty( $transition ) ) {
                intense_add_style( 'owlcarousel-transitions' );                
            }

            $navigation_color = intense_get_plugin_color( $navigation_color );
            $navigation_font_color = intense_get_plugin_color( $navigation_font_color );

            $prev = '<span class="prev">' . intense_run_shortcode( 'intense_button', array(
                'size' => $navigation_size,
                'color' => $navigation_color,
                'font_color' => $navigation_font_color,
                'border_radius' => $navigation_prev_border_radius,
                'icon' => $navigation_prev_icon,
                'link' => 'javascript: void(0)'
            ), $navigation_prev_text ) . '</span>';
            $next = '<span class="next">' . intense_run_shortcode( 'intense_button', array(
                'size' => $navigation_size,
                'color' => $navigation_color,
                'font_color' => $navigation_font_color,
                'border_radius' => $navigation_next_border_radius,
                'icon' => $navigation_next_icon,
                'link' => 'javascript: void(0)',
                'icon_position' => 'right'
            ), $navigation_next_text ) . '</span>';                      

            $title = '';            
            
            $navigation_style = "
                #slider_controls_$random .prev, #slider_controls_$random .next {
                    cursor: pointer;
                }

                #slider_controls_$random {               
                    z-index: 500;                
                }
                #slider_controls_$random .slider-controls {
                    top: 0;
                }
                #$slider_id .owl-controls .owl-page span {
                    background-color: $navigation_color;
                }

                #$slider_id .owl-numbers {
                    color: " . ( empty( $navigation_font_color ) ? intense_get_contract_color( $navigation_color ) : $navigation_font_color ) . ";
                }
            ";

            $controls = '<div id="slider_controls_' . $random . '" style="position: relative;">' . $title  . '<div class="slider-controls">' . $prev . " " . $next . '</div><div class="clearfix"></div></div>';

            $navigation_script = '';

            switch ( $navigation_position ) {
                case 'topleft':
                    $navigation_style .= "#slider_controls_$random .slider-controls { float: left; padding: 0 20px 10px 0; }";
                    break;                
                case 'topcenter':
                    $navigation_style .= "#slider_controls_$random .slider-controls { margin: 0px auto; text-align:center; padding: 0 20px 10px 20px; }";
                    break;
                case 'topright':
                    $navigation_style .= "#slider_controls_$random .slider-controls { float: right; padding: 0 0 10px 20px; }";
                    break;
                case 'middleinside':
                    $navigation_style .= "#slider_controls_$random .slider-controls .prev { left: 0; position: absolute; display: none; opacity: .65; }";
                    $navigation_style .= "#slider_controls_$random .slider-controls .next { right: 0; position: absolute; display: none; opacity: .65; }";
                    $navigation_style .= "#slider_controls_$random .slider-controls .prev:hover { opacity: 1; }";
                    $navigation_style .= "#slider_controls_$random .slider-controls .next:hover { opacity: 1; }";
                    $navigation_script = "                    
                        $('.owl-wrapper-outer, #slider_controls_$random .next, #slider_controls_$random .prev').mouseover(function() {                    
                            $('#slider_controls_$random .next').show();
                            $('#slider_controls_$random .prev').show();
                        }).mouseout(function() {                   
                            $('#slider_controls_$random .next').hide();
                            $('#slider_controls_$random .prev').hide();
                        });
                    ";
                    break;
                case 'middleoutside':
                    $navigation_style .= "#slider_controls_$random .slider-controls .prev { left: 0px; position: absolute; }";
                    $navigation_style .= "#slider_controls_$random .slider-controls .next { right: 0px; position: absolute; }";

                    //wrap the slider in a div and give that div the same amount of padding
                    //that is required to fit the prev and next buttons on both sizes
                    $output .= " 
                        var prevWidth = $('#slider_controls_$random .prev').outerWidth() + 6;
                        var nextWidth = $('#slider_controls_$random .next').outerWidth() + 6 ;  
                        var sliderwrapper = $('<div>');
                        
                        $('#$slider_id').wrap(sliderwrapper);
                        var container = $('#$slider_id').parent();
                        container.css('padding-left', prevWidth + 'px');
                        container.css('padding-right', nextWidth + 'px');
                        var shadow = $('.intense.shadow', container.parent().parent());

                        shadow.appendTo($('#$slider_id').parent());
                    ";
                    break;
                case 'bottomleft':
                    $navigation_style .= "#slider_controls_$random .slider-controls { left: 0; padding: 10px 20px 0 0; }";
                    $controls_before = false;
                    break;
                case 'bottomcenter':
                    $navigation_style .= "#slider_controls_$random .slider-controls { margin: 0px auto; text-align:center; padding: 10px 20px 0 20px; }";
                    $controls_before = false;
                    break;
                case 'bottomright':
                    $navigation_style .= "#slider_controls_$random .slider-controls { float: right; padding: 10px 0 0 20px; }";
                    $controls_before = false;
                    break;
                default:
                    $controls = '';
                    break;
            }     

            $transition = ($transition == 'slide' ? 'false' : "'$transition'" );

            $output .= "
                var owl_$random = $('#$slider_id').owlCarousel({                    
                    stopOnHover: " . ( $pause_on_hover ? 'true' : 'false' ) . ",                    
                    items: $items,
                    itemsDesktop: [1000,$items_desktop],
                    itemsDesktopSmall: [900,$items_desktop_small],
                    itemsTablet: [600,$items_tablet],
                    itemsMobile: [600,$items_mobile],
                    //autoHeight: true,
                    pagination: " . ( !empty( $navigation_pagination_type ) && $navigation_pagination_type == 'none' ? 'false' : 'true' ) . ",
                    paginationNumbers: " . ( $navigation_pagination_type === 'numbers' ? 'true' : 'false' ) . ",                    
                    afterAction: function() {
                        setTimeout(function() {
                            intenseAdjustOwlNavigation();
                        }, 500);
                    },
                    //scrollPerPage: true,
                    autoPlay: $speed,
                    slideSpeed: 1000,
                    transitionStyle: $transition
                });

                $('#slider_controls_$random .next').click(function(){
                    owl_$random.trigger('owl.next');
                });            

                $('#slider_controls_$random .prev').click(function(){
                    owl_$random.trigger('owl.prev');
                });

                $('#$slider_id').parent().find('.intense.shadow').css('margin-top', '-' + ($('#$slider_id .owl-controls').outerHeight(true)) + 'px');

                function intenseAdjustOwlNavigation() {
                    var prevWidth = $('#slider_controls_$random .prev').outerWidth() + 5;
                    var nextWidth = $('#slider_controls_$random .next').outerWidth() + 5 ;

                    $('#slider_controls_$random .next').css('margin-top', $('.owl-wrapper-outer', $('#$slider_id').parent()).height() / 2 - $('#slider_controls_$random .next').height() / 2);
                    $('#slider_controls_$random .prev').css('margin-top', $('.owl-wrapper-outer', $('#$slider_id').parent()).height() / 2 - $('#slider_controls_$random .prev').height() / 2);                              
                }                
                
                intenseAdjustOwlNavigation();

                $navigation_script
            ";   

            break;
        case 'flexslider':
            intense_add_style( 'flexslider' );
            intense_add_script( 'flexslider' );

            $output .= " $(document).ready(function() {";
            $output .= '    $(".intense.flexslider").hover(function(){
                                $(this).find(".flex-direction-nav a").css("opacity", ".8");
                            }, function(){
                                $(this).find(".flex-direction-nav a").css("opacity", "0");
                            });';
            $output .= " });";

            $output .= "$('#$slider_id').flexslider({
                        slideshowSpeed: " . $speed . ",
                        pauseOnHover: " . ( $pause_on_hover ? 'true' : 'false' ) . ",
                        prevText: '',
                        nextText: '',
                        animation: '$transition',"
                        . ( isset( $item_width ) ? "itemWidth: $item_width, itemMargin: 5, animationLoop: false, " : "" ) .
                        "//controlNav: 'thumbnails',
                        });";

            $wrapper_start = "<ul class='intense slides' $slider>";
            $wrapper_end = "</ul>";
            break;
        case 'basic':
            intense_add_script( 'intense.basicslider' );

            $slider = "data-basicslider='true'";

            $output .= " $(window).load(function() {";
            $output .= "    $('[data-basicslider=\"true\"]').basicslider({
                            speed: " . $speed . ",
                            pauseOnHover: " . ( $pause_on_hover ? 'true' : 'false' ) . "
                            });";
            $output .= " });";
            
            $wrapper_start = "<ul class='intense slides' $slider>";
            $wrapper_end = "</ul>";
            break;
        default:
            break;
        }

        $output .= "}); </script>";

        if ( isset( $navigation_style ) ) {
            $output .= "<style>" . $navigation_style . "</style>";
        }

        if ( $shadow ) {
            $output .= "<div><div style='margin-bottom: 0;'>";
        }

        if ( $controls_before ) {
            $output .= $controls;
        }

        $output .= "<div id='$slider_id' class='intense $type " . ( isset( $item_width ) ? "carousel" : "" ) . "' style='position: relative;'>" .             
            $wrapper_start . do_shortcode( $content ) . $wrapper_end . 
            "</div>";

        if ( $shadow ) {
            $output .= "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0; $shadow_style' alt='' /></div>";
        }

        if ( !$controls_before ) {
            $output .= $controls;
        }

        return $output;
    }

    
}

class Intense_Slide extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Slide', 'intense' );
        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-slides';
        $this->show_preview = true;       
        $this->hidden = true;
        $this->parent = 'intense_slider';
        $this->is_container = true;

        $this->fields = array(
            'link' => array(
                'type' => 'text',
                'title' => __( 'Link', 'intense' ),
                'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
            ),
            'title' => array(
                'type' => 'text',
                'title' => __( 'Title', 'intense' ),
                'skinnable' => '0'
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        global $intense_slider_type;

        $wrapper_start = "";
        $wrapper_end = "";

        switch ( $intense_slider_type ) {
            case 'owl-carousel':
                $wrapper_start = "<div>";
                $wrapper_end = "</div>";
                break;
            case 'flexslider':
                $wrapper_start = "<li>";
                $wrapper_end = "</li>";
                break;
            case 'basic':
                $wrapper_start = "<li>";
                $wrapper_end = "</li>";
                break;
            default:
                break;
        }

        if ( is_numeric( $link ) ) {
            $link = get_permalink( $link );
        }

        return $wrapper_start
            . ( isset( $link ) > 0 ? "<a href='$link'>" : "" )
            . do_shortcode( $content )
            . ( isset( $title )  ? "<p class='flex-caption'>$title</p>" : "" )
            . ( isset( $link ) ? "</a>" : "" ) . $wrapper_end;
    }
}
