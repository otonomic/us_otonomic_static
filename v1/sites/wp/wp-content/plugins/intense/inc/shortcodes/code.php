<?php

class Intense_Code extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Code', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-code';
		$this->show_preview = true;
		$this->preview_content = __( "Code", 'intense' );
		$this->vc_map_content = 'textarea';

		$this->fields = array(
			'color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Color', 'intense' ),
				'default' => 'muted'
			),
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
				'default' => ''
			),			
			'border_radius' => array(
				'type' => 'border_radius',
				'title' => __( 'Border Radius', 'intense' ),
			),			
			'inline' => array(
				'type' => 'checkbox',
				'title' => __( 'Inline', 'intense' ),
				'default' => "0",
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$radius = '';
		if ( isset( $border_radius ) ) {
			$radius = ' border-radius:' . $border_radius . ';';
		}

		$color = intense_get_plugin_color( $color );
        $font_color = intense_get_plugin_color( $font_color );

		if ( $font_color == '' ) {
			$font_color = intense_get_contract_color( $color );
		}

		$border_size = $intense_visions_options['intense_content_box_style']['border-top'];
		$border_color = $intense_visions_options['intense_content_box_style']['border-color'];
		$border_style = $intense_visions_options['intense_content_box_style']['border-style'];
		$border = 'border:' . $border_size . ' ' . $border_style . ' ' . $border_color . ';';

		if ( $inline ) {
			return "<code style='background: " . $color  . "; padding: 5px; color: " . $font_color . "; $border $radius'>" . trim( str_replace( "<br />", "", $content ) ) . "</code>";
		} else {
			return "<pre style='background: " . $color  . "; padding: 15px; $border $radius'><code style='color: " . $font_color . ";'>" . trim( str_replace( "<br />", "", $content ) ) . "</code></pre>";
		}
	}
}
