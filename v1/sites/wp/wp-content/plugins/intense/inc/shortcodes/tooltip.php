<?php

class Intense_Tooltip extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Tooltip', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-comment';
		$this->show_preview = true;
		$this->preview_content = __( "Content", 'intense' );
		$this->vc_map_content = 'textfield';

		$this->fields = array(
			'font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Font Color', 'intense' ),
				'default' => ''
			),
			'location' => array(
				'type' => 'dropdown',
				'title' => __( 'Location', 'intense' ),
				'default' => 'bottom',
				'options' => array(
					'top' => __( 'Top', 'intense' ),
					'right' => __( 'Right', 'intense' ),
					'bottom' => __( 'Bottom', 'intense' ),
					'left' => __( 'Left', 'intense' ),
				)
			),
			'title' => array(
				'type' => 'text',
				'title' => __( 'Title', 'intense' ),
				'composer_show_value' => true,
                'skinnable' => '0'
			),
			'link' => array(
				'type' => 'text',
				'title' => __( 'Link', 'intense' ),
				'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
			),
			'link_target' => array(
				'type' => 'link_target',
				'title' => __( 'Target', 'intense' ),
				'default' => '_self'
			),
			'tooltip_background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Tooltip Background Color', 'intense' ),
				'default' => '#111111'
			),
			'tooltip_font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Tooltip Font Color', 'intense' ),
				'default' => '#ffffff'
			),
			'tooltip_border_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Tooltip Border Color', 'intense' ),
				'default' => '#000000'
			),
			'tooltip_border_radius' => array(
				'type' => 'border_radius',
				'title' => __( 'Tooltip Border Radius', 'intense' ),
				'default' => '2px'
			),
			'tooltip_opacity' => array(
				'type' => 'text',
				'title' => __( 'Tooltip Opacity', 'intense' ),
				'description' => __( '0 - 100: default is 20', 'intense' ),
				'default' => '20'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$font_color = intense_get_plugin_color( $font_color );
		$tooltip_background_color = intense_get_plugin_color( $tooltip_background_color );
		$tooltip_font_color = intense_get_plugin_color( $tooltip_font_color );
		$tooltip_border_color = intense_get_plugin_color( $tooltip_border_color );

		$id = rand();

		intense_add_style( 'qtip2' );
		intense_add_script( 'imagesloaded' );
		intense_add_script( 'qtip2' );
		intense_add_script( 'intense.tooltip' );

		if ( $link != '' ) {
			if ( is_numeric( $link ) ) {
				$link = get_permalink( $link );
			}

			$open_tag = "a href='$link' target='$link_target'";
			$close_tag = "a";

			if ( $font_color == '' ) {
				$style = "";
			} else {
				$style = "color: " . $font_color . ";";
			}
		} else {
			$open_tag = "span";
			$close_tag = "span";
			$style = "border-bottom: 2px dotted #dfdfdf;";

			if ( $font_color != '' ) {
				$style .= " color: " . $font_color . ";";
			}
		}

		switch ( $location ) {
		case 'top':
			$location = 'bottom center';
			$at_location = 'top center';
			break;
		case 'right':
			$location = 'left center';
			$at_location = 'right center';
			break;
		case 'bottom':
			$location = 'top center';
			$at_location = 'bottom center';
			break;
		case 'left':
			$location = 'right center';
			$at_location = 'left center';
			break;
		default:
			// code...
			break;
		}

		$tooltip_opacity *= 0.01;

		return "
			<style>
				.qtip.tooltip_$id {
					background-color: $tooltip_background_color;
					color: $tooltip_font_color;
					border-color: $tooltip_border_color;
					border-radius: $tooltip_border_radius;
					opacity: $tooltip_opacity;
				}
			</style>
			<$open_tag id='tooltip_$id' data-intenseqtip='tooltip' data-placement='$location' data-at-placement='$at_location' title=\"" . esc_attr( $title ) . "\" style='$style'>" . do_shortcode( $content ) . "</$close_tag>";
	}
}
