<?php

class Intense_Definitions extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Definitions', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-book';
		$this->show_preview = true;
		$this->is_container = true;

		$this->fields = array(
			'definition' => array(
				'type' => 'repeater',
				'title' => __( 'Terms', 'intense' ),
				'fields' => array(
					'term' => array(
						'type' => 'text',
						'title' => __( 'Term', 'intense' ),
                        'skinnable' => '0'
					),
					'definition' => array(
						'type' => 'textarea',
						'title' => __( 'Definition', 'intense' ),
						'rows' => '5',
						'columns' => '50',
                        'skinnable' => '0'
					),				
				)
			),
			'horizontal' => array(
				'type' => 'checkbox',
				'title' => __( 'Horizontal', 'intense' ),
				'default' => "0",
			),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		return "<dl class='intense " . ( $horizontal ? "dl-horizontal " : "" ) . ( $rtl ? "rtl " : "" ) . "'>" . do_shortcode( $content ) . "</dl>";
	}
}

class Intense_Definition extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Definition', 'intense' );
		$this->category = __( 'Typography', 'intense' );
		$this->icon = 'dashicons-intense-book';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_definition';

		$this->fields = array(
			'term' => array(
				'type' => 'text',
				'title' => __( 'Term', 'intense' ),
                'skinnable' => '0'
			),
			'definition' => array(
				'type' => 'textarea',
				'title' => __( 'Definition', 'intense' ),
                'skinnable' => '0'
			),	
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		$output = '';

		if (isset( $term )) {
			$output .= "<dt>" . do_shortcode( $term ) . "</dt>";
		}

		if (!isset( $definition )) {
			$definition = $content;
		}

		$output .= "<dd>" . do_shortcode( $definition ) . "</dd>";

		return $output;
	}
}

/* intense_term is a deprecated shortcode */
add_shortcode( 'intense_term', 'intense_term_shortcode' );
function intense_term_shortcode( $atts, $content = null ) {
	return "<dt>" . do_shortcode( $content ) . "</dt>";
}

