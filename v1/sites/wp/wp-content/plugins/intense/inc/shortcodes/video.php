<?php

class Intense_Video extends Intense_Shortcode {

  function __construct() {
    global $intense_visions_options;

    $this->title = __( 'Video', 'intense' );
    $this->category = __( 'Media', 'intense' );
    $this->icon = 'dashicons-intense-film';
    $this->show_preview = true;

    $this->fields = array(
      'id' => array(
        'type' => 'text',
        'title' => __( 'ID', 'intense' ),
        'description' => __( 'optional - the client side id of the video. This id will be set on the HMTL tag.', 'intense' ),
        'skinnable' => '0'
      ),
      'video_type' => array(
        'type' => 'dropdown',
        'title' => __( 'Type', 'intense' ),
        'options' => array(
          '' => '',
          'wordpress' => __( 'WordPress', 'intense' ),
          'bliptv' => __( 'Blip.tv', 'intense' ),
          'collegehumor' => __( 'College Humor', 'intense' ),
          'flickr' => __( 'Flickr', 'intense' ),
          'funnyordie' => __( 'FunnyOrDie.com', 'intense' ),
          'hulu' => __( 'Hulu', 'intense' ),
          'revision3' => __( 'Revision3', 'intense' ),
          'screenr' => __( 'Screenr', 'intense' ),
          'ted' => __( 'TED', 'intense' ),
          'viddler' => __( 'Viddler', 'intense' ),
          'vimeo' => __( 'Vimeo', 'intense' ),
          'wordpresstv' => __( 'WordPress.tv', 'intense' ),
          'youtube' => __( 'YouTube', 'intense' ),
        )
      ),
      'video_url' => array(
        'type' => 'text',
        'title' => __( 'Video URL', 'intense' ),
        'class' => 'videosettings',
        'skinnable' => '0'
      ),
      'show_related' => array(
        'type' => 'checkbox',
        'title' => __( 'Show Related Videos', 'intense' ),
        'default' => '0',
        'class' => 'youtubesettings'
      ),
      'video_size' => array( 'type' => 'deprecated', 'description' => 'size is now calculated' ),
      'poster' => array(
        'type' => 'image',
        'title' => __( 'Poster Image', 'intense' ),
        'class' => 'wpsettings',
        'skinnable' => '0'
      ),
      'poster_id' => array( 'type' => 'deprecated', 'description' => 'use poster instead' ),
      'poster_url' => array( 'type' => 'deprecated', 'description' => 'use poster instead' ),
      'mp4' => array(
        'type' => 'video',
        'title' => __( 'MP4 Video', 'intense' ),
        'class' => 'wpsettings',
        'skinnable' => '0'
      ),
      'mp4_id' => array( 'type' => 'deprecated', 'description' => 'use mp4 instead' ),
      'mp4_url' => array( 'type' => 'deprecated', 'description' => 'use mp4 instead' ),
      'ogv' => array(
        'type' => 'video',
        'title' => __( 'OGV Video', 'intense' ),
        'class' => 'wpsettings',
        'skinnable' => '0'
      ),
      'ogv_id' => array( 'type' => 'deprecated', 'description' => 'use ogv instead' ),
      'ogv_url' => array( 'type' => 'deprecated', 'description' => 'use ogv instead' ),
      'webm' => array(
        'type' => 'video',
        'title' => __( 'WebM Video', 'intense' ),
        'class' => 'wpsettings',
        'skinnable' => '0'
      ),
      'webm_id' => array( 'type' => 'deprecated', 'description' => 'use webm instead' ),
      'webm_url' => array( 'type' => 'deprecated', 'description' => 'use webm instead' ),
    );
  }

  function shortcode( $atts, $content = null ) {
    global $intense_visions_options;
    extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

    $vidcontainer = "video-container-" . rand();

    if ( isset( $id ) ) {
      $video_id = $id;
    } else {
      $video_id = "video-" . rand();
    }

    if ( isset( $video_size ) ) {
      $size = $video_size;
    } else {
      $size = "auto";
    }

    if ( isset( $poster_url ) || isset( $poster_id ) ) {
      intense_add_style( 'intense_video_js_css' );
      intense_add_script( 'intense_video_js' );
    } elseif ( isset( $video_url ) ) {
      intense_add_script( 'intense_fitvids' );
    }

    if ( is_numeric( $poster ) ) {
      $poster_id = $poster;
    } else if ( !empty( $poster ) ) {
        $poster_url = $poster;
      }

    if ( !empty( $poster_id ) ) {
      $poster_info = wp_get_attachment_image_src( $poster_id, 'large1600' );
      $poster_url = $poster_info[0];
    } else if ( !empty( $poster_url ) ) {
        $poster_url = $poster_url;
      }

    if ( is_numeric( $mp4 ) ) {
      $mp4_id = $mp4;
    } else if ( !empty( $mp4 ) ) {
        $mp4_url = $mp4;
      }

    if ( !empty( $mp4_id ) ) {
      $mp4_url = wp_get_attachment_url( $mp4_id );
    } else if ( !empty( $mp4_url ) ) {
        $mp4_url = $mp4_url;
      }

    if ( is_numeric( $ogv ) ) {
      $ogv_id = $ogv;
    } else if ( !empty( $ogv ) ) {
        $ogv_url = $ogv;
      }

    if ( !empty( $ogv_id ) ) {
      $ogv_url = wp_get_attachment_url( $ogv_id );
    } else if ( !empty( $ogv_url ) ) {
        $ogv_url = $ogv_url;
      }

    if ( is_numeric( $webm ) ) {
      $webm_id = $webm;
    } else if ( !empty( $webm ) ) {
        $webm_url = $webm;
      }

    if ( !empty( $webm_id ) ) {
      $webm_url = wp_get_attachment_url( $webm_id );
    } else if ( !empty( $webm_url ) ) {
        $webm_url = $webm_url;
      }

    $output = "";

    if ( isset( $poster_url ) || isset( $poster_id ) ) {
      $output .= "<style>";
      $output .= ".videocontent {";
      $output .= "width: 100%;";
      $output .= "margin: 0 auto;";
      $output .= "}";
      $output .= ".video-js {";
      $output .= "width: 100%;";
      $output .= "}";
      $output .= ".vjs-fullscreen {";
      $output .= "  padding-top: 0px;";
      $output .= "}";
      $output .= "</style>";

      $output .= "<div id='" . $vidcontainer . "' class='videocontent'>";
      $output .= "<video id='" . $video_id . "' class='video-js vjs-default-skin vjs-big-play-centered' controls
      preload='auto' width='auto' height='auto' poster='" . $poster_url . "'
      data-setup='{}'>" .
        ( isset( $mp4_url ) ? "<source src='" . $mp4_url . "' type='video/mp4'>," : "" ) .
        ( isset( $ogv_url ) ? "<source src='" . $ogv_url . "' type='video/ogg'>," : "" ) .
        ( isset( $webm_url ) ? "<source src='" . $webm_url . "' type='video/webm'>," : "" ) .
        "</video>";
      $output .= "</div>";
    } elseif ( isset( $video_url ) ) {
      intense_add_script( 'intense.video' );

      $embed_code = wp_oembed_get( $video_url );

      switch ( $video_type ) {
      case 'youtube':
        if ( empty( $show_related ) )
          $embed_code = preg_replace( '/(youtube\.com.*)(\?feature=oembed)(.*)/', '$1?' . apply_filters( "intense_extra_youtube_querystring_parameters", "wmode=transparent&amp;" ) . 'rel=0$3', $embed_code );
        break;
      }

      $output .= "<div id='" . $vidcontainer . "' class='intenseVideoContainer'" . ( $video_type == "qik" || $video_type == "flickr" ? "style='padding-top:0px'" : "" ) .">";
      $output .= $embed_code;
      $output .= "</div>";

      $output .= "<style>";
      $output .= "#" . $vidcontainer . " {";
      $output .= "position: relative;";
      $output .= "}";
      $output .= "#" . $vidcontainer . " iframe, #" . $vidcontainer . " object, #" . $vidcontainer . " embed {";
      $output .= "position: absolute;";
      $output .= "top: 0;";
      $output .= "left: 0;";
      $output .= "width: 100%;";
      $output .= "height: 100%;";
      $output .= "overflow: hidden;";
      $output .= "}";
      $output .= "</style>";

      $output .= "<script>";
      $output .= "  jQuery(document).ready(function(){";
      $output .= "    jQuery('#" . $vidcontainer . "').fitVids();";
      $output .= "  });";
      $output .= "</script>";
    }

    return $output;
  }

  function render_dialog_script() {
?>
    <script>
        jQuery(function($) {
          // Uploading files
            $('.upload_image_button').live('click', function( event ){
              event.preventDefault();
              var target = $(this).data('target-id');

              window.parent.loadImageFrame($(this), function(attachment) {
                jQuery("#" + target).val(attachment.id);

                if ( $('#preview-content').length > 0 ) {
                  $('#preview').click();
                }
              });
          });

          $('.upload_video_button').live('click', function( event ){
              event.preventDefault();
              var target = $(this).data('target-id');

              window.parent.loadVideoFrame($(this), function(attachment) {
                jQuery("#" + target).val(attachment.id);

                if ( $('#preview-content').length > 0 ) {
                  $('#preview').click();
                }
              });
          });

          $(".wpsettings").hide();
          $(".videosettings").hide();
          $(".youtubesettings").hide();


          $("#video_type").change(function() {
            var type = $(this).val();

            $(".wpsettings").hide();
            $(".videosettings").hide();
            $(".youtubesettings").hide();

            switch(type) {
              case "wordpress":
                $(".wpsettings").show();
                break;
              case "bliptv":
              case "collegehumor":
              case "dailymotion":
              case "flickr":
              case "funnyordie":
              case "hulu":
              case "revision3":
              case "screenr":
              case "ted":
              case "viddler":
              case "vimeo":
              case "wordpresstv":
                $(".videosettings").show();
                break;
              case "youtube":
                $(".videosettings").show();
                $(".youtubesettings").show();
                break;
              default:
                $(".wpsettings").hide();
                $(".videosettings").hide();
                break;
            }
          });
        });
      </script>
    <?php
  }
}
