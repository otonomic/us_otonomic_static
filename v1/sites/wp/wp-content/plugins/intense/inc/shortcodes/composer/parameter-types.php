<?php

function intense_color_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );
	return '<div class="color_parameter_block">'
		. '<label>' . __( 'Type', 'intense' ) . '</label>
   			<select name="'.$settings['param_name'].'_type" class="colortype">
				<option value="plugin">' . __( 'Plugin Color', 'intense' ) . '</option>
				<option value="manual">' . __( 'Manual Color', 'intense' ) . '</option>
	  		</select>
	  		<label>' . __( 'Value', 'intense' ) . '</label>
	  		<select name="'.$settings['param_name'].'_plugin" class="plugincolor">
				<option value=""></option>
				<option value="primary">' . __( 'Primary', 'intense' ) . '</option>
				<option value="error">' . __( 'Error', 'intense' ) . '</option>
				<option value="info">' . __( 'Info', 'intense' ) . '</option>
				<option value="inverse">' . __( 'Inverse', 'intense' ) . '</option>
				<option value="muted">' . __( 'Muted', 'intense' ) . '</option>
				<option value="success">' . __( 'Success', 'intense' ) . '</option>
				<option value="warning">' . __( 'Warning', 'intense' ) . '</option>
				<option value="link">' . __( 'Link', 'intense' ) . '</option>
			</select>'
		. '<input name="'.$settings['param_name'].'_manual" class="manualcolor" type="text" />'
		.'<input name="'.$settings['param_name']
		.'" class="wpb_vc_param_value wpb-textinput colorvalue '
		.$settings['param_name'].' '.$settings['type'].'_field" type="hidden" value="'
		.$value.'" ' . $dependency . '/>'
		.'</div>';
}
add_shortcode_param( 'intense_color', 'intense_color_settings_field', INTENSE_PLUGIN_URL . "/inc/shortcodes/composer/js/intense_color.js" );

function intense_shadow_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );
	return '<div class="shadow_param_block shadowblock">
	  		<div class="pull-left"><select class="shadows wpb_vc_param_value '
		.$settings['param_name'].' '.$settings['type'].'_field" name="'.$settings['param_name'].'">
				<option value="0" ' . ( $value == 0 ? 'selected' : '' ) . '>' . __( 'None', 'intense' ) . '</option>
				<option ' . ( $value == 1 ? 'selected' : '' ) . '>1</option>
				<option ' . ( $value == 2 ? 'selected' : '' ) . '>2</option>
				<option ' . ( $value == 3 ? 'selected' : '' ) . '>3</option>
				<option ' . ( $value == 4 ? 'selected' : '' ) . '>4</option>
				<option ' . ( $value == 5 ? 'selected' : '' ) . '>5</option>
				<option ' . ( $value == 6 ? 'selected' : '' ) . '>6</option>
				<option ' . ( $value == 7 ? 'selected' : '' ) . '>7</option>
				<option ' . ( $value == 8 ? 'selected' : '' ) . '>8</option>
				<option ' . ( $value == 9 ? 'selected' : '' ) . '>9</option>
				<option ' . ( $value == 10 ? 'selected' : '' ) . '>10</option>
				<option ' . ( $value == 11 ? 'selected' : '' ) . '>11</option>
				<option ' . ( $value == 12 ? 'selected' : '' ) . '>12</option>
				<option ' . ( $value == 13 ? 'selected' : '' ) . '>13</option>
				<option ' . ( $value == 14 ? 'selected' : '' ) . '>14</option>
			</select></div>
			<div class="pull-left" style="width: 300px; margin-left: 10px">
				<div style="height:30px; background:#dfdfdf;"></div>
				<img class="shadowpreview" src="' . INTENSE_PLUGIN_URL . '/assets/img/shadow0.png" style="vertical-align: top; width: 100%; max-height: 20px;" />
			</div>
	</div>';
}
add_shortcode_param( 'intense_shadow', 'intense_shadow_settings_field', INTENSE_PLUGIN_URL . "/inc/shortcodes/composer/js/intense_shadow.js" );

function intense_video_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );
	return '<div class="video_parameter_block">'
		. '<input style="width: 90%; padding-right: 5px;" name="'.$settings['param_name']
		. '" class="wpb_vc_param_value wpb-textinput videovalue '
		. $settings['param_name'].' '.$settings['type'].'_field" type="text" value="'
		. $value.'" ' . $dependency . '/><a class="videoselect button">' . __( 'Select', 'intense' ) . '</a>'
		. '</div>';
}

add_shortcode_param( 'intense_video', 'intense_video_settings_field', INTENSE_PLUGIN_URL . "/inc/shortcodes/composer/js/intense_video.js" );

function intense_file_settings_field( $settings, $value ) {
	$dependency = vc_generate_dependencies_attributes( $settings );
	return '<div class="file_parameter_block">'
		. '<input style="width: 90%; padding-right: 5px;" name="'.$settings['param_name']
		. '" class="wpb_vc_param_value wpb-textinput filevalue '
		. $settings['param_name'].' '.$settings['type'].'_field" type="text" value="'
		. $value.'" ' . $dependency . '/><a class="fileselect button">' . __( 'Select', 'intense' ) . '</a>'
		. '</div>';
}

add_shortcode_param( 'intense_file', 'intense_file_settings_field', INTENSE_PLUGIN_URL . "/inc/shortcodes/composer/js/intense_file.js" );

function intense_tab_settings_field( $settings, $value ) {	
	return '<div style="border-top: 1px solid #ccc; background: #efefef; padding: 10px; margin: 0 -15px; font-weight: bold;">' . $settings['heading'] . '</div><style>.wpb_el_type_intense_tab .wpb_element_label { display: none; } </style>';
}

add_shortcode_param( 'intense_tab', 'intense_tab_settings_field', null );