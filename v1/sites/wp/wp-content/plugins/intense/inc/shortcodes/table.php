<?php

class Intense_Table extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Table', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-table';
		$this->show_preview = true;

		$this->fields = array(
			'id' => array(
				'type' => 'text',
				'title' => __( 'ID', 'intense' ),
				'description' => __( 'optional - used to set the client-side ID', 'intense' ),
                'skinnable' => '0'
			),
			'columns' => array(
				'type' => 'hidden',
				'title' => __( 'Columns', 'intense' ),
				'description' => __( 'comma separated list of column header text', 'intense' ),
				'skinnable' => '0'
			),			
			'data' => array(
				'type' => 'hidden',
				'title' => __( 'Data', 'intense' ),
				'description' => __( 'comma separated list of table data. Number of data items should match columns.', 'intense' ),
                'skinnable' => '0'
			),
			'type' => array(
            	'type' => 'dropdown',
                'title' => __( 'Type', 'intense' ),
                'default' => 'condensed',
				'options' => array(
					'condensed' => __( 'Condensed', 'intense' ),
					'standard' => __( 'Standard', 'intense' ),
					'striped' => __( 'Striped', 'intense' ),
				),			
            ),
            'source' => array(
            	'type' => 'dropdown',
                'title' => __( 'Source', 'intense' ),
                'default' => 'manual',
				'options' => array(
					'manual' => __( 'Manual', 'intense' ),
					'file' => __( 'File', 'intense' ),
				),
            ),
            'heading_background_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Heading Background Color', 'intense' ),				
			),
			'heading_font_color' => array(
				'type' => 'color_advanced',
				'title' => __( 'Heading Font Color', 'intense' ),				
			),			
            'bordered' => array(
                'type' => 'checkbox',
                'title' => __( 'Bordered', 'intense' ),
                'default' => "0",
            ),
            'file' => array(
				'type' => 'file',
				'title' => __( 'CSV File', 'intense' ),
				'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
				'class' => 'filesettings',
                'skinnable' => '0'
			),
			'delimiter' => array(
				'type' => 'text',
				'title' => __( 'Field Delimiter', 'intense' ),
				'default' => ',',
				'class' => 'filesettings'
			),
			'skip_empty_lines' => array(
                'type' => 'checkbox',
                'title' => __( 'Skip Empty Lines', 'intense' ),
                'default' => "1",
				'class' => 'filesettings'
            ),
            'trim_fields' => array(
                'type' => 'checkbox',
                'title' => __( 'Trim Fields', 'intense' ),
                'default' => "1",
				'class' => 'filesettings'
            ),
            'rtl' => array(
                'type' => 'hidden',
                'description' => __( 'right-to-left', 'intense' ),
                'default' => $intense_visions_options['intense_rtl']
            )
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( empty( $id ) ) $id = 'table_' . rand();

		$table_style = '';
		$heading_style = '';

		$heading_background_color = intense_get_plugin_color( $heading_background_color );
     	$heading_font_color = intense_get_plugin_color( $heading_font_color );

        if ( !empty( $heading_background_color ) ) $heading_style .= 'background: ' . $heading_background_color . ' !important;';
        if ( !empty( $heading_font_color ) ) $heading_style .= 'color: ' . $heading_font_color . ' !important;';

		$columns = explode( ',', $columns );
		$data = explode( ',', $data );
		$total = count( $columns );

		if ( is_numeric( $file ) ) {
			$url = wp_get_attachment_url( $file );
		} else if ( !empty( $file ) ) {
			$url = $file;
		}

		if ( isset( $url ) ) {
			$csv = file_get_contents( $url );			
			
			$csv = $this->parse_csv( $csv, $delimiter, $skip_empty_lines, $trim_fields );			
			$columns = array();
			$data = array();			

			foreach( $csv as $row => $fields ) {
				if ( $row == 0 ) {			
			 		$columns = array_merge( $columns, $fields );
			 		$total = count( $columns );
			 	} else {
			 		//fill in so you have the same number of fields as columns
			 		if ( count( $fields ) < $total ) {
			 			for ($i=0; $i <= $total - count( $fields ) ; $i++) { 
			 				$fields[] = '';
			 			}
			 		}

			 		$data = array_merge( $data, $fields );
			 	}
			}			
		}

		if ( $type == 'striped' || !empty( $heading_style ) ) {
			$table_style = "<style type='text/css'>";
		}

		if ( !empty( $heading_style ) ) {
			$table_style .= '#' . $id . ' th {' . $heading_style . '}';
		}

		if ( $type == 'striped' ) {
			$table_style .= '#' . $id . " tbody>tr:nth-child(odd)>td, #$id tbody>tr:nth-child(odd)>th {
							background-color: " . intense_get_plugin_color( "boxed" ) . " !important;
						}";
		} 

		if ( !empty( $table_style ) ) {
			$table_style .= '</style>';
		}

		$output = $table_style;
		$output .= "<table id='$id' class='intense table table-$type " . ( $bordered ? "table-bordered " : "" ) . ( $rtl ? 'rtl ' : '' ) . "'><thead><tr>";

		foreach ( $columns as $col ) {
			$output .= '<th>' . trim( $col ) . '</th>';
		}

		$output .= '</tr></thead><tbody><tr>';
		$counter = 1;

		foreach ( $data as $datum ) {
			$output .= '<td>' . $datum . '</td>';

			if ( $counter % $total == 0 ) {
				$output .= '</tr><tr>';
			}

			$counter++;
		}

		$output = substr( $output, 0, -4 );

		$output .= '</tbody></table>';

		return $output;
	}

	function render_dialog_script() {	
?>
		<style>
			#tablecontent input {
				width: 120px;
			}

			#tablecontent th {
				padding: 3px;
				background: #efefef;
			}

			#tablecontent th > input {
				font-weight: bold;
				font-size: larger;
			}

			#tablecontent td {
				padding: 3px;
			}

			#tablecontent table {
				border: 1px solid #ccc;
			}
		</style>
		<div class="form-group ">
			<label class="intense col-sm-2 control-label" for="bordered"><?php _e( 'Data', 'intense' ); ?></label>
			<div class="intense col-sm-8">
				<div id="tablecontent" style="height: 350px; overflow: auto; border: 1px solid #ccc; background: #fff; padding: 15px; margin-bottom: 10px;">
					<table>
						<thead>
							<tr>
								<th><input type="text"/></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<input type="text" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<input type="button" id="addrow" class="button" value="<?php _e( 'Add Row', 'intense' ); ?>" /> <input type="button" id="addcolumn" class="button" value="<?php _e( 'Add Column', 'intense' ); ?>" />	
				<span class="help-block"><?php _e( 'commas are not allowed - use &amp;#44; instead (example: Indiana<strong>&amp;#44;</strong> USA becomes Indiana, USA)', 'intense' ); ?></span>
			</div>
		</div>
		
        <script>
            jQuery(function($) {
            	// Uploading files
		  		$('.upload_file_button').live('click', function( event ){
	              event.preventDefault();
	              var target = $(this).data('target-id');
	        
	              window.parent.loadFileFrame($(this), function(attachment) {
	                jQuery("#" + target).val(attachment.id);

	                if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
	              });
	          	});

	          	$('.filesettings').hide();

	          	$('#source').change(function() {
	          		var value = $(this).val();

	          		switch (value) {
	          			case 'manual':
	          				$('#tablecontent').parent().parent().show();
	          				$('.filesettings').hide();
	          				break;
	          			case 'file':
	          				$('#tablecontent').parent().parent().hide();
	          				$('.filesettings').show();
	          				break;
	          		}
	          	});

		  		$('#addrow').click(function() {
					$('#tablecontent > table > tbody').append($('#tablecontent > table > tbody > tr:last').clone());
					$('#tablecontent > table > tbody > tr:last input').val('');
					setupTableChangeHandler();
				});

				$('#addcolumn').click(function() {					
					$('#tablecontent > table > thead > tr').append('<th><input type="text"/></th>');
					$('#tablecontent > table > tbody > tr').append('<td><input type="text"/></td>');
					setupTableChangeHandler();
				});

				function setupTableChangeHandler() {
					$('#tablecontent input').unbind('keypress');
					$('#tablecontent input').typeWatch( {
		                callback: updateHiddenFields,
		                wait: 500,
		                highlight: false,
		                captureLength: 0
		            } );
				}

				setupTableChangeHandler();

				function updateHiddenFields() {
					var $table = jQuery('#tablecontent table');
					var columns = [];
					var data = [];

					$table.find('th').each(function() {
						columns.push(jQuery(this).children('input').val());
					});

					$table.find('td').each(function() {
						data.push(jQuery(this).children('input').val());
					});

					$('#columns').val(columns.join(','));
					$('#data').val(data.join(','));

					if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
				}
            });
        </script>
    <?php
    }

    function encode_field( $field) {
    	return urlencode( utf8_encode( $field[1] ) );
    }

    function trim_field($line) {
    	global $parse_csv_delimiter;
    	global $parse_csv_trim_fields;

        $fields = $parse_csv_trim_fields ? array_map('trim', explode($parse_csv_delimiter, $line)) : explode($parse_csv_delimiter, $line);

        return array_map(
            array( __CLASS__, 'special_quote' ),
            $fields
        );
    }

    function special_quote($field) {
        return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
    }

    /**
     * CSV parser found on http://www.php.net/manual/en/function.str-getcsv.php
     * @param  string  $csv_string       string to parse
     * @param  string  $delimiter        field separator
     * @param  boolean $skip_empty_lines ignore empty lines
     * @param  boolean $trim_fields      trim whitespace from fields
     * @return array                     two dimensional array of rows and fields
     */
    function parse_csv ( $csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true ) {
    	global $parse_csv_delimiter;
    	global $parse_csv_trim_fields;

    	$parse_csv_delimiter = $delimiter;
    	$parse_csv_trim_fields = $trim_fields;

	    $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
	    $enc = preg_replace_callback(
	        '/"(.*?)"/s',
	        array( __CLASS__, 'encode_field' ),
	        $enc
	    );

	    $lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);

	    return array_map(
	        array( __CLASS__, 'trim_field' ),
	        $lines
	    );
	}
}
