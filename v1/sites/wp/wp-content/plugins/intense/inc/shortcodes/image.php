<?php

class Intense_Image extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Image', 'intense' );
		$this->category = __( 'Media', 'intense' );
		$this->icon = 'dashicons-intense-image';
		$this->show_preview = true;

		$this->fields = array(
			'image_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'image' => array(
						'type' => 'image',
						'title' => __( 'Image', 'intense' ),
						'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                        'skinnable' => '0'
					),
					'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'size' => array(
						'type' => 'image_size',
						'title' => __( 'Image Size', 'intense' ),
						'default' => 'full'
					),
					'alt' => array(
						'type' => 'text',
						'title' => __( 'Alt', 'intense' ),
						'description' => __( 'the image <a href="http://en.wikipedia.org/wiki/Alt_attribute" target="_blank">alt</a> text', 'intense' ),
                        'skinnable' => '0'
					),
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
					),
					'caption' => array(
						'type' => 'text',
						'title' => __( 'Caption', 'intense' ),
                        'skinnable' => '0'
					),
					'linkurl' => array(
						'type' => 'text',
						'title' => __( 'Link', 'intense' ),
                        'skinnable' => '0'
					),
					'linkurl_target' => array(
						'type' => 'link_target',
						'title' => __( 'Link Target', 'intense' ),
						'default' => '_self'
					),
					'shadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
					),
					'class' => array(
						'type' => 'text',
						'title' => __( 'CSS Class', 'intense' ),
					),
				),
			),
			'type_tab' => array(
				'type' => 'tab',
				'title' => __( 'Type', 'intense' ),
				'fields' =>  array(
					'lightbox_type' => array(
						'type' => 'dropdown',
						'title' => __( 'Lightbox Type', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'colorbox' => __( 'Colorbox', 'intense' ), //image, iframe, html
							'magnificpopup' => __( 'Magnific Popup', 'intense' ), //image, iframe, html
							'photoswipe' => __( 'PhotoSwipe', 'intense' ), //image
							'prettyphoto' => __( 'PrettyPhoto', 'intense' ), //image, iframe, html
							'swipebox' => __( 'Swipebox', 'intense' ), //image
							'thickbox' => __( 'Thickbox', 'intense' ), //image
							'touchtouch' => __( 'TouchTouch', 'intense' ), //image
						),
					),
					'type' => array(
						'type' => 'dropdown',
						'title' => __( 'Type', 'intense' ),
						'default' => 'standard',
						'options' => array(
							'standard' => __( 'Standard', 'intense' ),
							'caman' => __( 'CamanJS', 'intense' ),
							'fixed' => __( 'Parallax - Fixed', 'intense' ),
							'parallax' => __( 'Parallax', 'intense' ),
							'picstrip' => __( 'Picstrip', 'intense' )
						)
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
						'class' => 'standardsettings'
					),
					'splits' => array(
						'type' => 'text',
						'title' => __( 'Splits', 'intense' ),
						'default' => '10',
						'class' => 'picstripsettings'
					),
					'hgutter' => array(
						'type' => 'text',
						'title' => __( 'Horizontal Gutter', 'intense' ),
						'default' => '10px',
						'class' => 'picstripsettings'
					),
					'vgutter' => array(
						'type' => 'text',
						'title' => __( 'Vertical Gutter', 'intense' ),
						'default' => '10px',
						'class' => 'picstripsettings'
					),
					'bgcolor' => array(
						'type' => 'color',
						'title' => __( 'Background Color', 'intense' ),
						'default' => '#ffffff',
						'class' => 'picstripsettings'
					),
					'effect' => array(
						'type' => 'dropdown',
						'title' => __( 'Effect', 'intense' ),
						'default' => 'clarity',
						'options' => array(
							'clarity' => 'clarity',
							'concentrate' => 'concentrate',
							'crossProcess' => 'crossProcess',
							'emboss' => 'emboss',
							'glowingSun' => 'glowingSun',
							'grungy' => 'grungy',
							'hazyDays' => 'hazyDays',
							'hemingway' => 'hemingway',
							'herMajesty' => 'herMajesty',
							'jarques' => 'jarques',
							'lomo' => 'lomo',
							'love' => 'love',
							'nostalgia' => 'nostalgia',
							'oldBoot' => 'oldBoot',
							'orangePeel' => 'orangePeel',
							'pinhole' => 'pinhole',
							'posterize' => 'posterize',
							'sinCity' => 'sinCity',
							'sunrise' => 'sunrise',
							'vintage' => 'vintage',
						),
						'class' => 'camansettings'
					),
					'parallax_height' => array(
						'type' => 'text',
						'title' => __( 'Parallax Height', 'intense' ),
						'default' => '200',
						'class' => 'parallaxsettings'
					),
					'parallax_speed' => array(
						'type' => 'text',
						'title' => __( 'Parallax Speed', 'intense' ),
						'default' => '2',
						'class' => 'parallaxsettings parallaxstandard'
					),
				),
			),
			'hover_tab' => array(
				'type' => 'tab',
				'title' => __( 'Hover', 'intense' ),
				'fields' =>  array(
					'hover_effect' => array(
						'type' => 'dropdown',
						'title' => __( 'Hover Effect', 'intense' ),
						'default' => 'effeckt',
						'options' => array(
							'effeckt' => __( 'Effeckt', 'intense' ),
							'adipoli' => __( 'Adipoli', 'intense' ),
						)
					),
					'starteffect' => array(
						'type' => 'dropdown',
						'title' => __( 'Start Effect', 'intense' ),
						'default' => 'normal',
						'options' => array(
							'normal' => 'normal',
							'overlay' => 'overlay',
							'sliceDown' => 'sliceDown',
							'transparent' => 'transparent',
						),
						'class' => 'adipolisettings'
					),
					'hovereffect' => array(
						'type' => 'dropdown',
						'title' => __( 'Hover Effect', 'intense' ),
						'default' => 'boxRain',
						'options' => array(
							'boxRain' => 'boxRain',
							'boxRainGrow' => 'boxRainGrow',
							'boxRainGrowReverse' => 'boxRainGrowReverse',
							'boxRainReverse' => 'boxRainReverse',
							'boxRandom' => 'boxRandom',
							'fold' => 'fold',
							'foldLeft' => 'foldLeft',
							'normal' => 'normal',
							'popout' => 'popout',
							'sliceDown' => 'sliceDown',
							'sliceDownLeft' => 'sliceDownLeft',
							'sliceUp' => 'sliceUp',
							'sliceUpDown' => 'sliceUpDown',
							'sliceUpDownLeft' => 'sliceUpDownLeft',
							'sliceUpLeft' => 'sliceUpLeft',
							'sliceUpRandom' => 'sliceUpRandom',
						),
						'class' => 'adipolisettings'
					),
					'effeckt' => array(
						'type' => 'dropdown',
						'title' => __( 'Effeckt', 'intense' ),
						'default' => '',
						'options' => array(
							'' => '',
							'1'  => __( 'Appear', 'intense' ),
							'2'  => __( 'Quarter Slide Up', 'intense' ),
							'3'  => __( 'Sqkwoosh', 'intense' ),
							'4'  => __( 'Quarter Slide Side', 'intense' ),
							'5'  => __( 'Cover Fade', 'intense' ),
							'6'  => __( 'Quarter Fall In', 'intense' ),
							'7'  => __( 'Quarter Two-Step', 'intense' ),
							'8'  => __( 'Cover Push Right', 'intense' ),
							'9'  => __( 'Quarter Caption Zoom', 'intense' ),
							'10' => __( 'Revolving Door', 'intense' ),
							'11' => __( 'Caption Offset', 'intense' ),
							'12' => __( 'Guillotine Reverse', 'intense' ),
							'13' => __( 'Half Slide', 'intense' ),
							'14' => __( 'Tunnel', 'intense' ),
							'15' => __( 'Cover Slide Top', 'intense' ),
						),
						'class' => 'effecktsettings'
					),
					'effecktcolor' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background Color', 'intense' ),
						'default' => '#000000',
						'class' => 'effecktsettings'
					),
					'effecktopacity' => array(
						'type' => 'text',
						'title' => __( 'Background Opacity', 'intense' ),
						'description' => __( '0 - 100', 'intense' ),
						'default' => '80',
						'class' => 'effecktsettings'
					),
				),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $_wp_additional_image_sizes;

		$output = '';
		$imgID = '';

		if ( is_numeric( $image ) ) {
			$imageid = $image;
		} else if ( !empty( $image ) ) {
				$imageurl = $image;
			}

		$width = null;
		$height = null;
		$w = null;
		$h = null;
		$extra_class = null;
		$shadow_style = '';
		$effeckt_style = '';

		$alt = esc_attr( $alt );
		$title = esc_attr( $title );
		$caption = esc_attr( $caption );

		$radius = '';
		if ( isset( $border_radius ) ) {
			$radius = ' border-radius:' . $border_radius . ';';
		}

		if ( $imageurl != '' && isset( $_wp_additional_image_sizes[$size] ) ) {
			$width = intval( $_wp_additional_image_sizes[$size]['width'] );
			$height = intval( $_wp_additional_image_sizes[$size]['height'] );
			$w = intval( $_wp_additional_image_sizes[$size]['width'] );
			$h = intval( $_wp_additional_image_sizes[$size]['height'] );
		}

		if ( $effeckt != '' && isset( $effecktopacity ) && isset( $effecktcolor ) ) {
			$hex = str_replace( "#", "", $effecktcolor );
			$a = $effecktopacity / 100;

			if ( strlen( $hex ) == 3 ) {
				$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
				$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
				$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
			} else {
				$r = hexdec( substr( $hex, 0, 2 ) );
				$g = hexdec( substr( $hex, 2, 2 ) );
				$b = hexdec( substr( $hex, 4, 2 ) );
			}

			$effeckt_style = "background: rgba(" . $r . "," . $g . "," . $b . "," . $a . ") !important; rgb(" . $r . "," . $g . "," . $b . ")\9 !important;";
		}

		if ( $imageid != '' ) {
			$photo_info = wp_get_attachment_image_src( $imageid, $size );
			$photo_url = $photo_info[0];
			$width = $photo_info[1];
			$height = $photo_info[2];
			$w = $photo_info[1];
			$h = $photo_info[2];
			$full_size = ( isset( $intense_visions_options['intense_photosource_full_size'] ) ? $intense_visions_options['intense_photosource_full_size'] : 'large' );
			$photo_info = wp_get_attachment_image_src( $imageid, $full_size );
			$full_size_image = $photo_info[0];
		} else {
			$photo_url = $imageurl;
		}

		if ( isset( $width ) ) { $width = " width='" . $width . "' "; }
		if ( isset( $height ) ) { $height = " height='" . $height . "' "; };

		$imgID = 'img' . $imageid . '_' . rand();
		$shadow_style = "margin-bottom: 0; padding-bottom: 0; vertical-align: bottom !important;";
		$adipoliclass = "adipoli_" . rand();

		$effeckt_start = '';
		$effeckt_end = '';

		$additionalscript = '';

		if ( $effeckt != '' ) {
			intense_add_style( 'effeckt_caption' );

			$effeckt_start = "<figure class='intense effeckt-caption preload' data-effeckt-type='effeckt-caption-" . $effeckt . "'>";
			$effeckt_end = "<figcaption style='" . $effeckt_style . "'><div class='effeckt-figcaption-wrap'><h3>" . $title . "</h3><p>" . $caption . "</p></div></figcaption></figure><div class='clearfix'></div>";
			$additionalscript .= ' $(".preload").removeClass("preload"); ';
			$additionalscript .= ' if ($("#intense-browser-check").hasClass("ie8")) {
                $(".effeckt-caption").find("figcaption").fadeTo("fast", 0);
                $(".effeckt-caption").hover(
                    function() { $(this).find("figcaption").fadeTo("fast", ' . $effecktopacity / 100 . '); },
                    function() { $(this).find("figcaption").fadeTo("fast", 0); } );
            } ';
		}

		$shadow_start = '';
		$shadow_end = '';

		if ( $shadow ) {
			$shadow_start = "<div><div style='$shadow_style'>";
			$shadow_end = "</div><img src='" . INTENSE_PLUGIN_URL . "/assets/img/shadow{$shadow}.png' class='intense shadow' style='" . ( isset( $w ) ? 'width: ' . $w . 'px; height: auto;' : '' ) . "vertical-align: top; border:0px; border-radius: 0px; box-shadow: 0 0 0;' /></div>";
		}

		$link_start = '';
		$link_end = '';

		if ( $linkurl != '' ) {
			$link_start = "<a href='" . $linkurl . "' target='" . $linkurl_target . "'>";
			$link_end = "</a>";
		}

		if ( !empty( $lightbox_type ) ) {
			$lightbox = Intense_Lightboxes::create( $lightbox_type, '#link_' . $imgID );
            $additionalscript .= $lightbox->get_image_script();
           	//add id to the link that is created so it can be found later. Also remove the ending link tag
            $link_start = str_replace("<a", '<a id="link_' . $imgID . '"', str_replace("</a>", '', $lightbox->get_image( $imgID, $title, $alt, ( isset( $full_size_image ) ? $full_size_image : $photo_url ), '' ) ) );
            $link_end = "</a>";
            $output .= '<style type="text/css">' . $lightbox->get_css() . '</style>';
		}

		// Add CSS/JS for different image types and add JavaScript setup code
		switch ( $type ) {
		case 'standard':
			$output .= $shadow_start;
			$output .= $link_start;
			$output .= $effeckt_start;
			$output .= "<img id='" . $imgID . "' $width $height style='" . $radius . ";' src='" . $photo_url . "' alt='" . $alt . "' class='intense image " . $class . " " . $extra_class . "' title='" . $title . "' />";
			$output .= $effeckt_end;
			$output .= $link_end;
			$output .= $shadow_end;

			break;
		case 'picstrip':
			// PicStrips jquery plugin
			intense_add_script( 'picstrips' );

			$additionalscript .= "    setTimeout(function(){ $('#$imgID').picstrips({ splits: $splits, hgutter: '$hgutter', vgutter: '$vgutter', bgcolor: '$bgcolor' }), 3000});";

			$output .= $effeckt_start;
			$output .= "<img style='width: 100%;' id='" . $imgID . "' src='" . $photo_url . "' alt='" . $alt . "' title='" . $title . "' />";
			$output .= $effeckt_end;

			break;
		case 'caman':
			// CamanJS jquery plugin
			intense_add_script( 'camanjs' );

			$additionalscript .= "  setTimeout(function(){ Caman('#$imgID', function() {";
			$additionalscript .= "   this.$effect();";
			$additionalscript .= "   this.render();";
			$additionalscript .= "  }), 3000});";

			$output .= $shadow_start;
			$output .= $effeckt_start;
			$output .= "<img style='width: 100%;' id='" . $imgID . "' src='" . $photo_url . "' alt='" . $alt . "' title='" . $title . "' />";
			$output .= $effeckt_end;
			$output .= $shadow_end;

			break;
		case 'adipoli':
			intense_add_style( 'adipoli' );
			intense_add_script( 'adipoli' );

			if ( $starteffect != '' && $hovereffect != '' ) {
				$additionalscript .= "    var isOldIE = $('#intense-browser-check').hasClass('oldie'); ";
				$additionalscript .= "    if (!isOldIE) {";
				$additionalscript .= "      $('." . $adipoliclass . " img').adipoli({ 'startEffect': '" . $starteffect . "', 'hoverEffect': '" . $hovereffect . "' });";
				$additionalscript .="    }";
			}

			$output .= $shadow_start;
			$output .= $link_start;
			$output .= "<div class='" . $adipoliclass . "'><img id='" . $imgID . "' $width $height src='" . $photo_url . "' alt='" . $alt . "' class='" . $class . " " . $extra_class . "' title='" . $title . "' /></div>";
			$output .= $link_end;
			$output .= $shadow_end;

			break;
		case 'parallax':
			$output .= $shadow_start;
			$output .= $effeckt_start;
			$output .= intense_run_shortcode( 'intense_content_section', array( 
				'height' => $parallax_height,
				'size' => 'full',
				'breakout' => '0',
				'speed' => $parallax_speed,
				'imageurl' => $photo_url,
				'border_bottom' => '0',
				'border_top' => '0',
				'imagemode' => 'parallax',
				'margin_top' => '0',
				'margin_bottom' => '0'
			) );			
			$output .= $effeckt_end;
			$output .= $shadow_end;

			break;
		case 'fixed':
			$output .= $shadow_start;
			$output .= $effeckt_start;
			$output .= intense_run_shortcode( 'intense_content_section', array( 
				'height' => $parallax_height,
				'size' => 'full',
				'breakout' => '0',
				'speed' => $parallax_speed,
				'imageurl' => $photo_url,
				'border_bottom' => '0',
				'border_top' => '0',
				'imagemode' => 'fixed',
				'margin_top' => '0',
				'margin_bottom' => '0'
			) );			
			$output .= $effeckt_end;
			$output .= $shadow_end;

			break;
		}

		if ( isset( $additionalscript ) && $additionalscript != '' ) {
			$output .= "<script> jQuery(function($) {";
			$output .= " $(document).ready(function() {";
			$output .= $additionalscript;
			$output .= " });";
			$output .= "});</script>";
		}

		return $output;
	}

	function render_dialog_script() {
?>
		<script>
		jQuery(function($) {
	  		$('.upload_image_button').live('click', function( event ){
              	event.preventDefault();
              	var target = $(this).data('target-id');

          		window.parent.loadImageFrame($(this), function(attachment) {
	                jQuery("#" + target).val(attachment.id);
	                
	                if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }

	                jQuery("#caption").val(attachment.caption);
			    	jQuery("#title").val(attachment.title);
			    	jQuery("#alt").val(attachment.alt);

			    	if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
      			});
          	});

          	$('.parallaxsettings').hide();
          	$('.adipolisettings').hide();
          	$('.camansettings').hide();
          	$('.picstripsettings').hide();

          	$('#type').change(function() {
          		var value = $(this).val();

          		$('.parallaxsettings').hide();
          		$('.camansettings').hide();
          		$('.picstripsettings').hide();
          		$('.standardsettings').hide();

          		switch(value) {
          			case 'standard':
          				$('.standardsettings').show();
          				break;
					case 'caman':
						$('.camansettings').show();
						break;
					case 'fixed':
						$('.parallaxsettings').show();
						$('.parallaxstandard').hide();
						break;
					case 'parallax':
						$('.parallaxsettings').show();
						$('.parallaxstandard').show();
						break;
					case 'picstrip':
						$('.picstripsettings').show();
						break;
          		}
          	});

          	$('#hover_effect').change(function() {
          		var value = $(this).val();

          		$('.effecktsettings').hide();
          		$('.adipolisettings').hide();

          		switch(value) {
          			case 'effeckt':
          				$('.effecktsettings').show();
          				break;
          			case 'adipoli':
          				$('.adipolisettings').show();
          				break;
          		}
          	});
		});
	</script>

	<?php
	}
}
