<?php

class Intense_Social_Share extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Social Share', 'intense' );
        $this->category = __( 'Elements', 'intense' );
        $this->icon = 'dashicons-intense-share';
        $this->show_preview = true;
        $this->preview_content = __( "Social Share Content", 'intense' );

        $this->fields = array(
            'share_url' => array(
                'type' => 'text',
                'title' => __( 'Like/Share URL', 'intense' ),
                'class' => 'textsettings',
                'skinnable' => '0'
            ),
            'title' => array(
                'type' => 'text',
                'title' => __( 'Title (optional)', 'intense' ),
                'class' => 'textsettings',
                'skinnable' => '0'
            ),
            'size' => array(
                'type' => 'dropdown',
                'title' => __( 'Size', 'intense' ),
                'class' => 'textsettings',
                'default' => '16',
                'options' => array(
                    '24' => '24',
                    '23' => '23',
                    '22' => '22',
                    '21' => '21',
                    '20' => '20',
                    '19' => '19',
                    '18' => '18',
                    '17' => '17',
                    '16' => '16',
                    '15' => '15',
                    '14' => '14',
                    '13' => '13',
                    '12' => '12',
                    '11' => '11',
                    '10' => '10',
                    '9'=> '9',
                    '8'=> '8'
                ),
                'description' => __( 'Size is only used for the Font Awesome icons', 'intense' ),
            ),
            'facebook_tab' => array(
                'type' => 'tab',
                'title' => __( 'Facebook', 'intense' ),
                'fields' =>  array(
                    'show_facebook' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Facebook Button', 'intense' ),
                        'default' => "0",
                    ),
                    'facebook_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => 'button',
                        'options' => array(
                            'button' => __( 'Button', 'intense' ),
                            'button_count' => __( 'Button w/ Count', 'intense' ),
                            'standard' => __( 'Standard', 'intense' ),
                            'box_count' => __( 'Box w/ Count', 'intense' ),
                            'fontawesome' => __( 'Font Awesome Icon', 'intense' ),
                        )
                    ),
                    'facebook_faces' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Friends Faces', 'intense' ),
                        'default' => "0",
                    ),
                    'facebook_share' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Share Button', 'intense' ),
                        'default' => "0",
                    ),
                )
            ),
            'google_plus_tab' => array(
                'type' => 'tab',
                'title' => __( 'Google +', 'intense' ),
                'fields' =>  array(
                    'show_googleplus' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Google +1 Button', 'intense' ),
                        'default' => "0",
                    ),
                    'googleplus_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => 'medium',
                        'options' => array(
                            'medium' => __( 'Medium', 'intense' ),
                            'medium_bubble' => __( 'Medium Bubble', 'intense' ),
                            'medium_inline' => __( 'Medium Inline', 'intense' ),
                            'tall_bubble' => __( 'Tall Bubble', 'intense' ),
                            'fontawesome' => __( 'Font Awesome Icon', 'intense' ),
                        )
                    ),
                )
            ),
            'twitter_tab' => array(
                'type' => 'tab',
                'title' => __( 'Twitter', 'intense' ),
                'fields' =>  array(
                    'show_twitter' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Twitter Button', 'intense' ),
                        'default' => "0",
                    ),
                    'twitter_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => 'none',
                        'options' => array(
                            'none' => __( 'Regular', 'intense' ),
                            'horizontal' => __( 'Horizontal', 'intense' ),
                            'vertical' => __( 'Vertical', 'intense' ),
                            'fontawesome' => __( 'Font Awesome Icon', 'intense' ),
                        )
                    ),
                )
            ),
            'pinterest_tab' => array(
                'type' => 'tab',
                'title' => __( 'Pinterest', 'intense' ),
                'fields' =>  array(
                    'show_pinterest' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Pinterest Pin It Button', 'intense' ),
                        'default' => "0",
                    ),
                    'pinterest_image' => array(
                        'type' => 'image',
                        'title' => __( 'Image', 'intense' ),
                        'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                    ),
                    'pinterest_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => 'none',
                        'options' => array(
                            'none' => __( 'Regular', 'intense' ),
                            'beside' => __( 'Count Right', 'intense' ),
                            'above' => __( 'Count Top', 'intense' ),
                            'fontawesome' => __( 'Font Awesome Icon', 'intense' ),
                        )
                    ),
                )
            ),
            'linkedin_tab' => array(
                'type' => 'tab',
                'title' => __( 'LinkedIn', 'intense' ),
                'fields' =>  array(
                    'show_linkedin' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show LinkedIn Button', 'intense' ),
                        'default' => "0",
                    ),
                    'linkedin_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => 'none',
                        'options' => array(
                            'none' => __( 'No Count', 'intense' ),
                            'right' => __( 'Horizontal', 'intense' ),
                            'top' => __( 'Vertical', 'intense' ),
                            'fontawesome' => __( 'Font Awesome Icon', 'intense' ),
                        )
                    ),
                )
            ),
            'stumbleupon_tab' => array(
                'type' => 'tab',
                'title' => __( 'StumbleUpon', 'intense' ),
                'fields' =>  array(
                    'show_stumbleupon' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show StumbleUpon Button', 'intense' ),
                        'default' => "0",
                    ),
                    'stumbleupon_button' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Button Style', 'intense' ),
                        'default' => '1',
                        'options' => array(
                            '4' => __( 'Layout 4', 'intense' ),
                            '1' => __( 'Layout 1', 'intense' ),
                            '3' => __( 'Layout 3', 'intense' ),
                            '5' => __( 'Layout 5', 'intense' ),
                        )
                    ),
                )
            ),
            'reddit_tab' => array(
                'type' => 'tab',
                'title' => __( 'Reddit', 'intense' ),
                'fields' =>  array(
                    'show_reddit' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Reddit share icon', 'intense' ),
                        'default' => "0",
                    ),
                )
            ),
            'tumblr_tab' => array(
                'type' => 'tab',
                'title' => __( 'Tumblr', 'intense' ),
                'fields' =>  array(
                    'show_tumblr' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Tumblr share icon', 'intense' ),
                        'default' => "0",
                    ),
                )
            ),
            'email_tab' => array(
                'type' => 'tab',
                'title' => __( 'Email', 'intense' ),
                'fields' =>  array(
                    'show_email' => array(
                        'type' => 'checkbox',
                        'title' => __( 'Show Email icon', 'intense' ),
                        'default' => "0",
                    ),
                )
            ),
        );
    }

    function shortcode( $atts, $content = null ) {
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        $output = '';
        $facebook = '';
        $googleplus = '';
        $twitter = '';
        $pinterest = '';
        $linkedin = '';
        $stumbleupon = '';
        $email = '';
        $reddit = '';
        $tumblr = '';        

        if ( isset( $show_email ) && $show_email ) {
            $email = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "envelope",
                    'label' => "Email",
                    'mode' => "fontawesome",
                    'link' => 'mailto:?subject=' . $title . '&amp;body=' . $title . '&#37;&#48;&#65;&#37;&#48;&#65;' . urlencode( __( "Read More", "intensity" ) ) . '&#37;&#48;&#65;&#37;&#48;&#65;' . $share_url,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                  ) );
            $output .= $email;
        }

        if ( isset( $show_facebook ) && $show_facebook ) {
            if ( $facebook_button == 'fontawesome' ) {
                $facebook = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "facebook",
                    'label' => "Facebook",
                    'mode' => "fontawesome",
                    'link' => 'http://www.facebook.com/sharer.php?u=' . $share_url . '&amp;t=' . $title,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                ) );
            } else {
                intense_add_script( 'facebook_share' );
                $facebook = '<div style="padding-right: 20px; display:inline-block; vertical-align: top;"><div class="fb-like" data-href="' . $share_url . '" data-layout="' . $facebook_button . '" data-action="like" data-show-faces="' . ( $facebook_faces == '0' ? 'false' : 'true' ) . '" data-share="' . ( $facebook_share == '0' ? 'false' : 'true' ) . '"></div></div>';
            }
            
            $output .= $facebook;
        }

        if ( isset( $show_googleplus ) &&  $show_googleplus ) {
            if ( $googleplus_button == 'medium' ) {
                $annotation = 'none';
            }
            else if ( $googleplus_button == 'medium_inline' ) {
                $annotation = 'inline';
            }
            else {
                $annotation = 'bubble';
            }

            if ( $googleplus_button == 'fontawesome' ) {
                $googleplus = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "google-plus",
                    'label' => "Google+",
                    'mode' => "fontawesome",
                    'link' => 'https://plus.google.com/share?url=' . $share_url,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                ) ); 
            } else {
                intense_add_script( 'googleplus_share' );
                $googleplus = '<div style="/* padding-right: 20px; */ display:inline;"><div class="g-plusone" data-size="' . ( $googleplus_button == 'tall_bubble' ? 'tall' : 'medium' ) . '" data-annotation="' . $annotation . '" data-href="' . $share_url . '"></div></div>';
            }

            $output .= $googleplus;
        }

        if ( isset( $show_twitter ) &&  $show_twitter ) {
            if ( $twitter_button == 'fontawesome' ) {
                $twitter = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "twitter",
                    'label' => "Twitter",
                    'mode' => "fontawesome",
                    'link' => 'http://twitter.com/home?status=' . $title . "%20" . $share_url,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                ) );
            } else {
                intense_add_script( 'twitter_share' );
                $twitter = '<div style="display:inline; /* padding-right: 20px; */"><a href="https://twitter.com/share" class="twitter-share-button" data-url="' . $share_url . '" data-lang="en" data-count="' . $twitter_button . '">Tweet</a></div>';
            }

            $output .= $twitter;
        }

        if ( isset( $show_linkedin ) &&  $show_linkedin ) {
            if ( $linkedin_button == 'fontawesome' ) {
                $linkedin = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "linkedin",
                    'label' => "LinkedIn",
                    'mode' => "fontawesome",
                    'link' => 'http://linkedin.com/shareArticle?mini=true&amp;url=' . $share_url . '&amp;title=' . $title,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                ) );
            } else {
                intense_add_script( 'linkedin_share' );
                $linkedin = '<div style="display:inline; padding-right: 20px;"><script type="IN/Share" data-url="' . $share_url . '" ' . ( $linkedin_button == 'none' ? '' : 'data-counter="' . $linkedin_button . '"' ) . '></script></div>';
            }            

            $output .= $linkedin;
        }

        if ( isset( $show_pinterest ) &&  $show_pinterest ) {
            if ( is_numeric( $pinterest_image ) ) {
                $imageid = $pinterest_image;
            } else if ( !empty( $pinterest_image ) ) {
                $imageurl = $pinterest_image;
            }

            if ( !empty( $imageid ) ) {
                $photo_info = wp_get_attachment_image_src( $imageid, 'medium' );
                $photo_url = $photo_info[0];
                $width = $photo_info[1];
            } else if ( !empty( $imageurl ) ) {
                $photo_url = $imageurl;
            } else {
                $photo_url = '';
            }

            if ( $pinterest_button == 'fontawesome' ) {
                $pinterest = intense_run_shortcode( 'intense_social_icon', array(
                    'type' => "pinterest",
                    'label' => "Pinterest",
                    'mode' => "fontawesome",
                    'link' => 'http://pinterest.com/pin/create/button/?url=' . $share_url . '&amp;media=' . $photo_url . '&amp;description=' . $title,
                    'size' => $size,
                    'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                ) );
            } else {
                intense_add_script( 'pinterest_share' );
                $pinterest = '<div style="display:inline; padding-right: 20px;"><a href="//www.pinterest.com/pin/create/button/?url=' . $share_url . '&media=' . $photo_url . '&description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="' . $pinterest_button . '"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div>';
            }

            $output .= $pinterest;
        }

        if ( isset( $show_reddit ) &&  $show_reddit ) {
            $reddit = intense_run_shortcode( 'intense_social_icon', array(
                'type' => "icon-share",
                'label' => "Reddit",
                'mode' => "fontawesome",
                'link' => 'http://reddit.com/submit?url=' . $share_url . '&amp;title=' . $title,
                'size' => $size,
                'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
            ) );

            $output .= $reddit;
        }

        if ( isset( $show_tumblr ) &&  $show_tumblr ) {
            $tumblr_url = str_replace( 'http://', '', str_replace( 'https://', '', $share_url ) );
            $tumblr = intense_run_shortcode( 'intense_social_icon', array(
                'type' => "tumblr",
                'label' => "Tumblr",
                'mode' => "fontawesome",
                'link' => 'https://www.tumblr.com/share?v=3&amp;u=' . $tumblr_url . '&amp;t=' . $title . '&amp;s=',
                'size' => $size,
                'onclick' => "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
            ) );

            $output .= $tumblr;
        }

        if ( isset( $show_stumbleupon ) &&  $show_stumbleupon ) {
            intense_add_script( 'stumbleupon_share' );

            $stumbleupon = '<div style="display:inline; padding-right: 20px;"><su:badge layout="' . $stumbleupon_button . '" location="' . $share_url . '"></su:badge></div>';

            $output .= $stumbleupon;
        }

        return $output;
    }

}