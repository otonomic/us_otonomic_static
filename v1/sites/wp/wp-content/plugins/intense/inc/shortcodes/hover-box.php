<?php

class Intense_Hover_Box extends Intense_Shortcode {

    function __construct() {
        global $intense_visions_options;

        $this->title = __( 'Hover Box', 'intense' );
        $this->category = __( 'Layout', 'intense' );
        $this->icon = 'dashicons-intense-box';
        $this->show_preview = true;
        $this->preview_wrapper_start = '<div style="max-width: 600px;">';
        $this->preview_wrapper_end = "</div>";

        $this->fields = array(
            'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
                    'image' => array(
                        'type' => 'image',
                        'title' => __( 'Image', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'image_id' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
                    'image_url' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
                    'type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Type', 'intense' ),
                        'default' => 'style',
                        'options' => array(
                            'style' => __( 'Style 1', 'intense' ),
                            'style2' => __( 'Style 2', 'intense' ),
                            'style3' => __( 'Style 3', 'intense' ),
                            'style4' => __( 'Style 4', 'intense' ),
                            'style5' => __( 'Style 5', 'intense' ),
                            'style6' => __( 'Style 6', 'intense' ),
                            'style7' => __( 'Style 7', 'intense' ),
                        )
                    ),
                    'size' => array(
                        'type' => 'image_size',
                        'title' => __( 'Image Size', 'intense' ),
                        'default' => 'square400'
                    ),
                    'title' => array(
                        'type' => 'text',
                        'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'title_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Title Color', 'intense' ),
                    ),
                    'name' => array(
                        'type' => 'text',
                        'title' => __( 'Name', 'intense' ),
                        'description' => __( 'the author name or a short name for the image', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'name_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Name Color', 'intense' ),
                    ),
                    'divider_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Divider Color', 'intense' ),
                    ),
                    'background_image' => array(
                        'type' => 'image',
                        'title' => __( 'Background Image', 'intense' ),
                        'class' => 'backgroundimagesettings',
                        'skinnable' => '0'
                    ),
                    'background_imageid' => array( 'type' => 'deprecated', 'description' => 'use background_image instead' ),
                    'background_image_url' => array( 'type' => 'deprecated', 'description' => 'use background_image instead' ),
                    'background_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Background Color', 'intense' ),
                        'default' => '#ffffff',
                    ),
                    'background_opacity' => array(
                        'type' => 'text',
                        'title' => __( 'Background Opacity', 'intense' ),
                        'description' => __( '0 - 100: leave blank for solid color', 'intense' ),
                        'default' => '80',
                    ),
                    'background_center_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Background Color', 'intense' ),
                        'default' => '#ffffff',
                        'class' => 'backgroundcentersettings'
                    ),
                    'rtl' => array(
                        'type' => 'hidden',
                        'description' => __( 'right-to-left', 'intense' ),
                        'default' => $intense_visions_options['intense_rtl']
                    )
                ),
            ),
            'link_tab' => array(
                'type' => 'tab',
                'title' => __( 'Link', 'intense' ),
                'fields' =>  array(
                    'linktext' => array(
                        'type' => 'text',
                        'title' => __( 'Link Text', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'linktext_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Link Text Color', 'intense' ),
                    ),
                    'linkhover_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Link Hover Text Color', 'intense' ),
                    ),
                    'linkurl' => array(
                        'type' => 'text',
                        'title' => __( 'Link URL', 'intense' ),
                        'skinnable' => '0'
                    ),
                    'linkurl_target' => array(
                        'type' => 'link_target',
                        'title' => __( 'Link Target', 'intense' ),
                        'default' => '_self'
                    ),
                ),
            ),
            'border_tab' => array(
                'type' => 'tab',
                'title' => __( 'Border', 'intense' ),
                'fields' =>  array(
                    'border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Border Radius', 'intense' ),
                    ),
                    'border_width' => array(
                        'type' => 'text',
                        'title' => __( 'Border Width', 'intense' ),
                        'default' => "16"
                    ),
                    'border_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Border Color', 'intense' ),
                        'default' => '#ffffff'
                    ),
                ),
            ),            
        );
    }

    function shortcode( $atts, $content = null ) {
        global $intense_visions_options;
        extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

        intense_add_style( 'intense_hover_box' );
        intense_add_script( 'intense.hover.box' );

        $title_color = intense_get_plugin_color( $title_color );
        $name_color = intense_get_plugin_color( $name_color );
        $linktext_color = intense_get_plugin_color( $linktext_color );
        $linkhover_color = intense_get_plugin_color( $linkhover_color );
        $background_color = intense_get_plugin_color( $background_color );
        $border_color = intense_get_plugin_color( $border_color );
        $divider_color = intense_get_plugin_color( $divider_color );
        $background_center_color = intense_get_plugin_color( $background_center_color );

        if ( !is_numeric( $border_width ) ) {
            $border_width = '16';
        }

        if ( is_numeric( $image ) ) {
            $image_id = $image;
        } else if ( !empty( $image ) ) {
                $image_url = $image;
            }

        if ( !empty( $image_id ) ) {
            $photo_info = wp_get_attachment_image_src( $image_id, $size );
            $photo_url = $photo_info[0];
        } else if ( !empty( $image_url ) ) {
                $photo_url = $image_url;
            }

        $wordpress_size = Intense_Photo_Source::get_wordpress_size( $size );

        $width = $wordpress_size["width"];
        $height = $wordpress_size["height"];
        $random = rand();
        $itemClass = "ch-item-" . $random;
        $imgClass = "ch-img-" . $random;
        $infoClass = "ch-info-" . $random;
        $gridClass = "ch-grid-" . $random;
        $backClass = "ch-info-back-" . $random;
        $wrapClass = "ch-info-wrap-" . $random;
        $thumbClass = "ch-thumb-" . $random;
        $liClass = "ch-li-" . $random;
        $borderWidth = ( $border_width * 2 );
        $halfHeight = $height / 2;
        $innerHeight = $height - $borderWidth;
        $innerWidth = $width - $borderWidth;

        if ( is_numeric( $background_image ) ) {
            $background_imageid = $background_image;
        } else if ( !empty( $background_image ) ) {
                $background_image_url = $background_image;
            }

        if ( !empty( $background_imageid ) ) {
            $photo_info = wp_get_attachment_image_src( $background_imageid, $size );
            $background_image_url = $photo_info[0];
        } else if ( !empty( $background_image_url ) ) {
                $background_image_url = $background_image_url;
            }

        if ( $type == 'style' ) {
            $h3Height = $halfHeight;
        } elseif ( $type == 'style2' ) {
            $h3Height = $height / 3;
        } elseif ( $type == 'style3' ) {
            $h3Height = $halfHeight;
        } elseif ( $type == 'style4' || $type == 'style5' || $type == 'style6' ) {
            $h3Height = $innerHeight / 4;
        } else {
            $h3Height = $innerHeight / 3;
        }

        if ( $type == 'style' && isset( $background_opacity ) && isset( $background_color ) ) {
            $background_style = "background: " . intense_get_rgb_color( $background_color, $background_opacity ) . " !important; " . intense_get_rgb_color( $background_color ) . "\9 !important;";
        } else {
            $background_style = "";
        }

        $output = '';

        $output .= "<div id='outer-" . $gridClass . "' class='intense-hover-box' data-box-type='" . $type . "' data-aspect-ratio='" . ( $width / $height ) . "' data-border-width='" . $borderWidth . "' data-random='" . $random . "' data-background-rgb='" . intense_get_rgb_color( $background_color, $background_opacity ) . "' data-border-rgb='" . intense_get_rgb_color( $border_color, '80' ) . "'>";
        $output .= "<ul id='hover-" . $gridClass . "' class='ch-grid " . $gridClass . "'>";
        $output .= "<li class='" . $liClass . "'>";

        if ( $type == 'style' || $type == 'style2' ) {
            $output .= "<div id='" . $itemClass . "' class='ch-item-" . $type . " " . $imgClass . "'>";
            $output .= "<div class='ch-info-" . $type . " " . $infoClass . "'>";
            $output .= "<h3" . ( isset( $title_color ) ? " style='color: " . $title_color . ";'" : "" ) . ">" . $title . "</h3>";
            $output .= "<p" . ( isset( $name_color ) ? " style='color: " . $name_color . ";'" : "" ) . ">" . $name . " <a href='" . $linkurl . "' target='" . $linkurl_target . "'" . ( isset( $linktext_color ) ? " style='color: " . $linktext_color . "; color: " . intense_get_rgb_color( $linktext_color, '70' ) . " !important;'" : "" ) . ">" . $linktext . "</a></p>";
            $output .= "</div>";
            $output .= "</div>";
        } elseif ( $type == 'style3' ) {
            $output .= "<div id='" . $itemClass . "' class='ch-item-" . $type . "'>";
            $output .= "<div class='ch-info-" . $type . "'>";
            $output .= "<h3" . ( isset( $title_color ) ? " style='color: " . $title_color . ";'" : "" ) . ">" . $title . "</h3>";
            $output .= "<p" . ( isset( $name_color ) ? " style='color: " . $name_color . ";'" : "" ) . ">" . $name . " <a href='" . $linkurl . "' target='" . $linkurl_target . "'" . ( isset( $linktext_color ) ? " style='color: " . $linktext_color . "; color: " . intense_get_rgb_color( $linktext_color, '70' ) . " !important;'" : "" ) . ">" . $linktext . "</a></p>";
            $output .= "</div>";
            $output .= "<div class='ch-thumb-" . $type . " " . $imgClass . "'></div>";
            $output .= "</div>";
        } elseif ( $type == 'style4' || $type == 'style5' || $type == 'style6' ) {
            $output .= "<div id='" . $itemClass . "' class='ch-item-" . $type . " " . $imgClass . "'>";
            $output .= "<div class='ch-info-wrap-" . $type . " " . $wrapClass . "'>";
            $output .= "<div class='ch-info-" . $type . " " . $infoClass . "'>";
            $output .= "<div class='ch-info-front-" . $type . " " . $imgClass . "'></div>";
            $output .= "<div class='ch-info-back-" . $type . " " . $backClass . "'>";
            $output .= "<h3" . ( isset( $title_color ) ? " style='color: " . $title_color . ";'" : "" ) . ">" . $title . "</h3>";
            $output .= "<p" . ( isset( $name_color ) ? " style='color: " . $name_color . ";'" : "" ) . ">" . $name . " <a href='" . $linkurl . "' target='" . $linkurl_target . "'" . ( isset( $linktext_color ) ? " style='color: " . $linktext_color . "; color: " . intense_get_rgb_color( $linktext_color, '70' ) . " !important;'" : "" ) . ">" . $linktext . "</a></p>";
            $output .= "</div>";
            $output .= "</div>";
            $output .= "</div>";
            $output .= "</div>";
        } elseif ( $type == 'style7' ) {
            $output .= "<div id='" . $itemClass . "' class='ch-item-" . $type . "'>";
            $output .= "<div class='ch-info-" . $type . " " . $infoClass . "'>";
            $output .= "<div class='ch-info-front-" . $type . " " . $imgClass . "'></div>";
            $output .= "<div class='ch-info-back-" . $type . " " . $backClass . "'>";
            $output .= "<h3" . ( isset( $title_color ) ? " style='color: " . $title_color . ";'" : "" ) . ">" . $title . "</h3>";
            $output .= "<p" . ( isset( $name_color ) ? " style='color: " . $name_color . ";'" : "" ) . ">" . $name . " <a href='" . $linkurl . "' target='" . $linkurl_target . "'" . ( isset( $linktext_color ) ? " style='color: " . $linktext_color . "; color: " . intense_get_rgb_color( $linktext_color, '70' ) . " !important;'" : "" ) . ">" . $linktext . "</a></p>";
            $output .= "</div>";
            $output .= "</div>";
            $output .= "</div>";
        }

        $output .= "</li>";
        $output .= "</ul>";
        $output .= "</div>";

        $output .= "<style>";

        if ( $type == 'style' ) {
            $output .= "#" . $itemClass . ".ch-item-" . $type . " {";
            $output .= "box-shadow: inset 0 0 0 " . $border_width . "px " . intense_get_rgb_color( $border_color, '60' ) . ", 0 1px 2px rgba(0,0,0,0.1);";
            $output .= "}";
            $output .= "#" . $itemClass . ".ch-item-" . $type . ":hover {";
            $output .= "box-shadow: inset 0 0 0 1px " . intense_get_rgb_color( $border_color, '10' ) . ", 0 1px 2px rgba(0,0,0,0.1);";
            $output .= "}";
            $output .= ".ch-item-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " { border-radius: " . $border_radius . " !important; }";
        } elseif ( $type == 'style2' ) {
            $output .= "#" . $itemClass . ".ch-item-" . $type . " {";
            $output .= "box-shadow: inset 0 0 0 0 " . intense_get_rgb_color( $background_color, $background_opacity ) . ", inset 0 0 0 " . $border_width . "px " . intense_get_rgb_color( $border_color, '80' ) . ", 0 1px 2px rgba(0,0,0,0.1);";
            $output .= "}";
            $output .= ".ch-item-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " { border-radius: " . $border_radius . " !important; }";
        } elseif ( $type == 'style3' ) {
            $output .= ".ch-item-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-thumb-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-thumb-" . $type . ":after { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " { border-radius: " . $border_radius . " !important; " . ( isset( $background_imageid ) ? "background: " . $background_color . " url(" . $background_image_url . ") !important;" : "background: " . $background_color . " !important;" ) . " }";
            $output .= ".ch-info-" . $type . " p a { border-radius: " . $border_radius . " !important; }";
        } elseif ( $type == 'style4' || $type == 'style5' || $type == 'style6' ) {
            $output .= "." . $backClass . " {";
            $output .= ( isset( $background_imageid ) ? "background: " . $background_color . " url(" . $background_image_url . ") !important;" : "background: " . $background_color . " !important;" );
            $output .= "}";
            $output .= "." . $wrapClass . " {";
            $output .= "top: " . $border_width . "px !important;";
            $output .= "left: " . $border_width . "px !important;";
            $output .= "background: " . $background_center_color . " !important;";
            $output .= "box-shadow: 0 0 0 " . $border_width . "px " . intense_get_rgb_color( $border_color, '20' ) . ", inset 0 0 3px " . intense_get_rgb_color( $background_center_color, '80' ) . " !important;";
            $output .= "}";
            $output .= "." . $wrapClass . ":hover {";
            $output .= "box-shadow: 0 0 0 0 " . intense_get_rgb_color( $border_color, '80' ) . ", inset 0 0 3px " . intense_get_rgb_color( $background_center_color, '80' ) . " !important;";
            $output .= "}";
            $output .= ".ch-item-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-wrap-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " > div { border-radius: " . $border_radius . " !important; }";
        } elseif ( $type == 'style7' ) {
            $output .= "." . $backClass . " {";
            $output .= ( isset( $background_imageid ) ? "background: " . $background_color . " url(" . $background_image_url . ") !important;" : "background: " . $background_color . " !important;" );
            $output .= "}";
            $output .= ".ch-item-" . $type . " { border-radius: " . $border_radius . " !important; }";
            $output .= ".ch-info-" . $type . " > div { border-radius: " . $border_radius . " !important; }";
        }

        $output .= "." . $imgClass . " {";
        if ( isset( $photo_url ) ) {
            $output .= "background-image: url(" . $photo_url . ");";
        }
        $output .= "background-repeat: no-repeat;";
        $output .= "}";
        $output .= "." . $infoClass . " {";
        $output .= $background_style;
        $output .= "}";

        if ( isset( $divider_color ) ) {
            $output .= "." . $infoClass . " p {";
            $output .= "border-top: 1px solid " . intense_get_rgb_color( $divider_color, '50' ) . " !important;";
            $output .= "}";
        }

        if ( isset( $linkhover_color ) ) {
            $output .= "." . $infoClass . " p a:hover {";
            $output .= "color: " . $linkhover_color . " !important;";
            $output .= "color: " . intense_get_rgb_color( $linkhover_color, '70' ) . " !important;";
            $output .= "}";
        }

        $output .= "</style>";

        return $output;
    }

    function render_dialog_script() {
?>
        <script>
            jQuery(function($) {
                // Uploading files
                $('.upload_image_button').live('click', function( event ){
                  event.preventDefault();
                  var target = $(this).data('target-id');

                  window.parent.loadImageFrame($(this), function(attachment) {
                    jQuery("#" + target).val(attachment.id);
                    
                    if ( attachment.sizes.thumbnail ) {
                      jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
                    } else {
                      jQuery("#" + target + "-thumb").attr("src", attachment.url);
                    }

                    if ( $('#preview-content').length > 0 ) {
                      $('#preview').click();
                    }
                  });
                });

                $(".backgroundimagesettings").hide();

                $("#type").change(function() {
                    var type = $(this).val();

                    $(".backgroundimagesettings").hide();

                    switch(type) {
                        case "style3":
                        case "style4":
                        case "style5":
                        case "style6":
                        case "style7":
                            $(".backgroundimagesettings").show();
                            break;
                        case "":
                            $(".backgroundimagesettings").hide();
                        default:
                            $(".backgroundimagesettings").hide();
                            break;
                    }
                });

                $(".backgroundcentersettings").hide();

                $("#type").change(function() {
                    var type = $(this).val();

                    $(".backgroundcentersettings").hide();

                    switch(type) {
                        case "style4":
                        case "style5":
                        case "style6":
                            $(".backgroundcentersettings").show();
                            break;
                        case "":
                            $(".backgroundcentersettings").hide();
                        default:
                            $(".backgroundcentersettings").hide();
                            break;
                    }
                });
            });
        </script>
    <?php
    }
}
