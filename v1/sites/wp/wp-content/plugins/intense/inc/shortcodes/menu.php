<?php

class Intense_Menu extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Menu', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-menu';
		$this->show_preview = true;				

		$this->fields = array(
			'menu_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'type' => array(
						'type' => 'dropdown',
						'title' => __( 'type', 'intense' ),
						'default' => 'vertical',
						'options' => array(
							'vertical' => __( 'Vertical', 'intense' ),
							'horizontal' => __( 'Horizontal', 'intense' ),
						)
					),
					'name' => array(
						'type' => 'dropdown',
						'title' => __( 'Name', 'intense' ),
						'options' => $this->get_menus_option_array()
					),
					'theme_location' => array(
						'type' => 'text',
						'title' => __( 'Theme Location', 'intense' ),
						'description' => __( 'used during theme development and customization. Use the key not the description of the location.', 'intense' ),
					),
					'background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Background Color', 'intense' ),
					),
					'font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Font Color', 'intense' ),
					),
					'hover_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Hover Background Color', 'intense' ),
					),
					'hover_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Hover Font Color', 'intense' ),
					),
					'top_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Top Level Background Color', 'intense' ),
					),
					'top_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Top Level Font Color', 'intense' ),
					),
					'top_hover_background_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Top Level Hover Background Color', 'intense' ),
					),
					'top_hover_font_color' => array(
						'type' => 'color_advanced',
						'title' => __( 'Top Level Hover Font Color', 'intense' ),
					),
					'icon_before' => array(
						'type' => 'icon',
						'title' => __( 'Icon Before', 'intense' ),
					),
					'icon_before_parents_only' => array(
						'type' => 'checkbox',
						'title' => __( 'Icon Before Parents Only', 'intense' ),
						'description' => __( 'show the \'icon before\' only if menu item is a parent', 'intense' ),
						'default' => '1'
					),
					'icon_after' => array(
						'type' => 'icon',
						'title' => __( 'Icon After', 'intense' ),
						'default' => 'angle-right'
					),
					'icon_after_parents_only' => array(
						'type' => 'checkbox',
						'title' => __( 'Icon After Parents Only', 'intense' ),
						'description' => __( 'show the \'icon after\' only if menu item is a parent', 'intense' ),
						'default' => '1'
					),
					'id' => array(
						'type' => 'text',
						'title' => __( 'ID', 'intense' ),
						'description' => __( 'optional - used to set the client-side ID', 'intense' ),
                        'skinnable' => '0'
					),
					'class' => array(
						'type' => 'text',
						'title' => __( 'CSS Class', 'intense' ),
					),
				)
			),
			'borders_tab' => array(
				'type' => 'tab',
				'title' => __( 'Borders', 'intense' ),
				'fields' =>  array(
					'border' => array(
						'type' => 'border',
						'title' => __( 'All Borders', 'intense' ),
					),
					'border_top' => array(
						'type' => 'border',
						'title' => __( 'Top Border', 'intense' ),
					),
					'border_right' => array(
						'type' => 'border',
						'title' => __( 'Right Border', 'intense' ),
					),
					'border_bottom' => array(
						'type' => 'border',
						'title' => __( 'Bottom Border', 'intense' ),
					),
					'border_left' => array(
						'type' => 'border',
						'title' => __( 'Left Border', 'intense' ),
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
					),
				)
			),
			'padding_tab' => array(
                'type' => 'tab',
                'title' => __( 'Item Padding', 'intense' ),
                'fields' =>  array(
                    'padding_top' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Top', 'intense' ),
                    ),
                    'padding_bottom' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Bottom', 'intense' ),
                    ),
                    'padding_left' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Left', 'intense' ),
                    ),
                    'padding_right' => array(
                        'type' => 'text',
                        'title' => __( 'Padding Right', 'intense' ),
                    ),
                )
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		intense_add_style( 'intense.menu' );
		intense_add_script( 'intense.menu' );

		if ( $type == 'horizontal' ) {
			intense_add_script( 'hoverintent' );
			intense_add_script( 'superfish' );
			intense_add_script( 'slicknav');
			intense_add_style( 'slicknav');
		}

		if ( empty( $name ) && empty( $theme_location ) ) return;

		if ( empty( $id ) ) $id = 'menu_' . rand();

		$output = '';
		$style = '';
		$hover_style = '';
		$radius = '';
		$top_style = '';
		$top_hover_style = '';

		$background_color = intense_get_plugin_color( $background_color );
		$font_color = intense_get_plugin_color( $font_color );
		$hover_background_color = intense_get_plugin_color( $hover_background_color );
		$hover_font_color = intense_get_plugin_color( $hover_font_color );
		$top_font_color = intense_get_plugin_color( $top_font_color );
		$top_hover_font_color = intense_get_plugin_color( $top_hover_font_color );
		$top_hover_background_color = intense_get_plugin_color( $top_hover_background_color ); 
		$top_background_color =   intense_get_plugin_color( $top_background_color );

		if ( !empty( $background_color ) ) $style .= 'background: ' . $background_color . ' !important;';
		if ( !empty( $font_color ) ) $style .= 'color: ' . $font_color . ' !important;';
		if ( !empty( $hover_background_color ) ) $hover_style .= 'background: ' . $hover_background_color . ' !important;';
		if ( !empty( $hover_font_color ) ) $hover_style .= 'color: ' . $hover_font_color . ' !important;';

		if ( !empty( $top_background_color ) ) $top_style .= 'background: ' . $top_background_color . ' !important;';
		if ( !empty( $top_font_color ) ) $top_style .= 'color: ' . $top_font_color . ' !important;';
		if ( !empty( $top_hover_background_color ) ) $top_hover_style .= 'background: ' . $top_hover_background_color . ' !important;';
		if ( !empty( $top_hover_font_color ) ) $top_hover_style .= 'color: ' . $top_hover_font_color . ' !important;';

		$border_style = null;
		$border_top_style = null;
		$border_right_style = null;
		$border_bottom_style = null;
		$border_left_style = null;

		if ( isset( $border ) ) $border_style = "border: " . $border . ' !important;';
		if ( isset( $border_top ) ) $border_top_style = "border-top: " . $border_top . ' !important;';
		if ( isset( $border_right ) ) $border_right_style = "border-right: " . $border_right . ' !important;';
		if ( isset( $border_bottom ) ) $border_bottom_style = "border-bottom: " . $border_bottom . ' !important;';
		if ( isset( $border_left ) ) $border_left_style = "border-left: " . $border_left . ' !important;';
		if ( isset( $border_radius ) ) $radius = 'border-radius:' . $border_radius . ';';

		if ( is_numeric( $padding_top ) && $padding_top != 0 ) $padding_top .= "px";
        if ( is_numeric( $padding_bottom ) && $padding_bottom != 0 ) $padding_bottom .= "px";
        if ( is_numeric( $padding_left ) && $padding_left != 0 ) $padding_left .= "px";
        if ( is_numeric( $padding_right ) && $padding_right != 0 ) $padding_right .= "px";

        $padding = ( !empty( $padding_top ) ? ' padding-top: ' . $padding_top . '; ' : '' ) .
            ( !empty( $padding_bottom ) ? ' padding-bottom: ' . $padding_bottom . '; ' : '' ) .
            ( !empty( $padding_left ) ? ' padding-left: ' . $padding_left . '; ' : '' ) .
            ( !empty( $padding_right ) ? ' padding-right: ' . $padding_right . '; ' : '' );

		$style .= $border_style . $border_top_style . $border_right_style . $border_bottom_style . $border_left_style . $radius . $padding;

		if ( !empty( $style ) || !empty( $hover_style ) || !empty( $top_style ) || !empty( $top_hover_style ) ) {
			$output .= '<style>';
			$output .= !empty( $style ) ? '#' . $id . ' li > a {' . $style . '}' : '';
			$output .= !empty( $hover_style ) ? '#' . $id . ' li > a:hover {' . $hover_style . '}' : '';
			$output .= !empty( $hover_style ) ? '#' . $id . ' li.current-menu-item, #' . $id . ' li.current-menu-item > a {' . $hover_style . '}' : '';

			$output .= !empty( $top_style ) ? '#' . $id . ' > li > a  {' . $top_style . '}' : '';
			$output .= !empty( $top_hover_style ) ? '#' . $id . ' > li > a:hover {' . $top_hover_style . '}' : '';
			//$output .= !empty( $top_hover_style ) ? '#' . $id . ' > li, #' . $id . ' > li > a {' . $top_hover_style . '}' : '';
			$output .= '</style>';
		}		

		$link_before = !empty( $icon_before ) ? intense_run_shortcode( 'intense_icon', array( 'type' => $icon_before, 'extra_class' => 'hide icon-before' . ( $icon_before_parents_only ? ' only-parents' : '' ) ) ) : '';
		$link_after = !empty( $icon_after ) ? intense_run_shortcode( 'intense_icon', array( 'type' => $icon_after, 'extra_class' => 'hide icon-after' . ( $icon_after_parents_only ? ' only-parents' : '' )) ) : '';

		$menu_args = array(
			'echo' => false,
			'menu' => $name,
			'menu_id' => $id,
			'theme_location' => $theme_location,
			'container' => false,
			'fallback_cb' => array( __CLASS__, 'fallback_cb' ),
			'link_before' => $link_before,
			'link_after' => $link_after,
			'menu_class'=> 'intense menu' . ( !empty( $class ) ? ' ' . $class : '' ) . ( $type == 'horizontal' ? ' horizontal sf-menu' : ' vertical' )
		);

		$output .= '<span id="' . $id . '_parent">';

		$output .= wp_nav_menu( $menu_args );

		if ( $type == 'horizontal'  ) {
			$menu_args['menu_id'] = $id . '_mobile';
			$menu_args['menu_class'] = 'intense menu mobile';

			$output .= wp_nav_menu( $menu_args );
		}

		$output .= '</span>';

		return $output;
	}

	public static function fallback_cb() {
		return __( "This menu either doesn't exist or is empty", 'intense' );
	}

	function get_menus() {
		return get_terms( 'nav_menu', array( 'hide_empty' => true ) );
	}

	/**
	 * Gets a list of menus that can be used for the dropdown field
	 *
	 * @return array list of menus with the name as the key and value
	 */
	function get_menus_option_array() {
		$options = array('' => '');
		$menus = $this->get_menus();

		foreach ( $menus as $key => $menu ) {
			$options[ $menu->name ] = $menu->name;
		}

		return $options;
	}	
}
