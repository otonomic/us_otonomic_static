<?php

class Intense_Guests extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;
		global $post;

		$this->title = __( 'Guests', 'intense' );
		$this->category = __( 'Elements', 'intense' );
		$this->icon = 'dashicons-visibility';
		$this->preview_content = __( "Guest Only Content", 'intense' );
		$this->show_preview = false;
		$this->is_container = true;

		$this->fields = array(
			'class' => array(
				'type' => 'text',
				'title' => __( 'CSS Class', 'intense' ),
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		if ( is_feed() ) return;

		if ( !is_user_logged_in() ) {
			return '<div class="intense guests' . $class . '">' . do_shortcode( $content ) . '</div>';
		} else {
			return;
		}		
	}
}
