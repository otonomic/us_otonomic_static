<?php

class Intense_Person extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Person', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-user';
		$this->show_preview = true;
		$this->preview_content = __( 'Person Content', 'intense' );
		$this->is_container = true;

		$this->fields = array(
			'general_tab' => array(
				'type' => 'tab',
				'title' => __( 'General', 'intense' ),
				'fields' =>  array(
					'template' => array(
						'type' => 'template',
						'title' => __( 'Template', 'intense' ),
						'path' => '/person/',
						'default' => 'top'
					),
					'image' => array(
						'type' => 'image',
						'title' => __( 'Image', 'intense' ),
						'description' => __( 'enter a WordPress attachment ID or a URL', 'intense' ),
                        'skinnable' => '0'
					),
					'imageid' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'imageurl' => array( 'type' => 'deprecated', 'description' => 'use image instead' ),
					'imageposition' => array( 'type' => 'deprecated', 'description' => 'use template instead' ),
					'imageshadow' => array(
						'type' => 'shadow',
						'title' => __( 'Shadow', 'intense' ),
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
					),
					'size' => array(
						'type' => 'image_size',
						'title' => __( 'Image Size', 'intense' ),
						'default' => 'large'
					),
					'name' => array(
						'type' => 'text',
						'title' => __( 'Name', 'intense' ),
                        'skinnable' => '0'
					),
					'title' => array(
						'type' => 'text',
						'title' => __( 'Title', 'intense' ),
                        'skinnable' => '0'
					),
				)
			),
			'social_tab' => array(
				'type' => 'tab',
				'title' => __( 'Social', 'intense' ),
				'fields' =>  array(
					'facebook' => array(
						'type' => 'text',
						'title' => __( 'Facebook URL', 'intense' ),
                        'skinnable' => '0'
					),
					'twitter' => array(
						'type' => 'text',
						'title' => __( 'Twitter URL', 'intense' ),
                        'skinnable' => '0'
					),
					'linkedin' => array(
						'type' => 'text',
						'title' => __( 'LinkedIn URL', 'intense' ),
                        'skinnable' => '0'
					),
					'googleplus' => array(
						'type' => 'text',
						'title' => __( 'Google+ URL', 'intense' ),
                        'skinnable' => '0'
					),
					'dribbble' => array(
						'type' => 'text',
						'title' => __( 'Dribbble URL', 'intense' ),
                        'skinnable' => '0'
					),
					'instagram' => array(
						'type' => 'text',
						'title' => __( 'Instagram URL', 'intense' ),
                        'skinnable' => '0'
					),
					'tumblr' => array(
						'type' => 'text',
						'title' => __( 'Tumblr URL', 'intense' ),
                        'skinnable' => '0'
					),
					'pinterest' => array(
						'type' => 'text',
						'title' => __( 'Pinterest URL', 'intense' ),
                        'skinnable' => '0'
					),
					'youtube' => array(
						'type' => 'text',
						'title' => __( 'Youtube URL', 'intense' ),
                        'skinnable' => '0'
					),
					'social_target' => array(
						'type' => 'link_target',
						'title' => __( 'Target', 'intense' ),
						'default' => '_self'
					),
					'social_size' => array(
						'type' => 'dropdown',
						'title' => __( 'Size', 'intense' ),
						'options' => array(
							"32" => '32',
							"31" => '31',
							"30" => '30',
							"29" => '20',
							"28" => '28',
							"27" => '27',
							"26" => '26',
							"25" => '25',
							"24" => '24',
							"23" => '23',
							"22" => '22',
							"21" => '21',
							"20" => '20',
							"19" => '19',
							"18" => '18',
							"17" => '17',
							"16" => '16',
							"15" => '15',
							"14" => '14',
							"13" => '13',
							"12" => '12',
							"11" => '11',
							"10" => '10',
							"9" => '9',
							"8" => '8'
						),
						'default' => '22'
					),
					'person_custom_social' => array(
						'type' => 'repeater',
						'title' => __( 'Custom Social Links', 'intense' ),
						'fields' => array(
							'title' => array(
								'type' => 'text',
								'title' => __( 'Title', 'intense' ),
                        		'skinnable' => '0'
							),
							'link' => array(
								'type' => 'text',
								'title' => __( 'Link', 'intense' ),
								'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                        		'skinnable' => '0'
							),
							'image' => array(
								'type' => 'image',
								'title' => __( 'Image', 'intense' ),
                        		'skinnable' => '0'
							),
						),
					),
					'custom_social' => array( 'type' => 'deprecated', 'description' => 'use intense_person_custom_social instead' ),
				),
			),
			'rtl' => array(
				'type' => 'hidden',
				'description' => __( 'right-to-left', 'intense' ),
				'default' => $intense_visions_options['intense_rtl']
			)
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $intense_person, $intense_person_custom_social;

		if ( is_numeric( $image ) ) {
			$imageid = $image;
		} else if ( !empty( $image ) ) {
				$imageurl = $image;
			}

		if ( isset( $imageposition ) ) {
			$template = $imageposition;
		}
		
		global $intense_person_custom_social;
		global $intense_person_social_size;

		$intense_person_custom_social = array();

		if ( empty($custom_social) ) {
			// render shortcodes so that the custom social
			// icons can be gathered together
			do_shortcode($content);
		} else {
			$custom_social_items = explode( ',', $custom_social );

			foreach ( $custom_social_items as $social ) {
				$splitsocial = explode( '|', $social );

				if ( is_numeric( $splitsocial[2] ) ) {
			      $photo_info = wp_get_attachment_image_src( $splitsocial[2], $social_size );
			      $splitsocial[2] = $photo_info[0];
			    }
				
				if ( $splitsocial[0] != '' || $splitsocial[1] != '' ) {
					$link = $splitsocial[1];

					if ( is_numeric( $link ) ) { 
						$link = get_permalink( $link );
					}					

					$intense_person_custom_social[] = array(
						'title' => $splitsocial[0],
						'link' => $link,
						'image' => $splitsocial[2],
					);
				}
			}
		}

		$custom_social = $intense_person_custom_social;

		$intense_person = array(
			'template' => $template,
			'imageid' => $imageid,
			'imageurl' => $imageurl,
			'imageshadow' => $imageshadow,
			'border_radius' => $border_radius,
			'size' => $size,
			'name' => $name,
			'social_target' => $social_target,
			'title' => $title,
			'facebook' => $facebook,
			'twitter' => $twitter,
			'linkedin' => $linkedin,
			'dribbble' => $dribbble,
			'googleplus' => $googleplus,
			'instagram' => $instagram,
			'tumblr' => $tumblr,
			'pinterest' => $pinterest,
			'youtube' => $youtube,
			'content' => do_shortcode( $content ),
			'rtl' => $rtl,
			'social_size' => $social_size,
			'custom_social'  => $custom_social
		);

		$template = intense_locate_plugin_template( '/person/' . $template . '.php' );
		return intense_load_plugin_template( $template );
	}

	function render_dialog_script() {
?>
    <script>
      jQuery(function($) {
        $('.upload_image_button').live('click', function( event ){
          event.preventDefault();
          var target = $(this).data('target-id');

          window.parent.loadImageFrame($(this), function(attachment) {
            jQuery("#" + target).val(attachment.id);
            
            if ( attachment.sizes.thumbnail ) {
              jQuery("#" + target + "-thumb").attr("src", attachment.sizes.thumbnail.url);
            } else {
              jQuery("#" + target + "-thumb").attr("src", attachment.url);
            }

            if ( $('#preview-content').length > 0 ) {
              $('#preview').click();
            }
          });
        });
      });
    </script>
    <?php
	}
}

class Intense_Person_Custom_Social extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Person Custom Social', 'intense' );
		$this->category = __( 'Layout', 'intense' );
		$this->icon = 'dashicons-intense-user';
		$this->show_preview = true;
		$this->hidden = true;
		$this->parent = 'intense_person';

		$this->fields = array(
			'title' => array(
				'type' => 'text',
				'title' => __( 'Title', 'intense' ),
				'composer_show_value' => true,
                'skinnable' => '0'
			),
			'link' => array(
				'type' => 'text',
				'title' => __( 'Link', 'intense' ),
				'description' => __( 'URL, post ID, or page ID to direct the user to when clicked', 'intense' ),
                'skinnable' => '0'
			),
			'image' => array(
				'type' => 'image',
				'title' => __( 'Image', 'intense' ),
                'skinnable' => '0'
			),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		//gather custom social icons into a global array that can be passed to the person shortcode template
		global $intense_person_custom_social;
		global $intense_person_social_size;

		if ( is_numeric( $image ) ) {
	      $photo_info = wp_get_attachment_image_src( $image, $intense_person_social_size );
	      $atts['image'] = $photo_info[0];
	    } else if ( !empty( $image ) ) {
	      $atts['image'] = $image;
	    }

		$intense_person_custom_social[] = $atts;		

		return '';
	}
}
