<?php

class Intense_Recent_Posts extends Intense_Shortcode {

	function __construct() {
		global $intense_visions_options;

		$this->title = __( 'Recent Posts', 'intense' );
		$this->category = __( 'Posts', 'intense' );		
		$this->icon = 'dashicons-admin-post';
		$this->show_preview = true;
		$this->map_visual_composer = false;

		$this->fields = array(
			'general_tab' => array(
                'type' => 'tab',
                'title' => __( 'General', 'intense' ),
                'fields' =>  array(
					'layout' => array(
					    'type' => 'dropdown',
					    'title' => __( 'Layout', 'intense' ),
					    'default' => 'horizontal',
					    'options' => array(
					        'horizontal' => __( 'Horizontal', 'intense' ),
					        'vertical' => __( 'Vertical', 'intense' ),
					        'slider' => __( 'Slider', 'intense' ),
					    )
					),
					'post_type' => array(
						'type' => 'hidden',
						'description' => __( 'Post Type', 'intense' ),
						'default' => 'post'
					),
					'taxonomy' => array(
						'type' => 'taxonomy',
						'category' => 'category',
						'title' => __( 'Taxonomy', 'intense' ),
						'default' => 'category'
					),
					'template' => array(
						'type' => 'template',
						'title' => __( 'Template', 'intense' ),
						'path' => '/recent-post/',
						'default' => 'top'
					),
					'category_id' => array(
					    'type' => 'deprecated',
					    'description' => 'use categories instead'
					),
					'category_slug' => array(
					    'type' => 'deprecated',
					    'description' => 'use categories instead'
					),
					'categories' => array(
						'type' => 'category',
						'category' => 'category',
						'title' => __( 'Category(s)', 'intense' ),
						'description' => __( 'By default, all categories will be shown.', 'intense' )
					),
					'columns' => array(
					    'type' => 'text',
					    'title' => __( 'Columns', 'intense' ),
					    'description' => __( 'Enter a number from 1 - 12', 'intense' ),
					    'class' => 'layoutsettings',
					    'default' => '3',
					),
					'excerpt_length' => array(
					    'type' => 'text',
					    'title' => __( 'Excerpt Length', 'intense' ),
					    'description' => __( 'Enter the number of words you want to show.', 'intense' ),
					    'default' => '15',
					),
					'post_count' => array(
					    'type' => 'text',
					    'title' => __( 'Count', 'intense' ),
					    'description' => __( 'Enter the number of posts you want to show.', 'intense' ),
					    'default' => '3',
					),
					'show_title' => array(
					    'type' => 'checkbox',
					    'title' => __( 'Show Title', 'intense' ),
					    'default' => '1',
					),
					'show_thumbnail' => array(
					    'type' => 'checkbox',
					    'title' => __( 'Show Thumbnail', 'intense' ),
					    'default' => '1',
					),
					'show_meta' => array(
					    'type' => 'checkbox',
					    'title' => __( 'Show Meta', 'intense' ),
					    'default' => '1',
					),
					'show_excerpt' => array(
					    'type' => 'checkbox',
					    'title' => __( 'Show Excerpt', 'intense' ),
					    'default' => '1',
					),
					'show_read_more' => array(
						'type' => 'checkbox',
						'title' => __( 'Show "Read More" button', 'intense' ),
						'default' => '0',
					),
					'read_more_text' => array(
		            	'type' => 'text',
		                'title' => __( '"Read More" Display Text', 'intense' ),
					    'description' => __( 'Only visible if Show "Read More" button above is checked.', 'intense' ),						
						'default' => __( 'Read More', 'intense' ),
						'skinnable' => '0'
		            ),
					'image_size' => array(
						'type' => 'image_size',
						'title' => __( 'Image Size', 'intense' ),
						'default' => 'medium500',
					),
					'not_post_id' => array(
					    'type' => 'text',
					    'title' => __( 'Not Post ID', 'intense' ),
					    'description' => __( 'ID of post that you do not want to include in list.', 'intense' ),
					    'default' => '',
					),
					'show_missing_image' => array(
					    'type' => 'checkbox',
					    'title' => __( 'Show Missing Image', 'intense' ),
					    'description' => __( 'Show missing image if no image is set.', 'intense' ),
					    'default' => '0',
					),
					'related_post_id' => array(
					    'type' => 'text',
					    'title' => __( 'Related Post ID', 'intense' ),
					    'default' => '',
					),
					'border_radius' => array(
						'type' => 'border_radius',
						'title' => __( 'Border Radius', 'intense' ),
					),
					'no_post_message' => array(
						'type' => 'text',
						'title' => __( 'No Posts Message', 'intense' ),
						'default' => __( "Sorry, nothing to show here", "intense" ),
						'skinnable' => '0'
					),
					'rtl' => array(
						'type' => 'hidden',
						'description' => __( 'right-to-left', 'intense' ),
						'default' => $intense_visions_options['intense_rtl']
					),
				)
			),
			'owlslider_tab' => array(
                'type' => 'tab',
                'title' => __( 'Slider', 'intense' ),
                'class' => 'owlslidersettings',
                'fields' =>  array(
                	'slide_padding_right' => array(
                        'type' => 'text',
                        'title' => __( 'Slide Padding Right', 'intense' ),
                        'default' => '15',
                    ),
					'items' => array(
                        'type' => 'text',
                        'title' => __( 'Max Items', 'intense' ),
                        'default' => '5',
                    ),
                    'items_desktop' => array(
                        'type' => 'text',
                        'title' => __( 'Items Desktop', 'intense' ),
                        'description' => __( 'window width <= 1199', 'intense' ),
                        'default' => '4',
                    ),
                    'items_desktop_small' => array(
                        'type' => 'text',
                        'title' => __( 'Items Desktop Small', 'intense' ),
                        'description' => __( 'window width <= 979', 'intense' ),
                        'default' => '3',
                    ),
                    'items_tablet' => array(
                        'type' => 'text',
                        'title' => __( 'Items Tablet', 'intense' ),
                        'description' => __( 'window width <= 768', 'intense' ),
                        'default' => '2',
                    ),
                    'items_mobile' => array(
                        'type' => 'text',
                        'title' => __( 'Items Mobile', 'intense' ),
                        'description' => __( 'window width <= 479', 'intense' ),
                        'default' => '1',
                    ),
                    'navigation_position' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Position', 'intense' ),
                        'default' => 'middleoutside',
                        'options' => array(
                            '' => '',
                            'topleft' => __( 'Top Left', 'intense' ),
                            'topcenter' => __( 'Top Center', 'intense' ),
                            'topright' => __( 'Top Right', 'intense' ),
                            'middleinside' => __( 'Middle Inside', 'intense' ),
                            'middleoutside' => __( 'Middle Outside', 'intense' ),
                            'bottomleft' => __( 'Bottom Left', 'intense' ),
                            'bottomcenter' => __( 'Bottom Center', 'intense' ),
                            'bottomright' => __( 'Bottom Right', 'intense' ),
                        ),
                    ),
                    'navigation_prev_icon' => array(
                        'type' => 'icon',
                        'title' => __( 'Navigation Previous Icon', 'intense' ),
                        'default' => 'chevron-left',
                    ),
                    'navigation_next_icon' => array(
                        'type' => 'icon',
                        'title' => __( 'Navigation Next Icon', 'intense' ),
                        'default' => 'chevron-right',
                    ),
                    'navigation_size' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Size', 'intense' ),
                        'default' => 'default',
                        'options' => array(
                            'default' => __( 'default', 'intense' ),
                            'mini' => __( 'mini', 'intense' ),
                            'small' => __( 'small', 'intense' ),
                            'medium' => __( 'medium', 'intense' ),
                            'large' => __( 'large', 'intense' )
                        )
                    ),
                    'navigation_prev_text' => array(
                        'type' => 'text',
                        'title' => __( 'Navigation Previous Text', 'intense' ),
                    ),
                    'navigation_next_text' => array(
                        'type' => 'text',
                        'title' => __( 'Navigation Next Text', 'intense' ),
                    ),
                    'navigation_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Navigation Color', 'intense' ),
                        'default' => '#111111',
                    ),
                    'navigation_font_color' => array(
                        'type' => 'color_advanced',
                        'title' => __( 'Navigation Font Color', 'intense' ),
                        'description' => __( 'if left blank, either black or white will automatically be chosen', 'intense' ),
                    ),
                    'navigation_prev_border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Navigation Previous Border Radius', 'intense' ),
                    ),
                    'navigation_next_border_radius' => array(
                        'type' => 'border_radius',
                        'title' => __( 'Navigation Next Border Radius', 'intense' ),
                    ),
                    'navigation_pagination_type' => array(
                        'type' => 'dropdown',
                        'title' => __( 'Navigation Pagination Type', 'intense' ),
                        'default' => 'bullets',
                        'options' => array(
                            '' => __( 'None', 'intense' ),
                            'bullets' => __( 'Bullets', 'intense' ),
                            'numbers' => __( 'Numbers', 'intense' ),
                        ),
                    ),
                )
            ),
		);
	}

	function shortcode( $atts, $content = null ) {
		global $intense_visions_options, $intense_recent_post, $intense_post_types;
		extract( shortcode_atts( $this->get_shortcode_defaults( null, $atts ), $atts ) );

		global $wp_query;

		$intense_post_type = $intense_post_types->get_post_type( $post_type );

		$intense_recent_post = array(
			'template' => $template,
            'post_type' => $intense_post_type,
            'taxonomy' => $taxonomy,
            'category_id' => $category_id,
            'category_slug' => $category_slug,
            'categories' => $categories,
            'columns' => $columns,
            'excerpt_length' => $excerpt_length,
            'post_count' => $post_count,
            'show_title' => $show_title,
            'show_thumbnail' => $show_thumbnail,
            'show_meta' => $show_meta,
            'show_excerpt' => $show_excerpt,
			'show_read_more' => $show_read_more,
			'read_more_text' => $read_more_text,
            'image_size' => $image_size,
            'not_post_id' => $not_post_id,
            'show_missing_image' => $show_missing_image,
            'related_post_id' => $related_post_id,
            'layout' => $layout,
            'border_radius' => $border_radius,
            'no_post_message' => $no_post_message,
            'rtl' => $rtl,
		);

		$template_file = intense_locate_plugin_template( '/recent-post/' .  get_post_type() . '/' . $template . '.php' );

		if ( !isset( $template_file ) ) {
			$template_file = intense_locate_plugin_template( '/recent-post/post/' . $template . '.php' );
		}

		if ( !isset( $template_file ) ) {
			$template_file = intense_locate_plugin_template( '/recent-post/post/top.php' );
		}


		$i = 1;
	    $column_width = floor( 12 / $columns );

	    $original_query = $wp_query;

	    $wp_query = null;

	    $args = array(
	        'paged' => true,
	        'posts_per_page' => $post_count,
	    );

	    if ( !isset( $post_type ) && ( isset( $not_post_id ) ||  ( isset( $related_post_id ) && $related_post_id != '' ) ) ) {
	        $post_type = get_post_type( isset( $related_post_id ) ? $related_post_id : $not_post_id );
	    }

	    if ( isset( $post_type ) ) {
	        $args["post_type"] = $post_type;
	    }

	    $radius = '';
	    if ( isset( $border_radius ) ) {
			if ( is_numeric( $border_radius ) ) {
				$border_radius .= 'px';
			}

	        $radius = ' border-radius:' . $border_radius . ';';
	    }

	    if ( isset( $related_post_id ) && $related_post_id != '' ) {
	        $tags = wp_get_post_terms( $related_post_id, $taxonomy );
	        $first_tag = 'bananabananabanana';
	        $second_tag = 'bananabananabanana';
	        $third_tag = 'bananabananabanana';
	        $category_id = '';

	        // first try by taxonomy
	        // fallback to category
	        if ( isset( $tags ) && count( $tags ) > 0 ) {
	            if ( isset( $tags[0] ) ) $first_tag  = $tags[0]->term_id;
	            if ( isset( $tags[1] ) ) $second_tag = $tags[1]->term_id;
	            if ( isset( $tags[2] ) ) $third_tag  = $tags[2]->term_id;

	            $args['tax_query'] = array(
	                'relation' => 'OR',
	                array(
	                    'taxonomy' => $taxonomy,
	                    'terms' => $second_tag,
	                    'field' => 'id',
	                    'operator' => 'IN',
	                ),
	                array(
	                    'taxonomy' => $taxonomy,
	                    'terms' => $first_tag,
	                    'field' => 'id',
	                    'operator' => 'IN',
	                ),
	                array(
	                    'taxonomy' => $taxonomy,
	                    'terms' => $third_tag,
	                    'field' => 'id',
	                    'operator' => 'IN',
	                )
	            );
	        } else {
	            $categories = get_the_category();
	            if ( isset( $categories[0] ) ) {
	                $category_id = $categories[0]->cat_ID;
	            }
	        }
	    }

	    if ( ( isset( $category_id ) && $category_id != '' ) || ( isset( $category_slug ) && $category_slug != '' ) ) {
	        if ( isset( $post_type ) && $post_type != "post" ) {
	        	if ( isset( $category_id ) && $category_id != '' ) {
	        		if ( strpos( $category_id, ',' ) ) {
						$category_id = explode( ',', str_replace( ' ', '', $category_id ) );
						$args['tax_query'] = array(
							array(
								'taxonomy' => $taxonomy,
								'field'  => 'term_id',
								'terms'  => $category_id
							)
						);
	        		} else {
						$args['tax_query'] = array(
							array(
								'taxonomy' => $taxonomy,
								'field'  => 'term_id',
								'terms'  => array( $category_id )
							)
						);
	        		}
	        	} else {
	        		if ( strpos( $category_slug, ',' ) ) {
						$category_slug = explode( ',', str_replace( ' ', '', $category_slug ) );
						$args['tax_query'] = array(
							array(
								'taxonomy' => $taxonomy,
								'field'  => 'slug',
								'terms'  => $category_slug
							)
						);
	        		} else {
						$args['tax_query'] = array(
							array(
								'taxonomy' => $taxonomy,
								'field'  => 'slug',
								'terms'  => array( $category_slug )
							)
						);
	        		}
	        	}
	        } else {
	        	if ( isset( $category_id ) && $category_id != '' ) {
	        		if ( strpos( $category_id, ',' ) ) {
		        		$category_id = explode( ',', $category_id );
		            	$args['category__in'] = $category_id;

	        		} else {
		            	$args['category__in'] = array( $category_id );
		            }
	        	} else {
	        		$args['category_name'] = $category_slug;
	        	}
	        }
	    } else if ( isset( $categories ) && $categories != '' ) {
			if ( strpos( $categories, ',' ) ) {
				$categories = explode( ',', str_replace( ' ', '', $categories ) );

				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'  => 'slug',
						'terms'  => $categories
					)
				);
			} else {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'  => 'slug',
						'terms'  => array( $categories )
					)
				);
			}
	    } else {
	    	if ( $taxonomy != '' ) {
		    	$categories = get_terms( $taxonomy, array( 'fields'    => 'ids' ) );

		    	if ( isset( $post_type ) && $post_type != "post" ) {
		            $args['tax_query'] = array(
		                array(
		                    'taxonomy' => $taxonomy,
		                    'field' => 'term_id',
		                    'terms' => $categories
		                )
		            );
		    	} else {
					$args['category__in'] = $categories;
		    	}
		    }
	    }

	    if ( isset( $not_post_id ) && $not_post_id != '' ) {
	        $args['post__not_in'] = array( $not_post_id );
	    }

	    $wp_query = new WP_Query( $args );

	    $output = "";

	    if ( !$wp_query->have_posts() ) {
	        $output .= $no_post_message;
	    }

	    $horizontal = "";
	    $horizontal_ended = false;

	    $slider = '';

	    while ( $wp_query->have_posts() ): $wp_query->the_post();
	        global $post;

	        if ( $layout == "vertical" ) {
		        $output .= do_shortcode( intense_load_plugin_template( $template_file ) );
		    } else if ( $layout == 'slider' ) {
		    	if ( $i == 1 ) {
		    		$slider .= '[intense_slider type="owl-carousel" 
		    			items="' . $items . '"
                    	items_desktop="' . $items_desktop . '"
                    	items_desktop_small="' . $items_desktop_small . '"
                    	items_tablet="' . $items_tablet . '"
                    	items_mobile="' . $items_mobile . '"
                    	navigation_position="' . $navigation_position . '"
                    	navigation_prev_icon="' . $navigation_prev_icon . '"
                    	navigation_next_icon="' . $navigation_next_icon . '"
                    	navigation_size="' . $navigation_size . '"
                    	navigation_prev_text="' . $navigation_prev_text . '"
                    	navigation_next_text="' . $navigation_next_text . '"
                    	navigation_color="' . $navigation_color . '"
                    	navigation_font_color="' . $navigation_font_color . '"
                    	navigation_prev_border_radius="' . $navigation_prev_border_radius . '"
                    	navigation_next_border_radius="' . $navigation_next_border_radius . '"
                    	navigation_pagination_type="' . $navigation_pagination_type . '"
		    		]';
		    	} 

		    	$slider .= '[intense_slide]<div style="padding: 0 ' . $slide_padding_right . 'px 0 0">' . do_shortcode( intense_load_plugin_template( $template_file ) ) . '</div>[/intense_slide]';
	        } else {
		       $horizontal_ended = false;

	            if ( $i == 1 ) {
	                if ( $horizontal == "" ) {
	                    $horizontal .= "[intense_row padding_top='0']";
	                } else {
	                    $horizontal .= "[intense_row padding_top='30']";
	                }
	            }

	            $horizontal .= "[intense_column size='$column_width']";
	            $horizontal .= do_shortcode( intense_load_plugin_template( $template_file ) );
	            $horizontal .= "[/intense_column]";

	            if ( $i == $columns ) {
	                $horizontal .= "[/intense_row]";
	                $i = 0; //leave at 0 since it will get incremented later to 1                
			        $horizontal_ended = true;
	            }
	        }

			$i++;
	    endwhile;

	    if ( $wp_query->have_posts() && $layout == "horizontal" ) {
	        if ( !$horizontal_ended ) {
	            $horizontal .= "[/intense_row]";
	            $i = 0;
	        }

		   $output .= do_shortcode( $horizontal );
	    }

	    if ( !empty( $slider ) ) {
	    	$slider .= '[/intense_slider]';
	    	$output .= do_shortcode( $slider );
	    }

	    $wp_query = null;
	    $wp_query = $original_query;
	    wp_reset_postdata();

	    return $output;
	}

	function render_dialog_script() {
		$ajax_nonce = wp_create_nonce( "intense-plugin-noonce" );
?>
	<script>
		jQuery(function($) {

			$("#layout").change(function() {
				var type = $(this).val();

				switch(type) {
					case "horizontal":
						$(".layoutsettings").show();
						break;
					case "vertical":
						$(".layoutsettings").hide();
						break;
					case "slider":
						$(".layoutsettings").hide();
						break;
					default:
						$(".layoutsettings").show();
						break;
				}
			});

			$('#taxonomy').change(function() {
				setupCategory();
			});

			setupLists();
			setupCategory();

			function setupLists() {
				$("#taxonomy").empty();
				$("#categories").empty();
				$("#template").empty();


				if ( $("#post_type").val() != '' ) {
					var $loader = $('<img class="loader" src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/loader.gif" style="margin-left: -105px; margin-top: 5px; position: absolute;" />');
					var $loader2 = $loader.clone();

					$("#taxonomy").after($loader);
					$("#template").after($loader2);

					$.ajax({
						type: "POST",
						url: ajaxurl + '?action=intense_recent_post_action',
						data: {
							security: '<?php echo $ajax_nonce; ?>',
							postType: $("#post_type").val(),
							postTaxonomy: $("#taxonomy").val(),
					 	}
					}).done(function (data) {
						var results = jQuery.parseJSON(data);
						var templates = results.templates;
						var taxonomies = results.taxonomies;

						$.each(taxonomies, function(index, taxonomy) {
							$("#taxonomy").append('<option value="' + taxonomy.key + '">' + taxonomy.value + '</option>');
						});

						setupCategory();

						$.each(templates, function(index, template) {
							$("#template").append('<option value="' + template.key + '">' + template.value + '</option>');
						});

						$loader.remove();
						$loader2.remove();
					});
				}
			}

			function setupCategory() {
				$("#categories").empty();

				if ( $("#taxonomy").val() !== null && $("#taxonomy").val().length ) {
					var $loader = $('<img class="loader" src="<?php echo INTENSE_PLUGIN_URL; ?>/assets/img/loader.gif" style="margin-left: -105px; margin-top: 35px; position: absolute;" />');

					$("#categories").after($loader);

					$.ajax({
						type: "POST",
						url: ajaxurl + '?action=intense_recent_post_action',
						data: {
							security: '<?php echo $ajax_nonce; ?>',
							postType: $("#post_type").val(),
							postTaxonomy: $("#taxonomy").val()
					 	}
					}).done(function (data) {
						var results = jQuery.parseJSON(data);
						var categories = results.categories;

						$.each(categories, function(index, category) {
							$("#categories").append('<option value="' + category.key + '">' + category.value + '</option>');
						});

						$loader.remove();
					});
				}
			}
		});
	</script>
    <?php
	}
}

class Intense_Recent_Pages extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Pages', 'intense' );
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "page";
		$this->fields["general_tab"]["fields"]["taxonomy"]["type"] = "hidden";
		$this->fields["general_tab"]["fields"]["categories"]["type"] = "hidden";
		$this->icon = 'dashicons-admin-page';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "page";
		$atts["taxonomy"] = "";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Portfolios extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Portfolios', 'intense' );
		$this->custom_post_type = 'intense_portfolio';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_portfolio";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "portfolio_category";
		$this->icon = 'dashicons-portfolio';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_portfolio";		
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "portfolio_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Projects extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Projects', 'intense' );
		$this->custom_post_type = 'intense_project';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_project";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_project_category";
		$this->icon = 'dashicons-portfolio';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_project";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_project_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Books extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Books', 'intense' );
		$this->custom_post_type = 'intense_books';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_books";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_book_category";
		$this->icon = 'dashicons-book-alt';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_books";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_book_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Clients extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Clients', 'intense' );
		$this->custom_post_type = 'intense_clients';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_clients";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_clients_category";
		$this->icon = 'dashicons-businessman';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_clients";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_clients_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Coupons extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Coupons', 'intense' );
		$this->custom_post_type = 'intense_coupons';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_coupons";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_coupons_category";
		$this->icon = 'dashicons-intense-coupon';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_coupons";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_coupons_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Events extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Events', 'intense' );
		$this->custom_post_type = 'intense_events';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_events";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_events_category";
		$this->icon = 'dashicons-calendar';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_events";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_events_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Jobs extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Jobs', 'intense' );
		$this->custom_post_type = 'intense_jobs';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_jobs";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_job_category";
		$this->icon = 'dashicons-hammer';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_jobs";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_job_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Locations extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Locations', 'intense' );
		$this->custom_post_type = 'intense_locations';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_locations";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_locations_category";
		$this->icon = 'dashicons-location-alt';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_locations";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_locations_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Movies extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Movies', 'intense' );
		$this->custom_post_type = 'intense_movies';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_movies";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_movie_genre";
		$this->icon = 'dashicons-video-alt';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_movies";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_movie_genre";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_News extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent News', 'intense' );
		$this->custom_post_type = 'intense_news';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_news";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_news_category";
		$this->icon = 'dashicons-intense-newspaper';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_news";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_news_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Quotes extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Quotes', 'intense' );
		$this->custom_post_type = 'intense_quotes';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_quotes";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_quotes_category";
		$this->icon = 'dashicons-editor-quote';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_quotes";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_quotes_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Recipes extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Recipes', 'intense' );
		$this->custom_post_type = 'intense_recipes';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_recipes";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_recipes_ingredients";
		$this->icon = 'dashicons-intense-food';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_recipes";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_recipes_ingredients";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Team extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Team', 'intense' );
		$this->custom_post_type = 'intense_team';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_team";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_team_position";
		$this->icon = 'dashicons-groups';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_team";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_team_position";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}

class Intense_Recent_Testimonials extends Intense_Recent_Posts {
	function __construct() {
		parent::__construct();

		$this->title = __( 'Recent Testimonials', 'intense' );
		$this->custom_post_type = 'intense_testimonials';
		$this->fields["general_tab"]["fields"]["post_type"]["value"] = "intense_testimonials";
		$this->fields["general_tab"]["fields"]["taxonomy"]["default"] = "intense_testimonials_category";
		$this->icon = 'dashicons-testimonial';

		unset( $this->fields['show_meta'] );
	}

	function shortcode( $atts, $content = null ) {
		$atts["post_type"] = "intense_testimonials";
		if ( empty( $atts["taxonomy"] ) ) $atts["taxonomy"] = "intense_testimonials_category";
		$atts["show_meta"] = 0;

		return parent::shortcode($atts, $content);
	}
}
