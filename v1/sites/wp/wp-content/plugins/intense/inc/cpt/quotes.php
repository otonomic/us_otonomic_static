<?php

class Intense_Post_Type_Quotes extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_quotes_singular'] ) ? $intense_visions_options['intense_cpt_quotes_singular'] : __( 'Quote', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_quotes_plural'] ) ? $intense_visions_options['intense_cpt_quotes_plural'] : __( 'Quotes', 'intense' ) );
    
    $this->type = 'intense_quotes';
    $this->title = 'Enter ' . $name_singular . ' Title';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-editor-quote';
    $this->category_taxonomy_key = 'intense_quotes_category';
    $this->taxonomies = '';
    $this->fields = array(
      array (
        'key' => 'field_565t780h615a333',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_quotes_single_template',
        'type' => 'select',
        'choices' => array_merge(
            array( '' => '' ),
            intense_locate_available_plugin_templates('/custom-post/intense_quotes/single/'),
            intense_locate_single_cpt_templates( 'quotes', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52f6e152253bf',
        'label' => __( 'Author', "intense" ),
        'name' => 'intense_quote_author',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52f6e172253c0',
        'label' => __( 'Quote Date', "intense" ),
        'name' => 'intense_quote_date',
        'type' => 'date_picker',
        'instructions' => __( 'Set this date field if you want the full date to be displayed', "intense" ),
        'date_format' => 'yymmdd',
        'display_format' => 'mm/dd/yy',
        'first_day' => 0,
      ),
      array (
        'key' => 'field_52f6e242253c1',
        'label' => __( 'Quote Year', "intense" ),
        'name' => 'intense_quote_year',
        'type' => 'text',
        'instructions' => __( 'Set this field if you want only the year to be displayed', "intense" ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    );
  }
}
