<?php

class Intense_Post_Type_Snippets extends Intense_Post_Type {
  function __construct() {
    $this->type = 'intense_snippets';
    $this->title = 'Enter Snippet Title';
      $this->singular = __( 'Snippet', 'intense' );
      $this->plural = __( 'Snippets', 'intense' );
      $this->icon = 'dashicons-intense-scissors';
      $this->category_taxonomy_key = '';
      $this->taxonomies = '';
      $this->fields = '';
  }
}
