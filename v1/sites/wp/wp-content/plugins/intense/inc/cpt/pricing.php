<?php

class Intense_Post_Type_Pricing extends Intense_Post_Type {
  function __construct() {
    $this->type = 'intense_pricing';
    $this->title = 'Enter Pricing Table Name';
  }

	public function register() {

		//Pricing
		register_post_type(
			'intense_pricing',
			array(
				'labels' => array(
					'name' => __( 'Pricing Tables', "intense" ),
					'singular_name' => __( 'Pricing Tables', "intense" ),
					'menu_name' => __( 'Pricing Tables', "intense" ),
					'all_items' => __( 'All Pricing Tables', "intense" ),
					'add_new_item' => __( 'Add New Pricing Table', "intense" ),
					'edit_item' => __( 'Edit Pricing Table', "intense" ),
					'new_item' => __( 'New Pricing Table', "intense" ),
					'view_item' => __( 'View Pricing Table', "intense" ),
					'search_items' => __( 'Edit Pricing Table', "intense" ),
					'not_found' => __( 'No Pricing Tables found', "intense" ),
					'not_found_in_trash' => __( 'No Pricing Tables found in Trash', "intense" )
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array( 'slug' => 'pricing-items' ),
				'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
				'can_export' => true,        
        'menu_icon'=>'dashicons-intense-coin',
			)
		);

		register_taxonomy( 'intense_pricing_category', 'intense_pricing', array( 'hierarchical' => true, 'label' => __( 'Categories', "intense" ), 'query_var' => true, 'rewrite' => true ) );
	}
}
