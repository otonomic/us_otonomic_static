<?php

class Intense_Post_Type_Events extends Intense_Post_Type {
	function __construct() {
    	global $intense_visions_options;
    	$name_singular = ( isset( $intense_visions_options['intense_cpt_events_singular'] ) ? $intense_visions_options['intense_cpt_events_singular'] : __( 'Event', 'intense' ) );    
    	$name_plural = ( isset( $intense_visions_options['intense_cpt_events_plural'] ) ? $intense_visions_options['intense_cpt_events_plural'] : __( 'Events', 'intense' ) );
    
	    $this->type = 'intense_events';
	    $this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
	    $this->icon = 'dashicons-calendar';
	    $this->category_taxonomy_key = 'intense_events_category';
	    $this->taxonomies = '';
	    $this->fields = array(
			array (
				'key' => 'field_52f6d5fe8n895',
				'label' => __( 'Subtitle', "intense" ),
				'name' => 'intense_event_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_70fr504ht9712',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_events_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_events/single/'),
					intense_locate_single_cpt_templates( 'events', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d619a880549',
				'label' => __( 'Type', 'intense' ),
				'name' => 'intense_event_type',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61c1f8054f',
				'label' => __( 'Start Date', 'intense' ),
				'name' => 'intense_event_start_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52d61cc480550',
				'label' => __( 'End Date', 'intense' ),
				'name' => 'intense_event_end_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
				'information' => 'Leave blank if the event is only for one day. Make sure to fill in the Start Date.',
			),
			array (
				'key' => 'field_52d61a278054a',
				'label' => __( 'Address', 'intense' ),
				'name' => 'intense_event_address',
				'type' => 'textarea',
				'column_width' => 45,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_52d61db180551',
				'label' => __( 'Website', 'intense' ),
				'name' => 'intense_event_website',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61b7f8054c',
				'label' => __( 'Entrance Cost', 'intense' ),
				'name' => 'intense_event_entrance_cost',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61dc480552',
				'label' => __( 'Contact Name', 'intense' ),
				'name' => 'intense_event_contact_name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61e0d80553',
				'label' => __( 'Contact Phone', 'intense' ),
				'name' => 'intense_event_contact_phone',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61e2480554',
				'label' => __( 'Contact Email', 'intense' ),
				'name' => 'intense_event_contact_email',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_52d61hh68054d',
				'label' => __( 'Venue Name', 'intense' ),
				'name' => 'intense_event_venue_name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61b138054b',
				'label' => __( 'Venue Type', 'intense' ),
				'name' => 'intense_event_venue_type',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d61bb68054d',
				'label' => __( 'Venue Capacity', 'intense' ),
				'name' => 'intense_event_venue_capacity',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
	    );
	}

	public function get_subtitle() {
		return get_field('intense_event_subtitle');
	}
}
