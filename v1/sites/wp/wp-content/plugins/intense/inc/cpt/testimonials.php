<?php

class Intense_Post_Type_Testimonials extends Intense_Post_Type {
	function __construct() {
    	global $intense_visions_options;
    	$name_singular = ( isset( $intense_visions_options['intense_cpt_testimonials_singular'] ) ? $intense_visions_options['intense_cpt_testimonials_singular'] : __( 'Testimonial', 'intense' ) );    
    	$name_plural = ( isset( $intense_visions_options['intense_cpt_testimonials_plural'] ) ? $intense_visions_options['intense_cpt_testimonials_plural'] : __( 'Testimonials', 'intense' ) );
    
    	$this->type = 'intense_testimonials';
    	$this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
	    $this->icon = 'dashicons-testimonial';
	    $this->category_taxonomy_key = 'intense_testimonials_category';
	    $this->taxonomies = '';
	    $this->fields = array(
			array (
				'key' => 'field_5gt6r54h615j876',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_testimonials_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_testimonials/single/'),
					intense_locate_single_cpt_templates( 'testimonials', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52e44d4051e05',
				'label' => __( 'Author Name', 'intense' ),
				'name' => 'intense_testimonial_author',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52e72e0c7b29b',
				'label' => __( 'Testimonial Background', 'intense' ),
				'name' => 'intense_testimonial_background',
				'type' => 'color_picker',
				'default_value' => '#b7b7b7',
			),
			array (
				'key' => 'field_52e72e0c7b27c',
				'label' => __( 'Testimonial Font Color', 'intense' ),
				'name' => 'intense_testimonial_font_color',
				'type' => 'color_picker',
				'default_value' => '#ffffff',
			),
			array (
				'key' => 'field_52e44eb451e06',
				'label' => __( 'Author Image', 'intense' ),
				'name' => 'intense_testimonial_author_image',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_52e44f0551e07',
				'label' => __( 'Company', 'intense' ),
				'name' => 'intense_testimonial_company',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52e44f2a51e08',
				'label' => __( 'Company Link', 'intense' ),
				'name' => 'intense_testimonial_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52e44f5f51e09',
				'label' => __( 'Company Link Target', 'intense' ),
				'name' => 'intense_testimonial_link_target',
				'type' => 'select',
				'choices' => array (
					'_blank' => __( 'Blank', "intense" ),
					'_self' => __( 'Self', "intense" ),
					'_parent' => __( 'Parent', "intense" ),
					'_top' => __( 'Top', "intense" ),
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
	    );
	}
}
