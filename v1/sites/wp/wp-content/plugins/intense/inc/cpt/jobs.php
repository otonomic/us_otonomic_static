<?php

class Intense_Post_Type_Jobs extends Intense_Post_Type  {
	function __construct() {
	    global $intense_visions_options;
	    $name_singular = ( isset( $intense_visions_options['intense_cpt_jobs_singular'] ) ? $intense_visions_options['intense_cpt_jobs_singular'] : __( 'Job', 'intense' ) );    
	    $name_plural = ( isset( $intense_visions_options['intense_cpt_jobs_plural'] ) ? $intense_visions_options['intense_cpt_jobs_plural'] : __( 'Jobs', 'intense' ) );
	    
		$this->type = 'intense_jobs';
		$this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
	    $this->icon = 'dashicons-hammer';
	    $this->category_taxonomy_key = 'intense_job_category';
	    $this->taxonomies = array(
	      array(
	        'key' => 'intense_job_type',
	        'singular' => __( 'Type', "intense" ),
	        'plural' => __( 'Types', "intense" )
	      )
	    );
	    $this->fields = array(
			array (
				'key' => 'field_525g67jh898a712',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_jobs_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_jobs/single/'),
					intense_locate_single_cpt_templates( 'jobs', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52f6e95aa6128',
				'label' => __( 'Post Date', "intense" ),
				'name' => 'intense_job_post_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52f6e97fa6129',
				'label' => __( 'Expire Date', "intense" ),
				'name' => 'intense_job_expire_date',
				'type' => 'date_picker',
				'instructions' => __( 'Leave empty for no expiration', "intense" ),
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52f6eb8da6131',
				'label' => __( 'Status', "intense" ),
				'name' => 'intense_job_status',
				'type' => 'select',
				'choices' => array (
					'active' => __( 'Active', "intense" ),
					'filled' => __( 'Filled', "intense" ),
					'closed' => __( 'Closed', "intense" ),
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52n6eae3a509h',
				'label' => __( 'Job Page Link', "intense" ),
				'name' => 'intense_job_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52f6f207a6133',
				'label' => __( 'Featured', "intense" ),
				'name' => 'intense_job_featured',
				'type' => 'select',
				'instructions' => 'Featured listings will be sticky, and can be styled differently.',
				'choices' => array (
					'no' => __( 'No', "intense" ),
					'yes' => __( 'Yes', "intense" ),
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d61f2eag7u9',
				'label' => __( 'Qualifications', 'intense' ),
				'name' => 'intense_job_qualifications',
				'type' => 'wp_wysiwyg',
				'default_value' => '',
				'teeny' => 0,
				'media_buttons' => 1,
				'dfw' => 1,
			),
			array (
				'key' => 'field_52d61f2eadr47',
				'label' => __( 'Responsibilities', 'intense' ),
				'name' => 'intense_job_responsibilities',
				'type' => 'wp_wysiwyg',
				'default_value' => '',
				'teeny' => 0,
				'media_buttons' => 1,
				'dfw' => 1,
			),
			array (
				'key' => 'field_52d61f2eac4e6',
				'label' => __( 'Competencies', 'intense' ),
				'name' => 'intense_job_competencies',
				'type' => 'wp_wysiwyg',
				'default_value' => '',
				'teeny' => 0,
				'media_buttons' => 1,
				'dfw' => 1,
			),
            array (
				'key' => 'field_52d60f7ce7ff6',
				'label' => __( 'Compensation', 'intense' ),
				'name' => 'intense_job_compensation',
				'type' => 'textarea',
				'column_width' => 45,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'br',
            ),
			array (
				'key' => 'field_52f6e9ada612a',
				'label' => __( 'Company Name', "intense" ),
				'name' => 'intense_job_company',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52f6f0d0a6132',
				'label' => __( 'Company Tagline', "intense" ),
				'name' => 'intense_job_tagline',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => __( 'Brief description of company', "intense" ),
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52f6f97665d79',
				'label' => __( 'Company Logo', "intense" ),
				'name' => 'intense_job_company_logo',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_52f6e9f3a612b',
				'label' => __( 'Company Location', "intense" ),
				'name' => 'intense_job_location',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'e.g. "New York, NY", "Paris", "Anywhere"',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52f6eae3a612f',
				'label' => __( 'Company Website', "intense" ),
				'name' => 'intense_job_company_website',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d6125de7et8',
				'label' => __( 'Contact Name', 'intense' ),
				'name' => 'intense_job_contact',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52f6eb29a6130',
				'label' => __( 'Contact Email', "intense" ),
				'name' => 'intense_job_contact_email',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52h6743f769bt',
				'label' => __( 'Contact Phone', 'intense' ),
				'name' => 'intense_job_contact_phone',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
	    );
	}

	public function get_subtitle() {
		return '';
	}
}
