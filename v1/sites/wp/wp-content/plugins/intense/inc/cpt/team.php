<?php

class Intense_Post_Type_Team extends Intense_Post_Type {
	function __construct() {
    	global $intense_visions_options;
    	$name_singular = ( isset( $intense_visions_options['intense_cpt_team_singular'] ) ? $intense_visions_options['intense_cpt_team_singular'] : __( 'Member', 'intense' ) );    
    	$name_plural = ( isset( $intense_visions_options['intense_cpt_team_plural'] ) ? $intense_visions_options['intense_cpt_team_plural'] : __( 'Team', 'intense' ) );
    
    	$this->type = 'intense_team';
    	$this->title = 'Enter ' . $name_singular . ' Name';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
	    $this->icon = 'dashicons-groups';
	    $this->category_taxonomy_key = '';
	    $this->taxonomies = array(
	      array(
	        'key' => 'intense_team_position',
	        'singular' => __( 'Position', "intense" ),
	        'plural' => __( 'Positions', "intense" )
	      ),
	      array(
	        'key' => 'intense_team_skills',
	        'singular' => __( 'Skill', "intense" ),
	        'plural' => __( 'Skills', "intense" )
	      )
	    );
	    $this->fields = array(
			array (
				'key' => 'field_1g67080h615k7h0',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_team_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_team/single/'),
					intense_locate_single_cpt_templates( 'team', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d0e73f47bd5',
				'label' => __( 'Member Title', "intense" ),
				'name' => 'intense_member_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
            array (
				'key' => 'field_52t727b1gy3lp',
				'label' => __( 'Member Photo', "intense" ),
				'name' => 'intense_member_photo',
				'type' => 'image',
				'column_width' => 40,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
            ),
			array (
				'key' => 'field_52d0e7c647bd7',
				'label' => __( 'Image Shadow', "intense" ),
				'name' => 'intense_member_image_shadow',
				'type' => 'select',
				'choices' => array (
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
					9 => 9,
					10 => 10,
					11 => 11,
					12 => 12,
					13 => 13,
					14 => 14,
				),
				'default_value' => '',
				'allow_null' => 1,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d0e80c47bd8',
				'label' => __( 'Facebook', "intense" ),
				'name' => 'intense_member_facebook',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e8d947bd9',
				'label' => __( 'Google Plus', "intense" ),
				'name' => 'intense_member_googleplus',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e90047bda',
				'label' => __( 'Twitter', "intense" ),
				'name' => 'intense_member_twitter',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e91947bdb',
				'label' => __( 'Dribbble', "intense" ),
				'name' => 'intense_member_dribbble',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e93947bdc',
				'label' => __( 'Linked In', "intense" ),
				'name' => 'intense_member_linkedin',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e96147bdd',
				'label' => __( 'Custom Social Icon', "intense" ),
				'name' => 'intense_member_custom_social_icon',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_52d0e99c47bde',
				'label' => __( 'Custom Social Icon Link', "intense" ),
				'name' => 'intense_member_custom_social_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
	    );
	}
}
