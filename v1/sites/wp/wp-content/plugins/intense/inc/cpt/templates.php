<?php

class Intense_Post_Type_Templates extends Intense_Post_Type {
	function __construct() {
		add_action( 'save_post', array( __CLASS__, 'save_template_cache' ) );

    	$this->type = 'intense_templates';
    	$this->title = 'Enter Template Name';
	    $this->singular = __( 'Template', 'intense' );
	    $this->plural = __( 'Templates', 'intense' );
	    $this->icon = 'dashicons-editor-paste-text';
	    $this->category_taxonomy_key = 'intense_job_category';
	    $this->taxonomies = '';
	    $this->fields = array(
			array (
				'key' => 'field_536d98898a125',
				'label' => 'Post Type',
				'name' => 'intense_templates_post_type',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'books' => 'Books',
					'clients' => 'Clients',
					'coupons' => 'Coupons',
					'events' => 'Events',
					'faq' => 'FAQ',
					'jobs' => 'Jobs',
					'locations' => 'Locations',
					'movies' => 'Movies',
					'news' => 'News',
					'portfolio' => 'Portfolios',
					'post' => 'Posts',
					'project' => 'Projects',
					'quotes' => 'Quotes',
					'recipes' => 'Recipes',
					'team' => 'Team',
					'testimonials' => 'Testimonials',
				),
				'default_value' => 'intense_post',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_536d99bd8a126',
				'label' => 'type',
				'name' => 'intense_templates_type',
				'type' => 'select',
				'instructions' => 'Select \'single\' for single page post template and \'multiple\' for creating a template to display a list of posts.',
				'choices' => array (
					'single' => 'Single',
					'multiple' => 'Multiple',
				),
				'default_value' => 'single',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_536d9a658a127',
				'label' => 'Columns',
				'name' => 'intense_templates_columns',
				'type' => 'select',
				'instructions' => 'Select the number of columns you want the template to show when it is used.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_536d99bd8a126',
							'operator' => '==',
							'value' => 'multiple',
						),
					),
					'allorany' => 'all',
				),
				'choices' => array (
					'one' => 1,
					'two' => 2,
					'three' => 3,
					'four' => 4,
					'six' => 6,
				),
				'default_value' => '',
				'allow_null' => 1,
				'multiple' => 0,
			),
	    );
	}

	/**
	 * Save post metadata when a post is saved.
	 *
	 * @param int $post_id The ID of the post.
	 */
	public static function save_template_cache( $post_id ) {
	    // If this isn't one of our 'templates' posts, don't update it.
	    if ( 'intense_templates' != get_post_type( $post_id ) ) {
	        return;
	    }

	    Intense_Post_Type_Templates::save_as_file( $post_id );
	}

	public static function get_template_cache( $post_id ) {
		$uploads = wp_upload_dir();
		$intense_cache_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );
		$cache_file = $intense_cache_dir . 'template_' . $post_id . '.php';

		if ( !file_exists( $cache_file ) ) {
			Intense_Post_Type_Templates::save_as_file( $post_id );
		}

		return $cache_file;
	}

	public static function save_as_file( $post_id ) {
		$post = get_post( $post_id );
		$type =  get_post_meta( $post->ID, 'intense_templates_type', true );
		$template = '';

		switch ( $type ) {
			case 'multiple':
				$template = Intense_Post_Type_Templates::generate_list_template( $post );
				break;			
			case 'single':
				$template = Intense_Post_Type_Templates::generate_single_template( $post );
				break;
		}

		$uploads = wp_upload_dir();

		$intense_cache_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );
		$cache_file = $intense_cache_dir . 'template_' . $post->ID . '.php';

		if ( file_exists( $cache_file ) ) unlink( $cache_file );

		if ( !file_exists( $intense_cache_dir ) ) {
			mkdir( $intense_cache_dir, 0777, true );
		}

		if ( $template != '' ) {
			file_put_contents( $cache_file, $template );
		}
	}

	public static function generate_single_template( $post ){
		$template = "
        <?php
        /*
        Intense Template Name: " . get_the_title( $post->ID ) . "
        */

        get_header( 'intense' );
        " . '$content' . " = <<<TEMPLATE
        " .  $post->post_content . "
TEMPLATE;
        do_action( 'intense_before_main_content' ); 
        echo do_shortcode(" . '$content' . ");
        do_action( 'intense_after_main_content' );

        get_footer( 'intense' );
      ?>";

      return $template;
	}

	public static function generate_list_template( $post ) {
		global $intense_custom_post;
		$columns =  get_post_meta( $post->ID, 'intense_templates_columns', true );

		if ( $columns === 'two' ) {
			$column = '6';
		} elseif ( $columns === 'three' ) {
			$column = '4';
		} elseif ( $columns === 'four') {
			$column = '3';
		} elseif ( $columns === 'six') {
			$column = '2';
		} else {
			$column = '12';
		}

		$template = "<?php
/*
Intense Template Name: " . get_the_title( $post->ID ) . "
*/
" . 'global $intense_custom_post;' . "
" . '$classes = $intense_custom_post[\'post_classes\'];' . "
?>
<div class='intense col-lg-" . $column . " col-md-" . $column . " col-sm-12 col-xs-12 " . '<?php echo $classes; ?>' . " intense_post' style='margin-left: 0px; padding: 0 10px; padding-bottom: 0px; display:inline-block; vertical-align: top;'>
<article><?php 
" . '$content' . " = <<<TEMPLATE
" .  $post->post_content . "
TEMPLATE;
echo do_shortcode(" . '$content' . ");
?></article></div>";

		return $template;
	}
}
