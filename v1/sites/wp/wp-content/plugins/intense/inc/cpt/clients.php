<?php

class Intense_Post_Type_Clients extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_clients_singular'] ) ? $intense_visions_options['intense_cpt_clients_singular'] : __( 'Client', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_clients_plural'] ) ? $intense_visions_options['intense_cpt_clients_plural'] : __( 'Clients', 'intense' ) );
    
    $this->type = 'intense_clients';
    $this->title = 'Enter ' . $name_singular . ' Name';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-businessman';
    $this->category_taxonomy_key = 'intense_clients_category';
    $this->taxonomies = '';
    $this->fields = array( 
      array (
              'key' => 'field_52ft777gp9712',
              'label' => __( 'Single Post Template', "intense" ),
              'name' => 'intense_clients_single_template',
              'type' => 'select',
              'choices' => array_merge(
                array( '' => '' ),
                intense_locate_available_plugin_templates('/custom-post/intense_clients/single/'),
                intense_locate_single_cpt_templates( 'clients', true )
              ),
              'default_value' => '',
              'allow_null' => 0,
              'multiple' => 0,
            ),
            array (
              'key' => 'field_52t727b1gy32t',
              'label' => __( 'Logo', "intense" ),
              'name' => 'intense_client_logo',
              'type' => 'image',
              'column_width' => 40,
              'save_format' => 'object',
              'preview_size' => 'thumbnail',
              'library' => 'all',
            ),
            array (
              'key' => 'field_52f60cc9e7bg0',
              'label' => __( 'Slogan', 'intense' ),
              'name' => 'intense_client_slogan',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d60cc9e7ea2',
              'label' => __( 'Business Sector', 'intense' ),
              'name' => 'intense_client_sector',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d60ffee7ea5',
              'label' => __( 'Website', 'intense' ),
              'name' => 'intense_client_website',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d60f7ce7ea4',
              'label' => __( 'Address', 'intense' ),
              'name' => 'intense_client_address',
              'type' => 'textarea',
              'column_width' => 45,
              'default_value' => '',
              'placeholder' => '',
              'maxlength' => '',
              'formatting' => 'br',
            ),
            array (
              'key' => 'field_52d60f2fe7ea3',
              'label' => __( 'Employees', 'intense' ),
              'name' => 'intense_client_employees',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d6107de7ea7',
              'label' => __( 'Contact Name', 'intense' ),
              'name' => 'intense_client_contact',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d61025e7ea6',
              'label' => __( 'Contact Email', 'intense' ),
              'name' => 'intense_client_contact_email',
              'type' => 'email',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
            ),
            array (
              'key' => 'field_52d6156f769be',
              'label' => __( 'Contact Phone', 'intense' ),
              'name' => 'intense_client_contact_phone',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
    );
  }

  public function get_subtitle() {
    return get_field('intense_client_slogan');
  }
}
