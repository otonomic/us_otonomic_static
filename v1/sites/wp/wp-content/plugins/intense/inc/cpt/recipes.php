<?php

class Intense_Post_Type_Recipes extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_recipes_singular'] ) ? $intense_visions_options['intense_cpt_recipes_singular'] : __( 'Recipe', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_recipes_plural'] ) ? $intense_visions_options['intense_cpt_recipes_plural'] : __( 'Recipes', 'intense' ) );
    
    $this->type = 'intense_recipes';
    $this->title = 'Enter ' . $name_singular . ' Name';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-intense-food';
    $this->category_taxonomy_key = '';
    $this->taxonomies = array(
      array(
        'key' => 'intense_recipes_ingredients',
        'singular' => __( 'Ingredient', "intense" ),
        'plural' => __( 'Ingredients', "intense" )
      ),
      array(
        'key' => 'intense_recipes_cuisines',
        'singular' => __( 'Cuisine', "intense" ),
        'plural' => __( 'Cuisines', "intense" )
      ),
      array(
        'key' => 'intense_recipes_courses',
        'singular' => __( 'Course', "intense" ),
        'plural' => __( 'Courses', "intense" )
      ),
      array(
        'key' => 'intense_recipes_skill_levels',
        'singular' => __( 'Skill Levels', "intense" ),
        'plural' => __( 'Skill Levels', "intense" )
      )
    );
    $this->fields = array(
      array (
        'key' => 'field_9t78j80h444c650',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_recipes_single_template',
        'type' => 'select',
        'choices' => array_merge(
            array( '' => '' ),
            intense_locate_available_plugin_templates('/custom-post/intense_recipes/single/'),
            intense_locate_single_cpt_templates( 'recipes', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52e09df8cc31e',
        'label' => __( 'Yield', "intense" ),
        'name' => 'intense_recipe_yield',
        'type' => 'text',
        'instructions' => __( 'How much/many does the recipe produce?', "intense" ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52e09e2fcc31f',
        'label' => __( 'Servings', "intense" ),
        'name' => 'intense_recipe_servings',
        'type' => 'number',
        'instructions' => __( 'How many servings?', "intense" ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '',
        'max' => '',
        'step' => '',
      ),
      array (
        'key' => 'field_52e0a0b5cc323',
        'label' => __( 'Prep Time', "intense" ),
        'name' => 'intense_recipe_prep_time',
        'type' => 'number',
        'instructions' => __( 'How many minutes? (60+ minutes will be converted to hours)', "intense" ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '',
        'max' => '',
        'step' => '',
      ),
      array (
        'key' => 'field_52e0a0f9cc324',
        'label' => __( 'Cook Time', "intense" ),
        'name' => 'intense_recipe_cook_time',
        'type' => 'number',
        'instructions' => __( 'How many minutes? (60+ minutes will be converted to hours)', "intense" ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '',
        'max' => '',
        'step' => '',
      ),
      array (
        'key' => 'field_52e0a180cc326',
        'label' => __( 'Ingredients', "intense" ),
        'name' => 'intense_recipe_ingredients',
        'type' => 'repeater',
        'sub_fields' => array (
          array (
            'key' => 'field_52e0a1d9cc327',
            'label' => __( 'Amount', "intense" ),
            'name' => 'intense_recipe_amount',
            'type' => 'text',
            'column_width' => 10,
            'default_value' => '',
            'placeholder' => 0,
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_52e0a291cc328',
            'label' => __( 'Measurement', "intense" ),
            'name' => 'intense_recipe_measurement',
            'type' => 'text',
            'column_width' => 20,
            'default_value' => '',
            'placeholder' => __( 'cup, TBSP, tsp', "intense" ),
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_52e0a36ecc329',
            'label' => __( 'Ingredient', "intense" ),
            'name' => 'intense_recipe_ingredient',
            'type' => 'taxonomy',
            'taxonomy' => 'intense_recipes_ingredients',
            'field_type' => 'select',
            'allow_null' => 0,
            'load_save_terms' => 0,
            'return_format' => 'object',
            'multiple' => 0,
          ),
          array (
            'key' => 'field_52e0a3d9cc32a',
            'label' => __( 'Note', "intense" ),
            'name' => 'intense_recipe_note',
            'type' => 'textarea',
            'column_width' => 40,
            'default_value' => '',
            'placeholder' => __( 'chopped, fresh, ground', "intense" ),
            'maxlength' => '',
            'formatting' => 'br',
          ),
        ),
        'row_min' => '',
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => __( 'Add Ingredient', "intense" ),
      ),
      array (
        'key' => 'field_52e0a437cc32b',
        'label' => __( 'Instructions', "intense" ),
        'name' => 'intense_recipe_instructions',
        'type' => 'repeater',
        'sub_fields' => array (
          array (
            'key' => 'field_52e0a460cc32c',
            'label' => __( 'Description', "intense" ),
            'name' => 'intense_recipe_description',
            'type' => 'textarea',
            'instructions' => __( 'Describe this step', "intense" ),
            'column_width' => 60,
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'formatting' => 'br',
          ),
          array (
            'key' => 'field_52e0a4b1cc32d',
            'label' => __( 'Image', "intense" ),
            'name' => 'intense_recipe_image',
            'type' => 'image',
            'column_width' => 40,
            'save_format' => 'object',
            'preview_size' => 'thumbnail',
            'library' => 'all',
          ),
        ),
        'row_min' => '',
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => __( 'Add Instruction', "intense" ),
      ),
    );
  }
}
