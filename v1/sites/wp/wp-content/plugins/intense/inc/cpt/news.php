<?php

class Intense_Post_Type_News extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_news_singular'] ) ? $intense_visions_options['intense_cpt_news_singular'] : __( 'News', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_news_plural'] ) ? $intense_visions_options['intense_cpt_news_plural'] : __( 'News', 'intense' ) );
    
    $this->type = 'intense_news';
    $this->title = 'Enter ' . $name_singular . ' Title';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-intense-newspaper';
    $this->category_taxonomy_key = 'intense_news_category';
    $this->taxonomies = '';
    $this->fields = array(
      array (
        'key' => 'field_52f6d5be8e717',
        'label' => __( 'Subtitle', "intense" ),
        'name' => 'intense_news_subtitle',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_584gf567m898a327',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_news_single_template',
        'type' => 'select',
        'choices' => array_merge(
            array( '' => '' ),
            intense_locate_available_plugin_templates('/custom-post/intense_news/single/'),
            intense_locate_single_cpt_templates( 'news', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52d74722dc9ce',
        'label' => __( 'Author', 'intense' ),
        'name' => 'intense_news_author',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d759f677b66',
        'label' => __( 'Date', 'intense' ),
        'name' => 'intense_news_date',
        'type' => 'date_picker',
        'date_format' => 'yymmdd',
        'display_format' => 'mm/dd/yy',
        'first_day' => 0,
      ),
      array (
        'key' => 'field_52d75a5d77b67',
        'label' => __( 'Website', 'intense' ),
        'name' => 'intense_news_website',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d75b1377b68',
        'label' => __( 'Publisher', 'intense' ),
        'name' => 'intense_news_publisher',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d75b3077b69',
        'label' => __( 'Publisher Website', 'intense' ),
        'name' => 'intense_news_publisher_website',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    );
  }

  public function get_subtitle() {
    return get_field('intense_news_subtitle');
  }
}
