<?php

class Intense_Post_Type_Post extends Intense_Post_Type {
	function __construct() {
		$this->type = 'intense_post';
		$this->title = 'Enter Title';
		$this->singular = __( 'Post', 'intense' );
		$this->plural = __( 'Posts', 'intense' );
		$this->category_taxonomy_key = '';
		$this->taxonomies = '';
		$this->fields = array(
			array (
				'key' => 'field_47f6d5ah8n435',
				'label' => __( 'Subtitle', "intense" ),
				'name' => 'intense_post_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_7u890ky0h615a555',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_post_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/post/single/'),
					intense_locate_single_cpt_templates( 'post', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
		);
	}

	public function get_subtitle() {
		return get_field('intense_post_subtitle');
	}
}
