<?php

class Intense_Post_Type_Portfolio extends Intense_Post_Type {
	function __construct() {
    	global $intense_visions_options;
    	$name_singular = ( isset( $intense_visions_options['intense_cpt_portfolio_singular'] ) ? $intense_visions_options['intense_cpt_portfolio_singular'] : __( 'Portfolio', 'intense' ) );    
    	$name_plural = ( isset( $intense_visions_options['intense_cpt_portfolio_plural'] ) ? $intense_visions_options['intense_cpt_portfolio_plural'] : __( 'Portfolios', 'intense' ) );

	    $this->type = 'intense_portfolio';
	    $this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
		$this->icon = 'dashicons-portfolio';
		$this->category_taxonomy_key = 'portfolio_category';
		$this->taxonomies = array(
		  array(
		    'key' => 'portfolio_skills',
		    'singular' => __( 'Skill', "intense" ),
		    'plural' => __( 'Skills', "intense" )
		  )
		);
		$this->fields = array(
			array (
				'key' => 'field_47f6d5ah8n717',
				'label' => __( 'Subtitle', "intense" ),
				'name' => 'intense_portfolio_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_4r56j80h615a555',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_portfolio_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_portfolio/single/'),
					intense_locate_single_cpt_templates( 'portfolio', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d0e6db9b756',
				'label' => __( 'Date', 'intense' ),
				'name' => 'intense_portfolio_date',
				'type' => 'date_picker',
				'instructions' => '(yyyy-mm-dd)',
				'date_format' => 'yyyy-mm-dd',
				'display_format' => 'yy-mm-dd',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52d0e7a29b757',
				'label' => __( 'Designed By', 'intense' ),
				'name' => 'intense_designed_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e7cb9b758',
				'label' => __( 'Built By', 'intense' ),
				'name' => 'intense_built_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e7de9b759',
				'label' => __( 'Produced By', 'intense' ),
				'name' => 'intense_produced_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e7ed9b75a',
				'label' => __( 'Year Completed', 'intense' ),
				'name' => 'intense_year_completed',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e82c9b75b',
				'label' => __( 'Video Embed Code', 'intense' ),
				'name' => 'intense_video_embed_code',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_52d0e84f9b75c',
				'label' => __( 'Youtube/Vimeo Video URL for Lightbox', 'intense' ),
				'name' => 'intense_video_url',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e8a79b75d',
				'label' => __( 'Project URL', 'intense' ),
				'name' => 'intense_project_url',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e8c39b75e',
				'label' => __( 'Project URL Text', 'intense' ),
				'name' => 'intense_project_url_text',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e8e99b75f',
				'label' => __( 'Copyright URL', 'intense' ),
				'name' => 'intense_copy_url',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e9079b760',
				'label' => __( 'Copyright URL Text', 'intense' ),
				'name' => 'intense_copy_url_text',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		);
	}

	public function get_subtitle() {
		return get_field('intense_portfolio_subtitle');
	}
}
