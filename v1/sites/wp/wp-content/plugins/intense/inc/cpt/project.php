<?php

class Intense_Post_Type_Project extends Intense_Post_Type {
	function __construct() {
    	global $intense_visions_options;
    	$name_singular = ( isset( $intense_visions_options['intense_cpt_project_singular'] ) ? $intense_visions_options['intense_cpt_project_singular'] : __( 'Project', 'intense' ) );    
    	$name_plural = ( isset( $intense_visions_options['intense_cpt_project_plural'] ) ? $intense_visions_options['intense_cpt_project_plural'] : __( 'Projects', 'intense' ) );
    
    	$this->type = 'intense_project';
    	$this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
		$this->icon = 'dashicons-portfolio';
		$this->category_taxonomy_key = 'intense_project_category';
		$this->taxonomies = array(
		  array(
		    'key' => 'intense_project_skills',
		    'singular' => __( 'Skill', "intense" ),
		    'plural' => __( 'Skills', "intense" )
		  )
		);
		$this->fields = array(
			array (
				'key' => 'field_46f6d5at8n717',
				'label' => __( 'Subtitle', "intense" ),
				'name' => 'intense_project_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_90r4j80h615a454',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_project_single_template',
				'type' => 'select',
				'choices' => array_merge(
					array( '' => '' ),
					intense_locate_available_plugin_templates('/custom-post/intense_project/single/'),
					intense_locate_single_cpt_templates( 'project', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52d0e6bd9b756',
				'label' => __( 'Date', 'intense' ),
				'name' => 'intense_project_date',
				'type' => 'date_picker',
				'instructions' => '(yyyy-mm-dd)',
				'date_format' => 'yyyy-mm-dd',
				'display_format' => 'yy-mm-dd',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52d0e7b39b757',
				'label' => __( 'Designed By', 'intense' ),
				'name' => 'intense_project_designed_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e7cg8b758',
				'label' => __( 'Built By', 'intense' ),
				'name' => 'intense_project_built_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52d0e7h89b759',
				'label' => __( 'Produced By', 'intense' ),
				'name' => 'intense_project_produced_by',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_4k88j0kp8h139',
				'label' => __( 'Location Name', 'intense' ),
				'name' => 'intense_project_location',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_4p60j0kp8h351',
				'label' => __( 'Location Address', 'intense' ),
				'name' => 'intense_project_location_address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_3p61p0kp8h332',
				'label' => __( 'Company Name', 'intense' ),
				'name' => 'intense_project_company',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_8i60n7kp8h35r',
				'label' => __( 'Company Address', 'intense' ),
				'name' => 'intense_project_company_address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_47f6d5kp8h080',
				'label' => __( 'Quantity', 'intense' ),
				'name' => 'intense_project_quantity',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		);
	}
  
	public function get_subtitle() {
		return get_field('intense_project_subtitle');
	}
}
