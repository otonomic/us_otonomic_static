<?php

class Intense_Post_Type_Coupons extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_coupons_singular'] ) ? $intense_visions_options['intense_cpt_coupons_singular'] : __( 'Coupon', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_coupons_plural'] ) ? $intense_visions_options['intense_cpt_coupons_plural'] : __( 'Coupons', 'intense' ) );
    
    $this->type = 'intense_coupons';
    $this->title = 'Enter ' . $name_singular . ' Title';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-intense-coupon';
    $this->category_taxonomy_key = 'intense_coupons_category';
    $this->taxonomies = '';
    $this->fields = array(
      array (
        'key' => 'field_52ft624ht9712',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_coupons_single_template',
        'type' => 'select',
        'choices' => array_merge(
          array( '' => '' ),
          intense_locate_available_plugin_templates('/custom-post/intense_coupons/single/'),
          intense_locate_single_cpt_templates( 'coupons', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52d0ea0e6fa84',
        'label' => __( 'Start Date', 'intense' ),
        'name' => 'intense_coupon_start_date',
        'type' => 'date_picker',
        'date_format' => 'yymmdd',
        'display_format' => 'mm/dd/yy',
        'first_day' => 0,
      ),
      array (
        'key' => 'field_52d0ea846fa85',
        'label' => __( 'End Date', 'intense' ),
        'name' => 'intense_coupon_end_date',
        'type' => 'date_picker',
        'date_format' => 'yymmdd',
        'display_format' => 'mm/dd/yy',
        'first_day' => 0,
      ),
    );
  }

  public function get_excerpt( $limit ) {
    $excerpt = explode( ' ', get_field('intense_coupon_body'), $limit );

    return $this->get_clean_excerpt( $excerpt, $limit );
  }

  public function get_content( $limit ) {
    $content = get_field('intense_coupon_body');

    return $this->get_clean_content( $content, $limit );
  }
}
