<?php

class Intense_Post_Type_Faq extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_faq_singular'] ) ? $intense_visions_options['intense_cpt_faq_singular'] : __( 'FAQ', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_faq_plural'] ) ? $intense_visions_options['intense_cpt_faq_plural'] : __( 'FAQs', 'intense' ) );
    
    $this->type = 'intense_faq';
    $this->title = 'Enter ' . $name_singular . ' Title';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-editor-help';
    $this->category_taxonomy_key = 'faq_category';
    $this->taxonomies = '';
    $this->fields = array(
      array (
        'key' => 'field_93f6g77g8a870',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_faq_single_template',
        'type' => 'select',
        'choices' => array_merge(
          array( '' => '' ),
          intense_locate_available_plugin_templates('/custom-post/intense_faq/single/'),
          intense_locate_single_cpt_templates( 'faq', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52d0ebc1bb366',
        'label' => __( 'External Link', 'intense' ),
        'name' => 'intense_faq_link',
        'type' => 'text',
        'instructions' => __( 'Link to an external page when using [intense_faq] shortcode', 'intense' ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d0ebefbb367',
        'label' => __( 'External Link Target', 'intense' ),
        'name' => 'intense_faq_link_target',
        'type' => 'select',
        'choices' => array (
          '_blank' => __( 'Blank', 'intense' ),
          '_self' => __( 'Self', 'intense' ),
          '_parent' => __( 'Parent', 'intense' ),
          '_top' => __( 'Top', 'intense' ),
        ),
        'default_value' => '_blank',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52d3ebc1bft369',
        'label' => __( 'CSS Class', 'intense' ),
        'name' => 'intense_faq_css_class',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52e72e0c7b39b',
        'label' => __( 'Title Background', 'intense' ),
        'name' => 'intense_faq_title_background',
        'type' => 'color_picker',
        'default_value' => '#ffffff',
      ),
      array (
        'key' => 'field_52f73e0c7b49b',
        'label' => __( 'Content Background', 'intense' ),
        'name' => 'intense_faq_content_background',
        'type' => 'color_picker',
        'default_value' => '#ffffff',
      ),
      array (
        'key' => 'field_52g74e0c7b59b',
        'label' => __( 'Title Font Color', 'intense' ),
        'name' => 'intense_faq_title_font_color',
        'type' => 'color_picker',
        'default_value' => '',
      ),
      array (
        'key' => 'field_52h75e0c7b69b',
        'label' => __( 'Content Font Color', 'intense' ),
        'name' => 'intense_faq_content_font-color',
        'type' => 'color_picker',
        'default_value' => '',
      ),
    );
  }
}
