<?php


class Intense_Post_Type_Movies extends Intense_Post_Type {
	function __construct() {
	    global $intense_visions_options;
	    $name_singular = ( isset( $intense_visions_options['intense_cpt_movies_singular'] ) ? $intense_visions_options['intense_cpt_movies_singular'] : __( 'Movie', 'intense' ) );    
	    $name_plural = ( isset( $intense_visions_options['intense_cpt_movies_plural'] ) ? $intense_visions_options['intense_cpt_movies_plural'] : __( 'Movies', 'intense' ) );

	    $this->type = 'intense_movies';
	    $this->title = 'Enter ' . $name_singular . ' Title';
		$this->singular = $name_singular;
		$this->plural = $name_plural;
		$this->icon = 'dashicons-video-alt';
		$this->category_taxonomy_key = '';
		$this->taxonomies = array(
		  array(
		    'key' => 'intense_movie_genre',
		    'singular' => __( 'Genre', "intense" ),
		    'plural' => __( 'Genres', "intense" )
		  ),
		  array(
		    'key' => 'intense_movie_cast',
		    'singular' => __( 'Actor', "intense" ),
		    'plural' => __( 'Cast', "intense" )
		  ),
		  array(
		    'key' => 'intense_movie_director',
		    'singular' => __( 'Director', "intense" ),
		    'plural' => __( 'Directors', "intense" )
		  )
		);
		$this->fields = array(
			array (
				'key' => 'field_52f6d5me8n717',
				'label' => __( 'Subtitle', "intense" ),
				'name' => 'intense_movie_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_6tygj80444ta555',
				'label' => __( 'Single Post Template', "intense" ),
				'name' => 'intense_movies_single_template',
				'type' => 'select',
				'choices' => array_merge( 
					array( '' => '' ), 
					intense_locate_available_plugin_templates('/custom-post/intense_movies/single/'),
					intense_locate_single_cpt_templates( 'movies', true )
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52e6y7b1gy32d',
				'label' => __( 'Cover Image', "intense" ),
				'name' => 'intense_movie_image',
				'type' => 'image',
				'column_width' => 40,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
            array (
              'key' => 'field_52d60y76g7544',
              'label' => __( 'Website', 'intense' ),
              'name' => 'intense_movie_website',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_52d50ffbb7544',
              'label' => __( 'Purchase Link', 'intense' ),
              'name' => 'intense_movie_purchase_link',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
			array (
			    'key' => 'field_52f6d5548a760',
			    'label' => __( 'MPAA Rating', "intense" ),
			    'name' => 'intense_movie_rating',
			    'type' => 'select',
			    'choices' => array (
			      'G' => __( 'G (General Audiences)', "intense" ),
			      'PG' => __( 'PG (Parental Guidance Suggested)', "intense" ),
			      'PG13' => __( 'PG-13 (Parents Strongly Cautioned)', "intense" ),
			      'R' => __( 'R (Restricted)', "intense" ),
			      'NC-17' => __( 'NC-17 (No One 17 and Under Admitted)', "intense" ),
			    ),
			    'default_value' => '',
			    'allow_null' => 0,
			    'multiple' => 0,
			),
			array (
				'key' => 'field_52u89dj7cc31e',
				'label' => __( 'Trailer', "intense" ),
				'name' => 'intense_movie_trailer',
				'type' => 'text',
				'instructions' => __( 'Enter a Youtube or Vimeo URL or ID', "intense" ),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_77uhd5me8n717',
				'label' => __( 'Runtime', "intense" ),
				'name' => 'intense_movie_runtime',
				'type' => 'number',
				'instructions' => __( 'How many minutes? (60+ minutes will be converted to hours)', "intense" ),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_52f6c588e5fd1',
				'label' => __( 'Theatrical Release Date', "intense" ),
				'name' => 'intense_movie_release_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52f6c650e5fd2',
				'label' => __( 'DVD Release Date', "intense" ),
				'name' => 'intense_movie_dvd_release_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_52f6c797e5fd3',
				'label' => __( 'Languages', "intense" ),
				'name' => 'intense_movie_languages',
				'type' => 'repeater',
				'instructions' => __( 'Enter languages that the movie is released in (English, Spanish, etc...)', "intense" ),
				'sub_fields' => array (
					array (
						'key' => 'field_52f6c896e5fd4',
						'label' => __( 'Language', "intense" ),
						'name' => 'intense_movie_language',
						'type' => 'text',
						'column_width' => 100,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => __( 'Add Language', "intense" ),
			),
			array (
				'key' => 'field_52f6c94ee5fd5',
				'label' => __( 'Subtitles', "intense" ),
				'name' => 'intense_movie_subtitles',
				'type' => 'repeater',
				'instructions' => __( 'Enter subtitle languages (English, Spanish, etc...)', "intense" ),
				'sub_fields' => array (
					array (
						'key' => 'field_52f6c978e5fd6',
						'label' => __( 'Language', "intense" ),
						'name' => 'intense_movie_subtitle_language',
						'type' => 'text',
						'column_width' => 100,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => __( 'Add Language', "intense" ),
			),
			array (
				'key' => 'field_52f6cbb6e5fd7',
				'label' => __( 'Reviews', "intense" ),
				'name' => 'intense_movie_reviews',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52f6cc37e5fd8',
						'label' => __( 'Reviewer', "intense" ),
						'name' => 'intense_movie_reviewer',
						'type' => 'text',
						'column_width' => 30,
						'default_value' => '',
						'placeholder' => 'Name of reviewer',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_52f6d8440a38a',
						'label' => __( 'Review Date', "intense" ),
						'name' => 'intense_movie_review_date',
						'type' => 'date_picker',
						'column_width' => 15,
						'date_format' => 'yymmdd',
						'display_format' => 'mm/dd/yy',
						'first_day' => 0,
					),
					array (
						'key' => 'field_52f6cd55e5fda',
						'label' => __( 'Rating', "intense" ),
						'name' => 'intense_movie_review_rating',
						'type' => 'number',
						'column_width' => 10,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 0,
						'max' => 5,
						'step' => '.5',
					),
					array (
						'key' => 'field_52f6cd13e5fd9',
						'label' => __( 'Review', "intense" ),
						'name' => 'intense_movie_review',
						'type' => 'textarea',
						'column_width' => 45,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => __( 'Add Review', "intense" ),
			),
		);
	}

	public function get_subtitle() {
		return get_field('intense_movie_subtitle');
	}
}
