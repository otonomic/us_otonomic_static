<?php

class Intense_Post_Type_Locations extends Intense_Post_Type {
  function __construct() {
    global $intense_visions_options;
    $name_singular = ( isset( $intense_visions_options['intense_cpt_locations_singular'] ) ? $intense_visions_options['intense_cpt_locations_singular'] : __( 'Location', 'intense' ) );    
    $name_plural = ( isset( $intense_visions_options['intense_cpt_locations_plural'] ) ? $intense_visions_options['intense_cpt_locations_plural'] : __( 'Locations', 'intense' ) );
    
    $this->type = 'intense_locations';
    $this->title = 'Enter ' . $name_singular . ' Name';
    $this->singular = $name_singular;
    $this->plural = $name_plural;
    $this->icon = 'dashicons-location-alt';
    $this->category_taxonomy_key = 'intense_locations_category';
    $this->taxonomies = '';
    $this->fields = array(
      array (
        'key' => 'field_636gj80h898a555',
        'label' => __( 'Single Post Template', "intense" ),
        'name' => 'intense_locations_single_template',
        'type' => 'select',
        'choices' => array_merge(
            array( '' => '' ),
            intense_locate_available_plugin_templates('/custom-post/intense_locations/single/'),
            intense_locate_single_cpt_templates( 'locations', true )
        ),
        'default_value' => '',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array (
        'key' => 'field_52d61fe0ac1f0',
        'label' => __( 'Type', 'intense' ),
        'name' => 'intense_location_type',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d61ec9ac1eb',
        'label' => __( 'Address', 'intense' ),
        'name' => 'intense_location_address',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d61fb9ac1ef',
        'label' => __( 'Website', 'intense' ),
        'name' => 'intense_location_website',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_52d61f2eac1ed',
        'label' => __( 'Attractions', 'intense' ),
        'name' => 'intense_location_attractions',
        'type' => 'wp_wysiwyg',
        'default_value' => '',
        'teeny' => 0,
        'media_buttons' => 1,
        'dfw' => 1,
      ),
      array (
        'key' => 'field_52d61f90ac1ee',
        'label' => __( 'History', 'intense' ),
        'name' => 'intense_location_history',
        'type' => 'wp_wysiwyg',
        'default_value' => '',
        'teeny' => 0,
        'media_buttons' => 1,
        'dfw' => 1,
      ),
    );
  }
}
