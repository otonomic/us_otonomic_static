<?php

global $intense_post_types;

class Intense_Post_Type {
  public $type;
  public $title;
  public $singular;
  public $plural;
  public $fields;
  public $icon;

  public function register() {
    do_action( 'intense/cpt/register/before', $this );

    //CPT's
    if ( $this->type != 'intense_post' ) {
      $tag = 'post_tag';
      if ( $this->type == 'intense_templates' || $this->type == 'intense_snippets' ) {
        $tag = '';
      }

      register_post_type(
        $this->type,
        array(
          'labels' => array(
            'name' => $this->plural,
            'singular_name' => $this->singular,
            'menu_name' => $this->plural,
            'all_items' => __( 'All', "intense" ) . ' ' . $this->plural,
            'add_new_item' => __( 'Add New', "intense" ) . ' ' . $this->singular,
            'edit_item' => __( 'Edit', "intense" ) . ' ' . $this->singular,
            'new_item' => __( 'New', "intense" ) . ' ' . $this->singular,
            'view_item' => __( 'View', "intense" ) . ' ' . $this->singular,
            'search_items' => __( 'Edit', "intense" ) . ' ' . $this->singular,
            'not_found' => __( 'No', "intense" ) . ' ' . $this->plural . ' ' . __( 'found', "intense" ),
            'not_found_in_trash' => __( 'No', "intense" ) . ' ' . $this->plural . ' ' . __( 'found in Trash', "intense" )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array( 'slug' =>  strtolower( $this->singular ) ),
          'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
          'can_export' => true,
          'menu_icon'=> $this->icon,
          'taxonomies' => array( $tag ),
        )
      );
    }

    if ( function_exists( "register_field_group" ) && $this->type != 'intense_snippets' ) {
      $postType = $this->type;

      if ( $this->type == 'intense_post' ) {
        $postType = 'post';
      }

      register_field_group( array (
          'id' => 'acf_' . strtolower( $this->singular ) . '-options',
          'title' => $this->singular . ' ' . __( 'Options', "intense" ),
          'fields' => $this->fields,
          'location' => array (
            array (
              array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => $postType,
                'order_no' => 0,
                'group_no' => 0,
              ),
            ),
          ),
          'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
          ),
          'menu_order' => 0,
        ) );
    }

    if ( isset( $this->category_taxonomy_key ) && $this->category_taxonomy_key != '' ) {
      $this->register_category_taxonomy( $this->category_taxonomy_key );
    }

    if ( is_array( $this->taxonomies ) ) {
      foreach ( $this->taxonomies as $key => $taxonomy ) {
        if ( is_array( $taxonomy ) ) {
          $this->register_taxonomy( $taxonomy['key'] ,$taxonomy['singular'], $taxonomy['plural'] );
        }
      }
    }

    do_action( 'intense/cpt/register/after', $this );
  }

  public function register_category_taxonomy( $key ) {
    register_taxonomy( $key, $this->type, array( 'hierarchical' => true, 'label' => $this->singular . ' ' . __( 'Categories', "intense" ), 'query_var' => true, 'rewrite' => true ) );
  }

  public function register_taxonomy( $key, $singular, $plural ) {    
    $labels = array();
    $labels['name'] = $plural;
    $labels['singular_name']   = $singular;
    $labels['search_items']    = __( 'Search', "intense" ) . ' ' . $plural;
    $labels['add_new_item']    = __( 'Add New', "intense" ) . ' ' . $singular;
    $labels['new_item_name']   = __( 'New', "intense" ) . ' ' . $singular . ' ' . __( 'Name', "intense" );
    $labels['menu_name']     = $plural;

    register_taxonomy( $key, $this->type, array( 'hierarchical' => true, 'label' => $plural, 'labels' => $labels, 'query_var' => true, 'rewrite' => true ) );
  }

  public function get_excerpt( $limit ) {
    $excerpt = explode( ' ', get_the_excerpt(), $limit );

   return $this->get_clean_excerpt( $excerpt, $limit );
  }

  protected function get_clean_excerpt( $excerpt, $limit ) {
     if ( count( $excerpt ) >= $limit ) {
      array_pop( $excerpt );
      $excerpt = implode( " ", $excerpt ).'...';
    } else {
      $excerpt = implode( " ", $excerpt );
    }

    $excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );

    return $excerpt;
  }

  public function get_content( $limit ) {
    $content = get_the_content();

    return $this->get_clean_content( $content, $limit );
  }

  protected function get_clean_content( $content, $limit ) {
    $original_content = preg_replace( "~(?:\[/?)[^/\]]+/?\]~s", '', $content );

    $content = explode( ' ', $original_content, $limit );
    
    if ( count( $content ) >= $limit ) {
      array_pop( $content );
      $content = implode( " ", $content ).'...';
    } else {
      $content = implode( " ", $content );
    }

    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );

    return $content;
  }

  public function get_subtitle() {
    return '';
  }
}

class Intense_Post_Types {
  public $post_types;

  function __construct() {
    $this->add_types();

    add_action( 'wp_loaded', array( $this, 'register' ) );
    add_filter( 'enter_title_here', array( $this, 'get_title' ) );
    add_filter( 'template_include', array( $this, 'content_single' ) );
  }

  function add_types() {
    global $intense_visions_options;

    $types = array (
      "books",
      "clients",
      "coupons",
      "events",
      "faq",
      "jobs",
      "locations",
      "movies",
      "news",
      "portfolio",
      "post",
      //"pricing",
      "project",
      "quotes",
      "recipes",
      "snippets",
      "team",
      "templates",
      "testimonials",
    );

    foreach ( $types as $custom_post_type ) {
      if ( isset( $intense_visions_options[ 'intense_cpt_' . $custom_post_type ] ) &&  $intense_visions_options[ 'intense_cpt_' . $custom_post_type ] ) {
        require_once INTENSE_PLUGIN_FOLDER . '/inc/cpt/' . $custom_post_type . '.php';

        $class = "Intense_Post_Type_" . ucfirst( $custom_post_type );
        $object = new $class();

        $this->post_types[ strtolower( $class ) ] = $object;
      }
    }

    require_once INTENSE_PLUGIN_FOLDER . '/inc/cpt/post.php';
    $this->post_types[ 'intense_post_type_post' ] = new Intense_Post_Type_Post();
  }

  function is_type_enabled( $type ) {
    $post_type = $this->get_post_type( $type );

    if ( isset( $post_type ) ) {
      return true;
    } else {
      return false;
    }
    
  }

  function register() {
    foreach ($this->post_types as $key => $value) {
      $value->register();
    }
  }

  function get_post_type( $type ) {
    $class = strtolower( "Intense_Post_Type_" . ucfirst( str_replace( 'intense_', '', $type ) ) );

    if ( isset( $this->post_types[ $class ] ) ) {
      return  $this->post_types[ $class ];
    }

    return null;
  }

  function get_title( $title ) {
    $screen = get_current_screen();    
    $type = $screen->post_type;
    $post_type = $this->get_post_type( $type );

    if ( isset( $post_type ) ) {
      $title = $post_type->title;
    }
   
    return $title;
  }

  function content_single( $single_template ) {
    global $intense_visions_options;

    $post_type = $this->get_post_type( get_post_type() );

    if ( is_single() && !is_page() && !empty( $post_type ) && get_post_type() != 'intense_templates'  ) {
      if ( get_field( get_post_type() . '_single_template' ) != '' ) {
        $template = get_field( 'intense_' . str_replace( 'intense_', '', get_post_type() ) . '_single_template' );
      } else {
        $template = $intense_visions_options[ 'intense_cpt_' . str_replace( 'intense_', '', get_post_type() ) . '_single' ];
      }      

      // if the template is numeric then it is a templates that they have saved in WordPress
      if ( is_numeric( str_replace("template_", "", $template ) ) ) {
        $template_post = get_post( str_replace("template_", "", $template ) );
        $found_template = Intense_Post_Type_Templates::get_template_cache( $template_post->ID );
      } else {
        $found_template = intense_locate_plugin_template( '/custom-post/' . get_post_type() . '/single/' . $template . '.php' );
      }

      if ( !empty( $found_template ) ) {
        $single_template = $found_template;
      }
    }

    return $single_template;
  }
}

$intense_post_types = new Intense_Post_Types();

