<?php
$wp_sizes = get_intermediate_image_sizes();
$wp_sizes = array_combine( $wp_sizes, $wp_sizes );

$sections['Photo_Sources'] = array(
	'title' => __( 'Photo Sources', 'intense' ),
	//'desc' => __( '<p class="description">Photos can be pulled from several different sources. For some of the sources, authentication is required.</p>', 'intense' ),
	'icon' => 'el-icon-picture',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_photosource_full_size',
			'type' => 'select',
			'title' => __( "Desired Full Size Image", "intense" ),
			'desc' => __( "The size of photos to use when viewing a full size version of the photos. Sizes 1024 through 1600 are suggested for retina support. If you want to use a custom image size that you have defined in the extras options, simply reload this page after saving the custom image size.", "intense" ),
			'options' => $wp_sizes,
			'default' => 'large1600',
		),

		array(
			'id'=>'intense_flickr',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Flickr', 'intense' ),
		),
		array(
			'id'=>'intense_flickr_consumer_key',
			'type' => 'text',
			'title' => __( 'API Key', 'intense' ),
			'desc' => __( "The API key used to access the Flickr API.  Get your own key from <a target='_blank' href='http://www.flickr.com/services/apps/create/apply'>Flickr App Garden</a><br />Or use the default: e63611cdc6277814e1f836e7c23a93a9", "intense" ),
			'default' => 'e63611cdc6277814e1f836e7c23a93a9'
		),

		array(
			'id'=>'intense_smugmug',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'SmugMug', 'intense' ),
		),
		array(
			'id'=>'intense_smug_api_key',
			'type' => 'text',
			'title' => __( 'API Key', 'intense' ),
			'desc' => __( "The API key used to access the SmugMug API.  Get your own key from <a target='_blank' href='http://www.smugmug.com/hack/apikeys'>http://www.smugmug.com/hack/apikeys</a><br />Or use the default: TRj23D3KKjhfck3D19d6SWrSSU1eaDLt", "intense" ),
			'default' => 'TRj23D3KKjhfck3D19d6SWrSSU1eaDLt'
		),

		array(
			'id'=>'intense_zenfolio',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Zenfolio', 'intense' ),
		),
		array(
			'id'=>'intense_zenfolio_app_name',
			'type' => 'text',
			'title' => __( 'App Name', 'intense' ),
			'desc' => __( "The application name used to access the Zenfolio API. You can enter your own or user the default (default: Intensity)", "intense" ),
			'default' => 'Intensity'
		),

		array(
			'id'=>'intense_instagram',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Instagram', 'intense' ),
		),
		array(
			'id'=>'intense_instagram_section',
			'type' => 'info',
			'icon'=>'el-icon-info-sign',
			'desc' => "Authenticating with Instagram is a multi-step process.
					<ul><li>-First, enter your Client ID and Secret. This can be obtained from <a target='_blank' href='http://instagram.com/developer/clients/manage/'>http://instagram.com/developer/clients/manage/</a>.<br />When setting up a new client, use <br><strong>" . $this->instagram_OAuth->redirect_url . "</strong><br />as your redirect url.</li>
					<li>-After entering your Client ID and Secret, click 'Save Changes' at the top or bottom of this page.</li>
					<li>-After the page reloads, you will see a link to authenticate just below this line.</li>
					" . ( $this->instagram_OAuth->show_authentication_link() ? "<li>-Click on the link " . $this->instagram_OAuth->get_authentication_link() . " and follow the instructions.</li>" : "" ) . "<ul>",
		),
		array(
			'id'=>'intense_instagram_client_id',
			'type' => 'text',
			'title' => __( "Client ID", "intense" ),
			'desc' => __( "The client id used for authenticating with Instagram.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_instagram_client_secret',
			'type' => 'text',
			'title' => __( "Client Secret", "intense" ),
			'desc' => __( "The client secret to use for authenticating with Instagram.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_instagram_access_token',
			'type' => 'text',
			'title' => __( "Access Token", "intense" ),
			'desc' => __( "The access token for authenticating with Instagram. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),

		array(
			'id'=>'intense_facebook',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Facebook', 'intense' ),
		),
		array(
			'id'=>'intense_facebook_section',
			'type' => 'info',
			'icon'=>'el-icon-info-sign',
			'desc' => "Authenticating with Facebook is a multi-step process.
					<ul><li>-First, enter your App ID/API Key and App Secret. This can be obtained from <a target='_blank' href='https://developers.facebook.com/apps'>https://developers.facebook.com/apps</a>.<br />When setting up a new application, use <br><strong>" . $this->facebook_OAuth->redirect_url . "</strong><br />as your valid OAuth redirect URIs.  This can be found under the advanced settings.</li>
				<li>-After entering your App ID/API Key and App Secret, click 'Save Changes' at the top or bottom of this page.</li>
					<li>-After the page reloads, you will see a link to authenticate just below this line.</li>
				" . ( $this->facebook_OAuth->show_authentication_link() ? "<li>-Click on the link " . $this->facebook_OAuth->get_authentication_link() . " and follow the instructions.</li>" : "" ) . "<ul>",
		),
		array(
			'id'=>'intense_facebook_client_id',
			'type' => 'text',
			'title' => __( "App ID/API Key", "intense" ),
			'desc' => __( "The App ID/API Key used for authenticating with Facebook.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_facebook_client_secret',
			'type' => 'text',
			'title' => __( "App Secret", "intense" ),
			'desc' => __( "The app secret to use for authenticating with Facebook.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_facebook_access_token',
			'type' => 'text',
			'title' => __( "Access Token", "intense" ),
			'desc' => __( "The access token for authenticating with Facebook. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),

		array(
			'id'=>'intense_google',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Google', 'intense' ),
		),
		array(
			'id'=>'intense_google_section',
			'type' => 'info',
			'icon'=>'el-icon-info-sign',
			'desc' => "Authenticating with Google is a multi-step process.
					<ul><li>-First, enter your App ID/API Key and App Secret. This can be obtained from <a target='_blank' href='https://code.google.com/apis/console#access'>https://code.google.com/apis/console#access</a>.<br />When setting up a new application, use <br><strong>" . $this->google_OAuth->redirect_url . "</strong><br />as your valid OAuth redirect URIs.  This can be found under the advanced settings.</li>
				<li>-After entering your App ID/API Key and App Secret, click 'Save Changes' at the top or bottom of this page.</li>
					<li>-After the page reloads, you will see a link to authenticate just below this line.</li>
				" . ( $this->google_OAuth->show_authentication_link() ? "<li>-Click on the link " . $this->google_OAuth->get_authentication_link() . " and follow the instructions.</li>" : "" ) . "<ul>",
		),
		array(
			'id'=>'intense_google_client_id',
			'type' => 'text',
			'title' => __( "Client ID", "intense" ),
			'desc' => __( "The Client ID used for authenticating with Google.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_google_client_secret',
			'type' => 'text',
			'title' => __( "Client Secret", "intense" ),
			'desc' => __( "The client secret to use for authenticating with Google.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_google_access_token',
			'type' => 'text',
			'title' => __( "Access Token", "intense" ),
			'desc' => __( "The access token for authenticating with Google. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),

		array(
			'id'=>'intense_deviantART',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'deviant ART', 'intense' ),
		),
		array(
			'id'=>'intense_deviantART_section',
			'type' => 'info',
			'icon'=>'el-icon-info-sign',
			'desc' => "Authenticating with deviantART is a multi-step process.
					<ul><li>-First, enter your Client ID and Secret. This can be obtained from <a target='_blank' href='http://www.deviantart.com/submit/app/'>http://www.deviantart.com/submit/app/</a>. You will need to login to your deviantART account first.<br />When setting up a new client, use <br><strong>" . $this->deviantART_OAuth->redirect_url . "</strong><br />as your redirect url.</li>
				<li>-After entering your Client ID and Secret, click 'Save Changes' at the top or bottom of this page.</li>
					<li>-After the page reloads, you will see a link to authenticate just below this line.</li>
				" . ( $this->deviantART_OAuth->show_authentication_link()  ? "<li>-Click on the link " . $this->deviantART_OAuth->get_authentication_link() . " and follow the instructions.</li>" : "" ) . "<ul>",
		),
		array(
			'id'=>'intense_deviantART_client_id',
			'type' => 'text',
			'title' => __( "Client ID", "intense" ),
			'desc' => __( "The Client ID used for authenticating with deviantART.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_deviantART_client_secret',
			'type' => 'text',
			'title' => __( "Client Secret", "intense" ),
			'desc' => __( "The client secret to use for authenticating with deviantART.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_deviantART_access_token',
			'type' => 'text',
			'title' => __( "Access Token", "intense" ),
			'desc' => __( "The access token for authenticating with deviantART. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),

		array(
			'id'=>'intense_px500',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( '500px', 'intense' ),
		),
		array(
			'id'=>'intense_px500_section',
			'type' => 'info',
			'icon'=>'el-icon-info-sign',
			'desc' => "Authenticating with 500px is a multi-step process.
					<ul><li>-First, enter your Consumer Key and Secret. This can be obtained from <a target='_blank' href='http://500px.com/settings/applications'>http://500px.com/settings/applications/</a>. You will need to login to your 500px account first.<br />When registering a new application, use <br><strong>" . $this->px500_OAuth->redirect_url . "</strong><br />as your callback url.</li>
				<li>-After entering your Consumer Key and Secret, click 'Save Changes' at the top or bottom of this page.</li>
					<li>-After the page reloads, you will see a link to authenticate just below this line.</li>
				" . ( $this->px500_OAuth->show_authentication_link() ? "<li>-Click on the link " . $this->px500_OAuth->get_oauth_link() . " and follow the instructions.</li>" : "" ) . "<ul>",
		),
		array(
			'id'=>'intense_px500_consumer_key',
			'type' => 'text',
			'title' => __( "Consumer Key", "intense" ),
			'desc' => __( "The consumer key used for authenticating with 500px.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_px500_consumer_secret',
			'type' => 'text',
			'title' => __( "Consumer Secret", "intense" ),
			'desc' => __( "The consumer secret to use for authenticating with 500px.  See instructions.", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_px500_auth_token',
			'type' => 'text',
			'title' => __( "Auth Token", "intense" ),
			'desc' => __( "The auth token for authenticating with 500px. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),
		array(
			'id'=>'intense_px500_auth_token_secret',
			'type' => 'text',
			'title' => __( "Auth Token Secret", "intense" ),
			'desc' => __( "The auth token secret for authenticating with 500px. <strong>This will automatically be filled in after authentication</strong>", "intense" ),
			'default' => ''
		),
	)
);

