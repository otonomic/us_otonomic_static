<?php

function intense_migrate_v1_to_v2( $args ) {
	//global $ReduxFramework;
	//global $intense_visions_options;
	$intense_visions_options = array();

	$all_old_keys = array(
		"intense_color_options",
		"intense_main_color",
		"intense_info_color",
		"intense_success_color",
		"intense_warning_color",
		"intense_error_color",
		"intense_inverse_color",
		"intense_boxed_color",
		"intense_title_background_color",
		"intense_icon_color",
		"intense_content_box_style",
		"intense_shortcodes_options",
		"intense_content_section",
		"intense_content_section_box_class",
		"intense_content_section_breakout",
		"intense_content_section_top_margin",
		"intense_content_section_bottom_margin",
		"intense_content_section_right_margin",
		"intense_content_section_left_margin",
		"intense_content_section_top_padding",
		"intense_content_section_bottom_padding",
		"intense_content_section_right_padding",
		"intense_content_section_left_padding",
		"intense_layout_row",
		"intense_layout_row_top_margin",
		"intense_layout_row_bottom_margin",
		"intense_layout_row_top_padding",
		"intense_layout_row_bottom_padding",
		"intense_shortcode_css",
		"intense_shortcode_css_image",
		"intense_shortcode_css_mixitup",
		"intense_shortcode_css_posts",
		"intense_shortcode_css_shadow",
		"intense_shortcode_css_alert",
		"intense_shortcode_css_badge",
		"intense_shortcode_css_blockquote",
		"intense_shortcode_css_button",
		"intense_shortcode_css_collapsibles",
		"intense_shortcode_css_content_box",
		"intense_shortcode_css_content_section",
		"intense_shortcode_css_definitions",
		"intense_shortcode_css_dropcap",
		"intense_shortcode_css_hover_box",
		"intense_shortcode_css_hr",
		"intense_shortcode_css_icon",
		"intense_shortcode_css_label",
		"intense_shortcode_css_layout",
		"intense_shortcode_css_lead",
		"intense_shortcode_css_person",
		"intense_shortcode_css_progress",
		"intense_shortcode_css_promo",
		"intense_shortcode_css_slider",
		"intense_shortcode_css_table",
		"intense_shortcode_css_tabs",
		"intense_shortcode_css_testimony",
		"intense_shortcode_css_timeline",
		"intense_shortcode_css_tooltip",
		"intense_photosources_options",
		"photosource_full_size",
		"flickr_switch",
		"flickr_consumer_key",
		"smugmug_switch",
		"smug_api_key",
		"zenfolio_switch",
		"zenfolio_app_name",
		"instagram_switch",
		"instagram_section",
		"instagram_client_id",
		"instagram_client_secret",
		"instagram_access_token",
		"facebook_switch",
		"facebook_section",
		"facebook_client_id",
		"facebook_client_secret",
		"facebook_access_token",
		"google_switch",
		"google_section",
		"google_client_id",
		"google_client_secret",
		"google_access_token",
		"deviantART_switch",
		"deviantART_section",
		"deviantART_client_id",
		"deviantART_client_secret",
		"deviantART_access_token",
		"px500_switch",
		"px500_section",
		"px500_consumer_key",
		"px500_consumer_secret",
		"px500_auth_token",
		"px500_auth_token_secret",
		"intense_gallery_options",
		"colorbox_section",
		"colorbox_theme",
		"colorbox_transition",
		"colorbox_slideshow",
		"prettyPhoto_section",
		"prettyphoto_theme",
		"prettyphoto_social",
		"prettyphoto_slideshow",
		"galleria_section",
		"galleria_slideshow_switch",
		"galleria_lightbox",
		"galleria_transition",
		"supersized_section",
		"intense_supersized_color",
		"supersized_position",
		"slideshow_switch",
		"slideshow_autoplay",
		"slideshow_transition",
		"thumbnail_navigation",
		"thumbnail_links",
		"progress_bar",
		"stop_loop",
		"pause_hover",
		"keyboard_nav",
		"image_protect",
		"slideshow_navigation",
		"slide_counter",
		"slide_captions",
		"intense_cpt_options",
		"cptfaq",
		"cptportfolio",
		"cptproject",
		"intense_extra_options",
		"intense_missing_image",
		"intense_custom_css",
		"intense_html5_shiv",
		"intense_rtl",
		"intense_mixitup_section",
		"intense_mixitup_easing",
		"intense_mixitup_effects",
		"intense_custom_image_size_options",
		"intense_custom_image_sizes",
		"intense_backup_options",
		"intense_of_backup",
		"intense_of_transfer"
	);

	//commented out items can't be easily migrated
	$intense_easy_migrate_keys = array(
		"intense_main_color" => "intense_main_color",
		"intense_info_color" => "intense_info_color",
		"intense_success_color" => "intense_success_color",
		"intense_warning_color" => "intense_warning_color",
		"intense_error_color" => "intense_error_color",
		"intense_inverse_color" => "intense_inverse_color",
		"intense_boxed_color" => "intense_boxed_color",
		"intense_title_background_color" => "intense_title_background_color",
		"intense_icon_color" =>"intense_icon_color",
		//"intense_content_box_style" =>"intense_content_box_style",
		//"intense_missing_image" => "intense_missing_image",
		"intense_custom_css" => "intense_custom_css",
		"intense_html5_shiv" => "intense_html5_shiv",
		"intense_rtl" => "intense_rtl",
		"intense_mixitup_easing" => "intense_mixitup_easing",
		"intense_mixitup_effects" => "intense_mixitup_effects",
		//"intense_custom_image_sizes" => "intense_custom_image_sizes",
		"colorbox_theme" => "intense_colorbox_theme",
		"colorbox_transition" => "intense_colorbox_transition",
		"colorbox_slideshow" => "intense_colorbox_slideshow",
		"prettyphoto_theme" => "intense_prettyphoto_theme",
		"prettyphoto_social" => "intense_prettyphoto_social",
		"prettyphoto_slideshow" => "intense_prettyphoto_slideshow",
		"galleria_slideshow_switch" => "intense_galleria_slideshow",
		"galleria_lightbox" => "intense_galleria_lightbox",
		"galleria_transition" => "intense_galleria_transition",
		"intense_supersized_color" => "intense_supersized_color",
		"supersized_position" => "intense_supersized_position",
		"slideshow_switch" => "intense_supersized_slideshow",
		"slideshow_autoplay" => "intense_supersized_autoplay",
		"slideshow_transition" => "intense_supersized_slideshow_transition",
		"thumbnail_navigation" => "intense_supersized_thumbnail_navigation",
		"thumbnail_links" => "intense_supersized_thumbnail_links",
		"progress_bar" => "intense_supersized_progress_bar",
		"stop_loop" => "intense_supersized_stop_loop",
		"pause_hover" => "intense_supersized_pause_hover",
		"keyboard_nav" => "intense_supersized_keyboard_nav",
		"image_protect" => "intense_supersized_image_protect",
		"slideshow_navigation" => "intense_supersized_slideshow_navigation",
		"slide_counter" => "intense_supersized_slide_counter",
		"slide_captions" => "intense_supersized_slide_captions",
		"photosource_full_size" => "intense_photosource_full_size",
		"flickr_consumer_key" => "intense_flickr_consumer_key",
		"smug_api_key" => "intense_smug_api_key",
		"zenfolio_app_name" => "intense_zenfolio_app_name",
		"instagram_client_id" => "intense_instagram_client_id",
		"instagram_client_secret" => "intense_instagram_client_secret",
		"instagram_access_token" => "intense_instagram_access_token",
		"facebook_client_id" => "intense_facebook_client_id",
		"facebook_client_secret" => "intense_facebook_client_secret",
		"facebook_access_token" => "intense_facebook_access_token",
		"google_client_id" => "intense_google_client_id",
		"google_client_secret" => "intense_google_client_secret",
		"google_access_token" => "intense_google_access_token",
		"deviantART_client_id" => "intense_deviantART_client_id",
		"deviantART_client_secret" => "intense_deviantART_client_secret",
		"deviantART_access_token" => "intense_deviantART_access_token",
		"px500_consumer_key" => "intense_px500_consumer_key",
		"px500_consumer_secret" => "intense_px500_consumer_secret",
		"px500_auth_token" => "intense_px500_auth_token",
		"px500_auth_token_secret" => "intense_px500_auth_token_secret",
		"cptfaq" => "intense_cpt_faq",
		"cptportfolio" => "intense_cpt_portfolio",
		"cptproject" => "intense_cpt_project",
		"intense_content_section_box_class" => "intense_content_section_box_class",
		"intense_content_section_breakout" => "intense_content_section_breakout",
		//"intense_content_section_top_margin" => "intense_content_section_default_margin",
		//"intense_content_section_bottom_margin" => "intense_content_section_default_margin",
		//"intense_content_section_right_margin" => "intense_content_section_default_margin",
		//"intense_content_section_left_margin" => "intense_content_section_default_margin",
		//"intense_content_section_top_padding" => "intense_content_section_default_padding",
		//"intense_content_section_bottom_padding" => "intense_content_section_default_padding",
		//"intense_content_section_right_padding" => "intense_content_section_default_padding",
		//"intense_content_section_left_padding" => "intense_content_section_default_padding",
		//"intense_layout_row_top_margin" => "intense_layout_row_default_margin",
		//"intense_layout_row_bottom_margin" => "intense_layout_row_default_margin",
		//"intense_layout_row_top_padding" => "intense_layout_row_default_padding",
		//"intense_layout_row_bottom_padding" => "intense_layout_row_default_padding",
		//"intense_shortcode_css_image" => "intense_shortcode_css",
		//"intense_shortcode_css_mixitup" => "intense_shortcode_css",
		//"intense_shortcode_css_posts" => "intense_shortcode_css",
		//"intense_shortcode_css_shadow" => "intense_shortcode_css",
		//"intense_shortcode_css_alert" => "intense_shortcode_css",
		//"intense_shortcode_css_badge" => "intense_shortcode_css",
		//"intense_shortcode_css_blockquote" => "intense_shortcode_css",
		//"intense_shortcode_css_button" => "intense_shortcode_css",
		//"intense_shortcode_css_collapsibles" => "intense_shortcode_css",
		//"intense_shortcode_css_content_box" => "intense_shortcode_css",
		//"intense_shortcode_css_content_section" => "intense_shortcode_css",
		//"intense_shortcode_css_definitions" => "intense_shortcode_css",
		//"intense_shortcode_css_dropcap" => "intense_shortcode_css",
		//"intense_shortcode_css_hover_box" => "intense_shortcode_css",
		//"intense_shortcode_css_hr" => "intense_shortcode_css",
		//"intense_shortcode_css_icon" => "intense_shortcode_css",
		//"intense_shortcode_css_label" => "intense_shortcode_css",
		//"intense_shortcode_css_layout" => "intense_shortcode_css",
		//"intense_shortcode_css_lead" => "intense_shortcode_css",
		//"intense_shortcode_css_person" => "intense_shortcode_css",
		//"intense_shortcode_css_progress" => "intense_shortcode_css",
		//"intense_shortcode_css_promo" => "intense_shortcode_css",
		//"intense_shortcode_css_slider" => "intense_shortcode_css",
		//"intense_shortcode_css_table" => "intense_shortcode_css",
		//"intense_shortcode_css_tabs" => "intense_shortcode_css",
		//"intense_shortcode_css_testimony" => "intense_shortcode_css",
		//"intense_shortcode_css_timeline" => "intense_shortcode_css",
		//"intense_shortcode_css_tooltip" => "intense_shortcode_css",
	);

	$short_code_css_options = array(
		"intense_shortcode_css_image" => "intense_shortcode_css",
		"intense_shortcode_css_mixitup" => "intense_shortcode_css",
		"intense_shortcode_css_posts" => "intense_shortcode_css",
		"intense_shortcode_css_shadow" => "intense_shortcode_css",
		"intense_shortcode_css_alert" => "intense_shortcode_css",
		"intense_shortcode_css_badge" => "intense_shortcode_css",
		"intense_shortcode_css_blockquote" => "intense_shortcode_css",
		"intense_shortcode_css_button" => "intense_shortcode_css",
		"intense_shortcode_css_collapsibles" => "intense_shortcode_css",
		"intense_shortcode_css_content_box" => "intense_shortcode_css",
		"intense_shortcode_css_content_section" => "intense_shortcode_css",
		"intense_shortcode_css_definitions" => "intense_shortcode_css",
		"intense_shortcode_css_dropcap" => "intense_shortcode_css",
		"intense_shortcode_css_hover_box" => "intense_shortcode_css",
		"intense_shortcode_css_hr" => "intense_shortcode_css",
		"intense_shortcode_css_icon" => "intense_shortcode_css",
		"intense_shortcode_css_label" => "intense_shortcode_css",
		"intense_shortcode_css_layout" => "intense_shortcode_css",
		"intense_shortcode_css_lead" => "intense_shortcode_css",
		"intense_shortcode_css_person" => "intense_shortcode_css",
		"intense_shortcode_css_progress" => "intense_shortcode_css",
		"intense_shortcode_css_promo" => "intense_shortcode_css",
		"intense_shortcode_css_slider" => "intense_shortcode_css",
		"intense_shortcode_css_table" => "intense_shortcode_css",
		"intense_shortcode_css_tabs" => "intense_shortcode_css",
		"intense_shortcode_css_testimony" => "intense_shortcode_css",
		"intense_shortcode_css_timeline" => "intense_shortcode_css",
		"intense_shortcode_css_tooltip" => "intense_shortcode_css",
	);

	$old_values = get_theme_mods( );

	foreach ( $intense_easy_migrate_keys as $old_key => $new_key ) {
		$intense_visions_options[ $new_key ] = $old_values[ $old_key ];
	}

	$old_content_box_style = $old_values['intense_content_box_style'];
	$intense_visions_options['intense_content_box_style']['border-top'] = $old_content_box_style['width'] . "px";
	$intense_visions_options['intense_content_box_style']['border-right'] = $old_content_box_style['width'] . "px";
	$intense_visions_options['intense_content_box_style']['border-bottom'] = $old_content_box_style['width'] . "px";
	$intense_visions_options['intense_content_box_style']['border-left'] = $old_content_box_style['width'] . "px";
	$intense_visions_options['intense_content_box_style']['border-style'] = $old_content_box_style['style'];
	$intense_visions_options['intense_content_box_style']['border-color'] = $old_content_box_style['color'];

	$intense_visions_options['intense_missing_image']['url'] = $old_values['intense_missing_image'];

	$intense_visions_options['intense_content_section_default_margin'] = array();
	$intense_visions_options['intense_content_section_default_margin']['margin-top'] = $old_values['intense_content_section_top_margin'] . "px";
	$intense_visions_options['intense_content_section_default_margin']['margin-right'] = $old_values['intense_content_section_right_margin'] . "px";
	$intense_visions_options['intense_content_section_default_margin']['margin-bottom'] = $old_values['intense_content_section_bottom_margin'] . "px";
	$intense_visions_options['intense_content_section_default_margin']['margin-left'] = $old_values['intense_content_section_left_margin'] . "px";

	$intense_visions_options['intense_content_section_default_padding']['padding-top'] = $old_values['intense_content_section_top_padding'] . "px";
	$intense_visions_options['intense_content_section_default_padding']['padding-right'] = $old_values['intense_content_section_right_padding'] . "px";
	$intense_visions_options['intense_content_section_default_padding']['padding-bottom'] = $old_values['intense_content_section_bottom_padding'] . "px";
	$intense_visions_options['intense_content_section_default_padding']['padding-left'] = $old_values['intense_content_section_left_padding'] . "px";

	$intense_visions_options['intense_layout_row_default_margin'] = array();
	$intense_visions_options['intense_layout_row_default_margin']['margin-top'] = $old_values['intense_layout_row_top_margin'] . "px";
	//$intense_visions_options['intense_layout_row_default_margin']['padding-right'] = $old_values['intense_layout_row_right_margin'] . "px";
	$intense_visions_options['intense_layout_row_default_margin']['margin-bottom'] = $old_values['intense_layout_row_bottom_margin'] . "px";
	//$intense_visions_options['intense_layout_row_default_margin']['padding-left'] = $old_values['intense_layout_row_left_margin'] . "px";

	$intense_visions_options['intense_layout_row_default_padding']['padding-top'] = $old_values['intense_layout_row_top_padding'] . "px";
	//$intense_visions_options['intense_layout_row_default_padding']['padding-right'] = $old_values['intense_layout_row_right_padding'] . "px";
	$intense_visions_options['intense_layout_row_default_padding']['padding-bottom'] = $old_values['intense_layout_row_bottom_padding'] . "px";
	//$intense_visions_options['intense_layout_row_default_padding']['padding-left'] = $old_values['intense_layout_row_left_padding'] . "px";

	$size_string = $old_values['intense_custom_image_sizes'];
	$lines = preg_split( '/[\n\r]+/', $size_string );

	foreach ( $lines as $index => $line ) {
		$line = trim( $line );
		$line = str_replace( " ", "", $line );

		if ( $line != '' ) {
			$intense_visions_options['intense_custom_image_sizes'][] = $line;
		}
	}

	foreach ( $short_code_css_options as $old_key => $new_key ) {
		$intense_visions_options[ $new_key ][ str_replace( "intense_shortcode_css_", "", $old_key ) ] = $old_values[ $old_key ];
	}

	//$ReduxFramework->set_options( $intense_visions_options );
	
	foreach( $all_old_keys as $key ) {
	 	 remove_theme_mod( $key );
	//	unset( $old_values[ $key ] );
	}

	if ( !isset( $args['database'] )) $args['database'] = null;

	if ( $args['database'] === 'transient' ) {
	 set_transient( $args['opt_name'] . '-transient', $intense_visions_options, $args['transient_time'] );
	} else if ( $args['database'] === 'theme_mods' ) {
	 set_theme_mod( $args['opt_name'] . '-mods', $intense_visions_options );
	} else if ( $args['database'] === 'theme_mods_expanded' ) {
	 foreach ( $intense_visions_options as $k=>$v ) {
	  set_theme_mod( $k, $v );
	 }
	} else {
	 update_option( $args['opt_name'], $intense_visions_options );
	}	

	//echo '<div style="background: #efefef;"><div style="width: 45%; float: left"><pre>'; print_r( $old_values ); echo '</pre></div>';
	//echo '<div style="width: 45%; float: left; background:#fff;"><pre>'; print_r( $intense_visions_options ); echo '</pre></div><div style="clear: both;></div></div>';
}
