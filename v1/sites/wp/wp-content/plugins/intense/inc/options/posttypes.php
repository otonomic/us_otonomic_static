<?php

$this->sections['Custom_Post_Types'] = array(
	'title' => __( 'Custom Post Types', 'intense' ),
	//'desc' => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'intense' ),
	'icon' => 'el-icon-pencil',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_cpt_books',
			'type' => 'switch',
			"title"   => __( "Book", "intense" ),
			"desc"   => __( "Allow Book custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_clients',
			'type' => 'switch',
			"title"   => __( "Client", "intense" ),
			"desc"   => __( "Allow Client custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_coupons',
			'type' => 'switch',
			"title"   => __( "Coupon", "intense" ),
			"desc"   => __( "Allow Coupon custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_events',
			'type' => 'switch',
			"title"   => __( "Event", "intense" ),
			"desc"   => __( "Allow Event custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_faq',
			'type' => 'switch',
			'title' => __( "FAQ", "intense" ),
			'desc' => __( "Allow FAQ custom post type", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_cpt_jobs',
			'type' => 'switch',
			"title"   => __( "Job", "intense" ),
			"desc"   => __( "Allow Job custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_locations',
			'type' => 'switch',
			"title"   => __( "Location", "intense" ),
			"desc"   => __( "Allow Location custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_movies',
			'type' => 'switch',
			"title"   => __( "Movie", "intense" ),
			"desc"   => __( "Allow Movie custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_news',
			'type' => 'switch',
			"title"   => __( "News", "intense" ),
			"desc"   => __( "Allow News custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_portfolio',
			'type' => 'switch',
			'title' => __( "Portfolio", "intense" ),
			'desc' => __( "Allow Portfolio custom post type", "intense" ),
			'default' => 1,
		),
		// array(
		// 	'id'=>'intense_cpt_pricing',
		// 	'type' => 'switch',
		// 	"title"   => __( "Pricing Table", "intense" ),
		// 	"desc"   => __( "Allow Pricing Table custom post type", "intense" ),
		// 	'default' => 0,
		// ),
		array(
			'id'=>'intense_cpt_project',
			'type' => 'switch',
			"title"   => __( "Project", "intense" ),
			"desc"   => __( "Allow Project custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_quotes',
			'type' => 'switch',
			"title"   => __( "Quote", "intense" ),
			"desc"   => __( "Allow Quote custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_recipes',
			'type' => 'switch',
			"title"   => __( "Recipes", "intense" ),
			"desc"   => __( "Allow Recipes custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_snippets',
			'type' => 'switch',
			"title"   => __( "Snippets", "intense" ),
			"desc"   => __( "Allow Snippets custom post type", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_cpt_team',
			'type' => 'switch',
			"title"   => __( "Team", "intense" ),
			"desc"   => __( "Allow Team custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_templates',
			'type' => 'switch',
			"title"   => __( "Templates", "intense" ),
			"desc"   => __( "Allow Templates custom post type", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_cpt_testimonials',
			'type' => 'switch',
			"title"   => __( "Testimonials", "intense" ),
			"desc"   => __( "Allow Testimonial custom post type", "intense" ),
			'default' => 0,
		),

		array(
			'id'=>'intense_cpt_single_post_templates',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Single Post Templates', 'intense' ),
		),
		array(
			'id'=>'intense_cpt_books_single',
			'type' => 'select',
			"title"   => __( "Book", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_books/single/'), intense_locate_single_cpt_templates( 'books', true ) )
		),
		array(
			'id'=>'intense_cpt_clients_single',
			'type' => 'select',
			"title"   => __( "Client", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_clients/single/'), intense_locate_single_cpt_templates( 'clients', true ) )
		),
		array(
			'id'=>'intense_cpt_coupons_single',
			'type' => 'select',
			"title"   => __( "Coupon", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_coupons/single/'), intense_locate_single_cpt_templates( 'coupons', true ) )
		),
		array(
			'id'=>'intense_cpt_events_single',
			'type' => 'select',
			"title"   => __( "Event", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_events/single/'), intense_locate_single_cpt_templates( 'events', true ) )
		),
		array(
			'id'=>'intense_cpt_faq_single',
			'type' => 'select',
			'title' => __( "FAQ", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_faq/single/') )
		),
		array(
			'id'=>'intense_cpt_jobs_single',
			'type' => 'select',
			"title"   => __( "Job", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_jobs/single/'), intense_locate_single_cpt_templates( 'jobs', true ) )
		),
		array(
			'id'=>'intense_cpt_locations_single',
			'type' => 'select',
			"title"   => __( "Location", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_locations/single/'), intense_locate_single_cpt_templates( 'locations', true ) )
		),
		array(
			'id'=>'intense_cpt_movies_single',
			'type' => 'select',
			"title"   => __( "Movie", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_movies/single/'), intense_locate_single_cpt_templates( 'movies', true ) )
		),
		array(
			'id'=>'intense_cpt_news_single',
			'type' => 'select',
			"title"   => __( "News", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_news/single/'), intense_locate_single_cpt_templates( 'news', true ) )
		),
		array(
			'id'=>'intense_cpt_portfolio_single',
			'type' => 'select',
			'title' => __( "Portfolio", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_portfolio/single/'), intense_locate_single_cpt_templates( 'portfolio', true ) )
		),
		array(
			'id'=>'intense_cpt_post_single',
			'type' => 'select',
			"title"   => __( "Post", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/post/single/'), intense_locate_single_cpt_templates( 'post', true ) )
		),
		// array(
		// 	'id'=>'intense_cpt_pricing_single',
		// 	'type' => 'select',
		// 	"title"   => __( "Pricing Table", "intense" ),
		// 	'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_pricing/single/') )
		// ),
		array(
			'id'=>'intense_cpt_project_single',
			'type' => 'select',
			"title"   => __( "Project", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_project/single/'), intense_locate_single_cpt_templates( 'project', true ) )
		),
		array(
			'id'=>'intense_cpt_quotes_single',
			'type' => 'select',
			"title"   => __( "Quote", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_quotes/single/'), intense_locate_single_cpt_templates( 'quotes', true ) )
		),
		array(
			'id'=>'intense_cpt_recipes_single',
			'type' => 'select',
			"title"   => __( "Recipes", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_recipes/single/'), intense_locate_single_cpt_templates( 'recipes', true ) )
		),
		array(
			'id'=>'intense_cpt_team_single',
			'type' => 'select',
			"title"   => __( "Team", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_team/single/'), intense_locate_single_cpt_templates( 'team', true ) )
		),
		array(
			'id'=>'intense_cpt_testimonials_single',
			'type' => 'select',
			"title"   => __( "Testimonials", "intense" ),
			'options' => array_merge( array( '' => '' ), intense_locate_available_plugin_templates('/custom-post/intense_testimonials/single/'), intense_locate_single_cpt_templates( 'testimonials', true ) )
		),
		array(
			'id'=>'intense_cpt_post_names',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Custom Post Names', 'intense' ),
		),
		array(
			'id'=>'intense_cpt_books_singular',
			'type' => 'text',
			'title'   => __( 'Book Singular', "intense" ),
			'default' => __( 'Book', "intense" ),
		),
		array(
			'id'=>'intense_cpt_books_plural',
			'type' => 'text',
			'title'   => __( 'Book Plural', "intense" ),
			'default' => __( 'Books', "intense" ),
		),
		array(
			'id'=>'intense_cpt_clients_singular',
			'type' => 'text',
			'title'   => __( 'Client Singular', "intense" ),
			'default' => __( 'Client', "intense" ),
		),
		array(
			'id'=>'intense_cpt_clients_plural',
			'type' => 'text',
			'title'   => __( 'Client Plural', "intense" ),
			'default' => __( 'Clients', "intense" ),
		),
		array(
			'id'=>'intense_cpt_coupons_singular',
			'type' => 'text',
			'title'   => __( 'Coupon Singular', "intense" ),
			'default' => __( 'Coupon', "intense" ),
		),
		array(
			'id'=>'intense_cpt_coupons_plural',
			'type' => 'text',
			'title'   => __( 'Coupon Plural', "intense" ),
			'default' => __( 'Coupons', "intense" ),
		),
		array(
			'id'=>'intense_cpt_events_singular',
			'type' => 'text',
			'title'   => __( 'Event Singular', "intense" ),
			'default' => __( 'Event', "intense" ),
		),
		array(
			'id'=>'intense_cpt_events_plural',
			'type' => 'text',
			'title'   => __( 'Event Plural', "intense" ),
			'default' => __( 'Events', "intense" ),
		),
		array(
			'id'=>'intense_cpt_faq_singular',
			'type' => 'text',
			'title'   => __( 'FAQ Singular', "intense" ),
			'default' => __( 'FAQ', "intense" ),
		),
		array(
			'id'=>'intense_cpt_faq_plural',
			'type' => 'text',
			'title'   => __( 'FAQ Plural', "intense" ),
			'default' => __( 'FAQs', "intense" ),
		),
		array(
			'id'=>'intense_cpt_jobs_singular',
			'type' => 'text',
			'title'   => __( 'Job Singular', "intense" ),
			'default' => __( 'Job', "intense" ),
		),
		array(
			'id'=>'intense_cpt_jobs_plural',
			'type' => 'text',
			'title'   => __( 'Job Plural', "intense" ),
			'default' => __( 'Jobs', "intense" ),
		),
		array(
			'id'=>'intense_cpt_locations_singular',
			'type' => 'text',
			'title'   => __( 'Location Singular', "intense" ),
			'default' => __( 'Location', "intense" ),
		),
		array(
			'id'=>'intense_cpt_locations_plural',
			'type' => 'text',
			'title'   => __( 'Location Plural', "intense" ),
			'default' => __( 'Locations', "intense" ),
		),
		array(
			'id'=>'intense_cpt_movies_singular',
			'type' => 'text',
			'title'   => __( 'Movie Singular', "intense" ),
			'default' => __( 'Movie', "intense" ),
		),
		array(
			'id'=>'intense_cpt_movies_plural',
			'type' => 'text',
			'title'   => __( 'Movie Plural', "intense" ),
			'default' => __( 'Movies', "intense" ),
		),
		array(
			'id'=>'intense_cpt_news_singular',
			'type' => 'text',
			'title'   => __( 'News Singular', "intense" ),
			'default' => __( 'News', "intense" ),
		),
		array(
			'id'=>'intense_cpt_news_plural',
			'type' => 'text',
			'title'   => __( 'News Plural', "intense" ),
			'default' => __( 'News', "intense" ),
		),
		array(
			'id'=>'intense_cpt_portfolio_singular',
			'type' => 'text',
			'title'   => __( 'Portfolio Singular', "intense" ),
			'default' => __( 'Portfolio', "intense" ),
		),
		array(
			'id'=>'intense_cpt_portfolio_plural',
			'type' => 'text',
			'title'   => __( 'Portfolio Plural', "intense" ),
			'default' => __( 'Portfolios', "intense" ),
		),
		array(
			'id'=>'intense_cpt_project_singular',
			'type' => 'text',
			'title'   => __( 'Project Singular', "intense" ),
			'default' => __( 'Project', "intense" ),
		),
		array(
			'id'=>'intense_cpt_project_plural',
			'type' => 'text',
			'title'   => __( 'Project Plural', "intense" ),
			'default' => __( 'Projects', "intense" ),
		),
		array(
			'id'=>'intense_cpt_quotes_singular',
			'type' => 'text',
			'title'   => __( 'Quote Singular', "intense" ),
			'default' => __( 'Quote', "intense" ),
		),
		array(
			'id'=>'intense_cpt_quotes_plural',
			'type' => 'text',
			'title'   => __( 'Quote Plural', "intense" ),
			'default' => __( 'Quotes', "intense" ),
		),
		array(
			'id'=>'intense_cpt_recipes_singular',
			'type' => 'text',
			'title'   => __( 'Recipe Singular', "intense" ),
			'default' => __( 'Recipe', "intense" ),
		),
		array(
			'id'=>'intense_cpt_recipes_plural',
			'type' => 'text',
			'title'   => __( 'Recipe Plural', "intense" ),
			'default' => __( 'Recipes', "intense" ),
		),
		array(
			'id'=>'intense_cpt_team_singular',
			'type' => 'text',
			'title'   => __( 'Team Singular', "intense" ),
			'default' => __( 'Member', "intense" ),
		),
		array(
			'id'=>'intense_cpt_team_plural',
			'type' => 'text',
			'title'   => __( 'Team Plural', "intense" ),
			'default' => __( 'Team', "intense" ),
		),
		array(
			'id'=>'intense_cpt_testimonials_singular',
			'type' => 'text',
			'title'   => __( 'Testimonials Singular', "intense" ),
			'default' => __( 'Testimonial', "intense" ),
		),
		array(
			'id'=>'intense_cpt_testimonials_plural',
			'type' => 'text',
			'title'   => __( 'Testimonials Plural', "intense" ),
			'default' => __( 'Testimonials', "intense" ),
		),
	)
);
