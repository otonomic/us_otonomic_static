<?php

$this->sections['Gallery'] = array(
	'title' => __( 'Gallery', 'intense' ),
	'desc' => __( '<p class="description">Settings that apply to the Gallery shortcode.</p>', 'intense' ),
	'icon' => 'el-icon-picture',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_colorbox',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Colorbox', 'intense' ),
		),
		array(
			'id'=>'intense_colorbox_theme',
			'type' => 'select',
			'title' => __( 'Theme', 'intense' ),
			'options' => array(
				"1" => __( "Theme 1", "intense" ),
				"2" => __( "Theme 2", "intense" ),
				"3" => __( "Theme 3", "intense" ),
				"4" => __( "Theme 4", "intense" ),
				"5" => __( "Theme 5", "intense" )
			),
			'default' => '1',
		),
		array(
			'id'=>'intense_colorbox_transition',
			'type' => 'select',
			'title' => __( 'Transition', 'intense' ),
			'subtitle' => __( "Transition between images", "intense" ),
			'options' => array(
				"elastic" => __( "elastic", "intense" ),
				"fade"    => __( "fade", "intense" ),
				"none"    => __( "none", "intense" )
			),
			'default' => 'elastic',
		),
		array(
			'id'=>'intense_colorbox_slideshow',
			'type' => 'switch',
			'title' => __( 'Slideshow', 'intense' ),
			'subtitle' => __( "Play images in a slideshow", "intense" ),
			'default' => 1,
		),

		array(
			'id'=>'intense_pretty_photo',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( "Pretty Photo", "intense" ),
		),
		array(
			'id'=>'intense_prettyphoto_theme',
			'type' => 'select',
			'title' => __( 'Theme', 'intense' ),
			'options' => array(
				"pp_default" => __( "Default", "intense" ),
				"light_rounded" => __( "Light Rounded", "intense" ),
				"dark_rounded" => __( "Dark Rounded", "intense" ),
				"light_square" => __( "Light Square", "intense" ),
				"dark_square" => __( "Dark Square", "intense" ),
				"facebook" => __( "Facebook", "intense" )
			),
			'default' => 'pp_default',
		),
		array(
			'id'=>'intense_prettyphoto_social',
			'type' => 'ace_editor',
			'mode' => 'html',
			'title' => __( 'Social HTML', 'intense' ),
			'subtitle' => __( 'Add social network HTML', 'intense' ),
			'default' => ''
		),
		array(
			'id'=>'intense_prettyphoto_slideshow',
			'type' => 'switch',
			'title' => __( 'Slideshow', 'intense' ),
			'subtitle' => __( "Play images in a slideshow", "intense" ),
			'default' => 1,
		),

		array(
			'id'=>'intense_galleria',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( "Galleria", "intense" ),
		),
		array(
			'id'=>'intense_galleria_slideshow',
			'type' => 'switch',
			'title' => __( 'Slideshow', 'intense' ),
			'subtitle' => __( "Play images in a slideshow", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_galleria_lightbox',
			'type' => 'switch',
			'title' => __( 'Lightbox', 'intense' ),
			'subtitle' => __( "Display a lightbox when the image is clicked", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_galleria_transition',
			'type' => 'select',
			'title' => __( 'Transition', 'intense' ),
			'subtitle' => __( "Transition between images", "intense" ),
			'options' => array(
				"fade" => __( "fade", "intense" ),
				"flash"    => __( "flash", "intense" ),
				"pulse"    => __( "pulse", "intense" ),
				"slide"    => __( "slide", "intense" ),
				"fadeslide"    => __( "fadeslide", "intense" )
			),
			'default' => 'fade',
		),

		array(
			'id'=>'intense_supersized',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( "Supersized", "intense" ),
		),
		array(
			'id'=>'intense_supersized_color',
			'type' => 'color',
			'title' => __( 'Control Color', 'intense' ),
			'subtitle' => __( 'Pick a control color', 'intense' ),
			'default' => '#000',
			'validate' => 'color',
		),
		array(
			'id'=>'intense_supersized_position',
			'type' => 'select',
			'title' => __( 'Controls Position', 'intense' ),
			'subtitle' => __( "Location of navigation controls", "intense" ),
			'options' => array(
				"Bottom Left" => __( "Bottom Left", "intense" ),
				"Top Left"    => __( "Top Left", "intense" ),
				"None" => __( "None", "intense" )
			),
			'default' => 'Bottom Left',
		),
		array(
			'id'=>'intense_supersized_slideshow',
			'type' => 'switch',
			'title' => __( 'Slideshow', 'intense' ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_autoplay',
			'type' => 'switch',
			'title' => __( 'Autoplay', 'intense' ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_slideshow_transition',
			'type' => 'select',
			'title' => __( 'Transition', 'intense' ),
			'subtitle' => __( "Transition between images", "intense" ),
			'options' => array(
				"0" => "None",
				"1" => "Fade",
				"2" => "Slide Top",
				"3" => "Slide Right",
				"4" => "Slide Bottom",
				"5" => "Slide Left",
				"6" => "Carousel Right",
				"7" => "Carousel Left" ),
			'default' => '1',
		),
		array(
			'id'=>'intense_supersized_thumbnail_navigation',
			'type' => 'switch',
			'title' => __( 'Thumbnail Navigation', 'intense' ),
			'default' => 0,
		),
		array(
			'id'=>'intense_supersized_thumbnail_links',
			'type' => 'switch',
			'title' => __( 'Thumbnail Links', 'intense' ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_progress_bar',
			'type' => 'switch',
			'title' => __( "Progress Bar", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_stop_loop',
			'type' => 'switch',
			'title' => __( "Stop Loop", "intense" ),
			'subtitle' => __( "Pauses slideshow upon reaching the last slide", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_supersized_pause_hover',
			'type' => 'switch',
			'title' => __( "Pause on Hover", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_supersized_keyboard_nav',
			'type' => 'switch',
			'title' => __( "Keyboard Navigation", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_image_protect',
			'type' => 'switch',
			'title' => __( "Protect Image", "intense" ),
			'subtitle' => __( "Disables image dragging and right click with JavaScript", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_slideshow_navigation',
			'type' => 'switch',
			'title' => __( "Slideshow Controls", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_slide_counter',
			'type' => 'switch',
			'title' => __( "Slide Counter", "intense" ),
			'subtitle' => __( "Display slide numbers", "intense" ),
			'default' => 1,
		),
		array(
			'id'=>'intense_supersized_slide_captions',
			'type' => 'switch',
			'title' => __( "Slide Captions", "intense" ),
			'default' => 1,
		),
	)
);
