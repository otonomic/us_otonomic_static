<?php

require_once INTENSE_PLUGIN_FOLDER . '/inc/skins.php';

global $intense_skins;
$intense_skins = new Intense_Skins();

$this->sections['Skins'] = array(
	'title' => __( 'Skins', 'intense' ),
	'icon' => 'el-icon-list-alt',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_skin',
			'type' => 'text',
			'title' => __( "Skin", "intense" ),
			'subtitle' => __( "Skins set the default values for the different shortcodes.", "intense" ),
			'default' => 'default',
		),
		array(
			'id'        => 'opt-custom-callback',
			'type'      => 'callback',
			'title'     => __( '', 'intense' ),
			'callback'  => 'intense_skin_field'
		),		
	)
);

if ( !function_exists( 'intense_skin_field' ) ):
	function intense_skin_field( $field, $value ) {
		global $intense_skins;

		intense_add_style( 'intense.dialogs' );
		Intense_Shortcodes::enqueue_dialog_scripts();
?>
		<style>
			#intense-skin-list {
				margin-right: 15px;
			} 

			#intense-skin-edit {
				display: inline-block;
			} 

			#intense-skin-edit .button {
				margin-top: -3px;
				display: none;
			}

			#intense-skin-add, #intense-skin-export {
				display: none;
			}

			#intense-skin-add .intense-header, #intense-skin-export .intense-header {
				margin-top: 15px;
				margin-bottom: 15px;
			}

			#intense-skin-add label, #intense-skin-export label {
				font-weight: 700;
				font-size: 14px;
				width: 30%;
				display: inline-block;
			}

			#intense-skin-add input, #intense-skin-export input {
				width: auto;
				display: inline-block;
			}

			#intense-skin-add textarea, #intense-skin-export textarea {
				width: 100%;
				height: 250px;
				margin-top: 10px;
			}

			#intense-skin-add hr {
				margin: 10px 0;
			}

			#intense-skin-builder .dashicons-before {
				display: inline-block;
				margin-left: 20px;
        	}

        	#intense-skin-builder .dashicons-before:before {
        		font-size: 15px;
				width: 15px;
				height: 15px;
				margin-top: 5px;
				-webkit-animation: 1.5s linear 0s normal none infinite spin;
				animation: 1.5s linear 0s normal none infinite spin;
        	}

        	@-moz-keyframes spin {
			    from { -moz-transform: rotate(0deg); }
			    to { -moz-transform: rotate(360deg); }
			}
			@-webkit-keyframes spin {
			    from { -webkit-transform: rotate(0deg); }
			    to { -webkit-transform: rotate(360deg); }
			}
			@keyframes spin {
			    from {transform:rotate(0deg);}
			    to {transform:rotate(360deg);}
			}

			#intense-skin-editor {
				margin-top: 20px;
			}

			.intense-skin-shortcodes {
				float: left;
				background: #F3F3F3;
				/*margin-left: -20px;*/
				color: #777;
				width: 20%;
			}

			.intense.skin-shortcode-list {
				padding: 0;
				margin: 0;
			}
			
			.intense.skin-shortcode-list i {
				font-size: 13px;
				padding-right: 10px;
			}

			.intense.skin-shortcode-group {
				background: #333;
				padding: 3px;
				color: #FFF;
				cursor: pointer;
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
				padding: 10px 4px 10px 14px;		
			}

			.intense.skin-shortcode-group:hover {
				background: #444;
			}

			.intense.skin-shortcode-list li {
				padding: 10px 4px 10px 14px;
				border-bottom: 1px solid #E7E7E7;
				border-right: 1px solid #E7E7E7;
				border-left: 1px solid #E7E7E7;
			}

			.intense.skin-shortcode-list li:hover {
				background: #E5E5E5;
				cursor: pointer;
			}

			.intense.skin-shortcode-list li.active {
				background: #FeFeFe;
				margin-right: -3px;
				border-right: none;
				color: #222;
			}

			#intense-skin-shortcode-container {
				float: left;
				width: 78%;
				padding: 20px;
				box-sizing: border-box;
				border: 1px solid #D8D8D8;
				border-left: none;
			}

			.intense-shortcode-title, .intense-snippet-title {
				margin: -20px -20px 20px -20px !important;
				padding: 10px !important;
			}

			#shortcode-form .nav-tabs > li {
				margin-bottom: -1px !important;
			}
		</style>
    	<script>
    		(function ($) {
    			var skins;
				var newSkin = 
					"{\n" + 
						"\t\"key\": \"<?php echo rand(); ?>\",\n" + 
						"\t\"name\": \"My Custom Skin\",\n" + 
						"\t\"defaults\": []\n" + 
					"}";		
    			var $container = $('<div id="intense-skin-builder"></div>');
    			var $defaultSkinDropdown = $('<select id="intense-default-skin-list">');
    			var $skinDropdown = $('<select id="intense-skin-list">');
    			var $skinEditButtons = $('<div id="intense-skin-edit"><button class="button customize"><?php _e('Customize', 'intense'); ?></button> <button class="button reset"><?php _e('Reset', 'intense'); ?></button> <button class="button remove"><?php _e('Remove', 'intense'); ?></button> <button class="button export"><?php _e('Export', 'intense'); ?></button> <button class="button add"><?php _e('Add/Import', 'intense'); ?></button></div>');
    			var $skinAdd = $('<div id="intense-skin-add"><div class="intense-header redux-field-info"><?php _e('Add New Skin', 'intense'); ?></div><?php _e('Change the key, name, and defaults. If the key matches a skin file key, a new skin will be created in the database and will take priority over the skin file. If the skin already exists in the database, the new skin will override the existing.<br><br>After you have saved, you can customize the defaults using the skin building tools.', 'intense'); ?><br><br><label><?php _e('JSON', 'intense'); ?></label><textarea id="intense-new-skin-json">' + newSkin + '</textarea><div class="intense-header redux-field-info"><button class="button button-primary add"><?php _e('Add', 'intense'); ?></button> <button class="button cancel"><?php _e('Cancel', 'intense'); ?></button></div></div>');
    			var $skinExport = $('<div id="intense-skin-export"><div class="intense-header redux-field-info"><?php _e('Export Skin', 'intense'); ?></div><label><?php _e('JSON', 'intense'); ?></label><textarea id="intense-skin-json"></textarea><?php _e('When saving the new skin file, make sure to name the file the same as the key. Skin files can be saved within a theme or child theme inside an <strong>intense_skins</strong> folder.', 'intense'); ?><div class="intense-header redux-field-info"><button class="button cancel"><?php _e('Cancel', 'intense'); ?></button></div></div>');
    			var $skinEditor = $('<div id="intense-skin-editor"></div>');

    			$container.append('<?php _e('Default', 'intense'); ?> ');
    			$container.append($defaultSkinDropdown);
    			$container.append('<br><br><div class="intense-header redux-field-info"><?php _e('Skin Builder', 'intense'); ?></div><br>');
    			$container.append('<?php _e('Skin', 'intense'); ?> ');
    			$container.append($skinDropdown);
    			$container.append($skinEditButtons);
    			$container.append($skinAdd);
    			$container.append($skinExport);
    			$container.append($skinEditor);

    			$skinAdd.find('.add').click(intenseAddSkin);
    			$skinAdd.find('.cancel').click(intenseCancelAdd);

    			$skinExport.find('.cancel').click(intenseCancelExport);

    			$('#Skins_section_group .form-table').hide();
    			$('#Skins_section_group').append($container);

    			var saveXHR = null;

    			//override refresh and save the skin instead
    			intenseRefreshPreview = function() {
    				var $ = jQuery.noConflict();	    		
		    		var shortcode = intenseRenderShortcode();
		    		var key = $skinDropdown.val();

					if (saveXHR) {
						saveXHR.abort();
					}

					saveXHR = $.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_save_shortcode_action',
						data: {							
							'key': key,
							'shortcode': shortcode
						},
						beforeSend: function() {							
							intenseStartAction();
						},
						success: function(data) {
							//$skinEditor.html(data);
							intenseEndAction();
							//$('.intense.skin-shortcode-list li').click(intenseEditShortcode);
						},
						dataType: 'JSON'
					});
    			}

    			function intenseGetSkins( callback ) {
    				$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_list_action',
						beforeSend: function() {

						},
						success: function(data) {
							skins = data;
							callback( skins );
						},
						dataType: 'JSON'
					});
    			}

    			function intenseRenderSkinDropdown() {
    				$skinDropdown.html('');

    				$skinDropdown.append('<option value="" data-can-edit="blank"><?php _e('Select a skin...', 'intense'); ?></option>');

					$.each( skins, function(i, skin) {
						$skinDropdown.append('<option value="' + skin.key + '" data-can-edit="' + skin.can_edit + '">' + skin.name + ' (' + skin.source_location + ')</option>');
					});

					$skinDropdown.val($('#intense_skin-text').val());

					$skinDropdown.unbind('change');
					$skinDropdown.change(function() {
						intenseSetSkinButtonVisibility();
					});

					intenseSetSkinButtonVisibility();
    			}

    			function intenseRenderDefaultSkinDropdown() {
    				$defaultSkinDropdown.html('');

    				$defaultSkinDropdown.append('<option value="" data-can-edit="blank"><?php _e('Select a skin...', 'intense'); ?></option>');

					$.each( skins, function(i, skin) {
						$defaultSkinDropdown.append('<option value="' + skin.key + '" data-can-edit="' + skin.can_edit + '">' + skin.name + ' (' + skin.source_location + ')</option>');
					});

					$defaultSkinDropdown.val($('#intense_skin-text').val());

					$defaultSkinDropdown.unbind('change');
					$defaultSkinDropdown.change(function() {
						$('#intense_skin-text').val($defaultSkinDropdown.val());
						$('#intense_skin-text').change();
					});
    			}

    			function intenseSetSkinButtonVisibility() {
    				var $option = $skinDropdown.children('option:selected');

					$skinEditButtons.children('button').show();

					if ($option.data("can-edit") == true) {
						$skinEditButtons.children('.customize, .reset').hide();
						intenseEditSkin();
					} else if($option.data("can-edit") == false) {
						$skinEditButtons.children('.edit, .remove, .reset, .import').hide();
						intenseEditSkin();
					} else {
						$skinEditButtons.children(':not(".add")').hide();
					}    				
    			}

    			function intenseSetupSkinButtons() {
    				$skinEditButtons.children('.remove').click(intenseRemoveSkin);
    				$skinEditButtons.children('.reset').click(intenseRemoveSkin);
    				$skinEditButtons.children('.customize').click(intenseCustomizeSkin);
    				$skinEditButtons.children('.export').click(intenseExportSkin);
    				$skinEditButtons.children('.edit').click(intenseEditSkin);
    				$skinEditButtons.children('.add').click(function(e) {
    					$('#intense-skin-export').hide();
						$('#intense-skin-editor').stop().fadeOut();
    					$('#intense-skin-add').slideDown();    					
    					e.preventDefault();
    				});
    			}

				function intenseEditSkin(e) {
					var key = $skinDropdown.val();

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_edit_action',
						data: {							
							'key': key
						},
						beforeSend: function() {
							intenseStartAction();
							$('#intense-skin-editor').stop().fadeOut();
						},
						success: function(data) {
							$skinEditor.html(data);
							intenseEndAction();

							$('.intense.skin-shortcode-list li').click(intenseEditShortcode);							


							$('.skin-shortcode-group').click(function () {
								$(this).next().slideToggle();
							});

							$('#intense-skin-editor').stop().fadeIn();
						},
						dataType: 'html'
					});

					if (e) {
						e.preventDefault();
					}
				}

				function intenseEditShortcode(e) {
					var shortcode = $(this).data('shortcode');
					var key = $skinDropdown.val();
					var $element = $(this);

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_shortcode_dialog_action&shortcode=' + shortcode + '&skin=' + key + '&is_skin_dialog=true',
						beforeSend: function() {
							intenseStartAction();
						},
						success: function(data) {
							$('#intense-skin-shortcode-container').html(data);
							intenseEndAction();
							$('.intense.skin-shortcode-list li').removeClass('active');
							$element.addClass('active');
						},
						dataType: 'html'
					});

					if (e) {
						e.preventDefault();
					}
				}

				function intenseRemoveSkin(e) {
					var key = $skinDropdown.val();

					if ( key == $('#intense_skin-text').val() ) {
						$('#intense_skin-text').val('default');
					}

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_remove_action',
						data: {							
							'key': key
						},
						beforeSend: function() {
							intenseStartAction();
						},
						success: function(data) {
							$('#redux_save').click();
						},
						dataType: 'JSON'
					});

					e.preventDefault();
				}

				function intenseCustomizeSkin(e) {
					var key = $skinDropdown.val();

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_add_action',
						data: {
							'key': key
						},
						beforeSend: function() {
							intenseStartAction();
						},
						success: function(data) {
							$('#redux_save').click();
						},
						dataType: 'JSON'
					});

					e.preventDefault();
				}

				function intenseExportSkin(e) {
					var key = $skinDropdown.val();

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_export_action',
						data: {
							'key': key
						},
						beforeSend: function() {
							intenseStartAction();
							$('#intense-skin-editor').stop().fadeOut();
							$('#intense-skin-add').hide();
						},
						success: function(data) {
							$skinExport.slideDown();							
							$skinExport.find('textarea').val(JSON.stringify(data, null, '\t'));
							intenseEndAction();
						},
						dataType: 'JSON'
					});

					e.preventDefault();
				}

				function intenseAddSkin(e) {
					var key = $('#intense-new-skin-key').val();
					var name = $('#intense-new-skin-name').val();
					var json = $('#intense-new-skin-json').val();

					//$('#intense_skin-text').val(key);

					$.ajax({
						type: 'POST',
						cache: false,
						url: ajaxurl + '?action=intense_skins_add_action',
						data: {
							'key': key,
							'name': name,
							'json': json
						},
						beforeSend: function() {
							intenseStartAction();							
						},
						success: function(data) {
							$('#redux_save').click();
						},
						dataType: 'JSON'
					});	

					e.preventDefault();				
				}

				function intenseCancelAdd(e) {
					$('#intense-skin-add').hide();
					e.preventDefault();
					$('#intense-skin-editor').stop().fadeIn();
				}

				function intenseCancelExport(e) {
					$('#intense-skin-export').hide();
					e.preventDefault();
					$('#intense-skin-editor').stop().fadeIn();
				}

				function intenseStartAction() {
					intenseEndAction();
					$skinEditButtons.append('<div class="progress dashicons-before dashicons-update">');
				}

				function intenseEndAction() {
					$('.dashicons-update').remove();
				}

				function intenseInitSkins() {
					intenseGetSkins(function() {
						intenseRenderSkinDropdown();
						intenseRenderDefaultSkinDropdown();						
					});
				}

				intenseInitSkins();
				intenseSetupSkinButtons();			
    		})(jQuery);
    	</script>
<?php
	}
endif;
