<?php
require_once INTENSE_PLUGIN_FOLDER . '/inc/OAuth/Intense-OAuth1.php';
require_once INTENSE_PLUGIN_FOLDER . '/inc/OAuth/Intense-OAuth2.php';

if ( !class_exists( 'ReduxFramework' ) && file_exists( INTENSE_PLUGIN_FOLDER . '/inc/plugins/ReduxFramework/ReduxCore/framework.php' ) ) {
	require_once INTENSE_PLUGIN_FOLDER . '/inc/plugins/ReduxFramework/ReduxCore/framework.php';
}

if ( !class_exists( "ReduxFramework" ) ) {
	return;
}

if ( !class_exists( "Intense_Redux_Framework_Config" ) ) {

	class Intense_Redux_Framework_Config {
		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;
		public $instagram_OAuth;
		public $deviantART_OAuth;
		public $facebook_OAuth;
		public $google_OAuth;
		public $px500_OAuth;
		public $flickr_OAuth;

		public function __construct( ) {
			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if ( !isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/plugin/hooks', array( $this, 'remove_demo' ) );

			// Function to test the compiler hook and demo CSS output.
			//add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.

			// Change the arguments after they've been declared, but before the panel is created
			//add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

			// Change the default value of a field after it's been set, but before it's been used
			//add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			add_filter( 'redux/options/'.$this->args['opt_name'].'/sections', array( $this, 'dynamic_section' ) );

			// Add custom CSS for the options
			add_action( 'redux/page/'.$this->args['opt_name'].'/enqueue', array( $this, 'addPanelCSS' ) );

			// Add custom CSS for the options
			add_action( 'redux/options/'.$this->args['opt_name'].'/settings/change', array( $this, 'options_settings_change' ) );

			$this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}

		public function addPanelCSS() {
			$minified = ( INTENSE_DEBUG ? "" : "min." );

			wp_register_style(
				'intense-redux-custom-css',
				INTENSE_PLUGIN_URL . '/assets/css/intense/options.' . $minified . 'css',
				array( 'redux-css' ), // Be sure to include redux-css so it's appended after the core css is applied
				time(),
				'all'
			);
			wp_enqueue_style( 'intense-redux-custom-css' );
		}

		public function options_settings_change() {
			flush_rewrite_rules(); //Resets the permalinks
		}

		public function setArguments() {
			$this->args = array(
				'dev_mode' => false,
				'dev_mode_icon_class' => 'icon-large',
				'opt_name' => 'intense_visions_options',
				'output_tag' => true,
				'display_name' => "Intense WordPress Plugin by Intense Visions",
				'display_version' => 'v' . INTENSE_PLUGIN_VERSION,
				'google_api_key' => 'AIzaSyBZYcKMU9hkCC9iBnDj3kdcX6c9E571vWw',
				'import_icon_class' => 'icon-large',
				'default_icon_class' => 'icon-large',
				'menu_type'  => 'submenu',
				'page_parent'        => 'options-general.php',
				'menu_title' => __( 'Intense', 'intense' ),
				'page_title' => __( 'Intense Options', 'intense' ),
				'page_slug' => 'intense_visions_options',
				'default_show' => true,
				'default_mark' => '*',
				'allow_sub_menu' => true,
				'menu_icon'=>'dashicons-intense-intense-logo',
				'admin_bar' => false,
				'share_icons' => array(
					'twitter' => array(
						'link' => 'https://twitter.com/IntenseVisions',
						'title' => 'Follow Intense Visions on Twitter',
						'icon'  => 'el-icon-twitter'
					),
					'facebook' => array(
						'link' => 'https://www.facebook.com/intensevision',
						'title' => 'Like Intense Visions on Facebook',
						'icon'  => 'el-icon-facebook'
					),
					'google' => array(
						'link' => 'https://plus.google.com/b/100661863245884531948/100661863245884531948/',
						'title' => '+1 Intense Visions on Google+',
						'icon'  => 'el-icon-googleplus'
					)
				),
			);
		}

		public function setHelpTabs() {

		}

		public function setSections() {
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/shortcodes.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/skins.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/posttypes.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/color.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/extras.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/license.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/gallery.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/social.php';
		}

		public function setOAuth() {
			global $intense_visions_options;

			$this->instagram_OAuth = new Intense_OAuth2(
				"instagram",
				"https://api.instagram.com/oauth/authorize",
				"https://api.instagram.com/oauth/access_token",
				null
			);

			// sizes?: 400,600,800,900,1024,1280,1600,original
			$this->deviantART_OAuth = new Intense_OAuth2(
				"deviantART",
				"https://www.deviantart.com/oauth2/draft15/authorize",
				"https://www.deviantart.com/oauth2/draft15/token",
				null
			);

			$this->facebook_OAuth = new Intense_OAuth2(
				"facebook",
				"https://www.facebook.com/dialog/oauth",
				"https://graph.facebook.com/oauth/access_token",
				"user_photos,user_groups"
			);

			$this->google_OAuth = new Intense_OAuth2(
				"google",
				"https://accounts.google.com/o/oauth2/auth",
				"https://accounts.google.com/o/oauth2/token",
				"https://picasaweb.google.com/data/"
			);

			$this->px500_OAuth = new Intense_OAuth1(
				"px500",
				"https://api.500px.com/v1/oauth/request_token",
				"https://api.500px.com/v1/oauth/authorize",
				"https://api.500px.com/v1/oauth/access_token",
				null
			);

			$this->flickr_OAuth = new Intense_OAuth1(
				"flickr",
				"http://www.flickr.com/services/oauth/request_token",
				"http://www.flickr.com/services/oauth/authorize",
				"http://www.flickr.com/services/oauth/access_token",
				"&perms=read"
			);

			$this->instagram_OAuth->get_access_token();
			$this->instagram_OAuth->get_access_token();
			$this->deviantART_OAuth->get_access_token();
			$this->facebook_OAuth->get_access_token();
			$this->google_OAuth->get_access_token();
			$this->px500_OAuth->get_access_token();
			$this->flickr_OAuth->get_access_token();

			if ( isset( $_GET['page'] ) && $_GET['page'] == 'intense_visions_options' && ( isset(  $_GET['code'] ) || isset( $_GET['OAuth1'] ) ) ) {
				echo "Check your redirect URI setting";
				die( 1 );
			}
		}

		function dynamic_section( $sections ) {
			global $intense_visions_options;

			$this->setOAuth();

			if ( isset( $intense_visions_options['intense_custom_image_sizes'] ) && $intense_visions_options['intense_custom_image_sizes'] != '' ) {
				foreach ( $intense_visions_options['intense_custom_image_sizes'] as $key => $value ) {
					$sizes = explode( "|", $value );

					if ( !empty( $sizes[0] ) && !empty( $sizes[1] ) && !empty( $sizes[2] ) && !empty( $sizes[3] ) ) {
						add_image_size( $sizes[2], $sizes[0], $sizes[1], $sizes[3] );
					}
				}
			}

			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/oauth/OAuth-handler.php';
			require_once INTENSE_PLUGIN_FOLDER . '/inc/options/photosource.php';

			//manually add import/export section
			$sections[] = array(
                'title'     => __('Import / Export', 'intense'),
                'desc'      => __('Import and Export your settings from file, text or URL.', 'intense'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your options',
                        'full_width'    => false,
                    ),
                ),
            );

			return $sections;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
				remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_meta_demo_mode_link' ), null, 2 );
			}

			// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
			remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );

		}
	}

	new Intense_Redux_Framework_Config();
}
