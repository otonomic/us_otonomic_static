<?php

$this->sections['Colors'] = array(
	'title' => __( 'Colors', 'intense' ),
	'desc' => __( '<p class="description">Choose colors that are used throughout Intense Visions plugins and themes.</p>', 'intense' ),
	'icon' => 'el-icon-tint',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			"title"   => __( "Primary Color", "intense" ),			
			"id"   => "intense_main_color",
			"default"   => "#1a8be2",
			"type"   => "color"
		),
		array(
			"title"   => __( "Info Color", "intense" ),
			"id"   => "intense_info_color",
			"default"   => "#d6c000",
			"type"   => "color"
		),
		array(
			"title"   => __( "Success Color", "intense" ),
			"id"   => "intense_success_color",
			"default"   => "#40ba18",
			"type"   => "color"
		),
		array(
			"title"   => __( "Warning Color", "intense" ),
			"id"   => "intense_warning_color",
			"default"   => "#fe781e",
			"type"   => "color"
		),
		array(
			"title"   => __( "Error Color", "intense" ),
			"id"   => "intense_error_color",
			"default"   => "#ce0c1f",
			"type"   => "color"
		),
		array(
			"title"   => __( "Inverse Color", "intense" ),
			"id"   => "intense_inverse_color",
			"default"   => "#070707",
			"type"   => "color"
		),
		array(
			"title"   => __( "Boxed Element Color", "intense" ),
			"id"   => "intense_boxed_color",
			"default"   => "#e8e8e8",
			"type"   => "color"
		),
		array(
			"title"   => __( "HR Title Background Color", "intense" ),
			"id"   => "intense_title_background_color",
			"default"   => "#ffffff",
			"type"   => "color"
		),
		array(
			"title"   => __( "Icon and Marker Color", "intense" ),
			"subtitle"   => __( "Pick a color to be used for content box icons and map markers", "intense" ),
			"id"   => "intense_icon_color",
			"default"   => "#1a8be2",
			"type"   => "color"
		),		
	)
);
