<?php

$this->sections['Social'] = array(
	'title' => __( 'Social Sharing', 'intense' ),
	'icon' => 'el-icon-adult',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_social_email',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Email', 'intense' ),
		),
		array(
			'id'=>'intense_social_email_use',
			'type' => 'switch',
			"title"   => __( "Email icon", "intense" ),
			"desc"   => __( "Show Email icon on pages/posts.", "intense" ),
			'default' => 0,
		),
		
		array(
			'id'=>'intense_social_facebook',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Facebook', 'intense' ),
		),
		array(
			'id'=>'intense_social_facebook_use',
			'type' => 'switch',
			"title"   => __( "Facebook Like/Share", "intense" ),
			"desc"   => __( "Show Facebook like/share button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_facebook_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'button' => array( 'alt' => 'Button', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/facebook_button.jpg' ),
				'button_count' => array( 'alt' => 'Button Count', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/facebook_button_count.jpg' ),
				'standard' => array( 'alt' => 'Standard', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/facebook_standard.jpg' ),
				'box_count' => array( 'alt' => 'Box Count', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/facebook_box_count.jpg' ),
				'fontawesome' => array( 'alt' => 'Font Awesome', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/facebook_icon.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => 'button'
		),
		array(
			'id'=>'intense_social_facebook_faces',
			'type' => 'switch',
			"title"   => __( "Show Friends' Faces", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_facebook_share',
			'type' => 'switch',
			"title"   => __( "Show Share Button", "intense" ),
			'default' => 0,
		),


		array(
			'id'=>'intense_social_google_plus',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Google +', 'intense' ),
		),
		array(
			'id'=>'intense_social_google_plus_use',
			'type' => 'switch',
			"title"   => __( "Google+ +1 Button", "intense" ),
			"desc"   => __( "Show Google+ +1 button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_google_plus_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'medium' => array( 'alt' => 'Medium', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/google_plus_medium.jpg' ),
				'medium_bubble' => array( 'alt' => 'Medium Bubble', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/google_plus_medium_bubble.jpg' ),
				'medium_inline' => array( 'alt' => 'Medium Inline', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/google_plus_medium_inline.jpg' ),
				'tall_bubble' => array( 'alt' => 'Tall Bubble', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/google_plus_tall_bubble.jpg' ),
				'fontawesome' => array( 'alt' => 'Font Awesome', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/google_plus_icon.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => 'medium'
		),


		array(
			'id'=>'intense_social_linkedin',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'LinkedIn', 'intense' ),
		),
		array(
			'id'=>'intense_social_linkedin_use',
			'type' => 'switch',
			"title"   => __( "LinkedIn share button", "intense" ),
			"desc"   => __( "Show LinkedIn share button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_linkedin_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'none' => array( 'alt' => 'No Count', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/linkedin_no_count.jpg' ),
				'right' => array( 'alt' => 'Horizontal', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/linkedin_horizontal.jpg' ),
				'top' => array( 'alt' => 'Vertical', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/linkedin_vertical.jpg' ),
				'fontawesome' => array( 'alt' => 'Font Awesome', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/linkedin_icon.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => 'right'
		),


		array(
			'id'=>'intense_social_stumbleupon',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'StumbleUpon', 'intense' ),
		),
		array(
			'id'=>'intense_social_stumbleupon_use',
			'type' => 'switch',
			"title"   => __( "StumbleUpon submit button", "intense" ),
			"desc"   => __( "Show StumbleUpon submit button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_stumbleupon_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'4' => array( 'alt' => 'Layout 4', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/stumbleupon_layout4.jpg' ),
				'1' => array( 'alt' => 'Layout 1', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/stumbleupon_layout1.jpg' ),
				'3' => array( 'alt' => 'Layout 3', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/stumbleupon_layout3.jpg' ),
				'5' => array( 'alt' => 'Layout 5', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/stumbleupon_layout5.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => '1'
		),


		array(
			'id'=>'intense_social_twitter',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Twitter', 'intense' ),
		),
		array(
			'id'=>'intense_social_twitter_use',
			'type' => 'switch',
			"title"   => __( "Twitter like button", "intense" ),
			"desc"   => __( "Show Twitter like button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_twitter_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'none' => array( 'alt' => 'Regular', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/twitter_tweet.jpg' ),
				'horizontal' => array( 'alt' => 'Horizontal', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/twitter_tweet_horizontal.jpg' ),
				'vertical' => array( 'alt' => 'Vertical', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/twitter_tweet_vertical.jpg' ),
				'fontawesome' => array( 'alt' => 'Font Awesome', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/twitter_icon.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => 'tweet'
		),


		array(
			'id'=>'intense_social_pinterest',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Pinterest', 'intense' ),
		),
		array(
			'id'=>'intense_social_pinterest_use',
			'type' => 'switch',
			"title"   => __( "Pinterest Pin It", "intense" ),
			"desc"   => __( "Show Pinterest Pin It button on pages/posts.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_social_pinterest_layout',
			'type' => 'image_select',
			'title' => __( 'Button Style', 'intense' ),
			'desc' => __( 'Select the default button style.', 'intense' ),
			'options' => array(
				'none' => array( 'alt' => 'Regular', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/pinterest.jpg' ),
				'beside' => array( 'alt' => 'Count Right', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/pinterest_right.jpg' ),
				'above' => array( 'alt' => 'Count Top', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/pinterest_above.jpg' ),
				'fontawesome' => array( 'alt' => 'Font Awesome', 'img' => INTENSE_PLUGIN_URL . '/assets/img/social/pinterest_icon.jpg' )
			), //Must provide key => value(array:title|img) pairs for radio options
			'default' => 'none'
		),


		array(
			'id'=>'intense_social_reddit',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Reddit', 'intense' ),
		),
		array(
			'id'=>'intense_social_reddit_use',
			'type' => 'switch',
			"title"   => __( "Reddit share icon", "intense" ),
			"desc"   => __( "Show Reddit share icon on pages/posts.", "intense" ),
			'default' => 0,
		),


		array(
			'id'=>'intense_social_tumblr',
			'type' => 'info',
			'raw_html'=>true,
			'class' => 'intense-header',
			'desc' => __( 'Tumblr', 'intense' ),
		),
		array(
			'id'=>'intense_social_tumblr_use',
			'type' => 'switch',
			"title"   => __( "Tumblr share icon", "intense" ),
			"desc"   => __( "Show Tumblr share icon on pages/posts.", "intense" ),
			'default' => 0,
		),
	)
);

