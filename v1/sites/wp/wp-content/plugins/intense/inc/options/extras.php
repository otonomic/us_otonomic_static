<?php

$this->sections['Extras'] = array(
	'title' => __( 'Extras', 'intense' ),
	'desc' => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'intense' ),
	'icon' => 'el-icon-asterisk',
	'icon_class' => 'icon-large',
	// Leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id'=>'intense_missing_image',
			'type' => 'media',
			'url'=> true,
			'title' => __( "Missing Image", "intense" ),
			//'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
			'subtitle'=> __( "Upload an image to show when an image hasn't been set or is unavailable.", "intense" ),
			//'subtitle' => __( 'Upload any media using the WordPress native uploader', 'intense' ),
			'default'=>array( 'url'=> INTENSE_PLUGIN_URL . "/img/missing_image.png" ),
		),
		array(
			'id'=>'intense_html5_shiv',
			'type' => 'switch',
			'title' => __( "HTML5 Shiv", "intense" ),
			'desc' => __( "Include the HTML5 Shiv JavaScript library to add HTML5 support in Internet Explorer 8 and older. This may not be needed if the theme includes the HTML5 Shiv. For more information, see <a href='http://en.wikipedia.org/wiki/HTML5_Shiv'>HTML5 Shiv</a>", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_show_acf',
			'type' => 'switch',
			'title' => __( "ACF/Custom Fields Menu Item", "intense" ),
			'desc' => __( "Toggle this field if you want to turn on or off the ACF/Custom Fields menu item.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_hide_shortcodes_in_vc',
			'type' => 'switch',
			'title' => __( "Hide shortcodes in Visual Composer", "intense" ),
			'desc' => __( "Toggle this field if you want to hide Intense shortcodes in Visual Composer (ON will hide the shortcodes and OFF will show the shortcodes - OFF is default) The use of the shortcodes in the Visual Composer Text Box will still work.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_rtl',
			'type' => 'switch',
			'title' => __( "Right-to-Left Mode", "intense" ),
			'desc' => __( "Switch into Right-to-Left mode. This is for languages that read from right to left.", "intense" ),
			'default' => 0,
		),
		array(
			'id'=>'intense_custom_image_sizes',
			'type' => 'multi_text',
			'title' => __( 'Custom Image Sizes', 'intense' ),
			'desc' => __( 'Example:<br />940|400|postWide|true<br />600|600|square600|true', 'intense' ),
			'subtitle' => __( 'Create your own custom image sizes. You will need to Rebuild/Regenerate your thumbnails for these image sizes to work.<br /><br />
				First Parameter: Image Width (number only)<br />
     			Second Parameter: Image Height (number only)<br />
     			Third Parameter: Name (no spaces or special characters)<br />
     			Fourth Parameter: Crop (true or false)', 'intense' )
		),		
		// array(
		//  'id'=>"intense_custom_image_size",
		//  'type' => 'group', //doesn't need to be called for callback fields
		//  'title' => __( 'Custom Image Sizes', 'intense' ),
		//  //'subtitle' => __( 'Group any items together. Experimental. Use at your own risk.', 'intense' ),
		//  'groupname' => __( 'Image Size', 'intense' ), // Group name
		//  'subfields' =>
		//  array(
		//   array(
		//    'id'=>'intense_custom_image_name',
		//    'type' => 'text',
		//    'title' => __( 'Name', 'intense' ),
		//    'desc' => __( 'No spaces or special characters', 'intense' )
		//   ),
		//   array(
		//    'id'=>'intense_custom_image_dimensions',
		//    'type' => 'dimensions',
		//    'units' => 'px', // You can specify a unit value. Possible: px, em, %
		//    'units_extended' => 'true', // Allow users to select any type of unit
		//    'title' => __( 'Dimensions (Width/Height)', 'intense' ),
		//    'subtitle' => __( 'Select the image width and height', 'intense' ),
		//    //'desc' => __('You can enable or disable any piece of this field. Width, Height, or Units.', 'intense'),
		//    //'default' => array('width' => 200, 'height'=>'100', )
		//   ),
		//   array(
		//    'id'=>'intense_custom_image_crop',
		//    'type' => 'switch',
		//    'title' => __( "Crop", "intense" ),
		//    'desc' => __( "Crop the image to the exact width and height.", "intense" ),
		//    'default' => 0,
		//   ),
		//  ),
		// ),		
		array(
			'id'=>'intense_custom_css',
			'type' => 'ace_editor',
			'title' => __( 'CSS Code', 'intense' ),
			'subtitle' => __( 'Paste your CSS code here.', 'intense' ),
			'mode' => 'css',
			'theme' => 'monokai',
			'desc' => 'Custom CSS used to override the default CSS',
			'default' => ""
		),
		array(
			'id'=>'intense_tracking_code',
			'type' => 'ace_editor',
			//'required' => array( 'layout', 'equals', '1' ),
			'title' => __( 'Tracking Code', 'intense' ),
			'subtitle' => __( 'Paste your Google Analytics (or other) tracking code here. This will be added just before the closing body tag. Include any necessary script or html tags.', 'redux-framework-demo' ),
			'mode' => 'html',
			//'validate' => 'js',
			// 'desc' => 'Validate that it\'s javascript!',
		), array(
			'id'=>'intense_head_code',
			'type' => 'ace_editor',
			//'required' => array( 'layout', 'equals', '1' ),
			'title' => __( 'Closing &lt;/head&gt; Code', 'intense' ),
			'subtitle' => __( 'Custom code to add before the closing &lt;/head&gt; tag. It will be added as-is and should include any necessary script or html tags.', 'redux-framework-demo' ),
			'mode' => 'html',
			//'validate' => 'js',
			// 'desc' => 'Validate that it\'s javascript!',
		), array(
			'id'=>'intense_body_code',
			'type' => 'ace_editor',
			//'required' => array( 'layout', 'equals', '1' ),
			'title' => __( 'Closing &lt;/body&gt; Code', 'intense' ),
			'subtitle' => __( 'Custom code to add before the closing &lt;/body&gt; tag. It will be added as-is and should include any necessary script or html tags.', 'redux-framework-demo' ),
			'theme' => 'monokai',
			'mode' => 'html',
			//'validate' => 'js',
			// 'desc' => 'Validate that it\'s javascript!',
		),
	)
);
