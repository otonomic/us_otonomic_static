<?php

	// Load external file to add support for MultiPostThumbnails. Allows you to set more than one "feature image" per post.
	include_once ( INTENSE_PLUGIN_FOLDER . '/inc/plugins/multiple-post-thumbnails/multi-post-thumbnails.php');

	// Define additional "post thumbnails". Relies on MultiPostThumbnails to work
	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'page'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'page'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'page'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'page'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'post'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'post'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'post'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'post'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_portfolio'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_portfolio'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_portfolio'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_portfolio'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_project'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_project'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_project'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_project'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_news'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_news'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_news'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_news'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_team'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_team'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_team'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_team'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_locations'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_locations'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_locations'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_locations'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_events'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_events'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_events'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_events'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_clients'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_clients'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_clients'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_clients'
			)
		);
	};

	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => __( '2nd Featured Image', 'intense' ),
			'id' => 'feature-image-2',
			'post_type' => 'intense_recipes'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '3rd Featured Image', 'intense' ),
			'id' => 'feature-image-3',
			'post_type' => 'intense_recipes'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '4th Featured Image', 'intense' ),
			'id' => 'feature-image-4',
			'post_type' => 'intense_recipes'
			)
		);
		new MultiPostThumbnails(array(
			'label' => __( '5th Featured Image', 'intense' ),
			'id' => 'feature-image-5',
			'post_type' => 'intense_recipes'
			)
		);
	};
?>