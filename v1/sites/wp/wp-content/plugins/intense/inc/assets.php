<?php

class Intense_Assets {
	static $added_assets = array(
		'styles' => array(),
		'scripts' => array()
	);

	public static $css_files = array(
		'alert'           => '/assets/css/intense/shortcodes/alert.min.css',
		'audio'           => '/assets/css/intense/shortcodes/audio.min.css',
		'badge'           => '/assets/css/intense/shortcodes/badge.min.css',
		'blockquote'      => '/assets/css/intense/shortcodes/blockquote.min.css',
		'button'          => '/assets/css/intense/shortcodes/button.min.css',
		'collapsibles'    => '/assets/css/intense/shortcodes/collapsibles.min.css',
		'content_box'     => '/assets/css/intense/shortcodes/content-box.min.css',
		'content_section' => '/assets/css/intense/shortcodes/content-section.min.css',
		'definitions'     => '/assets/css/intense/shortcodes/definitions.min.css',
		'dropcap'         => '/assets/css/intense/shortcodes/dropcap.min.css',
		'hover_box'       => '/assets/css/intense/shortcodes/hover-box.min.css',
		'hr'              => '/assets/css/intense/shortcodes/hr.min.css',
		'icon'            => '/assets/css/intense/shortcodes/icon.min.css',
		'image'           => '/assets/css/intense/image.min.css',
		'label'           => '/assets/css/intense/shortcodes/label.min.css',
		'layout'          => '/assets/css/intense/shortcodes/layout.min.css',
		'lead'            => '/assets/css/intense/shortcodes/lead.min.css',
		'menu'            => '/assets/css/intense/shortcodes/menu.min.css',
		'mixitup'         => '/assets/css/intense/mixitup.min.css',
		'person'          => '/assets/css/intense/shortcodes/person.min.css',
		'posts'           => '/assets/css/intense/shortcodes/posts.min.css',
		'progress'        => '/assets/css/intense/shortcodes/progress.min.css',
		'promo'           => '/assets/css/intense/shortcodes/promo.min.css',
		'rss'             => '/assets/css/intense/shortcodes/rss.min.css',
		'shadow'          => '/assets/css/intense/shadow.min.css',
		'slider'          => '/assets/css/intense/shortcodes/slider.min.css',
		'table'           => '/assets/css/intense/shortcodes/table.min.css',
		'tabs'            => '/assets/css/intense/shortcodes/tabs.min.css',
		'testimony'       => '/assets/css/intense/shortcodes/testimony.min.css',
		'timeline'        => '/assets/css/intense/shortcodes/timeline.min.css',
		'tooltip'         => '/assets/css/intense/shortcodes/tooltip.min.css',
	);

	function __construct() {
		add_action( 'wp_head', array( __CLASS__, 'intense_head' ) );

		add_action( 'wp_head', array( __CLASS__, 'register' ) );
		add_action( 'admin_head', array( __CLASS__, 'register' ) );
		add_action( 'intense/preview/before', array( __CLASS__, 'register' ) );

		add_action( 'wp_footer', array( __CLASS__, 'enqueue' ) );
		add_action( 'admin_footer', array( __CLASS__, 'enqueue' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue' ) );

		add_action( 'wp_footer', array( __CLASS__, 'intense_footer' ), 1000 );

		add_action( 'intense/preview/after',  array( __CLASS__, 'write' ) );

		// Global variable used for dynamic styles added by shortcodes
		$GLOBALS['IntensePluginInlineStyle'] = null;	

		if (is_admin()) {
			intense_add_style( 'intense.adminicons' );
		}
	}

	public static function enqueue() {
		Intense_Assets::enqueue_style();
		Intense_Assets::enqueue_script();
	}

	/**
	 * Enqueue assets
	 */
	public static function enqueue_script() {
		$assets = self::added_assets();

		intense_add_script( 'intense_modernizr' );

		foreach ( $assets['scripts'] as $script ) wp_enqueue_script( $script );
		
		do_action( 'intense/assets/enqueue', $assets );
	}

	public static function enqueue_style() {
		$assets = self::added_assets();

		intense_add_style( 'intense-custom-css' );

		foreach ( $assets['styles'] as $style ) wp_enqueue_style( $style );

		self::inline_css();
		
		do_action( 'intense/assets/enqueue', $assets );
	}

	/**
	 * Write assets to page
	 */
	public static function write() {
		$assets = self::added_assets();

		intense_add_style( 'intense-custom-css' );
		intense_add_script( 'intense_modernizr' );

		wp_print_styles( $assets['styles'] );
		wp_print_scripts( $assets['scripts'] );

		self::inline_css();

		do_action( 'intense/assets/write', $assets );
	}

	/**
	 * Dumps inline css
	 */
	public static function inline_css() {
		if ( isset( $GLOBALS['IntensePluginInlineStyle'] ) ) {
			echo "<style>";
			echo "
			        /* shortcode styles */
			";

			foreach ( $GLOBALS['IntensePluginInlineStyle'] as $key => $value ) {
				echo $value . "\n";
			}

			echo "</style>";
		}
	}

	/**
	 * Registers the scripts and styles for enqueueing and writing
	 */
	public static function register() {
		$language = substr( get_bloginfo ( 'language' ), 0, 2 );
		$asset_path = INTENSE_PLUGIN_URL . '/assets';
		$minified = ( INTENSE_DEBUG ? "" : "min." );
		$src_path = ( INTENSE_DEBUG ? "src" : "dist" );
		$random = rand();

		wp_register_script( 'accounting.js', $asset_path . '/js/accounting/accounting.' . $minified . 'js', array( 'jquery' ), "0.3.2", true );
		wp_register_script( 'adipoli', $asset_path . '/js/Adipoli/jquery.adipoli.' . $minified . 'js', array( 'jquery' ), "cab9fc672e", true );
		wp_register_script( 'bootstrap-alert', $asset_path . '/js/bootstrap/alert.' . $minified . 'js', array( 'jquery' ), "3.0.1", true );
		wp_register_script( 'camanjs', $asset_path . '/js/caman/dist/caman.full.' . $minified . 'js', array( 'jquery' ), "4.1.2", true );
		wp_register_script( 'chartjs', $asset_path . '/js/chartjs/Chart.' . $minified . 'js', array( 'jquery' ), "8f025f33c0", true );
		wp_register_script( 'colorbox', $asset_path . '/js/colorbox/jquery.colorbox-min.js', array( 'jquery' ), "1.5.3", true );
		wp_register_script( 'colorbox-language', $asset_path . "/v1/js/Colorbox/i18n/jquery.colorbox-$language.js", array( 'jquery', 'colorbox' ), "1.4.27", true );
		wp_register_script( 'easing', $asset_path . '/js/supersized/slideshow/js/jquery.easing.min.js', array( 'jquery' ), "1.3", true );
		wp_register_script( 'effeckt_tabs', $asset_path . '/js/Effeckts/js/modules/tabs.js', array( 'jquery' ), "1.0", true );
		wp_register_script( 'facebook_share', $asset_path . '/js/intense/intense.social.facebook.' . $minified . 'js', null, "1.0", true );
		wp_register_script( 'flexslider', $asset_path . '/js/flexslider/jquery.flexslider-min.js', array( 'jquery' ), "2.2.2", true );
		wp_register_script( 'packery', $asset_path . '/js/packery/index.js', array( 'jquery' ), "v1.2.2", true );		
		wp_register_script( 'galleria', $asset_path . '/js/galleria/src/galleria.' . $minified . 'js', array( 'jquery' ), "1.3.5", true );
		wp_register_script( 'galleriatheme', $asset_path . '/js/galleria/src/themes/classic/galleria.classic.' . $minified . 'js', array( 'jquery', 'galleria' ), "1.3.5", true );
		wp_register_script( 'googlemap', 'https://maps.googleapis.com/maps/api/js?sensor=false&v=3&libraries=panoramio,weather', null, "3.0", true );
		wp_register_script( 'googleplus_share', $asset_path . '/js/intense/intense.social.googleplus.' . $minified . 'js', null, "1.0", true );
		wp_register_script( 'ias_infinite_scroll', $asset_path . '/js/infinite-ajax-scroll/jquery-ias.' . $minified . 'js', array( 'jquery' ), "2.1.0", true );
		wp_register_script( 'intense.animated', $asset_path . '/js/intense/intense.animated.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.animated.image', $asset_path . '/js/intense/intense.animated.image.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.audio', $asset_path . '/js/intense/intense.audio.' . $minified . 'js', null, INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.basicslider', $asset_path . '/js/intense/intense.basicslider.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.collapsible', $asset_path . '/js/intense/intense.collapsible.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.compatible.canvas', $asset_path . '/js/intense/intense.compatible.canvas.' . $minified . 'js', array( 'jquery', 'intense_modernizr' ), "1.0.0", true );
		wp_register_script( 'intense.contentbox', $asset_path . '/js/intense/intense.contentbox.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.contentsection', $asset_path . '/js/intense/intense.contentsection.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.counter', $asset_path . '/js/intense/intense.counter.' . $minified . 'js', array( 'jquery', 'jquery.appear' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.hover.box', $asset_path . '/js/intense/intense.hover.box.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.hr', $asset_path . '/js/intense/intense.hr.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.menu', $asset_path . '/js/intense/intense.menu.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.popover', $asset_path . '/js/intense/intense.popover.' . $minified . 'js', array( 'jquery', 'qtip2' ), "2.3.1", true );
		wp_register_script( 'intense.pricingtable', $asset_path . '/js/intense/intense.pricingtable.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.progress', $asset_path . '/js/intense/intense.progress.' . $minified . 'js', array( 'jquery', 'jquery.appear' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense.shortcodes', $asset_path . '/js/intense/intense.shortcodes.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_localize_script( 'intense.shortcodes', 'intense_vars', array( 
            'plugin_url' => INTENSE_PLUGIN_URL
        ) );
		wp_register_script( 'intense.tooltip', $asset_path . '/js/intense/intense.tooltip.' . $minified . 'js', array( 'jquery', 'qtip2' ), "2.3.1", true );
		wp_register_script( 'intense.video', $asset_path . '/js/intense/intense.video.' . $minified . 'js', array( 'jquery' ), INTENSE_PLUGIN_VERSION, true );
		wp_register_script( 'intense_fitvids', $asset_path . '/js/fitvids/jquery.fitvids.' . $minified . 'js', array( 'jquery' ), "1.1", true );
		wp_register_script( 'intense_modernizr', $asset_path . '/js/modernizr.' . $minified . 'js', array( 'jquery' ), "2.6.2", false );
		wp_register_script( 'intense_text_rotator', $asset_path . '/js/simple-text-rotator/jquery.simple-text-rotator.js', array( 'jquery' ), "v1", true );
		wp_register_script( 'intense_video_js', $asset_path . '/js/video-js/dist/video-js/video.js', array( 'jquery' ), "v4.4.3", true );
		wp_register_script( 'jquery.appear', $asset_path . '/js/jquery-appear/jquery.appear.' . $minified . 'js', array( 'jquery' ), "1.0", true );
		wp_register_script( 'jquery.event.move', $asset_path . '/js/twentytwenty/js/jquery.event.move.js', array( 'jquery' ), "1.3.1", true );
		wp_register_script( 'jquery.jplayer', $asset_path . '/js/jquery.jplayer/jquery.jplayer.min.js', array( 'jquery' ), "2.6.0", true );
		wp_register_script( 'jquery.parallax', $asset_path . '/js/parallax-master/source/jquery.parallax.js', array( 'jquery' ), "1.0", true );
		wp_register_script( 'klass.', $asset_path . '/js/PhotoSwipe/lib/klass.min.js', array( 'jquery' ), "1.0", true );
		wp_register_script( 'linkedin_share', '//platform.linkedin.com/in.js', null, "1.0", true );
		wp_register_script( 'minicolors', $asset_path . '/js/jquery-minicolors/jquery.minicolors.' . $minified . 'js', array( 'jquery' ), "2.1.2", true );
		wp_register_script( 'mixitup', $asset_path . '/js/mixitup/jquery.mixitup.min.js', array( 'jquery' ), "1.5.6", true );
		wp_register_script( 'okvideo', $asset_path . '/js/okvideo/src/okvideo.' . $minified . 'js', array( 'jquery' ), "2.3.2", true );
		wp_register_script( 'owlcarousel', $asset_path . '/js/OwlCarousel/owl-carousel/owl.carousel.' . $minified . 'js', array( 'jquery' ), "1.3.2", true );
		wp_register_script( 'photoswipe', $asset_path . '/js/PhotoSwipe/code.photoswipe.jquery-3.0.5.' . $minified . 'js', array( 'jquery' ), "3.0.3", true );
		wp_register_script( 'picstrips', $asset_path . '/js/picstrips/picstrips-2.0.' . $minified . 'js', array( 'jquery' ), "2.0", true );
		wp_register_script( 'pinterest_share', '//assets.pinterest.com/js/pinit.js', null, "1.0", true );
		wp_register_script( 'prettyphoto', $asset_path . '/js/jquery-prettyPhoto/js/jquery.prettyPhoto.' . $minified . 'js', array( 'jquery' ), "3.1.5", true );
		wp_register_script( 'qtip2', $asset_path . '/js/qtip2/jquery.qtip.' . $minified . 'js', array( 'jquery' ), "v2.2.0", true );
		//used with qtip2 to fix an isotope conflict. http://qtip2.com/guides#integration.isotope
		wp_register_script( 'imagesloaded', $asset_path . '/js/qtip2/imagesloaded.pkg.min.js', array( 'jquery', 'qtip2' ), "v3.0.4", true ); 
		wp_register_script( 'skrollr', $asset_path . '/js/skrollr/' . $src_path . '/skrollr.' . $minified . 'js', "0.6.21", true );
		wp_register_script( 'slicknav', $asset_path . '/js/slicknav/jquery.slicknav.' . $minified . 'js', "1.0.0", true );
		wp_register_script( 'smooth-scroll', $asset_path . '/js/jquery.smooth-scroll/jquery.smooth-scroll.' . $minified . 'js', "v1.4.13", true );
		wp_register_script( 'stumbleupon_share', $asset_path . '/js/intense/intense.social.stumbleupon.' . $minified . 'js', null, "1.0", true );
		wp_register_script( 'styledmarker', $asset_path . '/js/StyledMarker/index.' . $minified . 'js', null, "0.5", true );		
		wp_register_script( 'hoverintent', $asset_path . '/js/superfish/dist/js/hoverIntent.' . $minified . 'js', array( 'jquery' ), "r7", true );
		wp_register_script( 'superfish', $asset_path . '/js/superfish/dist/js/superfish.' . $minified . 'js', array( 'jquery', 'hoverintent' ), "1.7.4", true );
		wp_register_script( 'supersized_js', $asset_path. '/js/supersized/slideshow/js/supersized.3.2.7.' . $minified . 'js', array( 'jquery', 'easing' ), "3.2.7", true );
		wp_register_script( 'supersized_shutter_js', $asset_path. '/js/supersized-themes/slideshow/supersized.shutter.' . $minified . 'js', array( 'jquery', 'supersized_js' ), "3.2.7", true );
		wp_register_script( 'swipebox', $asset_path . '/js/swipebox/src/js/jquery.swipebox.' . $minified . 'js', array( 'jquery' ), "1.2.7", true );
		wp_register_script( 'touchtouch', $asset_path . '/js/TouchTouch/assets/touchTouch/touchTouch.jquery.' . $minified . 'js', array( 'jquery' ), "1.0", true );
		wp_register_script( 'twentytwenty', $asset_path . '/js/twentytwenty/js/jquery.twentytwenty.js', array( 'jquery', 'jquery.event.move' ), "20130814", true );
		wp_register_script( 'twitter_share', $asset_path . '/js/intense/intense.social.twitter.' . $minified . 'js', null, "1.0", true );
		wp_register_script( 'typewatch', $asset_path . '/js/TypeWatch/jquery.typewatch.js', array( 'jquery' ), "2.2", true );
		wp_register_script( 'videoBG', $asset_path . '/js/jquery.videoBG/jquery.videoBG.' . $minified . 'js', "0.2", true );

		wp_register_script( 'magnific-popup', $asset_path . '/js/magnific-popup/dist/jquery.magnific-popup.' . $minified . 'js', array( 'jquery' ), "v0.9.9", true );
		wp_register_script( 'selectize', $asset_path . '/js/selectize/dist/js/standalone/selectize.' . $minified . 'js', array( 'jquery' ), "v0.9.0" );
		wp_register_script( 'bootstrap-tab', $asset_path . '/js/bootstrap/tab.' . $minified . 'js', array( 'jquery' ), "2.3.1" );

		wp_register_style( 'selectize', $asset_path . '/js/selectize/dist/css/selectize.default.' . $minified . 'css', null, "v0.9.0" );
		wp_register_style( 'adipoli', $asset_path .'/js/Adipoli/adipoli.' . $minified . 'css', null, "20130127" );
		wp_register_style( 'colorbox1', $asset_path .'/js/colorbox/example1/colorbox.' . $minified . 'css', null, "1.4.27" );
		wp_register_style( 'colorbox2', $asset_path .'/js/colorbox/example2/colorbox.' . $minified . 'css', null, "1.4.27" );
		wp_register_style( 'colorbox3', $asset_path .'/js/colorbox/example3/colorbox.' . $minified . 'css', null, "1.4.27" );
		wp_register_style( 'colorbox4', $asset_path .'/js/colorbox/example4/colorbox.' . $minified . 'css', null, "1.4.27" );
		wp_register_style( 'colorbox5', $asset_path .'/js/colorbox/example5/colorbox.' . $minified . 'css', null, "1.4.27" );
		wp_register_style( 'effeckt_caption', $asset_path .'/css/intense/captions.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'flexslider', $asset_path . '/js/flexslider/flexslider.' . $minified . 'css', null, "2.1" );
		wp_register_style( 'galleria', $asset_path .'/js/galleria/src/themes/classic/galleria.classic.' . $minified . 'css', null, "1.3.5" );
		wp_register_style( 'intense-font-awesome', $asset_path .'/css/font-awesome.' . $minified . 'css', null, "3.2.1" );
		wp_register_style( 'intense.adminicons', $asset_path .'/css/intense/adminicons.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense.animated', $asset_path .'/css/intense/shortcodes/animated.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense.audio', $asset_path .'/css/intense/shortcodes/audio.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense_hover_box', $asset_path .'/css/intense/shortcodes/hover-box.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense_infinite_scroll', $asset_path .'/js/infinite-ajax-scroll/css/jquery.ias.' . $minified . 'css', null, "2.1.0" );
		wp_register_style( 'intense_text_rotator', $asset_path .'/js/simple-text-rotator/simpletextrotator.' . $minified . 'css', null, "v1" );
		wp_register_style( 'intense_testimonial', $asset_path .'/css/intense/shortcodes/testimonial.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense_video_js_css', $asset_path .'/js/video-js/dist/video-js/video-js.' . $minified . 'css', null, "v4.4.3" );
		wp_register_style( 'intence.visualcomposer', INTENSE_PLUGIN_URL . '/inc/shortcodes/composer/css/intense_composer.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'intense.dialogs', $asset_path .'/css/intense/dialogs.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'magnific-popup', $asset_path . '/js/magnific-popup/dist/magnific-popup.' . $minified . 'css', null, 'v0.9.9' );
		wp_register_style( 'minicolors', $asset_path . '/js/jquery-minicolors/jquery.minicolors.' . $minified . 'css', null, "2.1.2" );
		wp_register_style( 'owlcarousel', $asset_path . '/js/OwlCarousel/owl-carousel/owl.carousel.' . $minified . 'css', null, "1.3.2" );
		wp_register_style( 'owlcarousel-theme', $asset_path . '/js/OwlCarousel/owl-carousel/owl.theme.' . $minified . 'css', null, "1.3.2" );
		wp_register_style( 'owlcarousel-transitions', $asset_path . '/js/OwlCarousel/owl-carousel/owl.transitions.' . $minified . 'css', null, "1.3.2" );
		wp_register_style( 'photoswipe', $asset_path .'/js/PhotoSwipe/photoswipe.' . $minified . 'css', null, "3.0.5" );
		wp_register_style( 'prettyphoto', $asset_path .'/js/jquery-prettyPhoto/css/prettyPhoto.' . $minified . 'css', null, "3.1.5" );
		wp_register_style( 'qtip2', $asset_path .'/js/qtip2/jquery.qtip.' . $minified . 'css', null, "v2.2.0" );
		wp_register_style( 'slicknav', $asset_path .'/js/slicknav/slicknav.' . $minified . 'css', null, "1.0.0" );
		wp_register_style( 'supersized_shutter_style', $asset_path .'/js/supersized-themes/slideshow/supersized.shutter.' . $minified . 'css', array( 'supersized_style' ), "3.2.7" );
		wp_register_style( 'supersized_style', $asset_path .'/js/supersized/slideshow/css/supersized.' . $minified . 'css', null, "3.2.7" );
		wp_register_style( 'supersized_theme', $asset_path .'/css/supersized.' . $minified . 'css', null, INTENSE_PLUGIN_VERSION );
		wp_register_style( 'swipebox', $asset_path .'/js/swipebox/src/css/swipebox.' . $minified . 'css', null, "1.2.3" );
		wp_register_style( 'touchtouch', $asset_path .'/js/TouchTouch/assets/touchTouch/touchTouch.' . $minified . 'css', null, "1.0" );
		wp_register_style( 'twentytwenty', $asset_path .'/js/twentytwenty/css/twentytwenty.' . $minified . 'css', null, "20130814" );

		$css_key = Intense_Assets::get_intense_css_key();
		$hashed_filename = 'intense_' . $css_key . '.css';
		$uploads = wp_upload_dir();
		$intense_uploads_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );
		$intense_uploads_path = $uploads['baseurl'] . '/intense-cache/';

		if ( !file_exists( $intense_uploads_dir . $hashed_filename ) ) {
			Intense_Assets::set_intense_css( $css_key );
		}

		wp_register_style( 'intense-custom-css', $intense_uploads_path . $hashed_filename, null, INTENSE_PLUGIN_VERSION );		

		do_action( 'intense/assets/register' );
	}

	public static function intense_head() {
		global $intense_visions_options;

		// Old browser check
		echo '<!--[if lt IE 7]> <meta id="intense-browser-check" class="no-js ie6 oldie"> <![endif]-->
		        <!--[if IE 7]> <meta id="intense-browser-check" class="no-js ie7 oldie"> <![endif]-->
		        <!--[if IE 8]> <meta id="intense-browser-check" class="no-js ie8 oldie"> <![endif]-->
		        <!--[if IE 9]> <meta id="intense-browser-check" class="no-js ie9 oldie"> <![endif]-->
		        <!--[if gt IE 9]><!--> <meta id="intense-browser-check" class="no-js"> <!--<![endif]-->';

		// HTML5 Shiv
		if ( isset( $intense_visions_options['intense_html5_shiv'] ) && $intense_visions_options['intense_html5_shiv'] == 1 ) {
			echo '<!--[if lt IE 9]><script src="' . INTENSE_PLUGIN_URL . '/assets/js/html5shiv/dist/html5shiv.js' . '"></script><![endif]-->';
		}

		echo "<style type='text/css'>";

		echo ".intense.pagination .active > a { \n  background-color: " . intense_get_plugin_color( 'primary' ) . "; \n  border-color: " . intense_adjust_color_brightness( intense_get_plugin_color( 'primary' ), -30 ) . "; \n  color: " . intense_get_contract_color( intense_get_plugin_color( 'primary' ) ) . ";  } ";

		// Custom CSS
		if ( isset( $intense_visions_options['intense_custom_css'] ) ) {
			echo "\n/* custom css styles */\n" . $intense_visions_options['intense_custom_css'] . "\n";
		}

		echo "\n</style>";

		echo '<!--[if lt IE 9]><script src="' . INTENSE_PLUGIN_URL . '/assets/js/respond/dest/respond.min.js' . '"></script><![endif]-->';

		if ( !empty( $intense_visions_options['intense_head_code'] ) ) {
			echo $intense_visions_options['intense_head_code'];
		}
	}

	public static function intense_footer() {
		global $intense_visions_options;

		if ( !empty( $intense_visions_options['intense_body_code'] ) ) {
			echo $intense_visions_options['intense_body_code'];
		}

		if ( !empty( $intense_visions_options['intense_tracking_code'] ) ) {
			echo $intense_visions_options['intense_tracking_code'];
		}
	}

	/**
	 * Adds an asset to the list of used assets
	 */
	public static function add( $type, $handle ) {
		if ( is_array( $handle ) ) {
			foreach ( $handle as $h )
				self::add( $type, $h );
		} else {
			self::$added_assets[ $type ][ $handle ] = $handle;
		}
	}

	/**
	 * Gets a key used for dynamic intense css file
	 *
	 * @return string key
	 */
	public static function get_intense_css_key() {
		global $intense_visions_options;

		$enabled_styles = filemtime( INTENSE_PLUGIN_FOLDER . '/assets/css/intense/intense.min.css' ) . '|';

		foreach ( Intense_Assets::$css_files as $key => $value ) {
			if ( isset( $intense_visions_options['intense_shortcode_css'][ $key ] ) && $intense_visions_options['intense_shortcode_css'][ $key ] == 1 ) {
				$enabled_styles .= filemtime( INTENSE_PLUGIN_FOLDER . $value ) . '|';
			} else {
				$enabled_styles .= "--|";
			}
		}

		return hash( 'sha1', $enabled_styles . INTENSE_PLUGIN_VERSION );
	}

	/**
	 * Creates the dynamic css file
	 */
	public static function set_intense_css( $css_key ) {
		global $intense_visions_options;

		$css = file_get_contents( INTENSE_PLUGIN_FOLDER . '/assets/css/intense/intense.min.css' );

		foreach ( Intense_Assets::$css_files as $key => $value ) {
			if ( isset( $intense_visions_options['intense_shortcode_css'][ $key ] ) && $intense_visions_options['intense_shortcode_css'][ $key ] == 1 ) {
				$css .= file_get_contents( INTENSE_PLUGIN_FOLDER . $value );
			}
		}

		$hashed_filename = 'intense_' . $css_key . '.css';
		$uploads = wp_upload_dir();
		$intense_uploads_dir = trailingslashit( $uploads['basedir'] . '/intense-cache/' );

        if ( !file_exists( $intense_uploads_dir ) ) {
            mkdir( $intense_uploads_dir, 0777, true );
        }

		if ( !file_exists( $intense_uploads_dir . $hashed_filename ) ) {
			$URL_without_protocol = str_replace( "http://", "//", INTENSE_PLUGIN_URL );
			$URL_without_protocol = str_replace( "https://", "//", $URL_without_protocol );

			$css = str_replace( "[intense_assets_path]", $URL_without_protocol . "/assets" , $css );			
			$css = str_replace( ': ', ':', $css );

			if ( ! file_put_contents( $intense_uploads_dir . $hashed_filename, $css ) ) {
				echo "Oh, no! There were issues creating dynamic CSS for Intense. Check your permissions on the following folder to make sure it is writable: " . $intense_uploads_dir;
				die();
			}
		}
	}

	/**
	 * Get added assets
	 */
	public static function added_assets() {
		$assets = self::$added_assets;

		// Make unique and apply filter
		$assets['styles'] = array_unique( ( array ) apply_filters( 'intense/assets/css', ( array ) array_unique( $assets['styles'] ) ) );
		$assets['scripts'] = array_unique( ( array ) apply_filters( 'intense/assets/js', ( array ) array_unique( $assets['scripts'] ) ) );

		return $assets;
	}
}

new Intense_Assets();

/**
 * Helper function for adding scripts
 *
 * @param string/array $handle the script handle or array of script handles to add
 */
function intense_add_script( $handle ) {
	Intense_Assets::add( 'scripts', $handle );
}

/**
 * Helper function for adding styles
 * @param string/array $handle the style handle or array of style handles to add
 */
function intense_add_style( $handle ) {
	if (is_admin()) {
		Intense_Assets::add( 'styles', $handle );
	} else {
		// load the css inline to prevent janky loading 
		// only if the style has been registered and hasn’t been printed yet.
		// see http://www.peterrknight.com/loading-css-mid-page-html5-wordpress-passing-validation-part-2/
		if ( wp_style_is( $handle, 'registered') && ! wp_style_is( $handle, 'done' ) ) {
			global $wp_styles;
			
			add_filter( 'style_loader_tag', 'intense_html5_inline_stylesheet', 10, 2 );
			$print = $wp_styles->do_item( $handle );

			// set this style to ‘done’ status
			$wp_styles->done[] = $handle;

			// remove from to do just in case the style was invoked elsewhere for the footer
			$to_do = $wp_styles ->to_do;
			if ( is_array( $to_do ) ) {
				foreach( $to_do as $key => $to_do_handle ) {
					if( $to_do_handle == $handle ) {
						unset( $wp_styles->to_do[$key] );
					}
				}
			}			
		}
	}
}

// see http://www.peterrknight.com/loading-css-mid-page-html5-wordpress-passing-validation-part-2/
function intense_html5_inline_stylesheet( $output ) {	
	echo $output;

	//$url = preg_match("/(href=')(.+)(' type)/", $output, $styleurl);

	$output = '
	';

	return $output;
}
