<?php
require_once INTENSE_PLUGIN_FOLDER . '/inc/Intense-photosource.php';

add_action( 'widgets_init', 'intense_photos_widgets' );

function intense_photos_widgets() {
	register_widget( 'Photos_Widget' );
}

class Photos_Widget extends WP_Widget {

	function Photos_Widget() {
		$widget_options = array( 'classname' => 'photos',
			'description' => 'Your most recent public photos.' );
		$control_options = array( 'id_base' => 'photos-widget' );
		$this->WP_Widget( 'photos-widget', 'Intense | Photos', $widget_options, $control_options );
	}

	function widget( $args, $instance ) {
		global $intense_visions_options;
		extract( $args );

		$widget_title = apply_filters( 'widget_title', $instance['title'] );
		$number_of_photos = $instance['number_of_photos'];
		$photo_size = $instance['photo_size'];
		$user = $instance['user'];
		$setid = $instance['set'];
		$groupid = $instance['group'];
		$source = $instance['source'];
		$page = 0;

		//TODO: allow users to choose media items
		// $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		//       foreach ( $_attachments as $key => $val ) {
		//           $attachments[$val->ID] = $_attachments[$key];
		//       }

		echo $before_widget;

		if ( $widget_title ) {
			echo $before_title . $widget_title . $after_title;
		}

		$photo_source = new Intense_Photo_Source( $source, $attachments, $user, $groupid, $setid );
		$photos = $photo_source->get_photos( $number_of_photos, $page );
		$wordpress_size = $photo_source->get_wordpress_size( $photo_size );
	    $wordpress_full_size = $photo_source->get_wordpress_size( ( isset( $intense_visions_options['intense_photosource_full_size'] ) ? $intense_visions_options['intense_photosource_full_size'] : 'large' ) );

		foreach ( $photos as $photo ) {
			$requested_size = $photo->get_closest_size( $wordpress_size["width"], $wordpress_size["height"] );
        	$original_size = $photo->get_closest_size( $wordpress_full_size["width"], $wordpress_full_size["height"] );

			$title = $photo->title;
			$description = $photo->description;
			$link = $photo->link;
			$original_photo_url = $original_size['sourceurl'];
        	$sized_photo_url = $requested_size['sourceurl'];
			$sized_photo_markup = '<img src="' . $sized_photo_url . '" title="' . esc_attr( $title ) . '" alt="' . esc_attr( $description ) . '" />';

			echo '<a title="' . esc_attr( $title ) . '" href="' . $link . '">' . $sized_photo_markup . '</a>';
		}

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number_of_photos'] = $new_instance['number_of_photos'];
		$instance['photo_size'] = $new_instance['photo_size'];
		$instance['source'] = $new_instance['source'];
		$instance['user'] = $new_instance['user'];
		$instance['set'] = $new_instance['set'];
		$instance['group'] = $new_instance['group'];

		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title'     => 'Photos',
			'number_of_photos' => 6,
			'photo_size'    => 'square75',
			'source'     => 'flickr',
			'user'     => 'envato' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number_of_photos' ); ?>">Number of photos to show:</label>
			<input class="widefat" style="width: 40px;" id="<?php echo $this->get_field_id( 'number_of_photos' ); ?>" name="<?php echo $this->get_field_name( 'number_of_photos' ); ?>" value="<?php echo $instance['number_of_photos']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'photo_size' ); ?>">Size of Photos:</label><br />
			<select name="<?php echo $this->get_field_name( 'photo_size' ); ?>" id="<?php echo $this->get_field_id( 'photo_size' ); ?>" value>
				<option value="square75" <?php echo $instance['photo_size'] == "square75" ? "selected" : ""; ?>>Square</option>
				<option value="square150" <?php echo $instance['photo_size'] == "square150" ? "selected" : ""; ?>>Square Larger</option>
				<option value="thumbnail" <?php echo $instance['photo_size'] == "thumbnail" ? "selected" : ""; ?>>Thumbnail</option>
				<option value="small240" <?php echo $instance['photo_size'] == "small240" ? "selected" : ""; ?>>Small</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'source' ); ?>">Source:</label><br />
			<select name="<?php echo $this->get_field_name( 'source' ); ?>" id="<?php echo $this->get_field_id( 'source' ); ?>" value>
				<option value="facebook" <?php echo $instance['source'] == "facebook" ? "selected" : ""; ?>>Facebook</option>
				<option value="flickr" <?php echo $instance['source'] == "flickr" ? "selected" : ""; ?>>Flickr</option>
				<option value="instagram" <?php echo $instance['source'] == "instagram" ? "selected" : ""; ?>>Instagram</option>
				<option value="smugmug" <?php echo $instance['source'] == "smugmug" ? "selected" : ""; ?>>SmugMug</option>
				<option value="zenfolio" <?php echo $instance['source'] == "zenfolio" ? "selected" : ""; ?>>Zenfolio</option>
			</select>
			<p>The sources can be configured in the theme options.</p>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'user' ); ?>">User:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'user' ); ?>" name="<?php echo $this->get_field_name( 'user' ); ?>" value="<?php echo $instance['user']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'set' ); ?>">Set/Album:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'set' ); ?>" name="<?php echo $this->get_field_name( 'set' ); ?>" value="<?php echo $instance['set']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'group' ); ?>">Group/Tag:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'group' ); ?>" name="<?php echo $this->get_field_name( 'group' ); ?>" value="<?php echo $instance['group']; ?>" />
		</p>
	<?php
	}
}
?>
