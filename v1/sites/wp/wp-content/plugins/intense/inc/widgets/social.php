<?php

add_action('widgets_init', 'intense_social_widgets');

function intense_social_widgets() {
    register_widget('Social_Widget');
}

class Social_Widget extends WP_Widget {
    public $social_sites = array('Facebook', 'Dribbble', 'Blogger', 'Twitter', 'Tumblr', 'Google Plus', 'RSS', 'Reddit', 'Email',
        'YouTube', 'Yahoo', 'Vimeo', 'LinkedIn', 'Flickr', 'Foursquare', 'Skype', 'StumbleUpon', 'Deviant Art', 'Pinterest', 'Digg',
        'Forrst', 'MySpace', '500px', 'Aboutme', 'Bing', 'Add This', 'Amazon', "AOL", "App Store", "Apple", "Bebo", "Behance", "Blip",
        'Coroflot', 'Daytum', 'Delicious', 'Design Bump', 'DesignFloat', 'Dropplr', 'Drupal', 'EBay', 'Ember', 'Etsy', 'FeedBurner',
        'FoodSpotting', 'FriendFeed', 'Friendster', 'gdgt', 'github', 'Google Talk', 'Google',  'GrooveShark', 'Hyves', 'IconDock',
        'ICQ', 'Identi', 'iMessage', 'Instagram', 'iTune', 'LastFM', 'Meetup', 'Metacafe', 'Microsoft', 'Mister Wong', 'Mixx', 'MobileMe',
        'NetVibes', 'NewsVine', 'PayPal', 'PhotoBucket', 'Podcast', 'Posterous', 'Qik', 'Quora', 'Scribd', 'Sharethis', 'Slashdot',
        'Slideshare', 'SmugMug', 'SoundCloud', 'Spotify', 'Squidoo', 'StackOverflow', 'Technorati', 'Viddler', 'Virb', 'Wikipedia',
        'WordPress', 'Xing', 'Yelp');

    function Social_Widget() {
        $widget_options = array('classname' => 'social',
                            'description' => 'Displays links to social websites.');
        $control_options = array('id_base' => 'social-widget');
        $this->WP_Widget('social-widget', 'Intense | Social', $widget_options, $control_options);

        natcasesort($this->social_sites);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );

        echo $before_widget;
        echo $before_title . $title . $after_title;

        foreach ($this->social_sites as $site) {
            $id = str_replace(" ", "-", strtolower($site));
            $id_label = $id . "_label";

            if (isset($instance[$id]) && $instance[$id] != '') {
                if (filter_var($instance[$id], FILTER_VALIDATE_EMAIL)) {
                    $instance[$id] = 'mailto:' . $instance[$id];
                }

                echo '<a class="' . $id . ' social-link" href="' . $instance[$id] . '" rel="external nofollow" title="' . $instance[$id_label] . '"><img src="' . INTENSE_PLUGIN_URL . '/assets/img/social/PNG/32px/' . $id . '.png" alt="' . $instance[$id_label] . '" /></a>';
            }
        }

        ?>
        <script>            
            jQuery(function($){
                $(document).ready(function() {
                    $('.social-link').popover({ trigger: 'hover', placement: 'top'});
                });
            });
        </script>
        <?php
        echo $after_widget;

        wp_reset_query();
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags( $new_instance['title'] );

        foreach ($this->social_sites as $site) {
            $id = str_replace(" ", "-", strtolower($site));
            $id_label = $id . "_label";
            $instance[$id] = strip_tags( $new_instance[$id] );
            $instance[$id_label] = strip_tags( $new_instance[$id_label] );
        }

        return $instance;
    }

    function form( $instance ) {
        $defaults = array( 'title' => 'Social Links');

        foreach ($this->social_sites as $site) {
            $id = str_replace(" ", "-", strtolower($site));
            $id_label = $id . "_label";

            $defaults[$id_label] = $site;
        }

        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title', 'intense'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"  />
        </p>
        <div style="height: 300px; overflow: auto; border: 1px solid #ccc; background: #fff; padding: 5px; margin: 5px 0;">
        <?php

        foreach ($this->social_sites as $site) {
            $id = str_replace(" ", "-", strtolower($site));
            $id_label = $id . "_label";
            $url_value = isset($instance[$id]) ? $instance[$id] : "";
            $label_value = isset($instance[$id_label]) ? $instance[$id_label] : "";

            ?>
            <strong><img style="float: left; margin:0 8px 0 0; width: 15px; height: 15px;" src="<?php echo get_template_directory_uri(); ?>/img/social/PNG/32px/<?php echo $id; ?>.png" /><?php echo $site; ?></strong>
            <div style="margin-left:20px;">
                <label for="<?php echo $this->get_field_id( $id_label ); ?>" style="display: inline-block; width: 30px;">Label</label>
                <input type="text"  id="<?php echo $this->get_field_id( $id_label ); ?>" style="display: inline;" name="<?php echo $this->get_field_name( $id_label ); ?>" value="<?php echo $label_value ?>" size="20" />
                <label for="<?php echo $this->get_field_id( $id ); ?>" style="display: inline-block; width: 30px;">URL</label>
                <input type="text" id="<?php echo $this->get_field_id( $id ); ?>" style="display: inline;" name="<?php echo $this->get_field_name( $id ); ?>" value="<?php echo $url_value; ?>" size="20"/>
           </div>
            <?php
        }
        ?>
        </div>
        <?php
    }
}


