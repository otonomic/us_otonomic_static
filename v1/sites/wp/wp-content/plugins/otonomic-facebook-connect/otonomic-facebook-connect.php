<?php
/*
Plugin Name: Otonomic Facebook Connect
Plugin URI: http://otonomic.com
Description: Otonomic Facebook Connect Plugin
Author: otonomic.com
Author URI: http://otonomic.com
*/

function textdomain_load_jquery() {
    wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_script', 'textdomain_load_jquery' );


add_action( 'plugins_loaded', 'otonomic_facebook_connect_init' );

function otonomic_facebook_connect_init()
{
	/* facebook connect */
	add_action( 'wp_ajax_facebook_authorized', 'otonomic_facebook_authorized_callback' );
}

function otonomic_facebook_authorized_callback()
{
	require_once 'facebook_connect_config.php';
	require_once 'facebook-php-sdk/facebook.php';

    $connect_result = ['status' => 'fail']; // Init to false

	$Otonomic_First_Session = Otonomic_First_Session::get_instance();

	/* get current user login id and save the facebook token for this user */
	$wp_user = wp_get_current_user();

	$access_token = $_POST['access_token'];

	/* Now fetch list of pages for this user */
	$facebook = new Facebook(array(
		'appId'  => FACEBOOK_APP_ID,
		'secret' => FACEBOOK_SECRET_KEY,
	));
	$facebook->setAccessToken( $access_token );
	/* first try to extend the access token */

	$fb_user = $facebook->getUser();

	if ($fb_user) {
		$facebook->setExtendedAccessToken();
		if( isset($_SESSION["fb_" . FACEBOOK_APP_ID . "_access_token"]) )
		{
			$access_token = $_SESSION["fb_" . FACEBOOK_APP_ID . "_access_token"];
			$facebook->setAccessToken($access_token);
			$fb_user = $facebook->getUser();
		}

		/* Now fetch the list of pages owned by user */
		$accounts = $facebook->api('/'.$fb_user.'/accounts/');
		$page_list = array();
		$page_ids = array();

		if(count($accounts['data']>0)) {
			foreach($accounts['data'] as $app) {
				// if($app['category']!='Application') // we will see if this creates any error!!
				{
					$page_list[] = array(
						'id'            => $app['id'],
						'name'          => $app['name'],
						'access_token'  => $app['access_token']
					);
					$page_ids[] = $app['id'];
				}
			}
		}
		/* Gather facebook data */
		$fb_user_data = $facebook->api($fb_user);

		if( !$Otonomic_First_Session->is_user_connected( $wp_user->ID ) ) {
			if(!isset($fb_user_data['email'])) {
				$fb_user_data['email'] = $fb_user_data['username'] . '@facebook.com';
            }

			/* check for username */
			$iteration = 1;
			$temp_user_name = $fb_user_data['username'];
			while(username_exists( $temp_user_name )) {
				$temp_user_name = $fb_user_data['username'].$iteration;
				$iteration++;
			}
			$fb_user_data['username'] = $temp_user_name;

			/* Now check if user with fb email exists */
			$wp_user_id = email_exists( $fb_user_data['email'] );

			$OtonomicUser = new OtonomicUser();

			if ( !$wp_user_id ) {
				$wp_user_id = $wp_user->ID;
				/* we will need to update user for this facebook account */

				/* Lets associate current user with authorized facebook account */
				$OtonomicUser->connect_user_to_facebook($wp_user_id, $fb_user_data, true);

				/* Lets create new user for this facebook login */
				//$user_id = $OtonomicUser->registerFromFacebook( $fb_user_data );

            } else {
                if( !$Otonomic_First_Session->is_user_connected( $wp_user->ID ) ) {
                    $OtonomicUser->connect_user_to_facebook($wp_user_id, $fb_user_data, true);
                }

                if( !is_super_admin() ) {
                    /* user with facebook email already exists */
                    /* we will need to move the current user sites to that user and remove current user */

                    $current_user_blogs = get_blogs_of_user( $wp_user->ID );

                    if(count($current_user_blogs)>0) {
                        foreach($current_user_blogs as $current_user_blog) {
                            $blog_id = $current_user_blog->userblog_id;
                            $blog_role = $wp_user->roles[0];

                            /* assign this user data to old user */
                            add_user_to_blog( $blog_id, $wp_user_id, $blog_role );
                        }
                    }
                    /* Now remove current user and login with facebook user */
                    wp_delete_user($wp_user->ID, $wp_user_id);
                    /* Now login with fb user */
                    wp_clear_auth_cookie();
                    wp_set_auth_cookie  ( $wp_user_id , true);
                }
            }
		}

		$wp_user = wp_get_current_user();

		/* Lets update facebook tokens */
		if( !add_user_meta($wp_user->ID, 'fb_access_token', $access_token, true) ) {
			update_user_meta($wp_user->ID, 'fb_access_token', $access_token);
        }

		/* Now lets assign available site to this user */
		$OtonomicSite = new OtonomicSite();
		$assigned_blogs = $OtonomicSite->makeUserAdminOfPages($wp_user->ID, $page_ids);

        if (in_array(get_current_blog_id(), $assigned_blogs)) {
            $connect_result = ['status' => 'success', 'message' => 'Site authorized successfully'];

        } else {
            $connect_result = ['status' => 'fail', 'reason' => 'User not admin of page'];
        }
	}
	echo json_encode($connect_result);
	exit;
}

function load_scripts(){
    wp_enqueue_script( 'jquery' );

    //wp_register_script( 'modal_script', plugins_url( 'js/bootstrap.modal.js', __FILE__ ), array( 'jquery' ));
    //wp_register_script( 'bootstrap-script', plugins_url( 'js/bootstrap.min.js', __FILE__ ), array( 'jquery' ), '3.1.1', true);
    wp_register_script( 'first-loader-script', plugins_url( 'js/first-loader.js', __FILE__ ), array( 'jquery' ), '3.1.1', false);

    // wp_register_script( 'functions_script', plugins_url( 'js/functions1.3.js', __FILE__ ), array( 'jquery' ), '1.0', true);
    wp_register_script( 'functions_script', plugins_url( 'js/functions_oop.js', __FILE__ ), array( 'jquery' ), '1.2', true);
    wp_register_script( 'ga_tracking_script', plugins_url( 'js/ga_tracking.js', __FILE__ ), array( 'jquery' ), '1.1', true);

    //wp_enqueue_script( 'modal_script' );
    //wp_enqueue_script( 'bootstrap-script' );
    wp_enqueue_script( 'first-loader-script' );

    wp_enqueue_script( 'functions_script' );
    wp_enqueue_script( 'ga_tracking_script' );
}

function init_function() {
    require_once( plugin_dir_path( __FILE__ ) . 'facebook_connect.php');
}


// TODO: Enable this after fixes
if( is_admin() ){
    add_action('admin_enqueue_scripts', 'load_scripts');
    add_action('admin_footer', 'init_function');

} else{
    add_action('wp_enqueue_scripts', 'load_scripts');
    add_action('wp_footer', 'init_function');
}
?>