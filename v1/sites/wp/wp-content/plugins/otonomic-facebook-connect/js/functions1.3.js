// The DOM is ready!

//Globally used variables
var reference_time = new Date().getTime();
var user_facebook_id;
var access_token;
var fanpage_permissions = 'manage_pages,email,offline_access';
var personal_permissions = 'user_location,user_about_me,user_photos,user_events,user_videos';

var num_click_connect_facebook = 0;
var num_approve_app = 0;
var num_approve_manage_pages = 0;
var num_reject_basic_permissions = 0;
var num_reject_manage_pages = 0;
var img_height, img_width = 0;

function openModal(url) {
    jQuery.colorbox({
        maxWidth: '100%', maxHeight: '100%', width: '100%', height: '100%', opacity: '0.9', transition: 'elastic', overlayClose: '', fixed: '1', closeButton: '1',
        iframe: true,
        href: url,
        className: 'fbpopup'
    });
}

function openFacebookConnectModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'publish.html', function (responseTxt,statusTxt,xhr) {
     $('#publishModal').modal({
     keyboard: false
     });
     });
     */

    openModal(otonomic_facebook_popoup_url+'connect.php');
}

function openFacebookCancelModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'cancel.html', function (responseTxt,statusTxt,xhr){
     $('#fbCancelModal').modal({
     keyboard:false
     });
     });
     */

    openModal(otonomic_facebook_popoup_url+'rejected_app.php');
}

function closeFacebookCancelModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'cancel.html', function (responseTxt,statusTxt,xhr){
     $('#fbCancelModal').modal('hide');
     });
     */
    // $('#fbCancelModal').modal('hide');
    $.colorbox.close();
}

function openFacebookManagePagesRejectedModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'manage-pages.html', function (responseTxt,statusTxt,xhr) {
     $('#fbManagePagesModal').modal({
     keyboard: false
     });
     });
     */

    openModal(otonomic_facebook_popoup_url+'rejected_manage_pages.php');
}

function closeFacebookManagePagesRejectedModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'manage-pages.html', function (responseTxt,statusTxt,xhr) {
     $('#fbManagePagesModal').modal('hide');
     });
     */
    // $('#fbManagePagesModal').modal('hide');

    $.colorbox.close();
}

function openFacebookManagePagesApprovedModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'connected.html', function (responseTxt,statusTxt,xhr) {
     $('#fbConnectedModal').modal({
     keyboard: false
     });
     });
     */

    openModal(otonomic_facebook_popoup_url+'select_page.php');
}

function closeFacebookManagePagesApprovedModal() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'connected.html', function (responseTxt,statusTxt,xhr) {
     $('#fbConnectedModal').modal('hide');
     });
     */

    // $('#fbConnectedModal').modal('hide');

    $.colorbox.close();
}

function closeModalWindows() {
    /*
     jQuery('#otonomic-facebook-popoup-content').load(otonomic_facebook_popoup_url+'publish.html', function (responseTxt,statusTxt,xhr) {
     $('.publish-modal').modal('hide');
     });
     */

    // $('.publish-modal').modal('hide');

    $.colorbox.close();
}

function openThankYouModal() {
    $("#thankYouModal").modal({ backdrop: 'static', keyboard: false },'show');
    $("#thankYouModal").addClass('animateIn');
    $(".modal-backdrop").addClass('show');
}

function closeThankYouModal() {
    $('#thankYouModal').modal('hide');
}

function facebookManagePagesRejected() {
    trackFBConnect('Facebook Connect', 'App Approved', 'manage_pages not approved','redirecting on rejected manage pages');
    openFacebookManagePagesRejectedModal();
}

function facebookManagePagesApproved() {
    checkAssignSiteToUser(user_facebook_id, access_token);
}

function userAssignedSuccessfully(){
    openFacebookManagePagesApprovedModal();
}

function getListOfUserFanpages() {
    FB.api('/me/accounts', { limit: 100 }, function (response) {
        if (response.data != undefined && response.data.length > 0) {
            //only redirect to fanpage listing if user have any pages.
            redirect_user(connect_path+'facebook_fanpages.php', "Loading pages...");
            trackFBConnect('Facebook Connect', 'App Approved', 'manage_pages approved');

        } else {
            if (typeof rejected_manage_page != 'undefined' && rejected_manage_page == 1) {
                showMessage("You don't have any Facebook fan page", 3000);
                trackFBConnect('Facebook Connect', 'App Approved', 'manage_pages approved, but no pages','show message');

            } else {
                //manage pages permission not found redirect to reject manage fanpages page.
                trackFBConnect('Facebook Connect', 'App Approved', 'manage_pages approved, but no pages','redirecting on rejected manages pages');
                facebookManagePagesRejected();
            }
        }
    });
}

function checkAssignSiteToUser(user_id, access_token) {

    jQuery.post(BUILDER_PATH+"/users/checkFacebookLogin/.json",
        {
            facebook_id: user_id,
            access_token: access_token
        },
        function(data) {
            if(data.status == "fail"){
                window.location.href = data.redirect;
            } else {
                userAssignedSuccessfully();
            }
        }, "json"
    );
}

function connectFacebook(permissions, callback) {
    if (typeof FB == "undefined") {
        console.log('Warning: FB is undefined');
        trackFBConnect('Facebook Connect', 'Error', 'FB is undefined in connectFacebook()', '');
        return false;
    }

    if (typeof permissions == "undefined") {
        permissions = fanpage_permissions;
    }

    FB.login(function (response) {
        if (typeof callback != "undefined" && typeof callback == "function") {
            callback(response);
        } else {
            handleFacebookConnectResponse(response);
        }
    }, {
        scope: permissions
    });

    return false;
}

function handleFacebookConnectResponse(response) {
    if (response.status == 'connected') {
        // user is logged in - approved app
        user_facebook_id = response.authResponse.userID;
        access_token = response.authResponse.accessToken;

        checkPermission('manage_pages', function (result) {
            if (result.status) {
                facebookManagePagesApproved();

            } else {
                if (typeof rejected_manage_page == 'undefined') {
                    //manage pages permission not found redirect to reject manage fanpages page.
                    facebookManagePagesRejected()
                }
            }
        });

    } else {
        // user rejected the app
        if(num_reject_basic_permissions == 0) {
            trackFBConnect('Facebook Connect', 'Reject app');
            trackPageView('reject_app');
        }

        trackFBConnect('Facebook Connect', 'View', 'Facebook reject basic permissions', num_reject_basic_permissions);
        num_reject_basic_permissions++;
        openFacebookCancelModal();
    }
}



function askPersonalPermissions() {
    // trackPageView('facebook_personal_profile_selected');
    connectFacebook(personal_permissions, function (response) {
        checkPermission(personal_permissions, function (response) {
            _debug(response);
            if (response.status) {
                _debug("Personal permissions approved: " + personal_permissions);
                trackFBConnect('Facebook Connect', 'Approve personal permissions', (response.found_permissions).join());
                trackPageView('approve_personal_permissions');
                createPersonalSite();

            } else {
                _debug("Personal permissions rejected: " + personal_permissions);
                trackFBConnect('Facebook Connect', 'Reject personal permissions', (response.not_found_permissions).join());
                trackPageView('reject_personal_permissions');
                jQuery("#facebook_pconnector").trigger("click");
            }
        });
    });
}

function checkPermission(permissions, callback) {
    if (typeof FB == "undefined") {
        console.log('Warning: FB is undefined');
        trackFBConnect('Facebook Connect', 'Error', 'FB is undefined in checkPermission()', '');
        return false;
    }

    var result = {
        status: true,
        found_permissions: new Array(),
        not_found_permissions: new Array(),
        found_available_permissions: new Array()
    };

    if (typeof permissions == "string") {
        permissions = String.prototype.split.call(permissions, ',');
    }

    FB.api('/me/permissions', function (response) {
        if (typeof response.data == "undefined") {
            _debug("Failed to receive permission response");
            result.status = false;
            result.message = response.message;

        } else {
            result.found_available_permissions = response.data[0]; //All the permissions returned from Facebook
            jQuery.each(permissions, function (key, permission) {
                if (jQuery.trim(permission) in result.found_available_permissions) {
                    result.found_permissions.push(jQuery.trim(permission));
                    _debug(permission + ' found', 'info');
                } else {
                    result.status = false;
                    result.not_found_permissions.push(jQuery.trim(permission));
                    _debug(permission + ' not found', 'error');
                }
            });
        }
        callback(result);
    });
}

function _debug(message) {
    if (window.console && window.console.log && typeof window.console.log === "function") {
        console.log(message);
    }
}

function trackPageView(url) {
    if (typeof(_gaq) != 'undefined') {
        _gaq.push(['_trackPageview', '/virtual_pageviews/' + url]);
        console.log('Tracked page view: '+url);
    }
}



// IIFE - Immediately Invoked Function Expression
(function($, window, document) {
    // The $ is now locally scoped
    // Listen for the jQuery ready event on the document
    $(function() {
        jQuery(document).on("click", ".modal_close_btn", function (e) {
            e.preventDefault();
            closeModalWindows();
        });

        jQuery(document).on("click", ".btn-publish", function (e) {
            e.preventDefault();
            openFacebookConnectModal();
        });

        jQuery(document).on("click", ".facebook_connect", function (e) {
            e.preventDefault();
            closeModalWindows();
            if(num_click_connect_facebook == 0) {
                trackPageView('clickfacebook');
            }
            num_click_connect_facebook++;
            connectFacebook();
        });

        jQuery('#why_manage_pages').on('click', function () {
            jQuery('#why_manage_pages_div').slideToggle();
            trackFBConnect('Facebook Connect', 'Why manage pages', 'Reject manage pages');
        });
    });
}(window.jQuery, window, document));
