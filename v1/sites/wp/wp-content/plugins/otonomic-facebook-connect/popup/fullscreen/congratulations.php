<?php
define("WP_OTONOMIC_PLUGIN_URL", '/wp-content/mu-plugins/otonomic-first-session/');
define("WP_OTONOMIC_FBC_URL", '/wp-content/plugins/otonomic-facebook-connect/');
?>
<html>
<head>
	<link rel="stylesheet"  href="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/css/otonomic-guests.css" type="text/css" media="all" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo WP_OTONOMIC_FBC_URL; ?>js/popups.js"></script>
</head>
<body style="margin: 0;">
<div id="facebook_fanpages" class="otonomic-core-style">
	<div id="fbpopup">
		<div id="fbpopup-header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-4 left-section">
						<img src="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/fbpopup/images/logo.png" class="popup-logo" alt="Otonomic.com">
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-2 text-right">
					<span class="glyphicons beer"></span>
				</div>
				<div class="col-xs-10">
					<div class="fbpopup-heading">
						Congratulations!
					</div>
					<div class="fbpopup-sub-heading">
						We have verified that you manage <?php echo $_GET['site_name']; ?> and you can now fully enjoy your website!<br/>
						You can now go back to your website!
					</div>
					<div class="fbpopup-action-links" id="fbpopup-action-links">
						<a data-action="Back to website" href="javascript:void(0);" class="btn btn-oto-blue btn-lg modal_close_btn" data-analytics-label="Delete">Take me back to my website!</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>