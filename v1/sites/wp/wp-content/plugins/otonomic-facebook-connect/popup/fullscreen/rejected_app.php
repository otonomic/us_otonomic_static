<?php
define("WP_OTONOMIC_PLUGIN_URL", '/wp-content/mu-plugins/otonomic-first-session/');
define("WP_OTONOMIC_FBC_URL", '/wp-content/plugins/otonomic-facebook-connect/');
?>
<html>
	<head>
		<link rel="stylesheet"  href="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/css/otonomic-guests.css" type="text/css" media="all" />

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="<?php echo WP_OTONOMIC_FBC_URL; ?>js/popups.js"></script>
	</head>
	<body style="margin: 0;">
		<div id="facebook_fanpages" class="otonomic-core-style">
		    <div id="fbpopup">
		        <div id="fbpopup-header">
		            <div class="container-fluid">
		                <div class="row">
		                    <div class="col-xs-4 left-section">
		                        <img src="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/fbpopup/images/logo.png" class="popup-logo" alt="Otonomic.com">
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-2 text-right">
		                    <span class="glyphicons nameplate"></span>
		                </div>
		                <div class="col-xs-10">
		                    <div class="fbpopup-heading">
			                    Don't you want a website just like <span id="site-name"><?php echo $_GET['site_name']; ?>
		                    </div>
		                    <div class="fbpopup-sub-heading">
			                    Otonomic uses the contents of your Facebook business page to create a professional website for you. The website will look brilliant, allow your customers to buy from you, and absolutely FREE.
		                    </div>
		                    <div class="fbpopup-sub-heading">
			                    Please approve the app so we can find the Facebook pages you manage. Help us to help you!
		                    </div>
			                <div class="fbpopup-action-links" id="fbpopup-action-links">
				                <a data-action="delete_website" href="javascript:void(0);" class="fbpopup-action-btn fbpopup-action-delete modal_close_btn"><span class="glyphicons remove_2"></span>  No, I don't need a website</a>
				                <a data-action="connect_with_facebook" href="javascript:void(0);" class="btn btn-oto-blue facebook_connect" data-analytics-label="Yes this page is mine"><span class="social facebook"></span> Yes, create a website for me</a>
			                </div>
			                <p class="fbpopup-footer-text">
				                Otonomic will not use your personal Facebook data under any circumstances without your permission.<br/>
				                We just need to confirm you own the page.
			                </p>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</body>
</html>