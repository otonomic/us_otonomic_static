<?php
define("WP_OTONOMIC_PLUGIN_URL", '/wp-content/mu-plugins/otonomic-first-session/');
define("WP_OTONOMIC_FBC_URL", '/wp-content/plugins/otonomic-facebook-connect/');
?>
<html>
<head>
	<link rel="stylesheet"  href="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/css/otonomic-guests.css" type="text/css" media="all" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo WP_OTONOMIC_FBC_URL; ?>js/popups.js"></script>
</head>
<body style="margin: 0;">
<div id="facebook_fanpages" class="otonomic-core-style">
	<div id="fbpopup">
		<div id="fbpopup-header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-4 left-section">
						<img src="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>assets/fbpopup/images/logo.png" class="popup-logo" alt="Otonomic.com">
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-2 text-right">
					<span class="glyphicons kiosk_wheels"></span>
				</div>
				<div class="col-xs-10">
					<div class="fbpopup-heading">
						Turn your Facebook page into a website just like <span id="site-name"><?php echo $_GET['site_name']; ?></span>
					</div>
					<div class="fbpopup-content">
						You are about one minute away from launching a great new website with your own photos, videos and posts. All you need to do is connect with Facebook, we take care of the rest!
					</div>
					<div class="fbpopup-action-links" id="fbpopup-action-links">
						<a data-action="connect_with_facebook" href="#" class="btn btn-oto-blue facebook_connect" data-analytics-label="Facebook connect"><span class="social facebook"></span> Connect with facebook</a>
					</div>
					<p class="fbpopup-footer-text">
						Otonomic only reads from your facebook page. We will not share your login information in any way.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>