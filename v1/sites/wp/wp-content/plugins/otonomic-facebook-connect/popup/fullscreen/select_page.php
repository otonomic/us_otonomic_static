<?php
$http_host = $_SERVER['HTTP_HOST'];
$http_host = explode('.', $http_host);
if(count($http_host)>2)
{
	/* we just need last 2 values */
	//$http_host = $http_host[count($http_host)-2].'.'.$http_host[count($http_host)-1];
	$http_host = array_slice($http_host, count($http_host)-2);
	//unset($http_host[0]);
}
$http_host = implode('.', $http_host);
$site_creation_url = 'http://'.$http_host.'/progresslp2';

$access_token = $_GET['access_token'];
define("WP_OTONOMIC_PLUGIN_URL", '/wp-content/mu-plugins/otonomic-first-session/');
define("WP_OTONOMIC_FBC_URL", '/wp-content/plugins/otonomic-facebook-connect/');
require_once('../../facebook_connect_config.php');
require_once('../../facebook-php-sdk/facebook.php');

$facebook = new Facebook(array(
	'appId'  => FACEBOOK_APP_ID,
	'secret' => FACEBOOK_SECRET_KEY,
));
$facebook->setAccessToken( $access_token );

$fb_user = $facebook->getUser();

$page_list = array();

if ($fb_user)
{
	$accounts = $facebook->api('/'.$fb_user.'/accounts/');
	if(count($accounts['data']>0))
	{
		foreach($accounts['data'] as $app)
		{
			// if($app['category']!='Application') // we will see if this creates any error!!
			{
				$page_list[] = array(
					'id'=>$app['id'],
					'name'=>$app['name'],
					'access_token'=>$app['access_token']
				);
			}
		}
	}
}

?>
<html>
<head>
	<link rel="stylesheet"  href="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/css/otonomic-guests.css" type="text/css" media="all" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo WP_OTONOMIC_FBC_URL; ?>js/popups.js"></script>
</head>
<body style="margin: 0;">
<div id="facebook_fanpages" class="otonomic-core-style">
	<div id="fbpopup">
		<div id="fbpopup-header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-4 left-section">
						<img src="<?php echo WP_OTONOMIC_PLUGIN_URL; ?>/assets/fbpopup/images/logo.png" class="popup-logo" alt="Otonomic.com">
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-2 text-right">
					<span class="glyphicons cup"></span>
				</div>
				<div class="col-xs-10">
					<div class="fbpopup-heading">
                        Create Website
					</div>
					<div class="fbpopup-sub-heading">
                        Select a page you manage to create a website from it.
                    </div>

					<div class="fbpopup-page-list-wrapper">
						<?php if(count($page_list)>0): ?>
						<ul class="fbpopup-page-list clearfix">
							<?php foreach($page_list as $page): ?>
                                <?php $specific_site_creation_url = $site_creation_url.'?'.http_build_query(array('page_id' => $page['id'], 'page_name'=>$page['name'])); ?>
								<li>
									<div class="fbpopup-page-wrapper clearfix">
										<div class="fbpopup-page-image">
                                            <a href="<?= $specific_site_creation_url ?>" target="_blank" title="Automatically create a site for this Facebook page">
											    <img src="https://graph.facebook.com/<?php echo $page['id'] ?>/picture?access_token=<?php echo $access_token ?>" />
                                            </a>
										</div>
										<div class="fbpopup-page-name">
                                            <a href="<?= $specific_site_creation_url ?>" target="_blank" title="Automatically create a site for this Facebook page">
											    <?php echo $page['name']; ?>
                                            </a>
										</div>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</div>
					<p>
						<span class="glyphicons bookmark"></span> Don't worry, you'll be able to create websites for your other pages later.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>