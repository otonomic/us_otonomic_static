<?php
include_once 'facebook_connect_config.php';
?>
<script>
	/* GLOBAL VARIABLES */
	var WEBROOT = '<?= PAGE2SITE_FOLDER?>';
	var BUILDER_PATH = '<?= BUILDER_PATH;?>';
	var connect_path = '<?= FACEBOOK_CONNECT_PATH?>';
	var FACEBOOK_APP_ID = '<?= FACEBOOK_APP_ID?>';
	var LOCALHOST = '<?= LOCALHOST?>';
	var MAIN_DOMAIN = '<?= MAIN_DOMAIN?>';
</script>
<!-- Facebook script -->
<div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({ appId: "<?= FACEBOOK_APP_ID?>",status: true,cookie: true,xfbml: true});
        window.fbAsyncInit.fbLoaded.resolve();
    };
    window.fbAsyncInit.fbLoaded = jQuery.Deferred();

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js&appId="+FACEBOOK_APP_ID;
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /Facebook script -->
<link rel="stylesheet" href="<?= plugins_url( 'css/tb.bootstrap.css', __FILE__ );?>">
<link rel="stylesheet" href="<?= plugins_url( 'css/fb-connect.css?v=1.1', __FILE__ );?>">


<div id="otonomic-facebook-popoup-content">
</div>

<!-- pintrest -->
<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
<!-- twitter -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script>
	// var otonomic_facebook_popoup_url = "<?= plugins_url( 'popup/', __FILE__ );?>";
    var otonomic_facebook_popoup_url = "<?= plugins_url( 'popup/fullscreen/', __FILE__ );?>";
	var otonomic_facebook_site_name = "<?php echo get_bloginfo('name'); ?>";
</script>

<!-- google+ -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<!-- Linkedin -->
<script src="//platform.linkedin.com/in.js" type="text/javascript">
  lang: en_US
</script>
