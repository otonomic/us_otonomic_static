<!DOCTYPE html>
<!--[if lte IE 8]> <html class="ie8" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html lang="en">             <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Otonomic - turn your Facebook page into a professional website</title>
    <link rel="stylesheet" type="text/css" href="../css/fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css?v=0.0.1">
    <style>
        body{
            background: #fcf8f5 url(images/smartphone-bg-pattern.png);
        }
        .search_results .close-search{ display: block !important;}
    </style>
    <link rel="stylesheet" type="text/css" href="css/media-queries.css?v=0.0.3" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

</head>

<body>
				<center>
					<div id="tweet_results">
					<p>Content is locked! Please tweet it & unlock the content</p>					
						<a class="twitter-share-button" href="https://twitter.com/share"
						data-via="otonomic" data-text="We take your Facebook page and automatically turn it into a Web & Mobile website! " data-size="medium" data-url="http://otonomic.com/" data-count="vertical">
						Tweet
						</a>					
					</div>
				</center>				
				<div style="clear:both"></div>
				<center>
					<div id="fb_results">
						<p>Content is locked! Please share it & unlock the content</p>					
						<img onclick="shareOnFB();" style="cursor:pointer" src="https://d1wofkmqsniyp0.cloudfront.net/public/v2.0/imgs/fbshare.png">					
					</div>
				</center>

<div class="csszoom" id="smartphone-guy">
<div class="wrapper">
    <div id="main">
        <div class="container2">
            <header>
                <div class="logo"><img src="images/logo.png" alt=" " /></div>
                <div class="top_text">We take your Facebook page and automatically turn it into a Web & Mobile website!</div>
            </header>
            <div class="top_wraper2">
                <h2 class="heading_text">It's free, effortless,<br/>instant and beautiful.<br/>
                    <span style="font-weight: bold;">It's Your Website!</span></h2>
                <p class="text2">Let's enforce your brand and spread your presence. Let's get more customers and make more money. Let's do it now!</p>
                <p class="text3">Get your website now! It's instant and free </p>
                <div class="make_my_website wraps">
                    <table class="table_form search_table">
                        <tr class="form_input2">
                            <td>
                                <div style="position: relative" class="p2s_fanpages">
                                    <input id="main_search_box" type="text" class="input_type input_field LoNotSensitive" value="" name="fName" placeholder="Enter your Facebook page name (or URL)">
                                    <div class="tb search-wrapper" id="search_wrapper_main"></div>
                                    <span class="icon_clear close-search"><img class="close-search" src="/shared/fanpages/images/close.png" width="32" height="32"/></span>
                                </div>
                            </td>
                            <td id="btn_go" title='Choose your page from the suggestions below' class="input_title submit_btn">Get my Website</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear:both"></div>


			

            <a href="http://www.youtube.com/watch?v=c9HlSITDejc&fs=1&autoplay=1&vq=hd1080" id="btn_video" class="video tracking_enabled" data-ga-category="Search LP" data-ga-event="Video" data-ga-label="v=c9HlSITDejc" data-ajax-track="1">
                <span id="video-placeholder">
                </span>
            </a>

            <div class="testimonial">
                <div class="no_of_website_counting"><span class="counting_no">1,091,485</span><span>website created, and counting...</span></div>
                <div class="testimonials_wrap">
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user1.png" alt=" " /></div>
                        <blockquote><span>I was surprised by how easy it was to create and customize the website.</span></blockquote>
                    </div>
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user2.png" alt=" " /></div>
                        <blockquote><span>After years of putting off building a site, I finally have one that works and looks great.</span></blockquote>
                    </div>
                    <div class="testimonail_list">
                        <div class="user_image"><img src="images/user3.png" alt=" " /></div>
                        <blockquote><span>I was surprised by after years of putting off building a site, I finally have one.</span></blockquote>
                    </div>
                </div>
            </div>
            <div class="footer_text">Brought to you by <span style="font-family:Myriad Pro;font-size: 14px; font-weight: bold; color: #f54703;">OTONOMIC</span></div>
        </div>
    </div>
</div>
</div>
				
<?php //include_once('../../../../shared/fanpages/fanpage_autoload.php');?>
<div id="fb-root"></div>

</body>
</html>
