<?php
require_once(WP_OTONOMIC_ARTIST_PLUGIN_DIR.'/includes/classes/Loader/AutoLoader.class.php');
$AutoLoader = Otonomic\Loader\AutoLoader::getInstance();

require_once(WP_OTONOMIC_ARTIST_PLUGIN_DIR.'/public/OtonomicArtistPublic.class.php');
OtonomicArtistPublic::getInstance();

if(is_admin()) {
    require_once(WP_OTONOMIC_ARTIST_PLUGIN_DIR.'/admin/OtonomicArtistAdmin.class.php');
    OtonomicArtistAdmin::getInstance();
}
