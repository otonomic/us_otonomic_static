<?php
namespace Otonomic\Site\Shortcodes\Facebook\Comments;
class FacebookCommentsShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'facebook_comments';

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'numposts' => '5',
            'colorscheme'=>'light',
            'add_show_hide_button' => "false",
        ), $atts );

        $id = uniqid($this->slug.'_');
        $output = <<<E
<div class="fb-comments" id="{$id}" data-width="100%" data-numposts="{$this->atts['numposts']}" data-colorscheme="{$this->atts['colorscheme']}" mobile="auto-detect"></div>
E;

        if($this->atts['add_show_hide_button']==="true" || $this->atts['add_show_hide_button']===true) {
            $extra = <<<E
<div class="tb3"><div class="btn_show_hide btn btn-primary" data-id="{$id}">Show comments</div></div>

<script>
jQuery(document).ready(function($) {
    $('.btn_show_hide').on('click', function() {
        var target_id = $(this).attr('data-id');
        var target_el = $('#'+target_id);
        if(target_el.is(':visible')) {
            $(this).text('Hide comments');
        } else {
            $(this).text('Show comments');
        }
        target_el.toggle();
    });

    $('#{$id}').hide();
});
</script>
E;
            $output = $extra . $output;
        }
        return $output;
    }

    function enqueue_scripts() {

    }
}
