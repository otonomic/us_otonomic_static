<?php
namespace Otonomic\Site\Shortcodes\Facebook\Likebox;
class FacebookLikeboxShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'facebook_like_box';

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'class' => '',
            'show_faces' => 'true',
            'colorscheme'=>'light',
            'header' => null,
            'href' => null,
            'show_border' => null,
            'stream' => null,
            'width' => '100%',
            'height' => null,
        ), $atts );

        $tags = "";
        foreach($this->atts as $att => $value) {
            if($value) {
                $tags .= " data-{$att}='{$value}' ";
            }
        }
        $output = <<<E
<div class="fb-like-box {$this->atts['class']}" mobile="auto-detect" {$tags}></div>
E;
        return $output;
    }
}
