<?php
namespace Otonomic\Site\Shortcodes\Facebook\LikeButton;
class FacebookLikeButtonShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'facebook_like_button';

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'class' => '',
            'layout' => 'button',
            'action' => 'like',
            'show_faces' => 'true',
            'colorscheme'=>'light',
            'header' => null,
            'href' => null,
            'ref' => null,
            'share' => 'false',
            'width' => '100%',
            'height' => null,
        ), $atts );

        $tags = "";
        foreach($this->atts as $att => $value) {
            if($value) {
                $tags .= " data-{$att}='{$value}' ";
            }
        }
        $output = <<<E
<div class="fb-like {$this->atts['class']}" {$tags}></div>
E;
        return $output;
    }
}
