<?php
namespace Otonomic\Site\Shortcodes\ABTest;
class ABTestShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'abtest';

    function __construct() {
        add_shortcode('abtest_variation', [$this, 'abtest_variation_handler']);
        parent::__construct();
    }

    function abtest_variation_handler($atts = [], $content = "") {
        return json_encode([
            'atts' => $atts,
            'content' => $content
        ]);
    }

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'name' => 'abtest',
            'ga_slot' => '20',
            'type' => 'php', // php | html | javascript,
            'trial_mode' => false
        ), $atts );

        //include_once('lib/phpab/phpab.php');

        $test = new \Otonomic\ABTest\PhpAB($this->atts['name'], $this->atts['trial_mode']);
        $test->set_ga_slot($this->atts['ga_slot']);

        $matches = explode('[/abtest_variation]', $content);

        /*
        $regex = "/\[abtest_variation (.*?)\](.*)\[\/abtest_variation\]/";
        preg_match_all($regex, $content, $matches);
*/
        if(empty($matches)) { return ""; } // No variations found

        $num_variations = count($matches);
        $i=1;
        foreach($matches as $variation) {
            // $i<$num_variations; $i++) {
            $variation = trim($variation);
            if(!$variation) { continue; }

            $variation = $variation . "[/abtest_variation]";
            $variation = json_decode(do_shortcode($variation));
            $variation_atts = $variation->atts;
            $variation_name = !empty($variation_atts->name) ? $variation_atts->name : $this->atts['name'] . '-' . $i;
            $variation_name = trim(strtolower($variation_name));
            $variation_name = str_replace([' + ', '+'], [' plus '], $variation_name);
            $variation_name = preg_replace('/[^A-Za-z0-9 _]*/', '', $variation_name);
            $variation_name = str_replace(' ', '_', $variation_name);

            $variation_content = $variation->content;

            if($i>1) {
                $test->add_variation($variation_name, $variation_content); // add a variation
            }
            $variations[$variation_name] = $variation_content;
            $i++;
        }

        $user_variation = $test->get_user_segment();
        if($user_variation == 'control' && !isset($variations['control'])) {
            $user_variation = array_keys($variations)[0];
        }
        $current_page_title = get_the_title();

        $ga = <<<E
        <script>
            jQuery(document).ready(function (e){
                setTimeout(function (){
                    ga('send', 'event', 'ABTest {$this->atts['name']}', 'Variation {$user_variation}', '{$current_page_title}', {'nonInteraction': 1});
                    console.log('AB Test Variation recorded: {$user_variation}');
                }, 1000);

            });
        </script>
E;

        return do_shortcode($variations[$user_variation] . $ga);
    }
}
