<?php
namespace Otonomic\Loader\Shortcode;

class ShortcodesLoader extends \Otonomic\Base\Singleton {

    protected $shortcodes;
    function __construct() {
        $this->shortcodes = [
            '\Otonomic\Site\Shortcodes\Facebook\Comments\FacebookCommentsShortcode',
            '\Otonomic\Site\Shortcodes\Facebook\Likebox\FacebookLikeboxShortcode',
            '\Otonomic\Site\Shortcodes\Facebook\LikeButton\FacebookLikeButtonShortcode',
            '\Otonomic\Site\Shortcodes\Gallery\OtonomicGalleryShortcode',
            '\Otonomic\Site\Shortcodes\Video\EmbedVideoShortcode',
            '\Otonomic\Site\Shortcodes\ABTest\ABTestShortcode',
            '\Otonomic\Site\Shortcodes\Games\Playbuzz\PlaybuzzShortcode',
        ];
        add_action( 'init', array($this,'initShortcodes') );
    }

    function initShortcodes()
    {
        foreach($this->shortcodes as $shortcode)
        {
            call_user_func(array($shortcode, 'getInstance'));
        }
    }

}
