<?php
namespace Otonomic\Site\Shortcodes\Video;
class EmbedVideoShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'embed_video';

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'image' => '',
            'video'=>'',
            'width'=>'640',
            'height'=>'360'
        ), $atts );

        if(empty($this->atts['video']))
            return;

        $output = <<<E
<a class="otonomic-video-popup" href="{$this->atts['video']}">
    <img src="{$this->atts['image']}" width="{$this->atts['width']}" height="{$this->atts['height']}" />
    <span></span>
</a>
E;
        return $output;
    }
}
