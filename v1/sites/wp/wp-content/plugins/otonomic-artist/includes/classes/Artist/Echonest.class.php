<?php
namespace Otonomic\Artist;

class Echonest {

    private $api_key;
    private $api_url;
    private $available_buckets;

    private $transient_prefix;

    function __construct($api_key) {

        $this->api_key = $api_key;
        $this->api_url = 'http://developer.echonest.com/api/v4/';
        $this->available_buckets = [
            'biographies',
            'blogs',
            'familiarity',
            'hotttnesss',
            'images',
            'reviews',
            'songs'
        ];

        $this->transient_prefix = 'otonomic_artist_data_';
    }

    private function safeName($name)
    {
        /* Remove special characters */
        $name=strtolower($name);
        $old_pattern = array("/[^a-zA-Z0-9\/]/", "/_+/", "/_$/");
        $new_pattern = array("_", "_", "");
        $name = strtolower(preg_replace($old_pattern, $new_pattern , $name));
        return $name;
    }

    public function searchArtist($artist_name) {

        $safe_name = $this->safeName($artist_name);
        $artist_name = ($artist_name);

        if ( false === ( $data = get_transient( $this->transient_prefix . $safe_name ) ) ) {

            $search_params = [
                'name' => $artist_name
            ];
            $call_url = $this->generateURL('artist/search',$search_params, $this->available_buckets);
            $data = $this->__makeCall($call_url);

            /* Set transient */
            set_transient($this->transient_prefix . $safe_name, $data, 7 * DAY_IN_SECONDS);
        }
        return $data;
    }
    private function generateURL($type, $params, $buckets) {

        $url = $this->api_url.$type.'?format=json&api_key='.$this->api_key;

        foreach($params as $k=>$v)
            $url .= '&'.$k.'='.rawurlencode($v);

        foreach($buckets as $bucket)
            $url .= '&bucket='.$bucket;

        return $url;

    }

    function __makeCall($url) {

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");

        $output = curl_exec($ch);

        curl_close ($ch);

        return json_decode($output);
    }

}