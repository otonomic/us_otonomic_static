<?php
namespace Otonomic\Site\Shortcodes\Games\Playbuzz;
class PlaybuzzShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
    protected $slug = 'playbuzz';

    function shortcode_handler( $atts , $content="" )
    {
        $this->atts = shortcode_atts( array(
            'game' => '',
            'tags' => 'All',
            'height' => 'auto',
            'recommend' => 'false',
            'margin-top' => '0',
            'game-info' => 'false',
            'comments' => 'false',
            'shares' => 'false',
            'embed-by' => '345e758b-75f6-43ec-8bcf-93158404c675',
            'key' => 'Default'
        ), $atts );

        $tags = "";
        foreach($this->atts as $att => $value) {
            if($att) {
                $tags .= " data-{$att}='{$value}' ";
            }
        }
        $output = <<<E
<div class="card-game">
    <div class="pb_feed" data-height="{$atts['']}"
         data-game="{$atts['game']}" data-tags="{$atts['tags']}"
         data-recommend="{$atts['recommend']}" data-margin-top="{$atts['margin-top']}"
         data-game-info="{$atts['game-info']}" data-comments="{$atts['comments']}"
         data-shares="{$atts['shares']}" data-embed-by="{$atts['embed-by']}"
         data-key="{$atts['key']}">
    </div>
</div>
E;
        return $output;
    }
}
