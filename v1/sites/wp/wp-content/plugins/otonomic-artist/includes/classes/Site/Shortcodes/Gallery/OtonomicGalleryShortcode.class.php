<?php
namespace Otonomic\Site\Shortcodes\Gallery;
class OtonomicGalleryShortcode extends \Otonomic\Base\Shortcode\OtonomicShortcode
{
	protected $slug = 'otonomic_fb_gallery';

	function shortcode_handler( $atts , $content="" )
	{
		$this->atts = shortcode_atts( array(
			'limit' => 4,
            'link'    => 'none',
            'width'   => 90,
            'height'  => 90,
            'title'   => 'hover',
            'target'  => 'self',
            'class'   => ''
		), $atts );

        // Get data for the main content as array
        $images_data = $this->get_data();

        if(empty($images_data)) {
            return "";
        }

        // Get html of the main content, without wrappers of columns, rows, titles etc.
        $counter = 0;
        $items = [];

        $this->atts['target'] = ( $this->atts['target'] === 'yes' || $this->atts['target'] === 'blank' ) ? ' target="_blank"' : '';
        // Add lightbox class
        if ( $this->atts['link'] === 'lightbox' ) $this->atts['class'] .= ' su-lightbox-gallery';

        $return = '<div class="su-custom-gallery su-custom-gallery-title-' . $this->atts['title'] . su_ecssc( $this->atts ) . '">';

		foreach ( $images_data as $image) {
			// $share_image = $otonomic_url->get_share_image_url($item->image_url);
            $return .= $this->get_html_item($image);
            $counter++;
            if($counter>= $this->atts['limit']) { break;}
		}
        $return .= '<div class="su-clear"></div>';
        // Close gallery
        $return .= '</div>';
        if ( $atts['link'] === 'lightbox' ) {
            su_query_asset( 'css', 'magnific-popup' );
            su_query_asset( 'js', 'jquery' );
            su_query_asset( 'js', 'magnific-popup' );
            su_query_asset( 'js', 'su-galleries-shortcodes' );
        }
        su_query_asset( 'css', 'su-galleries-shortcodes' );

        return do_shortcode($return);
	}


    function get_html_item($item) {
        // Prepare slide title
        $title = ( $item->title ) ? '<span class="su-custom-gallery-title">' . stripslashes( $item->title ) . '</span>' : '';
        // Open slide
        $return = '<div class="su-custom-gallery-slide">';
        // Slide content with link
        if ( $item->link ) $return .= '<a href="' . $item->link . '"' . $item->target . '><img src="' . $item->image . '" alt="' . esc_attr( $item->title ) . '" width="' . $this->atts['width'] . '" height="' . $this->atts['height'] . '" />' . $title . '</a>';
        // Slide content without link
        else $return .= '<a href="'.$item->image_url.'"><img src="' . $item->image . '" alt="' . esc_attr( $item->title ) . '" width="' . $this->atts['width'] . '" height="' . $this->atts['height'] . '" />' . $title . '</a>';
        // Close slide
        $return .= '</div>';

        return $return;
    }


    function get_data() {

        if ( false === ( $images_data = get_transient( 'facebook_gallery_images' ) ) ) {

            //$otonomic_url = \otonomic_url::get_instance();

            $url = "https://graph.facebook.com/v2.0/". get_option('facebook_id') ."/albums?access_token=". FB_TOKEN ."&fields=id,name,count,photos.fields(id,picture,source,caption,width,height,created_time,updated_time,name)";
            $albums_data = json_decode(file_get_contents($url));
            $images_data = [];

            foreach ( $albums_data->data as $album) {
                foreach($album->photos->data as $photo) {
                    $item = new \stdClass();
                    $item->ID = $photo->id;
                    $item->short_description = "";
                    $item->image_url = $photo->source;
                    //$item->image_resized = $otonomic_url->otonomic_resize_image($item->image_url, $this->atts['width'], $this->atts['height']);
                    $item->image_resized = $item->image_url;
                    $item->title = "";
                    $item->subtitle = "";
                    $item->description = "";
                    $item->image = $item->image_resized;

                    // $item->image_resized = $item->image_url;

                    $images_data[] = $item;
                }
            }

            set_transient( 'facebook_gallery_images', $images_data, 5 * MINUTE_IN_SECONDS );
        }

        return $images_data;
    }
}