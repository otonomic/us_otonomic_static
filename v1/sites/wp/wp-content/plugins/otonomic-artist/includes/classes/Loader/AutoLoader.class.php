<?php
namespace Otonomic\Loader;

class AutoLoader {

    private function __construct() {
        spl_autoload_register( array($this,'autoLoad') );
    }

    public static function getInstance() {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    function autoLoad( $class ) {
        $class_array = explode('\\', $class);
        if($class_array[0] == 'Otonomic') {
            $class_name = end($class_array);
            $class_name = $class_name.'.class.php';
            $name_space_path = array_slice($class_array, 1, -1);
            $name_space_path = implode('/',$name_space_path);
            $class_path = WP_OTONOMIC_ARTIST_PLUGIN_DIR.'/includes/classes/'.$name_space_path.'/'.$class_name;

            if(file_exists($class_path)) {
                require_once($class_path);
            }
        }
    }
}