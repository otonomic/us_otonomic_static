<?php
namespace Otonomic\Loader\Widget;

class WidgetsLoader extends \Otonomic\Base\Singleton {

	protected $widgets;
    function __construct() {
        $this->widgets = [
            'Otonomic\Site\Widgets\ArtistBio',
        ];
		add_action( 'widgets_init', array($this,'initWidgets') );
    }

	function initWidgets()
	{
		foreach($this->widgets as $widget)
		{
			register_widget( $widget );
		}
	}

}
