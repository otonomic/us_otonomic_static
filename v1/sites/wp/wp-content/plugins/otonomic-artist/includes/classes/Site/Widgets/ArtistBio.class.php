<?php
namespace  Otonomic\Site\Widgets;
class ArtistBio extends \Otonomic\Base\Widget\OtonomicWidget {
    /**
     * Sets up the widgets name etc
     */
    public function __construct() {

        $this->name = 'otonomic_artist_bio';
        $this->slug = $this->name;
        $this->title = 'Otonomic Artist Bio';
        $this->locale = 'otonomic-widgets';
        $this->widget_options = array(
            'description'=> 'Artist Bio widget.'
        );

        $this->init_widget();

    }

    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['artist_name'] = ( ! empty( $new_instance['artist_name'] ) ) ? strip_tags( $new_instance['artist_name'] ) : '';
        return $instance;
    }
}