<?php
namespace Otonomic\Base\Widget;

abstract class OtonomicWidget extends \WP_Widget {
    var $dataType = ''; // E.g. 'services', 'testimonials', 'products'

    var $name,
        $slug,
        $title,
        $locale,
        $widget_options;

    protected $path;

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        if(!isset($args['before_widget'])) { $args['before_widget'] = ""; }
        if(!isset($args['after_widget'])) { $args['after_widget'] = ""; }

        echo $args['before_widget'];

        if(!empty($instance['template']) && file_exists($this->path . '/views/'.$instance['template'].'.php')) {
            include($this->path . '/'.$instance['template'].'.php');

        } else {
            include($this->path . '/widget.php');
        }

        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        $instance = array_merge($this->getDefaultInstance(), (array)$instance);
        include($this->path . '/admin.php');
    }

    /**
     * Helper function for registering and enqueueing scripts and styles.
     *
     * @name    The     ID to register with WordPress
     * @file_path       The path to the actual file
     * @is_script       Optional argument for if the incoming file_path is a JavaScript source file.
     */
    protected function load_file($name, $file_path, $is_script = false) {

        $url = WP_OTONOMIC_ARTIST_PLUGIN_DIR . $file_path;
        $file = WP_OTONOMIC_ARTIST_PLUGIN_URL . $file_path;

        if(file_exists($file)) {
            if($is_script) {
                wp_register_script($name, $url);
                wp_enqueue_script($name);

            } else {
                wp_register_style($name, $url);
                wp_enqueue_style($name);
            } // end if
        } // end if

    } // end load_file

    /**
     * Registers and enqueues stylesheets for the administration panel and the
     * public facing site.
     */
    protected function register_scripts_and_styles() {
    }

    protected function init_widget() {
        $this->path = WP_OTONOMIC_ARTIST_PLUGIN_DIR . '/views/widgets/' . $this->slug;
        $this->WP_Widget($this->name, __($this->title, $this->locale), $this->widget_options);
        load_plugin_textdomain($this->locale, false, WP_OTONOMIC_ARTIST_PLUGIN_DIR . '/lang/' );
        $this->register_scripts_and_styles();
    } // end init_plugin_constants



    function getSelectedData() {
        // get widget ID from url call

        // get widget instance from DB

        // get selected IDs

        // return selected IDs + their relevant data

        /*
         * For example, for a testimonials widget, the widget instance will have as a string IDs of specific testimonials that should be shown, in order.
         * Same goes for a gallery - e.g. ids = 3501,3503,3508
         * This function should get the widget ID, and return these ids (e.g. return "3501,3503,3508")
         * $.getJSON('/wp-admin/admin.php?page=ot-fs-testimonials')
         */
    }

    function getSelectedHtml() {
        $selected_data = $this->getSelectedData();
        /*
        * take these ids, and return a table for data manipulation from the relevant admin page
        * For example, for testimonials, it should return the results of
        */

    }

    protected function getDefaultInstance() {
        return [
            'title' => trim(str_replace("Otonomic", "", $this->title))
        ];
    }



    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }
}