<?php
/*
Plugin Name: Otonomic Artist
Plugin URI: http://otonomic.com
Description: Otonomic Artist.
Author: otonomic.com
Author URI: http://otonomic.com
*/

define("WP_OTONOMIC_ARTIST_PLUGIN_DIR", addslashes(dirname(__FILE__)));
$pinfo = pathinfo(WP_OTONOMIC_ARTIST_PLUGIN_DIR);
define("WP_OTONOMIC_ARTIST_PLUGIN_FILE", addslashes(__FILE__));
define("WP_OTONOMIC_ARTIST_PLUGIN_URL", plugins_url( $pinfo['basename'].'/', dirname(__FILE__)));

define("WP_OTONOMIC_ARTIST_PLUGIN_NAME", 'Otonomic Artist');
define("WP_OTONOMIC_ARTIST_PLUGIN_SLUG", 'otonomic-artist');

define("OTONOMIC_ECHONEST_API_KEY", 'TDUV4LMT3WNCP1AJ1');

require_once(WP_OTONOMIC_ARTIST_PLUGIN_DIR.'/includes/init.php');