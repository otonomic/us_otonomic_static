<?php
class OtonomicArtistPublic extends \Otonomic\Base\Singleton {
    public function __construct() {

        Otonomic\Loader\Widget\WidgetsLoader::getInstance();
        Otonomic\Loader\Shortcode\ShortcodesLoader::getInstance();

        add_action('init', array($this, 'initialize'));

        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

        add_action('wp_footer', array($this, 'footer'));
    }
    public function initialize() {

    }

    function enqueue_styles() {

        wp_register_style( WP_OTONOMIC_ARTIST_PLUGIN_SLUG.'-main-style', WP_OTONOMIC_ARTIST_PLUGIN_URL.'/assets/css/main.css', array(), '1.0', 'all' );
        wp_enqueue_style( WP_OTONOMIC_ARTIST_PLUGIN_SLUG.'-main-style' );

    }

    function enqueue_scripts() {

        wp_register_script(WP_OTONOMIC_ARTIST_PLUGIN_SLUG.'-main-script', WP_OTONOMIC_ARTIST_PLUGIN_URL.'/assets/js/main.js');
        wp_enqueue_script(WP_OTONOMIC_ARTIST_PLUGIN_SLUG.'-main-script');

    }
    public function footer() {
        ?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=<?php echo FACEBOOK_APP_ID; ?>&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
<?php
    }

}