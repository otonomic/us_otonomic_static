jQuery(document).ready( function ($){
    jQuery('.otonomic-video-popup').bind('click', function (e){
        var video = jQuery(this).attr('href');
        var image = jQuery(this).children('img').attr('src');
        jQuery(this).colorbox({
            width:"auto",
            height:"auto",
            html:'<div id="otonomic-video-content"><video class="html_video" autoplay="1" controls="1" poster="'+image+'"><source src="'+video+'" type="video/mp4"></video></div>'
        });
    });
});