<?php
$instance = wp_parse_args((array)$instance, array('title' => ''));
$title = $instance['title'];
$artist_name = $instance['artist_name'];
?>
<div class="otonomic-widget-settings clearfix">
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
        <label for="<?php echo $this->get_field_id( 'artist_name' ); ?>"><?php _e( 'Artist name:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'artist_name' ); ?>" name="<?php echo $this->get_field_name( 'artist_name' ); ?>" type="text" value="<?php echo esc_attr( $artist_name ); ?>">
    </p>
</div>