<?php
$Echonest = new Otonomic\Artist\Echonest(OTONOMIC_ECHONEST_API_KEY);
$artist_data = $Echonest->searchArtist( $instance['artist_name'] );
?>
<?php echo $args['before_title'] ?><?= $instance['title']; ?><?php echo $args['after_title'] ?>
<div class="artistbio">
        <?php echo $artist_data->response->artists[0]->biographies[0]->text; ?>
</div>