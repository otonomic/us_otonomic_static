<?php
class PG_Register_Post_Type{
	public function __construct(){
		add_action('init', array(&$this,'init'));
	}
	
	public function init(){
		// Register gallery post type
		$this->register_gallery_post_type();
	}
	
	function register_gallery_post_type(){
		$labels = array(
			'name'               => _x( 'Plus WP Gallery', 'post type general name', 'wp_plusgallery' ),
			'singular_name'      => _x( 'Plus WP Gallery', 'post type singular name', 'wp_plusgallery' ),
			'menu_name'          => _x( 'Plus-WPGallery', 'admin menu', 'wp_plusgallery' ),
			'name_admin_bar'     => _x( 'Gallery', 'add new on admin bar', 'wp_plusgallery' ),
			'add_new'            => _x( 'Create New Album', 'Album', 'wp_plusgallery' ),
			'add_new_item'       => __( 'Create New Album', 'wp_plusgallery' ),
			'new_item'           => __( 'New Album', 'wp_plusgallery' ),
			'edit_item'          => __( 'Edit Album', 'wp_plusgallery' ),
			'view_item'          => __( 'View Album', 'wp_plusgallery' ),
			'all_items'          => __( 'All Albums', 'wp_plusgallery' ),
			'search_items'       => __( 'Search Album', 'wp_plusgallery' ),
			'parent_item_colon'  => __( 'Parent Album:', 'wp_plusgallery' ),
			'not_found'          => __( 'No Album found.', 'wp_plusgallery' ),
			'not_found_in_trash' => __( 'No Album found in Trash.', 'wp_plusgallery' )
		);
			
		$args = array (
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'plus_wpgallery', 'with_front' => false, 'feeds' => true ) ,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'author', 'thumbnail' )
		);
			
		register_post_type( 'plus_wpgallery', $args );
			
		/* Register Project Categories
		 */
		$plus_wpgallery_cat_label = array(
			'name' => 'Plus-WPGallery Categories',
			'singular_name' => 'Plus-WPGallery Category'
		);
			
		$argss = array(
			'labels' => $plus_wpgallery_cat_label,
			'show_ui'               => true,
			'query_var'             => true,
			'show_in_nav_menus'     => false,
			'meta_box_cb'			=> false,
			'hierarchical' 			=> true,
		);
		// Register taxonomy
		register_taxonomy( 'plus_wpgallery_cat', 'plus_wpgallery', $argss);
	}
}
new PG_Register_Post_Type();