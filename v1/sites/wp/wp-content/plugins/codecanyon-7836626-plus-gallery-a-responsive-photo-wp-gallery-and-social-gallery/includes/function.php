<?php
function pg_get_option($key,$default=''){
	$options = get_option('pg_settings');
	if(isset($options[$key]))
		return $options[$key];
	return $default;
}