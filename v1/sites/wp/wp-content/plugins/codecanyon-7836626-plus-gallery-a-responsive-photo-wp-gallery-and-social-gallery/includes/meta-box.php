<?php
class PG_Meta_box{
	
	public function __construct(){
		add_action ('admin_enqueue_scripts',array(&$this,'enqueue_styles'));
		add_action ('admin_enqueue_scripts',array(&$this,'enqueue_scripts'));
		
		// Create metabox
		add_action('add_meta_boxes', array(&$this, 'add_meta_boxes') );
		add_action ( 'save_post', array (&$this,'save_meta_boxes' ), 1, 2 );
	}
	
	public function enqueue_styles(){
		wp_enqueue_style('tnpg_meta_box_css', plugin_dir_url(__FILE__).'assets/css/meta-box.css');
	}
	
	public function enqueue_scripts(){
		wp_enqueue_script('tnpg_meta_box_js',plugin_dir_url(__FILE__).'assets/js/meta-box.js',array('jquery','jquery-ui-sortable'));
	}
	
	function add_meta_boxes(){
		// Gallery
		$meta_box = array(
			'id' 		=> 'pggallery_data',
			'title' 	=> __('Gallery', 'wp_plusgallery'),
			'description' => '',
			'pages'		=> array( 'plus_wpgallery' ),
			'post_type' => 'plus_wpgallery',
			'context' 	=> 'normal',
			'priority' 	=> 'high',
			'fields' 	=> array(
				array(
					'name' => 'plus_wpgallery',
					'label' => __( 'Upload', 'wp_plusgallery' ),
					'description'	=> __( 'Upload images for this album' ),
					'std'	=> '',
					'type'	=> 'gallery'
				),
			)
		);
		add_meta_box ( $meta_box ['id'], $meta_box ['title'], array(&$this,'pg_render_meta_boxes'), $meta_box ['post_type'], $meta_box ['context'], $meta_box ['priority'], $meta_box );
	
		// Select category
		$taxonomy     = 'plus_wpgallery_cat';
		$orderby      = 'name';
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no
		$title        = '';
	
		$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'show_count'   => $show_count,
			'pad_counts'   => $pad_counts,
			'hierarchical' => $hierarchical,
			'title_li'     => $title,
			'hide_empty'	=> false,
		);
		$categories = get_categories($args);
		
		$categoriesList = array();
		$categoriesList[''] = '-- Select Category --';
		foreach ($categories as $cat){
			$categoriesList[$cat->slug] = $cat->name;
		}
		$meta_box = array(
			'id' 		=> 'pggallery_cat',
			'title' 	=> __('Category', 'wp_plusgallery'),
			'description' => '',
			'pages'		=> array( 'plus_wpgallery' ),
			'post_type' => 'plus_wpgallery',
			'context' 	=> 'normal',
			'priority' 	=> 'high',
			'fields' 	=> array(
				array(
					'name' => 'plus_wpcat',
					'label' => __( 'Category', 'wp_plusgallery' ),
					'description'	=> __( 'Select category for this album' ),
					'std'	=> '',
					'type'	=> 'select',
					'options'=> $categoriesList,
				),
			)
		);
		add_meta_box ( $meta_box ['id'], $meta_box ['title'], array(&$this,'pg_render_meta_boxes'), $meta_box ['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );
	}
	
	function pg_render_meta_boxes($post, $meta_box){
		$args = $meta_box ['args'];
		if(!defined('HT_META_BOX_NONCE')):
		define('HT_META_BOX_NONCE', 1);
		wp_nonce_field ('ht_meta_box_nonce', 'ht_meta_box_nonce',false);
		endif;
	
		if (! is_array ( $args ))
			return false;
	
		echo '<div class="ht-metaboxes">';
		if (isset ( $args ['description'] ) && $args ['description'] != '') {
			echo '<p>' . $args ['description'] . '</p>';
		}
	
		// Render
		foreach ( $args ['fields'] as $field ) {
			if(!isset($field['type']))
				continue;
	
			$field['name']          = isset( $field['name'] ) ? $field['name'] : '';
			$field['name'] 	= strstr( $field['name'], '_ht_' ) ? sanitize_title( $field['name'] ) : '_ht_' . sanitize_title( $field['name'] );
	
			$value = get_post_meta( $post->ID,$field['name'], true );
	
			$field['value']         = isset( $field['value'] ) ? $field['value'] : '';
			if($value !== '' && $value !== null && $value !== array() && $value !== false)
				$field['value'] = $value;
	
	
			$field['id'] 			= isset( $field['id'] ) ? $field['id'] : $field['name'];
			$field['description'] 	= isset($field['description']) ? $field['description'] : '';
			$field['label'] 		= isset( $field['label'] ) ? $field['label'] : '';
			$field['placeholder']   = isset( $field['placeholder'] ) ? $field['placeholder'] : $field['label'];
	
			$field['name'] = 'ht_meta['.$field['name'].']';
	
			switch ($field['type']){
				case 'hr':
					echo '<div style="margin-top:20px;margin-bottom:20px;">';
					echo '<hr>';
					echo '</div>';
					break;
				case 'select':
					$field['options']       = isset( $field['options'] ) ? $field['options'] : array();
					echo '<div  class="ht-meta-box-field ' . esc_attr( $field['id'] ) . '_field"><label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label><select id="' . esc_attr( $field['id'] ) . '" name="' . esc_attr( $field['name'] ) . '">';
					foreach ( $field['options'] as $key => $value ) {
						echo '<option value="' . esc_attr( $key ) . '" ' . selected( esc_attr( $field['value'] ), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
					}
					echo '</select> ';
					if ( ! empty( $field['description'] ) ) {
						echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
					}
					echo '</div>';
					break;
				case 'gallery':
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					}
	
					if(!defined('_HT_META_GALLERY_JS')):
					define('_HT_META_GALLERY_JS', 1);
					?>
									<script type="text/javascript">
										jQuery(document).ready(function($) {
											
											$('.ht-meta-gallery-select').on('click',function(e){
												e.stopPropagation();
												e.preventDefault();
												
												var $this = $(this),
													ht_meta_gallery_list = $this.closest('.ht-meta-box-field').find('.ht-meta-gallery-list'),
													ht_meta_gallery_frame,
													ht_meta_gallery_ids = $this.closest('.ht-meta-box-field').find('#ht_meta_gallery_ids'),
													_ids = ht_meta_gallery_ids.val();
					
												if(ht_meta_gallery_frame){
													ht_meta_gallery_frame.open();
													return false;
												}
												
												ht_meta_gallery_frame = wp.media({
													title: '<?php echo __('Add Images to Gallery',HT_PLUSGALLERY)?>',
													button: {
														text: '<?php echo __('Add to Gallery',HT_PLUSGALLERY)?>',
													},
													library: { type: 'image' },
													multiple: true
												});
					
												ht_meta_gallery_frame.on('select',function(){
													var selection = ht_meta_gallery_frame.state().get('selection');
													selection.map( function( attachment ) {
														attachment = attachment.toJSON();
														if ( attachment.id ) {
															_ids = _ids ? _ids + "," + attachment.id : attachment.id;
															ht_meta_gallery_list.append('\
																<li data-id="' + attachment.id +'">\
																	<div class="thumbnail">\
																		<div class="centered">\
																			<img src="' + attachment.url + '" />\
																		</div>\
																		<a href="#" title="<?php echo __('Delete',HT_PLUSGALLERY)?>"><?php echo __('x',HT_PLUSGALLERY)?></a></li>\
																	</div>\
																</li>'
															);
														}
														ht_meta_gallery_ids.val( ht_trim(_ids,',') );
														ht_meta_gallery_fn();
													});
												});
					
												ht_meta_gallery_frame.open();
											});
											var ht_meta_gallery_fn = function(){
												if($('.ht-meta-gallery-list').length){
													$('.ht-meta-gallery-list').each(function(){
														var $this = $(this);
														$this.sortable({
															items: 'li',
															cursor: 'move',
															forcePlaceholderSize: true,
															forceHelperSize: false,
															helper: 'clone',
															opacity: 0.65,
															placeholder: 'li-placeholder',
															start:function(event,ui){
																ui.item.css('background-color','#f6f6f6');
															},
															update: function(event, ui) {
																var _ids = '';
																$this.find('li').each(function() {
																	var _id = $(this).data( 'id' );
																	_ids = _ids + _id + ',';
																});
													
																$this.closest('.ht-meta-box-field').find('#ht_meta_gallery_ids').val( ht_trim(_ids,',') );
															}
														});
					
														$this.find('a').on( 'click',function(e) {
															e.stopPropagation();
															e.preventDefault();
															$(this).closest('li').remove();
															var _ids = '';
															$this.find('li').each(function() {
																var _id = $(this).data( 'id' );
																_ids = _ids + _id + ',';
															});
					
															$this.closest('.ht-meta-box-field').find('#ht_meta_gallery_ids').val( ht_trim(_ids,',') );
					
															return false;
														});
														
													});
												}
											}
											ht_meta_gallery_fn();
										});
									</script>
									<?php
									endif;
									echo '<div  class="ht-meta-box-field ' . esc_attr( $field['id'] ) . '_field">';
									echo '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label>';
									echo '<div class="ht-meta-gallery-wrap"><ul class="ht-meta-gallery-list">';
									if($field['value']){
										$value_arr = explode(',', $field['value']);
										if(!empty($value_arr) && is_array($value_arr)){
											foreach ($value_arr as $attachment_id ){
												if($attachment_id):
											?>
												<li data-id="<?php echo esc_attr( $attachment_id ) ?>">
													<div class="thumbnail">
														<div class="centered">
															<?php echo wp_get_attachment_image( $attachment_id, array(120,120) ); ?>						
														</div>
														<a title="<?php echo __('Delete',HT_PLUSGALLERY) ?>" href="#"><?php echo __('x',HT_PLUSGALLERY) ?></a>
													</div>						
												</li>
											<?php
												endif;
											}
										}
									}
									echo '</ul></div>';
									echo '<input type="hidden" name="' . $field['name'] . '" id="ht_meta_gallery_ids" value="' . $field['value'] . '" />';
									echo '<input type="button" class="button button-primary ht-meta-gallery-select" name="' . $field['id'] . '_button_upload" id="' . $field['id'] . '_upload" value="' . __('Add Gallery Images',HT_PLUSGALLERY) . '" /> ';
									if ( ! empty( $field['description'] ) ) {
										echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
									}
									echo '</div>';
								break;
				}
				echo '</div>';
			}
		}
		public function save_meta_boxes($post_id, $post) {
			// $post_id and $post are required
			if (empty ( $post_id ) || empty ( $post )) {
				return;
			}
			// Dont' save meta boxes for revisions or autosaves
			if (defined ( 'DOING_AUTOSAVE' ) || is_int ( wp_is_post_revision ( $post ) ) || is_int ( wp_is_post_autosave ( $post ) )) {
				return;
			}
			// Check the nonce
			if (empty ( $_POST ['ht_meta_box_nonce'] ) || ! wp_verify_nonce ( $_POST ['ht_meta_box_nonce'], 'ht_meta_box_nonce' )) {
				return;
			}
				
			// Check the post being saved == the $post_id to prevent triggering this call for other save_post events
			if (empty ( $_POST ['post_ID'] ) || $_POST ['post_ID'] != $post_id) {
				return;
			}
				
			// Check user has permission to edit
			if (! current_user_can ( 'edit_post', $post_id )) {
				return;
			}
				
			// Process
			foreach( $_POST['ht_meta'] as $key=>$val ){
				update_post_meta( $post_id, $key, $val );
			}
		}
	// End Class
}
new PG_Meta_box();
