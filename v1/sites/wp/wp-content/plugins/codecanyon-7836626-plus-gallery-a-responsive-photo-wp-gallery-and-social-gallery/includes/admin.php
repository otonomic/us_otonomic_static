<?php
class PG_Admin{
	var $fields = array();
	var $option;
	var $show_cat;
	var $layout;
	var $show_caption;
	var $show_desc;

	public function __construct(){
		$this->show_cat = array('show' => 'Show', 'hide' => 'Hide');// Show WP Categories
		$this->layout = array('3' => '3 Columns', '4' => '4 Columns', '5' => '5 Columns');
		$this->show_caption = array('show' => 'Show', 'hide' => 'Hide');
		$this->show_desc = array('show' => 'Show', 'hide' => 'Hide');

		add_action ('admin_init', array(&$this,'init'));
		add_action ('admin_enqueue_scripts',array(&$this,'enqueue_styles'));
		add_action ('admin_enqueue_scripts',array(&$this,'enqueue_scripts'));
		add_action ('admin_menu', array(&$this,'addMenuPage') );
	}
	
	public function init(){
		register_setting('pg_settings', 'pg_settings');
		add_action ('init', array(&$this,'register_gallery_post_type'));
	}
	
	public function enqueue_styles(){
		wp_enqueue_style('tnpg_bootstrap_css', plugin_dir_url(__FILE__).'assets/css/bootstrap-tabs.css');
		wp_enqueue_style('tnpg_plusgalleryadmin_css', plugin_dir_url(__FILE__).'assets/css/tnplusgalleryadmin.css');
	}
	
	public function enqueue_scripts(){
		wp_enqueue_style( 'wp-color-picker');
		wp_enqueue_script( 'wp-color-picker');
		wp_enqueue_script('tnpg_bootstrap_tab',plugin_dir_url(__FILE__).'assets/js/bootstrap-tab.js');
	}
	
	public function addMenuPage() {
		add_submenu_page('options-general.php', __('Plus Gallery','tnpg'), __('Plus Gallery','tnpg'), 'manage_options', 'pg_settings', array( &$this,'MenuPageRender'));
	}
	
	public function MenuPageRender(){
		global $PG_Manager;
		$dirUrlRoot = $PG_Manager->plugin_url;
		?>
			<div class="wrap wp-plus-galllery-settings">
				<div id="icon-options-general" class="icon32">
					<br>
				</div>
				<h2><?php _e('Plus Gallery Settings') ?></h2>
				<form method="post" action="options.php" id="tnpg_form" name="tnpg_form">
					<?php settings_fields('pg_settings')?>
					<p>
						<button type="submit" id="submit-tnpg-form" class="button button-primary ">Save Changes</button>
					</p>
					<!-- Tabs Options -->
					<div class="tbpg-tabs">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" id="PgTab"> 
							<li><a href="#tnpgDetails" data-toggle="tab">Details</a></li>
							<li><a href="#basicOptions" data-toggle="tab">Basic Options</a></li>
							<li><a href="#wpplOptions" data-toggle="tab">WP Gallery</a></li>
							<li><a href="#facebook" data-toggle="tab">Facebook</a></li>
							<li><a href="#instagram" data-toggle="tab">Instagram</a></li>
							<li><a href="#flickr" data-toggle="tab">Flickr</a></li>
							<li><a href="#google" data-toggle="tab">Google+</a></li>
							<li><a href="#plAdvancedOptions" data-toggle="tab">Advanced Options</a></li>						
						
						</ul>
						
						<!-- Tab panes -->
						<div class="tab-content">
							<!-- Details -->
							<div class="tab-pane fade in" id="tnpgDetails">
								<div class="tnpg-details">
									<br/>
									<a href='http://codecanyon.net/item/plus-gallery-responsive-social-gallery-for-wp/7836626' target='_blank'><img src="<?php echo $dirUrlRoot.'documentasion/assets/images/logo.png';?>"/></a>
									<p style="text-align:justify;font-size:1.3em;">
										<strong>Plus Gallery</strong> | A responsive photo gallery that helps you display Wordpress gallery and Social gallery like <strong>Facebook</strong>, <strong>Instagram</strong>, <strong>Flickr</strong> or <strong>Google Plus</strong>.
									</p>
									<hr/>
									<p style="text-align:justify;">
										Plus Gallery grabs all your albums and images from your Wordpress gallery or from an online source like Facebook, Instagram, Flickr or Google Plus and display them on your site. 
										It allows you to browse albums and galleries, or display just one Gallery at a time. 
										Take a photo with your iphone, post to <strong>Facebook</strong>, <strong>Instagram</strong>, <strong>Flickr</strong> or <strong>Google Plus</strong> and it is automatically added to your site as well.
									</p>
									<hr/>
									<p>
										<strong style="color:#ff6600;">Features:</strong>
										<ul class="tn_features">
											<li>Display your albums and images from Wordpress gallery or an online source like Facebook, Instagram, Google + or Flickr to your site's gallery.</li>
											<li>Manage gallery by including/excluding albums, limit the number of pictures to be display in each album.</li>
											<li>Compatible with Tablet, Smart phone</li>
											<li>Flexible Configuration.</li>
											<li>Support custom css of template.</li>
											<li>Fully Responsive: Display the same on any devices.</li>
											<li>Compatible with <strong>Visual Composer</strong> plugin.</li>
										</ul>
									</p>
								</div>
								<hr/>
								<ul>
									<li><a href="<?php echo $dirUrlRoot.'documentasion/index.html';?>" target="_blank">User Guide</a></li>
									<li><a href="http://codecanyon.net/user/TNT-Team" target="_blank">Support</a></li>
									<li><i>V <?php echo PLUSGALLERY_VER; ?></i></li>
								</ul>
							</div>
							<!-- wpplOptions tab -->
							<div class="tab-pane fade" id="basicOptions">
								<table class="form-table">
									<tbody>
										<tr valign="top">
											<th scope="row"><label for="boxColor"><?php _e('Box Color') ?></label></th>
											<td>											
											<input data-default-color="#38beea" type="text" id="boxColor" value="<?php echo pg_get_option('box_color','#38beea')?>" name="pg_settings[box_color]" />
											<p class="description"><?php _e('Set color for box gallery thumbnails.') ?></p>
											<script type="text/javascript">
												jQuery(document).ready(function($){
												    $("#boxColor").wpColorPicker();
												});
											 </script>
											</td>
										</tr> 
										<!-- // Box color -->
										<tr valign="top">
											<th scope="row"><label for="Layout"><?php _e('Layout') ?></label></th>
											<td>
											<select name="pg_settings[layout]">
											<?php foreach ($this->layout as $key => $value): ?>
												<?php if( $key ==  pg_get_option('layout','5') ): ?>
												<option selected="selected" value=<?php echo $key; ?>><?php echo $value; ?></option>
												<?php else: ?>
												<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
												<?php endif;?>
											<?php endforeach;?>
											</select>
											<p class="description"><?php _e('Set the albums layout ') ;?></p>
											</td>							
										</tr>
										<!-- // Layout -->
										<tr valign="top">
											<th scope="row"><label for="show_desc"><?php _e('Show Description') ?></label></th>
											<td>
											<select name="pg_settings[show_desc]">
											<?php foreach ($this->show_desc as $key => $value): ?>
												<?php if( $key ==  pg_get_option('show_desc') ): ?>
												<option selected="selected" value=<?php echo $key; ?>><?php echo $value; ?></option>
												<?php else: ?>
												<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
												<?php endif;?>
											<?php endforeach;?>
											</select>
											<p class="description"><?php _e('Show / Hide image\'s description') ;?></p>
											</td>							
										</tr>
										<!-- // show description -->							
										<tr valign="top">
											<th scope="row"><label for="desc_length"><?php _e('Description Length') ?></label></th>
											<td>
											<input id="caplength" name="tnpg[desc_length]" value="<?php echo pg_get_option('desc_length',500);?>" type="text" placeholder="500"/>
											<p class="description"><?php _e('Length of description displayed. Default is 500 character limit.') ?></p>
											</td>							
										</tr>
										<!-- // caplength -->
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Width (px)') ?></label></th>
											<td>
											<input id="width" name="pg_settings[width]" value="<?php echo pg_get_option('width','');?>" type="text"/>
											<p class="description"><?php _e('Width of Plus Gallery. Default is 100%.') ?></p>
											</td>							
										</tr>
										<!-- // Plus gallery width -->
										
									</tbody>
								</table>
							
							</div>
							<!-- wpplOptions tab -->
							<div class="tab-pane fade" id="wpplOptions">
								<table class="form-table">
									<tbody>
										<tr valign="top">
											<th scope="row"><label for="show_cat"><?php _e('Show Categories') ?></label></th>
											<td>
											<select name="pg_settings[show_cat]">
											<?php foreach ($this->show_cat as $key => $value): ?>
												<?php if( $key ==  pg_get_option('show_cat','show') ): ?>
												<option selected="selected" value=<?php echo $key; ?>><?php echo $value; ?></option>
												<?php else: ?>
												<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
												<?php endif;?>
											<?php endforeach;?>
											</select>
											<p class="description"><?php _e('Show / Hide Categories and filter albums ') ;?></p>
											</td>							
										</tr>
										<!-- Show Categories -->
										<tr valign="top">
											<th scope="row"><label for="show_caption"><?php _e('Show Caption') ?></label></th>
											<td>
											<select name="pg_settings[show_caption]">
											<?php foreach ($this->show_caption as $showCaption => $caption): ?>
												<?php if( $showCaption ==  pg_get_option('show_caption') ): ?>
												<option selected="selected" value=<?php echo $showCaption; ?>><?php echo $caption; ?></option>
												<?php else: ?>
												<option value="<?php echo $showCaption; ?>"><?php echo $caption; ?></option>
												<?php endif;?>
											<?php endforeach;?>
											</select>
											<p class="description"><?php _e('Show / Hide the caption ') ;?></p>
											</td>							
										</tr>
										<!-- // show caption -->
										<tr valign="top">
											<th scope="row"><label for="wp_albumid"><?php _e('Album ID') ?></label></th>
											<td>
											<input id="wp_albumid" name="pg_settings[wp_albumid]" value="<?php echo pg_get_option('wp_albumid','');?>" type="text"/>
											<p class="description"><?php _e('Adding the album id puts plus gallery into single album mode.')?></p>
											</td>							
										</tr>
										<!-- // Album ID -->
										<tr valign="top">
											<th scope="row"><label for="wp_albumlimit"><?php _e('Album Limit') ?></label></th>
											<td>
											<input id="wp_albumlimit" name="pg_settings[wp_albumlimit]" value="<?php echo pg_get_option('wp_albumlimit',20);?>" type="text"/>
											<p class="description"><?php _e('An integer to limit the number of albums intially loaded. defaults to 20.')?></p>
											</td>							
										</tr>
										<!-- // Album Limit -->
										
										<tr valign="top">
											<th scope="row"><label for="wp_include"><?php _e('Album IDs Include') ?></label></th>
											<td>
											<textarea id="wp_include" name="pg_settings[wp_include]" cols="22" rows="2"><?php echo pg_get_option('wp_include');?></textarea>									
											<p class="description"><?php _e('Example (Album a,Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space). If leaving it blank, default is getting all your albums.') ?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="wp_exclude"><?php _e('Album IDs Exclude') ?></label></th>
											<td>
											<textarea id="wp_exclude" name="pg_settings[wp_exclude]" cols="22" rows="2"><?php echo pg_get_option('wp_exclude');?></textarea>									
											<p class="description"><?php _e('Example (Album a, Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space) of excluded albums. If leaving it blank, default is getting all your albums.')?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="wp_limit"><?php _e('Limit') ?></label></th>
											<td>
											<input id="wp_limit" name="pg_settings[wp_limit]" value="<?php echo pg_get_option('wp_limit',20);?>" type="text"/>										
											<p class="description"><?php _e('An integer to limit individual Gallery results by. Default to 20.') ?></p>
											</td>							
										</tr>
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Shortcode') ?></label></th>
											<td>
											<pre>[wp_plusgallery data_type="wp_gallery"]</pre>
											<p class="description"><?php _e('This shortcode to use for Wordpress album') ?></p>
											</td>							
										</tr>
										
									</tbody>
								</table>
							
							</div>
							
							<!-- Facebook tab -->
							<div class="tab-pane fade " id="facebook">
								<table class="form-table">
									<tbody>									
										<tr valign="top">
											<th scope="row"><label for="fbUserID"><?php _e('Facebook ID') ?></label></th>
											<td>
											<input id="fbUserID" name="pg_settings[fb_userid]" value="<?php echo pg_get_option('fb_userid','');?>" type="text"/>
											<p class="description"><?php _e('Facebook fan page ID')?></p>
											</td>							
										</tr>
										<!-- // fbUserID -->
										<?php /*<tr valign="top">
											<th scope="row"><label for="fbGelleryMode"><?php _e('Gallery Mode') ?></label></th>
											<td>
											<select name="tnwf[fb_gellerymode]" id="fbGelleryMode">
												<option <?php echo tnwf_get_option('fb_gellerymode','showall') == 'showall' ? 'selected="selected"':''?> value="showall">Show All</option>
												<option <?php echo tnwf_get_option('fb_gellerymode','showall') == 'include' ? 'selected="selected"':''?> value="include">Include Albums</option>
												<option <?php echo tnwf_get_option('fb_gellerymode','showall') == 'exclude' ? 'selected="selected"':''?> value="exclude">Exclude Albums</option>
												<option <?php echo tnwf_get_option('fb_gellerymode','showall') == 'single' ? 'selected="selected"':''?> value="single">Single Album</option>
											</select>
											</td>							
										</tr>
										<!-- // Plus gallery mode -->*/?>
										<tr valign="top">
											<th scope="row"><label for="fb_albumid"><?php _e('Album ID') ?></label></th>
											<td>
											<input id="fb_albumid" name="pg_settings[fb_albumid]" value="<?php echo pg_get_option('fb_albumid','');?>" type="text"/>
											<p class="description"><?php _e('Adding the album id puts plus gallery into single album mode.')?></p>
											</td>							
										</tr>
										<!-- // Album ID -->
										<tr valign="top">
											<th scope="row"><label for="fb_albumlimit"><?php _e('Album Limit') ?></label></th>
											<td>
											<input id="fb_albumlimit" name="pg_settings[fb_albumlimit]" value="<?php echo pg_get_option('fb_albumlimit',20);?>" type="text"/>
											<p class="description"><?php _e('An integer to limit the number of albums intially loaded. defaults to 20.')?></p>
											</td>							
										</tr>
										<!-- // Album Limit -->
										
										<tr valign="top">
											<th scope="row"><label for="fb_include"><?php _e('Album IDs Include') ?></label></th>
											<td>
											<textarea id="fb_include" name="pg_settings[fb_include]" cols="22" rows="2"><?php echo pg_get_option('fb_include');?></textarea>									
											<p class="description"><?php _e('Example (Album a,Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space). If leaving it blank, default is getting all your albums.') ?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="fb_exclude"><?php _e('Album IDs Exclude') ?></label></th>
											<td>
											<textarea id="fb_exclude" name="pg_settings[fb_exclude]" cols="22" rows="2"><?php echo pg_get_option('fb_exclude');?></textarea>									
											<p class="description"><?php _e('Example (Album a, Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space) of excluded albums. If leaving it blank, default is getting all your albums.')?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="fb_limit"><?php _e('Limit') ?></label></th>
											<td>
											<input id="fb_limit" name="pg_settings[fb_limit]" value="<?php echo pg_get_option('fb_limit',20);?>" type="text"/>										
											<p class="description"><?php _e('An integer to limit individual Gallery results by. Default to 20.') ?></p>
											</td>							
										</tr>
										<!-- // Limit -->
										<tr valign="top">
											<th scope="row"><label>Tip:</label></th>
											<td>
												<p class="description">
													To get Album ID click here: <a href="https://developers.facebook.com/tools/explorer?method=GET&path=naturephotobook%3Ffields%3Dalbums.fields(id%2Cname)&version=v2.0" target="_blank">https://developers.facebook.co...</a> Then change the 'naturephotobook' by your Facebook fan page ID and you'll get all your album id.
												</p>
											</td>
										</tr>
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Shortcode') ?></label></th>
											<td>
											<pre>[wp_plusgallery data_type="facebook"]</pre>
											<p class="description"><?php _e('This shortcode to use for Facebook gallery') ?></p>
											</td>							
										</tr>
									</tbody>
								</table>
							
							</div>
							
							<!-- instagram tab -->
							<div class="tab-pane fade " id="instagram">
								<table class="form-table">
									<tbody>									
										<tr valign="top">
											<th scope="row"><label for="instaUserID"><?php _e('Instagram ID') ?></label></th>
											<td>
											<input id="instaUserID" name="pg_settings[insta_userid]" value="<?php echo pg_get_option('insta_userid','');?>" type="text"/>
											<p class="description"><?php _e('Instagram ID is the numeric represents your account.')?></p>
											</td>							
										</tr>
										<!-- // fbUserID -->
	
										<tr valign="top">
											<th scope="row"><label for="insta_limit"><?php _e('Limit') ?></label></th>
											<td>
											<input id="insta_limit" name="pg_settings[insta_limit]" value="<?php echo pg_get_option('insta_limit',20);?>" type="text"/>										
											<p class="description"><?php _e('An integer to limit individual Gallery results by. Default to 20.') ?></p>
											</td>							
										</tr>
										<!-- // Limit -->
										
										<tr valign="top">
											<th scope="row"><label>Tip:</label></th>
											<td>
												<p class="description">
													<a href='http://jelled.com/instagram/lookup-user-id' target='_blank'>Lookup Instagram User ID here</a>. Then copy and paste your Instagram User ID into 'Instagram ID'.
												</p>
											</td>
										</tr>
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Shortcode') ?></label></th>
											<td>
											<pre>[wp_plusgallery data_type="instagram"]</pre>
											<p class="description"><?php _e('This shortcode to use for Instagram gallery') ?></p>
											</td>							
										</tr>
									</tbody>
								</table>
							
							</div>
							
							<!-- flickr tab -->
							<div class="tab-pane fade " id="flickr">
								<table class="form-table">
									<tbody>									
										<tr valign="top">
											<th scope="row"><label for="flUserID"><?php _e('Flickr UserID') ?></label></th>
											<td>
											<input id="flUserID" name="pg_settings[fl_userid]" value="<?php echo pg_get_option('fl_userid','');?>" type="text"/>
											<p class="description"><a href="https://www.flickr.com/services/api/explore/flickr.photos.search" target="_blank">Flickr Api Explorer</a> Login to Flickr you'll see it in the top right. Ex: <a>24662369@N07</a></p>
											</td>							
										</tr>
										<!-- // flUserID -->
										
										<tr valign="top">
											<th scope="row"><label for="fl_albumid"><?php _e('Album ID') ?></label></th>
											<td>
											<input id="fl_albumid" name="pg_settings[fl_albumid]" value="<?php echo pg_get_option('fl_albumid','');?>" type="text"/>
											<p class="description"><?php _e('Adding the album id puts plus gallery into single album mode.')?></p>
											</td>							
										</tr>
										<!-- // Album ID -->
										<tr valign="top">
											<th scope="row"><label for="fl_albumlimit"><?php _e('Album Limit') ?></label></th>
											<td>
											<input id="fb_albumlimit" name="pg_settings[fl_albumlimit]" value="<?php echo pg_get_option('fl_albumlimit',20);?>" type="text"/>
											<p class="description"><?php _e('An integer to limit the number of albums intially loaded. defaults to 20.')?></p>
											</td>							
										</tr>
										<!-- // Album Limit -->
										
										<tr valign="top">
											<th scope="row"><label for="fl_include"><?php _e('Album IDs Include') ?></label></th>
											<td>
											<textarea id="fl_include" name="pg_settings[fl_include]" cols="22" rows="2"><?php echo pg_get_option('fl_include');?></textarea>									
											<p class="description"><?php _e('Example (Album a,Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space). If leaving it blank, default is getting all your albums.') ?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="fl_exclude"><?php _e('Album IDs Exclude') ?></label></th>
											<td>
											<textarea id="fl_exclude" name="pg_settings[fl_exclude]" cols="22" rows="2"><?php echo pg_get_option('fl_exclude');?></textarea>									
											<p class="description"><?php _e('Example (Album a, Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space) of excluded albums. If leaving it blank, default is getting all your albums.')?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="fl_limit"><?php _e('Limit') ?></label></th>
											<td>
											<input id="fl_limit" name="pg_settings[fl_limit]" value="<?php echo pg_get_option('fl_limit',20);?>" type="text"/>										
											<p class="description"><?php _e('An integer to limit individual Gallery results by. Default to 20.') ?></p>
											</td>							
										</tr>
										<!-- // Limit -->
										<tr valign="top">
											<th scope="row"><label>Tip:</label></th>
											<td>
												<p class="description">
													To get Album ID, you sign in your Flickr account then click <b>Sets</b> button and click album that you want to get ID then copy Album Id from address url. EX: http://www.flickr.com/photos/24662369@N07/sets/<a>72157644221437096</a>
												</p>
											</td>
										</tr>
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Shortcode') ?></label></th>
											<td>
											<pre>[wp_plusgallery data_type="flickr"]</pre>
											<p class="description"><?php _e('This shortcode to use for Flickr gallery') ?></p>
											</td>							
										</tr>
									</tbody>
								</table>
							</div>
							
							<!-- google tab -->
							<div class="tab-pane fade " id="google">
								<table class="form-table">
									<tbody>									
										<tr valign="top">
											<th scope="row"><label for="ggUserID"><?php _e('Google+ UserID') ?></label></th>
											<td>
											<input id="ggUserID" name="pg_settings[gg_userid]" value="<?php echo pg_get_option('gg_userid','');?>" type="text"/>
											<p class="description">The numeric represents your account . Ex: https://plus.google.com/<a>102738297300406129784</a><br/>Google+ UserID is: '102738297300406129784'.</p>
											</td>							
										</tr>
										<!-- // flUserID -->
										
										<tr valign="top">
											<th scope="row"><label for="gg_albumid"><?php _e('Album ID') ?></label></th>
											<td>
											<input id="gg_albumid" name="pg_settings[gg_albumid]" value="<?php echo pg_get_option('gg_albumid','');?>" type="text"/>
											<p class="description"><?php _e('Adding the album id puts plus gallery into single album mode.')?></p>
											</td>							
										</tr>
										<!-- // Album ID -->
										<tr valign="top">
											<th scope="row"><label for="gg_albumlimit"><?php _e('Album Limit') ?></label></th>
											<td>
											<input id="gg_albumlimit" name="pg_settings[gg_albumlimit]" value="<?php echo pg_get_option('gg_albumlimit',20);?>" type="text"/>
											<p class="description"><?php _e('An integer to limit the number of albums intially loaded. defaults to 20.')?></p>
											</td>							
										</tr>
										<!-- // Album Limit -->
										
										<tr valign="top">
											<th scope="row"><label for="gg_include"><?php _e('Album IDs Include') ?></label></th>
											<td>
											<textarea id="gg_include" name="pg_settings[gg_include]" cols="22" rows="2"><?php echo pg_get_option('gg_include');?></textarea>									
											<p class="description"><?php _e('Example (Album a,Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space). If leaving it blank, default is getting all your albums.') ?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="gg_exclude"><?php _e('Album IDs Exclude') ?></label></th>
											<td>
											<textarea id="gg_exclude" name="pg_settings[gg_exclude]" cols="22" rows="2"><?php echo pg_get_option('gg_exclude');?></textarea>									
											<p class="description"><?php _e('Example (Album a, Album b):<a>111111111,222222222</a> .There should be a comma to separate between IDs (no space) of excluded albums. If leaving it blank, default is getting all your albums.')?></p>
											</td>							
										</tr>
										<!-- // Include -->
										
										<tr valign="top">
											<th scope="row"><label for="gg_limit"><?php _e('Limit') ?></label></th>
											<td>
											<input id="gg_limit" name="pg_settings[gg_limit]" value="<?php echo pg_get_option('gg_limit',20);?>" type="text"/>										
											<p class="description"><?php _e('An integer to limit individual Gallery results by. Default to 20.') ?></p>
											</td>							
										</tr>
										<!-- // Limit -->
										<tr valign="top">
											<th scope="row"><label>Tip:</label></th>
											<td>
												<p class="description">
													To get Album ID, You sign in your Google+ account then click album that shared publicly then copy Album ID from address url. EX: https://plus.google.com/photos/102738297300406129784/albums/<b><a>6017377255028367361</a></b>
												</p>
											</td>
										</tr>
										<tr valign="top">
											<th scope="row"><label for="width"><?php _e('Shortcode') ?></label></th>
											<td>
											<pre>[wp_plusgallery data_type="google"]</pre>
											<p class="description"><?php _e('This shortcode to use for Google gallery') ?></p>
											</td>							
										</tr>
									</tbody>
								</table>
							</div>
							<!-- #plAdvancedOptions -->
							<div class="tab-pane fade " id="plAdvancedOptions">
								<table class="form-table">
									<tbody>									
																			
										<tr valign="top">
											<th scope="row"><label for="customCSS"><?php _e('Custom CSS') ?></label></th>
											<td>
											<textarea id="customCSS" name="pg_settings[custom_css]" cols="40" rows="4"><?php echo pg_get_option('custom_css','')?></textarea>
											<p class="description"><?php _e('Paste your CSS code.<br/>If you want to add some custom CSS code to the plugin and don\'t want to modify any files, then it\'s a good place to enter your code at this field.') ?></p>
											</td>							
										</tr>
										<!-- // Custom css -->
									</tbody>
								</table>
							</div>
						</div> 
						
					</div> <!-- //Tab options -->
				</form>
			</div>		
			
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$('#PgTab a:first').tab('show');
					
				});
			</script>
		<?php 
	}
	
	
	// End Class
}
new PG_Admin();