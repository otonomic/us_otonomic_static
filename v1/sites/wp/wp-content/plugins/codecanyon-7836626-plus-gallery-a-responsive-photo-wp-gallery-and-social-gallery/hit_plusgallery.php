<?php
/*
 * Plugin Name: Plus Gallery
 * URI: 
 * Description: A responsive photo gallery that helps you display Wordpress gallery and Social gallery like <strong>Facebook</strong>, <strong>Instagram</strong>, <strong>Flickr</strong> or <strong>Google Plus</strong>.
 * Version: 2.0.2
 * Author: TNT-Team 
 * URI: 
 * Copyright @2014 by TNT-Team
 * License: License GNU General Public License version 2 or later
 */

// don't load directly
if (!defined('ABSPATH')) die('-1');

// Current Plus Gallery version
if (!defined('PLUSGALLERY_VER'))
	define( 'PLUSGALLERY_VER', '2.0.2' );

if (!defined('HT_PLUSGALLERY'))
	define('HT_PLUSGALLERY','wp_plusgallery');

//Register post type
include_once 'includes/pg-register-post-type.php';

class PG_Manager{
	
	protected $plugin_dir;
	public $plugin_url;	
	
	
	public function __construct(){
		$this->plugin_dir = plugin_dir_path(__FILE__);
		$this->plugin_url = plugin_dir_url(__FILE__);		
		
		add_action('init', array(&$this,'init'));
		
		add_action('wp_ajax_pg_plusgallery_album', array($this,'pg_plusgallery_album_ajax'));
		add_action('wp_ajax_nopriv_pg_plusgallery_album', array($this,'pg_plusgallery_album_ajax'));
	}
	
	public function init(){
		include_once $this->plugin_dir.'includes/function.php';
		
		if(is_admin()){
			include_once $this->plugin_dir.'includes/admin.php';
			include_once $this->plugin_dir.'includes/meta-box.php';
		}else{
			add_action('wp_enqueue_scripts',array(&$this,'enqueue_styles')); // add style
			add_action('wp_enqueue_scripts',array(&$this,'enqueue_scripts')); // add script
			add_shortcode('wp_plusgallery', array(&$this,'PG_Shortcode_Func'));// PG shotcode
		}
	}
	
	public function enqueue_styles(){
		// register style sheet
		wp_register_style('tnpg_style', $this->plugin_url.'plusgallery.css','',PLUSGALLERY_VER);
		wp_enqueue_style('tnpg_style');
	}
	
	public function enqueue_scripts(){
 		wp_enqueue_script('jquery');
 		wp_enqueue_script('plus_isotope', $this->plugin_url.'assets/js/isotope.pkgd.min.js');
 		wp_enqueue_script('imagesloaded', $this->plugin_url.'assets/js/imagesloaded.pkgd.min.js');
 		wp_register_script('plusgallery_js', $this->plugin_url.'assets/js/plusgallery.js','',PLUSGALLERY_VER);
 		
                wp_localize_script( 'plusgallery_js', 'ajaxObj', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
                wp_enqueue_script('plusgallery_js');
 		
	}
	
	public function pg_plusgallery_album_ajax(){
		// Register Plus WP gallery image size
		//add_image_size('plus_wp_gallery_thumb',500,500,true);
		$args = array(
			'numberposts' => 0, 'offset' => 0,
			'category' => 0, 'orderby' => 'date',
			'order' => 'DESC', 'include' => array(),
			'exclude' => array(), 'meta_key' => '',
			'meta_value' =>'', 'post_type' => 'plus_wpgallery',
			'suppress_filters' => true
		);
		
		$data = $_GET['data']; // data gallery
		
		$myposts = get_posts( $args );
		$dataAlbums = array();
		$dataImages = array();
		
		foreach ( $myposts as $post ) :
			// Return data album
			$slug = get_post_meta($post->ID,'_ht_plus_wpcat',true);
			$imageURL = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
			$imageURL[0] = isset($imageURL[0]) ? $imageURL[0] : $this->plugin_url . 'assets/images/image-null.png';
			$dataAlbums []= array(
						"id" => $post->ID,
						"name" => $post->post_title,
						"galleryImage" => $imageURL[0],
						"link" => admin_url( 'admin-ajax.php', 'relative' )."?action=pg_plusgallery_album&data=images&id=".$post->ID,
						"dataCategory"	=> $slug, 
				);
		endforeach;
		
		if($data == 'albums'){
			wp_send_json($dataAlbums);// Return data albums
		}else{
			$albumID = $_GET["id"];
			// Return data images
			$attachment_ids = get_post_meta($albumID,'_ht_plus_wpgallery',true);
			//$attachment_ids = get_post_meta($albumID,'plus_wpgallery',true);
			$attachment_ids = explode(',',$attachment_ids);
			foreach ($attachment_ids as $attachment_id):
				$attachment = get_post($attachment_id) ;
				$imgObjArr 			= array();
				$imgObj 			= new stdClass();
				$imgObj->caption 	= $attachment->post_excerpt == '' ? '<br/>': $attachment->post_excerpt;
				$imgObj->desc 		= $attachment->post_content;
				$imgObj->height 	= 'auto';
				$imgObj->source 	= wp_get_attachment_url($attachment->ID);
				$imgObj->width 		= '100%';
				$dataImages[] 		= $imgObj;
			endforeach;
			wp_send_json(array('data'=>$dataImages)); // Return data images
			
		}
	}
	
	/* Shortcode */
	public function PG_Shortcode_Func($atts){
		//[wp_plusgallery data_type="" data_userid="" data_albumid="" data_albumlimit="" data_include="" data_exclude="" data_limit="" box_color="" show_caption="" show_desc="" desc_length=""]
		// Get Setting options
		$plusSettings = array(
			'box_color' => pg_get_option('box_color','#38beea'),
			'layout' => pg_get_option('layout','5'),
			'show_desc' => pg_get_option('show_desc','show'),
			'desc_length' => pg_get_option('desc_length',500),
			'width' => pg_get_option('width','') == 0 ? '' : (int)pg_get_option('width',''),
			
			// WP Options
			'show_cat' => pg_get_option('show_cat','show'),
			'show_caption' => pg_get_option('show_caption','show'),
			
			// Advanced Options
			'custom_css' => pg_get_option('custom_css',''),
		);

		
		// Get data attribute and set default atts
		extract( shortcode_atts(array(
		'data_type' 		=> 'facebook',
		'data_userid' 		=> '',
		'album_mode'		=> 'show_all',
		'data_albumid' 		=> '',
		'data_albumlimit' 	=> '',
		'data_include' 		=> '',
		'data_exclude' 		=> '',
		'data_limit' 		=> '',
		'box_color'	 		=> $plusSettings['box_color'],
		'show_caption' 		=> $plusSettings['show_caption'],
		'show_desc'			=> $plusSettings['show_desc'],
		'desc_length' 		=> $plusSettings['desc_length'],
		), $atts));
		
		// Return HTML
		$html ='';
			if ($data_type == 'wp_gallery' && $plusSettings['show_cat'] == 'show'){
				//Display category
				$taxonomy     = 'plus_wpgallery_cat';
				$orderby      = 'name';
				$show_count   = 0;      // 1 for yes, 0 for no
				$pad_counts   = 0;      // 1 for yes, 0 for no
				$hierarchical = 1;      // 1 for yes, 0 for no
				$title        = '';
				
				$args = array(
						'taxonomy'     => $taxonomy,
						'orderby'      => $orderby,
						'show_count'   => $show_count,
						'pad_counts'   => $pad_counts,
						'hierarchical' => $hierarchical,
						'title_li'     => $title,
						'hide_empty'	=> false,
				);
				$html .= '<div class="plus-category filters">';
				$html .= '<p>';
				$html .= '<span data-category="*" class="active">All</span>';
				$categories = get_categories($args);
				foreach ($categories as $category){
					$html .= '<span data-category=".'.$category->slug.'">/&nbsp;&nbsp;'.$category->name.'</span>';
				}
				$html .= '</p>';
				$html .= '</div>';		
			}
			
			$html .= '<div id="TNTPlusgalleryWrapper" style="width:'.$plusSettings['width'].'px'.'">';
			//<!-- Message -->
			$html .= '<div class="tnpg_message"></div>';
			$html .= '<div id="plusgallery" ';
			switch ($data_type){
				case 'wp_gallery':
					$html .='class="plus-wp-gallery"';
					$html .=' data-type="wp_gallery" ';
					$wp_albumID = ($data_albumid != '' ? $data_albumid : pg_get_option('wp_albumid',''));
					if( $wp_albumID ){
						$html .=' data-album-id="'. $wp_albumID .'"';
						$html .=' data-album-url="'.admin_url( 'admin-ajax.php', 'relative' ).'?action=pg_plusgallery_album&data=images&id='.$wp_albumID .'"';
					}else{
						$html .=' data-album-limit="'. ($data_albumlimit != '' ? $data_albumlimit : pg_get_option('wp_albumlimit','20')).'"';
						$html .=' data-include="'. ($data_include != '' ? $data_include : pg_get_option('wp_include','')).'"';
						$html .=' data-exclude="'. ($data_exclude != '' ? $data_exclude : pg_get_option('wp_exclude','')).'"';
						$html .=' data-limit="'. ($data_limit != '' ? $data_limit : pg_get_option('wp_limit','20')).'"';
						$html .=' data-album-url="'.admin_url( 'admin-ajax.php', 'relative' ).'?action=pg_plusgallery_album&data=albums"';
					}
					
					break;
				case 'facebook':
					$html .=' data-type="facebook" ';
					$html .=' data-userid="'. ($data_userid != '' ? $data_userid : pg_get_option('fb_userid','')).'"';
					$html .=' data-album-id="'. ($data_albumid != '' ? $data_albumid : pg_get_option('fb_albumid','')).'"';
					$html .=' data-album-limit="'. ($data_albumlimit != '' ? $data_albumlimit : pg_get_option('fb_albumlimit','20')).'"';
					$html .=' data-include="'. ($data_include != '' ? $data_include : pg_get_option('fb_include','')).'"';
					$html .=' data-exclude="'. ($data_exclude != '' ? $data_exclude : pg_get_option('fb_exclude','')).'"';
					$html .=' data-limit="'. ($data_limit != '' ? $data_limit : pg_get_option('fb_limit','20')).'"';
					break;
				case 'instagram':
					$html .=' data-type="instagram"';
					$html .=' data-userid="'. ($data_userid != '' ? $data_userid : pg_get_option('insta_userid','')).'"';
					$html .=' data-limit="'. ($data_limit != '' ? $data_limit : pg_get_option('insta_limit','')).'"';
					break;
				case 'flickr':
					$html .=' data-type="flickr"';
					$html .=' data-userid="'. ($data_userid != '' ? $data_userid : pg_get_option('fl_userid','')).'"';
					$html .=' data-album-id="'. ($data_albumid != '' ? $data_albumid : pg_get_option('fl_albumid','')).'"';
					$html .=' data-album-limit="'. ($data_albumlimit != '' ? $data_albumlimit : pg_get_option('fl_albumlimit','20')).'"';
					$html .=' data-include="'. ($data_include != '' ? $data_include : pg_get_option('fl_include','')).'"';
					$html .=' data-exclude="'. ($data_exclude != '' ? $data_exclude : pg_get_option('fl_exclude','')).'"';
					$html .=' data-limit="'. ($data_limit != '' ? $data_limit : pg_get_option('fl_limit','20')).'"';
					break;
				case 'google':
					$html .=' data-type="google"';
					$html .=' data-userid="'. ($data_userid != '' ? $data_userid : pg_get_option('gg_userid','')).'"';
					$html .=' data-album-id="'. ($data_albumid != '' ? $data_albumid : pg_get_option('gg_albumid','')).'"';
					$html .=' data-album-limit="'. ($data_albumlimit != '' ? $data_albumlimit : pg_get_option('gg_albumlimit','20')).'"';
					$html .=' data-include="'. ($data_include != '' ? $data_include : pg_get_option('gg_include','')).'"';
					$html .=' data-exclude="'. ($data_exclude != '' ? $data_exclude : pg_get_option('gg_exclude','')).'"';
					$html .=' data-limit="'. ($data_limit != '' ? $data_limit : pg_get_option('gg_limit','20')).'"';
					break;
				default:
			}
			$html .= 'data-image-path="'.$this->plugin_url.'assets/images/plusgallery"></div></div>';
			$html .= '<script type="text/javascript">';
			$html .= 'var TNPGOptions = {';
			if($desc_length) $html .= 'descLength: '.$desc_length.',';
			$html .= '};';
			$html .= 'jQuery("#plusgallery").plusGallery();';
			$html .= '</script>';
			$html .= '<style type="text/css">';
			$html .=' /*Set color for box gallery thumbnails. */';
			$html .=' #plusgallery a:hover, #pgzoomview a:hover, .pgalbumlink:hover .pgplus, #pgthumbcrumbs li#pgthumbhome:hover {';
			$html .='background-color: '.$box_color ;
			$html .='}';
			if ( $show_caption == 'no' || $show_caption == '') $html .='#pgthumbs .pgalbumtitle{display:none;}'; // Hide image's caption
			if ( $show_desc == 'no' || $show_desc == '') $html .=' .pgzoomcaption{display:none;}'; // Hide image's description
			// Set the albums layout
			switch ($plusSettings['layout']){
				case 3:
					$html .='.pgalbumthumb, .pgthumb {width:33.33% !important;}';
					break;
				case 4:
					$html .='.pgalbumthumb, .pgthumb {width:25% !important;}';
					break;
				default:
					
			}
			
			$html .='/*Custom css template */';
			$html .= trim($plusSettings['custom_css']);
			$html .='</style>';
			
			
			if (function_exists( 'wpb_js_remove_wpautop' ))
			$html = wpb_js_remove_wpautop($html, true);  //fix unclosed/unwanted paragraph tags in $content
		
			$output ="<div>{$html}</div>";
			
			
		return $html;		
	}
}

global $PG_Manager;
$PG_Manager = new PG_Manager;
/*
 * Add Plus Gallery to Visaul composer
 */
include_once plugin_dir_path(__FILE__). 'vc-plusgallery/vc_plusgallery.php';