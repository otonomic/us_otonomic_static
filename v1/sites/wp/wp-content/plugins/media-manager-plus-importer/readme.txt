=== Media Manager Plus: Importer ===
Contributors: dev7studios, polevaultweb
Tags: import, importer media, manager, 500px, flickr, instagram, dribbble, image, images
Requires at least: 3.4
Tested up to: 3.5.1
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A premium extension to the Media Manager Plus plugin to allow you to import images from third party sources in your media library when inserting into posts or pages.

== Description ==

The Media Manager Plus plugin allows your to easily browse images from third party image sources directly from the media manager popup. Once you have selected an image to insert, this plugin adds a new checkbox to the side bar of options that imports the image into you media library and inserts the new WordPress url for the image into your post or page.

The plugin also adds a new setting to the Media Manager Plus' General Settings where you can control if the Import checkbox is checked by default in the media popup.

This is a great way to save your images to your site. 

== Installation ==

Simple Install
1. Login to WordPress and go to Plugins > Add New > Upload
2. Upload media-manager-plus-importer.zip and activate


Manual Install
1. Upload the media-manager-plus-importer folder to your /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
