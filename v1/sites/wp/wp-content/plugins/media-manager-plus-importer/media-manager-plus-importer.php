<?php
/*  
Plugin Name: Media Manager Plus: Importer
Plugin URI: http://dev7studios.com/media-manager-plus/importer
Description: A premium extension to the Media Manager Plus plugin to allow you to import images from third party sources in your media library.
Author: Dev7studios 
Version: 1.0
Author URI: http://dev7studios.com
*/

class media_manager_plus_importer {
	private $required;
	
	function __construct() {
		$this->required = 1.2;
		
		register_activation_hook( __FILE__, array($this, 'activate') );
		
		global $uber_media;
		add_action( 'uber_media_settings_before', array( $this, 'print_media_templates' ) );
		add_filter( 'uber_media_pre_insert', array( $this, 'uber_media_pre_insert' ) );
		add_filter( 'uber_media_settings' , array( $this, 'uber_media_settings' ) );
		add_filter('mmp_default_settings', array($this,'mmp_default_settings'));
		
		require_once('wp-updates-plugin.php');
        new WPUpdatesPluginUpdater( 'http://wp-updates.com/api/1/plugin', 174, plugin_basename(__FILE__) );
	}
			
	function activate( $network_wide ) {
		$mmp_path = 'uber-media/uber-media.php';
		
		if (!is_plugin_active( $mmp_path )) {
			deactivate_plugins(basename(__FILE__));
			wp_die("This plugin requires the Media Manager Plus plugin to be installed and activated"); 
		}
				
		$default_headers = array( 'Version' => 'Version',  'Name' => 'Plugin Name');
		$plugin_data = get_file_data( dirname( dirname(__FILE__) ) .'/'. $mmp_path, $default_headers, 'plugin' );

		if (version_compare($plugin_data['Version'], $this->required, '<'))  {
			deactivate_plugins(basename(__FILE__));
			wp_die('This plugin requires Media Manager Plus version '. $this->required .' to be installed and activated'); 
		}
	}
	
	function mmp_default_settings($settings){
		$uber_options = get_option('ubermediasettings_settings', array());
		if ($uber_options) {
			$value = 'ubermediasettings_general_import-default';
			$import_default = ( !isset($uber_options[$value]) ) ? 0 : $uber_options[$value];
			$checked = ($import_default == 1) ? 'on' : 'off';
		}
		$settings['setting-import'] = $checked;
		return $settings;
	}
	
	
	function print_media_templates() {
		$uber_options = get_option('ubermediasettings_settings', array());
		if ($uber_options) {
			$value = 'ubermediasettings_general_import-default';
			$import_default = ( !isset($uber_options[$value]) ) ? 0 : $uber_options[$value];
			$checked = ($import_default == 1) ? 'checked="checked"' : '';
		}
	?>	
		<label class="setting alt-text">
			<span><?php _e('Import and Insert From Media Library'); ?></span>
			<input type="checkbox" name="uber-upload" data-setting="import" value="on" <?php echo $checked; ?>>
		</label>	
	<?php
	}
	
	function uber_media_pre_insert($response) {
		if (!isset($response['fields']['setting-import'])) return $response;
		if (!isset($response['fields']['id'])) return $response;
		if (!isset($response['imgsrc'])) return $response;
		if ($response['fields']['setting-import'] == 'off') return $response;
		
		$upload_src = $this->attach_image($response['imgsrc'], $response['fields']['title'], $response['fields']['postid']);
		if($upload_src != '') $response['imgsrc'] = $upload_src;
		return $response;
	}
	
	function uber_media_settings($wpsf_ubermedia_settings) {
		$fields = $wpsf_ubermedia_settings[2]['fields'];
		$fields[] = 	array(
						            'id' => 'import-default',
						            'title' => __( 'Import Images Default' ),
						            'desc' => __( 'Controls the default state of the Import checkbox in the image details sidebar' ),
						            'type' => 'checkbox',
						            'std' => 0
						        );
						        
		$wpsf_ubermedia_settings[2]['fields'] = $fields;

		return $wpsf_ubermedia_settings;
	}
	 
	function attach_image($url, $title, $post_id, $return_src = true) {
		$tmp = download_url( $url );
	
		$file = basename( $url );
		$info = pathinfo($file);
	
		$file_name = ($title == '') ? $file : $title;
		$file_name = sanitize_file_name($file_name);
		$file_name = remove_accents($file_name);
		$file_name = $file_title = substr($file_name, 0, 100);
		$file_name = $file_name .'.'. (isset($info['extension']) ? $info['extension'] : 'jpg');
	    $file_array = array(
	        'name' => $file_name,
	        'tmp_name' => $tmp
	    );
	
		if ( is_wp_error( $tmp ) ) {
			@unlink($file_array['tmp_name']);
			$file_array['tmp_name'] = '';
			return '';
		}
	
		$id = media_handle_sideload( $file_array, $post_id, $file_title );
	
		if ( is_wp_error($id) ) {
			@unlink($file_array['tmp_name']);
			return '';
		}
		if ($return_src) {
			$src = wp_get_attachment_url( $id );
			return $src;
		} else return $id;
	
	}
}
$media_manager_plus_importer = new media_manager_plus_importer();
