<?php
/*
Controller name: Panelwidgets
Controller description: Widgets Methods
*/
class JSON_API_Panelwidgets_Controller
{
	/* This function will get sidebar widget settings */
	public function get_widget_settings()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicPanelWidgetsInfo = OtonomicPanelWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->page_id))
		{
			$result['settings'] = $OtonomicPanelWidgetsInfo->get_widget_settings($json_api->query->widget_id, $json_api->query->page_id);
		}
		else
		{
			$result['error'] = 'widget_id and page_id is required';
		}
		return array(
			'respond'=> $result
		);
	}
	/* This function will set sidebar widget settings */
	public function set_widget_settings()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to set widget settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'set_widget_settings');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicPanelWidgetsInfo = OtonomicPanelWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->page_id) && isset($json_api->query->settings))
		{
			$OtonomicPanelWidgetsInfo->set_widget_settings($json_api->query->widget_id, $json_api->query->page_id, urldecode($json_api->query->settings) );
			$result['success'] = 'Settings updated successfully';
			$result['html'] = $OtonomicPanelWidgetsInfo->get_widget_front_html($json_api->query->widget_id, $json_api->query->page_id);
		}
		else
		{
			$result['error'] = 'widget_id, page_id and settings are required';
		}
		return array(
			'respond'=> $result
		);
	}

	/* This function will get sidebar widget settings form */
	public function get_widget_settings_form()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings form. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings_form');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicPanelWidgetsInfo = OtonomicPanelWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->page_id))
		{
			$result['settings_form'] = $OtonomicPanelWidgetsInfo->get_widget_settings_form($json_api->query->widget_id, $json_api->query->page_id);
		}
		else
		{
			$result['error'] = 'widget_id and page_id is required';
		}
		return array(
			'respond'=> $result
		);
	}

	/* This function will get sidebar widget front html */
	public function get_widget_front_html()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings form. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings_form');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicPanelWidgetsInfo = OtonomicPanelWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->page_id))
		{
			$result['html'] = $OtonomicPanelWidgetsInfo->get_widget_front_html($json_api->query->widget_id, $json_api->query->page_id);
		}
		else
		{
			$result['error'] = 'widget_id & page_id are required';
		}
		return array(
			'respond'=> $result
		);
	}
}