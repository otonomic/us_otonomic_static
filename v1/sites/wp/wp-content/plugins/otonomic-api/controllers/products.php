<?php
/*
Controller name: Products
Controller description: Products Methods
*/
class JSON_API_Products_Controller
{
    /* This function will create store product */
    public function create()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to create product. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'create');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }
        if(!isset($json_api->query->product_title))
        {
            $json_api->error("Product title is required.");
        }
        nocache_headers();

        $wpotonomic_product = wpotonomic_product::get_instance();
        $WPOtonomicProduct = new WPOtonomicProduct();

        $WPOtonomicProduct->product_type = 'simple';

        $WPOtonomicProduct->product_image_url   =   $json_api->query->image_url;
        $WPOtonomicProduct->product_id          =   0;
        $WPOtonomicProduct->product_sku         =   $json_api->query->product_sku;
        $WPOtonomicProduct->product_title       =   $json_api->query->product_title;
        $WPOtonomicProduct->product_description =   $json_api->query->product_description;
        $WPOtonomicProduct->product_price       =   $json_api->query->product_price;
        $WPOtonomicProduct->product_category    =   $json_api->query->product_category;

        $result = $wpotonomic_product->save_product($WPOtonomicProduct);

        if(!$result)
        {
            $json_api->error("Unable to create product, Something went wrong.");
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Product created'
			    )
	    );

    }
    /* This function will update store product */
    public function update()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to update product. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'update');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }
        $product_id = intval($json_api->query->product_id);

        /* Lets fetch the selected product */
        $wpotonomic_product = wpotonomic_product::get_instance();

        $WPOtonomicProduct = $wpotonomic_product->get_product_by_id($product_id);

        if(!$WPOtonomicProduct)
        {
            $json_api->error("Invalid product_id.");
        }
        nocache_headers();

        /* Lets start product update process */
        if(isset($json_api->query->product_image_url))
        {
            $WPOtonomicProduct->product_image_url   =   $json_api->query->image_url;
        }
        if(isset($json_api->query->product_sku))
        {
            $WPOtonomicProduct->product_sku   =   $json_api->query->product_sku;
        }
        if(isset($json_api->query->product_title))
        {
            $WPOtonomicProduct->product_title   =   $json_api->query->product_title;
        }
        if(isset($json_api->query->product_description))
        {
            $WPOtonomicProduct->product_description   =   $json_api->query->product_description;
        }
        if(isset($json_api->query->product_price))
        {
            $WPOtonomicProduct->product_price   =   $json_api->query->product_price;
        }
        if(isset($json_api->query->product_category))
        {
            $WPOtonomicProduct->product_category   =   $json_api->query->product_category;
        }

        $result = $wpotonomic_product->save_product($WPOtonomicProduct);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Product updated'
			    )
	    );
    }
    /* This function will delete store product */
    public function delete()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to delete product. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'delete');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        $product_id = intval($json_api->query->product_id);

        /* Lets fetch the selected product */
        $wpotonomic_product = wpotonomic_product::get_instance();

        $WPOtonomicProduct = $wpotonomic_product->get_product_by_id($product_id);

        if(!$WPOtonomicProduct)
        {
            $json_api->error("Invalid product_id.");
        }

        nocache_headers();

        $wpotonomic_product->delete_product($product_id);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Product deleted'
			    )
	    );

    }
    /* This function will hide store product */
    public function hide()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to hide product. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'hide');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        $product_id = intval($json_api->query->product_id);

        /* Lets fetch the selected product */
        $wpotonomic_product = wpotonomic_product::get_instance();

        $WPOtonomicProduct = $wpotonomic_product->get_product_by_id($product_id);

        if(!$WPOtonomicProduct)
        {
            $json_api->error("Invalid product_id.");
        }

        nocache_headers();

        $wpotonomic_product->update_status($product_id, 'draft');

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Product is now hidden from website'
			    )
	    );

    }
    /* This function will show store product */
    public function show()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to show product. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'show');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        $product_id = intval($json_api->query->product_id);

        /* Lets fetch the selected product */
        $wpotonomic_product = wpotonomic_product::get_instance();

        $WPOtonomicProduct = $wpotonomic_product->get_product_by_id($product_id);

        if(!$WPOtonomicProduct)
        {
            $json_api->error("Invalid product_id.");
        }

        nocache_headers();

        $wpotonomic_product->update_status($product_id, 'publish');

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Product is now displayed on website'
			    )
	    );

    }
    /*
    This function will show store product
    if product_id parameter is passed all other parameters will be ignored.
    */
    public function get()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to get products. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('products', 'get');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }
        nocache_headers();

        $wpotonomic_product = wpotonomic_product::get_instance();

        if(isset($json_api->query->product_id))
        {
            /* Get product by ID */
            $product_id = intval($json_api->query->product_id);

            /* Lets fetch the selected product */
            $WPOtonomicProduct = $wpotonomic_product->get_product_by_id($product_id);

            if(!$WPOtonomicProduct)
            {
                $json_api->error("Invalid product_id.");
            }
            $filtered_products_array = array($WPOtonomicProduct);
        }
        else
        {
            /* Check for other filters */
            $category = null;
            $post_status = 'publish';
            $count = -1;
            $order = 'DESC';
            $orderby = 'date';
            $tags=null;

            if(isset($json_api->query->category))
                $category = $json_api->query->category;

            if(isset($json_api->query->post_status))
                $post_status = $json_api->query->post_status;

            if(isset($json_api->query->count))
                $count = $json_api->query->count;

            if(isset($json_api->query->order))
                $order = $json_api->query->order;

            if(isset($json_api->query->orderby))
                $orderby = $json_api->query->orderby;

            if(isset($json_api->query->tags))
                $tags = $json_api->query->tags;

            if($json_api->query->search)
                $search = $json_api->query->search;

            $filtered_products = $wpotonomic_product->get_products($category, $post_status, $count, $order, $orderby, $tags, $search);
	        $filtered_products_array = $filtered_products;
        }

	    return array(
		    'respond'=>
			    array(
				    'products'=> $filtered_products_array
			    )
	    );

    }
}