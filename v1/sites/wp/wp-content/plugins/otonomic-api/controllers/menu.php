<?php
/*
Controller name: Menu
Controller description: Menu Methods
*/
class JSON_API_Menu_Controller
{
    /* This function will create new menu */
    public function create()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to create menu. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menu', 'create');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_name))
        {
            $json_api->error("Menu name is required.");
        }
        nocache_headers();

        $menu_name = $json_api->query->menu_name;

        $menu_exists = wp_get_nav_menu_object( $menu_name );
        if( !$menu_exists)
        {
            wp_create_nav_menu( $menu_name );
        }
        else
        {
            $json_api->error("Menu already exists.");
        }
	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu created'
			    )
	    );

    }

    /* This function will update menu */
    public function update()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to update menu. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menu', 'update');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->old_name))
        {
            $json_api->error("Old menu name is required.");
        }
        if(!isset($json_api->query->new_name))
        {
            $json_api->error("New menu name is required.");
        }

        nocache_headers();

        $old_name = $json_api->query->old_name;
        $new_name = $json_api->query->new_name;

        $menu_exists = wp_get_nav_menu_object( $old_name );
        if( $menu_exists)
        {
            wp_update_nav_menu_object($menu_exists->term_id, array( 'menu-name' => $new_name ));
        }
        else
        {
            $json_api->error("Menu not found.");
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu updated'
			    )
	    );
    }

    /* This function will delete menu */
    public function delete()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to delete menu. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menu', 'delete');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_name))
        {
            $json_api->error("Menu name is required.");
        }
        nocache_headers();

        $menu_name = $json_api->query->menu_name;

        $menu_exists = wp_get_nav_menu_object( $menu_name );

        if( $menu_exists)
        {
            wp_delete_nav_menu( $menu_name );
        }
        else
        {
            $json_api->error("Menu not found.");
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu deleted'
			    )
	    );
    }

    /* This function will add menu item in menu */
    public function add_item()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to add menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menu', 'add_item');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_name))
        {
            $json_api->error("Menu name is required.");
        }
        if(!isset($json_api->query->item_id))
        {
            $json_api->error("Item id is required.");
        }
        nocache_headers();

        $menu_name = $json_api->query->menu_name;

        $item_id = $json_api->query->item_id;

        $menu_exists = wp_get_nav_menu_object( $menu_name );

        if( !$menu_exists)
        {
            $json_api->error("Menu not found.");
        }
        /* first check if the provided menu item is of correct post type */
        $menu_item = get_post($item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }
        /* Add item to this menu */
        wp_set_object_terms($item_id, $menu_exists->slug, 'nav_menu');

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Item added in menu'
			    )
	    );
    }

    /* This function will remove menu item in menu */
    public function remove_item()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to remove menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menu', 'remove_item');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_name))
        {
            $json_api->error("Menu name is required.");
        }
        if(!isset($json_api->query->item_id))
        {
            $json_api->error("Item id is required.");
        }
        nocache_headers();

        $menu_name = $json_api->query->menu_name;

        $item_id = $json_api->query->item_id;

        $menu_exists = wp_get_nav_menu_object( $menu_name );

        if( !$menu_exists)
        {
            $json_api->error("Menu not found.");
        }
        /* first check if the provided menu item is of correct post type */
        $menu_item = get_post($item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }
        /* check if this item belongs to the provided menu */
        $terms = wp_get_object_terms($item_id, 'nav_menu');
        $belongs = false;
        if(count($terms)>0)
        {
            foreach($terms as $term)
            {
                if($term->term_id == $menu_exists->term_id)
                {
                    $belongs = true;
                    break;
                }

            }
        }
        if(!$belongs)
        {
            $json_api->error("Item doesn't belongs to provided menu.");
        }

        /* Now lets remove this item from this menu */
        wp_remove_object_terms($item_id, $menu_exists->slug, 'nav_menu');

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Item removed from menu'
			    )
	    );

    }
}