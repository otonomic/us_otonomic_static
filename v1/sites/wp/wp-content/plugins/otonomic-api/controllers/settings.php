<?php
/*
Controller name: Settings
Controller description: Settings Methods
*/
class JSON_API_Settings_Controller
{
    /* This function will fetch settings */
    public function get()
    {
        global $json_api;
	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to get settings. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('settings', 'get');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }
        if(!isset($json_api->query->key))
        {
            $json_api->error("Please provide a key to fetch its value.");
        }

        nocache_headers();

        $value = $Otonomic_First_Session->get_settings($json_api->query->key);

	    return array(
		    'respond'=>
			    array(
				    $json_api->query->key=>$value
			    )
	    );

        return $result;
    }

    /* This function will fetch multiple settings */
    public function get_many()
    {
        global $json_api;
	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to get_many settings. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('settings', 'get_many');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }
        if(!isset($json_api->query->keys))
        {
            $json_api->error("Please provide keys to fetch value.");
        }
        nocache_headers();
        $keys = $json_api->query->keys;

        if(!is_array($keys))
        {
            $keys = array($keys);
        }
        $result = array();
        foreach($keys as $key)
        {
            $result[$key] = $Otonomic_First_Session->get_settings($key);
        }

	    return array(
		    'respond'=>
			    array(
				    $result
			    )
	    );
    }

    /* This function will update/create settings */
    public function set()
    {
        global $json_api;
	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to set settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('settings', 'set');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        if(!isset($json_api->query->key))
        {
            $json_api->error("Please provide a key to set its value.");
        }
        if(!isset($json_api->query->value))
        {
            $json_api->error("Please provide value.");
        }
        nocache_headers();

	    $Otonomic_First_Session->set_settings($json_api->query->key, $json_api->query->value);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Setting saved.'
			    )
	    );

    }




    public function set_many()
    {
        global $json_api;
	    $Otonomic_First_Session = Otonomic_First_Session::get_instance();
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to set settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('settings', 'set');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        if(!isset($json_api->query->values))
        {
            $json_api->error("Please provide some values.");
        }

        nocache_headers();

        foreach($json_api->query->values as $key=>$value){
	        $Otonomic_First_Session->set_settings($key, $value);
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Setting saved.'
			    )
	    );
    }
}