<?php
/*
Controller name: Widgets
Controller description: Widgets Methods
*/
class JSON_API_Widgets_Controller
{
    /* This function will get list of registered sidebars */
    public function get_registered_sidebars()
    {
        global $json_api;
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get list of sidebars. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_registered_sidebars');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        nocache_headers();
	    $OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();

	    return array(
		    'respond'=>
			    array(
				    "sidebars" => $OtonomicWidgetsInfo->get_registered_sidebars()
			    )
	    );

    }
    /* This function will get all widgets in a sidebar */
    public function get_sidebar_widgets()
    {
	    global $json_api;
	    /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widgets in sidebar. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_sidebar_widgets');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
	    nocache_headers();
	    $OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();
	    $result = $OtonomicWidgetsInfo->get_sidebar_widgets($json_api->query->sidebar_id);
	    return array(
		    'respond'=>
			    array(
				    "widgets" => $result
			    )
	    );
    }
	/* This function will get sidebar widget settings */
	public function get_widget_settings()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id))
		{
			$result['settings'] = $OtonomicWidgetsInfo->get_widget_settings($json_api->query->widget_id);
		}
		else
		{
			$result['error'] = 'widget_id is required';
		}
		return array(
			'respond'=> $result
		);
	}
	/* This function will set sidebar widget settings */
	public function set_widget_settings()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to set widget settings. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'set_widget_settings');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->settings))
		{
			$OtonomicWidgetsInfo->set_widget_settings($json_api->query->widget_id, urldecode($json_api->query->settings) );
			$result['success'] = 'Settings updated successfully';
			$result['html'] = $OtonomicWidgetsInfo->get_widget_front_html($json_api->query->widget_id, $json_api->query->sidebar_id);
		}
		else
		{
			$result['error'] = 'widget_id & settings are required';
		}
		return array(
			'respond'=> $result
		);
	}

	/* This function will get sidebar widget settings form */
	public function get_widget_settings_form()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings form. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings_form');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id))
		{
			$result['settings_form'] = $OtonomicWidgetsInfo->get_widget_settings_form($json_api->query->widget_id);
		}
		else
		{
			$result['error'] = 'widget_id is required';
		}
		return array(
			'respond'=> $result
		);
	}

	/* This function will get sidebar widget front html */
	public function get_widget_front_html()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get widget settings form. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_widget_settings_form');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();

		$result = array();
		if(isset($json_api->query->widget_id) && isset($json_api->query->sidebar_id))
		{
			$result['html'] = $OtonomicWidgetsInfo->get_widget_front_html($json_api->query->widget_id, $json_api->query->sidebar_id);
		}
		else
		{
			$result['error'] = 'widget_id & sidebar_id are required';
		}
		return array(
			'respond'=> $result
		);
	}

	/* This function will get all available widgets */
	public function get_registered_widgets()
	{
		global $json_api;
		/* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get list of available widgets. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_registered_widgets');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
		nocache_headers();
		$OtonomicWidgetsInfo = OtonomicWidgetsInfo::get_instance();
		$result = $OtonomicWidgetsInfo->get_registered_widgets();
		return array(
			'respond'=>
				array(
					"widgets" => $result
				)
		);
	}
}