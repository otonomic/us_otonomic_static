<?php
/*
Controller name: Store
Controller description: Store Methods
*/
class JSON_API_Store_Controller
{
    /* This function will initialize the store and create a default product if required */
    public function create()
    {
        global $json_api;
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to create store. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('store', 'create');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        nocache_headers();

        /* Create store pages and setup the related settings */
	    $OtonomicStore = OtonomicStore::get_instance();
	    $OtonomicStore->show_store_pages();
        /* Check if we need to create the demo products */
        $demo_product = $json_api->query->demo_product;
        if($demo_product=='yes')
        {
	        $OtonomicStore->add_sample_product();
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Store created'
			    )
	    );

    }
    /* This function will hide the store pages */
    public function hide()
    {
        global $json_api;
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to hide store. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('store', 'hide');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        nocache_headers();

        /* Lets hide the store now */
        //wp_otonomic_hide_woocommerce_pages();
	    $OtonomicStore = OtonomicStore::get_instance();
	    $OtonomicStore->hide_store_pages();

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Store removed'
			    )
	    );

    }
}