<?php
/*
Controller name: Menu items
Controller description: Menu items Methods
*/
class JSON_API_Menuitems_Controller
{
    /* This function will create new menu item */
    public function create()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to create menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menuitems', 'create');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_item_name))
        {
            $json_api->error("Menu item name is required.");
        }
        if(!isset($json_api->query->menu_item_url))
        {
            $json_api->error("Menu item URL is required.");
        }

        nocache_headers();

        $menu_item_url = $json_api->query->menu_item_url;
        $menu_item_name = $json_api->query->menu_item_name;

        $args = array(
            'post_title' => $menu_item_name,
            'post_status' => 'publish',
            'post_type' => 'nav_menu_item',
        );

        $menu_item_id = wp_insert_post( $args );

        update_post_meta($menu_item_id, '_menu_item_url', $menu_item_url);
        update_post_meta($menu_item_id, '_menu_item_type', 'custom');
        update_post_meta($menu_item_id, '_menu_item_menu_item_parent', '0');
        update_post_meta($menu_item_id, '_menu_item_object_id', $menu_item_id);
        update_post_meta($menu_item_id, '_menu_item_object', 'custom');

        /* Create store pages and setup the related settings */

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu item created',
				    "menu_item_id"=>$menu_item_id
			    )
	    );

    }

    /* This function will update menu item */
    public function update()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to update menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menuitems', 'update');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_item_id))
        {
            $json_api->error("Menu item id is required.");
        }

        nocache_headers();

        $menu_item_id = $json_api->query->menu_item_id;

        $menu_item = get_post($menu_item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }

        if(isset($json_api->query->menu_item_url))
        {
            update_post_meta($menu_item_id, '_menu_item_url', $json_api->query->menu_item_url);
        }

        if(isset($json_api->query->menu_item_name))
        {
            $args = array(
                'ID' => $menu_item_id,
                'post_title' => $json_api->query->menu_item_name,
            );
            wp_update_post( $args );
        }

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu item updated',
			    )
	    );

    }

    /* This function will delete menu item */
    public function delete()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to delete menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menuitems', 'delete');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_item_id))
        {
            $json_api->error("Menu item id is required.");
        }

        nocache_headers();

        $menu_item_id = $json_api->query->menu_item_id;

        $menu_item = get_post($menu_item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }

        /* delete menu item */
        wp_delete_post($menu_item_id);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu item deleted',
			    )
	    );

    }

    /* This function will hide menu item */
    public function hide()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to delete menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menuitems', 'hide');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_item_id))
        {
            $json_api->error("Menu item id is required.");
        }

        nocache_headers();

        $menu_item_id = $json_api->query->menu_item_id;

        $menu_item = get_post($menu_item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }

        /* hide menu item */
        $args = array(
            'ID' => $menu_item_id,
            'post_status' => 'draft',
        );
        wp_update_post( $args );

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu item is now hidden from website.',
			    )
	    );

    }

    /* This function will show menu item */
    public function show()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to show menu item. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('menuitems', 'show');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->menu_item_id))
        {
            $json_api->error("Menu item id is required.");
        }

        nocache_headers();

        $menu_item_id = $json_api->query->menu_item_id;

        $menu_item = get_post($menu_item_id);
        if($menu_item->post_type != 'nav_menu_item')
        {
            $json_api->error("Invalid item id.");
        }

        /* hide menu item */
        $args = array(
            'ID' => $menu_item_id,
            'post_status' => 'publish',
        );
        wp_update_post( $args );

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Menu item is now displayed on website.',
			    )
	    );

    }
}