<?php
/*
Controller name: Testimonials
Controller description: Testimonials Methods
*/
class JSON_API_Testimonials_Controller
{
    /* This function will create new testimonial */
    public function create()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to create testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'create');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->content))
        {
            $json_api->error("Testimonial content is required.");
        }

        nocache_headers();

        /* Create testimonial */
        $WPOtonomicReview = new WPOtonomicReview();

        $WPOtonomicReview->reviewer_name = $json_api->query->username;
        $WPOtonomicReview->reviewer_review = $json_api->query->content;
        $WPOtonomicReview->reviewer_rating = $json_api->query->rating;
        $WPOtonomicReview->reviewer_image = $json_api->query->image_url;

        $wpotonomic_review = new wpotonomic_review();

        $wpotonomic_review->save_review($WPOtonomicReview);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Testimonial created'
			    )
	    );

    }

    /* This function will update testimonial */
    public function update()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to update testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'update');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->testimonial_id))
        {
            $json_api->error("Testimonial id is required.");
        }

        $wpotonomic_review = new wpotonomic_review();

        $WPOtonomicReview = $wpotonomic_review->get_review_by_id( $json_api->query->testimonial_id );

        if(!$WPOtonomicReview)
        {
            $json_api->error("Invalid testimonial id.");
        }

        nocache_headers();

        /* update testimonial */

        if(isset( $json_api->query->username ))
            $WPOtonomicReview->reviewer_name = $json_api->query->username;

        if(isset( $json_api->query->content ))
            $WPOtonomicReview->reviewer_review = $json_api->query->content;

        if(isset( $json_api->query->rating ))
            $WPOtonomicReview->reviewer_rating = $json_api->query->rating;

        if(isset( $json_api->query->image_url ))
            $WPOtonomicReview->reviewer_image = $json_api->query->image_url;


        $wpotonomic_review->save_review($WPOtonomicReview);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Testimonial updated'
			    )
	    );

    }

    /* This function will delete testimonial */
    public function delete()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to delete testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'delete');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->testimonial_id))
        {
            $json_api->error("Testimonial id is required.");
        }

        $wpotonomic_review = new wpotonomic_review();

        $WPOtonomicReview = $wpotonomic_review->get_review_by_id( $json_api->query->testimonial_id );

        if(!$WPOtonomicReview)
        {
            $json_api->error("Invalid testimonial id.");
        }

        nocache_headers();

        $wpotonomic_review->delete_review($json_api->query->testimonial_id);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Testimonial deleted'
			    )
	    );

    }

    /* This function will hide testimonial */
    public function hide()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to hide testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'hide');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->testimonial_id))
        {
            $json_api->error("Testimonial id is required.");
        }

        $wpotonomic_review = new wpotonomic_review();

        $WPOtonomicReview = $wpotonomic_review->get_review_by_id( $json_api->query->testimonial_id );

        if(!$WPOtonomicReview)
        {
            $json_api->error("Invalid testimonial id.");
        }

        nocache_headers();

        $WPOtonomicReview->review_status = 'draft';

        $wpotonomic_review->update_status($WPOtonomicReview);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Testimonial is now hidden from website'
			    )
	    );

    }

    /* This function will show testimonial */
    public function show()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to show testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'show');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        if(!isset($json_api->query->testimonial_id))
        {
            $json_api->error("Testimonial id is required.");
        }

        $wpotonomic_review = new wpotonomic_review();

        $WPOtonomicReview = $wpotonomic_review->get_review_by_id( $json_api->query->testimonial_id );

        if(!$WPOtonomicReview)
        {
            $json_api->error("Invalid testimonial id.");
        }

        nocache_headers();

        $WPOtonomicReview->review_status = 'publish';

        $wpotonomic_review->update_status($WPOtonomicReview);

	    return array(
		    'respond'=>
			    array(
				    "msg" => 'Testimonial is now displayed on website'
			    )
	    );

    }

    /* This function will get testimonial */
    public function get()
    {
        global $json_api;
        /* Test nonce */
        if (!$json_api->query->nonce) {
            $json_api->error("You must include a 'nonce' value to get testimonials. Use the `get_nonce` Core API method.");
        }
        $nonce_id = $json_api->get_nonce_id('testimonials', 'get');
        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
        }

        $wpotonomic_review = new wpotonomic_review();

        nocache_headers();

        $status = 'all';

        if(isset($json_api->query->status))
        {
            $status = $json_api->query->status;
        }
        $filtered_reviews = $wpotonomic_review->get_reviews(null, $status);

	    return array(
		    'respond'=>
			    array(
				    'testimonials'=> $filtered_reviews
			    )
	    );

    }
}