<?php
/*
Controller name: Adminpage
Controller description: Adminpage Methods
*/
class JSON_API_Adminpage_Controller
{
    /* This function will get list of registered sidebars */
    public function get_page_data()
    {
        global $json_api;
        /* Test nonce */
//        if (!$json_api->query->nonce) {
//            $json_api->error("You must include a 'nonce' value to get page data. Use the `get_nonce` Core API method.");
//        }
//        $nonce_id = $json_api->get_nonce_id('widgets', 'get_registered_sidebars');
//        if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
//            $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
//        }
        nocache_headers();
	    $result = array();
	    if(isset($json_api->query->page))
	    {
		    $page_class_name = "{$json_api->query->page}AdminPage";
		    /* include class file */
		    $otonomic_utility = otonomic_utility::get_instance();
		    //$file_path = WP_OTONOMIC_PLUGIN_DIR . '/admin/includes/classes/pages/'.$otonomic_utility->camelCaseToUnderscores($json_api->query->page).'.php';
		    //if(file_exists($file_path))
		    if(class_exists($page_class_name))
		    {
			    //require_once($file_path);
			    $page_object = new $page_class_name;
			    $page_data = $page_object->getPageData();
			    $result['data'] = $page_data;
		    }
			else
			{
				$result['error'] = 'page not found';
			}

	    }
	    else
	    {
		    $result['error'] = 'page is required';
	    }

	    return array(
		    'respond'=> $result
	    );

    }
}