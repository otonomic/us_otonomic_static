<?php

class JSON_API_Otonomic_Controller{
    
    function email_site_address(){
        global $json_api;
        
        if(!function_exists('mymail_send'))
            return array(
                'status' => 'error',
                'error' => 'You need to activate the plugin MyMail'
            );
        
        if(!filter_var($json_api->query->email,FILTER_VALIDATE_EMAIL))
            return array(
                'status' => 'error',
                'error' => 'Invalid email'
            );
        
        if(mymail_send('test','test content',$json_api->query->email,array(),array(),'first_login_tpl.html'))
            return array(
                'status' => 'ok'
            );
        
        return array(
            'status' => 'error',
            'error' => 'Could not send email.'
        );
    }

}