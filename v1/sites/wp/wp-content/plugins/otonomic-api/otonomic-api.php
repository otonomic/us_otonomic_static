<?php

/*
  Plugin Name: Otonomic API
  Plugin URI: http://otonomic.com/
  Description: Otonomic API Extends the JSON API for user authentiocation utilizing the Wordpress cookie validation and generation.
  Version: 1.0
  Author: otonomic.com
  Author URI: http://www.otonomic.com
  License: GPLv3
 */
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// latest version of WooCommerce doesn't include this in all needed cases, so we do it for it
include_once WP_PLUGIN_DIR . '/woocommerce/includes/admin/wc-admin-functions.php';

add_filter('json_api_controllers', 'pimOtoJsonApiController');
add_filter('json_api_ototest_controller_path', 'setOtoControllerPath');
add_filter('json_api_store_controller_path', 'setStoreControllerPath');
add_filter('json_api_widgets_controller_path', 'setWidgetsControllerPath');
add_filter('json_api_panelwidgets_controller_path', 'setPanelwidgetsControllerPath');
add_filter('json_api_adminpage_controller_path', 'setAdminpageControllerPath');
add_filter('json_api_products_controller_path', 'setProductsControllerPath');
add_filter('json_api_settings_controller_path', 'setSettingsControllerPath');
add_filter('json_api_testimonials_controller_path', 'setTestimonialsControllerPath');
add_filter('json_api_menu_controller_path', 'setMenuControllerPath');
add_filter('json_api_otonomic_controller_path', 'setOtonomicControllerPath');


add_filter('json_api_menuitems_controller_path', 'setMenuitemsControllerPath');

register_activation_hook( __FILE__, 'otonomic_api_activate' );

function pimOtoJsonApiController($aControllers) {
    $aControllers[] = 'Ototest';
    $aControllers[] = 'Store';
	$aControllers[] = 'Widgets';
	$aControllers[] = 'Panelwidgets';
	$aControllers[] = 'Adminpage';
    $aControllers[] = 'Products';
    $aControllers[] = 'Settings';
    $aControllers[] = 'Testimonials';
    $aControllers[] = 'Menu';
    $aControllers[] = 'Menuitems';
    $aControllers[] = 'Otonomic';
    return $aControllers;
}

function setOtonomicControllerPath($sDefaultPath){
    return dirname(__FILE__) . '/controllers/Otonomic.php';
}

function setOtoControllerPath($sDefaultPath) {
    return dirname(__FILE__) . '/controllers/Ototest.php';
}

function setStoreControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/store.php';
}

function setWidgetsControllerPath( $sDefaultPath )
{
	return dirname(__FILE__) . '/controllers/widgets.php';
}

function setPanelwidgetsControllerPath( $sDefaultPath )
{
	return dirname(__FILE__) . '/controllers/panelwidgets.php';
}

function setAdminpageControllerPath( $sDefaultPath )
{
	return dirname(__FILE__) . '/controllers/adminpage.php';
}

function setProductsControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/products.php';
}

function setSettingsControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/settings.php';
}
function setTestimonialsControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/testimonials.php';
}

function setMenuControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/menu.php';
}

function setMenuitemsControllerPath( $sDefaultPath )
{
    return dirname(__FILE__) . '/controllers/menuitems.php';
}

function otonomic_api_activate() {

   $option = update_option("json_api_controllers", "core,posts,ototest,store,widgets,panelwidgets,adminpage,products,settings,testimonials,menu,menuitems,otonomic");

    if(!$option){
        add_option("json_api_controllers", "core,posts,ototest,store,widgets,panelwidgets,adminpage,products,settings,testimonials,menu,menuitems,otonomic");
    }
}