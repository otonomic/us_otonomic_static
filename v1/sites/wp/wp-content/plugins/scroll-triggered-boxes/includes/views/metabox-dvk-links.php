<?php
if( ! defined( 'STB::VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
} ?>
<p>I am <a href="https://dannyvankooten.com/">Danny van Kooten</a>, a young Dutch developer. I write code and emails for a living while traveling the world.</p>
<p>I developed several <a href="https://dannyvankooten.com/wordpress-plugins/">plugins for WordPress</a> totalling well over a million downloads, the most important one being <a href="https://mc4wp.com/">MailChimp for WordPress</a>.</p>
<p><a href="https://twitter.com/dannyvankooten">Follow me on Twitter</a> to stay updated of what I'm working on.</p>
<p>Hope you like this plugin!</p>
<p><em>~ Danny</em></p>