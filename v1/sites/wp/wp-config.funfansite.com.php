<?php
define('MAIN_DOMAIN', 'funfansite.com');

define('WP_DEBUG', false);
define('SCRIPT_DEBUG', false);
define('WP_DEBUG_LOG', true);

define('SUBDOMAIN_INSTALL', true);

define( 'COOKIE_DOMAIN', MAIN_DOMAIN);

define('WP_HOME', MAIN_DOMAIN);
define('WP_SITEURL', WP_HOME);

define('DOMAIN_CURRENT_SITE',       'wp.'.MAIN_DOMAIN);
define('DOMAIN_FOR_SITE_CREATION',  MAIN_DOMAIN);
define('DOMAIN_FOR_THE_SYSTEM',     'wp.'.MAIN_DOMAIN);

define('DB_NAME', 'funfansite');
define('DB_USER', 'root');
define('DB_PASSWORD', 'otoOTO2611');
//define('DB_HOST', 'dbmaster.otonomic.com');
define('DB_HOST', '10.89.23.136');

define('FACEBOOK_APP_ID', '194646626082');



if (defined('DOING_AJAX') && DOING_AJAX) {
    define('WP_DEBUG_DISPLAY', isset($_GET['debug']) ? $_GET['debug'] : false);

} else {
    ini_set('display_errors', 1);
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
    define('WP_DEBUG_DISPLAY', isset($_GET['debug']) ? $_GET['debug'] : true);

    // error_reporting(-1);

    // ----------------------------------------------------------------------------------------------------
    // - Shutdown Handler
    // ----------------------------------------------------------------------------------------------------
    function ShutdownHandler()
    {
        if(@is_array($error = @error_get_last()))
        {
            return(@call_user_func_array('ErrorHandler', $error));
        };

        return(TRUE);
    };

    // register_shutdown_function('ShutdownHandler');

    // ----------------------------------------------------------------------------------------------------
    // - Error Handler
    // ----------------------------------------------------------------------------------------------------
    function ErrorHandler($type, $message, $file, $line)
    {
        $_ERRORS = Array(
            0x0001 => 'E_ERROR',
            0x0002 => 'E_WARNING',
            0x0004 => 'E_PARSE',
            0x0008 => 'E_NOTICE',
            0x0010 => 'E_CORE_ERROR',
            0x0020 => 'E_CORE_WARNING',
            0x0040 => 'E_COMPILE_ERROR',
            0x0080 => 'E_COMPILE_WARNING',
            0x0100 => 'E_USER_ERROR',
            0x0200 => 'E_USER_WARNING',
            0x0400 => 'E_USER_NOTICE',
            0x0800 => 'E_STRICT',
            0x1000 => 'E_RECOVERABLE_ERROR',
            0x2000 => 'E_DEPRECATED',
            0x4000 => 'E_USER_DEPRECATED'
        );

        if(!@is_string($name = @array_search($type, @array_flip($_ERRORS))))
        {
            $name = 'E_UNKNOWN';
        };

        return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
    };

    // $old_error_handler = set_error_handler("ErrorHandler");
}

define('TRACK_PIWIK', true);
define('TRACK_OTONOMIC', true);
define('TRACK_LUCKYORANGE', true);

define('LUCKY_ORANGE_SITE_ID', '33818');

define('PIWIK_URL', 'a.otonomic.com/');
define('PIWIK_SITE_ID', 2);

define('P2S_LOCAL', 1);
define('ANALYTICS_ID','UA-59582195-1'); // funfansite.com