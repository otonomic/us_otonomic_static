<?php
/**
 * Created by PhpStorm.
 * User: Omri
 * Date: 20/12/15
 * Time: 12:41
 */

if(empty($_GET['url'])) {
    die('Please set a url query tag to point to the required log file');
}
$format = isset($_GET['format']) ? $_GET['format'] : 'json';

$url = $_GET['url'];
$response = file_get_contents($url);

$records = explode('<!-- New Entry  -->', $response);

$data = [];
foreach($records as $record) {
    $rows = explode("\n", $record);

    $item = [];
    foreach($rows as $row) {
        if(substr($row,0,5) === 'Date:') {
            $item['date'] = substr($row, 9);
        }
        if(substr($row,0,5) === 'FBID:') {
            $item['fbid'] = substr($row, 5);
        }
        if(substr($row,0,5) === 'Name:') {
            $item['name'] = substr($row, 5);
        }
        if(substr($row,0,6) === 'Email:') {
            $item['email'] = substr($row, 6);
        }
        if(substr($row,0,6) === 'Token:') {
            $item['token'] = substr($row, 6);
        }
    }
    if($item) {
        $item = array_map('trim', $item);
        $data[] = $item;
    }
}

switch($format) {
    case 'csv':
        $fh = fopen('php://output', 'rw');
        fputcsv($fh, [ 'Date','FBID','Name','Email','Token']);
        foreach($data as $item) {
            fputcsv($fh, [ $item['date'], $item['fbid'], $item['name'], $item['email'], $item['token'] ]);
        }
        fclose($fh);
        break;

    case 'json':
    default:
        echo json_encode($data);
        break;
}
