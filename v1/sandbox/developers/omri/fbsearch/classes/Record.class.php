<?php

class Record {
    var $data;

    function __construct($data) {
        $this->data = $data;
    }

    function parseField($field_name) {
        if(!$field_name) { return null; }

        $method_name = 'parse_'.$field_name;
        if(method_exists($this, $method_name)) {
            return $this->$method_name();
        }
        if(isset($this->data[$field_name])) {
            return $this->data[$field_name];
        }

        $this->parse_city();

        return false;
    }

    function parse_city() {
        if(isset($this->data['location']['city'])) {
            return $this->data['location']['city'];
        }

        return false;
    }

    function parse_country() {
        if(isset($this->data['location']['country'])) {
            return $this->data['location']['country'];
        }

        return false;
    }
}
