<?php
include('Record.class.php');

Class FbSearch {
    protected $keyword;
    protected $format = 'csv';

    function __construct($keyword = null, $format) {
        $this->keyword = $keyword;
        if($format) {
            $this->format = $format;
        }
    }

    function getData() {
        $keyword = $this->keyword;

        $access_token = '389314351133865|O4FgcprDMY0k6rxRUO-KOkWuVoU';
        $limit = 1000;
        $num_results = $limit;
        /*
        $url = 'https://graph.facebook.com/search/?q='.urlencode($keyword).'&type=page&access_token='.$access_token.
            '&fields=id,name,username,category,category_list,likes,website,phone,location,talking_about_count,is_community_page,parent_page,cover&limit='.$limit;
        */
        $query_params = [
            'q'             => $keyword,
            'type'          => 'page',
            'access_token'  => $access_token,
            'fields'        => 'id,name,username,category,category_list,likes,website,phone,emails,location,talking_about_count,is_community_page,parent_page,cover',
            'limit'         => $limit
        ];
        $url = 'https://graph.facebook.com/search/?' . http_build_query($query_params);

        $response = json_decode(file_get_contents($url), true);
        $result = $response['data'];

        while($num_results > 0 && !empty($response['paging']['cursors']['after'])) {
            // $url = $response->paging->cursor->next;
            $query_params['after'] = $response['paging']['cursors']['after'];
            $url = 'https://graph.facebook.com/search/?' . http_build_query($query_params);

            $response = json_decode(file_get_contents($url), true);
            $result = array_merge($result, $response['data']);
            $num_results -= count($response['data']);
        }

        return $result;
    }

    function run() {
        $result = $this->getData();

        if($this->format === 'csv') {
            $result = $this->toCsv($result);
        }

        return $result;
    }

    function toArray($records = []) {
        $result = [];

        $csv_fields_map = [
            'id',
            'name',
            'username',
            'category',
            'category_list',
            'city',
            'country',
            'location',
            'phone',
            'website',
            'likes',
            'talking_about_count',
            'is_community_page',
            'parent_page'
        ];

        foreach($records as $record) {
            $c = count($csv_fields_map);
            $tmp = array_fill(0, $c, '');
            for($i=0; $i<$c; $i++) {
                if(isset($csv_fields_map[$i])) {
                    $obj = new Record($record);
                    $tmp[$i] = $obj->parseField($csv_fields_map[$i]);
                    if(is_array($tmp[$i])) {
                        $tmp[$i] = json_encode($tmp[$i]);
                    }
                }
            }
            $result[] = $tmp;
        }

        return $result;
    }

    function toCsv($records) {
        $result = $this->toArray($records);

        $fh = fopen('php://temp', 'rw');
        foreach($result as $row) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }
}


function makeclickable($str, $link_text = null) {
    if(!$link_text) { $link_text = $str; }
    return '=HYPERLINK("'.$str.'", "'.$link_text.'")';
}
