<?php
require __DIR__ . '\..\..\classes\FbSearch.php';

class FbSearchTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
    }

    public function testToArray()
    {
        $records = [
            [
                'id'    => 1,
                'name'  => 'Omri',
                'location'  => [
                    'city'  => 'Tel Aviv',
                    'country' => 'Israel'
                ]
            ]
        ];
        $obj = new FbSearch();
        $result = $obj->toArray($records);

        $expected = array (
            0 =>
                array (
                    0 => 1,
                    1 => 'Omri',
                    2 => false,
                    3 => false,
                    4 => false,
                    5 => 'Tel Aviv',
                    6 => 'Israel',
                    7 => '{"city":"Tel Aviv","country":"Israel"}',
                    8 => false,
                    9 => false,
                    10 => false,
                    11 => false,
                    12 => false,
                    13 => false,
                ),
            );

        $this->assertEquals($expected, $result);
    }

    public function testToCsv() {
        $records = [
            [
                'id'    => 1,
                'name'  => 'Omri',
                'location'  => [
                    'city'  => 'Tel Aviv',
                    'country' => 'Israel'
                ]
            ]
        ];
        $obj = new FbSearch();
        $result = $obj->toCsv($records);
        $expected = '1,Omri,,,,"Tel Aviv",Israel,"{""city"":""Tel Aviv"",""country"":""Israel""}",,,,,,' . "\n";

        $this->assertEquals($expected, $result);
    }
}