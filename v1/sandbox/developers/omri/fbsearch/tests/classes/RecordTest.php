<?php
require __DIR__ . '\..\..\classes\Record.class.php';

class RecordTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
    }

    public function testParseField()
    {
        $data = [
            'id'    => 1,
            'name'  => 'Omri',
            'location'  => [
                'city'  => 'Tel Aviv',
                'country' => 'Israel'
            ]
        ];
        $obj = new Record($data);

        $this->assertEquals('Omri', $obj->parseField('name'));
        $this->assertEquals(null, $obj->parseField('non_existant_field'));

        $this->assertEquals('Tel Aviv', $obj->parse_city());
        $this->assertEquals('Israel', $obj->parse_country());

        $this->assertEquals('Tel Aviv', $obj->parseField('city'));
        $this->assertEquals('Israel', $obj->parseField('country'));

    }
}