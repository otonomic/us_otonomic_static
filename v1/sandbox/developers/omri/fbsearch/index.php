<?php
require_once('classes/FbSearch.php');

if(empty($_GET['keyword'])) {
    die('ERROR: Please specify a keyword parameter');
}

$keyword = $_GET['keyword'];
$format = isset($_GET['format']) ? $_GET['format'] : null;
$fbsearch = new FbSearch($keyword, $format);
echo $fbsearch->run();