<?php
/**
 *
 * This class scrapes the businesses from the yelp search page.
 * Class YelpScrape
 */
require_once('simple_html_dom.php');

if(!empty($_GET['debug'])) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
}

function addslashes_array($value, $key) {
    return addslashes($value);
}

class YelpScrapeBusiness {
    public $url = null;
    public $html;
    public $limit = 10;
    protected $type;

     public function __construct($type = '') {
         $this->type = $type;
     }

    function getData($url) {
        if(!$url) { return ""; }

        if($this->url == $url) {
            die; // Weird bug causes it to rerun - die so we don't have infinite loop
        }
        $this->url = $url;
        
        $this->html = file_get_html($url);

        if(!$this->html) {
            throw new Exception('ERROR: Call to url failed. Url: '.$url);
        }

        $data = $this->getBusinesses();
        $more_data = $this->getMoreBusinesses();
        $data = array_merge($data,$more_data);

        switch($this->type) {
            case 'csv':
                $handle = fopen("php://output", "w");
                foreach($data as $item) {
                    fputcsv($handle, [$item['name'], $item['address'], $item['phone'], $item['phone_cleaned'], $item['category']]);
                    // fwrite($handle, "\r\n");
                }
                fclose($handle);
                break;

            case 'json':
            default:
                return json_encode([
                    'url' => $url,
                    'data' => $data,
                ]);
        }
    }
    
    
    public function setLimit($limit){
        $this->limit = $limit;
    }
    
    private function getMoreBusinesses(){
        if($this->limit < 10){
            return FALSE;
        }
            $next_url = $this->get_next_url();
            $this->url = $next_url;
            $more_data = array();
            for($limit=10; $limit < $this->limit; $limit +=10){
                $this->html = file_get_html($next_url);
                $data = $this->getBusinesses();
                $more_data = array_merge($more_data,$data);
                $next_url = $this->get_next_url();
                $this->url = $next_url;
            }
            return $more_data;
    }
    
    private function get_next_url(){
        $urldata = parse_url($this->url);
        parse_str($urldata['query'],$urlQuery);
        $urlQuery['start'] += 10;
        $new_query = http_build_query($urlQuery);
        $next_url = $urldata['scheme'].'://'.$urldata['host'].'/'.$urldata['path'].'?'.$new_query;
        return $next_url;
    }

    private function getBusinesses() {
        $result = [];

        if(!$this->html) { $this->html = file_get_html($this->url); }

        if(!$this->html) {
            return;
        }

        // Find all links
        foreach($this->html->find('li.regular-search-result') as $element) {
            $record = array();
            $record['name']             = trim($this->getBusinessName($element));
            $record['phone']            = trim($this->getPhone($element));
            $record['phone_cleaned']    = $this->cleanPhone($record);
            $record['address']          = trim($this->getAddress($element));
            $record['category']         = trim($this->getCategory($element));
            $result[] = $record;
        }

        return $result;
    }

    private function getValue($element, $selector) {
        $result = '';
        foreach ($element->find($selector) as $tag){
            $result =  $tag->innertext;
        }
        return $result;
    }

    private function getPhone($element){
        return $this->getValue($element, 'span.biz-phone');
    }

    public function cleanPhone($record) {
        return preg_replace('/\D+/', '', $record['phone']);
    }
    
    private function getAddress($element){
        return $this->getValue($element, 'div.secondary-attributes address');
    }

    private function getCategory($element){
        return strip_tags($this->getValue($element, 'span.category-str-list'));
    }


    private function getBusinessName($element){
        return $this->getValue($element, 'a.biz-name span');
    }
}


// Sample url - http://www.yelp.com/search?find_loc=Chicago,+IL,+USA&start=0&cflt=restaurants&attrs=RestaurantsPriceRange2.1
if(empty($_GET['url'])) {
    die('ERROR: Please set a url in the query tag.');
}
$url = $_GET['url'];

$type = isset($_GET['type']) ? $_GET['type'] : '';
$scraper = new YelpScrapeBusiness($type);
if(isset($_GET['limit'])){
    $scraper->setLimit($_GET['limit']);
}
try {
    echo $scraper->getData($url);
} catch(Exception $e) {
    if(!empty($_GET['debug'])) {
        echo $e->getMessage();
    }
}
die();
