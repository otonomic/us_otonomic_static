<?php
/**
 * the main plugin file
 *
 * @package Dynamic Template Grid Builder
 * @since 0.1
 */
 
/*
Plugin Name: Dynamic Template Grid Builder
Description: This plugin is used to create our own grid builder with our dynamic templates chosen.
Version: 0.1
*/
require_once( 'includes/grid-controller.php' );

$grid = new GridController();
$grid->init();
?>