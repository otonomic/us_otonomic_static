<?php

class GridController
{
    private $dao;
	
    public function __construct() {
		require_once plugin_dir_path( __DIR__ ). 'dao/TemplatesDao.php';
		$this->dao = new TemplatesDao;
    }
	
	public function init() {
		add_filter( 'vc_grid_item_shortcodes', array($this, 'setShortCodeAttrs') );
		add_shortcode('template_grid', array($this, 'createShortCode'));
	}
	
	public function setShortCodeAttrs( $shortcodes ) {
		$values = $this->dao->getAllTemplates();
		$params = array(
				'type' => 'dropdown',
				'holder' => 'div',
				'class' => '',
				'heading' => __( 'List of Templates' ),
				'param_name' => 'text_heading',
				'value' => array_values($values),
				'save_always' => true,
			);
		$shortcodes['template_grid'] = array(
			'name' => __( 'Templates', 'js_composer' ),
			'base' => 'template_grid',
			'param_name' => 'hello',
			'category' => __( 'Content', 'js_composer' ),
			'description' => __( 'Dynamic Templates', 'js_composer' ),
			'post_type' => Vc_Grid_Item_Editor::postType(),
			'params' => array( $params )
		);
	   return $shortcodes;
	}
	
	public function createShortCode( $atts ) {
		return $this->dao->getTemplate( $atts['text_heading'] );
    }
	
}

?>