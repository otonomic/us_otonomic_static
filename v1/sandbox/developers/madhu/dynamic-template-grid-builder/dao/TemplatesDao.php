<?php
 
class TemplatesDao
{
    public function __construct() { }
	
	public function getAllTemplates() {
		$args = array(
		  'post_type'   => 'template',
		  'post_status' => 'publish'
		);
		$posts = get_posts($args);
		$values = array();
		foreach( $posts as $post){
			$values[$post->post_name] = $post->post_title;
		}
		return $values;
	}
	
	public function getTemplate( $template_name ) {
		$args = array(
		  'name'        => $template_name,
		  'post_type'   => 'template',
		  'post_status' => 'publish',
		  'numberposts' => 1
		);
		$my_posts = get_posts($args);
		if( $my_posts ) :
			$post_content = $my_posts[0]->post_content;
		endif;
		return $post_content;
	}
}

?>