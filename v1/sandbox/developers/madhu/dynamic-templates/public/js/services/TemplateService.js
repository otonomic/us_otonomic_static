angular.module('TemplateService', [])
	.factory('Templates', ['$http', function($http) {
		return {
			get : function() {
				return $http.get('/api/templates');
			},
			create : function(templateData) {
				return $http.post('/api/template', templateData);
			}
		}
	}]);