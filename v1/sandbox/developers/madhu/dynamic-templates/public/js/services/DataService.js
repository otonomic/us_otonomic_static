angular.module('DataService', [])
	.factory('Datas', ['$http', function($http) {
		return {
			get : function() {
				return $http.get('/api/datas');;
			},
			create : function(data) {
				return $http.post('/api/data', data);
			},
			extract : function(content) {
				var start = content.indexOf("{{");
				if(start < 0) return undefined;
				var end = content.indexOf("}}");
				var extracted = content.substring(start+2, end);
				return { value: extracted, next: end+2 };
			}
		}
	}]);