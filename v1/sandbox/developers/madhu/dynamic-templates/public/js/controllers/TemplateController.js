angular.module('TemplateModule', [])
	.controller('TemplateController', ['$scope', '$http', 'Templates', 
		function($scope, $http, Templates) {
			$scope.gridOptions = {
					data: 'templates',
					columnDefs: [
						{field:'name', displayName:'Name'}, 
						{field:'description', displayName:'Description'}, 
						{field:'content', displayName:'Content'}
					]
				};
			Templates.get()
				.success(function(data) {
					$scope.templates = data;
				});
				
			$scope.createTemplate = function() {

				if ($scope.formData.name != undefined && 
						$scope.formData.content != undefined) {
					
					Templates.create($scope.formData)
						.success(function(data) {
							$scope.formData = {};
							$scope.templates = data;
							$('#templateModal').modal('toggle');
						});
				}
			};
			
			
		}]);