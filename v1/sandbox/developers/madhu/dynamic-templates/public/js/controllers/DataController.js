angular.module('DataModule', [])
	.controller('DataController', ['$scope', '$compile', '$http', 'Datas', 'Templates',
		function($scope, $compile, $http, Datas, Templates) {
			$scope.formData = {};
			$scope.preview = "preview";
			$scope.gridOptions = {
					data: 'datas',
					columnDefs: [
						{field:'name', displayName:'Name'}, 
						{field:'description', displayName:'Description'}, 
						{field:'content', displayName:'Content'}
					]
				};
			Datas.get()
				.success(function(data) {
					$scope.datas = data;
				});
				
			Templates.get()
				.success(function(data) {
					$scope.templates = data;
				});
				
			$scope.createData = function() {

				if ($scope.formData.name != undefined) {
					
					Datas.create($scope.formData)
						.success(function(data) {
							$scope.formData = {};
							$scope.datas = data;
							$('#appModal').modal('toggle');
						});
				}
			};
			
			$scope.applyTemplate = function() {
				$scope.formData.content = Mustache.render($scope.preview, $scope.templateData);
			};
			
			$scope.loadTemplate = function(id) {
				
				$http.get('/api/template/' + id)
					.success(function(data) {
						$('#template_form').html('');
						var content = data.content;
						$scope.preview = data.content;
						
						while(result = Datas.extract(content)) {
							var html = '<input type="text" placeholder="'+result.value+'" ng-model="templateData.'+result.value+'" class="form-control input-sm text-center" />'
							$('#template_form').append(html);
							content = content.substring(result.next);
						}
						var button = '<button type="submit" class="btn btn-primary btn-sm" ng-click="applyTemplate()">Apply</button>';
						$('#template_form').append(button);
						$compile($('#template_form'))($scope);
						
					});
			}
		}]);