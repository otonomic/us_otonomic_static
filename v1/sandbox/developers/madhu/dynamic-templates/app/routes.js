var Template = require('./models/template');
var Data = require('./models/data');

function getTemplates(res){
	Template.find(function(err, templates) {

			if (err)
				res.send(err)

			res.json(templates);
		});
};

function getData(res){
	Data.find(function(err, datas) {

			if (err)
				res.send(err)

			res.json(datas); 
		});
};

module.exports = function(app) {

	app.get('/api/templates', function(req, res) {
		getTemplates(res);
	});
	
	app.get('/api/datas', function(req, res) {
		getData(res);
	});

	app.post('/api/template', function(req, res) {

		Template.create({
			name : req.body.name,
			description : req.body.description,
			content : req.body.content
		}, function(err, todo) {
			if (err)
				res.send(err);
			getTemplates(res);
		});

	});
	
	app.post('/api/data', function(req, res) {

		Data.create({
			name : req.body.name,
			description : req.body.description,
			content : req.body.content
		}, function(err, todo) {
			if (err)
				res.send(err);
			getData(res);
		});

	});
	
	app.get('/api/template/:template_id', function(req, res) {
		Template.findOne({
			_id : req.params.template_id
		}, function(err, temp) {
			if (err)
				res.send(err);
			
			res.json(temp);
		});
	});

	app.get('*', function(req, res) {
		res.sendfile('./public/index.html'); 
	});
};