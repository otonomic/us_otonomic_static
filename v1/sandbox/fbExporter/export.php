<?php
define('FB_TOKEN', "389314351133865|O4FgcprDMY0k6rxRUO-KOkWuVoU");

$ids = trim($_POST['ids']);
$fbIds = explode("\n", $ids);

$fbdata = array();
$fbdata[] = array(
    'pageid' => 'fanpage id',
    'photoid' => 'photo id',
    'link' => 'link',
    'width' => 'width',
    'height' => 'height',
    'likes' => '# likes',
    'created' => 'created date'
);
foreach($fbIds as $id){
    $id = trim($id);
    $api_url = "https://graph.facebook.com/v2.3/". $id ."/photos/uploaded?access_token=". FB_TOKEN."&limit=100";
    $data = make_request($api_url);
    $data = json_decode($data,TRUE);
    if(isset($data['paging']['next'])){
        $next_page = $data['paging']['next'];
        while($next_page){
            $nextData = make_request($next_page);
            $nextData = json_decode($nextData,TRUE);
            $nextImages = process_images($nextData['data']);
            $fbdata = array_merge($fbdata,$nextImages);
            if(isset($nextData['paging']['next'])){
                $next_page = $nextData['paging']['next'];
            }else{
                $next_page = false;
            }
        }
    }
    $images = process_images($data['data']);
    $fbdata = array_merge($fbdata,$images);
}

 
convert_to_csv($fbdata, 'facebook_pages_photos.csv', ',');

function convert_to_csv($input_array, $output_file_name, $delimiter)
{
    /** open raw memory as file, no need for temp files */
    $temp_memory = fopen('php://memory', 'w');
    /** loop through array  */
    foreach ($input_array as $line) {
        /** default php csv handler **/
        fputcsv($temp_memory, $line, $delimiter);
    }
    /** rewrind the "file" with the csv lines **/
    fseek($temp_memory, 0);
    /** modify header to be downloadable csv file **/
    header('Content-Type: application/csv');
    header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
    /** Send file to browser for download */
    fpassthru($temp_memory);
}

function process_images($photos){
    $data = array();
    foreach($photos as $photo){
        $data[] = array(
            'pageid' => $photo['from']['id'],
            'photoid' => $photo['id'],
            'link' => $photo['link'],
            'width' => $photo['images'][0]['width'],
            'height' => $photo['images'][0]['height'],
            'likes' => '',
            'created' => $photo['created_time']
        );
    }
    return $data;
}

function make_request($url){
     $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}
?>