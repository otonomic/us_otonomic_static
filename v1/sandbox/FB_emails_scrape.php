<?php
/**
 *
 * This class scrapes the reviews from any facebook page that has review.
 * Class Scrape
 */
class Scrape {

    private $response;
    private $respond;

     public function __construct(){
     }

    public function getEmail($page_id = null) {
        if(!$page_id) { return false; }

        $url = "https://m.facebook.com/page/about.php?id={$page_id}&refid=17";
        $result = $this->getResults($url);
        return json_encode([
            'page_id' => $page_id,
            'result' => $result ? $result : ""
        ]);
    }

     public function getResults($url){
         $this->response = $this->getHTML($url);

         preg_match('/mailto:([a-zA-Z0-9_.\-(&#064;)]*)/', $this->response, $matches);

         if(isset($matches[1])) {
             return ($matches[1]);
         } else {
             return $matches;
         }
     }

     /**
      * Returns the HTML section that need to be scraped
      * @param $url
      * @return DOMNodeList
      */
     private function getHTML($url){

         $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36';
//         $opts = array(
//             'http'=>array(
//                 'method'=>"GET",
//                 'user_agent'=>$userAgent
//             )
//         );
//         $context = stream_context_create($opts);
//         $response = file_get_contents($url, false, $context);


         $ch = curl_init();
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
             'Accept-Encoding: gzip',
             'user_agent: '. $userAgent ,
             'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
             'Accept-Language: he,he-IL;q=0.8,en-US;q=0.5,en;q=0.3',
             'Connection: keep-alive',
             'Cookie: datr=zKR5U1BknSnDm0-bsatr-_MC; fr=0BRfA8jLgPEqghfSC.AWU8OVd_BqKAye1y39rqMKODEQw.BTkYc6.tn.FOp.AWXyf2q_; lu=TApjse2VsABYGSYhTAdU48rw; locale=en_US; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAffhU5y3JKC7OsceZgbucWDytBDeTeHEEaBeBRwe4zUvP8OgI_Be1np3ruWhRaqP9IpgGt3ZRrjgFKC0Sco89AXG%26smuh%3D58956%26lh%3DAc-2GUma-3fcgyYH; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2FPlaytimePizzaLR; noscript=1'
         ));
         curl_setopt($ch,CURLOPT_ENCODING , "gzip");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
         curl_setopt($ch, CURLOPT_URL,$url);
         $response=curl_exec($ch);
         curl_close($ch);
         return $response;
     }

    function getDomObject() {
        $dom = new DOMDocument();
        @$dom->loadHTML($this->response);
        $xpath     = new DOMXPath($dom);

        //1376899755889899
        $results = $xpath->query("//li[contains(@class, 'tipsTimelineItem')]");

        if($results->length == 0){
            $results = $xpath->query("//li[contains(@class, 'uiUnifiedStory')]");
        }

        return $results;
    }

    /**
     * Returns the HTML section that need to be scraped
     * @param $url
     * @return DOMNodeList
     */
    private function getMobileHTML($url){

        $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36';
//         $opts = array(
//             'http'=>array(
//                 'method'=>"GET",
//                 'user_agent'=>$userAgent
//             )
//         );
//         $context = stream_context_create($opts);
//         $response = file_get_contents($url, false, $context);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept-Encoding: gzip',
            'user_agent: '. $userAgent ,
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: he,he-IL;q=0.8,en-US;q=0.5,en;q=0.3',
            'Connection: keep-alive',
            'Cookie: datr=zKR5U1BknSnDm0-bsatr-_MC; fr=0BRfA8jLgPEqghfSC.AWU8OVd_BqKAye1y39rqMKODEQw.BTkYc6.tn.FOp.AWXyf2q_; lu=TApjse2VsABYGSYhTAdU48rw; locale=en_US; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAffhU5y3JKC7OsceZgbucWDytBDeTeHEEaBeBRwe4zUvP8OgI_Be1np3ruWhRaqP9IpgGt3ZRrjgFKC0Sco89AXG%26smuh%3D58956%26lh%3DAc-2GUma-3fcgyYH; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2FPlaytimePizzaLR; noscript=1'
        ));
        curl_setopt($ch,CURLOPT_ENCODING , "gzip");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $response=curl_exec($ch);
        curl_close($ch);



        $dom = new DOMDocument();
        @$dom->loadHTML($response);
        $xpath     = new DOMXPath($dom);

        $results = $xpath->query("//");

        return $results;
    }
 }

// Example usage:
if(isset($_GET['page_id'])) {
    $page_id = $_GET['page_id'];
    $scraper = new Scrape();
    $result = $scraper->getEmail($page_id);
    print_r($result);
    // echo stripslashes(json_encode($result));
}
