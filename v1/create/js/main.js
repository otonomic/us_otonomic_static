var builder_domain;
var appid;
var fb_app_token = '373931652687761|d154036467714f4ac4706e653a1211ad';
if (is_localhost()) {
    builder_domain = "http://wp.test";
    appid = '286934271328156';
} else {
	builder_domain = 'http://wp.'+window.location.hostname.replace('www.', '');
    appid = '373931652687761';
}
function saveDataOnServer() {

    var id = Cookies.get('otonomic_id');
    var user_email = Cookies.get('user_email');
    var website_name = Cookies.get('user_business_name');
    var fb_user_token = Cookies.get('user_fb_token');
    var fb_user_details = Cookies.get('user_fb_details');
    var fb_user_pages = Cookies.get('fb_user_pages');
    var fb_selected_page = Cookies.get('fb_selected_page');

    var dataSent = {
        id: id,
        user_email:user_email,
        website_name:website_name,
        fb_user_token:fb_user_token,
        fb_user_details:fb_user_details,
        fb_user_pages:fb_user_pages,
        fb_selected_page:fb_selected_page
    };

    request = $.ajax({
        type: "POST",
        url: 'http://'+window.location.hostname + '/create/server/save-data.php',
        data: dataSent,
        success: function (data, status, jqxhr) {
            //console.log(data);
            data = jQuery.parseJSON(data);
            Cookies.set('otonomic_id', data.id, { expires: 30 });
            Cookies.set('user_website_domain', data.website_domain, { expires: 30 });

            fireDataLayerEvent(
                'savedDataOnServer',
                'installer',
                {
                    'dataSent':dataSent,
                    'datareceived':data
                }
            );

        }
    });
}
function loadFBPages(fb_token, supressSlide, pageSlide) {
    jQuery('#start-site-creation').attr('disabled', 'disabled');
    try {
        FB.api('/me/accounts', {access_token: fb_token,limit: 100, fields:'access_token,category,name,id,is_published,perms'}, function (response) {
            Cookies.set('fb_user_pages', response, {expires: 30});
            saveDataOnServer();
            var page = $('<div />');
            if(response.data.length < 1){
                window.location.hash = '#bother-no-page';
            }
            var pageTemplate = '<div data-id="{{page-id}}" data-name="{{page-name}}" data-category="{{page-category}}" class="fb-page-select clearfix"><div class="pull-left"><img width="40" height="40" src="{{page-image}}?width=80&height=80" /></div><div class="pull-left fb-page-details"><div>{{page-name}}</div></div></div>';
            var $is_published_error = true;
            var pageCount = 0;
            var pageError = true;
            $(response.data).each(function (index, value) {
                pageCount++;
                if(value.is_published) {
                    pageError = false;
                    var tpage;
                    tpage = pageTemplate;
                    tpage = tpage.replace(/{{page-id}}/g, value.id);
                    tpage = tpage.replace(/{{page-name}}/g, value.name);
                    tpage = tpage.replace(/{{page-category}}/g, value.category);
                    tpage = tpage.replace(/{{page-image}}/g, "https://graph.facebook.com/" + value.id + "/picture/");

                    tpage = $(tpage);
                    tpage.attr('data-fb-id', value.id);
                    page.append(tpage);
                }
            });
            if(pageError) {
                event_type = 'fbNoPagesFound';
                if (pageCount > 0 && pageError) {
                    pageSlide = 'bother-unpub';
                    event_type = 'fbNoPublishedPagesFound';
                } else if (pageCount == 0) {
                    pageSlide = 'bother-no-page';
                }

                fireDataLayerEvent(
                    event_type,
                    'installer',
                    { }
                );

            } else {

                fireDataLayerEvent(
                    'fbPagesFetched',
                    'installer',
                    {
                        'pages':response.data
                    }
                );

                page = page.wrap('<div/>').parent().html();
                jQuery('#page-list').html(page);
                $('#page-list .fb-page-select').bind('click', function (e) {
                    jQuery('#page-list .fb-page-select').removeClass('selected');
                    jQuery(this).addClass('selected');
                    jQuery('#start-site-creation').removeAttr('disabled');
                    $('#fb_page_confirm').val(jQuery(this).attr('data-name'));
                    //pageSelected($(this).attr('data-fb-id'));
                });
            }
            if(!supressSlide) {
                move_slide_by_id(pageSlide);
                window.location.hash = pageSlide;
            }
        });
    } catch(e){
        console.log(e);
    }
}
function modalCallback(event, modal) {
    //console.log(event, modal);
    if(event == 'modalSkipped' && modal=='selectPage') {
        var fb_token = FB.getAuthResponse()['accessToken'];
        Cookies.set('user_fb_token', fb_token, { expires: 30 });
        jQuery('#fb_user_token').val(fb_token);
        fields = 'id,name,first_name,last_name';
        FB.api('/me', {fields: fields}, function(response) {

            fireDataLayerEvent('fbUserDetailsFetched', 'facebook', {'fb_data':response});

            Cookies.set('user_fb_details', response, { expires: 30 });
            jQuery('#fb_user_id').val(response.id);
            jQuery('.replace-fbname').html(response.first_name);
            saveDataOnServer();
        });
        jQuery('#fb-next').removeAttr('disabled');
        loadFBPages(fb_token, false, 'pageselect')
    }
    fireDataLayerEvent(
        event,
        'facebook',
        {
            'modal':modal,
            'fb_user_details':Cookies.get('user_fb_details')
        }
    );
}
function pageSelected(page) {
    jQuery('#facebook_id').val(page);
    Cookies.set('fb_selected_page', page, { expires: 30 });

    window.location.hash = "#confirm";
    createWebsiteUsingAjax(page);
    fireDataLayerEvent(
        'pageSelected',
        'facebook',
        {
            'page-id':page
        }
    );
    saveDataOnServer();
}
/*function TriggerclickNext($selector){
    if($selector.length < 1){
        $selector = 'a.btn-next';
    }
    $hash = window.location.hash;
    if(jQuery($hash+' '+$selector).length < 1){
        return false;
    }
    
    if(jQuery($hash+' '+$selector).attr('disabled') != 'disabled'){
        jQuery($hash+' '+$selector).trigger('click');
        $href = jQuery($hash+' '+$selector).attr('href');
        if(typeof $href != 'undefined' && $href != 'javascript:void(0)'){
            window.location.hash = jQuery($hash+' '+$selector).attr('href');
        }
    }
}*/
function activateEmailNext() {
    if(!is_valid_email(jQuery('#user_email').val())) {
        jQuery('#email-next').attr('disabled', 'disabled');
    } else {
        jQuery('#email-next').removeAttr('disabled');
    }
}
function activateBNameNext() {
    jQuery('#business_name-text-limit').hide();
    jQuery('#business_name').removeClass('overlimit');
    if(jQuery('#business_name').val().length<3) {
        jQuery('#bname-next').attr('disabled', 'disabled');
    } else if(jQuery('#business_name').val().length>30) {
        jQuery('#bname-next').attr('disabled', 'disabled');
        jQuery('#business_name-text-limit').show();
        jQuery('#business_name').addClass('overlimit');
    } else {
        jQuery('#bname-next').removeAttr('disabled');
    }
}

function activateFBNext() {
    jQuery('#fb-next').attr('disabled', 'disabled');
    var fb_token = Cookies.get('user_fb_token');
    if(fb_token) {
        /* Check if token is valid */
        try {
            FB.api('/me', {access_token: fb_token,limit: 100}, function (response) {
                jQuery('#fb-next').removeAttr('disabled');
            });
        } catch(e) {
            console.log(e);
        }
    }
}
// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
window.fbAsyncInit = function() {
    FB.init({
        appId      : appid,
        cookie     : true,
        xfbml      : true,
        version    : 'v2.5'
    });
    jQuery('.oto-fb-login').otoFBLogin({
        template: {
            'appRejected':{
                'view': '/create/popup-view/app-rejected.html',
                'callback': modalCallback,
                popup: {
                    load:false
                }
            },
            'premissionRejected':{
                'view': '/create/popup-view/premission-rejected.html',
                'callback': modalCallback,
            },
            'selectPage':{
                'view': '/create/popup-view/select-page.html',
                'callback': modalCallback,
                popup: {
                    load:false
                }
            }
        },
        permissions: 'email,public_profile,pages_show_list',/* pages_show_list permission is required */
        //onPageSelect: pageSelectCallback,
        onPageSelect: false,
        onProcessStart: function() {
            fireDataLayerEvent('fbLoginStarted', 'facebook', {});
            //console.log('FB login process initiated');
        }
    });

    if(Cookies.get('user_fb_details')) {
        user_fb_details = jQuery.parseJSON(Cookies.get('user_fb_details'));
        jQuery('.replace-fbname').html(user_fb_details.first_name);
    }
    if(Cookies.get('user_fb_token')) {
        loadFBPages( Cookies.get('user_fb_token'), true, '' );
        activateFBNext();
    }
};

function is_valid_email(email)
{
     return email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/);
}

function move_slide_by_id(id) {
    if(!id)
        id = "email";
    if($(".installer-stage:visible").attr('id') == id)
        return;
    $(".installer-stage:visible").fadeOut(100, function () {
        jQuery('#'+id).removeClass('hidden').fadeIn(100, function(){
            $('.installer-stage:visible:first').find('input[type=text],input[type=email],textarea,select,.oto-fb-login').filter(':visible:first').focus();
        });
    });
}

(function ($, window, undefined) {
    start_slide = $.param.fragment();
    if(start_slide) {
        move_slide_by_id(start_slide);
    }
    $(window).bind( 'hashchange', function(e) {
        var id = $.param.fragment();
        move_slide_by_id(id);
        Cookies.set('oto_stage', id, {expires: 30});
    });

    var settings = {
        user_edits_contact: false,
        user_edits_store: false,
        user_edits_booking: true
    };

	window.do_redirect = 0;

	var page_id = getParameterByName('page_id');
	var page_name = getParameterByName('page_name');
    var category = getParameterByName('category');
    var category_list = getParameterByName('category_list');

	window.authorized_channel = [];
	
	var page_load_timestamp;
	
	var contact_load_timestamp;
	var store_load_timestamp;
	var category_load_timestamp;
	
	page_load_timestamp = new Date();

    if (page_name) {
        $('.site-name').html(page_name);
        $('.ot-fb-name').html(page_name);
    }

    if(category) {
        $('#fb_category').val(category);
    }

    track_event('Loading Page', 'Start');
	track_virtual_pageview('installer_start');
	jQuery('input[type=text]').addClass('LoNotSensitive');

    function move_slide(pressed_button, event) {
        var current_slide = pressed_button.parents('.installer-stage');
        $('.notice').hide();
        fireDataLayerEvent('nextSlide', 'installer', {});
        // Check for required fields
        var required_fields = pressed_button.attr('data-required-fields');
        if(required_fields && required_fields.length) {
            required_fields = required_fields.split(',');
            var can_move_slide = true;
            $.each(required_fields, function (index, value) {
                if (!$(value).val()) {
                    $(value + '-notice').show();
                    can_move_slide = false;
                }
                if($(value).val() && (value == '#user_email' || value == '#user_email_confirm')){
                    if(!is_valid_email($(value).val())){
                        $(value + '-valid-notice').show();
                        can_move_slide = false;
                    }
                }
            });
            if (!can_move_slide) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }

        // Move to next slide
        track_event('Loading Page', 'Next', current_slide.attr('id'));
        current_slide.fadeOut(100, function () {
            current_slide.next().removeClass('hidden').fadeIn(100);
            $('html, body').animate({
                scrollTop: 0
            }, 500);
        });
        saveDataOnServer();
    }

    // Intro next btn - Let the magic begin
    $('.btn-back').click(function(event) {
        var $this = $(this);
        var current_slide = $this.parents('.installer-stage');
        track_event('Installer', 'Back', current_slide.attr('id'));
        //event.preventDefault();

        current_slide.fadeOut(100, function () {
            current_slide.addClass('hidden');
        });
        current_slide.prev().removeClass('hidden').fadeIn(100);
    });

    // Stage next btn
    $('.btn-next').click(function(event){
        //event.preventDefault();
        move_slide( $(this), event);
    });

	// Stage-3 next btn - Store/Booking
	$('.js-stage3-next').click(function(event){
        //event.preventDefault();
        move_slide( $(this), event);

        var values = {};
        values.show_store = $('#option-online-store').hasClass('checked') ? 'yes' : 'no';
        values.show_booking = $('#option-booking').hasClass('checked') ? 'yes' : 'no';
        enqueue_submit('show_store',   values.show_store,   'send_store');
        return enqueue_submit('show_booking', values.show_booking, 'send_booking');
	});

    $(".js-switch-to-congratz").click(function(event){
        //event.preventDefault();
        initializeLoading();
        window.location.hash = "#gather";
        move_slide( $(this), event);
        $('.websitedomain').html( Cookies.get('user_website_domain') );

        switchToCongratz();
        //$('#User_site_creation').submit();
    });

    $('#User_site_creation').submit(function(){

        return false;
    });


    jQuery('#link-tos').click(function (e){
		track_event('Loading Page', 'ToS', '');
	});

    $('[data-toggle="tooltip"]').tooltip();

	// function that switched to stage-5 from stage-4
	/////////////////////////////////////////////////////
	function switchToCongratz() {
        var form_data = $('#User_site_creation').serialize();

        var request_url;
        request_url = builder_domain+"/migration/index.php";

        enqueue_submit('blogname',   jQuery('#business_name_confirm').val(),   'business_name');
        enqueue_submit('user_email',   jQuery('#user_email_confirm').val(),   'submit_user_email');

        /*console.log(form_data);
        return false;*/

        /*$.ajax({
            url: request_url,
            type: "GET",
            dataType: "jsonp",
            data: form_data,
            jsonp: "callback",
            jsonpCallback: "callback"
        });*/

		// CountDown
		/*var sec = 10;
		var timer = setInterval(function() { 
			if (sec > 1) {
				$('#congratz #counter').text(sec+' seconds');
                sec--;

            } else {
				$('#congratz #counter').text('1 second');
                sec--;

				if (sec < -2) {
					$('.congratz-title').text('Taking you to your new website...');
                    $('.ot-fb-name').html('');
                    $('.site-name').html('');

                    clearInterval(timer);

					// now redirect
                    //window.location.replace(window.site_url);
				}
			}

            if(sec == 4) {
				ga('set', 'metric5', '1');
                track_event('Loading Page', 'Redirect to website', '');
                window.do_redirect = 1;
                redirect_to_website();
            }
		}, 1000);*/
    }


	if (page_id) {
        window.site_url = builder_domain+'/wp-admin/admin-ajax.php?action=check_page&page_id='+page_id;
        $('#oto-web-url').html('<a href="'+window.site_url+'">this link</a>');
		createWebsiteUsingAjax(page_id);
        if(settings.user_edits_address) {
		    getFacebookPageAddress(page_id);
        }
	}

    // typeahead
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                }
            });

            cb(matches);
        };
    };

    $("#user_email").on("keyup paste", function() {
        $("#user_email_confirm").val($(this).val());
        Cookies.set('user_email', $(this).val(), { expires: 30 });
        activateEmailNext();
    });
    $("#user_email_confirm").on("keyup paste", function() {
        $("#user_email").val($(this).val());
        Cookies.set('user_email', $(this).val(), { expires: 30 });
    });

    $("#business_name").on("keyup paste", function() {
        $("#business_name_confirm").val($(this).val());
        $('.replace-sitename').html($(this).val());
        Cookies.set('user_business_name', $(this).val(), { expires: 30 });
        activateBNameNext();
    });
    $("#business_name_confirm").on("keyup paste", function() {
        $("#business_name").val($(this).val());
        $('.replace-sitename').html($(this).val());
        Cookies.set('user_business_name', $(this).val(), { expires: 30 });
    });
    $("#user_email, #user_email_confirm").val( Cookies.get('user_email') );
    $("#business_name, #business_name_confirm").val( Cookies.get('user_business_name') );
    $('.replace-sitename').html(Cookies.get('user_business_name'));
    if(Cookies.get('oto_stage') != undefined && !disable_automove) {
        window.location.hash = "#" + Cookies.get('oto_stage');
    }else{
        window.location.hash = "#email";
    }
    activateEmailNext();
    activateBNameNext();
    $('#start-site-creation').bind('click', function(e){

        var selected_fb_page = jQuery('#page-list .fb-page-select.selected').attr('data-fb-id');
        pageName = jQuery('#page-list .fb-page-select.selected').attr('data-name');
        pageCategory = jQuery('#page-list .fb-page-select.selected').attr('data-category');

        /* Verify this page */
        try {
            FB.api('/'+selected_fb_page, {access_token: fb_app_token}, function (response) {
                if(response.id == selected_fb_page) {
                    pageSelected(selected_fb_page);
                } else {
                    pageSlide = 'bother-blocked';
                    move_slide_by_id(pageSlide);
                    window.location.hash = pageSlide;
                }
                console.log(response);
            });
        } catch(e) {

        }
    });
    $('.installer-stage:visible:first').find('input[type=text],input[type=email],textarea,select,.oto-fb-login').filter(':visible:first').focus();

    $('#user_email').keypress(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#email-next").simulateClick();
        }
    });
    $('#business_name').keypress(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#bname-next").simulateClick();
        }
    });

    $('.reload-fb-pages').bind('click', function (e){
        loadFBPages(Cookies.get('user_fb_token'), false, 'pageselect');
    });
    
    /*jQuery(document).keyup(function(e){
        if(window.location.hash == '#pagepull' && e.keyCode == '13'){
            jQuery('a.oto-fb-login').click();
        } else if(e.keyCode == '13'){
            if(window.location.hash == '#pageselect'){
                TriggerclickNext('#start-site-creation');
            }else if(window.location.hash == '#confirm'){
                TriggerclickNext('#create-website');
            }else{
                TriggerclickNext('');
            }
        }
    });*/
    $('.edit-link').bind('click', function(e){
        $href = '#confirm';
        if( $('#page-list .fb-page-select').length > 1 ) {
            $href = '#pageselect';
        }
        $('#bname-next').attr('href', $href);
    });
    $('.websitedomain').html( Cookies.get('user_website_domain') );

    $('#email-next').bind('click', function (e){
        fireDataLayerEvent(
            'emailFilled',
            'installer',
            {
                'email':$('#user_email').val()
            }
        );
    });
    $('#bname-next').bind('click', function (e){
        fireDataLayerEvent(
            'businessNameFilled',
            'installer',
            {
                'name':$('#business_name').val()
            }
        );
    });

})(jQuery, window);


/* required functions */
function timed_submit(submit_function, submit_parameter) {
    if (window.is_blog_ready == 1) {
        submit_function();
    } else {
        window[submit_parameter] = 1;
    }
}

function callback(data) {
	window.is_blog_ready = 1;

	if (data.redirect.indexOf("http://") < 0) {
		data.redirect = "http://" + data.redirect;
	}

	if (data.site_url.indexOf("http://") < 0) {
		data.site_url = "http://" + data.site_url;
	}

	if (data.status == 'fail') {
		//window.location = data.site_url;
        console.log('Site Exists');
        fireDataLayerEvent(
            'siteExists',
            'installer',
            {
                'message':data.message,
                'url':data.site_url
            }
        );
		track_event('Account Manage', 'Site Exists', data.message);
		ga('set', 'metric6', '1');
        track_virtual_pageview('site_exists');

	} else {
		var page_type = window.page_type || 'Fan Page';

        fireDataLayerEvent(
            'siteCreated',
            'installer',
            {
                'url':data.site_url
            }
        );

		track_event('Account Manage', 'Site Created', page_type);
		ga('set', 'metric4', '1');
        track_virtual_pageview('site_created');
	}

    <!-- START Facebook Pixel Tracking for Site created-->
	window._fbq = window._fbq || [];
    if(!is_localhost()) {
	    window._fbq.push(['track', facebook_site_created_pixel_id, {'value':'0.00', 'currency':'USD'}]);
    }
    <!-- END Facebook Pixel Tracking -->

	window.site_url = data.site_url;
	// window.blog_redirect = data.redirect;
    window.blog_redirect = data.site_url;
	window.blog_id = data.blog_id;
	window.token = data.token;

	jQuery('#oto-web-url').html('<a href="'+data.redirect+'">'+data.site_url+'</a>');

    if( data.status === 'fail') {
        /* Switch to error window */
        if(data.message != 'Sorry, that site already exists!') {
            if (progressTimer)
                clearInterval(progressTimer);
            /* Now move to error slide */

            move_slide_by_id('bother-stuck');
            window.location.hash = '#bother-stuck';
            if (data.message)
                jQuery('#gen-error-message').html(data.message);

            /*if(data.message != 'Sorry, that site already exists!') {
             alert(data.message);
             }*/
            //window.location.replace(data.redirect);
        }
        return;
    }

	blog_created();

    redirect_to_website();
}

function business_name() {
    return post_WP_settings({ blogname: jQuery('#business_name_confirm').val() });
}
function submit_user_email() {
    var user_email = window.user_email || '';
    track_event('Installer', 'Submit Email', user_email);
    var user_id = window.user_id;
    return post_WP_settings({ user_email: user_email, user_id: user_id }, 'Submit Email', 'settings.set_user_email');

}
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getFacebookPageAddress(page_id) {
	var facebook_query_page_url = "https://graph.facebook.com/" + page_id;
	$.get(facebook_query_page_url, function (data) {
		if (data.location != undefined && data.location.latitude != undefined && data.location.longitude != undefined) {
			delete data.location.latitude;
			delete data.location.longitude;
		}
		var address_parts = [];

		for (var x in data.location) {
			address_parts.push(data.location[x]);
		}

		var phone = (data.phone) ? data.phone : "";
		var address = (address_parts.join(", ")) ? address_parts.join(", ") : "";
		var email = (data.email) ? (data.email) : "";

		if (data.likes != undefined) {
			window.page_type = 'Fan Page';
		} else {
			window.page_type = 'Personal Page';
		}
		window.parsed_page_data = {
			'phone': phone,
			'address': address,
			'email': email
		}
	}, "json");
}

function is_localhost() {
	if (location.host == 'otonomic.test' || location.host == 'localhost') {
		return true;
	}

	return false;
}




function createWebsiteUsingAjax(page_id) {
	var request_data = {};
	request_data.theme = "dreamthemeVC";
	request_data.facebook_id = encodeURIComponent(page_id);

	// var request_url = "http://wp.otonomic.com/migration/index.php?" + $.param(request_data);
	localhost = is_localhost();

	var request_url;
    request_url = builder_domain+"/migration/index.php";

	return $.ajax({
		url: request_url,
		type: "GET",
		dataType: "jsonp",
		data: request_data,
		jsonp: "callback",
		jsonpCallback: "callback"
	});
}

function send_template() {
    var skin = window.skin || '';
    track_event('Loading Page', 'Select Template', skin);
    return post_WP_settings({ skin: skin }, 'Select Template');
}

function blog_created() {
    fireDataLayerEvent(
        'blogCreated',
        'installer',
        {}
    );
    window.callbacks = window.callbacks || [];
    $.each( window.callbacks, function(index, callback_function) {
        window[callback_function]();
    });

	send_user_fb_details();
	return;
}

function redirect_to_website() {
	if(window.do_redirect == 1 && window.is_blog_ready == 1) {
        window.location.replace(window.blog_redirect);
	}
}


function post_WP_settings(data, tracking_action, endpoint) {
    endpoint = endpoint  || 'settings.set_many';
    tracking_action = tracking_action  || data;

    return request = $.ajax({
        type: "POST",
        url: window.site_url + '/?json=' + endpoint,
        data: { values: data },
        success: function (data, status, jqxhr) {
            if (jqxhr.status == 307) {
                $.post(window.site_url + '/?json=settings.set_many', { values: values_changes });
                track_event('Loading Page', tracking_action, '307');
                return;
            }
            if (data.status == "ok") {
                track_event('Loading Page', tracking_action, 'Success');
            } else {
                track_event('Loading Page', tracking_action, 'Failure: data.respond.msg: ' + (data.respond && data.respond.msg));
            }
        },
        complete: function (jqxhr, status) {
            if (status !== 'success') {
                track_event('Loading Page', tracking_action, 'Failure: ' + status);
            }
        }

//		statusCode: {
//			200: function (data_or_jqxhr, status, jqxhr_or_err) {debugger;
//				return;
//			},
//			307: function (data_or_jqxhr, status, jqxhr_or_err) {debugger;
//				$.post(window.site_url + '/?json=settings.set_many', { values: values_changes });
//			}
//		}
    });
}

function enqueue_submit(setting, value, callback_function) {    
    window[setting] = value;

    if(window.is_blog_ready) {
        window[callback_function]();

    } else {
        window.callbacks = window.callbacks || [];
        window.callbacks.push(callback_function);
    }
}

function send_user_fb_details()
{
	fb_user_auth = 'yes';
	fb_user_id = jQuery('#fb_user_id').val();
	fb_user_t = jQuery('#fb_user_token').val();

	if(fb_user_auth == 'yes')
	{
		var settings_data = {
			wp_otonomic_blog_connected: 'yes',
			otonomic_connected_fb_user_id: fb_user_id,
			otonomic_connected_fb_user_token: fb_user_t
		};
		post_WP_settings(settings_data, 'FB Connected');
	}
}
function fireDataLayerEvent(event, method, data) {

    var Layerdata = {
        'event': event,
        'method': method
    };

    Layerdata = $.extend(true, {}, Layerdata, data);

    try {
        dataLayer.push(Layerdata);
    } catch(e) {
        console.log(e);
    }
}
$.fn.simulateClick = function() {
    return this.each(function() {
        if('createEvent' in document) {
            var doc = this.ownerDocument,
                evt = doc.createEvent('MouseEvents');
            evt.initMouseEvent('click', true, true, doc.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
            this.dispatchEvent(evt);
        } else {
            this.click(); // IE
        }
    });
};