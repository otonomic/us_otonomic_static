<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$link = mysqli_connect("dbmaster.otonomic.com", "root", "56#0BM]%a59(7*e4*", "otonomic_com");

$id = isset($_POST['id'])?$_POST['id']:false;
$user_email = addslashes(@$_POST['user_email']);
$website_name = addslashes(@$_POST['website_name']);
//$website_domain = @$_POST['website_domain'];
$fb_user_token = @$_POST['fb_user_token'];
$fb_user_details = addslashes(@$_POST['fb_user_details']);
$fb_user_pages = addslashes(@$_POST['fb_user_pages']);
$fb_selected_page = @$_POST['fb_selected_page'];
$website_domain = '';
if($website_name) {
    $website_domain = $website_name;
    $is_hebrew =  preg_match("/\p{Hebrew}/u", $website_domain);
    if($is_hebrew){
        $website_domain = '';
    } else {
        $encoding = mb_detect_encoding($website_domain,'auto');
        $website_domain = iconv('UTF-8','ASCII//TRANSLIT',$website_domain);
        $website_domain = preg_replace('/[^A-Za-z0-9\-]/', '', $website_domain);
        $website_domain = trim($website_domain,'-');
    }
}

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $user_ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $user_ip = $_SERVER['REMOTE_ADDR'];
}
if(!$id) {
    /* Test is email already exists */
    $sql = "select * from otonomic_onboarding where user_email = '{$user_email}'";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);

    if(count($row)>0) {
        /* Email found in DB */
        /* fetch user id from DB */
        $id = $row['id'];
    }
}
$now = date('Y-m-d H:i:s');
if($id) {
    $sql = "update otonomic_onboarding set user_email = '{$user_email}',
            website_name = '{$website_name}',
            website_domain = '{$website_domain}',
            fb_user_token = '{$fb_user_token}',
            fb_user_details = '{$fb_user_details}',
            fb_user_pages = '{$fb_user_pages}',
            fb_selected_page = '{$fb_selected_page}',
            modified_on = '{$now}'
    ";
} else {


    $sql = "insert into otonomic_onboarding (user_email, website_name, website_domain, fb_user_token, fb_user_details, fb_user_pages, fb_selected_page, user_ip, created_on, modified_on) values (
            '{$user_email}', '{$website_name}', '{$website_domain}', '{$fb_user_token}', '{$fb_user_details}', '{$fb_user_pages}', '{$fb_selected_page}', '{$user_ip}', '{$now}', '{$now}')
    ";
}
mysqli_query($link, $sql);
if(!$id){
    $id = mysqli_insert_id($link);
}
mysqli_close($link);
echo json_encode(
    array(
        'id'=>$id,
        'website_domain'=>$website_domain
    )
);

exit;