<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-scalable=0">
    <meta name="description" content="Otonomic Site Creation Page">
    <meta name="author" content="Otonomic">
    <link rel="shortcut icon" href="favicon.png">

    <title>Otonomic is creating your site...</title>

      <!-- Bootstrap core CSS -->
      <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

      <!-- Glyphicons -->
<!--
      <link href="fonts/Glyphicons-WebFont/glyphicons.css" rel="stylesheet">
-->
      <!--<link rel='stylesheet' id='sb_instagram_icons-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css?ver=4.2.0' type='text/css' media='all' />-->

      <!-- Aller font -->
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
      <link href="js/colorbox/theme2/colorbox.css" rel="stylesheet">
      <link href="/shared/fb_modal/css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/wp-loading-page.css" rel="stylesheet">

      <!--<script src="js/jquery-1.11.1.min.js"></script>-->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.js"></script>
      <!--<script src="js/bootstrap.min.js"></script>-->
      <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

      <script src="js/typeahead.jquery.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.0.4/js.cookie.min.js"></script>

      <script src="js/colorbox/jquery.colorbox.js"></script>
      <script src="/shared/fb_modal/js/otoFBLogin.jquery.js"></script>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

      <![endif]-->
      <script>
          var disable_automove = false;
      <?php
      if(isset($_POST['email'])) {
          /* Set email in cookies */
            setcookie('user_email',$_POST['email'], time()+60*60*24*30, '/');
          ?>
      disable_automove = true;
          jQuery(document).ready(function (e){
              jQuery('#email-next').trigger('click');
              jQuery('#user_email-notice').hide();
          });
        <?php
      }
      ?>
      </script>
      <script src="/v1/js/jquery.ba-bbq.min.js"></script>

      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-37736198-1', 'auto');
          ga('set', 'dimension4', window.location.hostname); // Site url
          ga('set', 'dimension5', 'otonomic marketing site'); // Site Type
          ga('send', 'pageview');
      </script>

    <script src="/v1/js/otonomic-analytics.js?v=1.0"></script>

      <style>
          body {
              direction: ltr;
              text-align: left;
          }
      </style>
  </head>

  <body class="wp-lp">
      <!-- Google Tag Manager -->
      <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK43MV"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WK43MV');</script>
      <!-- End Google Tag Manager -->


    <!-- Intro    ========================================================== -->
    <div class="container-fluid">
      <form id="User_site_creation">

      <input type="hidden" name="source" id="source" value="facebook">
      <input type="hidden" name="theme"  id="theme"  value="dreamthemeVC">
      <input type="hidden" name="lang"  id="lang"  value="en_US">


      <!-- Stage ================================================ -->
      <div id="email" class="row installer-stage">
          <div class="bg-image hidden-xs" style="
            background-image: url(images/night-sky.jpg);
            background-position-x: 58%;
          ">
              <div class="action-links">
                  Already signed up? <a href="http://wp.otonomic.com/wp-login.php">Log in</a>
              </div>

              <p class="user-concern">
                  By proceeding to create your account and use Otonomic, you are agreeing to our <a href="http://us.otonomic.com/terms-of-service/" target="_blank">Terms of Service</a> and <a href="http://us.otonomic.com/privacy/" target="_blank">Privacy Policy</a>. If you do not agree, you can not use Otonomic.
              </p>

          </div>
          <div class="content-panel align-rows">
              <div class="top-row">
                <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title">
                      Create a new Website
                  </h1>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <label class="marbtm8" for="email">Sign up with your <strong>e-mail address</strong></label>
                              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" class="tooltip"
                                 title="The email address will appear on your site. Contact messages will also be emailed to this address."></i>
                              <div id="user_email-notice" class="notice">
                                  Please fill this field
                              </div>
                              <div id="user_email-valid-notice" class="notice">
                                  Sure that's the right email address?
                              </div>
                              <input type="email" class="form-control" id="user_email" name="user_email" autocomplete="off" value="" placeholder="e.g. you@yourdomain.com">
                              <div class="tip">
                                  (Don't worry about setting a password right now, we'll e-mail you a link to create one)
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="bottom-row">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="action-buttons">
                              <a href="#sitename" id="email-next" class="btn-next pull-opposite btn btn-ttc btn-lg to-social-page validate-required" data-required-fields="#user_email">
                                  Next
                                  <span class="glyphicon glyphicon-arrow-right"></span>
                              </a>
                          </div>
                          <div class="nav-buttons">
                              <ul>
                                  <li class="active"><span></span></li>
                                  <li ><span></span></li>
                                  <li ><span></span></li>
                                  <li ><span></span></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <!-- Stage ================================================ -->
      <div id="sitename" class="row hidden installer-stage">
          <div class="bg-image hidden-xs" style="
            background-image: url(images/night-sky.jpg);
            background-position-x: 50%;
          ">
              <div class="action-links">
                  Already signed up? <a href="http://wp.otonomic.com/wp-login.php">Log in</a>
              </div>
          </div>
          <div class="content-panel align-rows">
              <div class="top-row">
                <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title">
                      Name your Site
                  </h1>
                  <p>What do you want to be called? This should be your business name and how you'll be found online.</p>
                  <!--<form role="form" id="business-details" action="">-->
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <label class="marbtm8" for="business_name">
                                  <strong>Website name</strong> (you can always change this later)
                              </label>
                              <div id="business_name-notice" class="notice">
                                  Please enter the name of your business
                              </div>
                              <input type="text" id="business_name" name="business_name" class="form-control business_name" autocomplete="off" placeholder="Ex. Acme or Acme Catering" />
                              <div id="business_name-text-limit" class="notice notice-orange">
                                  Name must be 30 characters or less (in English)
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="bottom-row">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="action-buttons">
                              <!-- a href="#email" class="btn btn-ttc-clear btn-back">
                                  <span class="glyphicons undo"></span>
                                  Back
                              </a -->
                              <a href="#pagepull" id="bname-next" class="btn-next pull-opposite btn btn-ttc btn-lg to-social-page validate-required" data-required-fields="#business_name">
                                  Next
                                  <span class="glyphicon glyphicon-arrow-right"></span>
                              </a>
                          </div>
                          <div class="nav-buttons">
                              <ul>
                                  <li class="active"><span></span></li>
                                  <li class="active"><span></span></li>
                                  <li ><span></span></li>
                                  <li ><span></span></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <!--</form>-->
          </div>
      </div>

      <!-- Stage ================================================ -->
      <div id="pagepull" class="row hidden installer-stage">
          <div class="bg-image hidden-xs" style="
            background-image: url(images/night-sky.jpg);
            background-position-x: 37%;
          ">
              <div class="action-links">
                  Already signed up? <a href="http://wp.otonomic.com/wp-login.php">Log in</a>
              </div>
          </div>
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title">
                      Pull your Content
                  </h1>
                  <p><strong>Auto-magically design your website</strong> and instantly add your business content by connecting your Facebook Page.</p>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <label class="marbtm8">
                                  <strong>Connect your Facebook Page</strong><br/>(you can add other social networks later.)
                              </label>
                              <div class="wrapper-oto-fb-login">
                                  <a href="javascript:void(0)" class="oto-fb-login">
                                    <img src="images/facebook.jpg" />
                                  </a>
                              </div>
                              <div class="tip">
                                  We'll never post to Facebook.
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
              <div class="bottom-row">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="action-buttons">
                              <a href="#pageselect" id="fb-next" class="btn-next pull-opposite btn btn-ttc btn-lg to-social-page validate-required" disabled="disabled" >
                                  Next
                                  <span class="glyphicon glyphicon-arrow-right"></span>
                              </a>
                          </div>
                          <div class="nav-buttons">
                              <ul>
                                  <li class="active"><span></span></li>
                                  <li class="active"><span></span></li>
                                  <li class="active"><span></span></li>
                                  <li ><span></span></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Stage ================================================ -->
      <div id="pageselect" class="row hidden installer-stage">
          <div class="bg-image hidden-xs" style="
        background-image: url(images/night-sky.jpg);
        background-position-x: 37%;
      ">
              <div class="action-links">
                  Already signed up? <a href="http://wp.otonomic.com/wp-login.php">Log in</a>
              </div>
          </div>
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title">
                      Which Page to use?
                  </h1>
                  <p><strong>Pick the Facebook Page</strong> you'd like to create your new website from. (You'll be able to create more sites later.)</p>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <div id="page-list">
                                  Loading...
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="bottom-row">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="action-buttons">
                              <!-- a href="#email" class="btn btn-ttc-clear btn-back">
                                  <span class="glyphicons undo"></span>
                                  Back
                              </a -->
                              <a href="javascript:void(0)" id="start-site-creation" class="pull-opposite btn btn-ttc btn-lg to-social-page validate-required" disabled="disabled" >
                                  Next
                                  <span class="glyphicon glyphicon-arrow-right"></span>
                              </a>
                          </div>
                          <div class="nav-buttons">
                              <ul>
                                  <li class="active"><span ></span></li>
                                  <li class="active"><span ></span></li>
                                  <li class="active"><span ></span></li>
                                  <li ><span ></span></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Stage ================================================ -->
      <div id="confirm" class="row hidden installer-stage">
          <div class="bg-image hidden-xs" style="
            background-image: url(images/night-sky.jpg);
            background-position-x: 37%;
          ">
              <div class="action-links">
                  Already signed up? <a href="http://wp.otonomic.com/wp-login.php">Log in</a>
              </div>
          </div>
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title">
                      Confirm your Details
                  </h1>
                  <!--<form role="form" id="business-details" action="">-->
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="form-group">
                              <label for="user_email_confirm"><strong>Email</strong></label>
                              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" class="tooltip" title="The email address will appear on your site. Contact messages will also be emailed to this address."></i>
                              <div id="user_email_confirm-notice" class="notice">
                                  Please fill this field
                              </div>
                              <div id="user_email_confirm-valid-notice" class="notice">
                                  Sure that's the right email address?
                              </div>
                              <div class="edit-link-wrapper">
                                  <input type="email" class="form-control confirm-field" id="user_email_confirm" name="user_email_confirm" value="<?php echo @$_POST['email'] ?>" autocomplete="off" disabled="disabled">
                                  <a href="#email" class="edit-link">Edit</a>
                              </div>

                          </div>
                          <div class="form-group">
                              <label for="business_name_confirm">
                                  <strong>Website name</strong>
                              </label>
                              <div id="business_name_confirm-notice" class="notice">
                                  Please enter the name of your business
                              </div>
                              <div class="edit-link-wrapper">
                                  <input type="text" id="business_name_confirm" name="business_name_confirm" class="form-control business_name confirm-field" autocomplete="off" placeholder="Ex. Acme or Acme Catering" autocomplete="off" disabled="disabled" />
                                  <a href="#sitename" class="edit-link">Edit</a>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="fb_page_confirm">
                                  <strong>Facebook Page</strong>
                              </label>
                              <div class="edit-link-wrapper">
                                  <input type="text" id="fb_page_confirm" name="fb_page_confirm" class="form-control business_name confirm-field" autocomplete="off" placeholder="Ex. Acme or Acme Catering" disabled="disabled" />
                                  <a href="#pageselect" class="edit-link">Edit</a>
                              </div>
                          </div>
                          <div class="checkbox">
                              <label for="confirm_subscription"><input type="checkbox" id="confirm_subscription" name="confirm_subscription" checked="checked">
                                  It's ok to send me (very occasional) email about the Otonomic service.
                              </label>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="bottom-row">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="submit-button">
                              <input type="hidden" name="fb_user_id" id="fb_user_id" value="">
                              <input type="hidden" name="facebook_id" id="facebook_id" value="">
                              <input type="hidden" name="fb_user_token" id="fb_user_token" value="">
                              <input type="submit" id="create-website" name="create-website" class="pull-opposite btn btn-lg js-switch-to-congratz" data-required-fields="#user_email_confirm,#business_name_confirm" value="Looks good">
                          </div>
                      </div>
                  </div>
              </div>
              <!--</form>-->
          </div>
      </div>
      
      <!-- Congratz ========================================================== -->
      <div id="gather" class="row hidden installer-stage">
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="congratz-title title text-center">This is going to be fantastic.</h1>
                  <div id="scrolling-text">
                      <div id="defaultSplash-phrases">
                          <div class="splash-text no-icon">&nbsp;</div>
                          <div class="splash-text no-icon">&nbsp;</div>
                      </div>
                      <div id="scrolling-text-gradient"></div>
                  </div>
                  <div id="bottom-text" class="text-center">
                      <div class="per-text"><span id="progress-percentage">0</span>%</div>
                      <div id="progress-text">gathering</div>
                  </div>
              </div>
              <div class="bottom-row">

              </div>
          </div>
      </div>

      <!-- Error page ========================================================== -->
      <div id="bother-stuck" class="row hidden installer-stage">
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="title text-center">Ahh, we're going to have to get back to you.</h1>
                  <h2 class="error-head" id="gen-error-message">Looks like we can't support you just yet, but our best people will be in touch real soon.</h2>
                  
              </div>
              <div class="bottom-row">
                  <p class="error-contact">Questions? Let us help. <br/><a href="mailto:help@otonomic.com?Subject=Trouble%20just%20getting%20started" target="_top">Contact us</a>.</p>
              </div>
          </div>
      </div>
      
      <!-- No facebook error page ========================================================== -->
      <div id="bother-no-page" class="text-center row hidden installer-stage">
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h1 class="text-center">It's us, not you.</h1>
                  <h2>During beta we can only accept businesses with Facebook Pages. If you decide to publish your Facebook Page, pop back over!</h2>
                  <p><a class="btn button-blue reload-fb-pages" href="javascript:void(0)">try again</a></p>
             </div>
              <div class="bottom-row">
                  <h6>If you have a Facebook Page and we're not finding it, or need any other help, just <a class="blue" href="mailto:help@otonomic.com?Subject=But%20I%20have%20a%20Facebook%20Page%20for%20my%20business" target="_top">tell us</a>. </h6>
              </div>
          </div>
      </div>
      
      <!-- if unpublished pages ========================================================== -->
      <div id="bother-unpub" class="text-center row hidden installer-stage">
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h2>We'll just come out and say it.</h2>
                  <h4>Your Facebook Pages are unpublished. To continue, first publish your Facebook business page then hit try again.</h4>
                  
                  <p><a class="btn button-blue reload-fb-pages" href="javascript:void(0)" >try again</a></p>
                  
              </div>
              <div class="bottom-row">
                  <h5> Questions? Let us help. <a class="blue" href="mailto:help@otonomic.com?Subject=But%20I%20have%20a%20Facebook%20Page%20for%20my%20business" target="_top">Contact us</a>.  </h5>
              </div>
          </div>
      </div>
      
      <!-- if facebook page blocked ========================================================== -->
      <div id="bother-blocked" class="text-center row hidden installer-stage">
          <div class="content-panel align-rows">
              <div class="top-row">
                  <img class="logo" src="images/logo.png">
              </div>
              <div class="mid-row">
                  <h2>Your Facebook Page is blocked.</h2>
                  <h4>We can't access your Facebook Page. it may be set for access in your country, have a minimum age requirement or some other block. To continue, first unblock your Facebook business page, then pop back and hit try again.</h4>
                  <h4>Access to your Facebook Page may be set to allow only local visitors, have a minimum age or some other limitation. Un-block your Facebook Page, then hit try again.</h4>
                  <p><a class="btn button-blue reload-fb-pages" href="javascript:void(0)">try again</a></p>
              </div>
              <div class="bottom-row">
                  <h5> Questions? Let us help. <a class="blue" href="mailto:help@otonomic.com?Subject=But%20I%20have%20a%20Facebook%20Page%20for%20my%20business" target="_top">Contact us</a>.  </h5>
              </div>
          </div>
      </div>

          <!-- Thank you ========================================================== -->
          <div id="build" class="row hidden installer-stage">
              <div class="content-panel align-rows">
                  <div class="top-row">
                      <img class="logo" src="images/logo.png">
                  </div>
                  <div class="mid-row">
                      <h1 class="title text-center">We're on it.</h1>
                      <p class="text-center">We've started to work on <span class="replace-sitename">{{siteName}}</span>!<br>
                      <span class="replace-fbname">{{firstName}}</span>, we'll send an email when it's ready.</p>
                      <p class="text-center">In the meantime, we reserved <span class="replace-sitename">{{siteName}}</span> custom domain:</p>
                      <h2 class="website-url text-center">http://<span class="website-otonomic-url"><span class="websitedomain">websitename</span>.otonomic.com</span></h2>
                      <p class="text-center">
                          <img src="images/cookie-dip.gif" class="img-responsive center-block" />
                      </p>
                  </div>
                  <div class="bottom-row">

                  </div>
              </div>
          </div>

        </form>
    </div><!-- /.container -->

      <script>
          var splash_loading_messages = [];
          var phrases;
          $.getJSON('messages.json', function (json) {
              splash_loading_messages = json;
              // Loop the phrases to cover the longest load time possible in the browser.
              phrases = shuffleArray(getPhrases());
              while (phrases.length > 0 && phrases.length < 70) {
                  phrases = phrases.concat(phrases);
              }
          });
      </script>
      <script type="text/javascript" src="js/ticker.js"></script>

    <script src="js/main.js?v=1.0.2"></script>
    <script type="text/javascript">

      jQuery(document).ready(function($) {
          trackFacebookPixel('viewed_installer');
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6021618382030', {'value':'0.00','currency':'USD'}]);

          // Online store / booking buttons
          jQuery('input, textarea').on('focus', function() {
              jQuery('html, body').animate({
                  scrollTop: jQuery(this).offset().top - 30
              }, 1000);
          });
      })
    </script>
      <!-- Google Code for Sites Created Conversion Page -->
      <script type="text/javascript">
          /* <![CDATA[ */
          var google_conversion_id = 959969618;
          var google_conversion_language = "en";
          var google_conversion_format = "3";
          var google_conversion_color = "ffffff";
          var google_conversion_label = "yMKFCM2tmGEQ0vLfyQM";
          var google_remarketing_only = false;
          /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
          <div style="display:inline;">
              <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/959969618/?label=yMKFCM2tmGEQ0vLfyQM&amp;guid=ON&amp;script=0"/>
          </div>
      </noscript>
  </body>
</html>


