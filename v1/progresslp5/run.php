<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-scalable=0">
    <meta name="description" content="Otonomic Site Creation Page">
    <meta name="author" content="Otonomic">
    <link rel="shortcut icon" href="favicon.ico">

    <title><?= __('Otonomic is creating your site...') ?></title>

      <!-- Bootstrap core CSS -->
      <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
      <!--<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">-->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

    <!-- Glyphicons -->
    <link href="fonts/Glyphicons-WebFont/glyphicons.css" rel="stylesheet">
    <!-- Aller font -->
    <link href="fonts/Aller-WebFont/aller_font.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/wp-loading-page.css" rel="stylesheet">

      <!--<script src="js/jquery-1.11.1.min.js"></script>-->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.js"></script>

      <!--<script src="js/bootstrap.min.js"></script>-->
      <!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

      <![endif]-->
      <script src="/v1/js/jquery.ba-bbq.min.js"></script>

      <?php include_once(__DIR__ . '/js/otonomic-analytics.js?v=1.0'); ?>

      <style>
          .notice {
              display: none;
              color: red;
              font-weight: bold;
          }
          .required {
              color: red;
          }
      </style>
  </head>

  <body class="wp-lp">

  <?php include(__DIR__ . '/parts/facebook_sdk.php'); ?>

    <!-- Intro    ========================================================== -->
    <div class="container-fluid">
      <div id="intro" class="row installer-stage">
        <div class="bg-image hidden-xs"></div>
        <div class="col-xs-12">
          <div class="text-center">
            <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
            <h1 class="title"><?= __('Create a website for ') ?></h1>
            <p class="site-name" id="ot-fb-name"><?= __('YOUR BUSINESS') ?></p>
            <h2 ><?= __('in just 5 clicks!') ?></h2>
            <a href="#stage-1" class="btn btn-ttc-blue js-intro-next">
                <?= sprintf(__('Let the magic begin! %s'), '<span class="glyphicon glyphicon-chevron-right"></span>'); ?>
            </a>
          </div>
        </div>
      </div>

      <!-- Stage 1 ========================================================== -->
        <div id="stage-1" class="row hidden installer-stage">
            <div class="bg-image hidden-xs ">
				<img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg3.jpg">
			</div>
            <div class="content-panel">
                <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">

                <div class="hidden-xs">
                  <h1 class="title"><?= __('Get your business ready to be found') ?></h1>
                  <h2><?= __('Do you want your website listed on the following search engines and directories?') ?></h2>
                  <div class="row">
                    <div class="col-xs-4">
                      <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Google" data-analytics-action="Search engines" data-analytics-label="Google"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/google.png"></button>
                    </div>
                    <div class="col-xs-4">
                      <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Yahoo" data-analytics-action="Search engines" data-analytics-label="Yahoo"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/yahoo.png"></button>
                    </div>
                    <div class="col-xs-4">
                      <button class="btn btn-ttc-white btn-checkbox btn-search-engine checked" data-engine="Bing" data-analytics-action="Search engines" data-analytics-label="Bing"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/search-buttons/bing.png"></button>
                    </div>
                  </div>
                  <hr>
                  <a href="#stage-2" class="btn btn-ttc-orange pull-right js-stage1-next">
                      Continue
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
                  <a href="#intro" class="btn btn-ttc-clear pull-right btn-back">
                    <span class="glyphicons undo"></span>
                    Back
                  </a>
                </div>

                <div class="visible-xs">
                  <h1 class="title">
                    <?= __('Get your business ready to be found.') ?>
                  </h1>
                  <p>
                    <?= __('We will get your website listed on Google, Yahoo, Bing and other search engines and directories.') ?>
                  </p>
                  <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right js-stage1-next">
                      Continue
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
                </div>
            </div>
        </div>

      <!-- Stage 2 ========================================================== -->
      <div id="stage-2" class="row hidden installer-stage">
        <div class="bg-image hidden-xs"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg4.jpg"></div>
        <div class="content-panel">
          <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
          <div class="hidden-xs">
            <h1 class="title">
                <?= __('Looking good!') ?>
            </h1>
            <h2>
                <?= __('Your website looks right on every screen.') ?>
            </h2>
            <p>
                <?= __('It pays off to look great on every format, clients and search engines take notice and put your business above the rest.</br>Select the devices for an optimized version of your site:') ?>
            </p>
            <div class="row">
              <div class="col-xs-4">
                <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Desktop"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/desktop.png"></button>
              </div>
              <div class="col-xs-4">
                <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Tablet"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/tablet.png"></button>
              </div>
              <div class="col-xs-4">
                <button class="btn btn-ttc-white btn-checkbox btn-device checked" data-analytics-action="Device" data-analytics-label="Mobile"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/devices-buttons/mobile.png"></button>
              </div>
            </div>
            <hr>
            <a href="#stage-3" class="btn btn-ttc-orange pull-right js-stage2-next next-btn">
              Continue
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <a href="#stage-1" class="btn btn-ttc-clear btn-back pull-right">
              <span class="glyphicons undo"></span>
              Back
            </a>
          </div>
          <div class="visible-xs">
            <h1 class="title">
                <?= __('Your website looks right on every screen.') ?>
            </h1>
            <img src="https://otonomic-static.s3.amazonaws.com/images/installer/image2.png" style="width: 40%;">
            <p>
                <?= __('Looking good on mobile, tablet and desktop, provides for easier navigation and puts your website above the rest.') ?>
            </p>
            <a href="#" onclick="return false;" class="btn btn-ttc-orange pull-right next-btn js-stage2-next">
                Continue
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
        </div>
      </div>


      <!-- Stage 3 ========================================================== -->
      <div id="stage-3" class="row hidden installer-stage">
        <div class="bg-image hidden-xs"><img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg7.jpg"></div>
        <div class="content-panel">
          <div class="hidden-xs">
          <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
          <h1 class="title">
            <?= __("Keep your business open 24/7") ?>
          </h1>
          <div class="row">



            <div class="col-xs-12">
              <button id="option-booking" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Booking"><span class="text-type-1">Online Booking</span> let your clients book online <span class="glyphicons calendar"></span></button>
            </div>

              <div class="col-xs-12">
                  <button id="option-online-store" class="btn btn-block btn-ttc-white btn-checkbox btn-add-on" data-analytics-action="Addons" data-analytics-label="Online Store"><span class="text-type-1">Online Store</span> sell products & services online <span class="glyphicons shop"></span></button>
              </div>

            <div class="col-xs-12">
              <button class="btn btn-block btn-ttc-white btn-checkbox btn-add-on btn-uncheck-others" data-analytics-action="Addons" data-analytics-label="I don’t need these features">
              I don’t need it<br/>
              <small>You can always add this later</small>
              <span class="glyphicons remove"></span>
              </button>
            </div>
          </div>
          <hr>
          <a href="#stage-auto-update" class="btn btn-ttc-orange pull-right disabled js-stage3-next next-btn">
            Select
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
          <a href="#stage-2" class="btn btn-ttc-clear btn-back pull-right">
            <span class="glyphicons undo"></span>
            Back
          </a>
          </div>
          <div class="visible-xs">
            <h1 class="title">
                <?= __("Your website is open 24/7:") ?>
            </h1>
            <p class="text-left"><span class="glyphicons ok_2" style="vertical-align: text-bottom;"></span>
                <?= __("Online Store: Sell your products & Services Online") ?>
            </p>
            <p class="text-left"><span class="glyphicons ok_2" style="vertical-align: text-bottom;"></span>
                <?= __("Online Booking: Let your clients book appointments") ?>
            </p>
            <a href="#stage-auto-update"  class="btn btn-ttc-orange pull-right js-stage3-next">
                Continue
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
        </div>
      </div>


      <!-- Stage  ========================================================== -->
      <div id="stage-auto-update" class="row hidden installer-stage">
      <div class="bg-image hidden-xs" style="
                background-image: url(https://otonomic-static.s3.amazonaws.com/images/installer/installer-social-new.jpg);
                background-position-x: 57%;
              "></div>

      <div class="content-panel">
          <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
          <h1 class="title">
              <?= __("Every social media post added to your website, in real time.") ?>
          </h1>
          <h2>
                <?= __('Promote your website with every post, picture, and video.') ?>
          </h2>
          <p class="social_searching_msg">
          </p>

          <p>
                <?= __('Add your business’ social media accounts to your website:') ?>
          </p>

          <div class="row">
              <div class="col-xs-12">

                  <!-- START Instagram -->
                  <div class="form-group social-media-field" id="instagram">
                      <div class="row">
                          <div class="col-xs-3">
                              <label for="social_media_instagram"><i class="fa fa-instagram"></i>
                                  Instagram
                              </label>
                          </div>
                          <div class="col-xs-9 has-feedback">
                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameInstagram.php" id="social_media_instagram" name="social[instagram]" value="">
                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-xs-12">
                              <div class="search-results-container" id="search-results-instagram"></div>
                          </div>
                      </div>
                  </div>
                  <!-- END Instagram -->

                  <!-- START YouTube -->
                  <div class="form-group social-media-field" id="youtube">
                      <div class="row">
                          <div class="col-xs-3">
                              <label for="social_media_youtube"><i class="fa fa-youtube"></i>
                                <?= __('Youtube') ?>
                              </label>
                          </div>
                          <div class="col-xs-9 has-feedback">
                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameYoutube.php" id="social_media_youtube" name="social[youtube]" value="">
                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-xs-12">
                              <div class="search-results-container" id="search-results-youtube"></div>
                          </div>
                      </div>
                  </div>
                  <!-- END YouTube -->

                  <!-- START Twitter -->
                  <div class="form-group social-media-field" id="twitter">
                      <div class="row">
                          <div class="col-xs-3">
                              <label for="social_media_twitter"><i class="fa fa-twitter"></i>
                                    <?= __('Twitter') ?>
                              </label>
                          </div>
                          <div class="col-xs-9 has-feedback">
                              <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameTwitter.php" id="social_media_twitter" name="social[twitter]" value="">
                              <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-xs-12">
                              <div class="search-results-container" id="search-results-twitter"></div>
                          </div>
                      </div>
                  </div>
                  <!-- END Twitter -->

                  <!-- START LinkedIn -->
                  <!--                      <div class="form-group social-media-field" id="linkedin">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="social_media_linkedin"><i class="fa fa-linkedin"></i>
                                                        LinkedIn
                                                    </label>
                                                </div>
                                                <div class="col-xs-9 has-feedback">
                                                    <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameLinkedin.php" id="social_media_linkedin" name="social[linkedin]" value="">
                                                    <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="search-results-container" id="search-results-linkedin"></div>
                                                </div>
                                            </div>
                                        </div>
                  -->
                  <!-- END LinkedIn -->

                  <!-- START Flickr -->
                  <!--
                                        <div class="form-group social-media-field" id="flickr">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="social_media_flickr"><i class="fa fa-flickr"></i>
                                                        Flickr
                                                    </label>
                                                </div>
                                                <div class="col-xs-9 has-feedback">
                                                    <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameFlickr.php" id="social_media_flickr" name="social[flickr]" value="">
                                                    <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="search-results-container" id="search-results-flickr"></div>
                                                </div>
                                            </div>
                                        </div>
                  -->
                  <!-- END Flickr -->


                  <!-- START Google+ -->
                  <!--
                                        <div class="form-group social-media-field" id="googleplus">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="social_media_googleplus"><i class="fa fa-google-plus"></i>
                                                        Google+
                                                    </label>
                                                </div>
                                                <div class="col-xs-9 has-feedback">
                                                    <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernameGoogleplus.php" id="social_media_googleplus" name="social[googleplus]" value="">
                                                    <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="search-results-container" id="search-results-google-plus"></div>
                                                </div>
                                            </div>
                                        </div>
                  -->
                  <!-- END Google+ -->

                  <!-- START Pinterest -->
                  <!--
                                        <div class="form-group social-media-field" id="pinterest">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="social_media_pinterest"><i class="fa fa-pinterest"></i>
                                                        Pinterest
                                                    </label>
                                                </div>
                                                <div class="col-xs-9 has-feedback">
                                                    <input type="text" class="form-control LoNotSensitive enable-suggest" data-suggest-url="searchUsernamePinterest.php" id="social_media_pinterest" name="social[pinterest]" value="">
                                                    <i class="glyphicons remove_2 form-control-feedback clear-input"></i>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="search-results-container" id="search-results-pinterest"></div>
                                                </div>
                                            </div>
                                        </div>
                  -->
                  <!-- END Pinterest -->

              </div>

              <div class="action-buttons col-xs-12">
                  <a href="#stage-3" class="btn btn-ttc-clear btn-back">
                      <span class="glyphicons undo"></span>
                      <?= __('Back') ?>
                  </a>
                  <a href="#choose-template" class="btn btn-ttc-orange pull-opposite btn-next">
                      <span class="glyphicon glyphicon-ok"></span>
                      <?= __('Next') ?>
                  </a>
              </div>

          </div>
      </div>
      </div>



      <!-- Choose a template ================================================ -->
      <div id="choose-template" class="row hidden installer-stage">
        <div class="content-panel">
          <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
          <div class="hidden-xs">
            <h1 class="title">
                <?= __('Your website, your vision.') ?>
            </h1>
            <h2>
                <?= __('Choose a template that you like. You can switch anytime.') ?>
            </h2>
          </div>
          <div class="visible-xs">
            <h1 class="title">
                <?= __('Choose a template that you like:') ?>
            </h1>
            <p>
                <?= __('(You can switch anytime)') ?>
            </p>
          </div>
          <div class="row">


                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="curly-beige">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/curly-beige.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="curly-beige">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>


                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="dream-salon">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-salon.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-salon">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="dream-fitness">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/dream-fitness.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="dream-fitness">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>



                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="blonde-rays">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/blonde-rays.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="blonde-rays">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>
              </div>




              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="igloo">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/igloo.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="igloo"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="fluffy-strokes">
                              <img class="img-responsive" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/fluffy-strokes.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="fluffy-strokes"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>

              </div>



              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-right">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="el-greco">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/el-greco.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="el-greco"><span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>


                  <div class="col-xs-12 col-sm-6">
                      <div class="template-conrainer pull-left center-block">
                          <div class="overlay hidden-xs hidden-md hidden-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                          <a href="#" class="btn-choose-template" data-option-value="cousteau">
                              <img class="img-responsive bordered" src="https://otonomic-static.s3.amazonaws.com/images/installer/templates/cousteau.png">
                          </a>
                          <div class="visible-xs visible-md visible-sm">
                              <button class="btn btn-ttc-blue btn-choose-template" data-option-value="cousteau">
                                  <span class="glyphicons ok_2"></span>
                                  Select
                              </button>
                          </div>
                      </div>
                  </div>


		  
          </div>
        </div>
      </div>

        <!-- Congratz ========================================================== -->
      <div id="congratz" class="row hidden installer-stage text-center">
        <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">
        <div class="upper-content">
          <p class="site-name" id="ot-fb-name">
              Your business
          </p>
          <h1 class="congratz-title">
              website will be ready in <span id="counter">7 seconds</span>
          </h1>
          <img class="oto-anima" src="https://otonomic-static.s3.amazonaws.com/images/installer/ottoHoverLoop.gif">
              <div class="fb-like" data-href="https://www.facebook.com/otonomic" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
        </div>
        <div class="lower-content">
          <h3 id="oto-web-url" class="hidden">http://wp.otonomic.com/newsite</h3>
          <p class="tos">
            <?= sprintf( __('By continuing to use the service, you accept the Otonomic %sTerms of Service%s'), '<a target="_blank" href="/pdfs/Otonomic_Terms_of_Service.pdf" id="link-tos">', '</a>') ?>
          </p>
        </div>
      </div>

        <!-- Email Screen ========================================================== -->
        <div id="stage-email" class="row hidden installer-stage">
            <div class="bg-image hidden-xs ">
                <img src="https://otonomic-static.s3.amazonaws.com/images/installer/bg2.jpg">
            </div>
            <div class="content-panel">
                <img class="logo" src="https://otonomic-static.s3.amazonaws.com/images/installer/otonomic-logo-dark.png">

                <div class="">
                    <h1 class="title">
                        <?= __('Congratulations! Your site is ready.') ?>
                    </h1>
                    <h2>
                        <?= __('Fill in your email address and we’ll send you your username and password.<br/>Once you’re done, we’ll take you to your new site.') ?>
                    </h2>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <span class="required"> * </span>
                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" class="tooltip" title="The email address will appear on your site. Contact messages will also be emailed to this address."></i>
                                <div id="email-notice" class="notice" style="display: none">
                                    <?= __('Please fill this field') ?>
                                </div>
                                <div id="email-notice-2" class="notice" style="display: none">
                                    <?= __('Please enter valid email') ?>
                                </div>
                                <input type="email" class="form-control" id="email" name="email" value="">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <a href="#" onclick="return false;" id="js-stage-email-next" class="btn btn-ttc-orange pull-right">
                        <?= __('Send me the login details') ?>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

            </div>
        </div>


    </div><!-- /.container -->

    <script src="/v1/js/installer.js"></script>

    <script>
        /*
        window.fbAsyncInit = function() {
            FB.init({
                appId: "373931652687761",
                status: true,
                cookie: true,
                xfbml: true
            });

            window.fbAsyncInit.fbLoaded.resolve();
            checkConnectedWithFacebook();
        };
        window.fbAsyncInit.fbLoaded = jQuery.Deferred();
        */
    </script>

    <script type="text/javascript">
      var base_url = '//otonomic.com/hybridauth/twitter.php';

      jQuery(document).ready(function($) {
          trackFacebookPixel('viewed_installer');
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6021618382030', {'value':'0.00','currency':'USD'}]);


          $('#social_connect_hybrid a').click(function(){
              var type = $(this).attr('id').split('_');

              track_event('Loading Page', 'Social Connect', type[1]);

              var url = base_url+"?social="+type[1];
              window.open(
                      url,
                      "hybridauth_social_sign_on",
                      "location=0,status=0,scrollbars=1,width=800,height=500"
              );
              return false;
          });

          // Change social buttons appearance depending on screen width
            var changeWidth = function(){
            console.log('Resized');
            if ( $(window).width() < 480 ){
                $('#social_connect_hybrid').addClass('btn-group-justified btn-group');
                $('#social_connect_hybrid a').addClass('btn');
              } else {
                $('.btn-group-vertical').removeClass('btn-group-justified btn-group');
                $('#social_connect_hybrid a').removeClass('btn');
              }
            };
            $(window).resize(changeWidth());

          // Search engine buttons
          $('.btn-search-engine').click(function(){
            // $('.btn-search-engine').removeClass('checked');
            $(this).toggleClass('checked');

              var engineName =$(this).data('engine');
            $('.js-stage1-next').removeClass('disabled').html('Rank high on search engines, got it!');
          });

          // Devices buttons
          $('.btn-device').click(function(){
            // $('.btn-device').removeClass('checked');
            $(this).toggleClass('checked');
            $(this).parents('.installer-stage').find('.next-btn').removeClass('disabled').html('Continue <span class="glyphicon glyphicon-chevron-right"></span>');
          });

          // Online store / booking buttons
          $('.btn-add-on').click(function(){
              var $this = $(this);
              if($this.hasClass('btn-uncheck-others') && !$this.hasClass('checked')) {
                $('.btn-add-on').removeClass('checked');
              } else {
                $('.btn-uncheck-others').removeClass('checked');
              }
              $this.toggleClass('checked');
              $this.parents('.installer-stage').find('.next-btn').removeClass('disabled').html('Continue <span class="glyphicon glyphicon-chevron-right"></span>');
          });

          // Devices buttons
          $('.social-btn').click(function(){
              $(this).toggleClass('selected');
          });

          var path_socialmedia_library = "/shared/lib/socialmedia/";
          jQuery('.enable-suggest').each(function(index){
              var wrapper = jQuery(this).parent().parent().parent();
              //jQuery(wrapper).append('<div class="search-results-container" />');
              jQuery(this).on('keyup', function() {
                  var $this = jQuery(this);
                  var searchval = $this.val();
                  //wrapper = jQuery(this).parent();
                  if(searchval.length > 2) {

                      jQuery('.search-results-container', wrapper).html('מחפש...').show();
                      jQuery.get(path_socialmedia_library + jQuery(this).attr('data-suggest-url') +"?format=html&search_box="+searchval, function(data) {
                          jQuery('.search-results-container', wrapper).html(data);
                      });
                  } else {
                      jQuery('.search-results-container', wrapper).html('').hide();
                  }
              });
          });
          jQuery('.search-results-container').on('click', '.media.selectable', function() {
              var value = jQuery(this).attr('data-value');
              var wrapper = jQuery(this).parent().parent().parent().parent();
              jQuery('input', wrapper).val(value);
              jQuery('.search-results-container', wrapper).hide();
          });
      })
    </script>

  </body>
</html>
