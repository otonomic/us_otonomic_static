<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Organization">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="js/colorbox/theme2/colorbox.css" rel="stylesheet">
    <link href="/shared/fb_modal/css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/colorbox/jquery.colorbox.js"></script>
    <script src="/shared/fb_modal/js/otoFBLogin.jquery.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
    <a href="" class="oto-fb-login">Login with Facebook</a>
</body>
