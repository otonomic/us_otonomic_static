function modalCallback(event, modal) {
    console.log(event, modal);
}
function pageSelectCallback(page, fb_user, fb_pages) {
    console.log(page);
    console.log(fb_user);
    console.log(fb_pages);
}
// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
window.fbAsyncInit = function() {
    FB.init({
        appId      : '286934271328156',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.5'
    });
    jQuery('.oto-fb-login').otoFBLogin({
        template: {
            'appRejected':{
                'view': '/shared/fb_modal/views/app-rejected.html',
                'callback': modalCallback,
                popup: {
                    width: '400px',
                    height: false,
                }
            },
            'premissionRejected':{
                'view': '/shared/fb_modal/views/premission-rejected.html',
                'callback': modalCallback,
                popup: {
                    width: '400px',
                    height: false,
                }
            },
            'selectPage':{
                'view': '/shared/fb_modal/views/select-page.html',
                'callback': modalCallback,
                popup: {
                    width: '400px',
                    height: false,
                }
            }
        },
        permissions: 'email,public_profile,pages_show_list',/* pages_show_list permission is required */
        onPageSelect: pageSelectCallback,
        onProcessStart: function() {
            console.log('FB login process initiated');
        }
    });
};
