<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">

        <title>Otonomic Sites</title>
        <link rel="stylesheet" type="text/css" href="inc/media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="inc/media/css/jquery.dataTables-responsive.css">
        <!--<link rel="stylesheet" type="text/css" href="inc/media/css/colorbox.css">-->
        <!--<link rel="stylesheet" type="text/css" href="inc/media/css/styles.css">-->
        <link rel="stylesheet" type="text/css" href="inc/media/css/dataTables.tableTools.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
        <!--<link rel="stylesheet" type="text/css" href="inc/media/js/datepicker/jquery-ui.min.css">-->

        <script type="text/javascript" language="javascript" src="inc/media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="inc/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="inc/media/js/jquery.dataTables.responsive.js"></script>
        <script type="text/javascript" language="javascript" src="inc/media/js/dataTables.tableTools.min.js"></script>
        <!--<script type="text/javascript" language="javascript" src="inc/media/js/jquery.colorbox.js"></script>-->
        <!--<script type="text/javascript" language="javascript" src="inc/media/js/datepicker/jquery-ui.min.js"></script>-->
        <script type="text/javascript" src="inc/media/js/functions.js" language="javascript" class="init"></script>

        <style type="text/css">
            body {
                font-family: arial, sans-serif;
                font-size: 12px;
            }
        </style>
    </head>
    
    <body class="dt-example">
        <div class="parentContainer">
            <section>

                <table id="data" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="all">ID</th>
                            <th>Email</th>
                            <th class="all">Website</th>
                            <th>FB user token</th>
                            <th>FB user details</th>
                            <th class="all">User pages</th>
                            <th class="all">Selected Page</th>
                            <th>User IP</th>
                            <th>Created on</th>
                            <th class="all">Modified on</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th class="all">ID</th>
                            <th>Email</th>
                            <th class="all">Website</th>
                            <th>FB user token</th>
                            <th>FB user details</th>
                            <th class="all">User pages</th>
                            <th class="all">Selected Page</th>
                            <th>User IP</th>
                            <th>Created on</th>
                            <th class="all">Modified on</th>
                        </tr>
                    </tfoot>
                </table>
            </section>
        </div>

    </body>
</html>