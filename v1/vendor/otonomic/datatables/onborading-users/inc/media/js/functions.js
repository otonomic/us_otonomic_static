$(document).ready(function() {
        $.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
	$('#data').dataTable( {
		"processing": false,
		"serverSide": true,
		"ajax": "inc/server_processing.php",
                responsive: true,
                "order": [[ 0, "desc" ]],
                "dom": 'T<"clear">lfrtip',
                "tableTools":{
                     "sSwfPath": "inc/media/swf/copy_csv_xls_pdf.swf"
                }
	} );
        
        $('#data').on( 'draw.dt', function () {
//            $('a.popup').colorbox({
//                inline: true
//            });
            
            $('.deleteSite').click(function(ev){
                ev.preventDefault();
                if(confirm('Are you sure you want to delete this website? This action is irreversible!')) {
                    window.location.href = $(this).attr('href');
                }
            });
            
            jQuery('a.save_pass').click(function(){
                
                var userid = jQuery(this).prev('input').attr('id').split('new_pass_')[1];
                var nonce = jQuery('#np_nonce_'+userid).val();
                var pass = jQuery(this).prev('input').val();
                if( pass === '') {
                    alert('Please enter a password!');
                } else {
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '../wp-admin/admin-ajax.php',
                        data: {
                            'action': 'oto_ajaxchangepassword',
                            'pass2': pass,
                            'user': userid,
                            '_nonce': nonce
                        },
                        success: function (data) {
                            alert(data.message);
                        }
                    });
                }
            });
            
//            $('.expirydateblog').datepicker({
//                dateFormat: "yy-mm-dd"
//            });
            
            $('form.blog_footer_update').submit(function(e){
                $this = $(this);
                $(this).find('.siteDatasubmit').hide();
                $(this).find('.updatingmsg').show();
                jQuery.ajax({
                    type: 'POST',
                    
                    url: 'http://wp.test/api/settings/set_site_footer/',
                    data: $( this ).serialize() ,
                    success: function (data) {
                        data = jQuery.parseJSON(data);
                        $this.find('.siteDatasubmit').show();
                        $this.find('.updatingmsg').hide();
                        alert(data.message);
                    }
                });
                return false;
            })
            
        } );
        
} );