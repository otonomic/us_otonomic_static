<?php

/*
 * DataTables server-side processing script.
 *
 *
 * @license MIT - http://datatables.net/license_mit
 */

//require_once '../../wp-load.php';


// DB table to use
$table = 'otonomic_onboarding';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes


$columns = array(
    array('db' => 'id', 'dt' => 0),
   
    array(
        'db' => 'user_email',
        'dt' => 1,
        'formatter' => function ($d, $row) {
                return '<a href="mailto:' . $d . '">' . $d . '</a>';
            }
    ),
    array(
            'db' => 'website_name', 
            'dt' => 2,
            'formatter' => function($d, $row){
                    return '<a target="blank" href="' . $d . '">' . $d . '</a>';
            }
        ),
    array('db' => 'fb_user_token', 'dt' => 3),
    array(
        'db' => 'fb_user_details',
        'dt' => 4
    ),
    array(
        'db' => 'fb_user_pages',
        'dt' => 5
    ),  
    array('db' => 'fb_selected_page', 'dt' => 6),
    array('db' => 'user_ip', 'dt' => 7),
            array('db' => 'created_on', 'dt' => 8),
            array('db' => 'modified_on', 'dt' => 9),
);

// SQL server connection information
        
//include('../../wp-config.php');


//define('DB_NAME1', 'wp');
//define('DB_USER1', 'root');
//define('DB_PASSWORD1', 'otoOTO2611');
//define('DB_HOST1', 'dbmaster.otonomic.com');       
        
include_once 'config.php';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );


function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}

$ret_data = json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns ), JSON_UNESCAPED_UNICODE);
header('Content-Type: application/json; charset=utf-8');
echo $ret_data;




class OtoSites {
    static function format_phone($d, $row) {
        return '<a target="_blank" href="tel:' . $d . '">' . $d . '</a>';
    }

    static function format_row($d, $row) {
        if ($d === 'Facebook fan page' && !empty($row['page_id'])) {
            return '
                        <a target="_blank" href="http://facebook.com/' . $row['page_id'] . '">
                            <i class="fa fa-facebook"></i>
                        </a>';
        } else {
            return $d;
        }
    }
}