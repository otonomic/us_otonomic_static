<?php
$theme_urls = [
    'Medical-Link'      => 'http://wp.pulsarmedia.ca/medical-link/',
    'Clinico'           => 'http://clinico.creaws.com/',
    'Medical Plus'      => 'http://themes.goodlayers.com/medicalplus/',

    /*
    'Health and Medical'  => 'http://themes.wplook.com/html/health-medical/index.html',
    'MedicalPress'      => 'http://inspirythemesdemo.com/medicalpress/',
    'Doctor'            => 'http://preview.ait-themes.com/doctor/wp1/',
    */
];
$theme = !empty($_GET['theme']) ? $_GET['theme'] : array_keys($theme_urls)[0];

$preview_url = $theme_urls[$theme];
?>



<!doctype html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Otonomic - Premium Sites and Themes</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="css/base.css?v=20141114">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css?v=20141114">

    <!-- Javascript
    ================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-37736198-1', 'auto');
            ga('set', 'dimension4', window.location.hostname); // Site url
            ga('set', 'dimension5', 'otonomic demo themes'); // Site Type
            ga('send', 'pageview');
    </script>

</head>
<body>


<nav class="navbar navbar-default navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="http://www.otonomic.com">
            <img src="images/logo.png" alt="Otonomic" class="otonomic-logo" />
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav">
            <li>
                <a href="#">
                    Current theme:
                    <span class="current_theme_name"><?= ucfirst($theme) ?></span>
                </a>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 8px;padding-bottom: 8px;">
                    <button class="btn btn-default">
                        Select theme <b class="caret"></b>
                    </button>
                </a>

                <ul class="dropdown-menu inverse-dropdown">
                    <?php foreach($theme_urls as $theme_slug => $theme_url): ?>
                        <?php $theme_name = ucwords(str_replace('_', ' ', $theme_slug)); ?>
                        <li class="theme-row" data-id="4" data-responsive="yes"
                            data-url=""
                            data-preview=""
                            data-key="<?= $theme_slug; ?>">
                            <a href="?theme=<?= $theme_slug ?>">
                                <span class="theme-name"><?= $theme_name; ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>

        <div class="navbar-right">
            <form class="navbar-form">
            <a class="" href="http://wp.otonomic.com/pricing">
                <button class="btn btn-success">
                    Get a site like this
                </button>
            </a>
            </form>
        </div>
    </div><!-- /.navbar-collapse -->
</nav>


<div id="bar-frame" class="desktop">
    <iframe id="target" src="<?= $preview_url ?>" width="100%" scrolling="auto" frameborder="0"></iframe>
</div>

</body>
</html>
