
    <style>

        @font-face {
            font-family: 'Dosis';
            font-style: normal;
            font-weight: 400;
            src: local('Dosis Regular'), local('Dosis-Regular'), url(http://fonts.gstatic.com/s/dosis/v4/4hYyXH_8WmbBLamf6WjLwg.woff2) format('woff2'), url(http://fonts.gstatic.com/s/dosis/v4/xIAtSaglM8LZOYdGmG1JqQ.woff) format('woff');
        }
        .pop-orange {
            background-color: #f44601;
            border: none;
            padding: 10px 20px;
            color: #FFF;
            margin-left: -5px;
            border-radius: 0 3px 3px 0;
        }

        .pop-inp {
            height: 40px;
            width: auto;
            padding-left: 10px;
            border-radius: 3px 0 0 3px;
            border: 1px solid #ccc;
        }

        body {
            background-color: #fbfbfb
        }

        h2 {
            margin-top: 0px;
            margin-bottom: 10px;
            font-size: 27px;
            font-family: 'Dosis', sans-serif;
            font-weight: bold;
        }

        .ttl-txt {
            color: #666;
            padding: 10px 35px 20px;
            font-size: 14px;
            font-weight: bold;
        }

        img {
            width: 100%;
        }

        #err_valid_email {
            font-weight: bold;
            color: #ff0000;
        }

        .header h2 {
            text-transform: uppercase;
            font-size: 28px;
            padding: 15px 20px;
        }

        .header p {
            font-size: 18px;
            padding-bottom: 15px;
        }
        
        #popup{
            background-color: #FFF;
            height: 415px;
        }

    </style>
<div id="popup">
    <div class="text-center header" style="background-color:#222; color:#FFF">
        <h2>Your Dream Site is Only 1 Click Away!</h2>

        <!--<p>Find out more about our Premium Plan</p>-->
    </div>


    <div class="text-center ttl-txt">
    Let our award-winning designer create the perfect website for your business.
    </div>
    <div class="ttl-txt">
        Join the Otonomic Premium program and get:
        <ul>
            <li>Custom design</li>
            <li>Search-engines visibility boost</li>
            <li>Online store</li>
            <li>Additional plugins</li>
            <li>and more!</li>
        </ul>
    </div>
    <div class="text-center ttl-txt">
        <a href="http://wp.otonomic.com/pricing" class="pop-orange" id="signup_email_col">Learn More</a>
    </div>
</div>


<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37736198-1']);
    _gaq.push(['_trackPageview']);

    _gaq.push(['_trackEvent', 'A/B Testing', 'Email Collector Variation', "Let us tell your business' story for you"]);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>