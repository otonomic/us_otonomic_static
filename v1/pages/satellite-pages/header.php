<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/favicon.ico">

    <title>Otonomic - Support Center</title>

    <!-- Custom styles for this page -->
    <link href="css/sitellite-pages.css" rel="stylesheet">
    <link href="js/vendor/prefect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="satellite-page">
    <!-- Side Navbar -->
    <?php include 'sidebar.php'; ?>
    <div class="navbar navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" id="menu-toggle" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".sidebar-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="htttp://otonomic.com"><img src="images/logo.png" alt="otonomic.com"></a>
          <div class="navbar-right" >
            <a href="#" class="btn btn-sm navbar-btn btn-oto-orange hidden-xs">Create Your Website Now</a>
          </div>
        </div>
      </div>
    </div>