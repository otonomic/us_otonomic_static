  <!-- Header -->
  <?php include 'header.php'; ?>

  <div id="main-wrapper" class="features">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Features</h1>
        <!-- Social buttons -->
        <?php include 'social-buttons.php'; ?>
      </div>
    </div>

    <div class="container">
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons kiosk big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>A Category this is</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Quick and simple site creation</h4>
            <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.</p>
         </div>
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Beautiful templates</h4>
            <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Content editing</h4>
            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p>
         </div>
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Make Your Photos Stand Out</h4>
            <p>I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath.</p>
          </div>
        </div>
      </section>
      <div class="row">
          <div class="col-sm-11 col-sm-offset-1">
            <span style="margin-right: 10px;">1-click website from your facebook page</span>
            <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
          </div>
      </div>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons crown big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>This is a different category and a really short one</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Blog</h4>
            <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.</p>
         </div>
          <div class="col-sm-5 col-sm-offset-1">
            <h4>Customer Reviews</h4>
            <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.</p>
          </div>
        </div>
      </section>
    </div> <!-- /container -->
    <!-- Footer -->
    <?php include 'footer.php'; ?>