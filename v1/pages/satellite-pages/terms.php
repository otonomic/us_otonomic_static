  <!-- Header -->
  <?php include 'header.php'; ?>

  <div id="main-wrapper" class="terms-of-service">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Terms of service</h1>
        <!-- Social buttons -->
        <?php include 'social-buttons.php'; ?>
      </div>
    </div>

    <div class="container">
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">1</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Acceptance of Terms</h3>
            <p>Welcome to Otonomic's website creation service (the "Service"). Your use of the Service is subject to these Terms of Service ("Terms"). We reserve the right to update and change these Terms from time to time without notice to you. These Terms will also be applicable to your use of the Service on a trial basis. By using the Service, you signify your acceptance of these Terms. If you do not agree to these Terms, do not use the Service.</p>
         </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">2</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Description Of Service</h3>
            <p>Our web-based Service allows users who register for an account (each an "Account Holder") to create and update an online web site. Once registered, each Account Holder receives his or her own Web Site and may post "Content" (defined in Section 7). Any new features on the Service, including the release of new Otonomic tools and resources, shall be subject to these Terms. To use the Service, you must have access to the Internet, either directly or through devices that access web-based content, and you must pay any fees associated with Internet access. In addition, you must provide all equipment necessary to make such connection to the Internet, including a web-enabled computer. The Service may include certain communications from us, such as service announcements, administrative messages, and the Otonomic Newsletter. These communications are considered part of Otonomic membership. You may not access the Service by any means other than through the Service interfaces we provide you.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">3</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Registration</h3>
            <p>To register as an Account Holder, you must provide us with a valid email address and other information ("Registration Data").You will choose a password and account designation for your web sites during the registration process and you will obtain a Otonomic ID. You are responsible for maintaining the confidentiality of the password and account, and for all activities that occur under your account. In consideration of use of the Service, you agree to maintain and update true, accurate, current and complete Registration Data. If you provide any information that is untrue, inaccurate, not current or incomplete, or if Otonomic has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, we may suspend or terminate your account and refuse any and all current or future use of the Service or any portion thereof. Individuals under the age of 13 are prohibited from creating or using accounts through Otonomic.com.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">4</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Cancellation And Termination</h3>
            <p>If you cancel the Service, your cancellation will take effect immediately. After cancellation, you may no longer have access to your web site and we may delete all information on your web site. We accept no liability for such deleted information or content.</p>
            <p>For as long as we continue to offer the Services, we will provide and seek to update, improve and expand the Service. As a result, we allow you to access the Service as it may exist and be available on any given day and have no other obligations, except as expressly stated in this Agreement. We may modify, replace, refuse access to, suspend or discontinue the Service, partially or entirely, or change and modify prices for all or part of the Services for you or for all our users in our sole discretion. All of these changes shall be effective upon their posting on our site or by direct communication to you unless otherwise noted. We further reserve the right to withhold, remove and or discard any Content available as part of your account, with or without notice if deemed by us to be contrary to this Agreement. For avoidance of doubt, we have no obligation to store, maintain or provide you a copy of any content that you or others provide when using the Service.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">5</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Otonomic Privacy Policy</h3>
            <p>How we collect, protect and use your Registration Data and certain other information about you are contained in our <a href="privacy-policy.php">Privacy Policy</a>, which is part of these Terms.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">6</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Website Account And Security</h3>
            <p>You are responsible for maintaining the security of your account and web site, for all activities that occur or actions taken under the account or in connection with the web site. You agree to immediately notify us in writing of any unauthorized uses of the account or any other breaches of security. We will not be liable for any loss or damage from your failure to comply with this security obligation. You acknowledge and agree that under no circumstances will we be liable, in any way, for any or your acts or omissions or those of any third party, including damages of any kind incurred as a result of such acts or omissions.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">7</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Your Rights In Your Content</h3>
            <p>Otonomic does not claim ownership of your Content, but you give us your permission to host and/or to display your Content on the Service. This permission exists only for as long as you continue to use the Service or remain an Account Holder.</p>
          </div>
        </div>
      </section> 
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">8</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Content And Conduct Rules And Obligations</h3>
            <p>All information, data, text, software, music, sound, photographs, graphics, video, messages, goods, products, services or other materials you post on a web site via the Service ("Content") are the sole responsibility of the person from which such Content originated. You are responsible for all Content that you upload, post, transmit or otherwise make available via the Service. We do not control the Content you post via the Service.</p>
            <p>By using the Service, you may be exposed to content that is offensive, indecent or objectionable. Under no circumstances will we be liable for your Content or the content of any third party, including, but not limited to, for any errors or omissions in your Content, or for any loss or damage of any kind incurred as a result of the use of any Content posted, transmitted or otherwise made available via the Service. You acknowledge that we do not pre-screen Content, but that we shall have the right (but not the obligation) to refuse, move or delete any Content that is available via the Service. We shall also have the right to remove any Content that violates these Terms or is otherwise objectionable in our sole discretion. You must evaluate, and bear all risks associated with, the use of any Content. You may not rely on any Content created by us. You acknowledge and agree that we may preserve Content and may also disclose Content if required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce these Terms; (c) respond to claims that any Content violates the rights of third-parties; or (d) protect our rights, property, or personal safety and those of our users and the public. The technical processing and transmission of the Service, including Content, may involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices.</p>
            <h4 class="orange-text">You agree that you will not:</h4>
            <p><b>(a)</b> upload, post, transmit, display or otherwise make available any Content that is unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene, libelous, invasive of another's privacy (up to, but not excluding any address, email, phone number, or any other contact information without the written consent of the owner of such information), hateful, or racially, ethnically or otherwise objectionable;</p>
            <p><b>(b)</b> harm minors in any way;</p>
            <p><b>(c)</b> impersonate any person or entity, including, but not limited to, a Otonomic official, forum leader, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity;</p>
            <p><b>(d)</b> forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted through the Service;</p>
            <p><b>(e)</b> upload, post or otherwise transmit any Content that you do not have a right to transmit under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements);</p>
            <p><b>(f)</b> upload, post or otherwise transmit any Content that infringes any patent, trademark, trade secret, copyright, rights of privacy or publicity, or other proprietary rights of any party;</p>
            <p><b>(g)</b> upload, post, or transmit unsolicited commercial email or "spam". This includes unethical marketing, advertising, or any other practice that is in any way connected with "spam", such as (i) sending mass email to recipients who haven't requested email from you or with a fake return address, (ii) promoting a site with inappropriate links, titles, descriptions, or (iii) promoting your site by posting multiple submissions in public forums that are identical;</p>
            <p><b>(h)</b> upload, post or otherwise transmit any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</p>
            <p><b>(i)</b> interfere with or disrupt the Service or servers or networks connected to the Service, or disobey any requirements, procedures, policies or regulations of networks connected to the Service;</p>
            <p><b>(j)</b> intentionally or unintentionally violate any applicable local, state, national or international law, including, but not limited to, regulations promulgated by the U.S. Securities and Exchange Commission, any rules of any national or other securities exchange, including without limitation, the New York Stock Exchange, the American Stock Exchange or the NASDAQ, and any regulations having the force of law;</p>
            <p><b>(k)</b> "stalk" or otherwise harass another;</p>
            <p><b>(l)</b> promote or provide instructional information about illegal activities, promote physical harm or injury against any group or individual, or promote any act of cruelty to animals. This may include, without limitation, providing instructions on how to assemble bombs, grenades and other weapons or incendiary devices;</p>
            <p><b>(m)</b> offer for sale or sell any item, good or service that (i) violates any applicable federal, state, or local law or regulation, (ii) you do not have full power and authority under all relevant laws and regulations to offer and sell, including all necessary licenses and authorizations, or (iii) we determine, in our sole discretion, is inappropriate for sale through the Service;</p>
            <p><b>(n)</b> use the Service as a forwarding service to another web site;</p>
            <p><b>(o)</b> solicit a third party’s passwords or personal identifying information for unlawful or phishing purposes;</p>
            <p><b>(p)</b> exceed the scope of the Service that you have signed up for; for example, by accessing and using the tools that you do not have a right to use, or deleting, adding to, or otherwise changing other peoples comments or content; (q) upload, post or otherwise transmit any Content that is intended to take advantage of a user. Such content may include, but is not limited to, "get rich quick", "get paid to surf", pyramid/MLM, or other dubious schemes.</p>
            <p><b>(q)</b> include more than three ad units per page, or any advertising that greatly reduces the usability of the site.</p>
            <p><b>(r)</b> upload files for the sole purpose of having them hosted by us and for use outside of a web site created using the Service.</p>
            <p><b>(s)</b> create a web site that provides an injurious user experience with custom programming. Examples include, but are not limited to, extreme flashing banners and excessive animated movement.</p>
            <p><b>(t)</b> upload, post or otherwise transmit any Content that is adult in nature, such as any nudity in a sexual context, any Content revealing exposed genitalia, or any Content with adult themes. We retain the right to terminate any account or user who has violated any of the above prohibitions.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">9</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Fees/payment</h3>
            <p>Some of the features on the Service require payment of fees. If you elect to sign up for these features, you shall pay all applicable fees, as described on the Service in connection with such features selected by you. We reserve the right to change our prices and at any time. You authorize us to make any reasonably necessary inquiries to validate your account and financial information. All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and you shall be responsible for payment thereof. You agree to pay for any taxes that might be applicable to your use of the Service and payments you make to us.</p>
            <p><b>Minors are not allowed to purchase Premium/Upgraded Services. Accordingly, by purchasing such services the user declares and represents that he/she is more than 18 years of age and that he/she has full legal capacity to complete such contractual action without need for any additional approvals or consents. </b></p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">10</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Money Back Guarantee</h3>
            <p>Otonomic Pro Accounts include a 30-day money back guarantee. If you are dissatisfied with the pro account service for any reason, you can receive a full refund if you cancel your pro account within 30 days of activation. Please direct refund requests to support@otonomic.com with the subject line: Refund Request. If you elect to purchase your own domain name, this purchase is non-refundable and not subject to this money back guarantee. Downgrading your account may cause the loss of content, features, or capacity of your account. We do not accept any liability for such loss.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">11</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Additional Software</h3>
            <p>If you elect to download or access any additional software or third party content made available by us through the Service, you must agree to additional terms and conditions before you use such software or third party content. If you do not agree to the third party’s terms of service or license agreement, do not download the software or content. Your use of any third party software or content obtained through the Service does not transfer to you any rights, title or interest in or to the third party software or such content beyond the terms contained in the third party provider's terms of service or license.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">12</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>International Use</h3>
            <p>Recognizing the global nature of the Internet, you agree to comply with all local rules regarding online conduct and acceptable content. Specifically, you agree to comply with all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside.</p>
          </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <div class="big-letter">13</div>
          </div>
          <div class="col-xs-11 col-md-9">
            <h3>Links</h3>
            <p>The Service may provide, or third parties may provide, links to other web sites or resources. Because we have no control over such sites and resources, we are not responsible for the availability of such external sites or resources, and we do not endorse and are not responsible or liable for any content, advertising, products, or other materials on or available from such sites or resources. We are not liable for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such site or resource.</p>
          </div>
        </div>
      </section>
    </div>
    <!-- Footer -->
  <?php include 'footer.php'; ?>