  <!-- Header -->
  <?php include 'header.php'; ?>

  <div id="main-wrapper" class="support-center">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Support center</h1>
        <!-- Social buttons -->
        <?php include 'social-buttons.php'; ?>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-12">
              <h3>What we give you</h3>
              <div class="panel-group" id="accordion">
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <span class="num">1</span>I want to create my own gorgeous website
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. <p><img src="images/press1.jpg" class="img-responsive" alt="Image alt"></p> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <span class="num">2</span>I want my website ASAP
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <span class="num">3</span>I need more than one site
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- CTA -->
          <div class="row">
              <div class="col-md-12">
                <span>1-click website from your facebook page</span>
                <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
              </div>
          </div><!-- /CTA -->
          <div class="row">
            <div class="col-md-12">
              <h3>How to create your website</h3>
              <div class="panel-group" id="accordion2">
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle yellow" data-toggle="collapse" data-parent="#accordion2" href="#collapse2-1">
                        <span class="num">1</span>I want to create my own gorgeous website
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2-1" class="panel-collapse collapse in">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle yellow collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2-2">
                        <span class="num">2</span>I want my website ASAP
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2-2" class="panel-collapse collapse">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="panel panel-oto">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle yellow collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2-3">
                        <span class="num">3</span>I need more than one site
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2-3" class="panel-collapse collapse">
                    <div class="panel-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- CTA -->
          <div class="row">
              <div class="col-md-12">
                <span>1-click website from your facebook page</span>
                <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
              </div>
          </div><!-- /CTA -->
          <div class="row">
            <div class="col-md-12">
              <h3>Did you find your answer</h3>
              <div class="row">
                <div class="col-md-5">
                  <h4 class="contact-title">Send us an Email</h4>
                </div>
                <div class="col-md-7">
                  <a href="mailto:support@otonomic.com" class="btn btn-block btn-oto-blue btn-xlarge"><span class="glyphicons message_full"></span> support@otonomic.com</a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5">
                  <h4 class="contact-title">Or fill a contact form</h4>
                </div>
                <div class="col-md-7">
                  <!-- <a href="#" class="btn btn-block btn-oto-blue btn-xlarge"><span class="glyphicons notes_2"></span> Fill out this form</a> -->
                    <div class="panel-group" id="contactAccordion">
                        <div class="panel panel-oto panel-blue">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#contactAccordion" href="#contactAccordion1">
                                <span class="glyphicons notes_2"></span> Fill out this form
                              </a>
                            </h4>
                          </div>
                          <div id="contactAccordion1" class="panel-collapse collapse">
                            <div class="panel-body">
                              <form role="form">
                                    <div class="form-group">
                                        <label for="inputName">Name</label>
                                        <input type="text" class="form-control" id="inputName" placeholder="e.g John Doe">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="email" class="form-control" id="inputEmail" placeholder="e.g johndoe@gmail.com">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputMessage">What's on your mind</label>
                                        <textarea name="inputMessage" class="form-control" rows="3" placeholder="Tell us what's bothering you"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-oto-blue btn-cornered">
                                        Let us know
                                    </button>
                                </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group has-feedback">
              <label class="control-label sr-only" for="search-faq-field">Search faq...</label>
              <input type="text" id="search-faq-field" class="form-control" placeholder="Search faq...">
              <span class="glyphicons search form-control-feedback js-search-icon"></span>
          </div>
          <a href="#website" class="btn btn-oto-white">Website</a>
          <a href="#website" class="btn btn-oto-white">Brand</a>
          <a href="#website" class="btn btn-oto-white">Store</a>
          <a href="#website" class="btn btn-oto-white">Widgets</a>
          <a href="#website" class="btn btn-oto-white">Responsive</a>
          <a href="#website" class="btn btn-oto-white">Parallax</a>
          <a href="#website" class="btn btn-oto-white">Mobile</a>
          <a href="#website" class="btn btn-oto-white">Log-In</a>
        </div>
      </div>
    </div> <!-- /container -->
    <!-- Footer -->
    <?php include 'footer.php'; ?>