    <!-- Header -->
    <?php include 'header.php'; ?>
    <div id="main-wrapper" class="about-us">
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
          <div class="container">
            <h1>About Otonomic</h1>
            <!-- Social buttons -->
            <?php include 'social-buttons.php'; ?>
          </div>
        </div>

        <div class="container">
          <section >
            <div class="row">
                <div class="col-md-9">
                    <h3>We are Otonomic</h3>
                    <p class="text-block">Otonomic is the world's simplest website builder. In a single click, Otonomic turns a user's Facebook business page into a professional website that updates automatically with new content posted on Facebook and other social media. The company is backed by leading VCs and angel investors, and launched its product on May 2014 with over 30,000 sites created in the platform so far.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h3>Management</h3>
                    <div class="row management">
                        <div class="col-xs-4">
                            <div class="thumbnail">
                              <img src="images/ceo-omri.png" alt="Omri Allouche  Founder & CEO" style="width:210px;">
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <div class="details">
                                <p><b class="name">Omri Allouche</b> Founder & CEO</p>
                                <p>An experienced entrepreneur, formerly the founder of social marketing platform Zink.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row management">
                        <div class="col-xs-4">
                            <div class="thumbnail">
                              <img src="images/vp-products-edik.png" alt="Edik Mitelman VP Products">
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <div class="details">
                                <p><b class="name">Edik Mitelman</b> VP Products</p>
                                <p>An experienced entrepreneur, formerly the founder of social marketing platform Zink.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row management">
                        <div class="col-xs-4">
                            <div class="thumbnail">
                              <img src="images/vp-design-ilan.png" alt="Ilan Lichtnaier VP Design">
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <div class="details">
                                <p><b class="name">Ilan Lichtnaier</b> VP Design</p>
                                <p>Formerly of Conduit, HP and Netcraft.</p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row management">
                        <div class="col-md-4">
                            <div class="thumbnail">
                              <img src="images/marketing-yoav.png" alt="Yoav Tzuker Head of Online Marketing">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="details">
                                <p><b class="name">Yoav Tzuker</b> Head of Online Marketing</p>
                                <p>Formerly of Conduit, HP and Netcraft.</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
          </section>
          <section>
                <div class="row">
                <div class="col-md-9">
                    <h3>The story of Otonomic</h3>
                    <p class="text-block">Otonomic is the world's simplest website builder. In a single click, Otonomic turns a user's Facebook business page into a professional website that updates automatically with new content posted on Facebook and other social media. The company is backed by leading VCs and angel investors, and launched its product on May 2014 with over 30,000 sites created in the platform so far.</p>
                    <p class="text-block">In a single click, Otonomic turns a user's Facebook business page into a professional website that updates automatically with new content posted on Facebook and other social media. The company is backed by leading VCs and angel investors.</p>
                </div>
            </div>
          </section>
          <section>
                <div class="row">
                    <div class="col-md-12">
                        <h3>In the media</h3>
                    </div>
                    <div class="col-md-12 media-images">
                        <div class="row">
                            <div class="thumbnail col-xs-6 col-md-2">
                                <img src="images/forbes.png" alt="...">
                            </div>
                            <div class="thumbnail col-xs-6 col-md-2">
                                <img src="images/parade.png" alt="...">
                            </div>
                            <div class="thumbnail col-xs-6 col-md-2">
                                <img src="images/inc.png" alt="...">
                            </div>
                            <div class="thumbnail col-xs-6 col-md-2">
                                <img src="images/allfacebook.png" alt="...">
                            </div>
                            <div class="thumbnail col-xs-6 col-md-2">
                                <img src="images/fox.png" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
          </section>
          <section>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Press releases</h3>
                    </div>
                    <div class="col-xs-12 col-sm-6 oto-app-holder">
                      <div class="thumbnail">
                        <a href="#">
                            <img src="images/press1.jpg" alt="Press 1">
                            <h4>The quick, brown fox jumps over a lazy dog flock by MTV</h4>
                            <p>A quick movement of the enemy will jeopardize six gunboats. All questions asked by five watch experts amazed the judge. Jack quietly moved up front...</p>
                            <button class="btn btn-oto-blue btn-cornered">
                                <span class="glyphicons new_window_alt"></span>
                            </button>
                        </a>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 oto-app-holder">
                      <div class="thumbnail">
                        <a href="#">
                            <img src="images/press2.jpg" alt="Press 1">
                            <h4>Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz</h4>
                            <p>A quick movement of the enemy will jeopardize six gunboats. All questions asked by five watch experts amazed the judge. Jack quietly moved up front...</p>
                            <button class="btn btn-oto-blue btn-cornered">
                                <span class="glyphicons new_window_alt"></span>
                            </button>
                        </a>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 oto-app-holder">
                      <div class="thumbnail">
                        <a href="#">
                            <img src="images/press3.jpg" alt="Press 1">
                            <h4>The quick, brown fox jumps over a lazy dog flock by MTV</h4>
                            <p>A quick movement of the enemy will jeopardize six gunboats. All questions asked by five watch experts amazed the judge. Jack quietly moved up front...</p>
                            <button class="btn btn-oto-blue btn-cornered">
                                <span class="glyphicons new_window_alt"></span>
                            </button>
                        </a>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 oto-app-holder">
                      <div class="thumbnail">
                        <a href="#">
                            <img src="images/press3.jpg" alt="Press 1">
                            <h4>The quick, brown fox jumps over a lazy dog flock by MTV</h4>
                            <p>A quick movement of the enemy will jeopardize six gunboats. All questions asked by five watch experts amazed the judge. Jack quietly moved up front...</p>
                            <button class="btn btn-oto-blue btn-cornered">
                                <span class="glyphicons new_window_alt"></span>
                            </button>
                        </a>
                      </div>
                    </div>
                </div>
          </section>
        </div> <!-- /container -->
    <!-- Footer -->
    <?php include 'footer.php'; ?>