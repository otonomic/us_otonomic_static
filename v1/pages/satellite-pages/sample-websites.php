    <!-- Header -->
    <?php include 'header.php'; ?>
    <div id="main-wrapper" class="sample-websites">
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
          <div class="container">
            <h1>Sample websites</h1>
            <!-- Social buttons -->
            <?php include 'social-buttons.php'; ?>
          </div>
        </div>

        <div class="container">
          <section >
            <div class="row">
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-1.jpg" class="img-responsive">
                  <h4>Babies making poop</h4>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-2.jpg" class="img-responsive">
                  <h4>Beer on the go</h4>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-3.jpg" class="img-responsive">
                  <h4>Summer moves on</h4>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-4.jpg" class="img-responsive">
                  <h4>Jewcer</h4>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-5.jpg" class="img-responsive">
                  <h4>David Gahan Lookalikes</h4>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <div class="thumbnail">
                  <img src="images/sample-websites/example-site-6.jpg" class="img-responsive">
                  <h4>Reflections</h4>
                </div>
              </div>
            </div>
          </section>
          <div class="row">
              <div class="col-xs-12">
                <span style="margin-right: 10px;">1-click website from your facebook page</span>
                <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
              </div>
          </div>
        </div>
        <!-- Footer -->
    <?php include 'footer.php'; ?>