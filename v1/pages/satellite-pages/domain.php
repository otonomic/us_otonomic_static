  <!-- Header -->
  <?php include 'header.php'; ?>

  <div id="main-wrapper" class="domain">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Domain name</h1>
        <!-- Social buttons -->
        <?php include 'social-buttons.php'; ?>
      </div>
    </div>
    <div class="container">
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons shopping_bag big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>Buy a New Domain</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>It’s easy to purchase your own custom domain for your Otonomic website.</h4>
            <p>Think about a name you want to use, search, buy (or get one free with the Yearly Unlimited Plan) and connect.<br>
            <a href="#">Once you purchase a domain</a>, simply subscribe to any one of our Premium Plans and follow the steps to connect it to your website. </p>
         </div>
        </div>
      </section>
      <div class="row">
        <div class="col-md-11 col-md-offset-1">
          <span>1-click website from your facebook page</span>
          <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
        </div>
      </div>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons globe_af big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>What’s a Domain?</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>A domain name, like www.mysite.com, is the address where visitors find your website online.</h4>
            <p>It is sometimes referred to as a URL or website address, and is your unique web identity.</p>
            <h4>Why do you need one?</h4>
            <p>A custom domain name for your website, like www.yourbusinessname.com, lets you personalize your web address, giving your business professional credibility and a super easy way for people to find you on the web.</p>
            <h4>Why get your domain through Otonomic?</h4>
            <p>Think of Otonomic as a one-stop shop. You can create your complete professional online presence - in one place. <a href="#">Register your own domain name at Otonomic, set up personalized email</a>, create a stunning website, and get top-grade hosting. <a href="#">Read more</a>.</p>
         </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons gift big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>Your Otonomic Site’s Free Domain</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>Otonomic provides everything you need to create your own professional website for free.</h4>
            <p>This includes a free website domain with a Otonomic address. For example, yourname.Otonomic.com/yoursitename. It’s a quick and hassle-free way to get your website online, until you are ready for a custom domain. Ready for your custom domain? We have a few ways you can get it done directly on Otonomic.com whether you need to connect, transfer or get a new domain.</p>
         </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons shirt big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>Get the Domain that’s Right for You</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>Choosing a domain name.</h4>
            <p>Your domain name is the first thing people see when they go to your website - so you want to get it right! We’ve created some <a href="#">tips & guidelines</a> to help you choose one that works for your website and business.</p>
            <h4>.Com? .Org? .Info?</h4>
            <p>We’re sure you’ve seen these before, but maybe you didn’t know that they’re officially called the <a href="#">top-level domain</a> (TLD) of a web address. With Otonomic, you can choose the TLD you want. For example, the TLD of www.Otonomic.com is .com. Each TLD has its own benefits, so make sure to choose the one that best suits your business and website.</p>
            <h4>Keep your personal information private</h4>
            <p>Otonomic offers two levels of privacy for your domain registration: public and private. When you register your domain, you enter your contact details into the “WHOIS” directory, a sort of online Yellow Pages that provides information about who owns each domain on the web. Private registration lets you keep your personal details confidential and unlisted. <a href="#">Read more</a>.</p>
         </div>
        </div>
      </section>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons retweet big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>Connect or Transfer a Domain</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>Already own your own domain? You can easily connect it to your Otonomic site.</h4>
            <p>Once you subscribe to any one of our <a href="#">Premium Plans</a>, simply follow the steps to connect your domain address to your Otonomic website.</p>
            <h4>Want to transfer your domain to Otonomic?</h4>
            <p>No problem! Once you subscribe to any one of our Premium Plans, use our simple <a href="#">step-by-step</a> guide to transfer your domain to Otonomic from a different registrar. This way you can manage your domain and site from the same place.</p>
         </div>
        </div>
      </section>
      <div class="row">
        <div class="col-md-11 col-md-offset-1">
          <span>1-click website from your facebook page</span>
          <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
        </div>
      </div>
      <section class="feature">
        <div class="row">
          <div class="col-xs-1 hidden-xs">
            <span class="glyphicons gift big-icon"></span>
          </div>
          <div class="col-xs-11">
            <h3>Get a New Custom Domain For Free</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1">
            <h4>Get a free custom domain* with our Yearly Unlimited Plan and connect it to your Otonomic site.</h4>
            <p>The Yearly Unlimited plan, which includes lots of additional benefits, is especially popular among small and medium business owners because it’s perfect for promoting your business.</p>
            <p><small>* Valid for one year free domain.</small></p>
         </div>
        </div>
      </section>
    </div>
  <!-- Footer -->
  <?php include 'footer.php'; ?>