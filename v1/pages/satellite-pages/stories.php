    <!-- Header -->
    <?php include 'header.php'; ?>
    <div id="main-wrapper" class="stories">
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
          <div class="container">
            <h1>Sample websites</h1>
            <!-- Social buttons -->
            <?php include 'social-buttons.php'; ?>
          </div>
        </div>

        <div class="container">
          <section class="story">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                  <a href="#">
                  <img class="fold right" src="images/stories/fold-1.png">
                  <img src="images/stories/story-img-1.jpg">
                  <button class="btn btn-oto-blue btn-cornered">
                      <span class="glyphicons new_window_alt"></span>
                  </button>
                  </a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <h3 class="name">Dave Flanaghan</h3>
                <a href="http://www.artisticembracetattoo.com">www.artisticembracetattoo.com</a>
                <p class="quote"><img src="images/stories/quotes.png">I've had my website for a little over a month now and my clients absolutely love it!!! They have told me that it's extremely easy to navigate and it looks awesome! I'm really glad I went with Otonomic. I couldn't have had a better website!</p>
              </div>
            </div>
          </section>
          <section class="story">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <h3 class="name">Shawna Todd</h3>
                <a href="http://www.boulderacupuncture.me">www.boulderacupuncture.me</a>
                <p class="quote"><img src="images/stories/quotes.png">I have been wanting a website for my small business for too long. I never found the time or energy to get it done. Now I have a beautiful professional site and I never had to take time out of my schedule to get it! I'm so grateful, I wish I had done it sooner.</p>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                  <a href="#">
                  <img class="fold left" src="images/stories/fold-2.png">
                  <img src="images/stories/story-img-2.jpg">
                  <button class="btn btn-oto-blue btn-cornered">
                      <span class="glyphicons new_window_alt"></span>
                  </button>
                  </a>
                </div>
              </div>
            </div>
          </section>
          <div class="row">
              <div class="col-xs-12 text-center">
                <span>1-click website from your facebook page</span>
                <a href="#" class="btn navbar-btn btn-oto-orange">Create Your Website Now</a>
              </div>
          </div>
          <section class="story">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                  <a href="#">
                  <img class="fold right" src="images/stories/fold-3.png">
                  <img src="images/stories/story-img-3.jpg">
                  <button class="btn btn-oto-blue btn-cornered">
                      <span class="glyphicons new_window_alt"></span>
                  </button>
                  </a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <h3 class="name">Steve Bross</h3>
                <a href="http://www.thehawkmobile.com">www.thehawkmobile.com</a>
                <p class="quote"><img src="images/stories/quotes.png">I've had my website for a little over a month now and my clients absolutely love it!!! They have told me that it's extremely easy to navigate and it looks awesome! I'm really glad I went with Otonomic. I couldn't have had a better website!</p>
              </div>
            </div>
          </section>
          <section class="story">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <h3 class="name">Brian O'Callaghan</h3>
                <a href="http://www.dublinacupunctureclinic.com">www.dublinacupunctureclinic.com</a>
                <p class="quote"><img src="images/stories/quotes.png">Otonomic offered me what I need at the time I needed it - a well designed, informative website that I update effortlessly wherever I travel.</p>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                  <a href="#">
                  <img class="fold left" src="images/stories/fold-4.png">
                  <img src="images/stories/story-img-4.jpg">
                  <button class="btn btn-oto-blue btn-cornered">
                      <span class="glyphicons new_window_alt"></span>
                  </button>
                  </a>
                </div>
              </div>
            </div>
          </section>
        </div>
    <!-- Footer -->
    <?php include 'footer.php'; ?>