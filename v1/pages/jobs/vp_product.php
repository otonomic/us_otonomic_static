<?php $position = 'VP Product'; ?>

<!DOCTYPE html>
<html>
<head>
    <title>Otonomic Jobs - <?= $position?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Otonomic is looking for a new <?= $position?>. Please help by sharing!" />
    <!-- Open Graph data -->
    <meta property="og:title" content="Otonomic Jobs - <?= $position?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="Otonomic is looking for a new <?= $position?>. Please help by sharing!" />
    <meta property="fb:admins" content="19717048">
    <meta property="fb:app_id" content="334469486646650">


    <link href="assets/css/fonts.css" rel="stylesheet" media="screen">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <link href="assets/css/style.css" rel="stylesheet" media="screen">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type='text/javascript' src='/js/otonomic-analytics.js'></script>
    <script src="//platform.linkedin.com/in.js" type="text/javascript">
        lang: en_US
    </script>
  </head>
  <body>
  	<div id="header">
		<div class="container">
			<div class="pull-left">
				<div id="logo">
					<img src="assets/img/logo.png" />
				</div>
			</div>
			<div class="pull-right">
				<div id="page-like">
					<div class="fb-like" data-href="http://www.facebook.com/otonomic/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
				</div>
			</div>
		</div>
	</div><!-- #header-ends -->
	<div id="page-heading">
		<div class="container">
			<div class="row-fluid">
				<div class="span8">
					<h1 class="page-title"><?= $position?></h1>
				</div>
				<div class="span4">
					<img src="assets/img/team.png" class="heading-img" />
				</div>
			</div>
		</div>
	</div><!-- #page-heading ends -->
	<div id="content">
		<div class="container">
			<div class="row-fluid">
				<div class="span8">
					<p>
						Otonomic is the world's simplest website builder. In a single click, Otonomic turns a user's Facebook business page into a professional website that updates automatically with new content posted on Facebook and other social media.
                        <br/>
                        The company is backed by leading VCs and angel investors, and launched its product on May 2014 with over 30,000 sites created in the platform so far.
					</p>

					<h2>Responsibilities</h2>
					<ul>
                        <li>
                            Oversee all marketing content initiatives, both internal and external, across multiple platforms and formats to drive sales, engagement, retention, leads and positive customer behavior
                            </li> <li>Be in charge of the company's content assets, including our site, newsletter, blog, and social media
                            </li> <li>Lead the development of content initiatives in all forms to drive new and current business. This includes ensuring all content is on-brand, consistent in terms of style, quality and tone of voice, and optimized for search and user experience for all channels of content including online, social media, email, mobile, video, print and in-person
                            </li> <li>Supervise writers, editors, content strategists; be an arbiter of best practices in grammar, messaging, writing, and style
                            </li> <li>Build relationships with the community by engaging with small business owners and influencers on blogs, social media and webinars
                            </li> <li>Identify trends, user needs and new content opportunities and ways to repurpose content created by our community
                            </li> <li>Conduct ongoing usability tests to gauge content effectiveness. Gather data, handle analytics and make recommendations based on those results
                            </li> <li>Analyze key metrics of user engagement and implement improvements
                            </li> <li>Leverage market data to develop content themes, topics and execute a plan to develop the assets that support a point of view and educate customers that leads to critical behavioral metrics
                            </li> <li>Provide basic customer service
                            </li> <li>Establishing work flow for requesting, creating, editing, publishing, and retiring content
                        </li>
                    </ul>

					<h2>Requirements</h2>
                    <ul>
                        <li>
                            Native English speaker. Familiarity with American culture a plus
                            </li><li>2+ years of work experience as a marketing specialist, copywriter, social media manager or a similar position for an international brand
                            </li><li>Superb writing skills. Able to effectively communicate ideas in our brand voice
                            </li><li>Experience with creating compelling messages for different target demographics. Crisis communications experience a plus
                            </li><li>Expertise in all things related to content and channel optimization, brand consistency, segmentation and localization, analytics and meaningful measurement
                            </li><li>High energy, can-do attitude with willingness to do what it takes to get things done
                            </li><li>Must be self-directed, able to work independently, as well as work in a team-oriented and fast paced environment as an individual contributor
                        </li>
                    </ul>
                    <p>Please email your CV and two writing samples to <a href="mailto:jobs@otonomic.com">jobs@otonomic.com</a>.</p>
				</div>

				<div class="span4">
					<div id="share">
						<ul class="social-share">
							<li class="google-plus">
								<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60"></div>
							</li>
							<li class="facebook">
								<!-- div class="fb-share-button" data-href="http://www.facebook.com/otonomic/"></div -->
								<a name="fb_share" type="box_count">Share</a>
							</li>
							<li class="tweeter">
								<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a>
							</li>
							<li class="linkedin">
								<script type="IN/Share" data-counter="top"></script>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!-- #content ends -->
	<div id="footer">
		<div class="row-fluid">
			<div class="span7 offset5">
				<div class="pull-right align-right">
					<img src="assets/img/tablet-dude.png" id="footer-overlap" />
				</div>
			</div>
		</div>
	</div><!-- #footer ends -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=373931652687761&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript">
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/platform.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
	</script>
	<script>
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
	</script>
	<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
  </body>
</html>