function timed_submit(submit_function, submit_parameter) {
    if (window.is_blog_ready == 1) {
        submit_function();
    } else {
        window[submit_parameter] = 1;
    }
}

function callback(data) {
    window.is_blog_ready = 1;

    if (data.redirect.indexOf("http://") < 0) {
        data.redirect = "http://" + data.redirect;
    }

    if (data.site_url.indexOf("http://") < 0) {
        data.site_url = "http://" + data.site_url;
    }

    if (data.status == 'fail') {
        //window.location = data.site_url;
        console.log('Site Exists');
        ot_analytics.track(
            'siteExists',
            'installer',
            {
                'message':data.message,
                'url':data.site_url
            }
        );
        ot_analytics.track('Account Manage', 'Site Exists', data.message);
        ga('set', 'metric6', '1');
        // track_virtual_pageview('site_exists');

    } else {
        var page_type = window.page_type || 'Fan Page';

        ot_analytics.track(
            'siteCreated',
            'installer',
            {
                'url':data.site_url
            }
        );

        ot_analytics.track('Account Manage', 'Site Created', page_type);
        ga('set', 'metric4', '1');
        // track_virtual_pageview('site_created');
    }

    <!-- START Facebook Pixel Tracking for Site created-->
    window._fbq = window._fbq || [];
    /*if(!is_localhost()) {
        window._fbq.push(['track', facebook_site_created_pixel_id, {'value':'0.00', 'currency':'USD'}]);
    }*/
    <!-- END Facebook Pixel Tracking -->

    window.site_url = data.site_url;
    // window.blog_redirect = data.redirect;
    window.blog_redirect = data.site_url;
    window.blog_id = data.blog_id;
    window.token = data.token;

    jQuery('#oto-web-url').html('<a href="'+data.redirect+'">'+data.site_url+'</a>');

    if( data.status === 'fail') {
        /* Switch to error window */
        if(data.message != 'Sorry, that site already exists!') {
            if (progressTimer)
                clearInterval(progressTimer);
            /* Now move to error slide */
            ot_analytics.track(
                'Onboarding errors',
                'Installer',
                {
                    'pageId':'stuck'
                }
            );
            move_slide_by_id('bother-stuck');
            window.location.hash = '#bother-stuck';
            if (data.message)
                jQuery('#gen-error-message').html(data.message);

            /*if(data.message != 'Sorry, that site already exists!') {
             alert(data.message);
             }*/
            //window.location.replace(data.redirect);
        }
        return;
    }

    blog_created();

    redirect_to_website();
}

function blog_created() {
    ot_analytics.track(
        'blogCreated',
        'installer',
        {}
    );
    window.callbacks = window.callbacks || [];
    $.each( window.callbacks, function(index, callback_function) {
        window[callback_function]();
    });

    send_user_fb_details();
    return;
}

function redirect_to_website() {
    if(window.do_redirect == 1 && window.is_blog_ready == 1) {
        window.location.replace(window.blog_redirect);
    }
}




function enqueue_submit(setting, value, callback_function) {
    window[setting] = value;

    if(window.is_blog_ready) {
        window[callback_function]();

    } else {
        window.callbacks = window.callbacks || [];
        window.callbacks.push(callback_function);
    }
}

function send_template() {
    var skin = window.skin || '';
    ot_analytics.track('Loading Page', 'Select Template', skin);
    return post_WP_settings({ skin: skin }, 'Select Template');
}

function post_WP_settings(data, tracking_action, endpoint) {
    endpoint = endpoint  || 'settings.set_many';
    tracking_action = tracking_action  || data;

    return request = $.ajax({
        type: "POST",
        url: window.site_url + '/?json=' + endpoint,
        data: { values: data },
        success: function (data, status, jqxhr) {
            if (jqxhr.status == 307) {
                $.post(window.site_url + '/?json=settings.set_many', { values: values_changes });
                ot_analytics.track('Loading Page', tracking_action, '307');
                return;
            }
            if (data.status == "ok") {
                ot_analytics.track('Loading Page', tracking_action, 'Success');
            } else {
                ot_analytics.track('Loading Page', tracking_action, 'Failure: data.respond.msg: ' + (data.respond && data.respond.msg));
            }
        },
        complete: function (jqxhr, status) {
            if (status !== 'success') {
                ot_analytics.track('Loading Page', tracking_action, 'Failure: ' + status);
            }
        }
    });
}

function send_user_fb_details()
{
    fb_user_auth = 'yes';
    fb_user_id = jQuery('#fb_user_id').val();
    fb_user_t = jQuery('#fb_user_token').val();

    if(fb_user_auth == 'yes')
    {
        var settings_data = {
            wp_otonomic_blog_connected: 'yes',
            otonomic_connected_fb_user_id: fb_user_id,
            otonomic_connected_fb_user_token: fb_user_t
        };
        post_WP_settings(settings_data, 'FB Connected');
    }
}

function createWebsiteUsingAjax(page_id) {
    var request_data = {};
    request_data.theme = "dreamthemeVC";
    request_data.facebook_id = encodeURIComponent(page_id);

    // var request_url = "http://wp.otonomic.com/migration/index.php?" + $.param(request_data);
    localhost = is_localhost();

    var request_url;
    request_url = builder_domain+"/migration/index.php";

    return $.ajax({
        url: request_url,
        type: "GET",
        dataType: "jsonp",
        data: request_data,
        jsonp: "callback",
        jsonpCallback: "callback"
    });
}

function business_name() {
    return post_WP_settings({ blogname: jQuery('#business_name_confirm').val() });
}
function submit_user_email() {
    var user_email = window.user_email || '';
    ot_analytics.track('Installer', 'Submit Email', user_email);
    var user_id = window.user_id;
    return post_WP_settings({ user_email: user_email, user_id: user_id }, 'Submit Email', 'settings.set_user_email');

}