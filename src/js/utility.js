function getParameterByName(name) {
    return getParameterByNameFromString(name, location.search);
}

function getParameterByNameFromString(name, str) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(str);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function is_localhost() {
    if (location.host == 'otonomic.test' || location.host == 'localhost') {
        return true;
    }

    return false;
}