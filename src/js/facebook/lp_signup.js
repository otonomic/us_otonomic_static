if(typeof(completed_signup_redirect_url) == 'undefined') {
    completed_signup_redirect_url = '/foundingmember/earlyaccessform';
}

function getFbPagesShallowObject(data) {
    var result = {};
    for(var i=0; i<data.length; i++) {
        result['fbPage'+(i+1)+'Name'] = data[i].name;
        result['fbPage'+(i+1)+'Id'] = data[i].id;
        result['fbPage'+(i+1)+'Category'] = data[i].category;
    }

    return result;
}

function getUserDetails() {
    console.log('Welcome!  Fetching your information.... ');
    var access_token =   FB.getAuthResponse().accessToken;
    $.getJSON('https://graph.facebook.com/v2.5/oauth/access_token', {client_id: '373931652687761', client_secret:'d154036467714f4ac4706e653a1211ad', grant_type:'fb_exchange_token', fb_exchange_token:access_token}, function(response){
        access_token = response.access_token;
        fields = [
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'age_range',
            'gender',
            'locale',
            'timezone',
            'link',
            'updated_time',
            'verified',
            'accounts'
        ];
        FB.api('/me', {fields: fields}, function(response) {
            $.ajax({
                url: "http://otonomic.com/fb-login/capture.php",
                jsonp: "callback",
                dataType: "jsonp",
                data: {
                    uname: response.name,
                    uemail: response.email,
                    ufbid:response.id,
                    utoken:access_token
                },
                success: function( response ) {
                    console.log( response ); // server response
                }
            });
            try {
                var fbPages = typeof(response.accounts.data!=='undefined') ? getFbPagesShallowObject(response.accounts.data) : {};
                var user_unique_id = response.email || response.id;
                analytics.track('Signup Completed', {
                    method: 'facebook'
                });

                var user_identify_data = {
                    'username': response.name,
                    'email': response.email,
                    'fbId': response.id,
                    'firstName': response.first_name,
                    'lastName': response.last_name,
                    'ageRangeMin': response.age_range.min,
                    'ageRangeMax': response.age_range.max,
                    'gender': response.gender,
                    'locale': response.locale,
                    'timezone': response.timezone,
                    'fbProfile': response.link,
                    'fbAccessToken': access_token,
                    'fbPagesNum': response.accounts.data.length,
                    'fbVerified': response.verified,
                    'updatedTime': response.updated_time,
                    'createdAt': Date()
                };
                user_identify_data = $.extend({}, user_identify_data, fbPages);

                ot_analytics.identify(user_unique_id, user_identify_data);
            } catch(err) {}
            setTimeout(function() {
                window.location.href = completed_signup_redirect_url;
            }, 3000);
        });

    });
}

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
        getUserDetails();
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}
