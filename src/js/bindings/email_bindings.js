(function ($, window, undefined) {
    $("#user_email").on("keyup paste blur", function() {
        $("#user_email_confirm").val($(this).val());
        activateEmailNext();
    });

    $('#user_email').keypress(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#email-next").simulateClick();
        }
    });

    $('#email-next').bind('click', function (e){
        var email = $('#user_email').val();
        Cookies.set('user_email', email, { expires: 365 });
        var user_unique_id = email;
        Cookies.set('user_unique_id', user_unique_id, { expires: 365 });

        var data = {
            email:    email,
            createdAt:  Date()
        };

        ot_analytics.track(
            'User identified',
            'Installer',
            data
        );
        ot_analytics.identify(
            user_unique_id,
            data
        );
    });
})(jQuery, window);
