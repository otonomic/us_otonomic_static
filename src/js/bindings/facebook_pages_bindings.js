(function ($, window, undefined) {
    $('.reload-fb-pages').bind('click', function (e) {
        loadFBPages({
            fb_token: Cookies.get('user_fb_token'),
            suppressSlide: false,
            pageSlide: 'gather'
        });
    });
})(jQuery, window);
