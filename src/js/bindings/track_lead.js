(function ($, window, undefined) {
    $(document).on("click", "a", function(e) {
        var el = $(this);
        if(el.attr('href') == site.create_url) {
            ot_analytics.track('Signup Interested', {});
            ot_analytics.track('Lead', window.location.href);
        }
    });
    $(document).on("submit", "form", function(e) {
        var el = $(this);
        if(el.attr('action') == site.create_url) {
            ot_analytics.track('Signup Interested', {});
            ot_analytics.track('Lead', window.location.href);
        }
    });
})(jQuery, window);