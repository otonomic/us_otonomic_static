(function ($, window, undefined) {
    $("#user_email, #user_email_confirm").val(Cookies.get('user_email'));

    $("#business_name, #business_name_confirm").val(Cookies.get('user_business_name'));

    $('.replace-sitename').html(Cookies.get('user_business_name'));
    if (Cookies.get('oto_stage') !== undefined && !disable_automove) {
        window.location.hash = "#" + Cookies.get('oto_stage');
    }
    activateEmailNext();
    activateBNameNext();

    $('#fb-page-next').bind('click', function (e) {

        var selected_fb_page = $('#page-list .fb-page-select.selected').attr('data-fb-id');
        pageName = $('#page-list .fb-page-select.selected').attr('data-name');
        pageCategory = $('#page-list .fb-page-select.selected').attr('data-category');

        /*
        ot_analytics.track(
            'Signup Fb Page Submitted',
            'Installer',
            {
                pageDafault: selected_fb_page,
                fbPages: Cookies.get('fb_user_pages')
            }
        );
        */

        /* Verify this page */
        try {
            FB.api('/' + selected_fb_page, { fields: 'category,category_list,link,name', access_token: fb_app_token}, function (response) {
                if (response.id == selected_fb_page) {
                    pageSelected(response);

                } else {
                    pageSlide = 'bother-blocked';
                    ot_analytics.track(
                        'Onboarding errors',
                        'Installer',
                        {
                            'pageId': 'blocked'
                        }
                    );
                    move_slide_by_id(pageSlide);
                    window.location.hash = pageSlide;
                }
                console.log(response);
            });
        } catch (e) {
        }
    });

    $('.installer-stage:visible:first').find('input[type=text],input[type=email],textarea,select,.oto-fb-login').filter(':visible:first').focus();

    $('.websitedomain').html(Cookies.get('user_website_domain'));
})(jQuery, window);
