(function ($, window, undefined) {
    $("#business_name").on("keyup paste blur", function() {
        $("#business_name_confirm").val($(this).val());
        $('.replace-sitename').html($(this).val());
        activateBNameNext();
    });

    $('#business_name').keypress(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#bname-next").simulateClick();
        }
    });

    $('#bname-next').bind('click', function (e){
        var business_name = $('#business_name').val();
        Cookies.set('user_business_name', business_name, { expires: 365 });
        var user_unique_id = Cookies.get('user_unique_id');

        var data = {
            'siteName': business_name,
            'business': business_name
        };

        if(user_unique_id) {
            ot_analytics.identify(
                user_unique_id,
                data
            );
        }
    });
})(jQuery, window);
