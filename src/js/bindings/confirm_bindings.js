(function ($, window, undefined) {
    $("#user_email_confirm").on("keyup paste blur", function() {
        $("#user_email").val($(this).val());
        Cookies.set('user_email', $(this).val(), { expires: 30 });
    });

    $("#business_name_confirm").on("keyup paste blur", function() {
        $("#business_name").val($(this).val());
        $('.replace-sitename').html($(this).val());
        Cookies.set('user_business_name', $(this).val(), { expires: 30 });
    });

    $('.edit-link').bind('click', function(e){
        $href = '#confirm';
        if( $('#page-list .fb-page-select').length > 1 ) {
            $href = '#gather';
        }
        $('#bname-next').attr('href', $href);
    });
})(jQuery, window);
