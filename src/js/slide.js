function move_slide_by_id(id) {
    if(!id)
        id = "email";
    if($(".installer-stage:visible").attr('id') == id)
        return;
    $(".installer-stage:visible").fadeOut(100, function () {
        jQuery('#'+id).removeClass('hidden').fadeIn(100, function(){
            $('.installer-stage:visible:first').find('input[type=text],input[type=email],textarea,select,.oto-fb-login').filter(':visible:first').focus();
        });

        ot_analytics.track('Viewed ob slide: ' + id, 'onSlide', {"category": "Onboarding", "action": "Slide viewed", "label": id});
    });
}

function move_slide(pressed_button, event) {
    var current_slide = pressed_button.parents('.installer-stage');
    $('.notice').hide();
    var next_slide = current_slide.next().attr('id');
    // Check for required fields
    var required_fields = pressed_button.attr('data-required-fields');
    if(required_fields && required_fields.length) {
        required_fields = required_fields.split(',');
        var can_move_slide = true;
        $.each(required_fields, function (index, value) {
            if (!$(value).val()) {
                $(value + '-notice').show();
                can_move_slide = false;
            }
            if($(value).val() && (value == '#user_email' || value == '#user_email_confirm')){
                if(!is_valid_email($(value).val())){
                    $(value + '-valid-notice').show();
                    can_move_slide = false;
                }
            }
        });
        if (!can_move_slide) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
    }

    // Move to next slide
    current_slide.fadeOut(100, function () {
        current_slide.next().removeClass('hidden').fadeIn(100);
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });
    saveDataOnServer.run();
}
