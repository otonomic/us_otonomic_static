var ot_analytics = (function() {
    var identify = function (user_unique_id, user_identify_data) {
        user_unique_id      = user_unique_id || null;
        user_identify_data  = user_identify_data || {};

        try {
            var props = Cookies.getJSON('utm');

            if (props && typeof(mixpanel)!='undefined') {
                mixpanel.people.set_once(props);
            }
            /* mixpanel.identify(); */
        } catch(e) {}

        if(user_unique_id) {
            analytics.alias(user_unique_id);
            Cookies.set('user_unique_id', user_unique_id, { expires: 365 });

        } else {
            user_unique_id = Cookies.get('user_unique_id');
        }

        return analytics.identify(user_unique_id, user_identify_data);
    };

    var track = function (event, method, eventData) {
        if(method) {
            var data = {
                'method': method
            };
            eventData = $.extend(true, {}, data, eventData);
        }
        try {
            return analytics.track(event, eventData);
        } catch(e) {
            console.log(e);
        }
        return false
    };

    var store_utm_params = function () {
        var campaign_keywords = 'utm_source utm_medium utm_campaign utm_content utm_term'.split(' ');
        var props = {};

        //check to see if the UTM super props exists
        for (var index = 0; index < campaign_keywords.length; index++) {
            if (get_property(campaign_keywords[index])) {
                props[campaign_keywords[index]] = get_property(campaign_keywords[index]);
            }
        }

        //if at least once exists, create the people properties
        if (Object.keys(props).length) {
            try {
                Cookies.set('utm', props);
            } catch(e) {
                console.log('Failed saving UTM cookie.');
            }
            return props;
        }

        return false;
    };

    var get_property = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    var get_event_name = function(el) {
        if(typeof el.attr('id') !== 'undefined') {
            return "Clicked #" + el.attr('id');
        }
        if(typeof el.attr('name') !== 'undefined') {
            return "Clicked name " + el.attr('name');
        }
        if(el.text().trim()) {
            return "Clicked text '" + el.text().trim() + "'";
        }
        if(typeof el.attr('class') !== 'undefined') {
            return "Clicked ." + el.attr('class').replace(' ', '.');
        }
        return "Clicked an unknown button";
    };

    var get_event_params = function(el) {
        var event_params = {};
        event_params.type = 'click';
        if(typeof el.attr('id') !== 'undefined') {
            event_params.id = el.attr('id');
        }
        if(typeof el.attr('href') !== 'undefined') {
            event_params.href = el.attr('href');
        }
        if(typeof el.attr('name') !== 'undefined') {
            event_params.name = el.attr('name');
        }

        if(typeof el.attr('class') !== 'undefined') {
            event_params.class = el.attr('class');
        }
        if(typeof el.attr('data-analytics') !== 'undefined' && el.attr('data-analytics')) {
            try {
                var data = JSON.parse(el.attr('data-analytics'));
                event_params = $.extend({}, event_params, data);

            } catch(e) {
                console.log('Could not decode JSON string ' + el.attr('data-analytics'));
            }
        }

        return event_params;
    };

    return {
        identify: identify,
        store_utm_params: store_utm_params,
        get_property: get_property,
        track: track,
        get_event_name: get_event_name,
        get_event_params: get_event_params
    };

})();

var props = ot_analytics.store_utm_params();