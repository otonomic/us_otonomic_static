var  ot_facebook = (function() {
    var compile_page_template = function (value) {
        var pageTemplate = '<div data-id="{{page-id}}" data-name="{{page-name}}" data-category="{{page-category}}" class="fb-page-select clearfix"><div class="pull-left"><img width="40" height="40" src="{{page-image}}?width=80&height=80" /></div><div class="pull-left fb-page-details"><div>{{page-name}}</div></div></div>';

        var tpage;
        tpage = pageTemplate;
        tpage = tpage.replace(/{{page-id}}/g, value.id);
        tpage = tpage.replace(/{{page-name}}/g, value.name);
        tpage = tpage.replace(/{{page-category}}/g, value.category);
        tpage = tpage.replace(/{{page-image}}/g, "https://graph.facebook.com/" + value.id + "/picture/");

        return tpage;
    };

    /*
     Turn a javascript array of Facebook fan page objects into an object of a single level, so its properties can be sent to MixPanel for tracking
     */
    var get_pages_shallow_object = function(data) {
        if(typeof(data) == 'undefined') { return {}; }

        var result = {};

        for (var i = 0; i < data.length; i++) {
            result['fbPage' + (i + 1) + 'Name'] = data[i].name;
            result['fbPage' + (i + 1) + 'Id'] = data[i].id;
            result['fbPage' + (i + 1) + 'Category'] = data[i].category;
        }

        return result;
    };

    /*
     Get data about the user for analytics
     */
    var get_user_data = function( callback ) {
        getLongLivedAccessToken(function(response) {
            var access_token = response.access_token;
            jQuery('#fb_user_token').val(access_token);

            Cookies.set('user_fb_token', access_token, { expires: 365 });

            fields = [
                'id',
                'name',
                'first_name',
                'last_name',
                'email',
                'age_range',
                'gender',
                'locale',
                'timezone',
                'link',
                'updated_time',
                'verified',
                'accounts'
            ];

            FB.api('/me', {fields: fields}, function(response) {
                var fbPagesNum = typeof(response.accounts)!='undefined' ? response.accounts.data.length : 0;
                var age_range = typeof(response.age_range)!='undefined' ? response.age_range : {};

                var fbPages = (typeof(response.accounts) !== 'undefined') ? ot_facebook.get_pages_shallow_object(response.accounts.data) : {};

                var user_identify_data = {
                    'username': response.name,
                    'email': response.email,
                    'fbId': response.id,
                    'firstName': response.first_name,
                    'lastName': response.last_name,
                    'ageRangeMin': age_range.min,
                    'ageRangeMax': age_range.max,
                    'gender': response.gender,
                    'locale': response.locale,
                    'timezone': response.timezone,
                    'fbProfile': response.link,
                    'fbAccessToken': access_token,
                    'fbPagesNum': fbPagesNum,
                    'fbVerified': response.verified,
                    'updatedTime': response.updated_time,
                    'createdAt': Date(),

                    '$username': response.name,
                    '$email': response.email,
                    '$first_name': response.first_name,
                    '$last_name': response.last_name,
                };
                user_identify_data = $.extend({}, user_identify_data, fbPages);

                callback(response, user_identify_data);
            });
        });

    };

    /*
     Expose methods as public
     */
    return {
        compile_page_template: compile_page_template,
        get_pages_shallow_object: get_pages_shallow_object,
        get_user_data: get_user_data
    };

})();



function getLongLivedAccessToken(callback) {
    var access_token = FB.getAuthResponse().accessToken;
    $.getJSON('https://graph.facebook.com/v2.5/oauth/access_token', {
        client_id: settings.app_id,
        client_secret: settings.app_secret,
        grant_type: 'fb_exchange_token',
        fb_exchange_token: access_token
    }, function (response) {
        callback(response);
    });
}

function getFacebookPageAddress(page_id) {
    var facebook_query_page_url = "https://graph.facebook.com/" + page_id;
    $.get(facebook_query_page_url, parseFacebookPageAddress(data), "json");
}

// TODO: Test this function
function parseFacebookPageAddress(data) {
    if (data.location != undefined && data.location.latitude != undefined && data.location.longitude != undefined) {
        delete data.location.latitude;
        delete data.location.longitude;
    }
    var address_parts = [];

    for (var x in data.location) {
        address_parts.push(data.location[x]);
    }

    var phone = (data.phone) ? data.phone : "";
    var address = (address_parts.join(", ")) ? address_parts.join(", ") : "";
    var email = (data.email) ? (data.email) : "";

    if (data.likes != undefined) {
        window.page_type = 'Fan Page';
    } else {
        window.page_type = 'Personal Page';
    }
    window.parsed_page_data = {
        'phone': phone,
        'address': address,
        'email': email
    }
}
