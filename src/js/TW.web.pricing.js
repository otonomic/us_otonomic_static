function bindUI() {
    _$modal = $("#plan_modal");

/*
    $(".btn.create_free").on("click", function (e) {
        e.preventDefault();
        window.location = "/create";
        return
    });
*/

/*
    $(".plan").hover(function () {
        var plan_name = $(this).data("plan-name");
        if (plan_name == "enterprise")return;
        $(".plan_details").find('.column[data-plan="' + plan_name + '"]').addClass("active")
    }, function () {
        $(".plan_details").find('.column[data-plan="' + $(this).data("plan-name") + '"]').removeClass("active")
    });
    $("#team_members").on("textchange", function () {
        var num = parseInt($.trim($(this).val()));
        if (parseInt(num) > 5e3) {
            num = 5e3;
            $("#team_members").val(5e3)
        }
        if (parseInt(num)) {
            TS.web.pricing.members_for_calcs = parseInt(num)
        } else {
            TS.web.pricing.members_for_calcs = TS.web.pricing.members
        }
        TS.web.pricing.update()
    });
    $("#annual_toggle").on("change", function () {
        if ($(this).is(":checked")) {
            TS.web.pricing.is_annual = true
        } else {
            TS.web.pricing.is_annual = false
        }
        TS.web.pricing.update()
    }).trigger("change");
    $(".team_credits").html("$" + TS.web.pricing.format_currency(TS.web.pricing.credits));
*/

/*
    _$nav = $(".common_questions_nav");
    _$tweet_section = $(".from_the_wall_of_love");
    _$tweet_carousel = $(".tweet_carousel");
    _$tweet_carousel_container = $(".tweet_carousel_container");
    _$window = $(window);
    _win_scroll_top = _$window.scrollTop();
    _win_height = _$window.height();
    _nav_offset = _$nav.offset();
    _nav_height = _$nav.height();
    _$common_questions_section = $(".common_questions_and_resources");
    _section_offset = _$common_questions_section.offset();
    _section_height = _$common_questions_section.height();
    _top_padding = _win_height / 2 - _nav_height / 2;
    _fixed_set = false;
    _stuck_to_bottom = false;
    _categories = _calculateQuestionCategoryOffests();
    _updateQuestionNavPosition();
    _updateQuestionNavState(_categories, _win_scroll_top);
    if (_$tweet_carousel.length)_startTweetAnimation();
    _$window.scroll(_scrollHander);
    _$window.resize(function () {
        TS.utility.throttle.method(_windowResizeHandler)
    });

    _$tweet_section.find(".tweet_carousel_controls a").on("click", function (e) {
        e.preventDefault();
        _stopTweetAnimation();
        var action = $(e.currentTarget).data("action");
        switch (action) {
            case"left":
                return _moveTweetAnimationLeft();
            case"right":
                return _moveTweetAnimationRight()
        }
    });
*/

    $(".question_list h4 a").on("click", function (e) {
        e.preventDefault();
        var $question_header = $(e.target).closest("li");
        $question_header.toggleClass("expanded");
        $question_header.find(".ts_icon").toggleClass("ts_icon_plus_square_o").toggleClass("ts_icon_minus_square_o");
        _categories = _calculateQuestionCategoryOffests();
        _updateQuestionNavState(_categories, _$window.scrollTop());
        _section_height = _$common_questions_section.height()
    });
    $(".common_questions_nav li a").on("click", function (e) {
        var id = $(e.target).data("associated-section-id");
        var category = _categories.filter(function (element) {
            return element.id === id
        });
        if (category.length === 0)return;
        var scroll_position = category[0].top;
        $("html, body").animate({scrollTop: scroll_position + 10}, 500);
        e.preventDefault()
    });
/*
    $(".back_to_the_top .btn").on("click", function (e) {
        $("html, body").animate({scrollTop: 0}, 500);
        e.preventDefault()
    })
*/
}

bindUI();
