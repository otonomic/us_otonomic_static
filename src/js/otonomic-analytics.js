function is_localhost() {
    if (location.host == 'otonomic.test' || location.host == 'localhost') {
        return true;
    }
    return false;
}

function track_event(category, action, label, value){
	console.log('track_event: ' + category + ' - ' + action + ' - ' + label + ' - ' + value); // DEBUG

    if(label == undefined)
        label = '';

    if(!value){
        value = null;
    }

	submit_options = {
			'event': action,
			'category': category,
			'action': action,
			'label': label,
			'value':value
		}

	// trackOtonomic(submit_options);
//    track_event_otonomic(category, action, label, value);
}

function track_virtual_pageview(url, title) {
    var options = {
        'hitType': 'pageview',
        'page': '/virtual_pageviews/' + url
    };
    if(typeof(title) !== 'undefined') {
        options.title = title;
    }
}
