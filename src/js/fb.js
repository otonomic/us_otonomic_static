var settings;
if(document.domain == 'localhost') {
    settings = {
        app_id: '955071824573738',
        app_secret: '299522c3de843e23fcbd163b772b7810'
    }
} else {
    settings = {
        app_id: '373931652687761',
        app_secret: 'd154036467714f4ac4706e653a1211ad'
    }
}

function saveFBData(response, user_identify_data) {
    if(! (user_unique_id = Cookies.get('user_unique_id')) ) {
        var user_unique_id = response.email || response.id;
        Cookies.set('user_unique_id', user_unique_id, { expires: 365 });
    }

    ot_analytics.identify(
        user_unique_id,
        user_identify_data
    );

    analytics.track('Completed facebook connect', {
        method: 'facebook',
        fbPagesNum: user_identify_data.fbPagesNum
    });

    Cookies.set('user_fb_details', response, { expires: 365 });
    jQuery('#fb_user_id').val(response.id);
    jQuery('.replace-fbname').html(response.first_name);
    saveDataOnServer.run();
}

function modalCallback(event, modal) {
    //console.log(event, modal);
    if(event == 'modalSkipped' && modal=='premissionRejected') {
        ot_analytics.track(
            'Rejected facebook connect',
            'facebook'
        );

        // User rejected permissions - automatically retry until user approves them
        jQuery('#fb-next').trigger('click');

    } else if(event == 'modalSkipped' && modal=='selectPage') {
        // User approved permissions - show a list of Facebook fan pages
        var fb_token = FB.getAuthResponse().accessToken;

        ot_facebook.get_user_data(saveFBData);
        // ot_facebook.get_user_data().then(saveFBData);

        jQuery('#fb-next').removeAttr('disabled');
        loadFBPages({ fb_token: fb_token, supressSlide: false, pageSlide: 'pageselect'});

        ot_analytics.track(
            'Approved facebook connect',
            'facebook',
            {
                'modal': modal,
                'fb_user_details': Cookies.get('user_fb_details')
            }
        );

    }
}

function loadFBPages(options) {
    var fb_token        = options.fb_token;
    var supressSlide    = options.supressSlide;
    var pageSlide       = options.pageSlide;

    jQuery('#fb-page-next').attr('disabled', 'disabled');

    try {
        FB.api('/me/accounts', {
                access_token: fb_token,
                limit: 100,
                fields:'access_token,category,name,id,is_published,perms'
        }, function (response) {
            Cookies.set('fb_user_pages', response, {expires: 365});
            saveDataOnServer.run();

            var page = $('<div />');

            // var $is_published_error = true;
            var pageCount = 0;
            var pageError = true;
            var tpage = '';

            $(response.data).each(function (index, value) {
                pageCount++;
                if(value.is_published) {
                    pageError = false;

                    tpage = ot_facebook.compile_page_template(value);
                    tpage = $(tpage);
                    tpage.attr('data-fb-id', value.id);
                    page.append(tpage);
                }
            });
            if (pageError) {
                pageSlide = facebookPagesError(pageCount);

            } else {
                ot_analytics.track(
                    'Fetched facebook pages',
                    'installer',
                    {
                        'pages': response.data
                    }
                );

                page = page.wrap('<div/>').parent().html();
                jQuery('#page-list').html(page);
                $('#page-list').on('click', ' .fb-page-select', function (e) {
                    $('#page-list .fb-page-select').removeClass('selected');
                    $(this).addClass('selected');
                    $('#fb-page-next').removeAttr('disabled');
                    $('#fb_page_confirm').val(jQuery(this).attr('data-name'));
                });

            }

            if(!supressSlide) {
                move_slide_by_id(pageSlide);
                window.location.hash = pageSlide;
            }
        });
    } catch(e){
        console.log(e);
    }
}

function facebookPagesError(pageCount) {
    if (pageCount > 0) {
        pageSlide = 'bother-unpub';
        event_type = 'fbNoPublishedPagesFound';
        pid = 'unpub';

    } else {
        pageSlide = 'bother-no-page';
        event_type = 'fbNoPagesFound';
        pid = 'no-page';
    }

    ot_analytics.track(
        'Onboarding errors',
        'Installer',
        {
            'pageId':pid
        }
    );

    return pageSlide;
}

function pageSelected(page) {
    jQuery('#facebook_id').val(page.id);
    Cookies.set('fb_selected_page', page.id, { expires: 365 });

    window.location.hash = "#confirm";
    createWebsiteUsingAjax(page.id);
    ot_analytics.track(
        'Selected facebook page',
        'facebook',
        {
            'page id': page.id,
            'page name': page.name,
            'page category': page.category,
            'page link': page.link
        }
    );

    ot_analytics.identify(null, {
        'site fb page id': page.id,
        'site fb page name': page.name,
        'site fb page category': page.category,
        'site fb page category list': JSON.stringify(page.category_list),
        'site fb page url': page.link,
    });

    saveDataOnServer.run();
}




// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        appId      : settings.app_id,
        cookie     : true,
        xfbml      : true,
        version    : 'v2.5'
    });
    jQuery('.oto-fb-login').otoFBLogin({
        template: {
            'appRejected':{
                'view': '/popup-view/app-rejected.html',
                'callback': modalCallback,
                popup: {
                    load:false
                }
            },
            'premissionRejected':{
                'view': '/popup-view/premission-rejected.html',
                'callback': modalCallback,
                popup: {
                    load:false
                }
            },
            'selectPage':{
                'view': '/popup-view/select-page.html',
                'callback': modalCallback,
                popup: {
                    load:false
                }
            }
        },
        permissions: 'email,public_profile,pages_show_list',/* pages_show_list permission is required */
        //onPageSelect: pageSelectCallback,
        onPageSelect: false,
        onProcessStart: function() {
            ot_analytics.track(
                'Started facebook connect',
                'Facebook',
                {}
            );
            //ot_analytics.track('fbLoginStarted', 'facebook', {});
            //console.log('FB login process initiated');
        }
    });

    if(Cookies.get('user_fb_details')) {
        user_fb_details = jQuery.parseJSON(Cookies.get('user_fb_details'));
        jQuery('.replace-fbname').html(user_fb_details.first_name);
    }
    if(Cookies.get('user_fb_token')) {
        loadFBPages({
            fb_token: Cookies.get('user_fb_token'),
            suppressSlide: true,
            pageSlide: ''
        });
        activateFBNext();
    }
};