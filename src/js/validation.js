function activateEmailNext() {
    if(!is_valid_email(jQuery('#user_email').val())) {
        jQuery('#email-next').attr('disabled', 'disabled');
    } else {
        jQuery('#email-next').removeAttr('disabled');
    }
}

function activateBNameNext() {
    jQuery('#business_name-text-limit').hide();
    jQuery('#business_name').removeClass('overlimit');

    if(jQuery('#business_name').val().length<3) {
        jQuery('#bname-next').attr('disabled', 'disabled');

    } else if(jQuery('#business_name').val().length>30) {
        jQuery('#bname-next').attr('disabled', 'disabled');
        jQuery('#business_name-text-limit').show();
        jQuery('#business_name').addClass('overlimit');

    } else {
        jQuery('#bname-next').removeAttr('disabled');
    }
}

function activateFBNext() {
    jQuery('#fb-next').attr('disabled', 'disabled');
    var fb_token = Cookies.get('user_fb_token');
    if(fb_token) {
        /* Check if token is valid */
        try {
            FB.api('/me', {access_token: fb_token,limit: 100}, function (response) {
                jQuery('#fb-next').removeAttr('disabled');
            });
        } catch(e) {
            console.log(e);
        }
    }
}
function is_valid_email(email) {
    var result = email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/);
    return !!result;
}