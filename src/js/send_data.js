var saveDataOnServer = (function() {

    var identified = false;

    getDataFromCookies = function() {
        var id = Cookies.get('otonomic_id');
        var user_email = Cookies.get('user_email');
        var website_name = Cookies.get('user_business_name');
        var fb_user_token = Cookies.get('user_fb_token');
        var fb_user_details = Cookies.get('user_fb_details');
        var fb_user_pages = Cookies.get('fb_user_pages');
        var fb_selected_page = Cookies.get('fb_selected_page');

        return {
            id: id,
            user_email:user_email,
            website_name:website_name,
            fb_user_token:fb_user_token,
            fb_user_details:fb_user_details,
            fb_user_pages:fb_user_pages,
            fb_selected_page:fb_selected_page
        };
    };

    sendRequest = function(dataSent, successCallback) {
        request = $.ajax({
            type: "POST",
            url: '/server/save-data.php',
            data: dataSent,
            success: successCallback
        });
    };

    successCallback = function (data, status, jqxhr) {
        var dataSent = getDataFromCookies();
        data = jQuery.parseJSON(data);
        Cookies.set('otonomic_id', data.id, { expires: 30 });
        Cookies.set('user_website_domain', data.website_domain, { expires: 30 });

        /*
         ot_analytics.track(
            'savedDataOnServer',
            'installer',
            {
                'dataSent': dataSent,
                'datareceived': data
            }
        );
        if(!identified) {
            identified = true;
         ot_analytics.identify(
                data.id,
                dataSent
            );
        }
        */
    };

    run = function() {
        var dataSent = getDataFromCookies();
        sendRequest(dataSent, successCallback);
    };

    return {
        'run': run,
        'getDataFromCookies': getDataFromCookies,
    }
}());
