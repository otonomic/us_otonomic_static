(function ($, window, undefined) {

    var usr_email = getParameterByName('email');
    console.log(usr_email);
    if(usr_email) {
        var disable_automove = true;
        $(document).ready(function (e) {
            $('#user_email').val(usr_email);
            $('#user_email_confirm').val(usr_email);
            $('#email-next').simulateClick();
            $('#user_email-notice').hide();
        });

    }

    var start_slide = $.param.fragment();
    if(start_slide) {
        move_slide_by_id(start_slide);
    }
    $(window).bind( 'hashchange', function(e) {
        var id = $.param.fragment();
        move_slide_by_id(id);
        Cookies.set('oto_stage', id, {expires: 30});
    });

    var settings = {
        user_edits_contact: false,
        user_edits_store: false,
        user_edits_booking: true
    };

    window.do_redirect = 0;

    var page_id = getParameterByName('page_id');
    var page_name = getParameterByName('page_name');
    var category = getParameterByName('category');
    var category_list = getParameterByName('category_list');

    window.authorized_channel = [];

    /*
    var contact_load_timestamp;
    var store_load_timestamp;
    var category_load_timestamp;

    page_load_timestamp = new Date();
     */

    if (page_name) {
        $('.site-name').html(page_name);
        $('.ot-fb-name').html(page_name);
    }

    if(category) {
        $('#fb_category').val(category);
    }

    // Stage next btn
    $('.btn-next').click(function(event){
        //event.preventDefault();
        move_slide( $(this), event);
    });

    /*
    // Stage-3 next btn - Store/Booking
    $('.js-stage3-next').click(function(event){
        //event.preventDefault();
        move_slide( $(this), event);

        var values = {};
        values.show_store = $('#option-online-store').hasClass('checked') ? 'yes' : 'no';
        values.show_booking = $('#option-booking').hasClass('checked') ? 'yes' : 'no';
        enqueue_submit('show_store',   values.show_store,   'send_store');
        return enqueue_submit('show_booking', values.show_booking, 'send_booking');
    });
*/

    $(".js-switch-to-congratz").click(function(event){
        initializeLoading();
//        window.location.hash = "#congratz";
        move_slide( $(this), event);
        $('.websitedomain').html( Cookies.get('user_website_domain') );

        switchToCongratz();
        //$('#User_site_creation').submit();
    });

    $('#User_site_creation').submit(function(){
        return false;
    });

    $('[data-toggle="tooltip"]').tooltip();

    // function that switched to stage-5 from stage-4
    /////////////////////////////////////////////////////
    function switchToCongratz() {
        var form_data = $('#User_site_creation').serialize();

        var request_url;
        request_url = builder_domain+"/migration/index.php";

        enqueue_submit('blogname',   $('#business_name_confirm').val(),   'business_name');
        enqueue_submit('user_email',   $('#user_email_confirm').val(),   'submit_user_email');
    }

    if (page_id) {
        window.site_url = builder_domain+'/wp-admin/admin-ajax.php?action=check_page&page_id='+page_id;
        $('#oto-web-url').html('<a href="'+window.site_url+'">this link</a>');
        createWebsiteUsingAjax(page_id);
        if(settings.user_edits_address) {
            getFacebookPageAddress(page_id);
        }
    }
})(jQuery, window);
