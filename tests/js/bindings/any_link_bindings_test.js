var assert = chai.assert;

describe('any_link_bindings', function() {
    describe('on click a', function () {
        it('should bind return element id as event name if id exists', function () {
            var el = $('#test-id-1');
            ot_analytics.track = sinon.spy();
            el.trigger('click');
            assert(ot_analytics.track.calledOnce);
            assert(ot_analytics.track.getCalls()[0].args, ['Clicked #test-id-1', null, {
                type: 'click',
                id: 'test-id-1',
                class: 'test-class-1a test-class-1b',
                href: 'test-href#123',
                name: 'test-name-1'
            }]);
        });
    });
});