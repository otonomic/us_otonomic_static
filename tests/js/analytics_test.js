var assert = chai.assert;

describe('analytics', function() {
    describe('#ot_analytics.get_event_name()', function () {
        it('should return element id as event name if id exists', function () {
            var el = $('#test-id-1');
            var event_name = ot_analytics.get_event_name(el);
            assert.equal(event_name, 'Clicked #test-id-1');
        });

        it('should return element name as event name if id does not exist', function () {
            var el = $('[name="test-name-2"]');
            var event_name = ot_analytics.get_event_name(el);
            assert.equal(event_name, 'Clicked name test-name-2');
        });

        it('should return element text as event name if id and name do not exist', function () {
            var el = $('.test-class-no-id');
            var event_name = ot_analytics.get_event_name(el);
            assert.equal(event_name, "Clicked 'Click me'");
        });
    });

    describe('#ot_analytics.get_event_params()', function () {
        it('should return element id as event name if id exists', function () {
            var el = $('#test-id-1');
            var event_params = ot_analytics.get_event_params(el);
            assert.deepEqual(event_params, {
                type: 'click',
                id: 'test-id-1',
                class: 'test-class-1a test-class-1b',
                href: 'test-href#123',
                name: 'test-name-1'
            });
        });

        it('should return element params overriden by data-analytics, if set', function () {
            var el = $('#test-id-3');
            var event_params = ot_analytics.get_event_params(el);
            assert.deepEqual(event_params, {
                type: 'click',
                id: 'test-analytics-id',
                class: 'test-class-3a test-class-3b',
                href: 'test-href#123',
                name: 'test-analytics-name'
            });
        });
    });
});