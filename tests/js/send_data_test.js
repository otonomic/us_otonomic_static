var assert = chai.assert;

describe('Send Data', function() {
    describe('#getDataFromCookies()', function () {
        it('should create data object from cookies', function () {
            Cookies = {
                get: function() {}
            };
            sinon.stub(Cookies, 'get').returnsArg(0);
            var result = saveDataOnServer.getDataFromCookies();

            expected = {
                id: 'otonomic_id',
                user_email: 'user_email',
                website_name: 'user_business_name',
                fb_user_token: 'user_fb_token',
                fb_user_details: 'user_fb_details',
                fb_user_pages: 'fb_user_pages',
                fb_selected_page: 'fb_selected_page'
            }
            assert.deepEqual(expected, result);
        });
    });


    describe('#successCallback()', function () {
        it('should set cookies and fire a segment event', function () {
        });
    });
});