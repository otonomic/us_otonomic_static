var assert = chai.assert;

describe('Utility', function() {
    describe('#getParameterByName()', function () {
        it('should get param from string', function () {
            var input = '?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8';

            assert.equal('chrome-instant', getParameterByNameFromString('sourceid', input));
            assert.equal('1', getParameterByNameFromString('ion', input));
            assert.equal('', getParameterByNameFromString('xxxxxxx', input));
        });
    });
});