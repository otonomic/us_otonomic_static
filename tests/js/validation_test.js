var assert = chai.assert;

describe('Validation', function() {
    describe('#is_valid_email()', function () {
        it('should return true for valid emails', function () {
            assert.isFalse(is_valid_email('omri@abc'));
            assert.isFalse(is_valid_email('@otonomic.com'));
            assert.isTrue(is_valid_email('omri@otonomic.com'));
            assert.isTrue(is_valid_email('omri@otonomic.com.br'));
        });
    });
});