var page = require('webpage').create();

// var test_url = 'http://usstatic.otonomic.com/create/';
var test_url = 'http://localhost/us_otonomic_static/output_dev/create/index.html';

page.onConsoleMessage = function(msg, lineNum, sourceId) {
    console.log('    CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
};
page.onCallback = function(data) {
    data.message && console.log(data.message);
    data.exit && phantom.exit();
};

page.open(test_url, function (status) {
    if (status !== 'success') {
        console.error("Failed to open", page.frameUrl);
        phantom.exit();
    }

    page.injectJs("../vendor/mocha.js");
    page.injectJs("../vendor/chai.js");
    page.injectJs("../vendor/sinon-1.17.2.js");
    // page.injectJs("../vendor/jquery.mockjax.js");
    page.injectJs("../vendor/reporter.js");

    page.injectJs("create_test.js");

    page.evaluate(function() {
        // Run tests
        window.mocha.run();
    })
    exit();

    function exit() {
        setTimeout(function () {
            phantom.exit(0);
        }, 2000);
    }
});
