var assert = chai.assert;
// var assert = sinon.assert;

function init_stubs() {
    // Stubs of analytics functions
    spy_sendRequest = sinon.stub(saveDataOnServer, "run", function() {
        console.log('Stub: saveDataOnServer.run() was called.');
    });

    spy_track_event = sinon.stub(window, "track_event", function() {
        console.log('Stub: track_event() was called.');
    });

    spy_fireSegmentIOEvent = sinon.stub(window, "ot_analytics.track", function(event, method, data) {
        console.log('Stub: ot_analytics.track was called with event "'+event+'", method "'+method+'", data '+JSON.stringify(data));
    });
}

function restore_stubs() {
    spy_sendRequest.restore();
    spy_track_event.restore();
    spy_fireSegmentIOEvent.restore();
}

describe('create', function() {
    beforeEach(function() {
        init_stubs();
    });

    afterEach(function() {
        restore_stubs();
    });

    describe('#stage-email', function () {
        it('should have button disabled if email is invalid', function () {
            $("#user_email").val("test");
            $("#user_email").keyup();
            var result = $("#email-next").attr("disabled");
            assert.equal('disabled', result);
        });

        it('Invalid email inserted - without domain extension. Next button should be disabled', function () {
            $("#user_email").val("test@otonomic.");
            $("#user_email").keyup();
            var result = $("#email-next").attr("disabled");
            assert.equal('disabled', result);
        });

        it('Valid email inserted. Next button should not be *enabled*', function () {
            $("#user_email").val("test@otonomic.com");
            $("#user_email").keyup();
            var result = $("#email-next").attr("disabled");
            assert.notEqual('disabled', result);
        });

        it('Click on enabled "next" button should send analytics data', function () {
            $('#email-next').click();
            // assert(spy_fireSegmentIOEvent.calledOnce, 'Incorrect number of calls to analytics function');
            assert(spy_fireSegmentIOEvent.calledWith('Clicked #email-next'), 'Analytics function called with incorrect parameters');
        });

    });

    describe('#stage-business-name', function () {

/*
        beforeEach(function() {
            init_stubs();
        });

        afterEach(function() {
            restore_stubs();
        });
*/

        it('should have next button disabled if input has < 3 chars', function() {
            $('#business_name').val('ot');
            $("#business_name").keyup();
            var result = $("#bname-next").attr("disabled");
            assert.equal('disabled', result);
        });

        it('should make next button enabled if input has > 3 chars', function() {
            $('#business_name').val('otonomic');
            $('#business_name').keyup();
            var result = $("#bname-next").attr("disabled");
            assert.notEqual('disabled', result);
        });
    });

    // $('#bname-next').click();

    // click 'connect to facebook' button
    // $('.wrapper-oto-fb-login .oto-fb-login').click();

});
