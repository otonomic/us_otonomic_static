var assert = chai.assert;

describe('fb', function() {
    describe('#ot_facebook.compile_page_template()', function () {
        it('should return html string with details of Facebook page', function () {
            var value = {
                id:     123,
                name:   'test name',
                category: 'test category'
            };
            result = ot_facebook.compile_page_template(value);
            var expected = '<div data-id="123" data-name="test name" data-category="test category" class="fb-page-select clearfix"><div class="pull-left"><img width="40" height="40" src="https://graph.facebook.com/123/picture/?width=80&height=80" /></div><div class="pull-left fb-page-details"><div>test name</div></div></div>';
            assert.equal(expected, result);
        });
    });

    describe('#facebookPagesError()', function () {
        it('should return bother-no-page when page count is 0', function () {
            ot_analytics.track = sinon.spy();
            assert.equal('bother-no-page', facebookPagesError(0));
            // TODO: Assert that the function was called with the correct parameters
            assert(ot_analytics.track.calledOnce);
        });

        it('should return bother-unpub when page count is > 0', function () {
            ot_analytics.track = sinon.spy();
            assert.equal('bother-unpub', facebookPagesError(1));
            // TODO: Assert that the function was called with the correct parameters
            assert(ot_analytics.track.calledOnce);
        });
    });
});