module.exports = function(grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    captureFile: 'results.txt', // Optionally capture the reporter output to a file
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
                },
                src: ['tests/**/*.js']
            }
        },

        mocha: {
            test: {
                src: ['tests/**/*.html'],
                options: {
                    run: true
                }
            }
        },

        exec: {
            files: ["tests/js/acceptance/*.js"],
            phantom_test: {
                cmd: 'phantomjs tests/js/acceptance/create_site_runner.js'
            }
        },


        sass: {
            main_style: {
                options: {
                    compass: false,
                    style: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: 'src/scss',
                    src: ['*.scss'],
                    dest: 'dist/css',
                    ext: '.css'
                }]
            }
        },

        imagemin: {
            options: {
                cache: false
            },

            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/'
                }]
            }
        },

        watch: {
            sass: {
                files : ['src/scss/**/*.scss', '!*tmp*.js'],
                tasks : ['sass']
            },
            sculpin: {
                files : ['source/*.html'],
                tasks : ['sculpin']
            },
            jade: {
                files : ['src/jade/**/*.jade'],
                tasks : ['jade']
            },

        },

        concat: {
            options: {
                separator: ';\n'
            },
            dist: {
                src: [
                    'src/js/ot_analytics/ot_analytics.js',
                    'src/js/bindings/any_link_bindings.js',
                    'src/js/bindings/track_lead.js',
                    'src/js/TW.web.pricing.js'
                ],
                dest: 'dist/js/main.js'
            },

            create: {
                src: [
                    'src/js/init.js',

                    'src/js/bindings/bindings.js',
                    'src/js/bindings/email_bindings.js',
                    'src/js/bindings/business_name_bindings.js',
                    'src/js/bindings/confirm_bindings.js',
                    'src/js/bindings/facebook_pages_bindings.js',
                    'src/js/bindings/track_lead.js',

                    'src/js/loading_messages/script.js',

                    'src/js/ticker.js',
                    'src/js/vars.js',
                    'src/js/utility.js',
                    'src/js/send_data.js',
                    'src/js/validation.js',
                    'src/js/slide.js',
                    'src/js/site_creation.js',
                    'src/js/plugin-click.js'
                ],
                dest: 'dist/js/create.js'
            },

            landingPageScript: {
                src: [
                    'src/js/lp/segmentio.js',
                    'src/js/lp/mixpanel-track-utm-through-segmentio.js',
                    'src/js/lp/fb-sdk.js',
//                    'src/js/lp/lp_signup.js',
                    'src/js/lib/jquery.ba-bbq.min.js'
                ],
                dest: 'dist/js/lp.js'
            },
        },

        es6transpiler: {
            dist: {
                files: {
                    'main.js': 'dist/js/main.js'
                }
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            build: {
                src: 'dist/js/main.js',
                dest: 'dist/js/main.js'
            }
        },

        scsslint: {
            allFiles: [
                'src/scss/*.scss',
            ],
            options: {
            },
        },

        jshint: {
            all: ['Gruntfile.js', 'src/js/**/*.js', 'js/**/*.js', 'tests/**/*.js']
        },

        'sculpin-generate': {
            options: {
                // Task-specific options go here.
            },
            prod: {
                args: {
                    env: 'prod'
                }
            },
            dev: {
                args: {
                    env: 'dev'
                }
            }
        },

        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: [ {
                    expand: true,
                    src: "**/*.jade",
                    dest: "dist",
                    cwd: "src/jade/pages",
                    ext: '.html'
                } ]
            }
        },

        copy: {
            main: {
                files: [
                    // includes files within path and its sub-directories
                    {expand: true, cwd: 'src/fonts', src: ['**/*'], dest: 'dist/fonts'},
                    {expand: true, cwd: 'src/js/lib', src: ['**/*'], dest: 'dist/js'},
                    {expand: true, cwd: 'src/js/slack', src: ['**/*'], dest: 'dist/js'}
                ],
            },
        },

        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                metadata: '',
                regExp: false
            }
        }
    });

    grunt.registerTask('default', ['dev']);

    grunt.registerTask('dev', ['compileassets']);
    grunt.registerTask('push', ['dev', 'bump']);
    grunt.registerTask('deploy', ['compileassets', 'images']);

	grunt.registerTask('compileassets', ['html', 'css', 'js', 'copy']);
    grunt.registerTask('test', ['jshint', 'mocha', 'exec']);
    grunt.registerTask('html', ['jade']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('images', ['imagemin']);
};
